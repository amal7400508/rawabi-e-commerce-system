<?php

return [
    'delete_event_type' => 'delete event type',
    'delete_categories' => 'delete categories',
    'delete_unit' => 'delete unit',
    'delete_offer_type' => 'delete offer type',
    'delete_notification' => 'delete notification',
    'delete_taste' => 'delete taste',
];
