<?php

return [
    'show_event_type' => 'عرض نوع المناسبة',
    'create_event_type' => 'إنشاء نوع المناسبة',
    'update_event_type' => 'تعديل نوع المناسبة',
    'delete_event_type' => 'حذف نوع المناسبة',

    'show_categories' => 'عرض التصنيفات',
    'create_categories' => 'إنشاء التصنيفات',
    'update_categories' => 'تعديل التصنيفات',
    'delete_categories' => 'حذف التصنيفات',

    'show_unit' => 'عرض الوحدة',
    'create_unit' => 'إنشاء الوحدة',
    'update_unit' => 'تعديل الوحدة',
    'delete_unit' => 'حذف الوحدة',

    'show_offer_type' => 'عرض نوع العرض',
    'create_offer_type' => 'إنشاء نوع العرض',
    'update_offer_type' => 'تعديل نوع العرض',
    'delete_offer_type' => 'حذف نوع العرض',

    'show_notification' => 'عرض الإشعارات',
    'create_notification' => 'إنشاء الإشعارات',
    'update_notification' => 'تعديل الإشعارات',
    'delete_notification' => 'حذف الإشعارات',

    'show_taste' => 'عرض المذاق',
    'create_taste' => 'إنشاء المذاق',
    'update_taste' => 'تعديل المذاق',
    'delete_taste' => 'حذف المذاق',

    'show_advertisement' => 'عرض الأعلانات',
    'create_advertisement' => 'إنشاء الأعلانات',
    'update_advertisement' => 'تعديل الأعلانات',
    'delete_advertisement' => 'حذف الأعلانات',

    'show_minimum_order' => 'عرض الحد الأدنى للطلب',
    'create_minimum_order' => 'إنشاء الحد الأدنى للطلب',
    'update_minimum_order' => 'تعديل الحد الأدنى للطلب',
    'delete_minimum_order' => 'حذف الحد الأدنى للطلب',

    'show_boxing__types' => 'عرض تسعيرة التغليف',
    'create_boxing__types' => 'إنشاء تسعيرة التغليف',
    'update_boxing__types' => 'تعديل تسعيرة التغليف',
    'delete_boxing__types' => 'حذف تسعيرة التغليف',

    'show_delivery_pricing' => 'عرض تسعيرة التوصيل',
    'create_delivery_pricing' => 'إنشاء تسعيرة التوصيل',
    'update_delivery_pricing' => 'تعديل تسعيرة التوصيل',
    'delete_delivery_pricing' => 'حذف تسعيرة التوصيل',
];
