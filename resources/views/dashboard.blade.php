@php($is_morris = 1)
@php($page_title = __('admin.Dashboard'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-body">
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-12">
                    <a href="{{url('app/users')}}">
                        <div class="card pull-up bg-cyan">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-white text-left">
                                            <h3 class="text-white">{{$users}}</h3>
                                            <span>{{__('admin.total_users')}}</span>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="ft ft-users text-white font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!--/ end users-->

                <!-- start carrier -->
                <div class="col-xl-3 col-lg-6 col-12">
                    <a href="{{url('carrier_management/carriers')}}">
                        <div class="card pull-up bg-gradient-directional-blue"
                             style="background: #19365d">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-white text-left">
                                            <h3 class="text-white">{{$carrier}}</h3>
                                            <span>{{__('admin.total_carrier')}}</span>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="la la-truck text-white font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!--/ end users-->

                <!-- start product -->
                <div class="col-xl-3 col-lg-6 col-12">
                    <a href="{{url('providers_management/branches')}}">
                        <div class="card pull-up bg-orange">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-white text-left">
                                            <h3 class="text-white">{{$branch}}</h3>
                                            <span>{{__('admin.total_branch')}}</span>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="la la-building text-white font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!--/ end product-->

                <!-- start request -->
                <div class="col-xl-3 col-lg-6 col-12">
                    <a href="{{url('requests_management/request')}}">
                        <div class="card pull-up bg-danger">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-white text-left">
                                            <h3 class="text-white">{{$request}}</h3>
                                            <span>{{__('admin.total_request')}}</span>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="ft ft-shopping-cart text-white font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!--/ end request-->
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card pull-up">
                        <div class="card-content collapse show bg-hexagons">
                            <div class="card-body pt-0 text-center">
                                <div id="donut-users" class="chart-dashboard"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card pull-up">
                        <div class="card-content collapse show bg-hexagons">
                            <div class="card-body pt-0 text-center">
                                <div id="donut-carrier" class="chart-dashboard"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card pull-up">
                        <div class="card-content collapse show bg-hexagons">
                            <div class="card-body pt-0 text-center">
                                <div id="donut-product" class="chart-dashboard"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card pull-up">
                        <div class="card-content collapse show bg-hexagons">
                            <div class="card-body pt-0 text-center">
                                <div id="donut-request" class="chart-dashboard"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Appointment Bar Line Chart -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{__('admin.change_curve_request')}}</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body chartjs">
                                <canvas id="combo-bar-line" height="400"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Appointment Bar Line Chart Ends -->
            </div>
            <script>
                Morris.Donut({
                    element: 'donut-users',
                    data: [
                        {label: "{{__('admin.active')}}", value: "{{$users_active}}"},
                        {label: "{{__('admin.black_list')}}", value: "{{$users_black_list}}"},
                        {label: "{{__('admin.deleted')}}", value: "{{$users_delete}}"},
                        {label: "{{__('admin.attitude')}}", value: "{{$users_stop}}"}
                    ],
                    colors: ["#0f9eaf", "#47cadc", "#47cadc", "#47cadc"],

                });
                Morris.Donut({
                    element: 'donut-carrier',
                    data: [
                        {label: "{{__('admin.active')}}", value: "{{$carrier_active}}"},
                        {label: "{{__('admin.attitude')}}", value: "{{$carrier_stop}}"},
                    ],
                    colors: ["#19365d", "rgb(11, 98, 164)"],
                });
                Morris.Donut({
                    element: 'donut-product',
                    data: [
                        {label: "{{__('admin.enable')}}", value: "{{$branch_enable}}"},
                        {label: "{{__('admin.disable')}}", value: "{{$branch_disable}}"},
                    ],
                    colors: ["#fab216", "#ffc107"],
                });
                Morris.Donut({
                    element: 'donut-request',
                    data: [
                        {label: "{{__('admin.has_been_approved')}}", value: "{{$request_has_been_approved}}"},
                        {label: "{{__('admin.not_approved')}}", value: "{{$request_not_approved}}"},
                    ],
                    colors: ["#f91735", "#fe5067", "#fd7b8c", "#fbafb9"],
                });
            </script>
            <script>
                $(window).on("load", function () {
                    /*Get the context of the Chart canvas element we want to select*/
                    var ctx = $("#combo-bar-line");
                    Chart.Legend.prototype.afterFit = function () {
                        this.height = this.height + 50;
                    };
                    /*Chart Options*/
                    var chartOptions = {
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            xAxes: [{
                                display: true,
                                barPercentage: 0.75,
                                categoryPercentage: 0.3,
                                gridLines: {
                                    color: "#f3f3f3",
                                    drawTicks: false,
                                },
                                scaleLabel: {
                                    display: false,
                                    labelString: 'Days'
                                }
                            }],
                            yAxes: [{
                                display: true,
                                gridLines: {
                                    color: "#f3f3f3",
                                    drawTicks: false,
                                },
                                scaleLabel: {
                                    display: false,
                                    labelString: 'Value'
                                }
                            }]
                        },
                        title: {
                            display: false,
                            text: 'Appointment Statistics'
                        }
                    };
                    /* Chart Data*/
                    var chartData = {
                        labels: ["{{__('admin.SUN')}}", "{{__('admin.MON')}}",
                            "{{__('admin.TUE')}}", "{{__('admin.WED')}}", "{{__('admin.THU')}}", "{{__('admin.FRI')}}", "{{__('admin.SAT')}}"],
                        datasets: [{
                            type: 'line',
                            label: "{{__('admin.change_curve')}}",
                            data: [{{$reports[0]}},{{$reports[1]}},{{$reports[2]}},{{$reports[3]}},{{$reports[4]}},{{$reports[5]}},{{$reports[6]}}],
                            borderColor: "rgb(30,159,242)",
                            backgroundColor: "transparent",
                            borderWidth: 2,
                            pointBorderColor: "#1e9ff2",
                            pointBackgroundColor: "#FFF",
                            pointBorderWidth: 2,
                            pointHoverBorderWidth: 2,
                            pointRadius: 4,
                        },
                            {
                                type: 'bar',
                                label: "{{__('admin.requests')}}",
                                data: [{{$reports[0]}},{{$reports[1]}},{{$reports[2]}},{{$reports[3]}},{{$reports[4]}},{{$reports[5]}},{{$reports[6]}}],
                                backgroundColor: "#00A5A8",
                                borderColor: "transparent",
                                borderWidth: 2
                            },
                            {
                                type: 'bar',
                                label: "{{__('admin.requests')}} {{__('admin.from_delivery')}}",
                                data: [{{$reports_request_from_delivery[0]}},{{$reports_request_from_delivery[1]}},{{$reports_request_from_delivery[2]}},
                                    {{$reports_request_from_delivery[3]}},{{$reports_request_from_delivery[4]}},{{$reports_request_from_delivery[5]}},{{$reports_request_from_delivery[6]}}],
                                backgroundColor: "#853a1b",
                                borderColor: "transparent",
                                borderWidth: 2
                            },

                            {
                                type: 'bar',
                                label: "{{__('admin.has_been_approved')}}",
                                data: [{{$reports_status_1[0]}},{{$reports_status_1[1]}},{{$reports_status_1[2]}},
                                    {{$reports_status_1[3]}},{{$reports_status_1[4]}},{{$reports_status_1[5]}},{{$reports_status_1[6]}}],
                                backgroundColor: "#0679f0",
                                borderColor: "transparent",
                                borderWidth: 2
                            },
                            {
                                type: 'bar',
                                label: "{{__('admin.not_approved')}}",
                                data: [{{$reports_status_0[0]}},{{$reports_status_0[1]}},{{$reports_status_0[2]}},
                                    {{$reports_status_0[3]}},{{$reports_status_0[4]}},{{$reports_status_0[5]}},{{$reports_status_0[6]}}],
                                backgroundColor: "#EA1C0D",
                                borderColor: "transparent",
                                borderWidth: 2
                            },
                            {
                                type: 'bar',
                                label: "{{__('admin.canceled')}}",
                                data: [{{$reports_status_canceled[0]}},{{$reports_status_canceled[1]}},
                                    {{$reports_status_canceled[2]}},{{$reports_status_canceled[3]}},
                                    {{$reports_status_canceled[4]}},{{$reports_status_canceled[5]}},{{$reports_status_canceled[6]}}],
                                backgroundColor: "#36474F",
                                borderColor: "transparent",
                                borderWidth: 2
                            },
                            {
                                type: 'bar',
                                label: "{{__('admin.Special_requests')}}",
                                data: [{{$reports_special_request[0]}},{{$reports_special_request[1]}},
                                    {{$reports_special_request[2]}},{{$reports_special_request[3]}},
                                    {{$reports_special_request[4]}},{{$reports_special_request[5]}},{{$reports_special_request[6]}}],
                                backgroundColor: "#548164",
                                borderColor: "transparent",
                                borderWidth: 2
                            },
                        ]
                    };
                    var config = {
                        type: 'bar',
                        /*Chart Options*/
                        options: chartOptions,
                        data: chartData
                    };
                    /* Create the chart*/
                    var lineChart = new Chart(ctx, config);
                });
            </script>
        </div>
    </div>
@endsection
@section('script')
@endsection
