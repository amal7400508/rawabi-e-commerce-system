{{-- start model Don Delete Message--}}
<div id="messageDonDelete" class="modal fade text-left" role="dialog">
    <div class="modal-dialog">
        <div class="card-content collapse show">
            <div class="card-body">
                <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                    <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                    <strong id="title-error">{{__('provider.successfully_done')}}!</strong>
                    <p id="error-deleting">{{__('provider.deleted_successfully')}}.</p>
                </div>
            </div>
        </div>
    </div>
</div>
{{--end model Don Delete Message--}}
