<div id="confirm-modal-loading-show" class="modal text-left" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content text-center">
            <hr>
            <a id="message-loading-or-error">  {{__('admin.loading')}} <i class="la la-spinner spinner"></i></a>
            <hr>
        </div>
    </div>
</div>
