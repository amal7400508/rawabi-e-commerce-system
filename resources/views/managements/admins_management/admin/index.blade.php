@php($page_title = __('admin.employees'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.employees_affairs')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item active">{{__('admin.employees')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="card">
            <div class="card-head">
                <div class="card-header">
                    @can('create admin')
                        <a href="{{ route('admin.create')}}" class="btn btn-primary"><i
                                class="ft-plus white"></i> {{__('admin.add')}} {{__('admin.employee')}} </a>
                    @endcan
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <table width="100%"
                               class="table table-white-space  {{--row-grouping--}} display no-wrap icheck table-middle"
                               id="user_table">
                            <thead>
                            <tr>
                                <th hidden>{{__('admin.updated_at')}}</th>
                                <th>{{__('admin.name')}}</th>
                                <th>{{__('admin.email')}}</th>
                                <th>{{__('admin.gender')}}</th>
                                <th>{{__('admin.phone')}}</th>
                                <th>{{__('admin.status')}}</th>
                                <th>{{__('admin.action')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                </div>
            </div>
        </div>
    </div>
    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center" style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark" data-dismiss="modal">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- start model show  Message--}}
    <div id="confirmModalShow" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="">
                    <div class="card-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                        </button>
                        <div class="form-details card-body row">
                            <div class="col-md-4">
                                <img src="" class="rounded-circle height-150" id="user-image">
                            </div>
                            <div class="col-md-8">
                                <h2 class="name product-title"
                                    style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif padding-top: 25px;"></h2>
                                <h6 class="email product-title"
                                    style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                                <h6 class="phone product-title"
                                    style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                                <div class=" btn-round position-absolute round"
                                     style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif font-size: 20px"
                                     id="show_status"></div>
                            </div>
                        </div>
                        <table class="table  mb-0" id="table-desc-admin">
                            <tr>
                                <th>{{__('admin.id')}}</th>
                                <td id="id">25 Year</td>
                            </tr>
                            <tr>
                                <th>{{__('admin.gender')}}</th>
                                <td id="Gender"></td>
                            </tr>
                            <tr>
                                <th>{{__('admin.the')}}{{__('admin.role')}}</th>
                                <td id="role_admin"></td>
                            </tr>
                        </table>
                        <div class="row p-1">
                            <div class="col-sm-12">
                                <span class="details price" style="color: #0679f0"></span>
                                <hr>
                            </div>
                            <div class="col-sm-9">
                                <small class="price category-color">{{__('admin.created_at')}}:</small>
                                <small class="price category-color" id="created"></small>
                                <br>
                                <small class="price category-color">{{__('admin.updated_at')}}:</small>
                                <small class="price category-color" id="updated_at"></small>
                            </div>
                            <div class="btn_dele col-sm-3 text-center">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show  Message--}}

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

@endsection
@section('script')
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    <!-- BEGIN: Page JS-->
    <script>
        /*start code show all data in datatable ajax*/
        $(document).ready(function () {
            $('#user_table').DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{  route('admin') }}",
                },
                columns: [
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                        class: 'updated_at'
                    },
                    {
                        data: 'name',
                        name: 'name',
                        class: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email',
                        class: 'email'
                    },
                    {
                        data: 'gender',
                        name: 'gender',
                        class: 'gender',
                        render: function (data, type, full, meta) {
                            if (data == 'man') {
                                return "<div class='fonticon-wrap'><i class='icon-user' style='color:#8BC34A'></i></div>"
                            } else {
                                return "<div class='fonticon-wrap'><i class='icon-user-female' style='color:#FC0021'></i></div>"
                            }
                        },
                    },
                    {
                        data: 'phone',
                        name: 'phone',
                        class: 'phone',
                    },
                    {
                        data: 'status',
                        name: 'status',
                        class: 'status',
                        render: function (data, type, full, meta) {
                            if (data == 1) {
                                return "<div class='fonticon-wrap'><i class='ft-unlock' style='color:#0679f0'></i></div>"
                            } else if (data == 2) {
                                return "<div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>"
                            }
                        },
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    }
                ],
                'order': [[0, 'desc']],
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });
        });
        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        /*end  message save*/
        /*start code Delete ajax*/
        var $carrier_id;
        $(document).on('click', '.deleteB', function () {
            console.log('click befo');
            $carrier_id = $(this).attr('id');
            $('#confirmModalDelete').modal('show');
            console.log($carrier_id);
        });
        $('#ok_button').click(function () {
            console.log('click end');
            $.ajax({
                url: "carrier/destroy/" + $carrier_id,
                beforeSend: function () {
                    $('#ok_button').text('{{__('admin.deleting')}}...');
                },
                success: function (data) {
                    console.log('click success'),
                        setTimeout(function () {
                            $('#confirmModalDelete').modal('hide');
                            $('#user_table').DataTable().ajax.reload();
                            console.log('click before send'),
                                $('#ok_button').text('{{__('admin.yes')}}');
                        }, 500);
                    $('#messageDonDelete').modal('hide');
                    setTimeout(function () {
                        $('#messageDonDelete').modal('show');
                    }, 510,);
                    setTimeout(function () {
                        $('#messageDonDelete').modal('hide');
                    }, 3000,);
                    $('#confirmModalShow').modal('hide');
                },
                error: function (data) {
                    console.log('click failed')
                }
            })
        });
        /*end code Delete ajax*/

        /*start code show details ajax*/
        var admin_no;
        $(document).on('click', '.show-admin', function () {
            admin_no = $(this).attr('id');
            detail_url = "{{ url('admins_management/admin/show')}}" + "/" + admin_no;
            console.log(detail_url);
            console.log('click show');
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $('#confirm-modal-loading-show').modal('hide');
                    if (data.status == 1) {
                        $('#show_status').html('<button class="btn btn-round btn-primary btn-sm">{{__('admin.active')}}</button>');
                        $('#table-desc-admin').attr('style', 'background-color: #fff');
                    } else if (data.status == 2) {
                        $('#show_status').html('<button class="btn btn-round btn-danger btn-sm">{{__('admin.attitude')}}</button>');
                        $('#table-desc-admin').attr('style', 'background-color: #fefafa');
                    }
                    if (data.gender === 'man') {
                        $('#Gender').text("{{__('admin.man')}}");
                    } else {
                        $('#Gender').text("{{__('admin.woman')}}");
                    }
                    console.log(data.gender);
                    $('.form-details .name').text(data.name);
                    $('.form-details .email').text(data.email);
                    $('.form-details .phone').text(data.phone);
                    $('#id').text(data.id);
                    $('#created').text(data.created_at);
                    $('#updated_at').html(data.updated_at);
                    $('#role_admin').html(data.role_admin);
                    $('#user-image').attr('src', '{{asset('/storage/admins/360')}}' +"/"+ data.image);
                    $('#confirmModalShow').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code show details ajax*/
    </script>
    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
@endsection
