@php($page_title = __('admin.employees'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block"> {{__('admin.employees_affairs')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="{{ url('/provider')}}">{{__('admin.employees')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.edit')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            @if(session('success'))
                <div id="messageSave" class="modal fade text-left" role="dialog">
                    <div class="modal-dialog">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                    <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                    <strong>{{__('admin.successfully_done')}}!</strong>
                                    <p>{{session('success')}}.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <div class="card">
                    <form class="form" action="{{ route('admin.update',['id'=>$admin->id])}}" method="POST"
                    <form class="form" action="{{ route('admin.update',['id'=>$admin->id])}}" method="POST"
                          id="my_form_id" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <h4 class="card-title"
                                id="from-actions-top-bottom-center">{{__('admin.create')}} {{__('admin.employee')}}</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <fieldset>
                                        <div class="float-left">
                                            <input type="checkbox" class="switch" id="switch10" data-off-label="false"
                                                   data-on-label="false" data-icon-cls="fa"
                                                   data-off-icon-cls="icon-user-female" data-on-icon-cls="icon-user"
                                                   value="man" name="gender"
                                                   @if(count($errors)>0) @if(old('gender')=='man') checked @endif
                                                   @else @if($admin->gender=='man') checked @endif
                                                @endif
                                            />
                                        </div>
                                    </fieldset>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.name')}}<span class="danger">*</span></label>
                                                    <input type="text" required value=""
                                                           class="form-control @error('name') is-invalid @enderror"
                                                           name="name" id="name">
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.email')}} <span
                                                            class="danger">*</span></label>
                                                    <input type="email" required value=""
                                                           class="form-control @error('email') is-invalid @enderror"
                                                           name="email" id="email">
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group col-12">
                                                <label>{{__('admin.status')}}</label>
                                                <div class="input-group">
                                                    <div class="row icheck_minimal skin">
                                                        <fieldset style="margin-left: 20px; margin-right: 20px">
                                                            <input type="radio" name="status" id="input-radio-15" value="1">
                                                            <label
                                                                style="color: #0679f0">{{__('admin.available')}}</label>
                                                        </fieldset>
                                                        <fieldset style="margin-left: 20px; margin-right: 20px">
                                                            <input type="radio" name="status" id="input-radio-16" value="2">
                                                            <label
                                                                style="color: #FFC107;">{{__('admin.un_available')}}</label>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.phone')}} <span
                                                            class="danger">*</span></label>
                                                    <input type="tel" required value=""
                                                           class="form-control @error('phone') is-invalid @enderror"
                                                           name="phone" id="phone">
                                                    @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.image')}}</label>
                                                    <input type="text" hidden name="img" value="{{$admin->image}}"
                                                           id="image-name">
                                                    <input type="file" accept=".jpg, .jpeg, .png" id="edit_image_ar"
                                                           class="form-control @error('image') is-invalid @enderror"
                                                           name="image" onchange="loadAvatar(this);">
                                                    <button type="button"
                                                            @if($admin->image == null or $admin->image == 'avatar.jpg') hidden
                                                            @endif
                                                            onclick="delete_img()" style="position: static; "
                                                            class="btn btn-sm btn-outline-danger" id="delete-img"><i
                                                            class="ft-trash"></i></button>
                                                    <img id="avatar"
                                                         @if($admin->image == 'avatar.jpg') hidden @endif
                                                         @if($admin->image != null)
                                                         src="{{asset('storage/admins/150').'/'. $admin->image}}"
                                                         @endif
                                                         style="max-width: 140px; height: auto; margin:10px;">
                                                    @error('image')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.roles')}}</label>
                                                    <select name="role" class="form-control">
                                                        <option value="">{{__('admin.select_option')}}</option>
                                                        @foreach($role as $roles)
                                                            <option value="{{$roles->name}}"
                                                                    @if($role_admin==$roles->name) selected @endif>
                                                                {{$roles->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="col-md-2 btn btn-primary pull-right" id="button_save">
                                        <i class="ft ft-save"></i> {{__('admin.edit')}}
                                    </button>
                                    <a href="{{ url('/admins_management/admin')}}" class="col-md-1 btn btn-dark">
                                        <i class="ft-x"></i> {{__('admin.cancel')}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            @if(count($errors)>0)
            @if(old('status')== 1)
            $('#input-radio-15').iCheck('check');
            @elseif(old('status')== 2)
            $('#input-radio-16').iCheck('check');
            @endif
            document.getElementById('name').value = "{{old('name')}}";
            document.getElementById('phone').value = "{{old('phone')}}";
            document.getElementById('email').value = "{{old('email')}}";
            document.getElementById('role-name').value = "{{old('role')}}";
            @if(old('gender')== 'man')
            $('#switch10').attr('checked', true);
            @else
            $('#switch10').attr('checked', false);
            @endif
            @else
            @if($admin->status== 1)
            $('#input-radio-15').iCheck('check');
            @elseif($admin->status== 2)
            $('#input-radio-16').iCheck('check');
            @endif
            document.getElementById('name').value = "{{$admin->name}}";
            document.getElementById('phone').value = "{{$admin->phone}}";
            document.getElementById('email').value = "{{$admin->email}}";
            document.getElementById('role-name').value = "{{$admin->role}}";
            @endif
        });

        function delete_img() {
            document.getElementById('edit_image_ar').value = null;
            $('#avatar').attr('hidden', true);
            $('#delete-img').attr('hidden', true);
            document.getElementById('image-name').value = null;
        }

        $(document).on('click', '#button_save', function () {
            document.getElementById('my_form_id').submit();
            $('#button_save').attr('disabled', true);
        });

        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);

        /*end  message save*/

        function loadAvatar(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#avatar').attr('hidden', false);
                    $('#delete-img').attr('hidden', false);
                    document.getElementById('image-name').value = e.target.result;
                    var image = document.getElementById('avatar');
                    image.src = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/checkbox-radio.js')}}"></script>
@endsection
