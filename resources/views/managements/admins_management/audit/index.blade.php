@php($page_title = __('admin.audit'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.audit')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item active">{{__('admin.audit')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="card">
            <div class="card-head">
                <div class="card-header">
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <table width="100%"
                               class="table table-white-space  {{--row-grouping--}} display no-wrap icheck table-middle"
                               id="user_table">
                            <thead>
                            <tr>
                                <th hidden>{{__('admin.updated_at')}}</th>
                                <th>{{__('admin.your_username')}}</th>
                                <th>{{__('admin.event')}}</th>
                                <th>{{__('admin.table')}}</th>
                                <th>{{__('admin.old_value')}}</th>
                                <th>{{__('admin.new_value')}}</th>
                                <th>{{__('admin.user_agent')}}</th>
                                <th>{{__('admin.date_process')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    <!-- BEGIN: Page JS-->
    <script>
        /*start code show all data in datatable ajax*/
        $(document).ready(function () {
            $('#user_table').DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{  route('audit') }}",
                },
                columns: [
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                        class: 'updated_at'
                    },
                    {
                        data: 'admin_name',
                        name: 'admin_name',
                        class: 'admin_name'
                    },
                    {
                        data: 'event',
                        name: 'event',
                        class: 'event',
                    },
                    {
                        data: 'auditable_type',
                        name: 'auditable_type',
                    },
                    {
                        data: 'old_values',
                        name: 'old_values',
                    },
                    {
                        data: 'new_values',
                        name: 'new_values',
                    },
                    {
                        data: 'user_agent',
                        name: 'user_agent',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                ],
                'order': [[0, 'desc']],
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });
        });
    </script>
    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
@endsection
