@php($page_title = __('admin.permission'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.employees_affairs')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('dashboard')}}">{{__('admin.Dashboard')}}</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('/admins_management/permission')}}">{{__('admin.permission')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.show')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-head">
            </div>
            <div class="card-content">
                <div class="card-body">
                    <table class="table ">
                        <thead>
                        <tr>
                            <th>{{__('admin.screen')}}</th>
                            <th>{{__('admin.show')}}</th>
                            <th>{{__('admin.create')}}</th>
                            <th>{{__('admin.edit')}}</th>
                            <th>{{__('admin.delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <th>{{__('admin.Dashboard')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show dashboard')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>

                        <tr>
                            <th>{{__('admin.main_categories')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show main_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create main_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update main_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete main_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.working_times')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show working_times')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create working_times')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update working_times')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete working_times')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.sub_categories')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.areas')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show areas')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create areas')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update areas')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete areas')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.unit_types')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show unit_types')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create unit_types')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update unit_types')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete unit_types')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.units')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show units')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create units')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update units')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete units')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.colors')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show color')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create color')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update color')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete color')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.deal_of_the_day_types')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show deal_of_the_day_types')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create deal_of_the_day_types')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update deal_of_the_day_types')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete deal_of_the_day_types')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.notes')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show notes')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create notes')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update notes')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete notes')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.branches_categories')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show pharmacy_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create pharmacy_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update pharmacy_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete pharmacy_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.main_category_categories')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show main_category_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create main_category_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete main_category_categories')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.branch_services')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show branch_services')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create branch_services')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete branch_services')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.social_medias')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show social_medias')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create social_medias')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update social_medias')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete social_medias')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.minimum_order')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show ceiling')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create ceiling')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update ceiling')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete ceiling')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>

{{--                        <tr>--}}
{{--                            <th>{{__('admin.advertisement_types')}}</th>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='show advertisement_types')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='create advertisement_types')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='update advertisement_types')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='delete advertisement_types')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                        </tr>--}}
                        <tr>
                            <th>{{__('admin.advertisements')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show advertisements')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create advertisements')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update advertisements')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete advertisements')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
{{--                        <tr>--}}
{{--                            <th>{{__('admin.paid_advertisements')}}</th>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='show paid_advertisements')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='create paid_advertisements')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='update paid_advertisements')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='delete paid_advertisements')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                        </tr>--}}

                        <tr>
                            <th>{{__('admin.material')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show material')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create material')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update material')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete material')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.users')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show users')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update users')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.deposits')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show deposits')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create deposits')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update deposits')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.event_type')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show event_type')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create event_type')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update event_type')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete event_type')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>

                        <tr>
                            <th>{{__('admin.user_occasions')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show user_event')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>

                        <tr>
                            <th>{{__('admin.users_locations')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show user_addresses')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.products')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show products')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update products')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                        </tr>
{{--                        <tr>--}}
{{--                            <th>{{__('admin.attentions')}}</th>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='show attentions')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='create attentions')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='update attentions')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='delete attentions')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                        </tr>--}}
                        <tr>
                            <th>{{__('admin.deal_of_the_day')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show deal_of_the_day')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create deal_of_the_day')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update deal_of_the_day')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete deal_of_the_day')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>

                        <tr>
                            <th>{{__('admin.commissions')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show commissions')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create commissions')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update commissions')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete commissions')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>

                        <tr>
                            <th>{{__('admin.request')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show request')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update request')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.tracking_request')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show tracking')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>

                        <tr>
                            <th>{{__('admin.carriers')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show carriers')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create carriers')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update carriers')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete carriers')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.delivery_prices')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show delivery_prices')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create delivery_prices')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update delivery_prices')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete delivery_prices')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>

                        <tr>
                            <th>{{__('admin.providers')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show provider')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update provider')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.branches')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show branches')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update branches')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                        </tr>

                        <tr>
                            <th>{{__('admin.Suggestions_Complaints')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show suggestions')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.Evaluation')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show rates')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>


                        <tr>
                            <th>{{__('admin.notification')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show notifications')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create notifications')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update notifications')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete notifications')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.notifications_movement')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show notification_movement')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create notification_movement')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update notification_movement')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete notification_movement')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>

                        <tr>
                            <th>{{__('admin.coupon_type')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show coupon_type')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create coupon_type')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update coupon_type')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete coupon_type')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.coupons')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show coupons')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create coupons')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update coupons')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete coupons')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.volume_discount')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show volume_discounts')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update volume_discounts')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.permission')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show permission')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create permission')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update permission')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete permission')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.working_times_branch')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show working_times_branch')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create working_times_branch')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update working_times_branch')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete working_times_branch')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        <tr>
                            <th>{{__('admin.product')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show product')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create product')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update product')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete product')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
{{--                        <tr>--}}
{{--                            <th>{{__('admin.Special_requests')}}</th>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='show special_product')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>_______</td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='update special_product')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>_______</td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <th>{{__('admin.deferred')}}</th>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='show deferred')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>_______</td>--}}
{{--                            <td>_______</td>--}}
{{--                            <td>_______</td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <th>{{__('admin.prescription_insurance')}}</th>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='show prescription-replies')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>_______</td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='update prescription-replies')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>_______</td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <th>{{__('admin.prescription')}}</th>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='show prescription')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>_______</td>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='update prescription')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>_______</td>--}}
{{--                        </tr>--}}
                        <tr>
                            <th>{{__('admin.product_discounts')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show product_discounts')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create product_discounts')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update product_discounts')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                        </tr>
{{--                        <tr>--}}
{{--                            <th>{{__('admin.statistics')}}</th>--}}
{{--                            <td>--}}
{{--                                @for($i=0;$i<count($permission);$i++)--}}
{{--                                    @if($permission[$i]=='show statistics')--}}
{{--                                        <i class="ft-check"--}}
{{--                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>--}}
{{--                                    @endif--}}
{{--                                @endfor--}}
{{--                            </td>--}}
{{--                            <td>_______</td>--}}
{{--                            <td>_______</td>--}}
{{--                            <td>_______</td>--}}
{{--                        </tr>--}}
                        <tr>
                            <th>{{__('admin.user_report')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show users_report')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.products_report')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show products_report')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.requests_report')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show request_report')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.carrier_report')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show carriers_report')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.employees')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show admin')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create admin')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update admin')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.audit')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show audit')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>_______</td>
                            <td>_______</td>
                            <td>_______</td>
                        </tr>
                        <tr>
                            <th>{{__('admin.permission')}}</th>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='show permission')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='create permission')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='update permission')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($permission);$i++)
                                    @if($permission[$i]=='delete permission')
                                        <i class="ft-check"
                                           style="color: #18A367; font-weight: 600; font-size: 22px"></i>
                                    @endif
                                @endfor
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
