@php($page_title = __('admin.permission'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.employees_affairs')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('dashboard')}}">{{__('admin.Dashboard')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.permission')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="card">
            <div class="card-head">
                <div class="card-header">
                    @can('create permission')
                        <a href="{{ route('permission.create')}}" class="btn btn-primary"><i
                                class="ft-plus white"></i> {{__('admin.create').' '.__('admin.role')}} </a>
                    @endcan
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <table width="100%"
                               class="table table-white-space  display no-wrap icheck table-middle"
                               id="user_table">
                            <thead>
                            <tr>
                                <th hidden>{{__('admin.updated_at')}}</th>
                                <th>{{__('admin.roles')}}</th>
                                <th>{{__('admin.created_at')}}</th>
                                <th>{{__('admin.updated_at')}}</th>
                                <th width="25%">{{__('admin.action')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                </div>
            </div>
        </div>
    </div>
    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center" style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark" data-dismiss="modal">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- start model Don Delete Message--}}
    <div id="messageDonDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                        <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                        <strong id="title-delete">{{__('admin.successfully_done')}}!</strong>
                        <p id="message-delete">{{__('admin.deleted_successfully')}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--end model Don Delete Message--}}
@endsection
@section('script')
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    <!-- BEGIN: Page JS-->
    <script>
        /*start code show all data in datatable ajax*/
        $(document).ready(function () {
            $('#user_table').DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{  route('permission') }}",
                },
                columns: [
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    }
                ],
                'order': [[0, 'desc']],
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });
        });
        /*start message save*/

        /*start code Delete ajax*/
        var $carrier_id;
        $(document).on('click', '.deleteB', function () {
            console.log('click befo');
            $carrier_id = $(this).attr('id');
            $('#confirmModalDelete').modal('show');
            console.log($carrier_id);
        });
        $('#ok_button').click(function () {
            console.log("{{ url('/permission')}}" + "/destroy/" + $carrier_id);
            $.ajax({
                url: "{{ url('/admins_management/permission')}}" + "/destroy/" + $carrier_id,
                beforeSend: function () {
                    console.log('click before send'),
                        $('#ok_button').text('{{__('admin.deleting')}}...');
                },
                success: function (data) {
                    if (data.error_delete != undefined) {
                        $('#title-delete').text("{{__('admin.error_message')}}");
                        $('#message-delete').text(data.error_delete);
                        $('#messageDonDelete').modal('show');
                        setTimeout(function () {
                            $('#messageDonDelete').modal('hide');
                        }, 3000,);
                        $('#ok_button').text('{{__('admin.yes')}}');
                    } else {
                        setTimeout(function () {
                            $('#title-delete').text("{{__('admin.successfully_done')}}");
                            $('#message-delete').text("{{__('admin.deleted_successfully')}}.");
                            $('#confirmModalDelete').modal('hide');
                            $('#user_table').DataTable().ajax.reload();
                            $('#ok_button').text('{{__('admin.yes')}}');
                        }, 500);
                        $('#messageDonDelete').modal('hide');
                        setTimeout(function () {
                            $('#messageDonDelete').modal('show');
                        }, 510,);
                        setTimeout(function () {
                            $('#messageDonDelete').modal('hide');
                        }, 3000,);
                        $('#confirmModalShow').modal('hide');
                    }
                },
                error: function (data) {
                    console.log(data);
                    console.log('click failed')
                }
            })
        });
        /*end code Delete ajax*/

        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        /*end  message save*/
    </script>
    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
@endsection
