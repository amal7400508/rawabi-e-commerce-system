@php($page_title = __('admin.permission'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" href="{{asset('css/switchery.min.css')}}"/>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block"> {{__('admin.employees_affairs')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="{{ url('/admins_management/permission')}}">{{__('admin.permission')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.create')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            @if(session('success'))
                <div id="messageSave" class="modal fade text-left" role="dialog">
                    <div class="modal-dialog">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                    <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                    <strong>{{__('admin.successfully_done')}}!</strong>
                                    <p>{{session('success')}}.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <div class="card">
                    <form class="form" action="{{ route('permission.store')}}" method="POST" id="my_form_id"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <input class="form-control col-md-8 col-sm-8 @error('role') is-invalid @enderror"
                                   name="role" required
                                   placeholder="{{__('admin.enter_the_role_name')}}">
                            @error('role')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="heading-elements">
                                <ul class="">
                                    <fieldset>
                                        <div class="float-left">
                                            {{__('admin.select_all')}}
                                            <input type="checkbox" class="js-switch pull-right float-left select-all"/>
                                        </div>
                                    </fieldset>
                                    <div class="form-group mt-1">
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.Dashboard')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show dashboard"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.main_categories')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create main_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update main_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show main_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete main_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.sub_categories')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.working_times')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create working_times"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update working_times"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show working_times"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete working_times"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.working_times_branch')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create working_times_branch"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update working_times_branch"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show working_times_branch"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete working_times_branch"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.areas')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create areas"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update areas"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show areas"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete areas"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.unit_types')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create unit_types"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update unit_types"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show unit_types"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete unit_types"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.units')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create units"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update units"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show units"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete units"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.colors')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create color"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update color"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show color"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete color"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.deal_of_the_day_types')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create offer_types" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update offer_types" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show offer_types" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete offer_types" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.notes')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create notes"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update notes"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show notes"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete notes"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.branches_categories')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create pharmacy_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update pharmacy_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show pharmacy_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete pharmacy_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.commissions')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create commissions"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update commissions"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show commissions"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete commissions"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.main_category_categories')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create main_category_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show main_category_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete main_category_categories"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.branch_services')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create branch_services"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show branch_services"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete branch_services"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.social_medias')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create social_medias" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update social_medias" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show social_medias" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete social_medias" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.minimum_order')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create ceiling" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update ceiling" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show ceiling" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete ceiling" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.advertisement_types')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="create advertisement_types" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update advertisement_types" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show advertisement_types" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="delete advertisement_types" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.advertisements')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create advertisements" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update advertisements" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show advertisements" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete advertisements" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.material')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create material"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update material"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show material"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete material"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.paid_advertisements')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="create paid_advertisements" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update paid_advertisements" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show paid_advertisements" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="delete paid_advertisements" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.users')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update users" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show users" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.deposits')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create deposits"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show deposits"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update deposits"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.event_type')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create event_type"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update event_type"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show event_type"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete event_type"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.user_occasions')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show user_event" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.users_locations')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show user_addresses"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.product')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update products" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show products" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.product')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create product"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update product"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show product"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete product"
                                                               name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <div class="form-group padding-bottom">--}}
{{--                                                    <h4 class="text-bold-600 pull-left">{{__('admin.opening_balances')}}</h4>--}}
{{--                                                </div>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch event_type"--}}
{{--                                                               data-size="sm" value="create stocks"--}}
{{--                                                               name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch event_type"--}}
{{--                                                               data-size="sm" value="update stocks"--}}
{{--                                                               name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch event_type"--}}
{{--                                                               data-size="sm" value="show stocks"--}}
{{--                                                               name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch event_type"--}}
{{--                                                               data-size="sm" value="delete stocks"--}}
{{--                                                               name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.attentions')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="create attentions" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update attentions" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show attentions" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="delete attentions" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.deal_of_the_day')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create offer" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update offer" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show offer" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete offer" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.request')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update request" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show request" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.tracking_request')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show tracking" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.Special_requests')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update special_product" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show special_product" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.deferred_special_requests')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show deferred" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.prescription_insurance')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update prescription-replies" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show prescription-replies" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.prescription')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update prescription" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show prescription" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.carriers')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create carriers" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update carriers" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show carriers" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete carriers" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.delivery_prices')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create delivery_prices" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update delivery_prices" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show delivery_prices" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete delivery_prices" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.providers')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show provider" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update provider" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.branches')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show branches" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update provider" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.Suggestions_Complaints')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show suggestions" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.Evaluation')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show rates" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.notification')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create notifications" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update notifications" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show notifications" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete notifications" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.notifications_movement')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create notification_movement" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update notification_movement" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show notification_movement" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete notification_movement" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.coupon_type')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create coupon_type" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update coupon_type" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show coupon_type" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete coupon_type" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.coupons')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create coupons" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update coupons" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show coupons" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete coupons" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.volume_discount')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update volume_discounts" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show volume_discounts" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.product_discounts')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create product_discounts" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update product_discounts" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show product_discounts" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.accounts')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show volume_discounts" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.receipt')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="create receipt" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show receipt" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.statistics')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="switcherySize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show statistics" name="permission[]"/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.user_report')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show users_report" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.products_report')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show products_report" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.requests_report')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show request_report" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.carrier_report')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="switcherySize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show carriers_report" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.employees')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create admin" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update admin" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show admin" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.audit')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show audit" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.permission')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create permission" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update permission" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show permission" name="permission[]"/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete permission" name="permission[]"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="col-md-2 btn btn-primary pull-right" id="button_save">
                                        <i class="ft ft-save"></i> {{__('admin.save')}}
                                    </button>
                                    <a href="{{ url('/admins_management/permission')}}" class="col-md-1 btn btn-dark">
                                        <i class="ft-x"></i> {{__('admin.cancel')}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </section>
        <!-- // Form actions layout section end -->
    </div>
@endsection
@section('script')
    <script src="{{asset('js/switchery.min.js')}}"></script>
    <script>
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });
        var special = document.querySelectorAll('.js-switch')
            , specialButton = document.querySelector('.select-all');
        specialButton.addEventListener('change', function (e) {
            if (specialButton.checked) {
                special.forEach(function (html) {
                    html.checked = true;
                    onChange(html);
                });
            } else {
                special.forEach(function (html) {
                    html.checked = false;
                    onChange(html);
                });
            }
        });

        function onChange(el) {
            if (typeof Event === 'function' || !document.fireEvent) {
                var event = document.createEvent('HTMLEvents');
                event.initEvent('change', true, true);
                el.dispatchEvent(event);
            } else {
                el.fireEvent('onchange');
            }
        }
    </script>
    <script>
        $(document).on('click', '#button_save', function () {
            document.getElementById('my_form_id').submit();
            $('#button_save').attr('disabled', true);
        });
        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        /*end  message save*/
    </script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/checkbox-radio.js')}}"></script>
@endsection
