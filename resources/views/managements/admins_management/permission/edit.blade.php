@php($page_title = __('admin.permission'))
@extends('layouts.main')
<link rel="stylesheet" href="{{asset('css/switchery.min.css')}}"/>
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block"> {{__('admin.employees_affairs')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="{{ url('/admins_management/permission')}}">{{__('admin.permission')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.edit')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <form class="form" action="{{ route('permission.update',['id'=>$role->id])}}" method="POST"
                          id="my_form_id"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <input class="form-control col-md-8 col-sm-8 @error('role') is-invalid @enderror"
                                   value="{{$role->name}}" name="role" required
                                   placeholder="{{__('admin.enter_the_role_name')}}">
                            @error('role')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="heading-elements">
                                <ul class="">
                                    <fieldset>
                                        <div class="float-left">
                                            {{__('admin.select_all')}}
                                            <input type="checkbox" class="js-switch pull-right float-left select-all"/>
                                        </div>
                                    </fieldset>
                                    <div class="form-group mt-1">
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.Dashboard')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show dashboard" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show dashboard') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.main_categories')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create main_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create main_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update main_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update main_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show main_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show main_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete main_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete main_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.sub_categories')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete categories') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.working_times')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create working_times" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create working_times') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update working_times" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update working_times') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show working_times" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show working_times') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete working_times" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete working_times') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.working_times_branch')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create working_times_branch" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create working_times_branch') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update working_times_branch" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update working_times_branch') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show working_times_branch" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show working_times_branch') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete working_times_branch" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete working_times_branch') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.areas')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create areas" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create areas') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update areas" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update areas') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show areas" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show areas') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete areas" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete areas') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.unit_types')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create unit_types" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create unit_types') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update unit_types" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update unit_types') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show unit_types" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show unit_types') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete unit_types" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete unit_types') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.units')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create units" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create units') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update units" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update units') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show units" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show units') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete units" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete units') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.colors')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create color" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create color') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update color" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update color') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show color" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show color') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete color" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete color') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.deal_of_the_day_types')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create deal_of_the_day_types" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create deal_of_the_day_types') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update deal_of_the_day_types" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update deal_of_the_day_types') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show deal_of_the_day_types" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show deal_of_the_day_types') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete deal_of_the_day_types" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete deal_of_the_day_types') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.notes')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create notes" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create notes') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update notes" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update notes') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show notes" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show notes') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete notes" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete notes') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.branches_categories')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create pharmacy_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create pharmacy_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update pharmacy_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update pharmacy_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show pharmacy_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show pharmacy_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete pharmacy_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete pharmacy_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.commissions')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create commissions" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create commissions') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update commissions" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update commissions') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show commissions" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show commissions') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete commissions" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete commissions') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.main_category_categories')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create main_category_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create main_category_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show main_category_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show main_category_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete main_category_categories" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete main_category_categories') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.branch_services')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create branch_services" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create branch_services') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show branch_services" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show branch_services') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete branch_services" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete branch_services') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.social_medias')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create social_medias" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create social_medias') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update social_medias" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update social_medias') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show social_medias" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show social_medias') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete social_medias" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete social_medias') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.minimum_order')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create ceiling" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create ceiling') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update ceiling" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update ceiling') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show ceiling" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show ceiling') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete ceiling" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete ceiling') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.advertisement_types')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="create advertisement_types" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create advertisement_types') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update advertisement_types" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update advertisement_types') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show advertisement_types" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show advertisement_types') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="delete advertisement_types" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete advertisement_types') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.advertisements')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create advertisements" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create advertisements') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update advertisements" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update advertisements') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show advertisements" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show advertisements') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete advertisements" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete advertisements') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.material')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create material" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create material') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update material" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update material') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show material" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show material') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete material" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete material') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.paid_advertisements')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="create paid_advertisements" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create paid_advertisements') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update paid_advertisements" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update paid_advertisements') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show paid_advertisements" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show paid_advertisements') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="delete paid_advertisements" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete paid_advertisements') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

{{--                                            </div>--}}
{{--                                        </div>--}}

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.users')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update users" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update users') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show users" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show users') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.deposits')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create deposits" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create deposits') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update deposits" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update deposits') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show deposits" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show deposits') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <div class="form-group padding-bottom">
                                                    <h4 class="text-bold-600 pull-left">{{__('admin.event_type')}}</h4>
                                                </div>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="create event_type"
                                                               name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create event_type') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="update event_type"
                                                               name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update event_type') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="show event_type"
                                                               name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show event_type') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch event_type"
                                                               data-size="sm" value="delete event_type"
                                                               name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete event_type') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.user_occasions')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show user_event" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show user_event') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.users_locations')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show user_addresses" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show user_addresses') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.product')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create product" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create product') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update product" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update product') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show product" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show product') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete product" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete product') checked @endif @endfor/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.products')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update products" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update products') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show products" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show products') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.opening_balances')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="create stocks" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create stocks') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update stocks" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update stocks') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show stocks" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show stocks') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="delete stocks" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete stocks') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.attentions')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="create attentions" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create attentions') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update attentions" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update attentions') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show attentions" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show attentions') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="delete attentions" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete attentions') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.deal_of_the_day')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create deal_of_the_day" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create deal_of_the_day') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update deal_of_the_day" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update deal_of_the_day') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show deal_of_the_day" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show deal_of_the_day') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete deal_of_the_day" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete deal_of_the_day') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.request')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update request" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update request') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show request" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show request') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.tracking_request')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show tracking" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show tracking') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.Special_requests')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update special_product" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update special_product') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show special_product" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show special_product') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.deferred_special_requests')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show deferred" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show deferred') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.prescription_insurance')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update prescription-replies" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update prescription-replies') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show prescription-replies" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show prescription-replies') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.prescription')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update prescription" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update prescription') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show prescription" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show prescription') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.carriers')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create carriers" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create carriers') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update carriers" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update carriers') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show carriers" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show carriers') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete carriers" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete carriers') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.delivery_prices')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create delivery_prices" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create delivery_prices') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update delivery_prices" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update delivery_prices') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show delivery_prices" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show delivery_prices') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete delivery_prices" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete delivery_prices') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.providers')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="update provider" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update provider') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show provider" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show provider') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.branches')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update branches" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update branches') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show branches" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show branches') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.Suggestions_Complaints')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show suggestions" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show suggestions') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.Evaluation')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show rates" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show suggestions') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.notification')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create notifications" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create notifications') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update notifications" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update notifications') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show notifications" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show notifications') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete notifications" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete notifications') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.notifications_movement')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create notification_movement" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create notification_movement') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update notification_movement" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update notification_movement') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show notification_movement" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show notification_movement') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete notification_movement" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete notification_movement') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.coupons_types')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create coupon_type" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create coupon_type') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update coupon_type" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update coupon_type') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show coupon_type" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show coupon_type') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete coupon_type" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete coupon_type') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.coupons')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create coupons" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create coupons') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update coupons" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update coupons') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show coupons" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show coupons') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete coupons" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete coupons') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.volume_discount')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update volume_discounts" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update volume_discounts') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show volume_discounts" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show volume_discounts') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.product_discounts')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create product_discounts" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create product_discounts') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update product_discounts" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update product_discounts') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show product_discounts" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show product_discounts') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.accounts')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show accounts" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show accounts') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.receipt')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="create receipt" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show receipt') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show receipt" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show receipt') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                                            <div class="div-style-group">--}}
{{--                                                <h4 class="text-bold-600">{{__('admin.statistics')}}</h4>--}}
{{--                                                <hr>--}}
{{--                                                <div class="form-group padding-10">--}}
{{--                                                    <div class="pull-left">--}}
{{--                                                        <label for="js-switchSize12"--}}
{{--                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="pull-right">--}}
{{--                                                        <input type="checkbox" class="js-switch" data-size="sm"--}}
{{--                                                               value="show statistics" name="permission[]"--}}
{{--                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show statistics') checked @endif @endfor/>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.user_report')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show users_report" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show users_report') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.products_report')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show products_report" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show products_report') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.requests_report')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show request_report" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show request_report') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.carrier_report')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show carriers_report" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show carriers_report') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.employees')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create admin" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create admin') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update admin" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update admin') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show admin" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show admin') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.audit')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show audit" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show audit') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-12">
                                            <div class="div-style-group">
                                                <h4 class="text-bold-600">{{__('admin.permission')}}</h4>
                                                <hr>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.create')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="create permission" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='create permission') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.edit')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="update permission" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='update permission') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.show')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="show permission" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='show permission') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                                <div class="form-group padding-10">
                                                    <div class="pull-left">
                                                        <label for="js-switchSize12"
                                                               class="font-medium-2 text-bold-500 mr-1">{{__('admin.delete')}}</label>
                                                    </div>
                                                    <div class="pull-right">
                                                        <input type="checkbox" class="js-switch" data-size="sm"
                                                               value="delete permission" name="permission[]"
                                                               @for($i=0;$i<count($permission);$i++)@if($permission[$i]=='delete permission') checked @endif @endfor/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="col-md-2 btn btn-primary pull-right" id="button_save">
                                        <i class="ft ft-save"></i> {{__('admin.edit')}}
                                    </button>
                                    <a href="{{ url('/admins_management/permission')}}" class="col-md-1 btn btn-dark">
                                        <i class="ft-x"></i> {{__('admin.cancel')}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </section>
        <!-- // Form actions layout section end -->
    </div>
@endsection
@section('script')
    <script src="{{asset('js/switchery.min.js')}}"></script>
    <script>
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html, {size: 'small'});
        });
        var special = document.querySelectorAll('.js-switch')
            , specialButton = document.querySelector('.select-all');
        specialButton.addEventListener('change', function (e) {
            if (specialButton.checked) {
                special.forEach(function (html) {
                    html.checked = true;
                    onChange(html);
                });
            } else {
                special.forEach(function (html) {
                    html.checked = false;
                    onChange(html);
                });
            }
        });

        function onChange(el) {
            if (typeof Event === 'function' || !document.fireEvent) {
                var event = document.createEvent('HTMLEvents');
                event.initEvent('change', true, true);
                el.dispatchEvent(event);
            } else {
                el.fireEvent('onchange');
            }
        }
    </script>
    <script>
        $(document).on('click', '#button_save', function () {
            document.getElementById('my_form_id').submit();
            $('#button_save').attr('disabled', true);
        });
        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);

        /*end  message save*/
    </script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/checkbox-radio.js')}}"></script>
@endsection
