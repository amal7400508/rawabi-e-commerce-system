@php($page_title = __('admin.notes'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.encodings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.notes')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create notes')
                                            @if($count == 0)
                                                <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.note')}}</a>
                                            @endif
                                        @endcan
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('notes')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.message')}}</th>
                                                        <th>{{__('admin.start_date')}}</th>
                                                        <th>{{__('admin.end_date')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        @canany(['update notes', 'delete notes'])
                                                            <th>{{__('admin.action')}}</th>
                                                        @endcanany
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $note)
                                                        <tr style="{{$note->background_color_row}}">
                                                            <td hidden>{{$note->updated_at}}</td>
                                                            <td class="font-default">{{$note->id}}</td>
                                                            <td>{{$note->message}}</td>
                                                            <td>{{$note->start_date_carbon}}</td>
                                                            <td>{{$note->end_date_carbon}}</td>
                                                            <td>
                                                                @php($status = $note->status)
                                                                @if($status == 1)
                                                                    <div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>
                                                                @else
                                                                    <div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>
                                                                @endif
                                                            </td>
                                                            @canany(['update notes', 'delete notes'])
                                                                <td>
                                                                    {!! $note->actions !!}
                                                                </td>
                                                            @endcanany
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.note')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements" id="check_show">
                                <input type="checkbox" name="status" value="1"
                                       class="switchBootstrap" id="switchBootstrap18"
                                       data-on-color="primary" data-off-color="danger"
                                       data-on-text="{{__('admin.enable')}}"
                                       data-off-text="{{__('admin.disable')}}" data-color="primary"
                                       data-label-text="{{__('admin.status')}}"
                                       checked/>
                            </div>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="message_ar">{{__('admin.message')}} (ar)<span class="danger">*</span></label>
                                                        <textarea id="message_ar" class="form-control" name="message_ar" rows="4" required></textarea>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="message_en">{{__('admin.message')}} (en)<span class="danger">*</span></label>
                                                        <textarea id="message_en" class="form-control" name="message_en" rows="4" required></textarea>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="start_date">{{__('admin.start_date')}}<span class="danger">*</span></label>
                                                        <input type="date" id="start_date"
                                                               class="form-control"
                                                               name="start_date"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="end_date">{{__('admin.end_date')}}<span class="danger">*</span></label>
                                                        <input type="date" id="end_date"
                                                               class="form-control"
                                                               name="end_date"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>


@endsection
@section('script')
    @include('managements.encoding.notes.js')
@endsection
