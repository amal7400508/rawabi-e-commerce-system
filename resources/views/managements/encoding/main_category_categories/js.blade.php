<script>
    $(document).on('change', '#main_category', function () {
        reloadTable($(this).val());
    });

    function reloadTable(category_id) {
        let table_body = $('#table tbody');
        $.ajax({
            type: 'GET',
            url: "{{url('encoding/main_category_categories/get-category')}}" + "/" + category_id,
            dataType: "json",
            beforeSend: function () {
                return table_body.html(
                    '<tr class="text-center"><td colspan="10">{{__('admin.loading')}} <i class="la la-spinner spinner"></i></td></tr>'
                );
            },
            success: function (data) {
                if (data.data.length === 0) {
                    return table_body.html(
                        '<tr class="text-center"><td colspan="10">{{__('admin.no_data')}}</td></tr>'
                    );
                }
                table_body.empty();
                for (let i = 0; i < data.data.length; i++) {
                    table_body.append(
                        '<tr>' +
                        '<td>' + data.data[i].main_category_name + '</td>' +
                        '<td>' + data.data[i].sub_category_name + '</td>' +
                        @can('delete main_category_categories')
                            '<td><a class="action-item delete-row" id="' + data.data[i].id + '"><i class="la la-trash color-red"></i></a></td>' +
                        @endcan
                            '</tr>'
                    );
                }
            },
            error: function (data) {
                return table_body.html(
                    '<tr class="text-center"><td colspan="10">{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i></td></tr>'
                );
            }
        });
    }

    @can('create main_category_categories')
    $('#form').on('submit', function (event) {
        event.preventDefault();
        let btn = $('#btn-save');
        let formData = new FormData(this);

        $.ajax({
            url: "{{route('main_category_categories.store')}}",
            method: "POST",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            type: 'post',
            beforeSend: function () {
                $('#form').find('span strong').empty();
                $('#form select').removeClass('is-invalid');
                btn.html('<i class="la la-spinner spinner"></i><span> {{__('admin.adding')}}...</span>');
            },
            success: function (data) {
                reloadTable(data.category_id);

                btn.text('{{__('admin.add')}}');
                $('#message-modal p').text(data.success + '.');
                $('#message-modal').modal('show');
                setTimeout(function () {
                    $('#message-modal').modal('hide');
                }, 3000);
            },
            error: function (data) {
                btn.text('{{__('admin.add')}}');
                let obj = JSON.parse((data.responseText));
                let main_category = obj.error.main_category;
                let sub_category = obj.error.sub_category;

                if (main_category !== undefined || sub_category !== undefined) {
                    if (main_category !== undefined) {
                        $('#main_category').addClass('is-invalid').parent().find('span strong').text(main_category)
                    }

                    if (sub_category !== undefined) {
                        $('#sub_category').addClass('is-invalid').parent().find('span strong').text(sub_category)
                    }
                } else {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            }
        });
    });
    @endcan

    @can('delete main_category_categories')
    /*start code Delete ajax*/
    $(document).on('click', '.delete-row', async function () {

        let id_row = $(this).attr('id');
        let route = "{{url('encoding/main_category_categories/destroy')}}" + "/" + id_row;
        try {
            let data = await deletedItems(id_row, route);
            reloadTable(data.category_id);
        } catch (e) {
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan
</script>
