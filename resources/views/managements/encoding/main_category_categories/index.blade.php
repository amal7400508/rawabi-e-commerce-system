@php($page_title = __('admin.main_category_categories'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.encodings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.main_category_categories')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>{{__('admin.index')}} {{__('admin.main_category_categories')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('main_category_categories')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="form-body">
                                                <form id="form">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <label for="main_category">{{__('admin.main_categories')}}</label>
                                                                <select required class="select2 form-control" id="main_category" name="main_category">
                                                                    <option value="">{{__('admin.select_option')}}</option>
                                                                    @foreach($main_categories as $main_category)
                                                                        <option value="{{$main_category->id}}">{{$main_category->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="error-message">
                                                                     <strong></strong>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <label for="sub_category">{{__('admin.sub_categories')}}</label>
                                                                <select required class="select2 form-control" id="sub_category" name="sub_category">
                                                                    <option value="">{{__('admin.select_option')}}</option>
                                                                    @foreach($sub_categories as $sub_category)
                                                                        <option value="{{$sub_category->id}}">{{$sub_category->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="error-message">
                                                                    <strong></strong>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        @can('create main_category_categories')
                                                            <div class="col-md-2">
                                                                <button type="submit" id="btn-save" class="btn btn-primary btn-save-ctg- col-md-12">{{__('admin.add')}}</button>
                                                            </div>
                                                        @endcan
                                                    </div>
                                                </form>
                                            </div>

                                            <br>
                                            <!-- Invoices List table -->
                                            <div class="table-responsive">
                                                <table width="100%"
                                                       class="table table-striped table-responsive table-custom-responsive"
                                                       id="table">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('admin.main_categories')}}</th>
                                                        <th>{{__('admin.sub_categories')}}</th>
                                                        @can('delete main_category_categories')
                                                            <th>{{__('admin.action')}}</th>
                                                        @endcan
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="text-center">
                                                        <td colspan="10">{{__('admin.no_data')}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @include('managements.encoding.main_category_categories.js')
@endsection
