@php($page_title = __('admin.sub_categories'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.encodings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.sub_categories')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create categories')
                                            <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.category')}}</a>
                                        @endcan
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('categories')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number"
                                                                            @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="name"
                                                                            @if($search_type == 'name') selected @endif>{{__('admin.name')}}</option>
                                                                    <option value="details"
                                                                            @if($search_type == 'details') selected @endif>{{__('admin.details')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th>{{__('admin.details')}}</th>
                                                        <th>{{__('admin.level')}}</th>
                                                        <th>{{__('admin.main_categories')}}</th>
                                                        <th style="width: 10px">{{__('admin.status')}}</th>
                                                        <th>{{__('admin.image')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $category)
                                                        <tr style="{{$category->background_color_row}}">
                                                            <td hidden>{{$category->updated_at}}</td>
                                                            <td class="font-default">{{$category->id}}</td>
                                                            <td>{{$category->name}}</td>
                                                            <td>{{$category->details}}</td>
                                                            <td>
                                                                <button id="{{$category->id}}" data-no="{{$category->level}}" @can('update provider') title="Double click to change" @else disabled @endcan
                                                                class="btn btn-sm btn-primary font-default change-level">
                                                                    {{$category->level}}
                                                                </button>
                                                            </td>
                                                            <td>{{$category->Main->name}}</td>
                                                            <td>
                                                                @php($status = $category->status)
                                                                @if($status == 1)
                                                                    <div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>
                                                                @else
                                                                    <div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($category->image != null)
                                                                    <img class="img-index-40" src="{{$category->image_path_64}}"
                                                                         onerror=this.src="{{asset('storage')}}/logo_orginal_icon.png"
                                                                    />
                                                                @else
                                                                    <img class="img-index-30" src="{{asset('storage')}}/logo_orginal_icon.png"/>
                                                                @endif
                                                            </td>

                                                            <td>
                                                                {!! $category->actions !!}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.categories')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements" id="check_show">
                                <input type="checkbox" name="status" value="1"
                                       class="switchBootstrap" id="switchBootstrap18"
                                       data-on-color="primary" data-off-color="danger"
                                       data-on-text="{{__('admin.enable')}}"
                                       data-off-text="{{__('admin.disable')}}" data-color="primary"
                                       data-label-text="{{__('admin.status')}}"
                                       checked/>
                            </div>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6 p-0">
                                            <row>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="main_categories">{{__('admin.main_categories')}}<span class="danger">*</span></label>
                                                        <select class="form-control" id="main_categories" required name="main_categories">
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($mainCategory as $main)
                                                                <option value="{{$main->id}}">{{$main->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="name_ar">{{__('admin.name')}} (ar)<span class="danger">*</span></label>
                                                        <input type="text" id="name_ar"
                                                               class="form-control"
                                                               name="name_ar"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="name_en">{{__('admin.name')}} (en)<span class="danger">*</span></label>
                                                        <input type="text" id="name_en"
                                                               class="form-control"
                                                               name="name_en"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="level">{{__('admin.level')}}<span class="danger">*</span></label>
                                                        <input type="number" id="level"
                                                               class="form-control"
                                                               name="level"
                                                               value="100"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>

                                            </row>
                                        </div>
                                        <div class="col-md-6 p-0">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="details_ar">{{__('admin.detail')}}(ar)</label>
                                                    <textarea id="details_ar" name="details_ar" rows="4" class="form-control">
                                                    </textarea>
                                                    <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="details_en">{{__('admin.detail')}}(en)</label>
                                                    <textarea id="details_en" name="details_en" rows="4" class="form-control">
                                                    </textarea>
                                                    <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="edit_image_ar"
                                                           id="label-image">{{__('admin.image')}} <span
                                                            class="danger">*</span></label>
                                                    <input type="file" accept=".jpg, .jpeg, .png"
                                                           id="edit_image_ar"
                                                           class="form-control"
                                                           name="image" onchange="loadAvatar(this);"
                                                           required
                                                    >
                                                    <button type="button" hidden onclick="delete_img()"
                                                            style="position: static; "
                                                            class="btn btn-sm btn-outline-danger"
                                                            id="delete-img"><i class="ft-trash"></i>
                                                    </button>

                                                    <img id="avatar" style="max-width: 140px; height: auto; margin:10px;">
                                                    <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    @include('managements.encoding.categories.show')
@endsection
@section('script')
    @include('managements.encoding.categories.js')
    @include('managements.encoding.categories.show_js')
@endsection
