<script>

    let table_show = $('#table-show');
    let table_count = $('#table-count');

    function delete_img() {
        document.getElementById('edit_image_ar').value = null;
        $('#avatar').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
    }

    function loadAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                var image = document.getElementById('avatar');
                image.src = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("select[name=table_length]").click(function () {
        let open = $(this).data("is_open");
        if (open) {
            window.location.href = $(this).val()
        }
        $(this).data("is_open", !open);
    });

    @can('create categories')
    $(document).on('click', '#openAddModal', function () {
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);
        delete_img();
        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('categories.store')}}")
            .attr("data_type", "add");
        $('input[name=level]').val("100");
    });
    @endcan

    /*start code edit*/
    @can('update categories')
    let level;
    $(document).on('dblclick', '.change-level', async function () {
        level = $(this);
        level.attr('style', '');
        let level_no = level.attr('data-no');
        level.html('<input type="number" class="change-value" value="' + level_no + '">');
    });

    $(document).on('keypress', '.change-value', function (e) {
        let level_value = $(this).val();
        let id = level.attr('id');
        let route = "{{url('encoding/categories/change-level')}}" + "/" + id;
        if (e.which == 13) {
            $(this).attr('disabled', true);
            $.ajax({
                type: 'POST',
                data: {
                    _token: $('input[name ="_token"]').val(),
                    level: level_value,
                },
                url: route,
                dataType: "json",
                success: function (data) {
                    level.attr('style', '');
                    level.text(level_value).attr('id', level_value);
                    level.attr('data-no', level_value)
                },
                error: function (data) {
                    level.attr('style', 'background: #c30015 !important');
                }
            })
        }
    });

    let category_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        category_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('encoding/categories/edit')}}" + '/' + category_id;
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#avatar').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('encoding/categories/update')}}" + '/' + category_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);

            $('#edit_image_ar').attr('required', false);
            $('#label-image').text("{{__('admin.image')}}");
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            document.getElementById('name_ar').value = data.name_ar;
            document.getElementById('name_en').value = data.name_en;
            document.getElementById('details_ar').value = data.details_ar;
            document.getElementById('details_en').value = data.details_en;
            document.getElementById('main_categories').value = data.main_categories;
            $('input[name=level]').val(data.level);
            $('#avatar').attr('src', data.image).attr('hidden', false);
            $('#delete-img').attr('hidden', false);

            if (data.status == 1) {
                $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                    .bootstrapSwitch('state', true);
            } else {
                $('#switchBootstrap18').attr('value', 1).attr('checked', false)
                    .bootstrapSwitch('state', false);
            }

            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    @endcan
    /*end code edit*/

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('category_id', category_id);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                /*let response_data = JSON.parse((data.responseText)).category;*/
                let response_data = data.category;

                $('#addModal').modal('hide');

                let status, tr_color_red = '';
                if (response_data.status == 1) {
                    status = "<div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>";
                } else {
                    tr_color_red = 'tr-color-red';
                    status = "<div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>";
                }
                let details = response_data.details == null ? '' : response_data.details;

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td class='font-default'>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.name + "</td>");
                let col3 = $("<td>" + details + "</td>");
                let col4 = $("<td> <button  class='btn btn-sm btn-primary font-default change-level' </button>" + response_data.level + "</td>");
                let col5 = $("<td>" + response_data.category_name + "</td>");
                let col6 = $("<td>" + status + "</td>");
                let col7 = $('<td><img class="img-index-40" src="' + response_data.image_path_64 + '"' +
                    'onerror=this.src="{{asset('storage')}}/logo_orginal_icon.png"/></td>'
                );
                let col8 = $("<td>" + response_data.actions + "</td>");

                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5, col6, col7,col8).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5, col6, col7,col8);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.name_ar !== undefined) {
                $('#form input[name=name_ar]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.name_ar);
            }
            if (obj.name_en !== undefined) {
                $('#form input[name=name_en]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.name_en);
            }
            if (obj.image !== undefined) {
                $('#form input[name=image]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.image);
            }
            if (obj.level !== undefined) {
                $('#form input[name=level]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.level);
            }
        }
    });
    /*end code add or update*/

    @can('delete categories')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('encoding/categories/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            this_row.remove();
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    @endcan
    /*end code Delete ajax*/

</script>
