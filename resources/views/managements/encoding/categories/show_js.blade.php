<script>
    /*start code show details ajax*/
    let category_details_id;
    $(document).on('click', '.show-detail-category', async function () {
        category_details_id = $(this).attr('id');
        let url = "{{url('encoding/categories/show')}}" + '/' + category_details_id;

        try {
            let data = await responseEditOrShowData(url);

            if (data.status == 1) {
                $('#show_status').html('<i class="ft-unlock color-primary"></i>');
            } else {
                $('#show_status').html('<i class="ft-lock color-red"></i>');
            }

            $('.form-details .name').text(data.name);
            $('.form-details .id').text(data.id);
            $('.form-details .details').text(data.details);
            $('.form-details .image').text(data.image);
            $('#created').text(data.created_at);
            $('#updated_at').html(data.updated_at);
            $('#ModalShowCategory').modal('show');
            $('#show-image').attr('src', data.image);
            $('#confirm-modal-loading-show').modal('hide');
        } catch (error) {
            return error;
        }
    });
    /*end code show details ajax*/
</script>
