<script>
    function delete_img() {
        document.getElementById('edit_image_ar').value = null;
        $('#avatar').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
    }

    function loadAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                var image = document.getElementById('avatar');
                image.src = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function () {
        treeBuilding();

        function treeBuilding() {
            $.ajax({
                type: 'GET',
                url: "{{route('encoding.main-categories.treeBuilding')}}",
                dataType: "json",

                success: function (data) {
                    var i, j;
                    $('#min-tree').html(
                        '<ul>' +
                        '<li>' +
                        '<a href="javascript:void(0);"><div class="member-view-box">' +
                        '<button type="button" class="btn category"><span class="span span-category">{{__('admin.categories')}}</span><img class="img-category" src="{{asset('storage/logo_orginal_icon.png')}}"></button>' +
                        @can('create main_categories')

                            '<button type="button" class="add-category btn btm-sm btn-success add-sub-row" title="اضافة تصنيف تحت هذا التصنيف"' +
                        'data-ranks="0" data-id="0" hidden><i class="ft-plus"></i></button>' +
                        @endcan

                            '<br>' +
                        '<button type="button" class="plus btn">-</button>' +
                        '</div>' +
                        '</a>' +
                        '<ul class="active" id="null"></ul>' +
                        '</li>' +
                        '</ul>'
                    );
                    for (i = 0; i < data.category.length; i++) {
                        for (j = 0; j < data.category[i].length; j++) {
                            data.category[i][j]['image'] = data.category[i][j]['image'] === null ?
                                "{{asset('storage/icon.png')}}" :
                                "{{asset('storage/main-categories')}}" + "/" + data.category[i][j]['image'];
                            if (data.category[i][j]['category_id'] === null) data.category[i][j]['category_id'] = 'null';
                            btn_delete = '';
                            @can('delete main_categories')
                                btn_delete = '<button type="button" class="btn btm-sm btn-danger delete-sub-row" data-id="' + data.category[i][j]['id'] + '" title="حذف" hidden><i class="ft-trash-2"></i></button>';
                            @endcan

                                tree = '<li>' +
                                '<a href="javascript:void(0);">' +
                                '<div class="member-view-box">';

                            @can('update main_categories')
                                tree += '<button type="button" class="btn btm-sm btn-blue edit-sub-row" data-id="' + data.category[i][j]['id'] + '" title="{{__('admin.edit')}} " hidden><i class="ft-edit"></i></button>';
                            @endcan
                                tree += '<button type="button" class="btn category"><span class="span span-category">' + data.category[i][j]['name'] + '</span><img class="img-category" src="' + data.category[i][j]['image'] + '" onerror=this.src="{{ url('/storage/icon.png')}}"></button>';
                            if (data.category[i][j]['category_type'] === 'رئيسي') {
                            }
                            @can('create main_categories')
                            if (data.category[i][j]['branches_count'] === 0) {
                                tree += '<button type="button" class="add-category btn btm-sm btn-success add-sub-row" title="اضافة تصنيف تحت هذا التصنيف" data-ranks="' + data.category[i][j]['ranks'] + '" data-id="' + data.category[i][j]['id'] + '" hidden><i class="ft-plus"></i></button>';
                            }
                            @endcan
                            /*else {
                                if (data.category[i][j]['is_leaf'] === false) tree += btn_delete;
                            }*/

                            if (data.category[i][j]['is_leaf'] === true) tree += '<br><button type="button" class="plus btn">-</button>';
                            else {
                                if (data.category[i][j]['branches_count'] === 0) tree += '<br>' + btn_delete;
                            }

                            tree += '</div></a>';
                            if (data.category[i][j]['is_leaf'] === true) tree += '<ul class="active" id="' + data.category[i][j]['id'] + '"></ul>';

                            tree += '</li>';

                            $('#' + data.category[i][j]['main_category_id']).append(tree);
                        }
                    }
                    eventTree();
                    $('#save').text('إضافة');
                    $('#edit').text('{{__('admin.edit')}}');

                },
            });
        }

        let data_id, data_ranks;

        function eventTree() {
            $('.category').on('click', function (e) {
                e.preventDefault();
                if ($(this).hasClass('btn-hide')) {
                    $(this).removeClass('btn-hide');
                    $(this).parent().find('.edit-sub-row').attr('hidden', true);
                    $(this).parent().find('.delete-sub-row').attr('hidden', true);
                    $(this).parent().find('.add-sub-row').attr('hidden', true);
                    $(this).removeClass('active');
                } else {
                    $(this).addClass('btn-hide');
                    $(this).parent().find('.edit-sub-row').attr('hidden', false);
                    $(this).parent().find('.delete-sub-row').attr('hidden', false);
                    $(this).parent().find('.add-sub-row').attr('hidden', false);
                    $(this).addClass('active');
                }
            });

            $(function () {
                $('.genealogy-tree ul').hide();
                $('.genealogy-tree>ul').show();
                $('.genealogy-tree ul.active').show();
                $('.genealogy-tree .plus').on('click', function (e) {
                    var children = $(this).parent().parent().parent().find('> ul');
                    if (children.is(":visible")) {
                        children.hide('fast').removeClass('active');
                        $(this).text('+');
                    } else {
                        children.show('fast').addClass('active');
                        $(this).text('-');
                    }
                    e.stopPropagation();
                });
            });

            $('.add-category').on('click', function () {
                inputEmpty();
                $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                    .bootstrapSwitch('state', true);
                $('form#form-category span.danger').val('');
                $('#save').attr('hidden', false);
                $('#edit').attr('hidden', true);
                data_id = $(this).attr('data-id');
                data_ranks = $(this).attr('data-ranks');
                $('input[name=level]').val('100');

                /*$('#message-error-category').text('');
                $('#message-error-image').text('');
                $('#message-error-type').text('');
                document.getElementById('category-type').value = '';*/

                $('#addModal').modal('show');
            });
        }

        function inputEmpty() {
            delete_img();
            $('form#form-category input:not(:first)').removeClass('is-invalid').val('');
            $('#form-category span strong').empty();
        }

        let items_id;

        @can('update main_categories')
        $(document).on('click', '.edit-sub-row', function () {
            inputEmpty();
            /*$('#type-group').attr('hidden', true);*/
            items_id = $(this).attr('data-id');
            $.ajax({
                type: 'GET',
                url: "{{url('encoding/main-categories/edit')}}" + "/" + items_id,
                dataType: "json",
                success: function (data) {
                    if (data.status == 1) {
                        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                            .bootstrapSwitch('state', true);
                    } else {
                        $('#switchBootstrap18').attr('value', 1).attr('checked', false)
                            .bootstrapSwitch('state', false);
                    }

                    $('#save').attr('hidden', true);
                    $('#edit').attr('hidden', false);
                    $('#avatar').attr('hidden', false);
                    $('#avatar').attr('src', data.image);
                    $('#delete-img').attr('hidden', false);
                    $('input[name=category_name_ar]').val(data.name_ar);
                    $('input[name=category_name_en]').val(data.name_en);
                    $('input[name=level]').val(data.level);
                    $('#addModal').modal('show');
                },
                error: function () {
                }
            });
        });
        @endcan

        @can('create main_categories')
        $('#form-category').on('submit', function (event) {
            event.preventDefault();
            let route = "{{ route('encoding.main-categories.store')}}";
            let formData = new FormData(this);
            formData.append('main_category_id', data_id);
            formData.append('ranks', data_ranks);
            saveAndUpdate(formData, route, 'save');
        });
        @endcan

        @can('update main_categories')
        $('#edit').click(function (event) {
            event.preventDefault();
            let route = "{{route('encoding.main-categories.update')}}";
            let formData = new FormData($('#form-category')[0]);
            formData.append('id', items_id);
            formData.append('ranks', 1);
            saveAndUpdate(formData, route, 'edit');
        });
        @endcan

        function saveAndUpdate(data, route, _type) {
            let type;
            if (_type === 'save') type = 'إضافة';
            else if (_type === 'edit') type = '{{__('admin.edit')}}';
            $.ajax({
                url: route,
                method: "POST",
                data: data,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $('#' + _type).html('<i class="la la-spinner spinner"></i><span>جاري ال' + type + '...</span>');
                    $('#form-category').find('input').removeClass('is-invalid');
                    $('#form-category span strong').empty();
                },
                success: function (data) {
                    treeBuilding();
                    $('#addModal').modal('hide');
                    swal({
                        icon: "success",
                        text: data.message + "!",
                        title: "تم ال" + type + " !",
                        button: 'موافق'
                    });
                },
                error: function (data) {
                    $('#' + _type + '').text(type);
                    let obj = JSON.parse((data.responseText));

                    if (obj.error.category_name_ar !== undefined) {
                        $('#form-category input[name=category_name_ar]').addClass('is-invalid')
                            .parent().find('span strong').text(obj.error.category_name_ar);
                    }
                    if (obj.error.category_name_en !== undefined) {
                        $('#form-category input[name=category_name_en]').addClass('is-invalid')
                            .parent().find('span strong').text(obj.error.category_name_en);
                    }
                    if (obj.error.image !== undefined) {
                        $('#form-category input[name=image]').addClass('is-invalid').parent()
                            .find('span strong').text(obj.error.image);
                    }
                    if (obj.error.level !== undefined) {
                        $('#form-category input[name=level]').addClass('is-invalid').parent()
                            .find('span strong').text(obj.error.level);
                    }
                    /*$('#message-error-image').text(image_error);*/
                }

            });
        }

        @can('delete main_categories')
        $(document).on('click', '.delete-sub-row', async function () {
            items_id = $(this).attr('data-id');
            try {
                await deletedItems(items_id, "{{route('encoding.main-categories.destroy')}}");
                treeBuilding();
            } catch (e) {
                return e;
            }
        });
        @endcan

    });

</script>
