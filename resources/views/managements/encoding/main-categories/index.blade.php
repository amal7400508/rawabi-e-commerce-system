@php($page_title = __('admin.main_categories'))
@extends('layouts.main')

@section('content')
    <style>

        /*----------------genealogy-scroll----------*/

        /*.genealogy-scroll::-webkit-scrollbar {
            width: 5px;
            height: 8px;
        }

        .genealogy-scroll::-webkit-scrollbar-track {
            border-radius: 10px;
            background-color: #e4e4e4;
        }

        .genealogy-scroll::-webkit-scrollbar-thumb {
            background: #231f20;
            border-radius: 10px;
            transition: 0.5s;
        }

        .genealogy-scroll::-webkit-scrollbar-thumb:hover {
            background: #214679;
            transition: 0.5s;
        }
*/

        /*----------------genealogy-tree----------*/
        .genealogy-body {
            white-space: nowrap;
            overflow-y: hidden;
            padding: 50px;
            min-height: 500px;
            padding-top: 10px;
        }

        .genealogy-tree ul {
            padding-top: 20px;
            position: relative;
            padding-left: 0px;
            display: flex;
        }

        .genealogy-tree li {
            float: left;
            text-align: center;
            list-style-type: none;
            position: relative;
            padding: 20px 5px 0 5px;
        }

        .genealogy-tree li::before, .genealogy-tree li::after {
            content: '';
            position: absolute;
            top: 0;
            right: 50%;
            border-top: 2px solid #ccc;
            width: 50%;
            height: 18px;
        }

        .genealogy-tree li::after {
            right: auto;
            left: 50%;
            border-left: 2px solid #ccc;
        }

        .genealogy-tree li:only-child::after, .genealogy-tree li:only-child::before {
            display: none;
        }

        .genealogy-tree li:only-child {
            padding-top: 0;
        }

        .genealogy-tree li:first-child::before, .genealogy-tree li:last-child::after {
            border: 0 none;
        }

        .genealogy-tree li:last-child::before {
            border-right: 2px solid #ccc;
            border-radius: 0 5px 0 0;
            -webkit-border-radius: 0 5px 0 0;
            -moz-border-radius: 0 5px 0 0;
        }

        .genealogy-tree li:first-child::after {
            border-radius: 5px 0 0 0;
            -webkit-border-radius: 5px 0 0 0;
            -moz-border-radius: 5px 0 0 0;
        }

        .genealogy-tree ul ul::before {
            content: '';
            position: absolute;
            top: 0;
            left: 50%;
            border-left: 2px solid #ccc;
            width: 0;
            height: 20px;
        }

        .genealogy-tree li a {
            text-decoration: none;
            color: #666;
            font-family: arial, verdana, tahoma;
            font-size: 11px;
            display: inline-block;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
        }

        .genealogy-tree li a:hover + ul li::after,
        .genealogy-tree li a:hover + ul li::before,
        .genealogy-tree li a:hover + ul::before,
        .genealogy-tree li a:hover + ul ul::before {
            border-color: #231f20;

        }

        /*--------------memeber-card-design----------*/
        .member-view-box {
            padding: 0px 20px;
            text-align: center;
            border-radius: 4px;
            position: relative;
        }

        .member-image {
            width: 60px;
            position: relative;
        }

        .member-image img {
            width: 60px;
            height: 60px;
            border-radius: 6px;
            background-color: #000;
            z-index: 1;
        }

        .category {
            border: 1px solid #ccc;
            /*text-decoration: none;*/
            font-weight: 500;
            color: #666;
            padding: 5px 15px;
            /*font-family: arial, verdana, tahoma;*/
            font-size: 13px;
            display: inline-block;
            /*border-radius: 10px;*/
            /*border-bottom-right-radius: 15px !important;*/
            /*border-bottom-left-radius: 0 !important;*/
            /*border-top-left-radius: 15px !important;*/
            /*border-top-right-radius: 0 !important;*/
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            background: #ffffff;

            -moz-transition: all 0.5s;
            margin-bottom: 3px;
            /*padding-right: 40px;*/
        }

        .span-category {
            /*padding: 10px 15px;*/
        }

        .category:hover, button.btn.category.active {
            background: #231f20 !important;
            color: white !important;
            border: 1px solid #231f20 !important;
        }

        .position_ctgry {
            top: -39px;
            left: -8px;
            /* margin: 5px; */
            /* display: flow-root; */
            position: absolute;
        }

        .plus:hover {
            background: #231f20;
            color: white;
        }

        .plus {
            padding: 0 8px 0 8px;
            border-radius: 0;
            height: 23px;
            background: #fab216;
            color: white;
            border: 1px solid #fab216;
        }

        .edit-sub-row {
            /*position: absolute;*/
            padding: 7px 10px;
            font-size: 10px;
            /*margin-top: 45px;*/
            margin-right: 5px;
        }

        .delete-sub-row {
            /*position: absolute;*/
            padding: 7px 10px;
            font-size: 10px;
            /*margin-top: 45px;*/
            margin-right: 5px;
            margin-left: 5px;
        }

        .add-sub-row {
            /*position: absolute;*/
            padding: 7px 10px;
            font-size: 10px;
            /*margin-top: 41px;*/
            margin-left: 5px;
        }

        .img-category {
            margin-left: 8px;
            /*margin-left: 60px;*/
            /*position: absolute;*/
            height: 25px;
            /*margin-top: 6px;*/
        }

        .btn-success {
            border-color: #231f20 !important;
            background-color: #231f20 !important;
            color: #FFFFFF;
        }

        .btn-success:hover, .btn-success:active,
        .btn-success:active, .btn-success:focus {
            border-color: #231f20 !important;
            background-color: #231f20 !important;
            color: #FFFFFF;
        }

        .btn-blue {
            border-color: #fab216 !important;
            background-color: #fab216 !important;
            color: #FFFFFF !important;
        }

        .btn-blue:focus, .btn-blue:active, .btn-blue:hover {
            border-color: #ffc419 !important;
            background-color: #ffc419 !important;
            color: #FFFFFF !important;
        }
    </style>

    <div class="content-wrapper">
        <div class="content-detached">
            <div class="content-body">
                <div class="content-overlay"></div>
                <section class="row">
                    <div class="col-12">
                        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                            <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.encodings')}}</h3>
                            <div class="row breadcrumbs-top d-inline-block">
                                <div class="breadcrumb-wrapper col-12">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                                        </li>
                                        <li class="breadcrumb-item active font-Dinar">{{__('admin.categories')}}</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="row all-contacts" dir="ltr">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-head">
                                <div class="card-header">
                                    <h4 class="card-title">{{__('admin.categories')}}</h4>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="body genealogy-body genealogy-scroll">
                                    <div class="genealogy-tree" style="margin-top: 10px" id="min-tree">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form method="POST" id="form-category" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="addModalLabel1">أضافة تصنيف جديد</h5>
                            <div class="heading-elements" id="check_show">
                                <input type="checkbox" name="status" value="1"
                                       class="switchBootstrap" id="switchBootstrap18"
                                       data-on-color="primary" data-off-color="danger"
                                       data-on-text="{{__('admin.enable')}}"
                                       data-off-text="{{__('admin.disable')}}" data-color="primary"
                                       data-label-text="{{__('admin.status')}}"
                                       checked/>
                            </div>
                        </div>
                        <div class="modal-body span">
                            <fieldset class="form-group col-12">
                                <label class="span label-control">
                                    {{__('admin.category_name')}} (ar):
                                    <span class="danger">*</span>
                                </label>
                                <input type="text"
                                       name="category_name_ar"
                                       class="form-control"
                                       placeholder="{{__('admin.category_name')}}">
                                <span class="error-message">
                                    <strong></strong>
                                </span>
                            </fieldset>
                            <fieldset class="form-group col-12">
                                <label class="span label-control">
                                    {{__('admin.category_name')}} (en):
                                    <span class="danger">*</span>
                                </label>
                                <input type="text"
                                       name="category_name_en"
                                       class="form-control"
                                       placeholder="{{__('admin.category_name')}}">
                                <span class="error-message">
                                    <strong></strong>
                                </span>
                            </fieldset>
                            <fieldset class="form-group col-12">
                                <label for="level" class="span label-control">{{__('admin.level')}}:<span class="danger">*</span></label>
                                <input id="level" type="number" value="100" name="level" class="form-control" required>
                                <span class="error-message">
                                    <strong></strong>
                                </span>
                            </fieldset>
                            <fieldset class="form-group col-12" id="type-group">
                                <label class="span label-control">
                                    {{__('admin.image')}}:
                                    <span class="danger">*</span>
                                </label>
                                <input type="file" accept=".jpg, .jpeg, .png"
                                       id="edit_image_ar" name="image"
                                       class="form-control @error('image') is-invalid @enderror"
                                       onchange="loadAvatar(this);"
                                >
                                <button type="button" hidden onclick="delete_img()"
                                        style="position: static; "
                                        class="btn btn-sm btn-outline-danger"
                                        id="delete-img"><i class="ft-trash"></i>
                                </button>

                                <img id="avatar" style="max-width: 140px; height: auto; margin:10px;">
                                <span class="error-message">
                                    <strong></strong>
                                </span>
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" id="save" class="btn btn-primary">إضافة</button>
                                <button type="button" id="edit" class="btn btn-primary" hidden>تعديل</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @include('managements.encoding.main-categories.js')
@endsection
