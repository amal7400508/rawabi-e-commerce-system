<script>
    let count = parseInt("{{$count}}");
    let table_show = $('#table-show');
    let table_count = $('#table-count');

    $("select[name=table_length]").click(function () {
        let open = $(this).data("is_open");
        if (open) {
            window.location.href = $(this).val()
        }
        $(this).data("is_open", !open);
    });

    @can('create ceiling')
    $(document).on('click', '#openAddModal', function () {
        if (count === 0) {
            $('#form input:not(:first), #form textarea').val('').removeClass('is-invalid');
            $('#form span strong').empty();
            $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                .bootstrapSwitch('state', true);

            $('#addModal').modal('show');
            $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
                .attr("data_url", "{{route('ceiling.store')}}")
                .attr("data_type", "add");
        }
    });
    @endcan

    @can('update ceiling')
    /*start code edit*/
    let ceiling_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        ceiling_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('encoding/ceiling/edit')}}" + '/' + ceiling_id;
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');

        $('#avatar').attr('hidden', true);
        $('#form span strong').empty();

        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('encoding/ceiling/update')}}" + '/' + ceiling_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);

            $('#edit_image_ar').attr('required', false);
            $('#label-image').text("{{__('admin.image')}}");
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            document.getElementById('price').value = data.price;
            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/
    @endcan

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('ceiling_id', ceiling_id);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                let response_data = data.ceiling;

                $('#addModal').modal('hide');
                let tr_color_red = '';
                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.price + "</td>");
                let col3 = $("<td>" + response_data.created_at + "</td>");
                let col4 = $("<td>" + response_data.updated_at + "</td>");
                let col5 = $("<td>" + response_data.actions + "</td>");

                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                    count++;
                    $("#openAddModal").attr('hidden', true);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.price !== undefined) {
                $('#form input[name=price]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.price);
            }
        }
    });
    /*end code add or update*/

    @can('delete ceiling')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('encoding/ceiling/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
            this_row.remove();
            count = 0;
            $("#openAddModal").attr('hidden', false);
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan
</script>
