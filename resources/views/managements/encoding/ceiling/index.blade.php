@php($page_title = __('admin.minimum_order'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.encodings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i
                                        class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.minimum_order')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create ceiling')
                                            @if($count == 0)
                                                <a class="btn btn-primary" id="openAddModal"><i
                                                        class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.minimum_order')}}
                                                </a>
                                            @endif
                                        @endcan
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('ceiling')}}" style="color: #6B6F82;"><i
                                                            class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <table width="100%" id="table"
                                                       class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.minimum_order')}}</th>
                                                        <th>{{__('admin.created_at')}}</th>
                                                        <th>{{__('admin.updated_at')}}</th>
                                                        @canany(['update ceiling', 'delete ceiling'])
                                                            <th>{{__('admin.action')}}</th>
                                                        @endcanany
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $ceiling)
                                                        <tr style="{{$ceiling->background_color_row}}">
                                                            <td hidden>{{$ceiling->updated_at}}</td>
                                                            <td class="font-default">{{$ceiling->id}}</td>
                                                            <td>{{$ceiling->price}}</td>
                                                            <td>{{$ceiling->created_at}}</td>
                                                            <td>{{$ceiling->updated_at}}</td>
                                                            @canany(['update ceiling', 'delete ceiling'])
                                                                <td>
                                                                    {!! $ceiling->actions !!}
                                                                </td>
                                                            @endcanany
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>x

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form"
                                style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.minimum_order')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="price">{{__('admin.minimum_order')}}<span
                                                        class="danger">*</span></label>
                                                <input type="number" id="price"
                                                       class="form-control"
                                                       name="price"
                                                       required>
                                                <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>


@endsection
@section('script')
    @include('managements.encoding.ceiling.js')
@endsection
