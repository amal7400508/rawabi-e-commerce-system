<script>
    $(document).on('change', '#categories', function () {
        reloadTable($(this).val());
    });

    function reloadTable(category_id)
    {
        let table_body = $('#table tbody');
        $.ajax({
            type: 'GET',
            url: "{{url('encoding/branches_categories/get-branches')}}" + "/" + category_id,
            dataType: "json",
            beforeSend: function () {
                return table_body.html(
                    '<tr class="text-center"><td colspan="4">{{__('admin.loading')}} <i class="la la-spinner spinner"></i></td></tr>'
                );
            },
            success: function (data) {
                if(data.data.length === 0)
                {
                    return table_body.html(
                        '<tr class="text-center"><td colspan="4">{{__('admin.no_data')}}</td></tr>'
                    );
                }
                table_body.empty();
                for (let i = 0; i < data.data.length; i++) {
                    table_body.append(
                        '<tr>' +
                        '<td>' + data.data[i].category_name + '</td>' +
                        '<td>' + data.data[i].branch_name + '</td>' +
                        '<td>' + data.data[i].commission + '</td>' +
                        '<td><a class="action-item delete-row" id="'+ data.data[i].id +'"><i class="la la-trash color-red"></i></a></td>' +
                        '</tr>'
                    );
                }
            },
            error: function (data) {
                return table_body.html(
                    '<tr class="text-center"><td colspan="4">{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i></td></tr>'
                );                }
        });
    }

    $('#form').on('submit', function (event) {
        event.preventDefault();
        let btn = $('#btn-save');
        let formData = new FormData(this);

        $.ajax({
            url: "{{route('branches_categories.store')}}",
            method: "POST",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            type: 'post',
            beforeSend: function () {
                $('#commission').removeClass('is-invalid');

                $('#form').find('span strong').empty();
                $('#form select').removeClass('is-invalid');
                btn.html('<i class="la la-spinner spinner"></i><span> {{__('admin.adding')}}...</span>');
            },
            success: function (data) {
                $('#branch').val('');
                $('#commission').val(0);

                reloadTable(data.category_id);

                btn.text('{{__('admin.add')}}');
                $('#message-modal p').text(data.success + '.');
                $('#message-modal').modal('show');
                setTimeout(function () {
                    $('#message-modal').modal('hide');
                }, 3000);
            },
            error: function (data) {
                btn.text('{{__('admin.add')}}');
                let obj = JSON.parse((data.responseText));
                let category_id = obj.error.category_id;
                let branch_id = obj.error.branch;
                let commission = obj.error.commission;

                if (category_id !== undefined || branch_id !== undefined || commission !== undefined) {
                    if (category_id !== undefined) {
                        $('#categories').addClass('is-invalid').parent().find('span strong').text(category_id)
                    }

                    if (branch_id !== undefined) {
                        $('#branch').addClass('is-invalid').parent().find('span strong').text(branch_id)
                    }

                    if (commission !== undefined) {
                        $('#commission').addClass('is-invalid').parent().parent().find('span strong').text(commission)
                    }
                } else {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            }
        });
    });


    /*start code Delete ajax*/
    $(document).on('click', '.delete-row', async function () {

        let id_row = $(this).attr('id');
        let route = "{{url('encoding/branches_categories/destroy')}}" + "/" + id_row;
        try {
            let data = await deletedItems(id_row, route);
            reloadTable(data.category_id);
        } catch (e) {
            return e;
        }
    });
    /*end code Delete ajax*/
</script>
