@php($page_title = __('admin.branches_categories'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.encodings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.branches_categories')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>{{__('admin.index')}} {{__('admin.branches_categories')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('branches_categories')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="form-body">
                                                <form id="form">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div
                                                                class="form-group">
                                                                <label for="categories">{{__('admin.categories')}}</label>
                                                                <select required class="select2 form-control" id="categories" name="category_id">
                                                                    <option value="">{{__('admin.select_option')}}</option>
                                                                    @foreach($categories as $category)
                                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="error-message">
                                                                     <strong></strong>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="branch">{{__('admin.branches')}}</label>
                                                                <select required class="select2 form-control" id="branch" name="branch">
                                                                    <option value="">{{__('admin.select_option')}}</option>
                                                                    @foreach($branches as $branch)
                                                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="error-message">
                                                                    <strong></strong>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="commission">{{__('admin.commission_rate')}}</label>

                                                                <div class="input-group">
                                                                    <input type="number" id="commission" name="commission" value="0" class="form-control">
                                                                    <div class="input-group-append"><span class="input-group-text font-default">%</span></div>
                                                                </div>

                                                                <span class="error-message">
                                                                    <strong></strong>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <button type="submit" id="btn-save" class="btn btn-primary btn-save-ctg- col-md-12" style="">{{__('admin.add')}}</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <br>
                                            <!-- Invoices List table -->
                                            <div class="table-responsive">
                                                <table width="100%"
                                                       class="table table-striped table-responsive table-custom-responsive"
                                                       id="table">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('admin.category')}}</th>
                                                        <th>{{__('admin.branch')}}</th>
                                                        <th>{{__('admin.commission')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="text-center">
                                                        <td colspan="4">{{__('admin.no_data')}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @include('managements.encoding.branches_categories.js')
@endsection
