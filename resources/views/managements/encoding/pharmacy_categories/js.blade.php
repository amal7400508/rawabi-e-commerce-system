<script>
    /*Restricts input for the given textbox to the given inputFilter.*/
   /* function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
            textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    }*/

    /*setInputFilter(document.getElementById("commission"), function(value) {
        return /^-?\d*[.,]?\d*$/.test(value); });*/

    /* Install input filters.
    setInputFilter(document.getElementById("intTextBox"), function(value) {
        return /^-?\d*$/.test(value); });
    setInputFilter(document.getElementById("uintTextBox"), function(value) {
        return /^\d*$/.test(value); });
    setInputFilter(document.getElementById("intLimitTextBox"), function(value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500); });

    setInputFilter(document.getElementById("currencyTextBox"), function(value) {
        return /^-?\d*[.,]?\d{0,2}$/.test(value); });
    setInputFilter(document.getElementById("latinTextBox"), function(value) {
        return /^[a-z]*$/i.test(value); });
    setInputFilter(document.getElementById("hexTextBox"), function(value) {
        return /^[0-9a-f]*$/i.test(value); });*/

    $(document).on('change', '#pharmacy', function () {
        reloadTable($(this).val());
    });

    function reloadTable(category_id) {
        let table_body = $('#table tbody');
        $.ajax({
            type: 'GET',
            url: "{{url('encoding/pharmacy_categories/get')}}" + "/" + category_id,
            dataType: "json",
            beforeSend: function () {
                return table_body.html(
                    '<tr class="text-center"><td colspan="10">{{__('admin.loading')}} <i class="la la-spinner spinner"></i></td></tr>'
                );
            },
            success: function (data) {
                if (data.data.length === 0) {
                    return table_body.html(
                        '<tr class="text-center"><td colspan="10">{{__('admin.no_data')}}</td></tr>'
                    );
                }
                table_body.empty();
                for (let i = 0; i < data.data.length; i++) {
                    table_body.append(
                        '<tr>' +
                        '<td>' + data.data[i].pharmacy + '</td>' +
                        '<td>' + data.data[i].category + '</td>' +
                        /*'<td>' + data.data[i].sub_category + '</td>' +*/
                        // '<td>' + data.data[i].insurance_company + '</td>' +
                        /*'<td>' + data.data[i].commission + '</td>' +*/
                        @can('delete pharmacy_categories')
                            '<td><a class="action-item delete-row" id="' + data.data[i].id + '"><i class="la la-trash color-red"></i></a></td>' +
                        @endcan
                            '</tr>'
                    );
                }
            },
            error: function (data) {
                return table_body.html(
                    '<tr class="text-center"><td colspan="10">{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i></td></tr>'
                );
            }
        });
    }

    @can('create pharmacy_categories')
    $('#form').on('submit', function (event) {
        event.preventDefault();
        let btn = $('#btn-save');
        let formData = new FormData(this);

        $.ajax({
            url: "{{route('pharmacy_categories.store')}}",
            method: "POST",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            type: 'post',
            beforeSend: function () {
                $('#form').find('span strong').empty();
                $('#form select').removeClass('is-invalid');
                btn.html('<i class="la la-spinner spinner"></i><span> {{__('admin.adding')}}...</span>');
            },
            success: function (data) {
                reloadTable(data.pharmacies_id);
                console.log(data.pharmacies_id);
                btn.text('{{__('admin.add')}}');
                $('#message-modal p').text(data.success + '.');
                $('#message-modal').modal('show');
                setTimeout(function () {
                    $('#message-modal').modal('hide');
                }, 3000);
            },
            error: function (data) {
                btn.text('{{__('admin.add')}}');
                let obj = JSON.parse((data.responseText));
                // let insurance_company = obj.error.insurance_company;
                let pharmacy = obj.error.pharmacy;
                let category = obj.error.category;
                /*let commission = obj.error.commission;*/

                {{--if (insurance_company !== undefined || pharmacy !== undefined || category !== undefined || commission !== undefined) {--}}
                {{--    if (insurance_company !== undefined) {--}}
                {{--        $('#insurance_company').addClass('is-invalid').parent().find('span strong').text(insurance_company)--}}
                {{--    }--}}

                {{--    if (pharmacy !== undefined) {--}}
                {{--        $('#pharmacy').addClass('is-invalid').parent().find('span strong').text(pharmacy)--}}
                {{--    }--}}

                {{--    if (category !== undefined) {--}}
                {{--        $('#category').addClass('is-invalid').parent().parent().find('span strong').text(category)--}}
                {{--    }--}}
                {{--    /*if (commission !== undefined) {--}}
                {{--        $('#commission').addClass('is-invalid').parent().parent().find('span strong').text(commission)--}}
                {{--    }*/--}}
                {{--} else {--}}
                {{--    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');--}}
                {{--    $('#confirm-modal-loading-show').modal('show');--}}
                {{--}--}}
            }
        });
    });
    @endcan

    @can('delete pharmacy_categories')
    /*start code Delete ajax*/
    $(document).on('click', '.delete-row', async function () {

        let id_row = $(this).attr('id');
        let route = "{{url('encoding/pharmacy_categories/destroy')}}" + "/" + id_row;
        try {
            let data = await deletedItems(id_row, route);
            // reloadTable(data.insurance_company_id);
        } catch (e) {
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan

    /*$(document).on('change', '#category', function () {
        let main_category_id = $(this).val();
        let sub_category_element = $('select[name=sub_category]');
        $.ajax({
            type: 'GET',
            url: "{{url('encoding/pharmacy_categories/get-sub-categories')}}" + "/" + main_category_id,
            dataType: "json",
            beforeSend: function () {
                sub_category_element.html('<option value="">{{__('admin.loading')}}...</option>')
            },
            success: function (data) {
                sub_category_element.html('<option value="">{{__('admin.select_option')}}</option>');
                $.each(data, function (index, item) {
                    sub_category_element.append('<option value="' + item.id + '">' + item.name + '</option>');
                });

            },
            error: function (data) {
                sub_category_element.html('<option value="">{{__('admin.loading_failed')}}</option>')
            }
        });
    });*/
</script>
