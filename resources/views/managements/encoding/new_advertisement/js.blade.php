<script>

    let table_show = $('#table-show');
    let table_count = $('#table-count');

    $("select[name=table_length]").click(function () {
        let open = $(this).data("is_open");
        if (open) {
            window.location.href = $(this).val()
        }
        $(this).data("is_open", !open);
    });

    $(document).on('click', '#openAddModal', function () {
        delete_img();
        $('#form input[type=text],#form input[type=file], #form input[type=date], #form textarea, #from select').val('').removeClass('is-invalid');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);

        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('encoding.new_advertisement.store')}}")
            .attr("data_type", "add");
    });

    /*start code edit*/
    let advertisement_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        linkType('product');
        advertisement_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('/encoding/new_advertisement/edit')}}" + '/' + advertisement_id;
        $('#form input[type=text], #form input[type=file], #form input[type=date], #form textarea, #from select').val('').removeClass('is-invalid');

        /*$('#form input:not(:first), #form textarea, #from select').val('').removeClass('is-invalid');*/

        $('#avatar').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('/encoding/new_advertisement/update')}}" + '/' + advertisement_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);

            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            $('#image').attr('required', false);
            $('#avatar').attr('src', data.image).attr('hidden', false);
            $('#delete-img').attr('hidden', false);
            $('select[name=type]').val(data.type);
            $('select[name=slide_number]').val(data.slide_number);
            $('input[name=end_date]').val(data.end_date);
            $('#description_ar').val(data.description_ar);
            $('#description_en').val(data.description_en);

            // if (data.ibb && data.sana_a) {
            //     $('#sana_a').prop('checked', true);
            //     $('#ibb').prop('checked', true);
            // } else if (data.ibb) {
            //     $('#ibb').prop('checked', true);
            //     $('#sana_a').prop('checked', false);
            // } else if (data.sana_a) {
            //     $('#sana_a').prop('checked', true);
            //     $('#ibb').prop('checked', false);
            // } else {
            //     $('#sana_a').prop('checked', false);
            //     $('#ibb').prop('checked', false);
            // }

            $('#check-link').prop('checked', false);
            $('#row_like select').attr('disabled', true);
            if (data.check_link) {
                $('#check-link').prop('checked', true);
                $('#row_like select').attr('disabled', false);
                $('select[name=link_type]').val(data.link_type);
                linkType(data.link_type);
                $('select[name=offer]').val(data.advertisementable_id);
                $('select[name=product]').append('<option value="' + data.advertisementable_id + '"> ' + data.product_name + ' </option>');

            }

            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('advertisement_id', advertisement_id);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                let response_data = data.advertisement;

                $('#addModal').modal('hide');

                let tr_color_red = '';

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.type + "</td>");
                let col3 = $("<td>" + response_data.slide_number + "</td>");
                let col4 = $("<td>" + response_data.desc + "</td>");
                let col5 = $('<td><img style="max-width:64px" src="{{asset('storage')}}' + '/' + response_data.image + '"' +
                    'onerror=this.src="{{asset('/storage/thumbnail/150/defualt_product.png')}}"/></td>'
                );
                let col6 = $("<td>" + response_data.actions + "</td>");
                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5,col6).prependTo("#table");
                    this_row = $('#row_' + response_data.id);

                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5,col6);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.type !== undefined) {
                $('#form select[name=type]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.type);
            }
            if (obj.slide_number !== undefined) {
                $('#form select[name=slide_number]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.slide_number);
            }
            if (obj.end_date !== undefined) {
                $('#form input[name=end_date]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.end_date);
            }
            if (obj.description_ar !== undefined) {
                $('#form input[name=description_ar]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.description_ar);
            }
            if (obj.description_en !== undefined) {
                $('#form input[name=description_en]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.description_en);
            }
            if (obj.link_type !== undefined) {
                $('#form select[name=link_type]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.link_type);
            }
            if (obj.product !== undefined) {
                $('#form select[name=product]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.product);
            }
            if (obj.offer !== undefined) {
                $('#form select[name=offer]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.offer);
            }
            if (obj.image !== undefined) {
                $('#form input[name=image]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.image);
            }
        }
    });
    /*end code add or update*/

    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('/encoding/new_advertisement/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
            this_row.remove();
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });

    /*end code Delete ajax*/


    function delete_img() {
        document.getElementById('image').value = null;
        $('#avatar').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
    }

    function loadAvatar(input) {
        $('#avatar').attr('hidden', false);
        $('#delete-img').attr('hidden', false);

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                var image = document.getElementById('avatar');
                image.src = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    /*ajax falter start*/
    $(document).on('change', '#category', function () {
        let category_id = $(this).val();
        let category_url = "{{url('encoding/new_advertisement/falterProduct')}}" + "/" + category_id;
        if (category_id) {
            $.ajax({
                type: "GET",
                url: category_url,
                success: function (data) {
                    if (data) {
                        $('#product').empty().append('<option value=""> {{__("admin.select_option")}}  </option>');
                        $.each(data, function (key, value) {
                            $("#product").append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    } else {
                        $('#product').empty();
                    }
                }
            });
        } else {
            $('#product').empty();
        }
    });
    /*ajax falter end*/
    $('#row_like select').attr('disabled', true);
    $(document).on('change', '#check-link', function () {
        if ($(this).is(':checked')) {
            return $('#row_like select').attr('disabled', false);
        }
        return $('#row_like select').attr('disabled', true);
    });

    $(document).on('change', '#link_type', function () {
        let value = $(this).val();
        linkType(value);
    });

    function linkType(value) {
        if (value == 'product') {
            $('#div-item').html(
                '<div class="form-group col-md-6">' +
                '    <label for="category">{{__('admin.categories')}}<span class="danger">*</span></label>' +
                '    <select class="form-control" name="category" id="category">' +
                '        <option value="">{{__('admin.select_option')}}</option>' +
                '        @foreach($categories as $category)' +
                '            <option value="{{$category->id}}">{{$category->name}}</option>' +
                '        @endforeach' +
                '    </select>' +
                '    <span class="error-message">' +
                '        <strong></strong>' +
                '    </span>' +
                '</div>' +
                '<div class="form-group col-md-6">' +
                '    <label for="product">{{__('admin.product')}}<span class="danger">*</span></label>' +
                '    <select required class="form-control" name="product" id="product">' +
                '    </select>' +
                '    <span class="error-message">' +
                '        <strong></strong>' +
                '    </span>' +
                '</div>'
            );
        } else if (value == 'offer') {
            $('#div-item').html(
                '<div class="form-group col-md-12">' +
                '    <label for="offer">{{__('admin.offer')}}<span class="danger">*</span></label>' +
                '    <select required class="form-control" name="offer" id="offer">' +
                '        <option value="">{{__('admin.select_option')}}</option>' +
                '        @foreach($offers as $offer)' +
                '            <option value="{{$offer->id}}">{{$offer->name}}</option>' +
                '        @endforeach' +
                '    </select>' +
                '    <span class="error-message">' +
                '        <strong></strong>' +
                '    </span>' +
                '</div>'
            );
        }
    }

</script>
