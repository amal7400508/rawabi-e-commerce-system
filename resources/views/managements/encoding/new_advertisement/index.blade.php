@php($page_title = __('admin.advertisement'))
@extends('layouts.main')
@section('css')

@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.advertisement')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">{{__('admin.Dashboard')}}</a></li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.advertisement')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                <div class="btn-group float-md-right">
                    @can('delete advertisement')
                        <a href="{{--{{url('advertisement/recycle_bin')}}--}}" class="btn btn-primary"
                           style="color: white">{{__('admin.recycle_bin')}} <i class="ft-trash position-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <button class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.advertisement')}}</button>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('encoding.new_advertisement')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number"
                                                                            @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="media"
                                                                            @if($search_type == 'media') selected @endif>{{__('admin.media')}}</option>
                                                                    <option value="media_type"
                                                                            @if($search_type == 'media_type') selected @endif>{{__('admin.media_type')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.type')}}</th>
                                                        <th>{{__('admin.slide_number')}}</th>
                                                        <th>{{__('admin.description')}}</th>
                                                        <th>{{__('admin.image')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $advertisement)
                                                        <tr style="{{$advertisement->background_color_row}}">
                                                            <td hidden>{{$advertisement->updated_at}}</td>
                                                            <td class="font-default">{{$advertisement->id}}</td>
                                                            <td>{{$advertisement->type}}</td>
                                                            <td>{{$advertisement->slide_number}}</td>
                                                            <td>{{$advertisement->desc}}</td>
                                                            <td>
                                                                <img style="max-width:64px" src="{{asset('storage').'/'.$advertisement->image}}">
                                                            </td>
                                                            <td>
                                                                {!! $advertisement->actions !!}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends(request()->all())->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.advertisement')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>

                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6 p-0">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="type">{{__('admin.media_type')}}<span class="danger">*</span></label>
                                                    <select class="form-control" id="type" required name="type">
                                                        <option value="">{{__('admin.select_option')}}</option>
                                                        <option value="slider">slider</option>
                                                        <option value="popup">popup</option>
                                                    </select>
                                                    <span class="error-massege">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="slide_number">{{__('admin.slide_number')}}<span class="danger">*</span></label>
                                                    <select  name="slide_number"  id="slide_number" class="form-control">
                                                        <option value="">{{__('admin.select_option')}}</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>

                                                    </select>

                                                    <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="image">{{__('admin.image')}}<span class="danger">*</span></label>
                                                    <input type="file" id="image" class="form-control" name="image" onchange="loadAvatar(this);" required>


                                                    <button type="button" hidden onclick="delete_img()"
                                                            style="position: static; "
                                                            class="btn btn-sm btn-outline-danger"
                                                            id="delete-img"><i class="ft-trash"></i>
                                                    </button>

                                                    <img id="avatar"
                                                         style="max-width: 140px; height: auto; margin:10px;">

                                                    <span class="error-massege">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <br>
{{--                                            <div class="form-group col-md-12">--}}
{{--                                                <div class="row">--}}
{{--                                                    <label class="col-md-5 label-control">{{__('admin.available_in_city')}}--}}
{{--                                                        <span class="danger">*</span></label>--}}
{{--                                                    <div class="col-md-7 mx-auto">--}}
{{--                                                        <div class="input-group">--}}
{{--                                                            <div class="d-inline-block custom-control custom-checkbox mr-1">--}}
{{--                                                                <input type="checkbox" name="city[]" value="sana_a" class="custom-control-input" id="sana_a">--}}
{{--                                                                <label class="custom-control-label cursor-pointer" for="sana_a">{{__('admin.sana_a')}}</label>--}}
{{--                                                            </div>--}}
{{--                                                            <div class="d-inline-block custom-control custom-checkbox">--}}
{{--                                                                <input type="checkbox" name="city[]" value="ibb" class="custom-control-input" id="ibb">--}}
{{--                                                                <label class="custom-control-label cursor-pointer" for="ibb">{{__('admin.ibb')}}</label>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        <span class="error-massege">--}}
{{--                                                            <strong id="error_day"></strong>--}}
{{--                                                        </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

                                        </div>
                                        <div class="col-md-6 p-0">
                                            <div class="form-group col-md-12">
                                                <label for="end_date">{{__('admin.end_date')}}<span class="danger">*</span></label>
                                                <input type="date" id="end_date" class="form-control" name="end_date" required>
                                                <span class="error-massege">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="description_ar">{{__('admin.description')}} (ar)<span class="danger">*</span></label>
                                                <input type="text" id="description_ar" class="form-control" name="description_ar" required>
                                                <span class="error-massege">
                                                    <strong></strong>
                                                </span>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label for="description_en">{{__('admin.description')}} (en)<span class="danger">*</span></label>
                                                <input type="text" id="description_en" class="form-control" name="description_en" required>
                                                <span class="error-massege">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <div class="d-inline-block custom-control custom-checkbox mr-1">
                                                    <input type="checkbox" name="check_link" value="1" class="custom-control-input" id="check-link">
                                                    <label class="custom-control-label cursor-pointer" for="check-link">{{__('admin.add_link')}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div id="row_like" class="row" style="background-color: #ffffff08;margin: 5px 0px;border: 1px solid #c5cbd921;padding: 13px;border-radius: 5px;">
                                                <div class="form-group col-md-4">
                                                    <label for="link_type">{{__('admin.link_type')}}<span class="danger">*</span></label>
                                                    <select class="form-control" name="link_type" id="link_type">
                                                        <option value="product">{{__('admin.a_product')}}</option>
                                                        <option value="offer">{{__('admin.offer')}}</option>
                                                    </select>
                                                    <span class="error-massege">
                                                    <strong></strong>
                                                </span>
                                                </div>
                                                <div class="col-md-8 row" id="div-item">
                                                    <div class="form-group col-md-6">
                                                        <label for="category">{{__('admin.categories')}}<span class="danger">*</span></label>
                                                        <select required class="form-control" name="category" id="category">
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-massege">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="product">{{__('admin.product')}}<span class="danger">*</span></label>
                                                        <select required class="form-control" name="product" id="product">
                                                        </select>
                                                        <span class="error-massege">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    @include('managements.advertisements.advertisements.show')

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')
@endsection
@section('script')
    <script src="{{asset('app-assets/vendors/js/extensions/sweetalert.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/extensions/sweet-alerts.js')}}"></script>

    @include('include.ajax-CRUD')
    @include('managements.encoding.new_advertisement.js')
    @include('managements.advertisements.advertisements.show_js')

@endsection
