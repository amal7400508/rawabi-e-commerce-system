<script>
    $(document).on('change', '#branch_id', function () {
        reloadTable($(this).val());
    });

    function reloadTable(branch_id) {
        let table_body = $('#table tbody');
        $.ajax({
            type: 'GET',
            url: "{{url('encoding/branch_services/get')}}" + "/" + branch_id,
            dataType: "json",
            beforeSend: function () {
                return table_body.html(
                    '<tr class="text-center"><td colspan="10">{{__('admin.loading')}} <i class="la la-spinner spinner"></i></td></tr>'
                );
            },
            success: function (data) {
                if (data.data.length === 0) {
                    return table_body.html(
                        '<tr class="text-center"><td colspan="10">{{__('admin.no_data')}}</td></tr>'
                    );
                }
                table_body.empty();
                for (let i = 0; i < data.data.length; i++) {
                    table_body.append(
                        '<tr>' +
                        '<td>' + data.data[i].branch + '</td>' +
                        '<td>' + data.data[i].service_type + '</td>' +
                        @can('delete branch_services')
                            '<td><a class="action-item delete-row" id="' + data.data[i].id + '"><i class="la la-trash color-red"></i></a></td>' +
                        @endcan
                            '</tr>'
                    );
                }
            },
            error: function (data) {
                return table_body.html(
                    '<tr class="text-center"><td colspan="10">{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i></td></tr>'
                );
            }
        });
    }

    @can('create branch_services')
    $('#form').on('submit', function (event) {
        event.preventDefault();
        let btn = $('#btn-save');
        let formData = new FormData(this);

        $.ajax({
            url: "{{route('branch_services.store')}}",
            method: "POST",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            type: 'post',
            beforeSend: function () {
                $('#form').find('span strong').empty();
                $('#form select').removeClass('is-invalid');
                btn.html('<i class="la la-spinner spinner"></i><span> {{__('admin.adding')}}...</span>');
            },
            success: function (data) {
                reloadTable(data.branch_id);
                btn.text('{{__('admin.add')}}');
                $('#message-modal p').text(data.success + '.');
                $('#message-modal').modal('show');
                setTimeout(function () {
                    $('#message-modal').modal('hide');
                }, 3000);
            },
            error: function (data) {
                btn.text('{{__('admin.add')}}');
                let obj = JSON.parse((data.responseText));
                let branch_id = obj.error.branch_id;
                let service_type_id = obj.error.service_type_id;

                if (branch_id !== undefined || service_type_id !== undefined || category !== undefined) {
                    if (branch_id !== undefined) {
                        $('#branch_id').addClass('is-invalid').parent().find('span strong').text(branch_id)
                    }

                    if (service_type_id !== undefined) {
                        $('#service_type_id').addClass('is-invalid').parent().find('span strong').text(service_type_id)
                    }

                } else {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            }
        });
    });
    @endcan

    @can('delete branch_services')
    /*start code Delete ajax*/
    $(document).on('click', '.delete-row', async function () {

        let id_row = $(this).attr('id');
        let route = "{{url('encoding/branch_services/destroy')}}" + "/" + id_row;
        try {
            let data = await deletedItems(id_row, route);
            reloadTable(data.branch_id);
        } catch (e) {
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan
</script>
