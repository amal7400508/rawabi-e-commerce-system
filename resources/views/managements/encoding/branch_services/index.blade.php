@php($page_title = __('admin.branch_services'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.encodings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.branch_services')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>{{__('admin.index')}} {{__('admin.branch_services')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('branch_services')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="form-body">
                                                <form id="form">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="branch_id">{{__('admin.branches')}}</label>
                                                                        <select required class="select2 form-control" id="branch_id" name="branch_id">
                                                                            <option value="0">{{__('admin.select_option')}}</option>
                                                                            @foreach($branches as $branch)
                                                                                <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <span class="error-message">
                                                                    <strong></strong>
                                                                </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="service_type_id">{{__('admin.service_types')}}</label>
                                                                        <select required class="select2 form-control" id="service_type_id" name="service_type_id">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            @foreach($service_types as $service_type)
                                                                                <option value="{{$service_type->id}}">{{$service_type->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <span class="error-message">
                                                                     <strong></strong>
                                                                </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @can('create branch_services')
                                                            <div class="col-md-2">
                                                                <button type="submit" id="btn-save" class="btn btn-primary btn-save-ctg- col-md-12" style="">{{__('admin.add')}}</button>
                                                            </div>
                                                        @endcan
                                                    </div>
                                                </form>
                                            </div>

                                            <br>
                                            <!-- Invoices List table -->
                                            <div class="table-responsive">
                                                <table width="100%"
                                                       class="table table-striped table-responsive table-custom-responsive"
                                                       id="table">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('admin.branch')}}</th>
                                                        <th>{{__('admin.service_type')}}</th>
                                                        @can('delete branch_services')
                                                            <th>{{__('admin.action')}}</th>
                                                        @endcan
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="text-center">
                                                        <td colspan="10">{{__('admin.no_data')}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @include('managements.encoding.branch_services.js')
@endsection
