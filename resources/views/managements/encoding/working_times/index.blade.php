@php($page_title = __('admin.working_times'))
@extends('layouts.main')
@section('css')
    <style>
        .modal-backdrop {
            opacity: 0 !important;
        }

        .modal-backdrop:last-of-type {
            opacity: 0.5 !important;
        }

        .modal-content {
            -webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, .16), 0 3px 6px rgba(0, 0, 0, .23);
            box-shadow: 0 3px 6px rgba(0, 0, 0, .16), 0 3px 6px rgba(0, 0, 0, .23);
        }

        .bg-attitude {
            background-color: #fffafa !important;
        }
    </style>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.encodings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.working_times')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div id="messageSave" class="modal fade text-left" role="dialog">
            <div class="modal-dialog">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="alert bg-info alert-icon-left mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                            <strong>{{__('admin.successfully_done')}}!</strong>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div id="card-create" class="col-md-5 hidden">
                    <section id="basic-form-layouts">
                        <div class="row match-height">
                            <div class="col-md-12">
                                <div class="card">
                                    <form class="form" action="{{route('working_times.store')}}" method="POST"
                                          enctype="multipart/form-data" id="my_form_id">
                                        <input type="text" name="method_type" hidden>
                                        <div class="card-header">
                                            <h4 class="card-title" id="basic-layout-form">{{__('admin.create')}} {{__('admin.working_times')}}</h4>
                                            <a class="heading-elements-toggle"><i
                                                    class="la la-ellipsis-v font-medium-3"></i></a>
                                            <div class="heading-elements" id="check_show">
                                                <input type="checkbox" name="status" value="1"
                                                       class="switchBootstrap" id="switchBootstrap18"
                                                       data-on-color="primary" data-off-color="danger"
                                                       data-on-text="{{__('admin.enable')}}"
                                                       data-off-text="{{__('admin.disable')}}" data-color="primary"
                                                       data-label-text="{{__('admin.status')}}" checked/>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="card-content collapse show">
                                            <div class="card-body">
                                                @csrf
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4>
                                                                <strong>{{__('admin.day')}}:</strong>
                                                                <span id="day">
                                                                    {{__('admin.saturday')}}
                                                                </span>
                                                            </h4>
                                                            <hr>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div
                                                                class="form-group {{ $errors->has('start_time') ? ' has-error' : '' }}">
                                                                <label>{{__('admin.start_time')}}<span
                                                                        class="danger">*</span></label>
                                                                <input type="time"
                                                                       class="form-control @error('start_time') is-invalid @enderror"
                                                                       name="start_time"
                                                                       value="{{ old('start_time') }}">
                                                                @error('start_time')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div
                                                                class="form-group {{ $errors->has('end_time') ? ' has-error' : '' }}">
                                                                <label>{{__('admin.end_time')}}<span
                                                                        class="danger">*</span></label>
                                                                <input type="time"
                                                                       class="form-control @error('end_time') is-invalid @enderror"
                                                                       name="end_time" value="{{ old('end_time') }}">
                                                                @error('end_time')
                                                                <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-primary col-md-6 pull-right"
                                                            id="button_save">
                                                        <i class="la la-check-square-o"></i> {{__('admin.save')}}
                                                    </button>
                                                    <button type="button" class="btn btn-dark col-md-4 pull-left"
                                                            id="button_cancel">{{__('admin.cancel')}}
                                                    </button>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-12" id="index">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create working_times')
                                            <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.working_times')}}</a>
                                        @endcan
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a href="{{url('dashboard')}}" data-action="close"><i
                                                            class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <table width="100%" class="table  zero-configuration" id="table">
                                                    <thead>
                                                    <tr>
                                                        {{--                                                        <th>{{__('admin.updated_at')}}</th>--}}
                                                        <th>{{__('admin.week_day')}}</th>
                                                        {{--                                                        <th>{{__('admin.start_time')}}</th>--}}
                                                        {{--                                                        <th>{{__('admin.end_time')}}</th>--}}
                                                        {{--                                                        <th>{{__('admin.status')}}</th>--}}
                                                        {{--                                                        <th width="30%">{{__('admin.note')}}</th>--}}
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    {{-- start model show--}}
    <div id="confirmModalShow" class="modal fade text-left" role="dialog">
        <div class="modal-dialog long-mode">
            <div class="modal-content">
                <div class="card-content">
                    <div class="modal-header danger">
                        <h5 class="modal-title" id="myModalLabel1">
                            {{__('admin.day')}}:
                            <span id="day-name" class="color-blue"></span>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span class="text-bold-600">{{__('admin.post_index')}} {{__('admin.working_times')}}</span>
                        <hr>
                        <table width="100%" class="table table-striped table-responsive table-custom-responsive" id="details_table">
                            <thead style="background: linear-gradient(#A90f15 ,#ed1b24 );color: white">
                            <tr>
                                {{--                                <th>#</th>--}}
                                <th>{{__('admin.start_time')}}</th>
                                <th>{{__('admin.end_time')}}</th>
                                {{--                                <th>{{__('admin.note')}}</th>--}}
                                <th>{{__('admin.status')}}</th>
                                <th>{{__('admin.time')}}</th>
                                @canany(['update working_times', 'delete working_times'])
                                    <th class="hide-in-tracking">{{__('admin.action')}}</th>
                                @endcanany
                            </tr>
                            </thead>
                            <tbody class="font-default">
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <hr class="p0 m0">
                    <div class="col-md-12">
                        <small class="price category-color">{{__('admin.last_update')}}
                            :</small>
                        <small class="price category-color" id="created"></small>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show--}}

    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center"
                        style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark"
                            data-dismiss="modal">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- start model Don Delete Message--}}
    <div id="messageDonDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                        <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                        <strong>{{__('admin.successfully_done')}}!</strong>
                        <p>{{__('admin.deleted_successfully')}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--end model Don Delete Message--}}

    @can('create working_times')
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <section class="contact-form">
                        <form id="form-add-times">
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title">{{__('admin.add')}}  {{__('admin.working_times')}}</h5>
                                <button type="button" class="close"
                                        data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-body">
                                <fieldset class="form-group col-12" id="fieldset-days" style="background: #f0f0f0; padding: 10px;border-radius: 5px;">
                                    <table class="padding_right">
                                        <tr>
                                            <td width="110px">
                                                <input id="sat" type="checkbox" name="days[]" value="SATURDAY">
                                                <label for="sat">{{__('admin.saturday')}}</label>
                                            </td>
                                            <td width="110px">
                                                <input id="sun" type="checkbox" name="days[]" value="SUNDAY">
                                                <label for="sun">{{__('admin.sunday')}}</label>
                                            </td>
                                            <td width="110px">
                                                <input id="mon" type="checkbox" name="days[]" value="MONDAY">
                                                <label for="mon">{{__('admin.monday')}}</label>
                                            </td>
                                            <td width="110px">
                                                <input id="tue" type="checkbox" name="days[]" value="TUESDAY">
                                                <label for="tue">{{__('admin.tuesday')}}</label>
                                            </td>
                                            <td width="110px">
                                                <input id="wed" type="checkbox" name="days[]" value="WEDNESDAY">
                                                <label for="wed">{{__('admin.wednesday')}}</label>
                                            </td>
                                            <td width="110px">
                                                <input id="thu" type="checkbox" name="days[]" value="THURSDAY">
                                                <label for="thu">{{__('admin.thursday')}}</label>
                                            </td>
                                            <td width="110px">
                                                <input id="fri" type="checkbox" name="days[]" value="FRIDAY">
                                                <label for="fri">{{__('admin.friday')}}</label>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <fieldset class="form-group col-md-12">
                                    <span class="error-massege" id="days-error"><strong></strong></span>
                                </fieldset>
                                <fieldset class="form-group row col-md-12">
                                    <div class="col-md-6">
                                        <label>{{__('admin.start_time')}}:<span class="danger">*</span></label>
                                        <input type="time" name="start_time" class="form-control">
                                        <span class="error-massege">
                                        <strong></strong>
                                    </span>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{__('admin.end_time')}}:<span class="danger">*</span></label>
                                        <input type="time" name="end_time" class="form-control">
                                        <span class="error-massege">
                                       <strong></strong>
                                    </span>
                                    </div>
                                </fieldset>
                                {{-- <fieldset class="form-group col-12 span">
                                     <label>{{__('admin.note')}} (ar):</label>
                                     <textarea name="message_ar_add" rows="4" class="form-control"></textarea>
                                 </fieldset>
                                 <fieldset class="form-group col-12 span">
                                     <label>{{__('admin.note')}} (en):</label>
                                     <textarea name="message_en_add" id="message_en_add" rows="4" class="form-control"></textarea>
                                 </fieldset>--}}
                            </div>
                            <div class="modal-footer">
                                <fieldset id="button-container"
                                          class="form-group position-relative has-icon-left mb-0">
                                    <button type="submit" class="btn btn-primary" id="btn-save"><i
                                            class="ft-save"></i> {{__('admin.save')}}</button>
                                </fieldset>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    @endcan

@endsection
@section('script')
    @include('managements.encoding.working_times.js')
@endsection
