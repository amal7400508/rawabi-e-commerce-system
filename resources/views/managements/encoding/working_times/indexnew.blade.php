@php($page_title = __('admin.working_times'))
@extends('layouts.main')
@section('css')
    <style>
        .modal-backdrop {
            opacity: 0 !important;
        }

        .modal-backdrop:last-of-type {
            opacity: 0.5 !important;
        }

        .modal-content {
            -webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, .16), 0 3px 6px rgba(0, 0, 0, .23);
            box-shadow: 0 3px 6px rgba(0, 0, 0, .16), 0 3px 6px rgba(0, 0, 0, .23);
        }

        .bg-attitude {
            background-color: #fffafa !important;
        }
    </style>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.working_times')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item active font-Dinar">{{__('admin.working_times')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div id="messageSave" class="modal fade text-left" role="dialog">
            <div class="modal-dialog">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="alert bg-info alert-icon-left mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                            <strong>{{__('admin.successfully_done')}}!</strong>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div id="card-create" class="col-md-5 hidden">
                    <section id="basic-form-layouts">
                        <div class="row match-height">
                            <div class="col-md-12">
                                <div class="card">
                                    <form class="form" action="{{route('working_times.store')}}" method="POST"
                                          enctype="multipart/form-data" id="my_form_id">
                                        <input type="text" name="method_type" hidden>
                                        <div class="card-header">
                                            <h4 class="card-title"
                                                id="basic-layout-form">{{__('admin.create')}} {{__('admin.note')}}</h4>
                                            <a class="heading-elements-toggle"><i
                                                    class="la la-ellipsis-v font-medium-3"></i></a>
                                            <div class="heading-elements" id="check_show">
                                                <input type="checkbox" name="status" value="1"
                                                       class="switchBootstrap" id="switchBootstrap18"
                                                       data-on-color="primary" data-off-color="danger"
                                                       data-on-text="{{__('admin.enable')}}"
                                                       data-off-text="{{__('admin.disable')}}" data-color="primary"
                                                       data-label-text="{{__('admin.status')}}" checked/>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="card-content collapse show">
                                            <div class="card-body">
                                                @csrf
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4>
                                                                <strong>{{__('admin.day')}}:</strong>
                                                                <span id="day">
                                                                    {{__('admin.saturday')}}
                                                                </span>
                                                            </h4>
                                                            <hr>
                                                        </div>

                                                            <div class="col-md-12">
                                                                <div
                                                                    class="form-group {{ $errors->has('branch_id') ? ' has-error' : '' }}">
                                                                    <label
                                                                        for="projectinput2">{{__('admin.branch_name')}}
                                                                        <span class="danger">*</span></label>
                                                                    <br>
                                                                    <select
                                                                        class="form-control @error('branch_id') is-invalid @enderror"
                                                                        id="branch_id" required name="branch_id">
                                                                        <option
                                                                            value="">{{__('admin.select_option')}}</option>
                                                                        @foreach($branches as $branch)
                                                                            <option
                                                                                value="{{$branch->id}}">{{$branch->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @error('branch_id')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
{{--                                                        @else--}}
{{--                                                            <input type="hidden" name="branch_id"--}}
{{--                                                                   value="{{ getLoggedInadminBranchId() }}">--}}
{{--                                                        @endif--}}

                                                        <div class="col-md-12">
                                                            <div
                                                                class="form-group {{ $errors->has('start_time') ? ' has-error' : '' }}">
                                                                <label>{{__('admin.start_time')}}<span
                                                                        class="danger">*</span></label>
                                                                <input type="time"
                                                                       class="form-control @error('start_time') is-invalid @enderror"
                                                                       name="start_time"
                                                                       value="{{ old('start_time') }}">
                                                                @error('start_time')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div
                                                                class="form-group {{ $errors->has('end_time') ? ' has-error' : '' }}">
                                                                <label>{{__('admin.end_time')}}<span
                                                                        class="danger">*</span></label>
                                                                <input type="time"
                                                                       class="form-control @error('end_time') is-invalid @enderror"
                                                                       name="end_time" value="{{ old('end_time') }}">
                                                                @error('end_time')
                                                                <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-primary col-md-6 pull-right"
                                                            id="button_save">
                                                        <i class="la la-check-square-o"></i> {{__('admin.save')}}
                                                    </button>
                                                    <button type="button" class="btn btn-dark col-md-4 pull-left"
                                                            id="button_cancel">{{__('admin.cancel')}}
                                                    </button>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-12" id="index">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create working_times')
                                            <a class="btn btn-primary" id="openAddModal"> <i
                                                    class="ft ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.working_times')}}
                                            </a>
                                        @endcan
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a href="{{ url('dashboard')}}" data-action="close"><i
                                                            class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <table width="100%"
                                                       class="table zero-configuration"
                                                       id="table">
                                                    <thead>
                                                    <tr>
                                                        {{--                                                        <th>{{__('admin.updated_at')}}</th>--}}
                                                        <th>{{__('admin.week_day')}}</th>
                                                        {{--                                                        <th>{{__('admin.start_time')}}</th>--}}
                                                        {{--                                                        <th>{{__('admin.end_time')}}</th>--}}
                                                        {{--                                                        <th>{{__('admin.status')}}</th>--}}
                                                        {{--                                                        <th width="30%">{{__('admin.note')}}</th>--}}
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    {{-- start model show--}}
    <div id="confirmModalShow" class="modal fade text-left" role="dialog">
        <div class="modal-dialog long-mode">
            <div class="modal-content">
                <div class="card-content">
                    <div class="modal-header danger">
                        <h5 class="modal-title" id="myModalLabel1">
                            {{__('admin.day')}}:
                            <span id="day-name" class="color-blue"></span>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span
                            class="text-bold-600">{{__('admin.post_index')}} {{__('admin.working_times')}}</span>
                        <hr>
                        <table width="100%" class="table table-responsive table-custom-responsive"
                               id="details_table">
                            <thead style="background-color: #19365d; color: white">
                            <tr>
                                {{--                                <th>#</th>--}}
                                <th>{{__('admin.branch_name')}}</th>
                                <th>{{__('admin.start_time')}}</th>
                                <th>{{__('admin.end_time')}}</th>
                                <th>{{__('admin.status')}}</th>
                                <th>{{__('admin.time')}}</th>
                                @canany(['update working_times','delete working_times'])
                                    <th class="hide-in-tracking">{{__('admin.action')}}</th>
                                @endcanany
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <hr class="p0 m0">
                    <div class="col-md-12">
                        <small class="price category-color">{{__('admin.last_update')}}
                            :
                        </small>
                        <small class="price category-color" id="created"></small>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show--}}

    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center"
                        style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark"
                            data-dismiss="modal">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- start model Don Delete Message--}}
    <div id="messageDonDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                        <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                        <strong>{{__('admin.successfully_done')}}!</strong>
                        <p>{{__('admin.deleted_successfully')}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--end model Don Delete Message--}}

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form id="form-add-times">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title">{{__('admin.add')}}  {{__('admin.working_times')}}</h5>
                            <button type="button" class="close"
                                    data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <fieldset class="form-group col-12" id="fieldset-days"
                                      style="background: #f0f0f0; padding: 10px;border-radius: 5px;">
                                <table class="padding_right">
                                    <tr>
                                        <td width="110px">
                                            <input id="sat" type="checkbox" name="days[]" value="SATURDAY">
                                            <label for="sat">{{__('admin.saturday')}}</label>
                                        </td>
                                        <td width="110px">
                                            <input id="sun" type="checkbox" name="days[]" value="SUNDAY">
                                            <label for="sun">{{__('admin.sunday')}}</label>
                                        </td>
                                        <td width="110px">
                                            <input id="mon" type="checkbox" name="days[]" value="MONDAY">
                                            <label for="mon">{{__('admin.monday')}}</label>
                                        </td>
                                        <td width="110px">
                                            <input id="tue" type="checkbox" name="days[]" value="TUESDAY">
                                            <label for="tue">{{__('admin.tuesday')}}</label>
                                        </td>
                                        <td width="110px">
                                            <input id="wed" type="checkbox" name="days[]" value="WEDNESDAY">
                                            <label for="wed">{{__('admin.wednesday')}}</label>
                                        </td>
                                        <td width="110px">
                                            <input id="thu" type="checkbox" name="days[]" value="THURSDAY">
                                            <label for="thu">{{__('admin.thursday')}}</label>
                                        </td>
                                        <td width="110px">
                                            <input id="fri" type="checkbox" name="days[]" value="FRIDAY">
                                            <label for="fri">{{__('admin.friday')}}</label>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            <fieldset class="form-group col-md-12">
                                <span class="error-massege" id="days-error"><strong></strong></span>
                            </fieldset>
                            <fieldset class="form-group row col-md-12">
                                @if($branches->count() > 1)
                                    <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('branch_id') ? ' has-error' : '' }}">
                                            <label for="projectinput2">{{__('admin.branch_name')}}
                                                <span class="danger">*</span></label>
                                            <br>
                                            <select class="form-control @error('branch_id') is-invalid @enderror"
                                                    id="branch_id" required name="branch_id">
                                                <option value="">{{__('admin.select_option')}}</option>
                                                @foreach($branches as $branch)
                                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('branch_id')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                @elseif($branches->count() == 0)
                                    <span class="error-massege" style="margin-top: 30px">عذراً يجب توفر فرع قبل انشاء وقت عمل. </span>
                                @else
                                    <input type="hidden" name="branch_id" value="{{ getLoggedInadminBranchId() }}">
                                @endif
                                <div class="col-md-4">
                                    <label>{{__('admin.start_time')}}:<span class="danger">*</span></label>
                                    <input type="time" name="start_time" class="form-control">
                                    <span class="error-massege">
                                        <strong></strong>
                                    </span>
                                </div>
                                <div class="col-md-4">
                                    <label>{{__('admin.end_time')}}:<span class="danger">*</span></label>
                                    <input type="time" name="end_time" class="form-control">
                                    <span class="error-massege">
                                       <strong></strong>
                                    </span>
                                </div>
                            </fieldset>
                            {{-- <fieldset class="form-group col-12 span">
                                 <label>{{__('admin.note')}} (ar):</label>
                                 <textarea name="message_ar_add" rows="4" class="form-control"></textarea>
                             </fieldset>
                             <fieldset class="form-group col-12 span">
                                 <label>{{__('admin.note')}} (en):</label>
                                 <textarea name="message_en_add" id="message_en_add" rows="4" class="form-control"></textarea>
                             </fieldset>--}}
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

@endsection
@section('script')
    <script>
        let form_add = $('#form-add-times');

        $(document).on('click', '#button_save', function () {
            document.getElementById('my_form_id').submit();
            $('#button_save').attr('disabled', true).html('<i class="la la-spinner spinner"></i> {{__('admin.saving')}}...');
        });

        $('#button_cancel').click(function () {
            $("#card-create").addClass("hidden");
            $("#index").removeClass("col-md-7").addClass('col-md-12');
            $("#table").attr('style', 'width: 1160px');
        });

        $('#close-card').click(function () {
            $("#card-create").addClass("hidden");
            $("#index").removeClass("col-md-7").addClass('col-md-12');
            $("#table").attr('style', 'width: 1160px');
        });

        /*start code show all data in datatable ajax*/
        $(document).ready(function () {
            let filter;
            $(document).on('click', '.filter-day', function () {
                filter = $(this).attr('id');
                console.log(filter);
                $('#table').DataTable().ajax.reload();
            });

            $('#table').DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('working_times') }}",
                    method: 'GET',
                    data: function (d) {
                        /* todo:: custom filter goes here*/
                        if (filter !== undefined) {
                            d.filter = filter;
                        }
                    },
                },
                columns: [
                    /*{
                        data: 'updated_at',
                        name: 'updated_at',
                    },*/
                    {
                        data: 'day',
                        name: 'day',
                    },
                    /* {
                         data: 'start_time',
                         name: 'start_time',
                     },
                     {
                         data: 'end_time',
                         name: 'end_time',
                     },
                     {
                         data: 'status',
                         name: 'status',
                         render: function (data, type, full, meta) {
                             if (data == 1) {
                                 return "<div class='fonticon-wrap'><i class='ft-unlock' style='color:#002581'></i></div>"
                             } else {
                                 return "<div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>"
                             }
                         },
                     },
                     {
                         data: 'message',
                         name: 'message',
                     },*/
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    },
                ],
                "ordering": false,
                /*'order': [[0, 'desc']],*/
                /*"columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]*/
            });
        });
        /*end code show all data in datatable ajax*/


        /*start code Delete ajax*/
        $(document).on('click', '.delete', async function () {
            let working_time_id = $(this).attr('id');
            let route = "{{url('encoding/working_times/destroy')}}" + "/" + working_time_id;
            try {
                await deletedItems(working_time_id, route);
                $('#confirmModalShow').modal('hide');
                $('#table').DataTable().ajax.reload();
            } catch (e) {
                return e;
            }
        });
        /*end code Delete ajax*/

        /*start code add working times*/
        $('#form-add-times').on('submit', function (event) {
            event.preventDefault();
            let data = new FormData(this);
            let btn_submit = $('#btn-save');
            let route = "{{route('working_times.store')}}";

            $.ajax({
                url: route,
                method: "POST",
                data: data,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    form_add.find('span strong').empty();
                    $('#fieldset-days').css('border', 'none');
                    form_add.find('input[type=time]').removeClass('is-invalid');
                    btn_submit.html('<i class="la la-spinner spinner"></i>{{__('admin.saving')}}...');
                },
                success: function (data) {
                    $('#table').DataTable().ajax.reload();
                    $('#addModal').modal('hide');
                    btn_submit.html('<i class="ft-save"></i>{{__('admin.save')}}');
                    $('#messageSave p').text(data.message);

                    /*message save*/
                    $('#messageSave').modal('show');
                    setTimeout(function () {
                        $('#messageSave').modal('hide');
                    }, 3000);
                },
                error: function (data) {
                    btn_submit.html('<i class="ft-save"></i>{{__('admin.save')}}');
                    let obj = JSON.parse((data.responseText));
                    let start_time = undefined;
                    let end_time = undefined;
                    let days_error = data.responseJSON.days;

                    if (obj.error !== undefined) {
                        start_time = obj.error.start_time ?? undefined;
                        end_time = obj.error.end_time ?? undefined;
                    }

                    if (start_time !== undefined || end_time !== undefined || days_error !== undefined) {
                        if (start_time !== undefined) {
                            $('input[name=start_time]').addClass('is-invalid').parent().find('span strong').text(start_time)
                        }

                        if (end_time !== undefined) {
                            $('input[name=end_time]').addClass('is-invalid').parent().find('span strong').text(end_time)
                        }

                        if (days_error !== undefined) {
                            $('#days-error strong').text(days_error);
                            $('#fieldset-days').css('border', '1px solid red');
                        }
                    } else {
                        $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                        $('#confirm-modal-loading-show').modal('show');
                    }

                }
            });

        });
        /*end code add working times*/

        /*start code edit*/
        let frm = $('#my_form_id');
        let note_id;
        $(document).on('click', '.edit-table-row', function () {
            note_id = $(this).attr('id');
            let detail_url = "{{url('encoding/working_times/edit')}}" +"/" + note_id;
            $.ajax({
                url: detail_url,
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $('#confirmModalShow').modal('hide');
                    $("#card-create").removeClass("hidden");
                    $("#index").removeClass("col-md-12").addClass('col-md-7');
                    $("#table").attr('style', 'width: 650px');

                    $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");
                    $('#message_ar').text(data.message_ar);
                    $('#message_en').text(data.message_en);
                    $('#day').text(data.day);
                    $('select[name=branch_id]').val(data.branch_id);
                    $('input[name=start_time]').val(data.start_time);
                    $('input[name=end_time]').val(data.end_time);
                    $('input[name=method_type]').val('update');

                    if (data.is_active == 1) {
                        $('#switchBootstrap18').bootstrapSwitch('state', true);
                    } else {
                        $('#switchBootstrap18').bootstrapSwitch('state', false);
                    }

                    frm.attr('action', '{{url('encoding/working_times/update')}}'  + '/' + data.id);
                    frm.attr('method', 'POST');
                    $('#confirm-modal-loading-show').modal('hide');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code edit*/

        $(document).on('click', '#openAddModal', function () {
            $('#message_en_add').text('');
            $('#message_ar_add').empty();
            $('#form-add-times input[name=start_time]').val('');
            $('#form-add-times input[name=end_time]').val('');
            $('#form-add-times input[type=checkbox]').attr('checked', false);
            $('#addModal').modal('show');
        });

        @if(old('method_type') == 'update')
        $("#card-create").removeClass("hidden");
        $("#index").removeClass("col-md-12").addClass('col-md-7');
        $("#table").attr('style', 'width: 650px');

        $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");
        $('#message_ar').text('{{old('message_ar')}}');
        $('#message_en').text('{{old('message_en')}}');
        $('input[name=start_time]').val('{{old('start_time')}}');
        $('input[name=end_time]').val('{{old('end_time')}}');
        $('input[name=method_type]').val('update');

        frm.attr('action', '{{url('encoding/working_times/update')}}' + '/' + '{{session()->get('note_id')}}');
        frm.attr('method', 'POST');
        @endif

        @if(session('success'))
        $('#messageSave p').text("{{session('success')}}.");
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        @endif
    </script>

    <script>
        /*start code show details ajax*/
        let week_day;

        $(document).on('click', '.detail', function () {
            week_day = $(this).attr('id');
            detail_url =  "{{url('encoding/working_times/details')}}" +  "/" + week_day;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    let working_time_data = data.data;
                    let working_time;
                    $("#details_table tbody").empty();
                    $('#day-name').text(data.day);
                    $('#created').text(data.max_updated_at);
                    for (let i = 0; i < working_time_data.length; i++) {
                        working_time = working_time_data[i];
                        $("#details_table tbody").append(
                            "<tr class='" + working_time.status_color + "'>" +
                            /*"<td>" + working_time.id + "</td>" +*/
                            "<td>" + working_time.branch_name + "</td>" +
                            "<td>" + working_time.start_time + "</td>" +
                            "<td>" + working_time.end_time + "</td>" +
                            "<td>" + working_time.status + "</td>" +
                            "<td><small><i class='ft-clock'></i>" + working_time.created_at + "</small></td>" +
                            @canany(['update working_times','delete working_times'])
                                "<td>" + working_time.action + "</td>" +
                            @endcanany
                                "</tr>"
                        );
                    }

                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');

                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code show details ajax*/
    </script>

@endsection
