<script>
    let form_add = $('#form-add-times');

    $(document).on('click', '#button_save', function () {
        document.getElementById('my_form_id').submit();
        $('#button_save').attr('disabled', true).html('<i class="la la-spinner spinner"></i> {{__('admin.saving')}}...');
    });

    $('#button_cancel').click(function () {
        $("#card-create").addClass("hidden");
        $("#index").removeClass("col-md-7").addClass('col-md-12');
        $("#table").attr('style', 'width: 1160px');
    });

    $('#close-card').click(function () {
        $("#card-create").addClass("hidden");
        $("#index").removeClass("col-md-7").addClass('col-md-12');
        $("#table").attr('style', 'width: 1160px');
    });

    /*start code show all data in datatable ajax*/
    $(document).ready(function () {
        let filter;
        $(document).on('click', '.filter-day', function () {
            filter = $(this).attr('id');
            $('#table').DataTable().ajax.reload();
        });

        $('#table').DataTable({
            @if(app()->getLocale() == 'ar')
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            @endif
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('working_times') }}",
                method: 'GET',
                data: function (d) {
                    /* todo:: custom filter goes here*/
                    if (filter !== undefined) {
                        d.filter = filter;
                    }
                },
            },
            columns: [
                /*{
                    data: 'updated_at',
                    name: 'updated_at',
                },*/
                {
                    data: 'day',
                    name: 'day',
                    render: function (data, type, full, meta) {
                        return "<span>" + data + "</span>"
                    },
                },
                /* {
                     data: 'start_time',
                     name: 'start_time',
                 },
                 {
                     data: 'end_time',
                     name: 'end_time',
                 },
                 {
                     data: 'status',
                     name: 'status',
                     render: function (data, type, full, meta) {
                         if (data == 1) {
                             return "<div class='fonticon-wrap'><i class='ft-unlock' style='color:#0679f0'></i></div>"
                         } else {
                             return "<div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>"
                         }
                     },
                 },
                 {
                     data: 'message',
                     name: 'message',
                 },*/
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                },
            ],
            "ordering": false,
            /*'order': [[0, 'desc']],*/
            /*"columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }
            ]*/
        });
    });
    /*end code show all data in datatable ajax*/

    @can('delete working_times')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let working_time_id = $(this).attr('id');
        let route = "{{url('encoding/working_times/destroy')}}" + "/" + working_time_id;
        try {
            await deletedItems(working_time_id, route);
            $('#confirmModalShow').modal('hide');
            $('#table').DataTable().ajax.reload();
        } catch (e) {
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan

    @can('create working_times')
    /*start code add working times*/
    $('#form-add-times').on('submit', function (event) {
        event.preventDefault();
        let data = new FormData(this);
        let btn_submit = $('#btn-save');
        let route = "{{route('working_times.store')}}";

        $.ajax({
            url: route,
            method: "POST",
            data: data,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                form_add.find('span strong').empty();
                $('#fieldset-days').css('border', 'none');
                form_add.find('input[type=time]').removeClass('is-invalid');
                btn_submit.html('<i class="la la-spinner spinner"></i>{{__('admin.saving')}}...');
            },
            success: function (data) {
                $('#table').DataTable().ajax.reload();
                $('#addModal').modal('hide');
                btn_submit.html('<i class="ft-save"></i>{{__('admin.save')}}');
                $('#messageSave p').text(data.message);

                /*message save*/
                $('#messageSave').modal('show');
                setTimeout(function () {
                    $('#messageSave').modal('hide');
                }, 3000);
            },
            error: function (data) {
                btn_submit.html('<i class="ft-save"></i>{{__('admin.save')}}');
                let obj = JSON.parse((data.responseText));
                let start_time = undefined;
                let end_time = undefined;
                let days_error = data.responseJSON.days;

                if (obj.error !== undefined) {
                    start_time = obj.error.start_time ?? undefined;
                    end_time = obj.error.end_time ?? undefined;
                }

                if (start_time !== undefined || end_time !== undefined || days_error !== undefined) {
                    if (start_time !== undefined) {
                        $('input[name=start_time]').addClass('is-invalid').parent().find('span strong').text(start_time)
                    }

                    if (end_time !== undefined) {
                        $('input[name=end_time]').addClass('is-invalid').parent().find('span strong').text(end_time)
                    }

                    if (days_error !== undefined) {
                        $('#days-error strong').text(days_error);
                        $('#fieldset-days').css('border', '1px solid red');
                    }
                } else {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }

            }
        });

    });
    /*end code add working times*/
    @endcan

    @can('update working_times')
    /*start code edit*/
    let frm = $('#my_form_id');
    let note_id;
    $(document).on('click', '.edit-table-row', function () {
        note_id = $(this).attr('id');
        let detail_url = "{{url('encoding/working_times/edit')}}" + "/" + note_id;
        $.ajax({
            url: detail_url,
            beforeSend: function () {
                $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                $('#confirm-modal-loading-show').modal('show');
            },
            success: function (data) {
                $('#confirmModalShow').modal('hide');
                $("#card-create").removeClass("hidden");
                $("#index").removeClass("col-md-12").addClass('col-md-7');
                $("#table").attr('style', 'width: 650px');

                $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");
                $('#message_ar').text(data.message_ar);
                $('#message_en').text(data.message_en);
                $('#day').text(data.day);
                $('input[name=start_time]').val(data.start_time);
                $('input[name=end_time]').val(data.end_time);
                $('input[name=method_type]').val('update');

                if (data.is_active == 1) {
                    $('#switchBootstrap18').bootstrapSwitch('state', true);
                } else {
                    $('#switchBootstrap18').bootstrapSwitch('state', false);
                }

                frm.attr('action', '{{url('encoding/working_times/update')}}' + '/' + data.id);
                frm.attr('method', 'POST');
                $('#confirm-modal-loading-show').modal('hide');
            },
            error: function (data) {
                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                $('#confirm-modal-loading-show').modal('show');
            }
        })
    });
    /*end code edit*/
    @endcan

    @can('create working_times')
    $(document).on('click', '#openAddModal', function () {
        $('#message_en_add').text('');
        $('#message_ar_add').empty();
        $('#form-add-times input[name=start_time]').val('');
        $('#form-add-times input[name=end_time]').val('');
        $('#form-add-times input[type=checkbox]').attr('checked', false);
        $('#addModal').modal('show');
    });
    @endcan

    @if(old('method_type') == 'update')
    $("#card-create").removeClass("hidden");
    $("#index").removeClass("col-md-12").addClass('col-md-7');
    $("#table").attr('style', 'width: 650px');

    $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");
    $('#message_ar').text('{{old('message_ar')}}');
    $('#message_en').text('{{old('message_en')}}');
    $('input[name=start_time]').val('{{old('start_time')}}');
    $('input[name=end_time]').val('{{old('end_time')}}');
    $('input[name=method_type]').val('update');

    frm.attr('action', '{{url('encoding/working_times/update')}}' + '/' + '{{session()->get('note_id')}}');
    frm.attr('method', 'POST');
    @endif

    @if(session('success'))
    $('#messageSave p').text("{{session('success')}}.");
    $('#messageSave').modal('show');
    setTimeout(function () {
        $('#messageSave').modal('hide');
    }, 3000);
    @endif

    /*start code show details ajax*/
    let week_day;

    $(document).on('click', '.detail', function () {
        week_day = $(this).attr('id');
        detail_url = "{{url('encoding/working_times/details')}}" + "/" + week_day;
        $.ajax({
            type: 'GET',
            url: detail_url,
            dataType: "json",
            beforeSend: function () {
                $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                $('#confirm-modal-loading-show').modal('show');
            },
            success: function (data) {
                let working_time_data = data.data;
                let working_time;
                $("#details_table tbody").empty();
                $('#day-name').text(data.day);
                $('#created').text(data.max_updated_at);
                for (let i = 0; i < working_time_data.length; i++) {
                    working_time = working_time_data[i];
                    $("#details_table tbody").append(
                        "<tr class='" + working_time.status_color + "'>" +
                        /*"<td>" + working_time.id + "</td>" +*/
                        "<td>" + working_time.start_time + "</td>" +
                        "<td>" + working_time.end_time + "</td>" +
                        "<td>" + working_time.status + "</td>" +
                        "<td><small><i class='ft-clock'></i>" + working_time.created_at + "</small></td>" +
                        @canany(['update working_times', 'delete working_times'])
                            "<td>" + working_time.action + "</td>" +
                        @endcanany
                            "</tr>"
                    );
                }

                $('#confirm-modal-loading-show').modal('hide');
                $('#confirmModalShow').modal('show');

            },
            error: function (data) {
                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                $('#confirm-modal-loading-show').modal('show');
            }
        })
    });
    /*end code show details ajax*/
</script>
