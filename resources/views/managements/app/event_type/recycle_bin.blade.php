@php($page_title = __('admin.MOKASWEETS').' | '.__('admin.event_type'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.event_type')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">{{__('admin.Dashboard')}}</a>
                            </li>
                            <li class="breadcrumb-item "><a
                                    href="{{route('event_type')}}">{{__('admin.event_type')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.recycle_bin')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.recycle_bin')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <form id="frm-example" action="/path/to/your/script.php" method="POST">
                                                    <table width="100%"
                                                           class="table  zero-configuration"
                                                           id="user_table">
                                                        <thead>
                                                        <tr>
                                                            <th>{{__('admin.id')}}</th>
                                                            <th>{{__('admin.name')}}</th>
                                                            <th>{{__('admin.notification_title')}}</th>
                                                            <th>{{__('admin.notification_message')}}</th>
                                                            <th>{{__('admin.action')}}</th>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            {{--start model Message Restore--}}
            <div id="confirmModalRestore" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header danger">
                            <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4 align="center"
                                style="margin:0;">{{__('admin.are_you_sure_you_want_to_restore_this_data')}}</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" name="ok_button_restore" id="ok_button_restore"
                                    class="btn btn-primary">{{__('admin.yes')}}</button>
                            <button type="button" class="btn btn-dark"
                                    data-dismiss="modal">{{__('admin.cancel')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            {{--end model Message Restore--}}

            {{-- start model Don Restore Message--}}

            <div id="messageDonRestor" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-primary alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="ft ft-rotate-cw"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{__('admin.restored_successfully')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--end model Don Restor Message--}}

            {{-- start model check Delete Message--}}
            <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4 align="center"
                                style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" name="ok_button" id="ok_button"
                                    class="btn btn-danger">{{__('admin.yes')}}</button>
                            <button type="button" class="btn btn-dark"
                                    data-dismiss="modal">{{__('admin.cancel')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- end model check Delete Message--}}

            {{-- start model Don Delete Message--}}
            <div id="messageDonDelete" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                                <strong id="title-delete">{{__('admin.successfully_done')}}!</strong>
                                <p id="message-delete">{{__('admin.deleted_successfully')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--end model Don Delete Message--}}

            @endsection
            @section('script')
                <script>
                    /*start code show all data in datatable ajax*/

                    $(document).ready(function () {
                        $('#user_table').DataTable({
                            @if(app()->getLocale() == 'ar')
                            "language": {
                                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                            },
                            @endif
                            processing: true,
                            serverSide: true,
                            ajax: {
                                url: "{{ route('event_type.recycle_bin') }}",
                            },
                            columnDefs: [
                                {
                                    'targets': 0,
                                    'checkboxes': {
                                        'selectRow': true
                                    }
                                }
                            ],
                            columns: [
                                {
                                    data: 'event_type_id',
                                    name: 'event_type_id',
                                    class: 'event_type_id'
                                },
                                {
                                    data: 'name',
                                    name: 'name',
                                    class: 'name'
                                },
                                {
                                    data: 'notification_title',
                                    name: 'notification_title',
                                    class: 'notification_title',
                                },
                                {
                                    data: 'notification_message',
                                    name: 'notification_message',
                                    class: 'notification_message',
                                },
                                {
                                    data: 'action',
                                    name: 'action',
                                    orderable: false
                                }
                            ]
                        });
                    });
                    /*end code show all data in datatable ajax*/

                    /*start code restore ajax*/
                    var restore_id;
                    var detail_url;

                    $(document).on('click', '.restore', function () {
                        restore_id = $(this).attr('id');
                        $('#confirmModalRestore').modal('show');
                    });

                    $('#ok_button_restore').click(function () {
                        $.ajax({
                            type: 'GET',
                            url: "restore/" + restore_id,
                            dataType: "json",
                            beforeSend: function () {
                                $('#ok_button_restore').text("{{__('admin.restoring')}}...");
                            },
                            success: function (data) {
                                setTimeout(function () {
                                        $('#confirmModalRestore').modal('hide');
                                        $('#user_table').DataTable().ajax.reload();
                                        $('#ok_button_restore').text("{{__('admin.yes')}}");
                                    }, 500);
                                $('#messageDonRestor').modal('hide');
                                setTimeout(function () {
                                    $('#messageDonRestor').modal('show');
                                }, 510,);
                                setTimeout(function () {
                                    $('#messageDonRestor').modal('hide');
                                }, 3000,);
                            },
                        })
                    });
                    /*end code restore ajax*/

                    /*start code Delete ajax*/
                    var $event_type_id;

                    $(document).on('click', '.delete', function () {
                        $event_type_id = $(this).attr('id');
                        $('#confirmModalDelete').modal('show');
                    });

                    $('#ok_button').click(function () {
                        console.log('click end');
                        $.ajax({
                            url: "hdelete/" + $event_type_id,
                            beforeSend: function () {
                                $('#ok_button').text("{{__('admin.deleting')}}...");
                            },
                            success: function (data) {
                                if (data.error_delete != undefined) {
                                    $('#title-delete').text("{{__('admin.error_message')}}");
                                    $('#message-delete').text(data.error_delete);
                                    $('#messageDonDelete').modal('show');
                                    setTimeout(function () {
                                        $('#messageDonDelete').modal('hide');
                                    }, 3000,);
                                    $('#ok_button').text('{{__('admin.yes')}}');
                                } else {
                                    setTimeout(function () {
                                        $('#title-delete').text("{{__('admin.successfully_done')}}");
                                        $('#message-delete').text("{{__('admin.deleted_successfully')}}.");
                                        $('#confirmModalDelete').modal('hide');
                                        $('#user_table').DataTable().ajax.reload();
                                        $('#ok_button').text('{{__('admin.yes')}}');
                                    }, 500);
                                    $('#messageDonDelete').modal('hide');
                                    setTimeout(function () {
                                        $('#messageDonDelete').modal('show');
                                    }, 510,);
                                    setTimeout(function () {
                                        $('#messageDonDelete').modal('hide');
                                    }, 3000,);
                                    $('#confirmModalShow').modal('hide');
                                }
                            },
                            error: function (data) {
                                console.log('click failed');
                            }
                        })
                    });
                    /*end code Delete ajax*/
                </script>
@endsection
