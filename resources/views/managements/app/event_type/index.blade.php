@php($page_title = __('admin.event_type'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.event_type')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">{{__('admin.Dashboard')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.event_type')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                <div class="btn-group float-md-right">
                    @can('delete event_type')
                        <a href="{{url('/app/event_type/recycle_bin')}}" class="btn btn-primary"
                           style="color: white">{{__('admin.recycle_bin')}} <i class="ft-trash position-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                @canany('create event_type', 'update event_type')
                    <div id="card-create" class="col-md-5">
                        <section id="basic-form-layouts">
                            <div class="row match-height">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title"
                                                id="basic-layout-form">{{__('admin.create')}} {{__('admin.event_type')}}</h4>
                                            <a class="heading-elements-toggle"><i
                                                    class="la la-ellipsis-v font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                    <li><a data-action="close" id="close-card"><i class="ft-x"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-content collapse show">
                                            <div class="card-body">
                                                <form class="form" action="{{route('event_type.store')}}"
                                                      id="my_form_id" method="POST">
                                                    {{csrf_field()}}
                                                    <div class="form-body">
                                                        <div class="row">

                                                            <div class="col-md-12">
                                                                <div
                                                                    class="form-group {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                                                    <label for="projectinput2">{{__('admin.name')}} (ar)
                                                                        <span class="danger">*</span></label>
                                                                    <input type="text" id="edit_name_ar"
                                                                           value="{{ old('ar_name') }}" required
                                                                           class="form-control @error('ar_name') is-invalid @enderror"
                                                                           name="ar_name" autocomplete="ar_name"
                                                                           autofocus>
                                                                    @error('ar_name')
                                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div
                                                                    class="form-group {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                                                    <label for="projectinput2">{{__('admin.name')}} (en)
                                                                        <span class="danger">*</span></label>
                                                                    <input type="text" id="edit_name_en"
                                                                           value="{{ old('en_name') }}" required
                                                                           class="form-control @error('en_name') is-invalid @enderror"
                                                                           name="en_name">
                                                                    @error('en_name')
                                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div
                                                                    class="form-group {{ $errors->has('ar_notification_title') ? ' has-error' : '' }}">
                                                                    <label
                                                                        for="projectinput2">{{__('admin.notification_title')}}
                                                                        (ar) <span class="danger">*</span></label>
                                                                    <input type="text" id="edit_notification_title_ar"
                                                                           value="{{ old('ar_notification_title') }}"
                                                                           required
                                                                           class="form-control @error('ar_notification_title') is-invalid @enderror"
                                                                           name="ar_notification_title">
                                                                    @error('ar_notification_title')
                                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div
                                                                    class="form-group {{ $errors->has('en_notification_title') ? ' has-error' : '' }}">
                                                                    <label
                                                                        for="projectinput2">{{__('admin.notification_title')}}
                                                                        (en) <span class="danger">*</span></label>
                                                                    <input type="text" id="edit_notification_title_en"
                                                                           value="{{ old('en_notification_title') }}"
                                                                           required
                                                                           class="form-control @error('en_notification_title') is-invalid @enderror"
                                                                           name="en_notification_title">
                                                                    @error('en_notification_title')
                                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div
                                                                    class="form-group {{ $errors->has('ar_notification_message') ? ' has-error' : '' }}">
                                                                    <label
                                                                        for="projectinput2">{{__('admin.notification_message')}}
                                                                        (ar) <span class="danger">*</span></label>
                                                                    <input type="text" id="edit_notification_message_ar"
                                                                           value="{{ old('ar_notification_message') }}"
                                                                           required
                                                                           class="form-control @error('ar_notification_message') is-invalid @enderror"
                                                                           name="ar_notification_message">
                                                                    @error('ar_notification_message')
                                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div
                                                                    class="form-group {{ $errors->has('en_notification_message') ? ' has-error' : '' }}">
                                                                    <label
                                                                        for="projectinput2">{{__('admin.notification_message')}}
                                                                        (en) <span class="danger">*</span></label>
                                                                    <input type="text" id="edit_notification_message_en"
                                                                           value="{{ old('en_notification_message') }}"
                                                                           required
                                                                           class="form-control @error('en_notification_message') is-invalid @enderror"
                                                                           name="en_notification_message">
                                                                    @error('en_notification_message')
                                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <button type="submit" id="button_save"
                                                                class="btn btn-primary col-md-12">
                                                            <i class="ft-save"></i> {{__('admin.save')}}
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                @endcanany
                <div id="card-index" class="col-md-7">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.post_index')}} {{__('admin.event_type')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <form id="frm-example" action="{{url('/app/event_type/dele/{id}')}}"
                                                      method="POST">
                                                    <table width="100%"
                                                           class="display table  zero-configuration"
                                                           id="user_table" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th hidden>{{__('admin.updated_at')}}</th>
                                                            <th width="30%">{{__('admin.name')}}</th>
                                                            <th width="40%">{{__('admin.notification_title')}}</th>
                                                            {{--                                                    <th>{{__('admin.notification_message')}}</th>--}}
                                                            <th width="30%">{{__('admin.action')}}</th>
                                                        </tr>
                                                        </thead>
                                                    </table>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

    {{-- start model show  Message--}}
    <div id="confirmModalShow" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="form-details  modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"
                        id="myModalLabel1">{{__('admin.show')}} {{__('admin.event_types')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form form-bordered form-horizontal">
                                <div class="form-body">
                                    <hr>
                                    <div class="form-group row">
                                        <label class="col-md-4 label-control"
                                               style="font-weight: bold">{{__('admin.id')}}</label>
                                        <div class="col-md-8 mx-auto">
                                            <p class="id"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 label-control" for="projectinput1"
                                               style="font-weight: bold">{{__('admin.name')}}</label>
                                        <div class="col-md-8 mx-auto">
                                            <p class="name"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 label-control" for="projectinput2"
                                               style="font-weight: bold">{{__('admin.notification_title')}}</label>
                                        <div class="col-md-8 mx-auto">
                                            <p class="notification_title"></p>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 label-control" for="projectinput3"
                                               style="font-weight: bold">{{__('admin.notification_message')}}</label>
                                        <div class="col-md-8 mx-auto">
                                            <p class="notification_message"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 label-control"
                                               style="font-weight: bold">{{__('admin.updated_at')}}</label>
                                        <div class="col-md-8 mx-auto">
                                            <p class="updated_at_c"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 label-control"
                                               style="font-weight: bold">{{__('admin.created_at')}}</label>
                                        <div class="col-md-8 mx-auto">
                                            <p class="created_at_c"></p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="btn111 modal-footer">
                </div>
            </div>
        </div>
    </div>
    {{-- end model show  Message--}}

    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center"
                        style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark"
                            data-dismiss="modal">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- start model Don Delete Message--}}
    <div id="messageDonDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                        <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                        <strong id="title-delete">{{__('admin.successfully_done')}}!</strong>
                        <p id="message-delete">{{__('admin.deleted_successfully')}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--end model Don Delete Message--}}
@endsection
@section('script')
    <script> $('#close-card').click(function () {
            $("#card-create").addClass("hidden");
            $("#card-index").removeClass("col-md-7").addClass('col-md-12');
            $("#user_table").attr('style', 'width: 1160px');
        });
        @can('create event_type') $('#card-create').attr('hidden', false);
        $('#card-index').addClass('col-md-7');
        $('#card-index').removeClass('col-md-12');
        @else $(document).ready(function () {
            $('#card-create').attr('hidden', true);
            $('#card-index').addClass('col-md-12');
            $('#card-index').removeClass('col-md-7');
        }); @endcan</script>
    <script type="text/javascript" src="{{asset('js/datatable_check/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/datatable_check/dataTables.checkboxes.min.js')}}"></script>
    <script> $(document).ready(function () {
            $('#user_table').DataTable({
                @if(app()->getLocale() == 'ar') "language": {"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"},
                @endif processing: true,
                serverSide: true,
                ajax: {url: "{{ route('event_type') }}"},
                columns: [{data: 'updated_at', name: 'updated_at', class: 'updated_at'}, {
                    data: 'name',
                    name: 'name',
                    class: 'name'
                }, {
                    data: 'notification_title',
                    name: 'notification_title',
                    class: 'notification_title',
                }, {data: 'action', name: 'action', orderable: false}],
                'order': [[0, 'desc']],
                "columnDefs": [{"targets": [0], "visible": false, "searchable": false}]
            });
        });
        $('#messageSave').modal('show');
        setTimeout(function () {
                        $('#messageSave').modal('hide');
                    }, 3000);

                    var $event_type_id;
                    $(document).on('click', '.delete', function () {
                        $event_type_id = $(this).attr('id');
                        $('#confirmModalDelete').modal('show');
                    });

                    $('#ok_button').click(function () {
                        console.log('click end');
                        $.ajax({
                            url: "{{url('/app/event_type')}}" + "/destroy/" + $event_type_id,
                            beforeSend: function () {
                                    $('#ok_button').text('{{__('admin.deleting')}}...');
                            },
                            success: function (data) {
                                $('#button_save').html("<i class='ft-save'></i> {{__('admin.save')}}");
                                document.getElementById('edit_name_ar').value = '';
                                document.getElementById('edit_name_en').value = '';
                                document.getElementById('edit_notification_title_ar').value = '';
                                document.getElementById('edit_notification_title_en').value = '';
                                document.getElementById('edit_notification_message_ar').value = '';
                                document.getElementById('edit_notification_message_en').value = '';
                                frm.attr('action', '{{url('/app/event_type/store')}}');
                                if (data.error_delete != undefined) {
                                    $('#title-delete').text("{{__('admin.error_message')}}");
                                    $('#message-delete').text(data.error_delete);
                                    $('#messageDonDelete').modal('show');
                                    setTimeout(function () {
                                        $('#messageDonDelete').modal('hide');
                                    }, 3000,);
                                    $('#ok_button').text('{{__('admin.yes')}}');
                                } else {
                                    setTimeout(function () {
                                        $('#title-delete').text("{{__('admin.successfully_done')}}");
                                        $('#message-delete').text("{{__('admin.deleted_successfully')}}.");
                                        $('#confirmModalDelete').modal('hide');
                                        $('#user_table').DataTable().ajax.reload();
                                        $('#ok_button').text('{{__('admin.yes')}}');
                                    }, 500);
                                    $('#messageDonDelete').modal('hide');
                                    setTimeout(function () {
                                        $('#messageDonDelete').modal('show');
                                    }, 510,);
                                    setTimeout(function () {
                                        $('#messageDonDelete').modal('hide');
                                    }, 3000,);
                                    $('#confirmModalShow').modal('hide');
                                }
                            },
                            error: function (data) {
                                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                                $('#confirm-modal-loading-show').modal('show');
                            }
                        })
                    });

                    var event_type_detals_id;


                    $(document).on('click', '.showdetail', function () {
                        event_type_detals_id = $(this).attr('id');
                        detail_url = "/app/event_type/show/" + event_type_detals_id;
                        console.log(detail_url);
                        console.log('click show');
                        $.ajax({
                            type: 'GET',
                            url: detail_url,
                            dataType: "json",
                            beforeSend: function () {
                                $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                                $('#confirm-modal-loading-show').modal('show');
                            },
                            success: function (data) {
                                $('.form-details .name').text(data.name);
                                $('.form-details .id').text(data.id);
                                $('.form-details .notification_title').text(data.notification_title);
                                $('.form-details .notification_message').text(data.notification_message);
                                $('.form-details .created_at_c').text(data.created_at_e);
                                $('.form-details .updated_at_c').html(data.updated_at_e);
                                $('#confirm-modal-loading-show').modal('hide');
                                $('#confirmModalShow').modal('show');
                            },
                            error: function (data) {
                                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                                $('#confirm-modal-loading-show').modal('show');
                            }
                        })
                    });

                    @can('update event_type')


                    var frm = $('#my_form_id');
                    var create_id;
                    $(document).on('click', '.edit-table-row', function () {
                        $('#card-create').attr('hidden', false);
                        $('#card-index').addClass('col-md-7');
                        $('#card-index').removeClass('col-md-12');
                        console.log('button mohammed click');
                        create_id = $(this).attr('id');
                        detail_url = "/app/event_type/edit/" + create_id;
                        console.log(detail_url);
                        console.log('click show');
                        $.ajax({
                            type: 'GET',
                            url: detail_url,
                            dataType: "json",
                            beforeSend: function () {
                                $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                                $('#confirm-modal-loading-show').modal('show');
                            },
                            success: function (data) {
                                $('#confirm-modal-loading-show').modal('hide');
                                $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");
                                document.getElementById('edit_name_ar').value = data.edit_name_ar;
                                document.getElementById('edit_name_en').value = data.edit_name_en;
                                document.getElementById('edit_notification_title_ar').value = data.edit_notification_title_ar;
                                document.getElementById('edit_notification_title_en').value = data.edit_notification_title_en;
                                document.getElementById('edit_notification_message_ar').value = data.edit_notification_message_ar;
                                document.getElementById('edit_notification_message_en').value = data.edit_notification_message_en;
                                frm.attr('action', '{{url('/app/event_type/update')}}' + '/' + data.edit_id);
                                frm.attr('method', 'POST');
                            },
                            error: function (data) {
                                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                                $('#confirm-modal-loading-show').modal('show');
                            }
                        })
                    });

                    @if(old('method') == 'update')
                    $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");
                    document.getElementById('edit_name_ar').value = "{{old('ar_name')}}";
                    document.getElementById('edit_name_en').value = "{{old('en_name')}}";
                    document.getElementById('edit_notification_title_ar').value = "{{old('ar_notification_title')}}";
                    document.getElementById('edit_notification_title_en').value = "{{old('en_notification_title')}}";
                    document.getElementById('edit_notification_message_ar').value = "{{old('ar_notification_message')}}";
                    document.getElementById('edit_notification_message_en').value = "{{old('en_notification_message')}}";
                    frm.attr('action', '{{url('/app/event_type/update')}}' + '/' + "{{old('id')}}");
                    frm.attr('method', 'POST');
                    @endif
                    @endcan
                </script>
@endsection
