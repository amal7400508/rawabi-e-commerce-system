<script>
    /*start code show details edit ajax*/
    $(document).on('click', '.show-detail', async function () {
        let attention_id = $(this).attr('id');
        deposit_row = $(this).parent().parent();
        let url = "{{url('app/attentions/show')}}" + "/" + attention_id;

        try {
            let data = await responseEditOrShowData(url);
            if (data.status == 1) {
                $('#status').html('<span class="btn btn-primary position-absolute round" style="margin:10px;" ><i class="ft-unlock"></i></span>');
            } else {
                $('#status').html('<span class="btn btn-danger position-absolute round" style="margin:10px;" ><i class="ft-lock"></i></span>');
            }
            $('#show-image').attr('src', '{{asset('/storage/alert/')}}' + '/' + data.image);
            $('#title').text(data.title);
            $('#content').text(data.content);
            $('#start_date_td').text(data.start_date_carbon);
            $('#end_date_td').text(data.end_date_carbon);
            $('#branch_td').text(data.branch);

            $('#created').text(data.created_at);
            $('#updated_at').html(data.updated_at);
            $('#confirm-modal-loading-show').modal('hide');
            $('#confirmModalShow').modal('show');

        } catch (error) {
            return error;
        }
    });
    /*end code show details edit ajax*/
</script>
