<script>
    let table_show = $('#table-show');
    let table_count = $('#table-count');

    function delete_img() {
        document.getElementById('edit_image_ar').value = null;
        $('#avatar').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
    }

    function loadAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                var image = document.getElementById('avatar');
                image.src = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    @can('create attentions')
    /*start code open add modal*/
    $(document).on('click', '#openAddModal', function () {
        edit_row = $(this).parent().parent();
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);
        delete_img();
        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('attentions.store')}}")
            .attr("data_type", "add");
    });
    /*start code open add modal*/
    @endcan

    @can('update attentions')
    /*start code edit*/
    let attention_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        attention_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('app/attentions/edit')}}" + '/' + attention_id;
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#avatar').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('app/attentions/update')}}" + '/' + attention_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);

            $('#edit_image_ar').attr('required', false);
            $('#label-image').text("{{__('admin.image')}}");
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            document.getElementById('title_ar').value = data.title_ar;
            document.getElementById('title_en').value = data.title_en;
            document.getElementById('content_ar').value = data.content_ar;
            document.getElementById('content_en').value = data.content_en;
            document.getElementById('start_date').value = data.start_date;
            document.getElementById('end_date').value = data.end_date;
            $('#avatar').attr('src', data.image).attr('hidden', false);
            $('#delete-img').attr('hidden', false);

            if (data.status == 1) {
                $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                    .bootstrapSwitch('state', true);
            } else {
                $('#switchBootstrap18').attr('value', 1).attr('checked', false)
                    .bootstrapSwitch('state', false);
            }

            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/
    @endcan

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('attention_id', attention_id);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                /*let response_data = JSON.parse((data.responseText)).category;*/
                let response_data = data.attention;

                $('#addModal').modal('hide');

                let status, tr_color_red = '';
                if (response_data.status == 1) {
                    status = "<div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>";
                } else {
                    tr_color_red = 'tr-color-red';
                    status = "<div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>";
                }
                let content = response_data.content == null ? '' : response_data.content;

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td class='font-default'>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.title + "</td>");
                let col3 = $("<td>" + content + "</td>");
                let col4 = $("<td>" + response_data.start_date + "</td>");
                let col5 = $("<td>" + response_data.end_date + "</td>");
                let col6 = $("<td>" + status + "</td>");
                let col7 = $("<td>" + response_data.buttons_index + "</td>");

                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5, col6, col7).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5, col6, col7);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.title_ar !== undefined) {
                $('#form input[name=title_ar]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.title_ar);
            }
            if (obj.title_en !== undefined) {
                $('#form input[name=title_en]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.title_en);
            }
            if (obj.content_ar !== undefined) {
                $('#form textarea[name=content_ar]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.content_ar);
            }
            if (obj.content_en !== undefined) {
                $('#form textarea[name=content_en]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.content_en);
            }
            if (obj.start_date !== undefined) {
                $('#form input[name=start_date]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.start_date);
            }
            if (obj.end_date !== undefined) {
                $('#form input[name=end_date]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.end_date);
            }
            if (obj.image !== undefined) {
                $('#form input[name=image]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.image);
            }
        }
    });
    /*end code add or update*/

    @can('delete attentions')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('app/attentions/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            this_row.remove();
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan

    const firebaseConfig = {
            apiKey: "AIzaSyBrHGWSz2oHgXc8JtMwR7mTEHxf0P1Srrs",
            authDomain: "alrwabimainapp.firebaseapp.com",
            projectId: "alrwabimainapp",
            storageBucket: "alrwabimainapp.appspot.com",
            messagingSenderId: "355341060736",
            appId: "1:355341060736:web:6c57607285a218d222b465",
            measurementId: "G-3X6P4ZC60D"
        };
</script>
