{{-- start model show  Message--}}
<div id="confirmModalShow" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="card-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                </button>
                <div class="form-details card-body row">
                    <div class="user-display col-4" style="    padding-right: 30px;    padding-top: 10px;">
                        <img src="" class="rounded-circle img-thumbnail img-user-show-100" id="user-image">
                    </div>
                    <div class="user-display col-8 p-0">
                        <h2 class="name product-title"
                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif padding-top: 25px;"></h2>
                        <h6 class="email product-title font-default"
                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                        <h6 class="phone product-title font-default"
                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                    </div>
                </div>
                <table class="table mb-0">
                    <tr>
                        <th>{{__('admin.id')}}</th>
                        <td id="td-id"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.amount')}}</th>
                        <td id="td-amount"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.branch_name')}}</th>
                        <td id="td-branch_name"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.status')}}</th>
                        <td id="td-is_approved"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.note')}}</th>
                        <td id="td-note"></td>
                    </tr>
                </table>
                <div class="row p-1">
                    <div class="col-sm-12">
                        <span class="details price" style="color: #0679f0"></span>
                    </div>
                    <div class="col-sm-9">
                        <small class="price category-color">{{__('admin.created_at')}}:</small>
                        <small class="price category-color" id="created"></small>
                        <br>
                        <small class="price category-color">{{__('admin.updated_at')}}:</small>
                        <small class="price category-color" id="updated_at"></small>
                    </div>
                    <div class="col-sm-3 text-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model show  Message--}}
