<script>
    $(document).on('click', '.show-detail', async function () {
        let deposit_id = $(this).attr('id');
        let url = "{{url('app/deposits/show')}}" + "/" + deposit_id;
        try {
            let data_response = await responseEditOrShowData(url);
            $('.form-details .name').text(data_response.name);
            $('.form-details .email').text(data_response.email);
            $('.form-details .phone').text(data_response.phone);
            if (data_response.image != null)
                $('#user-image').attr('src', '{{asset('/storage')}}' + '/' + data_response.image);
            else $('#user-image').attr('src', '{{asset('/storage/admins/150/avatar.jpg')}}');

            $('#td-id').text(data_response.id);
            $('#td-amount').text(data_response.amount);
            $('#td-branch_name').text(data_response.branch_name);
            $('#td-note').text(data_response.note);
            $('#td-is_approved').text(data_response.is_approved);

            $('#created').text(data_response.created_at);
            $('#updated_at').html(data_response.updated_at);

            $('#confirmModalShow').modal('show');

        } catch (e) {
            return e;
        }
    });
</script>
