@php($page_title =__('admin.user_occasions'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.user_occasions')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">{{__('admin.Dashboard')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.user_occasions')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
        </div>
        <div class="col-md-12">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{__('admin.post_index')}} {{__('admin.user_occasions')}}</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload" id="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a href="{{url('/dashboard')}}" data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table width="100%"
                                               class="table  zero-configuration"
                                               id="user_table">
                                            <thead>
                                            <tr>
                                                <th>{{__('admin.user_name')}}</th>
                                                <th>{{__('admin.name')}}</th>
                                                <th>{{__('admin.date')}}</th>
                                                <th>{{__('admin.event_type')}}</th>
                                                <th>{{__('admin.action')}}</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

    {{-- start model show  --}}
    <div id="confirmModalShow" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="  modal-content">
                <div class="card pull-up">
                    <div class="card-content">
                        <div class="card-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                            </button>
                            <h3>{{__('admin.post_index')}} {{__('admin.user_occasions')}}</h3>
                            <hr>
                            <div class="form-details card-body ">
                                <div class="col-md-12">
                                    <form class="form form-bordered form-horizontal">
                                        <div class="form-body">
                                            <hr>
                                            <div class="form-group row">
                                                <label class="col-md-4 label-control"
                                                       style="font-weight: bold">{{__('admin.id')}}</label>
                                                <div class="col-md-8 mx-auto">
                                                    <p id="id"></p>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 label-control"
                                                       style="font-weight: bold">{{__('admin.user_name')}}</label>
                                                <div class="col-md-8 mx-auto">
                                                    <p id="user_name"></p>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 label-control"
                                                       style="font-weight: bold">{{__('admin.name')}}</label>
                                                <div class="col-md-8 mx-auto">
                                                    <p id="name"></p>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 label-control"
                                                       style="font-weight: bold">{{__('admin.date')}}</label>
                                                <div class="col-md-8 mx-auto">
                                                    <p id="date"></p>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 label-control"
                                                       style="font-weight: bold">{{__('admin.event_type')}}</label>
                                                <div class="col-md-8 mx-auto">
                                                    <p id="event_type_translations_name"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="row p-1">
                                    <div class="col-sm-9">
                                        <small class="price category-color">{{__('admin.created_at')}}:</small>
                                        <small class="price category-color" id="created"></small>
                                        <br>
                                        <small class="price category-color">{{__('admin.updated_at')}}:</small>
                                        <small class="price category-color" id="updated_at"></small>
                                    </div>
                                    <div class="btn_dele col-sm-3 text-center">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show  --}}
@endsection
@section('script')
    <script>
        /*Reload the data and display it in the datatable*/
        @include('include.reload-datatable')

        /*start code show all data in datatable ajax*/
        $(document).ready(function () {
            $('#user_table').DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('user_event') }}",
                },
                columns: [
                    {
                        data: 'user_name',
                        name: 'user_name',
                        class: 'user_name',
                    },
                    {
                        data: 'name',
                        name: 'name',
                        class: 'name'
                    },
                    {
                        data: 'date',
                        name: 'date',
                        class: 'date',
                    },
                    {
                        data: 'event_type_translations_name',
                        name: 'event_type_translations_name',
                        class: 'event_type_translations_name',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    }
                ]
            });
        });
        /*end code show all data in datatable ajax*/

        /*start code show details ajax*/
        var user_event_detals_id;
        $(document).on('click', '.showB', function () {
            user_event_detals_id = $(this).attr('id');
            detail_url = "/app/user_event/show/" + user_event_detals_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $('#name').text(data.name);
                    $('#id').text(data.id);
                    $('#date').text(data.date);
                    $('#user_name').text(data.user_name);
                    $('#event_type_translations_name').text(data.event_type_translations_name);
                    $('#created').text(data.created_at);
                    $('#updated_at').html(data.updated_at);
                    $(' #btn_dele').html(data.btn);
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code show details ajax*/
    </script>
@endsection
