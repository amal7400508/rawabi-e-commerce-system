@php($page_title = __('admin.product_branch'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.products')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item "><a href="{{ route('product')}}">{{__('admin.products')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.recycle_bin')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.recycle_bin')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a href="{{ url('/app/product')}}}" data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <table width="100%"
                                                       class="table zero-configuration"
                                                       id="user_table">
                                                    <thead>
                                                    <tr>
                                                    <tr>
                                                        <th>{{__('admin.item_code')}}</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th>{{__('admin.categorie')}}</th>
{{--                                                        <th>{{__('admin.type')}}</th>--}}
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.image')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            {{--start model Message Restore--}}
            <div id="confirmModalRestore" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header danger">
                            <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4 align="center"
                                style="margin:0;">{{__('admin.are_you_sure_you_want_to_restore_this_data')}}</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" name="ok_button_restore" id="ok_button_restore"
                                    class="btn btn-primary">{{__('admin.yes')}}</button>
                            <button type="button" class="btn btn-dark"
                                    data-dismiss="modal">{{__('admin.cancel')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            {{--end model Message Restore--}}

            {{-- start model Don Restore Message--}}
            <div id="messageDonRestor" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-primary alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="ft ft-rotate-cw"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{__('admin.restored_successfully')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--end model Don Restor Message--}}

            {{-- start model check Delete Message--}}
            <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4 align="center"
                                style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" name="ok_button" id="ok_button"
                                    class="btn btn-danger">{{__('admin.yes')}}</button>
                            <button type="button" class="btn btn-dark"
                                    data-dismiss="modal">{{__('admin.cancel')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- end model check Delete Message--}}

            {{-- start model Don Delete Message--}}
            <div id="messageDonDelete" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{__('admin.deleted_successfully')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--end model Don Delete Message--}}
            @endsection
            @section('script')
                <script>
                    /*start code show all data in datatable ajax*/
                    $(document).ready(function () {
                        $('#user_table').DataTable({
                            @if(app()->getLocale() == 'ar')
                            "language": {
                                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                            },
                            @endif
                            processing: true,
                            serverSide: true,
                            ajax: {
                                url: "{{  route('product.recycle_bin') }}",
                            },
                            columns: [
                                {
                                    data: 'number',
                                    name: 'number',
                                    class: 'number',
                                },
                                {
                                    data: 'name',
                                    name: 'name',
                                    class: 'name',
                                    render: function (data, type, full, meta) {
                                        return full['is_new'] != 1 ? data : data + "<span class='badge badge btn-primary float-right'>{{__('admin.new')}}</span>"
                                    },
                                },
                                {
                                    data: 'category_translations_name',
                                    name: 'category_translations_name',
                                    class: 'category_translations_name',
                                },
                                /*{
                                    data: 'type',
                                    name: 'type',
                                    render: function (data, type, full, meta) {
                                        if (data == 'public') {
                                            return "<span class='btn btn-sm btn-outline-primary'>{{__('admin.public')}}</span>"
                                        } else {
                                            return "<span class='btn btn-sm btn-outline-danger'>{{__('admin.private')}}</span>"
                                        }
                                    },
                                },*/
                                {
                                    data: 'status',
                                    name: 'status',
                                    class: 'status',
                                    render: function (data, type, full, meta) {
                                        if (data == 1) {
                                            return "<div class='fonticon-wrap'><i class='ft-unlock' style='color:#002581'></i></div>"
                                        } else {
                                            return "<div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>"
                                        }
                                    },
                                },
                                {
                                    data: 'image',
                                    name: 'image',
                                    class: 'image',
                                    render: function (data, type, full, meta) {
                                        var img= '<img  style="max-height:64px; max-width:64px"' +
                                            'src="{{ url().'/storage/thumbnail/64/'}}' + data + '"';
                                        img += ' onerror=this.src="{{  url('/storage/thumbnail/150/logo.png')}}">';
                                        return img;
                                    },
                                    orderable: false
                                },
                                {
                                    data: 'action',
                                    name: 'action',
                                    orderable: false
                                },
                            ],
                            'order': [[0, 'desc']],
                            "columnDefs": [
                                {
                                    "targets": [0],
                                    "visible": false,
                                    "searchable": false
                                }
                            ]
                        });
                    });
                    /*end code show all data in datatable ajax*/

                    /*start code restore ajax*/
                    var restore_id;
                    var detail_url;
                    $(document).on('click', '.restore', function () {
                        restore_id = $(this).attr('id');
                        $('#confirmModalRestore').modal('show');
                        console.log('click');
                    });
                    $('#ok_button_restore').click(function () {
                        $.ajax({
                            type: 'GET',
                            url: "restore/" + restore_id,
                            dataType: "json",
                            beforeSend: function () {
                                $('#ok_button_restore').text("{{__('admin.restoring')}}...");
                                console.log('click show');
                                console.log("restore/" + restore_id);
                            },
                            success: function (data) {
                                console.log('click success'),
                                    setTimeout(function () {
                                        $('#confirmModalRestore').modal('hide');
                                        $('#user_table').DataTable().ajax.reload();
                                        $('#ok_button_restore').text("{{__('admin.yes')}}");
                                    }, 500);
                                $('#messageDonRestor').modal('hide');
                                setTimeout(function () {
                                    $('#messageDonRestor').modal('show');
                                }, 510,);
                                setTimeout(function () {
                                    $('#messageDonRestor').modal('hide');
                                }, 3000,);
                            },
                        })
                    });
                    /*end code restore ajax*/

                    /*start code Delete ajax*/
                    var $product_id;

                    $(document).on('click', '.delete', function () {
                        $product_id = $(this).attr('id');
                        $('#confirmModalDelete').modal('show');
                    });
                    $('#ok_button').click(function () {
                        console.log('click end');
                        $.ajax({
                            url: "hdelete/" + $product_id,
                            beforeSend: function () {
                                console.log('click before send'),
                                    $('#ok_button').text("{{__('admin.deleting')}}...");
                            },
                            success: function (data) {
                                console.log('click success'),
                                    setTimeout(function () {
                                        $('#confirmModalDelete').modal('hide');
                                        $('#user_table').DataTable().ajax.reload();
                                        console.log('click before send'),
                                            $('#ok_button').text("{{__('admin.yes')}}");
                                    }, 500);
                                $('#messageDonDelete').modal('hide');
                                setTimeout(function () {
                                    $('#messageDonDelete').modal('show');
                                }, 510,);
                                setTimeout(function () {
                                    $('#messageDonDelete').modal('hide');
                                }, 3000,);
                            },
                            error: function (data) {
                                console.log('click failed')
                            }
                        })
                    });
                    /*end code Delete ajax*/
                </script>
@endsection
