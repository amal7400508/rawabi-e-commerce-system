@php($page_title = __('admin.product_branch'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.product')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard')}}">{{__('admin.Dashboard')}}</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('/app/product')}}">{{__('admin.product')}}</a></li>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.products_report')}}</li>
                        </ol>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.create')}} {{__('admin.products_report')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                               </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <form action="{{ route('product.export')}}" method="get">
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.from')}}</label>
                                                                        <input type="date" class="form-control"
                                                                               name="from">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.to')}}</label>
                                                                        <input type="date" class="form-control" name="to">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.name')}} </label>
                                                                        <input type="text" class="form-control" name="name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.id')}}</label>
                                                                        <input type="number" class="form-control" name="id"/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label style="margin: 36px 4px 0 18px;">{{__('admin.new')}}</label>
                                                                        <input type="checkbox" value="1" name="is_new">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.category')}}</label>
                                                                        <select class="form-control" name="category" aria-invalid="false">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            @foreach($categories as $category)
                                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.status')}}</label>
                                                                        <select class="form-control" name="status">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            <option value="1">{{__('admin.active')}}</option>
                                                                            <option value="0">{{__('admin.attitude')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.type')}}</label>
                                                                        <select class="form-control" name="type">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            <option value="public">{{__('admin.public')}}</option>
                                                                            <option value="private">{{__('admin.private')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.from') .': '. __('admin.date_1') .' '. __('admin.requests_number')}}</label>
                                                                        <input type="date" class="form-control" name="from_date_requests_number">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.to') .': '. __('admin.date_1') .' '. __('admin.requests_number')}}</label>
                                                                        <input type="date" class="form-control" name="to_date_requests_number">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions pull-right">
                                                    <a href="#request_table" type="button" class=" btn btn-primary" id="button_preview"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-eye"></i> {{__('admin.preview')}}
                                                    </a>
                                                    <button type="submit" class=" btn btn-primary" id="button_export"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-printer"></i> {{__('admin.export')}}
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.post_index')}} {{__('admin.product')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload" id="reload"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <table width="100%"
                                                       class="table zero-configuration"
                                                       id="request_table">
                                                    <thead>
                                                    <tr>
                                                        <th hidden>{{__('admin.updated_at')}}</th>
                                                        <th>{{__('admin.item_code')}}</th>
                                                        <th style="min-width: 250px">{{__('admin.name')}}</th>
                                                        <th>{{__('admin.categorie')}}</th>
                                                        <th>{{__('admin.type')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.image')}}</th>
                                                        <th>{{__('admin.requests_number')}}</th>
    *                                                    <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

    {{-- start model show  Message--}}
    @include('managements.app.product.show')
    {{-- end model show  Message--}}

@endsection
@section('script')
    <script>
        var confirm = 0;

        /*start code show all data in datatable ajax*/
        var request_table = $('#request_table');
        $(document).ready(function () {
            request_table.DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('product.report')}}",
                    method: 'GET',
                    data: function (data) {
                        /* todo:: custom filter goes here*/
                        /* they are the parameters of the url*/
                        if (confirm === 1) {
                            data.confirm = confirm;
                            data.from = $('input[name=from]').val();
                            data.to = $('input[name=to]').val();
                            data.id = $('input[name=id]').val();
                            data.name = $('input[name=name]').val();
                            data.category = $('select[name=category]').val();
                            data.status = $('select[name=status]').val();
                            data.type = $('select[name=type]').val();
                            data.is_new = $('input[name=is_new]:checked').val();
                            data.from_date_requests_number = $('input[name=from_date_requests_number]').val();
                            data.to_date_requests_number = $('input[name=to_date_requests_number]').val();
                        }
                    },
                },
                columns: [
                    {
                        data: 'updated_at',
                        name: 'updated_at'
                    },
                    {
                        data: 'number',
                        name: 'number',
                    },
                    {
                        data: 'name',
                        name: 'name',
                        render: function (data, type, full, meta) {
                            return full['is_new'] != 1 ? data : data + "<span class='badge badge btn-primary float-right'>{{__('admin.new')}}</span>"
                        },
                    },
                    {
                        data: 'category_name',
                        name: 'category_name',
                    },
                    {
                        data: 'type',
                        name: 'type',
                        render: function (data, type, full, meta) {
                            if (data == 'public') {
                                return "<span class='btn btn-sm btn-outline-primary'>{{__('admin.public')}}</span>"
                            } else {
                                return "<span class='btn btn-sm btn-outline-danger'>{{__('admin.private')}}</span>"
                            }
                        },
                    },
                    {
                        data: 'status',
                        name: 'status',
                        class: 'status',
                        render: function (data, type, full, meta) {
                            if (data == 1) {
                                return "<div class='fonticon-wrap'><i class='ft-unlock' style='color:#002581'></i></div>"
                            } else {
                                return "<div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>"
                            }
                        },
                    },
                    {
                        data: 'image',
                        name: 'image',
                        class: 'image',
                        render: function (data, type, full, meta) {
                            var img= '<img  style="max-height:64px; max-width:64px"' +
                                'src="{{ url().'/storage/thumbnail/64/'}}' + data + '"';
                            img += ' onerror=this.src="{{  url('/storage/thumbnail/150/logo.png')}}">';
                            return img;
                        },
                        orderable: false
                    },
                    {
                        data: 'most_product',
                        name: 'most_product',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    },
                ],
                'order': [[0, 'desc']],
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });


            $('#button_preview').click(function () {
                confirm = 1;
                request_table.DataTable().draw();
            });
        });
        /*end code show all data in datatable ajax*/

        /*start code show details ajax*/
        $('#offer_details_table')
            .addClass('table-white-space')
            .addClass('table-responsive')
            .css('display', 'block');

        $('#product-price-number-th').attr('hidden', false);
        var product_details_id;
        var data_taste;
        var data_unit;
        $(document).on('click', '.showB', function () {
            product_details_id = $(this).attr('id');
            detail_url = "{{ url('/app/product')}}" + "/show/" + product_details_id;
            let report_data = {
                start_date : $('input[name=from_date_requests_number]').val(),
                end_date : $('input[name=to_date_requests_number]').val()
            };
            $.ajax({
                type: 'GET',
                url: detail_url,
                data : report_data,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $("#offer_details_tbody").empty();
                    for (i = 0; i < data.product_price.length; i++) {
                        variable_coloer = data.product_price[i].order_availability == 1 ? '#fff' : '#feeff0';
                        data_unit = data.product_price[i].id_unit != 1 ? data.product_price[i].unit_name : "<span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>";
                        data_taste = data.product_price[i].taste_id != 1 ? data.product_price[i].taste : "<span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>";

                        if (data.product_price[i].image == null) {
                            data.product_price[i].image = 'thumbnail/150/logo.png';
                        }
                        {{--$("#offer_details_table").append("<tr style='background-color:" + variable_coloer + "'><td>" + data_unit + "</td><td>" + data_taste + "</td>" +--}}
                        {{--    "<td>" + data.product_price[i].price + "</td><td><img  style='max-height:50px; max-width:50px' src='{{  url('/'.providerType().'/').'/storage/product_price/64/' }}" + data.product_price[i].image + "' /></td></tr>");--}}
                        $("#offer_details_table").append(
                            '<tr style="background-color:' + variable_coloer + '">' +
                            '<td>' + data_unit + '</td>' +
                            '<td>' + data_taste + '</td>'+
                            '<td>' + data.product_price[i].price + '</td>' +
                            '<td >' + data.product_price[i].request_number + '</td>' +
                            '<td><img  style="max-height:50px; max-width:50px" src="{{  url().'/storage/product_price/64/' }}' + data.product_price[i].image + '" onerror=this.src="{{asset('storage/thumbnail/150/logo.png')}}" /></td>' +
                            '</tr>'
                        );
                    }
                    if (data.type == 'public') {
                        $('#type').html("<span class='btn btn-sm btn-outline-primary'>{{__('admin.public')}}</span>")
                    } else {
                        $('#type').html("<span class='btn btn-sm btn-outline-danger'>{{__('admin.private')}}</span>")
                    }
                    if (data.status == 1) {
                        $('#status').html('<span class="btn btn-primary position-absolute round" style="margin:10px;" ><i class="ft-unlock"></i></span>');
                    } else {
                        $('#status').html('<span class="btn btn-danger position-absolute round" style="margin:10px;" ><i class="ft-lock"></i></span>');
                    }
                    $('#show-image').attr('src', '{{asset('/storage/thumbnail/640/')}}' + '/' + data.image);
                    $('#name').text(data.name);
                    $('#id').text(data.id);
                    $(' #details').text(data.details);
                    $(' #taste').text(data.taste);
                    $(' #image').text(data.image);
                    if (data.is_new == '1') {
                        $(' #is_new').html("<span class='btn btn-sm btn-primary float-right'>{{__('admin.new')}}<span>");
                    }
                    data.state_id == 2 ? $(' #state_id').html("{{__('admin.this_product_is_available_in_Ibb')}}."):
                        $(' #state_id').empty();

                    $(' #category_translations_name').text(data.category_translations_name);
                    $('#created').text(data.created_at);
                    $('#updated_at').html(data.updated_at);
                    $(' #btn_dele').html(data.btn);
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code show details ajax*/
    </script>

@endsection
