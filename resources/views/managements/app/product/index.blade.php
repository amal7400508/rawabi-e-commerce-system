@php($page_title = __('admin.product_branch'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/icheck/custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/file-uploaders/dropzone.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/file-uploaders/dropzone.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/pages/dropzone.css')}}">
    {{--    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css')}}">--}}
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.products')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item active">{{__('admin.products')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                <div class="btn-group float-md-right">
                    @can('delete product')
                        <a href="{{ url('/app/product/recycle_bin')}}" class="btn btn-primary"
                           style="color: white"><i class="ft-trash position-right"></i> {{__('admin.recycle_bin')}}
                        </a>

                    @endcan
                    {{--<a href="{{ route(providerType().'product.report')}}" class="btn btn-primary"
                       --}}{{--style="background:#002582 !important;"--}}{{-->
                        <i class="ft ft-printer"></i>
                        {{__('admin.products_report')}}
                    </a>--}}
                </div>
            </div>
        </div>
        <div id="messageSave" class="modal fade text-left" role="dialog">
            <div class="modal-dialog">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="alert bg-info alert-icon-left mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                            <strong>{{__('admin.successfully_done')}}!</strong>
                            <p id="message"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-head">
                <div class="card-header">
                    <div class="btn-group">
                        @can('create product')
                            <a href="{{ url('/app/product/create')}}" class="btn btn-primary">
                                <i class="ft-plus white"></i> {{__('admin.create').' '.__('admin.is_new')}} </a>

                            <a class="btn btn-primary" id="openModalAppExal"
                               style="color: white">
                                <i class="la la-file-excel-o position-right"></i>{{__('admin.import_from_excel')}}
                            </a>
                            <a {{url('/app/product/exportExcelFormProductPrices')}} class="btn btn-primary" id="exportModalAppExcel"
                               style="color: white">
                                <i class="ficon ft-printer position-right"></i>{{__('admin.export')}}
                            </a>
                            <style>
                                #btn-excel-icon:hover:active:before:focus {
                                    border: none;
                                    background-color: #0000;
                                }
                            </style>
                            <form action="{{url('/app/product/exportExcelFormProducts')}}" method="get">
                                <button id="btn-excel-icon" type="submit" title="{{__('admin.export_excel_form_to_enter_products')}}" style="border: none;background-color: #0000;">
                                    <img src="{{asset('icons/excel.png')}}" alt="" style="width: 27px;margin: 8px 12px">
                                </button>
                            </form>
                        @endcan
                        {{--<button class="btn btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" --}}{{--style="background:#002582 !important;"--}}{{-->
                            <i class="ft-filter icon-left"></i> {{__('admin.filter')}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <button class="dropdown-item filterRequestClick" id="">{{__('admin.all')}}</button>
                            @foreach($categories as $category)
                                <button class="dropdown-item filterRequestClick"
                                        id="{{$category->id}}">{{$category->name}}</button>
                            @endforeach
                        </div>--}}
                    </div>

                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload" id="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                    </div>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                    <div class="table-responsive">
                        <div class="row">
                            @include('include.table_length')
                            <div class="col-sm-12 col-md-9">
                                <form style="display: flex;justify-content: start;">
                                    @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                    <div class="col-sm-12 col-md-4">
                                        <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                            <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                            <option value="name" @if($search_type == 'name') selected @endif>{{__('admin.name')}}</option>
                                            <option value="category" @if($search_type == 'category') selected @endif>{{__('admin.category')}}</option>
                                            <option value="branch_name" @if($search_type == 'branch_name') selected @endif>{{__('admin.branch_name')}}</option>
                                            <option value="image" @if($search_type == 'image') selected @endif>{{__('admin.image_null')}}</option>
                                            <option value="is_approved" @if($search_type == 'is_approved') selected @endif>{{__('admin.accept_and_reject')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <input type="search" class="form-control form-control-sm"
                                               placeholder="{{__('admin.search')}}"
                                               name="query"
                                               aria-controls="user_table"
                                               value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @can('delete product')
                            <button type="button" id="button-delete-all" class="btn btn-sm btn-danger mb-1 span">حذف الكل</button>
                        @endcan
                        <table width="100%" id="table" class="table icheck zero-configuration">
                            <thead>
                            <tr>
                                @can('delete product')
                                <th><input type="checkbox" class="input-chk icheckbox_square-blue" id="check-all-products"></th>
                                @endcan
                                    <th style="width: 10px">{{__('admin.item_code')}}</th>
                                <th>{{__('admin.branch_name')}}</th>
                                <th style="width: 200px">{{__('admin.name')}}</th>
                                <th>{{__('admin.first_pricing')}}</th>
                                {{--                                                        <th>{{__('admin.categories')}}</th>--}}
                                <th style="width: 10px">{{__('admin.status')}}</th>
                                <th>{{__('admin.image')}}</th>
                                <th style="width: 6550px">{{__('admin.action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($data) == 0)
                                <tr id="row-not-found">
                                    <td colspan="9" class="text-center">
                                        {{__('admin.no_data')}}
                                        <hr>
                                    </td>
                                </tr>
                            @endif
                            @foreach($data as $product)
                                <tr style="{{$product->background_color_row}}" class="@if($product->is_approved == 3) text-line-td @endif">
                                    <td hidden>{{$product->updated_at}}</td>
                                    @can('delete product')
                                        <td><input name="items-id[]" value={{$product->id}} type="checkbox" class="input-chk icheckbox_square-blue" id="check-all{{$product->id}}"></td>
                                    @endcan
                                    <td class="font-default">{{$product->number}}</td>
                                    <td class="font-default">{{$product->branch->name ?? null}}</td>
                                    {{--<td>
                                        {!! $product->is_new != 1 ? $product->name : $product->name . "<button class='is_new badge badge btn-primary float-right'>". __('admin.new') ."</button>" !!}
                                    </td>--}}
                                    <td>

                                        {{$product->name}}
                                        <div class="btn-group float-right" role="group" aria-label="Button group with nested dropdown">
                                            @if($product->is_new == 1)
                                                <button @can('update product') @else disabled @endcan class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2"
                                                        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    {{__('admin.new')}}
                                                </button>
                                            @else
                                                <button @can('update product') @else disabled @endcan class="btn btn-sm btn-cyan dropdown-toggle dropdown-menu-right box-shadow-2"
                                                        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    {{__('admin.old')}}
                                                </button>
                                            @endif
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                <button class="dropdown-item statusClick" data-id-row="{{$product->id}}" data-status="new">{{__('admin.new')}}</button>
                                                <button class="dropdown-item statusClick" data-id-row="{{$product->id}}" data-status="old">{{__('admin.old')}}</button>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="font-default">{{$product->productPrice->first()->price ?? null}}</td>
                                    {{--                                                            <td>{{$product->category->name ?? null}}</td>--}}
                                    <td>
                                        @if ($product->status == 1)
                                            <div class='fonticon-wrap'><i class='ft-unlock' style='color:#0679f0'></i></div>
                                        @else
                                            <div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>
                                        @endif
                                    </td>
                                    <td>
                                        <img style="max-height:64px; max-width:64px" src="{{url('/').'/storage/thumbnail/64/'.$product->image}}" onerror=this.src="{{ url('/storage/thumbnail/150/logo.png')}}">
                                    </td>
                                    <td>
                                        {!! $product->actions_branch !!}
                                        {{--                                                                <a><small class="show-detail color-info" id="{{$product->id}}">{{__('admin.detail')}}...</small></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $data->appends($pagination_links)->links() }}
                    <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                </div>
            </div>
            {{--<div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <button type="button" id="button-delete-all" class="btn btn-sm btn-danger mb-1 span">حذف الكل</button>

                        <table width="100%" class="table table-white-space display icheck  no-wrap table-middle" id="data_table">
                            <thead>
                            <tr>
                                <th hidden></th>
                                <th><input type="checkbox" class="input-chk" id="check-all-products"></th>
                                <th>{{__('admin.item_code')}}</th>
                                <th>{{__('admin.name')}}</th>
                                <th>{{__('admin.categories')}}</th>
                                <th>{{__('admin.status')}}</th>
                                <th>{{__('admin.image')}}</th>
                                <th>{{__('admin.action')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                </div>
            </div>--}}
        </div>
    </div>

    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center"
                        style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark" data-dismiss="modal">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    @include('managements.app.product.images')

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

    {{-- start model show  Message--}}
    @include('managements.app.product.show')
    {{-- end model show  Message--}}

    {{-- start model createPrice  Message--}}
    @include('managements.app.product.create_product_price')
    {{-- end model createPrice Message--}}

    {{-- start model createAdd  Message--}}
    @include('managements.app.product.create_product_add')
    {{-- end model createAdd Message--}}

    {{-- start model check Delete product price Message--}}
    <div id="confirmModalDeletePrice" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center" style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button_price" id="ok_button_price"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark"
                            id="cancel_button_price" {{--data-dismiss="modal"--}}>{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete product price Message--}}

    <div class="modal" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form id="form-add-excel" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title">{{__('admin.add_products_via_excel_file')}}</h5>
                            <button type="button" class="close" id="modal-edit-close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <fieldset class="form-group col-12">
                                    <label for="type">{{__('admin.type')}}
                                        <span class="danger">*</span>
                                    </label>
                                    <br>
                                    <select id="type" class="form-control" required name="type">
                                        <option value="">{{__('admin.select_option')}}</option>
                                        <option value="product">{{__('admin.product')}}</option>
                                        <option value="Product_pricing">{{__('admin.Product_pricing')}}</option>
                                    </select>
                                    <span class="error-message">
                                            <strong></strong>
                                        </span>
                                </fieldset>
                                <fieldset class="form-group col-12">
                                    <label for="InputBranch">{{__('admin.branch_name')}}
                                        <span class="danger">*</span>
                                    </label>
                                    <br>
                                    <select id="InputBranch" class="form-control" required name="branch_id">
                                        <option value="">{{__('admin.select_option')}}</option>
                                        @foreach($branches as $branch)
                                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="error-message">
                                            <strong></strong>
                                        </span>
                                </fieldset>
                                <fieldset class="form-group col-12">
                                    <label class="span label-control">{{__('admin.excel_file')}}</label>
                                    <input required type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" id="file-products" name="file_products" class="form-control">
                                    <span class="error-message">
                                        <strong></strong>
                                    </span>
                                </fieldset>
                            </div>
                            <div class="modal-footer">
                                <fieldset id="button-container"
                                          class="form-group position-relative has-icon-left mb-0">
                                    <button type="submit" id="save" class="btn btn-primary"><i class="la la-save"></i> {{__('admin.add')}}</button>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
    <div class="modal" id="addModalPrice" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form action="{{url('/app/product/exportExcelFormProductPrices')}}" id="form-add-excel-price" method="get" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title">{{__('admin.add_products_via_excel_file')}}</h5>
                            <button type="button" class="close" id="modal-edit-close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <fieldset class="form-group col-12">
                                    <label for="InputBranch">{{__('admin.branch_name')}}
                                        <span class="danger">*</span>
                                    </label>
                                    <br>
                                    <select id="InputBranch" class="form-control" required name="branch_id_price">
                                        <option value="">{{__('admin.select_option')}}</option>
                                        @foreach($branches as $branch)
                                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="error-message">
                                            <strong></strong>
                                        </span>
                                </fieldset>
                            </div>
                            <div class="modal-footer">
                                <fieldset id="button-container"
                                          class="form-group position-relative has-icon-left mb-0">
                                    <button type="submit" id="save_price" class="btn btn-primary"><i class="la la-save"></i> {{__('admin.export')}}</button>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editProductModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.edit')}} {{__('admin.product')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements" id="check_show">
                                <label for="edit_new">{{__('admin.is_new')}}</label>
                                <input id="edit_new" type="checkbox" name="is_new" value="1"/>
                            </div>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group {{ $errors->has('branch_id') ? ' has-error' : '' }}">
                                                        <label for="branch_id">{{__('admin.branch_name')}}
                                                            <span class="danger">*</span></label>
                                                        <br>
                                                        <select class="form-control branchName" id="branch_id" required name="branch_id">
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($branches as $branch)
                                                                <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-message">
                                                                    <strong></strong>
                                                                </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="category_id">{{__('admin.categories')}}<span class="danger">*</span></label>
                                                        <select class="form-control" id="category_id" required name="category_id">
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="number">{{__('admin.number')}}<span class="danger">*</span></label>
                                                        <input type="text" id="number" class="form-control" name="number" required>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="ar_name">{{__('admin.name')}} (ar)<span class="danger">*</span></label>
                                                        <input type="text" id="ar_name" class="form-control" name="ar_name" required>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="en_name">{{__('admin.name')}} (en)<span class="danger">*</span></label>
                                                        <input type="text" id="en_name" class="form-control" name="en_name" required>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="category_id">{{__('admin.material')}}<span class="danger">*</span></label>
                                                        <select class="form-control" id="materials_id" required name="materials_id">
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($materials as $material)
                                                                <option value="{{$material->id}}">{{$material->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="edit_image" id="label-image">{{__('admin.image')}}</label>
                                                    <input disabled type="text" hidden name="img" id="image-name">
                                                    <input type="file" accept=".jpg, .jpeg, .png" id="edit_image" class="form-control" name="image" onchange="loadAvatar(this);">
                                                    <button type="button" hidden onclick="delete_img()" style="position: static; " class="btn btn-sm btn-outline-danger" id="delete-img"><i class="ft-trash"></i></button>

                                                    <img id="edit-product-image" style="max-width: 140px; height: auto; margin:10px;">
                                                    <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ar_details">{{__('admin.details')}} (ar)</label>
                                                    <textarea rows="5" id="ar_details" class="form-control" name="ar_details"></textarea>
                                                    <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                                <div class="form-group">
                                                    <label for="en_details">{{__('admin.details')}} (en)</label>
                                                    <textarea rows="5" id="en_details" class="form-control" name="en_details"></textarea>
                                                    <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                                <br>

                                                <div class="col-md-12 form-group ">
                                                    <div class="row">
                                                        <label class="col-md-5 label-control">{{__('admin.status')}}</label>
                                                        <div class="col-md-7 mx-auto">
                                                            <div class="input-group">
                                                                <div class="d-inline-block custom-control custom-radio mr-1">
                                                                    <input type="radio" name="status" value="1" class="custom-control-input" id="available">
                                                                    <label class="custom-control-label cursor-pointer" for="available">{{__('admin.available')}}</label>
                                                                </div>
                                                                <div class="d-inline-block custom-control custom-radio">
                                                                    <input type="radio" name="status" value="0" class="custom-control-input" id="un_available">
                                                                    <label class="custom-control-label cursor-pointer" for="un_available">{{__('admin.un_available')}}</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    @include('include.message-don-delete')

@endsection
@section('script')
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    @include('managements.app.product.index_js')
    @include('managements.app.product.js_show')
    @include('include.ajax-CRUD')

    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>

    <script src="{{asset('app-assets/vendors/js/extensions/dropzone.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/extensions/dropzone.js')}}"></script>
    {{--    <script src="{{asset('app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')}}"></script>--}}

@endsection
