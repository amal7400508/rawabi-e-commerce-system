<script>
    /*start code show details ajax*/
    var product_details_id;
    var data_taste;
    var data_unit;
    $(document).on('click', '.showB', function () {
        product_details_id = $(this).attr('id');
        detail_url = "{{ url('/app/product')}}" + "/show/" + product_details_id;
        $.ajax({
            type: 'GET',
            url: detail_url,
            dataType: "json",
            beforeSend: function () {
                $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                $('#confirm-modal-loading-show').modal('show');
            },
            success: function (data) {
                $("#offer_details_tbody").empty();
                for (i = 0; i < data.product_price.length; i++) {
                    variable_coloer = data.product_price[i].order_availability == 1 ? '#fff0' : '#feeff0';
                    data_unit = data.product_price[i].id_unit != null ? data.product_price[i].unit_name : "<span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>";

                    if (data.product_price[i].image == null) {
                        data.product_price[i].image = 'thumbnail/150/logo.png';
                    }
                    {{--$("#offer_details_table").append("<tr style='background-color:" + variable_coloer + "'><td>" + data_unit + "</td><td>" + data_taste + "</td>" +--}}
                    {{--    "<td>" + data.product_price[i].price + "</td><td><img  style='max-height:50px; max-width:50px' src='{{  url('/'.providerType().'/').'/storage/product_price/64/' }}" + data.product_price[i].image + "' /></td></tr>");--}}
                    $("#offer_details_table").append(
                        '<tr style="background-color:' + variable_coloer + '">' +
                        '<td>' + data_unit + '</td>' +
                        // '<td>' + data.product_price[i].color_name + '</td>' +
                        '<td>' + data.product_price[i].currency + ' ' + data.product_price[i].price + '</td>' +
                        '<td>' + data.product_price[i].note_ar + '</td>' +
                        '<td><img  style="max-height:50px; max-width:50px" src="{{asset('/storage/product_price/64/').'/'}}' + data.product_price[i].image + '" onerror=this.src="{{asset('storage/thumbnail/150/logo.png')}}" /></td>' +
                        '</tr>'
                    );
                }
                /*if (data.type == 'public') {
                    $('#type').html("<span class='btn btn-sm btn-outline-primary'>{{__('admin.public')}}</span>")
                } else {
                    $('#type').html("<span class='btn btn-sm btn-outline-danger'>{{__('admin.private')}}</span>")
                }*/
                if (data.status == 1) {
                    $('#status').html('<span class="btn btn-primary position-absolute round" style="margin:10px;" ><i class="ft-unlock"></i></span>');
                } else {
                    $('#status').html('<span class="btn btn-danger position-absolute round" style="margin:10px;" ><i class="ft-lock"></i></span>');
                }
                $('#show-image').attr('src', '{{asset('/storage/thumbnail/640/')}}' + '/' + data.image);
                $('#name').text(data.name);
                $('#id').text(data.id);
                $('#details').text(data.details);
                $('#show_branch_name').text(data.branch_name);
                $('#taste').text(data.taste);
                $('#image').text(data.image);
                if (data.is_new == '1') {
                    $(' #is_new').html("<span class='btn btn-sm btn-primary float-right'>{{__('admin.new')}}<span>");
                }
                /*data.states != null ? $(' #state_id').html("{{__('admin.this_product_is_available_in_city')}}."): */
                /*$('#state_id').empty();
                if ( data.states != null){
                    $('#state_id').append("{{__('admin.this_product_is_available_in_city')}} : ");
                    for (let i = 0; i < data.states.length; i++){
                        $('#state_id').append(data.states[i] + ' . ');
                    }
                }*/
                $(' #category_translations_name').text(data.category_translations_name);
                $('#created').text(data.created_at);
                $('#updated_at').html(data.updated_at);
                $(' #btn_dele').html(data.btn);
                $('#confirm-modal-loading-show').modal('hide');
                $('#confirmModalShow').modal('show');
            },
            error: function (data) {
                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                $('#confirm-modal-loading-show').modal('show');
            }
        })
    });
    /*end code show details ajax*/
</script>
