<script type="text/javascript">
    $(document).ready(function () {
        $('#data_table').DataTable({
            @if(app()->getLocale() == 'ar')
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            @endif
            processing: true,
            serverSide: true,
            ajax: '{{  route('product') }}',
            'drawCallback': function () {
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    increaseArea: '20%'
                });
            },
            'columnDefs': [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
            ],
            'order': [[0, 'desc']],
            columns: [
                {
                    data: 'id',
                    name: 'id',
                    class: 'id'
                },
                {
                    data: 'check',
                    name: 'check',
                    orderable: false,
                },
                {
                    data: 'number',
                    name: 'number',
                },
                {
                    data: 'name',
                    name: 'name',
                    render: function (data, type, full, meta) {
                        return full['is_new'] != 1 ? data : data + "<span class='badge badge btn-primary float-right'>{{__('admin.new')}}</span>"
                    },
                },
                {
                    data: 'category_name',
                    name: 'category_name',
                },

                {
                    data: 'status',
                    name: 'status',
                    class: 'status',
                    render: function (data, type, full, meta) {
                        if (data == 1) {
                            return "<div class='fonticon-wrap'><i class='ft-unlock' style='color:#0679f0'></i></div>"
                        } else {
                            return "<div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>"
                        }
                    },
                },
                {
                    data: 'image',
                    name: 'image',
                    class: 'image',
                    render: function (data, type, full, meta) {
                        var img = '<img  style="max-height:64px; max-width:64px"' +
                            'src="{{url('/').'/storage/thumbnail/64/'}}' + data + '"';
                        img += ' onerror=this.src="{{ url('/storage/thumbnail/150/logo.png')}}">';
                        return img;
                    },
                    orderable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                },
            ],
        });
    });

    var lock = "<i class='ft-lock'></i>",
        unlock = "<i class='ft-unlock'></i>";

    var counter = 0;  /*created for image in product price*/
    var unit_count = 0;
    var count = 1;
        @can('create product')
    $(document).on('click', '#addRow', function () {
        console.log($(this));
        unit_count++;
        dynamic_field(unit_count);
        $('.switchBootstrap').bootstrapSwitch();
    });

    $(document).on('click', '#addRowAdd', function () {
        console.log($(this));
        unit_count++;
        dynamic_field_add(unit_count);
        $('.switchBootstrap').bootstrapSwitch();
    });

    function dynamic_field(unit_count) {
        counter++;
        var div;
        div = '<div class="row">' +
            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.unit_type')}}</label>' +
            '<br>' +
            '<select class="form-control all-typeUnit" data-typeUnit-order="' + unit_count + '">' +
            '<option value="">{{__('admin.select')}}</option>' +
            @foreach($unit_types as $unit_type)
                '<option value="{{$unit_type->id}}">{{$unit_type->name}}</option>' +
            @endforeach
                {{--    '<option value="1">{{__('admin.size')}}</option>' +
                '<option value="2">{{__('admin.ball')}}</option>' +
                '<option value="3">{{__('admin.weight')}}</option>' +--}}
                '</select>' +
            '</div>' +
            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.unit')}} </label>' +
            '<br>' +
            '<select class="form-control" id="unitAll-' + unit_count + '" name="unit_id[]">' +
            '</select>' +
            '</div>' +

            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.colors')}} </label>' +
            '<br>' +
            '<select class="form-control" data-typeUnit-order="' + unit_count + '" name="color_id[]">' +
            '<option value="">{{__('admin.select')}}</option>' +
            @foreach($colors as $color)
                '<option value="{{$color->id}}">{{$color->name}}</option>' +
            @endforeach

                '</select>' +
            '</div>' +

            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.currency')}}</label>' +
            '<br>' +
            '<select class="form-control" data-typeUnit-order="' + unit_count + '" name="currency[]">' +
            '<option value="ريال يمني">{{__('admin.yemeni_riyal')}}</option>' +
            '<option value="دولار أمريكي">{{__('admin.american_dollar')}}</option>' +
            '<option value="ريال سعودي">{{__('admin.saudi_riyal')}}</option>' +
            '</select>' +
            '</div>' +
            /*'<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.taste')}} </label>' +
                '<br>' +
                /!*'<select class="form-control" id="profession" name="taste[]">' +
                '<option value="">{{__('admin.select')}}</option>' +
{{--                '@foreach($taste as $tastea)'--}}
            {{--                @if($tastea->id != 1)+--}}
            {{--                '<option value="{{$tastea->id}}">{{$tastea->taste}}</option>'--}}
            {{--                @endif +--}}
            {{--                '@endforeach' +--}}
            '</select>' *!/+
            '</div>' +*/
            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.price')}} <span class="danger">*</span></label>' +
            '<br>' +
            '<input type="number" step="any"  class="form-control " value="{{ old('price[]') }}" required name="price[]">' +
            '@error("price[]")' +
            '<span class="invalid-feedback" role="alert">' +
            '<strong>{{ $message }}</strong>' +
            '</span>' +
            '@enderror' +
            '</div>' +
            '<div class="form-group col-sm-12 col-md-3">' +
            '<label >{{__('admin.image')}}</label>' +
            '<br>' +
            '<input type="file" accept="image/*" id="input-file-' + counter + '" data-num="' + counter + '"' +
            'onchange="loadAvatarImag(this);" class="form-control" name="image[]">' +
            '<span' +
            'style="font-size: 13px; margin: 7px">{{__('admin.please_enter_square_image')}}</span>' +
            '</div>' +
            '<div>' +
            '<button type="button" class="delete_img btn btn-sm btn-outline-danger" ' +
            'style="height: 35px;width: 35px;position: absolute;border-radius: 50%;background-color: #FF4961; color: #fff" ' +
            'hidden id="delete-img-' + counter + '" data-num="' + counter + '">' +
            '<i class="ft-trash"></i></button>' +
            '<img id="image-' + counter + '" class="image-price" style="max-width: 100px; height: auto; margin:10px;">' +
            '</div>' +
            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.note')}}(ar)</label>' +
            '<br>' +
            '<textarea cols="20" class="form-control" id="note_ar" name="note_ar[]" rows="2"></textarea>' +
            '</div>' +
            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.note')}}(en)</label>' +
            '<br>' +
            '<textarea cols="20" class="form-control" id="note_en" name="note_en[]" rows="2"></textarea>' +
            '</div>';
        /*'<div class="form-group col-sm-12 col-md-4">' +
        '<div class="heading-elements" id="check_show" style="margin: 5px">' +

        '<label >{{__('admin.note')}}</label>' +
                '<br>' +

                '<input type="checkbox" ' +
                'name="status[]" ' +
                'value="1" ' +
                'class="switchBootstrap status-' + unit_count + '"' +
                'data-default-value="' + unit_count + '"' +
                'id="switchBootstrap18" ' +
                'data-on-color="primary" ' +
                'data-off-color="danger" ' +
                'data-on-text="{{__('admin.yes')}}" ' +
                'data-off-text="{{__('admin.no')}}" ' +
                'data-color="primary" data-label-text="{{__('admin.status')}}" />' +
                '</div>' +
                '</div>';*/
        if (unit_count > 1) {
            div += '<div class="form-group mb-1 col-sm-12 col-md-1">' +
                '<br><div style="padding-top: 11px"></div>' +
                '<button type="button" class="remove btn btn-sm btn-danger pull-right" style="border-radius: 5px; padding: 9px"><i class="la la-close font-small-3" ></i>  {{__('admin.delete')}}</button>' +
                '</div>';
        } else {
            div += '</div>';
        }
        $('.form-body-rep').append(div);
    }

    function dynamic_field_add(unit_count) {
        counter++;
        var div;
        div ='<div class="row">' +
            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.type')}}  <span class="danger">*</span></label>' +
            '<br>' +
            '         <select class="form-control" id="type_id[]" name="type_id[]">' +
            '             <option value="" >{{__('admin.select_option')}}</option>' +
            {{--'             @foreach($types as $type)' +--}}
            {{--'                 <option value="{{$type->id}}">{{$type->name}}</option>' +--}}
            {{--'             @endforeach' +--}}
            '         </select>' +
            '@error("type_id[]")' +
            '<span class="invalid-feedback" role="alert">' +
            '<strong>{{ $message }}</strong>' +
            '</span>' +
            '@enderror' +
            '</div>' +
            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.color')}}  <span class="danger">*</span></label>' +
            '<br>' +
            '<input type="color" step="any"  class="form-control " value="#0F0000" required name="color[]">' +
            '@error("color")' +
            '<span class="invalid-feedback" role="alert">' +
            '<strong>{{ $message }}</strong>' +
            '</span>' +
            '@enderror' +
            '</div>' +
            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.addition')}} (ar) <span class="danger">*</span></label>' +
            '<br>' +
            '<input type="text" step="any"  class="form-control " value="{{ old('add_ar[]') }}" required name="add_ar[]">' +
            '@error("add_ar[]")' +
            '<span class="invalid-feedback" role="alert">' +
            '<strong>{{ $message }}</strong>' +
            '</span>' +
            '@enderror' +
            '</div>' +
            '<div class="form-group col-sm-12 col-md-3">' +
            '<label>{{__('admin.addition')}} (en) <span class="danger">*</span></label>' +
            '<br>' +
            '<input type="text" step="any"  class="form-control " value="{{ old('add_en[]') }}" required name="add_en[]">' +
            '@error("add_en[]")' +
            '<span class="invalid-feedback" role="alert">' +
            '<strong>{{ $message }}</strong>' +
            '</span>' +
            '@enderror' +
            '</div>';

        if (unit_count > 1) {
            div += '<div class="form-group mb-1 col-sm-12 col-md-1">' +
                '<br><div style="padding-top: 11px"></div>' +
                '<button type="button" class="remove btn btn-sm btn-danger pull-right" style="border-radius: 5px; padding: 9px"><i class="la la-close font-small-3" ></i>  {{__('admin.delete')}}</button>' +
                '</div>';
        } else {
            div += '</div>';
        }
        $('.form-body-rep').append(div);
    }

        @endcan

    $('.form-body-rep').on('click', '.remove', function () {
        $(this).parent().parent().remove();
    });

    var id_delete;
    $('.form-body-rep').on('click', '.final-delete', function () {
        id_delete = $(this).attr('data-id-delete');
        var result = confirm("{{__('admin.are_you_sure_you_want_to_remove_this_data')}}");
        if (result == true) {
            $.ajax({
                url: "{{ url('/app/product_price/delete')}}" + '/' + id_delete,
                beforeSend: function () {
                    console.log('click before send');
                },
                success: function (data) {
                    console.log(id_delete);
                    if (data.error_deleting != undefined) {
                        $('#messageDonDelete').modal('hide');
                        setTimeout(function () {
                            $('#messageDonDelete').modal('show');
                        }, 510,);
                        setTimeout(function () {
                            $('#messageDonDelete').modal('hide');
                        }, 3000,);
                        $('#error-deleting').text(data.error_deleting);
                        $('#title-error').text("{{__('admin.error_message')}}!");
                    } else {
                        $('.form-body-rep').find("[data-id-delete='" + id_delete + "']").parent().parent().remove();
                    }
                },
                error: function (data) {
                },
                complete: function (data) {

                }
            })
        }
    });
    $('.form-body-rep').on('click', '.final-delete-add', function () {
        id_delete = $(this).attr('data-id-delete');
        var result = confirm("{{__('admin.are_you_sure_you_want_to_remove_this_data')}}");
        if (result == true) {
            $.ajax({
                url: "{{ url('/app/add/delete')}}" + '/' + id_delete,
                beforeSend: function () {
                    console.log('click before send');
                },
                success: function (data) {
                    console.log(id_delete);
                    if (data.error_deleting != undefined) {
                        $('#messageDonDelete').modal('hide');
                        setTimeout(function () {
                            $('#messageDonDelete').modal('show');
                        }, 510,);
                        setTimeout(function () {
                            $('#messageDonDelete').modal('hide');
                        }, 3000,);
                        $('#error-deleting').text(data.error_deleting);
                        $('#title-error').text("{{__('admin.error_message')}}!");
                    } else {
                        $('.form-body-rep').find("[data-id-delete='" + id_delete + "']").parent().parent().remove();
                    }
                },
                error: function (data) {
                },
                complete: function (data) {

                }
            })
        }
    });
    $('#cancel_button_price').click(function () {
        $('#confirmModalDeletePrice').modal('hide');
    });
</script>
<!-- BEGIN: Page JS-->
<script>
    /*Reload the data and display it in the datatable*/
    @include('include.reload-datatable')

    @if(count($errors)>0)
    $('#createPriceModel').modal('show');
    @endif

    @if(count($errors)>0)
    $('#createAddModel').modal('show');
    @endif

    /*start code show all data in datatable ajax*/
    $(document).ready(function () {
        var filter;
        $(document).on('click', '.filterRequestClick', function () {
            filter = $(this).attr('id');
            $('#data_table').DataTable().draw();
        });
    });
    /*end code show all data in datatable ajax*/

    /*ajax falter start*/
    $(document).on('change', '.all-typeUnit', function () {
        var data_number = $(this).attr('data-typeUnit-order');
        console.log(data_number);
        var type_unit = $(this).val();
        detail_url = "{{ url('/app/product/falter')}}" + "/" + type_unit;
        console.log(detail_url);
        if (type_unit) {
            console.log('type_unit');
            $.ajax({
                type: "GET",
                url: "{{ url('/app/product/falter')}}" + "/" + type_unit,
                success: function (data) {
                    console.log('success');
                    if (data) {
                        $("#unitAll-" + data_number).empty();
                        $("#unitAll-" + data_number).append('<option value=""> {{__("admin.select_option")}}  </option>');
                        $.each(data, function (key, value) {
                            $("#unitAll-" + data_number).append('<option value="' + key + '">' + value + '</option>');
                        });
                    } else {
                        $("#unitAll-" + data_number).empty();
                    }
                }
            });
        } else {
            $("#unitAll-" + data_number).empty();
        }
    });
    /*ajax falter end*/

    /*start message save*/
    @if(session('success'))
    $('#message').text("{{session('success')}}.");
    $('#messageSave').modal('show');
    setTimeout(function () {
        $('#messageSave').modal('hide');
    }, 3000);
    @endif
    /*end  message save*/

    /*start code Delete ajax*/
    var $product_id;
    $(document).on('click', '.deleteB', function () {
        $product_id = $(this).attr('id');
        $('#confirmModalDelete').modal('show');
        console.log($product_id);
    });
    $('#ok_button').click(function () {
        $.ajax({
            url: "product/destroy/" + $product_id,
            beforeSend: function () {
                $('#ok_button').text('{{__('admin.deleting')}}...');
            },
            success: function (data) {
                if (data.error_deleting != undefined) {
                    $('#error-deleting').text(data.error_deleting);
                    $('#title-error').text("{{__('admin.error_message')}}!");
                    $('#messageDonDelete').modal('show');
                    setTimeout(function () {
                        $('#ok_button').text('{{__('admin.yes')}}');
                    }, 200,);
                    setTimeout(function () {
                        $('#messageDonDelete').modal('hide');
                    }, 3000,);
                } else {
                    $('#error-deleting').text("{{__('admin.deleted_successfully')}}.");
                    $('#title-error').text("{{__('admin.successfully_done')}}!");
                    setTimeout(function () {
                        $('#confirmModalDelete').modal('hide');
                        $('#data_table').DataTable().ajax.reload();
                        $('#ok_button').text('{{__('admin.yes')}}');
                    }, 500);
                    $('#messageDonDelete').modal('hide');
                    setTimeout(function () {
                        $('#messageDonDelete').modal('show');
                    }, 510,);
                    setTimeout(function () {
                        $('#messageDonDelete').modal('hide');
                    }, 3000,);
                    $('#confirmModalShow').modal('hide');
                }
            },
            error: function (data) {
                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                $('#confirm-modal-loading-show').modal('show');
            }
        })
    });
    /*end code Delete ajax*/

        @canany(['create product','update product'])
    /*start createPrice save*/
    var row_price;
    $(document).on('click', '.createPriceClick', function () {
        $('.createPriceClick').parent().parent().css("background-color", "rgb(255 255 255 / 0%)");
        row_price = $(this).parent().parent();
        var product_price_id;
        product_price_id = $(this).attr('id');
        var product_id_value = $('#product_id_value');
        product_id_value.attr('value', product_price_id);
        $.ajax({
            type: 'GET',
            url: "{{ url('/app/product_price')}}" + "/edit/" + product_price_id,
            dataType: "json",
            beforeSend: function () {
                $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                $('#confirm-modal-loading-show').modal('show');
            },
            success: function (data) {
                unit_count = 0;
                $('.form-body-rep').empty();
                if (data.product_price_data.length == 0) {
                    unit_count++;
                    dynamic_field(unit_count);
                    $('.switchBootstrap').bootstrapSwitch();
                }

                let price_note;
                for (i = 0; i < data.product_price_data.length; i++) {
                    counter++;

                    let hidden = data.product_price_data[i].image == null ? "hidden" : "";
                    var div = '<div class="row">';
                    div += '<input type="text" hidden value="' + data.product_price_data[i].image + '" name="image_edit[]">' +
                        '<input type="text" hidden value="' + data.product_price_data[i].id + '" name="product_price_id[]">';
                    if (unit_count > 0) {
                        div += '<div class="form-group col-sm-12 col-md-12 p-0" style="margin-bottom: 23px;"><hr></div>';
                    }
                    var px = unit_count > 0 ? "21px" : "-28px";
                    var status_price = data.product_price_data[i].status_price == 1 ? ' checked ' : '';
                    let currency = data.product_price_data[i].currency;

                    let price_note_ar = '';
                    let price_note_en = '';
                    let unit_id = '';
                    let unitName = '';
                    if (data.product_price_data[i].note_ar != null) {
                        price_note_ar = data.product_price_data[i].note_ar;
                    }
                    if (data.product_price_data[i].note_en != null) {
                        price_note_en = data.product_price_data[i].note_en;
                    }
                    if (data.product_price_data[i].unit_id != null) {
                        unit_id = data.product_price_data[i].unit_id;
                    }
                    if (data.product_price_data[i].unitName != null) {
                        unitName = data.product_price_data[i].unitName;
                    }
                    div += '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.unit_type')}}</label>' +
                        '<br>' +
                        '<select class="form-control all-typeUnit" data-typeUnit-order="' + unit_count + '">' +
                        '<option value="">{{__('admin.select')}}</option>' +
                        @foreach($unit_types as $unit_type)
                            '<option value="{{$unit_type->id}}">{{$unit_type->name}}</option>' +
                        @endforeach
                        /*'<select class="form-control all-typeUnit" data-typeUnit-order="' + unit_count + '" >' +
                        '<option value="">{{__('admin.select')}}</option>' +
                            '<option value="1">{{__('admin.size')}}</option>' +
                            '<option value="2">{{__('admin.ball')}}</option>' +
                            '<option value="3">{{__('admin.weight')}}</option>' +*/
                        '</select>' +
                        '</div>' +




                        '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.unit')}}</label>' +
                        '<br>' +
                        '<select class="form-control" id="unitAll-' + unit_count + '"  name="unit_id[]">';
                    div += '<option value="' + unit_id + '">' + unitName + '</option>';

                    div += '</select>' +
                        '</div>' +

                        '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.colors')}}</label>' +
                        '<br>' +
                        '<select class="form-control " data-typeUnit-order="' + unit_count + '" name="color_id[]">' +

                        '<option value="">{{__('admin.select')}}</option>' +
                        @foreach($colors as $color)
                            '<option value="{{$color->id}}" >{{$color->name}}</option>' +
                        @endforeach

                        '</select>' +
                        '</div>' +

                        /*'<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.taste')}} </label>' +
                            '<br>' +
                            '<select class="form-control" id="profession" name="taste[]">' +
                            '<option value="">{{__('admin.select')}}</option>' +
{{--                            '@foreach($taste as $tastea)--}}
                        {{--                                @if($tastea->id != 1)';--}}
                        {{--                        var id_tastea = "{{$tastea->id}}";--}}
                        {{--                        div += '<option value="{{$tastea->id}}" ';--}}
                        if (id_tastea == data.product_price_data[i].taste_id) {
                            div += ' selected ';
                        }
{{--div += '>{{$tastea->taste}}</option>' +--}}
                        {{--    '@endif--}}
                        {{--                                @endforeach' +--}}
                        '</select>' +
                        '</div>' +*/
                        '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.currency')}}</label>' +
                        '<br>' +
                        '<select class="form-control " data-typeUnit-order="' + unit_count + '" required name="currency[]">' +
                        '<option value="ريال يمني" ' + checkSelected(currency, "ريال يمني") + '>{{__('admin.yemeni_riyal')}}</option>' +
                        '<option value="دولار أمريكي" ' + checkSelected(currency, "دولار أمريكي") + '>{{__('admin.american_dollar')}}</option>' +
                        '<option value="ريال سعودي" ' + checkSelected(currency, "ريال سعودي") + '>{{__('admin.saudi_riyal')}}</option>' +
                        '</select>' +
                        '</div>' +
                        '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.price')}} <span class="danger">*</span></label>' +
                        '<br>' +
                        '<input type="number" step="any"  class="form-control @error("price[]") is-invalid @enderror" value="' + data.product_price_data[i].price + '" required name="price[]">' +
                        '@error("price[]")' +
                        '<span class="invalid-feedback" role="alert">' +
                        '<strong>{{ $message }}</strong>' +
                        '</span>' +
                        '@enderror' +
                        '</div>' +
                        '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.image')}}</label>' +
                        '<br>' +
                        '<input type="file" accept="image/*" id="input-file-' + counter + '" data-num="' + counter + '"' +
                        'onchange="loadAvatarImag(this);" class="form-control" name="image[]">' +
                        '<span style="font-size: 13px; margin: 7px">{{__('admin.please_enter_square_image')}}</span>' +
                        '</div>' +
                        '<div>' +
                        '<button ' + hidden + ' type="button" class="delete_img btn btn-sm btn-outline-danger" ' +
                        'style="height: 35px;width: 35px;position: absolute;border-radius: 50%;background-color: #FF4961; color: #fff" ' +
                        ' id="delete-img-' + counter + '" data-num="' + counter + '">' +
                        '<i class="ft-trash"></i></button>' +
                        '<img id="image-' + counter + '" class="image-price" ' + hidden +
                        ' src="{{asset('storage/product_price/150')}}' + '/' + data.product_price_data[i].image + '" ' +
                        'style="max-width: 100px; height: auto; margin:10px;">' +
                        '</div>' +
                        '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.note')}} (ar)</label>' +
                        '<br>' +
                        '<textarea cols="20" class="form-control" id="note_ar" name="note_ar[]" rows="2"> ' + price_note_ar + '</textarea>' +
                        '</div>' +
                        '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.note')}} (en)</label>' +
                        '<br>' +
                        '<textarea cols="20" class="form-control" id="note_en" name="note_en[]" rows="2"> ' + price_note_en + '</textarea>' +
                        '</div>';
                    /*'<div class="form-group col-sm-12 col-md-4">' +
                    '<div class="heading-elements" id="check_show" style="margin: 5px">' +

                    '<label >{{__('admin.available_for_hospitality')}}</label>' +
                            '<br>' +

                            '<input type="checkbox" ' +
                            'name="status[]" ' +
                            'value="1" ' +
                            'class="switchBootstrap status-' + unit_count + '"' +
                            'data-default-value="' + unit_count + '"' +
                            'id="switchBootstrap18" ' +

                            'data-on-color="primary" ' +
                            'data-off-color="danger" ' +
                            'data-on-text="{{__('admin.yes')}}" ' +
                            'data-off-text="{{__('admin.no')}}" ' +
                            'data-color="primary" data-label-text="{{__('admin.status')}}" ';*/
                    if (data.product_price_data[i].available == 1) {
                        div += ' checked ';
                    }
                                        @can('delete product')
                    if (unit_count > 0) {
                        div += '<div class="form-group mb-1 col-sm-12 col-md-1">' +
                            '<br><div style="padding-top: 11px"></div>' +
                            '<button type="button" class="final-delete btn btn-sm btn-danger pull-right"' +
                            'data-id-delete="' + data.product_price_data[i].id + '" style="border-radius: 5px; padding: 9px">' +
                            '<i class="la la-close font-small-3" ></i>  {{__('admin.delete')}}</button>' +
                            '</div>' +
                            '</div>';
                    }
                    div += '</div></div>';

                                        @endcan
                        unit_count++;
                    $('.form-body-rep').append(div);
                    $('.switchBootstrap').bootstrapSwitch();
                }
                $('#confirm-modal-loading-show').modal('hide');
                $('#createPriceModel').modal('show');
            },
            error: function (data) {
                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                $('#confirm-modal-loading-show').modal('show');
            }
        })
    });
    $(document).on('click', '.createAddClick', function () {
        $('.createAddClick').parent().parent().css("background-color", "rgb(255 255 255 / 0%)");
        row_price = $(this).parent().parent();
        var product_price_id_add;
        product_price_id_add = $(this).attr('id');
        var product_id_value = $('#product_id_value_add');
        product_id_value.attr('value', product_price_id_add);
        $.ajax({
            type: 'GET',
            url: "{{ url('/app/add')}}" + "/edit/" + product_price_id_add,
            dataType: "json",
            beforeSend: function () {
                $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                $('#confirm-modal-loading-show').modal('show');
            },
            success: function (data) {
                unit_count = 0;
                $('.form-body-rep').empty();
                if (data.product_price_data.length == 0) {
                    unit_count++;
                    dynamic_field_add(unit_count);
                    $('.switchBootstrap').bootstrapSwitch();
                }

                let price_note;
                for (i = 0; i < data.product_price_data.length; i++) {
                    counter++;

                    let hidden = data.product_price_data[i].image == null ? "hidden" : "";
                    var div = '<div class="row">';
                    div += '<input type="text" hidden value="' + data.product_price_data[i].image + '" name="image_edit[]">' +
                        '<input type="text" hidden value="' + data.product_price_data[i].id + '" name="product_price_id_add[]">';
                    if (unit_count > 0) {
                        div += '<div class="form-group col-sm-12 col-md-12 p-0" style="margin-bottom: 23px;"><hr></div>';
                    }
                    var px = unit_count > 0 ? "21px" : "-28px";
                    var status_price = data.product_price_data[i].status_price == 1 ? ' checked ' : '';
                    let currency = data.product_price_data[i].currency;

                    let add_type_id = '';
                    let add_add_ar = '';
                    let add_color = '';
                    let add_add_en = '';
                    if (data.product_price_data[i].type_id != null) {
                        add_type_id = data.product_price_data[i].type_id;
                    }
                    if (data.product_price_data[i].color != null) {
                        add_color = data.product_price_data[i].color;
                    }
                    if (data.product_price_data[i].add_ar != null) {
                        add_add_ar = data.product_price_data[i].add_ar;
                    }
                    if (data.product_price_data[i].add_en != null) {
                        add_add_en = data.product_price_data[i].add_en;
                    }
                    div += '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.type')}}  <span class="danger">*</span></label>' +
                        '<br>' +
                        '         <select class="form-control" value="' + add_type_id + '" id="type_id[]" name="type_id[]">' +
                        '             <option value="' + add_type_id + '"  >{{__('admin.select_option')}}</option>' +
                        {{--'             @foreach($types as $type)' +--}}
                        {{--'                <option value="{{$type->id}}">{{$type->name}}</option>' +--}}

                        {{--'             @endforeach' +--}}
                        '         </select>' +
                        '@error("type_ar[]")' +
                        '<span class="invalid-feedback" role="alert">' +
                        '<strong>{{ $message }}</strong>' +
                        '</span>' +
                        '@enderror' +
                        '</div>' +
                        '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.color')}}  <span class="danger">*</span></label>' +
                        '<br>' +
                        '<input type="color"  step="any"  class="form-control " value="' + add_color + '" required name="color[]">' +
                        '@error("color[]")' +
                        '<span class="invalid-feedback" role="alert">' +
                        '<strong>{{ $message }}</strong>' +
                        '</span>' +
                        '@enderror' +
                        '</div>' +
                        '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.addition')}} (ar) <span class="danger">*</span></label>' +
                        '<br>' +
                        '<input type="text" step="any"  class="form-control " value="' + add_add_ar + '" required name="add_ar[]">' +
                        '@error("add_ar[]")' +
                        '<span class="invalid-feedback" role="alert">' +
                        '<strong>{{ $message }}</strong>' +
                        '</span>' +
                        '@enderror' +
                        '</div>' +
                        '<div class="form-group col-sm-12 col-md-3">' +
                        '<label>{{__('admin.addition')}} (en) <span class="danger">*</span></label>' +
                        '<br>' +
                        '<input type="text" step="any"  class="form-control " value="' + add_add_en + '" required name="add_en[]">' +
                        '@error("add_en[]")' +
                        '<span class="invalid-feedback" role="alert">' +
                        '<strong>{{ $message }}</strong>' +
                        '</span>' +
                        '@enderror' +
                        '</div>';
                                        @can('delete product')
                    if (unit_count > 0) {
                        div += '<div class="form-group mb-1 col-sm-12 col-md-1">' +
                            '<br><div style="padding-top: 11px"></div>' +
                            '<button type="button" class="final-delete-add btn btn-sm btn-danger pull-right"' +
                            'data-id-delete="' + data.product_price_data[i].id + '" style="border-radius: 5px; padding: 9px">' +
                            '<i class="la la-close font-small-3" ></i>  {{__('admin.delete')}}</button>' +
                            '</div>' +
                            '</div>';
                    }
                    div += '</div></div>';

                                        @endcan
                        unit_count++;
                    $('.form-body-rep').append(div);
                    $('.switchBootstrap').bootstrapSwitch();
                }
                $('#confirm-modal-loading-show').modal('hide');
                $('#createAddModel').modal('show');
            },
            error: function (data) {
                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                $('#confirm-modal-loading-show').modal('show');
            }
        })
    });
    /*end createPrice save*/
        @endcanany

    /*ajax falter start*/
    $(document).on('change', '.all-typeUnit', function () {
        var data_number = $(this).attr('data-typeUnit-order');
        var type_unit = $(this).val();
        detail_url = "{{ url('/app/product/falter')}}" + "/" + type_unit;
        console.log(detail_url);
        if (type_unit) {
            console.log('type_unit');
            $.ajax({
                type: "GET",
                url: "{{ url('/app/product/falter')}}" + "/" + type_unit,
                success: function (data) {
                    console.log('success');
                    if (data) {
                        $("#unitAll-" + data_number).empty();
                        $("#unitAll-" + data_number).append('<option value=""> {{__("admin.select_option")}}  </option>');
                        $.each(data, function (key, value) {
                            $("#unitAll-" + data_number).append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    } else {
                        $("#unitAll-" + data_number).empty();
                    }
                }
            });
        } else {
            $("#unitAll-" + data_number).empty();
        }
    });
    /*ajax falter end*/

    $('#my_form_id').on('submit', function (event) {
        var route = "{{  route('product_price.store')}}";
        /*
        var formData = new FormData(this);
        formData.append('no', 1);
        */
        var formData = new FormData($('#my_form_id')[0]);
        event.preventDefault();

        var type = "حفظ";
        var _type = "button_save";

        $.ajax({
            url: route,
            method: "POST",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#' + _type).html('<i class="la la-spinner spinner"></i><span>جاري ال' + type + '...</span>')
            },
            success: function (data) {
                $('#' + _type + '').text(type);
                $('#createPriceModel').modal('hide');
                $('#message').text(data.message + ".");
                $('#messageSave').modal('show');
                setTimeout(function () {
                    $('#messageSave').modal('hide');
                }, 3000);
                row_price.css("background-color", "rgb(0 255 125 / 3%)");
                setTimeout(function () {
                    row_price.css("background-color", "rgb(255 255 255 / 0%)");
                }, 11000);
            },
            error: function (data) {
                $('#' + _type + '').text(type);
            }

        });
    });
    $('#my_form_add').on('submit', function (event) {
        var route = "{{  route('add.store')}}";
        /*
        var formData = new FormData(this);
        formData.append('no', 1);
        */
        var formData = new FormData($('#my_form_add')[0]);
        event.preventDefault();

        var type = "حفظ";
        var _type = "button_save";

        $.ajax({
            url: route,
            method: "POST",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#' + _type).html('<i class="la la-spinner spinner"></i><span>جاري ال' + type + '...</span>')
            },
            success: function (data) {
                $('#' + _type + '').text(type);
                $('#createAddModel').modal('hide');
                $('#message').text(data.message + ".");
                $('#messageSave').modal('show');
                setTimeout(function () {
                    $('#messageSave').modal('hide');
                }, 3000);
                row_price.css("background-color", "rgb(0 255 125 / 3%)");
                setTimeout(function () {
                    row_price.css("background-color", "rgb(255 255 255 / 0%)");
                }, 11000);
            },
            error: function (data) {
                $('#' + _type + '').text(type);
            }

        });
    });

    function loadAvatarImag(input) {
        let num = input.getAttribute('data-num');
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image-' + num).attr('hidden', false);
                $('#delete-img-' + num).attr('hidden', false);
                $('#input-text-' + num).val('full');
                /*document.getElementById('image-name').value = e.target.result;*/
                var image = document.getElementById('image-' + num);
                image.src = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('click', '.delete_img', function () {
        let num = $(this).attr('data-num');
        $('#image-' + num).attr('hidden', true);
        $('#delete-img-' + num).attr('hidden', true);
        $('#input-text-' + num).val("");
        $(this).parent().parent().find('input[type=file]').val("");
    });

    function checkSelected(currency, s) {
        let selected = '';
        if (currency === s) selected = "selected";

        return selected;
    }

    let data_send = null;

    /* edit product images*/
    $(document).on('click', '.edit-image', function () {
        let product_id = $(this).attr('id');
        load_images(product_id);
        $('input[name=_id]').val(product_id);
        $('#dropzoneForm div').remove();
        $('#dropzoneForm').append('<div class="dz-default dz-message"><span>Drop files here to upload</span></div>');
        /*$('#dropzoneForm div:not(:first)').remove();*/
        $('#imagesModal').modal('show');
    });

    function load_images(id) {
        $.ajax({
            url: "{{ url('dropzone/fetch')}}" + '/' + id,
            success: function (data) {
                $('.uploaded_image').html(data);
            }
        })
    }

    $(document).on('click', '#btn-refresh', function () {
        let id = $('input[name=_id]').val();
        load_images(id);
        $('#dropzoneForm div').remove();
        $('#dropzoneForm').append('<div class="dz-default dz-message"><span>Drop files here to upload</span></div>');

    });

    $(document).on('click', '.remove_image', function () {
        var btn = $(this);

        var name = btn.attr('data-path');
        var id = btn.attr('data-id');
        $.ajax({
            url: "{{ route('dropzone.delete') }}",
            data: {
                name: name,
                id: id
            },
            beforeSend: function () {
                btn.html('<i class="la la-spinner spinner"></i>')
            },
            success: function (id) {
                load_images(id);
                btn.html('<i class="ft-trash-2"></i>');
            },
            error: function (data) {
                btn.html('<i class="ft-trash-2"></i>');
            },
        })
    });


</script>


<script>
    $(document).on('click', '#openModalAppExal', function () {
        $('select[name=branch_id],select[name=type], input[name=file_products]').removeClass('is-invalid').val('');
        $('#form-add-excel span strong').empty();
        $('#addModal').modal('show');
    });

    $(document).on('click', '#exportModalAppExcel', function () {
        $('select[name=branch_id_price]').removeClass('is-invalid').val('');
        $('#form-add-excel-price span strong').empty();
        $('#addModalPrice').modal('show');
    });

    $('#form-add-excel').on('submit', async function (event) {
        event.preventDefault();
        let route = "{{route('product.add-from-excel')}}";
        let formData = new FormData($('#form-add-excel')[0]);

        $.ajax({
            url: route,
            method: "POST",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#save').attr('disabled', true).html('<i class="la la-spinner spinner"></i><span>{{__('admin.saving')}}...</span>')
            },
            success: function (data) {
                $('#save').attr('disabled', false).html('<i class="la la-save"></i> {{__('admin.add')}}');
                $('#data_table').DataTable().draw();
                $('#addModal').modal('hide');
                swal({
                    icon: "success",
                    text: data.message + "!",
                    title: "تم الحفظ" + " !",
                    button: 'موافق'
                });
            },
            error: function (error) {
                $('#save').attr('disabled', false).html('<i class="la la-save"></i> {{__('admin.add')}}');
                if (error.status === 500) {
                    swal({
                        icon: "error",
                        text: "هناك تكرار في المنتجات أو الملف غير صحيح !",
                        title: "لم يتم الحفظ!",
                        button: 'موافق'
                    });
                }

                let obj = JSON.parse((error.responseText)).error;
                if (obj.branch_id !== undefined) {
                    $('#form-add-excel select[name=branch_id]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.branch_id);
                }
                if (obj.file_products !== undefined) {
                    $('#form-add-excel input[name=file_products]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.file_products);
                }
            }

        });
    });

    $('#button-delete-all').on('click', async function () {
        let items_id = $('#table').find("input[name ='items-id[]']:checkbox:checked")
            .map(function () {
                return $(this).val()
            }).get();

        let route = "{{route('product.destroy-all')}}";

        try {
            await deletedItems(items_id, route);
            window.location.reload();
            $('#table').table().ajax.reload();
            $('#check-all-products').iCheck('uncheck');

        } catch (e) {
            return e;
        }

    });

    /* Check All checkbox start*/
    $(document).on("ifClicked", 'input#check-all-products', function () {
        var checkboxes = $("input.input-chk");
        $(document).on("ifChecked", 'input#check-all-products', function (event) {
            checkboxes.iCheck("check");
        });
        $(document).on("ifUnchecked", 'input#check-all-products', function (event) {
            checkboxes.iCheck("uncheck");
        });
    });

    /* Delete row*/
    $('#users-contacts').on("click", ".delete", function () {
        userDataTable.row($(this).parents('tr')).remove().draw();
    });

    $(document).on("click", ".delete-all", function () {
        userDataTable.rows($("#users-contacts").find(".checked").closest("tr")).remove().draw();
        $("input#check-all-products").iCheck("uncheck");
    });

    function delete_img() {
        document.getElementById('edit_image').value = null;
        $('#edit-product-image').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
        document.getElementById('image-name').value = null;
    }

    function loadAvatar(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $('#edit-product-image').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                document.getElementById('image-name').value = e.target.result;
                let image = document.getElementById('edit-product-image');
                image.src = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    /*start code edit*/
    let product_edit_id = 0;
    let edit_row;
    $(document).on('click', '.edit-row', async function () {
        product_edit_id = $(this).attr('id');
        edit_row = $(this).parent().parent().parent().parent();
        let url = "{{url('/app/product/edit')}}" + '/' + product_edit_id;
        $('#form input[type=text]:not(:first), #form input[type=number], #form textarea, #form select')
            .val('')
            .removeClass('is-invalid');
        $('#edit-product-image').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('/app/product/update')}}" + '/' + product_edit_id)
            .attr("data_type", "Update");

        $('#ar_name').attr('disabled', false);
        $('#en_name').attr('disabled', false);
        $('#ar_details').attr('disabled', false);
        $('#en_details').attr('disabled', false);
        $('#number').attr('disabled', false);
        $('#branch_id').attr('disabled', false);
        $('#category_id').attr('disabled', false);
        $('#materials_id').attr('disabled', false);
        $('#edit_image').attr('disabled', false);

        try {
            let data = await responseEditOrShowData(url);
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");
            if (data.is_new == 1) {
                $('#edit_new').iCheck("check");
            } else {
                $('#edit_new').iCheck("uncheck");
            }

            if (data.status == 1) {
                $('#available').attr('checked', true);
            } else {
                $('#un_available').attr('checked', true);
            }

            document.getElementById('ar_name').value = data.name_ar;
            document.getElementById('en_name').value = data.name_en;
            document.getElementById('ar_details').value = data.details_ar;
            document.getElementById('en_details').value = data.details_en;
            document.getElementById('number').value = data.number;
            $('#branch_id').val(data.branch_id);
            $('#input_branch_id').val(data.branch_id);
            document.getElementById('category_id').value = data.category_id;
            document.getElementById('materials_id').value = data.materials_id;

            /*if ((data.is_approved === 1)) {
                $('#ar_name').attr('disabled', true);
                $('#en_name').attr('disabled', true);
                $('#ar_details').attr('disabled', true);
                $('#en_details').attr('disabled', true);
                $('#number').attr('disabled', true);
                $('#branch_id').attr('disabled', true);
                $('#category_id').attr('disabled', true);
                $('#edit_image').attr('disabled', false);
            }*/

            if (data.image === null) {
                $('#edit-product-image').attr('hidden', true);
                $('#delete-img').attr('hidden', true);

            } else {
                $('#edit-product-image').attr('src', data.image).attr('hidden', false);
                $('#delete-img').attr('hidden', false);
            }
            $('#editProductModal').modal('show');

        } catch (error) {
            return error;
        }

    });
    /*end code edit*/

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                /*location.reload();*/
                /*let response_data = JSON.parse((data.responseText)).carrier;*/
                let response_data = data.product;

                $('#editProductModal').modal('hide');

                let tr_color_red = response_data.class_color_row;
                let status;
                let is_new;
                if (response_data.status == 1) {
                    status = "<div class='fonticon-wrap'><i class='ft-unlock color-primary' style='color:#0679f0'></i></div>";
                } else {
                    tr_color_red = 'tr-color-red';
                    status = "<div class='fonticon-wrap'><i class='ft-lock color-red' style='color:#FC0021'></i></div>";
                }
                if (response_data.is_new == 1) {
                    is_new = "    <button @can('update product') @else disabled @endcan class='btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2' " +
                        "            id='btnGroupDrop1' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> " +
                        "        {{__('admin.new')}} " +
                        "    </button> ";
                } else {
                    is_new = "    <button @can('update product') @else disabled @endcan class='btn btn-sm btn-cyan dropdown-toggle dropdown-menu-right box-shadow-2' " +
                        "            id='btnGroupDrop1' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> " +
                        "        {{__('admin.old')}} " +
                        "    </button> "
                }
                let product_name = response_data.name + is_new +
                    "<div class='dropdown-menu' aria-labelledby='btnGroupDrop1'> " +
                    "    <button class='dropdown-item statusClick' data-id-row='" + response_data.id + "' data-status='new'>{{__('admin.new')}}</button> " +
                    "    <button class='dropdown-item statusClick' data-id-row='" + response_data.id + "' data-status='old'>{{__('admin.old')}}</button> " +
                    "</div>";

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $('<td><input name="items-id[]" value="' + response_data.id + '" type="checkbox" class="input-chk" id="check-all' + response_data.id + '" ></td>');
                let col2 = $("<td class='font-default'>" + response_data.number + "</td>");
                let col3 = $("<td>" + response_data.branch.name + "</td>");
                let col4 = $("<td>" + product_name + "</td>");
                let col5 = $("<td>" + response_data.price + "</td>");
                let col6 = $("<td>" + status + "</td>");
                let col7 = $('<td><img style="max-width: 60px" src="' + response_data.image_path_64 + '"' +
                    'onerror=this.src="{{ url('/storage/thumbnail/150/logo.png')}}"/></td>'
                );

                {{--let col7 = $('<td><img style="max-width: 60px" src="{{asset('storage/thumbnail/64')}}/' + response_data.image + '"' +--}}
                {{--    ' onerror=this.src="{{asset('storage/thumbnail/150')}}/default-carrier.jpg"/></td>'--}}
                {{--);--}}

                {{--let col8 = $('<td><img style="max-width: 60px" src="{{asset('storage/thumbnail/64')}}/' + response_data.image + '"' +--}}
                {{--    'onerror=this.src="{{asset('storage/thumbnail/150/logo.png')}}"/></td>'--}}
                {{--);--}}
                let col8 = $("<td>" + response_data.actions_branch + "</td>");

                let this_row;

                this_row = edit_row;
                edit_row.attr("class", tr_color_red).attr('style', '');
                edit_row.empty().append( col1, col2, col3, col4, col5, col6,col7,col8);
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    increaseArea: '20%'
                });

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).errors;
            if (obj.name_ar !== undefined) {
                $('#form input[name=name_ar]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.name_ar);
            }
            if (obj.name_en !== undefined) {
                $('#form input[name=name_en]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.name_en);
            }
            if (obj.image !== undefined) {
                $('#form input[name=image]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.image);
            }
            if (obj.details_en !== undefined) {
                $('#form textarea[name=details_en]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.details_en);
            }
            if (obj.details_ar !== undefined) {
                $('#form textarea[name=details_ar]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.details_ar);
            }
            if (obj.number !== undefined) {
                $('#form input[name=number]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.number);
            }
            if (obj.branch_id !== undefined) {
                $('#form select[name=branch_id]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.branch_id);
            }
            if (obj.category_id !== undefined) {
                $('#form select[name=category_id]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.category_id);
            }
        }
    });
    /*end code add or update*/
    $(document).on('click', '.statusClick', function () {
        let td = $(this).parent().parent().parent();

        let is_new = $(this).attr('data-status');
        let id_row = $(this).attr('data-id-row');
        let route = "{{url('app/products/update')}}" + "/" + id_row;
        $.ajax({
            type: 'POST',
            data: {
                _token: $('input[name ="_token"]').val(),
                is_new: is_new,
            },
            url: route,
            dataType: "json",
            success: function (data) {
                let status_append = statusAppend(data.id, data.is_new);
                td.empty().html(data.name + status_append);
                td.parent().attr('style', data.background_color_row);
            },
        });
    });

    function statusAppend(id, is_new) {

        let status_append = '<div class="float-right btn-group" role="group" aria-label="Button group with nested dropdown">';
        if (is_new == 1) {
            status_append +=
                '<button class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2"' +
                '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                '   {{__('admin.new')}}' +
                '</button>';
        } else {
            status_append +=
                '<button class="btn btn-sm btn-cyan dropdown-toggle dropdown-menu-right box-shadow-2"' +
                '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                '    {{__('admin.old')}}' +
                '</button>';
        }

        status_append +=
            '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
            '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="new">{{__('admin.new')}}</button>' +
            '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="old">{{__('admin.old')}}</button>' +
            '    </div>' +
            '</div>';

        return status_append;
    }

</script>
