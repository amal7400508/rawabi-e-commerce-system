{{-- start model createAdd  Message--}}
<div id="createAddModel" class="modal fade text-left" role="dialog">
    <div class="modal-dialog" style="max-width: 80%;">
        <div class="modal-content">
            <div class="">
                <div class="card-content">
                    <div class="card-header" style="padding-bottom: 0;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                        </button>
                        <h3>{{__('admin.create')}} {{__('admin.addition')}}</h3>
                    </div>
                    <hr>
                    <form class="form form-horizontal" action="{{ route('add.store')}}" method="POST"
                          id="my_form_add" enctype="multipart/form-data">
                        @csrf
                        <input type="number" id="product_id_value_add" name="product_id_add" value="" hidden>
                        <div class="form-body-rep card-body">
                            <div class="row">
                                <div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.unit_type')}} <span class="danger">*</span></label>
                                    <br>
                                    <select class="form-control all-typeUnit" data-typeUnit-order="0" required>
                                        <option value="">{{__('admin.select')}}</option>
                                        <option value="1">{{__('admin.size')}}</option>
                                        <option value="2">{{__('admin.ball')}}</option>
                                        <option value="3">{{__('admin.weight')}}</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.unit_type')}} <span class="danger">*</span></label>
                                    <br>
                                    <select class="form-control all-typeUnit" data-typeUnit-order="0">
                                        <option value="">{{__('admin.select')}}</option>
                                        <option value="1">{{__('admin.size')}}</option>
                                        <option value="2">{{__('admin.ball')}}</option>
                                        <option value="3">{{__('admin.weight')}}</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.unit')}} <span class="danger">*</span></label>
                                    <br>
                                    <select class="form-control" id="unitAll-0" name="unit_id[]">
                                    </select>
                                </div>
                                {{--<div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.taste')}} <span class="danger">*</span></label>
                                    <br>
                                    --}}{{--<select class="form-control" id="profession" required name="--}}{{----}}{{--taste[]--}}{{----}}{{--">
                                        <option value="">{{__('admin.select')}}</option>
--}}{{----}}{{--                                        @foreach($taste as $tastea)--}}{{----}}{{--
--}}{{----}}{{--                                            @if($tastea->id != 1)--}}{{----}}{{--
                                                <option value="--}}{{----}}{{--{{$tastea->id}}--}}{{----}}{{--">--}}{{----}}{{--{{$tastea->taste}}--}}{{----}}{{--</option>
--}}{{----}}{{--                                            @endif--}}{{----}}{{--
--}}{{----}}{{--                                        @endforeach--}}{{----}}{{--
                                    </select>--}}{{--
                                </div>--}}
                                <div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.price')}} <span class="danger">*</span></label>
                                    <br>
                                    <input type="number" class="form-control @error('price[]') is-invalid @enderror"
                                           value="{{ old('price[]') }}" required name="price[]" min="0"
                                           max="99999999.99">
                                    @error('price[]')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-sm-12 col-md-6">
                                    <label>{{__('admin.image')}}</label>
                                    <br>
                                    <input type="file" class="form-control" name="image[]"
                                           accept="image/png, image/jpeg">
                                </div>
                                <div class="form-group col-sm-12 col-md-3">
                                    <div class="heading-elements" id="check_show" style="margin: 5px">
                                        <br>
                                        <div style="padding-top: 6px"></div>
                                        <input type="checkbox" name="status[]" value="1" class="switchBootstrap"
                                               id="switchBootstrap18" data-on-color="primary" data-off-color="danger"
                                               data-on-text="{{__('admin.enable')}}"
                                               data-off-text="{{__('admin.disable')}}" data-color="primary"
                                               data-label-text="{{__('admin.status')}}" checked/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('create product')
                            <div class="col-12">
                                <button type="button" id="addRowAdd" class="addRow btn btn-primary"><i
                                        class="ft-plus"></i> {{__('admin.add')}}</button>
                            </div>
                        @endcan
                        <div class="form-actions text-right">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="col-md-12 btn btn-primary" id="button_save">
                                        <i class="fa-save"></i> {{__('admin.save')}}
                                    </button>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model createAdd Message--}}
