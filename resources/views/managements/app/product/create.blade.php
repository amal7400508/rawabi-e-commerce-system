@php($page_title = __('admin.product_branch'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block"> {{__('admin.products')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/app/product')}}">{{__('admin.products')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.create')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="col-md-12">
            <div class="card">
                <form class="form" action="{{route('product.store')}}" method="POST" id="my_form_id"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="card-header">
                        <h4 class="card-title"
                            id="from-actions-top-bottom-center">{{__('admin.create')}} {{__('admin.product')}}</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <label for="switcherySize2" class="font-medium-2 text-bold-600 ml-1"
                                       style="margin-left: 10px;margin-right: 10px;">{{__('admin.new')}}</label>
                                <input type="checkbox" id="switcherySize2" value="1" class="switchery" data-size="sm"
                                       data-color="primary" name="is_new"/>
                                <div class="form-group mt-1">
                                </div>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('branch_id') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.branch_name')}}
                                                        <span class="danger">*</span></label>
                                                    <br>
                                                    <select
                                                        class="form-control @error('branch_id') is-invalid @enderror"
                                                        id="branch_id" required name="branch_id">
                                                        <option value="">{{__('admin.select_option')}}</option>
                                                        @foreach($branches as $branch)
                                                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('branch_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        <div class="col-md-12">
                                            <div
                                                class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                                <label for="profession">{{__('admin.categories')}} <span
                                                        class="danger">*</span> </label>
                                                <br>
                                                <select class="form-control @error('category_id') is-invalid @enderror"
                                                        id="category_id" required name="category_id">
                                                    <option value="">{{__('admin.select_option')}}</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                                @error('category_id')
                                                <span class="invalid-feedback" role="alert">
                                                   <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('number') ? ' has-error' : '' }}">
                                                <label for="projectinput2">{{__('admin.item_code')}} <span
                                                        class="danger">*</span></label>
                                                <input type="text" id="edit_number" required
                                                       value="{{ old('number') }}"
                                                       class="form-control @error('number') is-invalid @enderror"
                                                       name="number">
                                                @error('number')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                                <label for="projectinput2">{{__('admin.name')}} (ar) <span
                                                        class="danger">*</span></label>
                                                <input type="text" id="edit_name_ar" required
                                                       value="{{ old('ar_name') }}"
                                                       class="form-control @error('ar_name') is-invalid @enderror"
                                                       name="ar_name">
                                                @error('ar_name')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                                <label for="projectinput2">{{__('admin.name')}} (en) <span
                                                        class="danger">*</span></label>
                                                <input type="text" id="edit_name_en" required
                                                       value="{{ old('en_name') }}"
                                                       class="form-control @error('en_name') is-invalid @enderror"
                                                       name="en_name">
                                                @error('en_name')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- <div class="form-group col-12">
                                             <label for="switcherySize2" class=" ml-0"
                                                    style="margin: 0  8%">{{__('admin.this_product_is_available_in_city')}}</label>
                                             <input type="checkbox" value="2" class="switchery" data-size="sm"
                                                    data-color="primary" name="state_id" />
                                         </div>--}}

                                        {{--<div class="form-group col-md-12">
                                            <div class="row">
                                                <label class="col-md-5 label-control">{{__('admin.this_product_is_available_in_city')}}
                                                    <span class="danger">*</span></label>
                                                <div class="col-md-7 mx-auto">
                                                    <div class="input-group">
                                                        <div class="d-inline-block custom-control custom-checkbox mr-1">
                                                            <input type="checkbox" name="city[]" value="sana_a" class="custom-control-input" checked id="sana_a">
                                                            <label class="custom-control-label cursor-pointer" for="sana_a">{{__('admin.sana_a')}}</label>
                                                        </div>
                                                        <div class="d-inline-block custom-control custom-checkbox">
                                                            <input type="checkbox" name="city[]" value="ibb" class="custom-control-input" id="ibb">
                                                            <label class="custom-control-label cursor-pointer" for="ibb">{{__('admin.ibb')}}</label>
                                                        </div>
                                                    </div>
                                                    <span class="error-massege">
                                                        <strong id="error_day"></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>--}}

                                        <div class="col-md-12">
                                            <div
                                                class="form-group {{ $errors->has('materials_id') ? ' has-error' : '' }}">
                                                <label for="profession">{{__('admin.material')}} <span
                                                        class="danger">*</span> </label>
                                                <br>
                                                <select class="form-control @error('materials_id') is-invalid @enderror"
                                                        id="materials_id" required name="materials_id">
                                                    <option value="">{{__('admin.select_option')}}</option>
                                                    @foreach($materials as $material)
                                                        <option value="{{$material->id}}">{{$material->name}}</option>
                                                    @endforeach
                                                </select>
                                                @error('materials_id')
                                                <span class="invalid-feedback" role="alert">
                                                   <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>



                                        {{--                                        <div class="form-group col-12">--}}
                                        {{--                                            <label>{{__('admin.status')}}</label>--}}
                                        {{--                                            <div class="input-group">--}}
                                        {{--                                                <div class="row icheck_minimal skin">--}}
                                        {{--                                                    <fieldset style="margin-left: 20px; margin-right: 20px">--}}
                                        {{--                                                        <input type="radio" name="status" id="input-radio-15" required--}}
                                        {{--                                                               value="1">--}}
                                        {{--                                                        <label style="color: #002581">{{__('admin.available')}}</label>--}}
                                        {{--                                                    </fieldset>--}}
                                        {{--                                                    <fieldset style="margin-left: 20px; margin-right: 20px">--}}
                                        {{--                                                        <input type="radio" name="status" id="input-radio-16" required--}}
                                        {{--                                                               value="0">--}}
                                        {{--                                                        <label--}}
                                        {{--                                                            style="color: #FFC107;">{{__('admin.un_available')}}</label>--}}
                                        {{--                                                    </fieldset>--}}
                                        {{--                                                </div>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}

                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                                <label for="projectinput2">{{__('admin.image')}} </label>
                                                <input type="file" accept=".jpg, .jpeg, .png" id="input-file"
                                                       class="form-control @error('image') is-invalid @enderror"
                                                       name="image" onchange="loadAvatar(this);" autocomplete="image"
                                                       autofocus> <span
                                                    style="font-size: 13px; margin: 7px">{{__('admin.please_enter_square_image')}}</span>
                                                <br>
                                                <button type="button" hidden onclick="delete_img()"
                                                        style="position: static; " class="btn btn-sm btn-outline-danger"
                                                        id="delete-img"><i class="ft-trash"></i></button>
                                                <img id="avatar" style="max-width: 140px; height: auto; margin:10px;">
                                                @error('image')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div
                                                class="form-group {{ $errors->has('ar_details') ? ' has-error' : '' }}">
                                                <label for="projectinput2">{{__('admin.details')}} (ar) <span
                                                        class="danger"></span></label>
                                                <textarea rows="4" id="edit_details_ar"
                                                          class="form-control @error('ar_details') is-invalid @enderror"
                                                          name="ar_details">{{ old('ar_details') }}</textarea>
                                                @error('ar_details')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div
                                                class="form-group {{ $errors->has('en_details') ? ' has-error' : '' }}">
                                                <label for="projectinput2">{{__('admin.details')}} (en) <span
                                                        class="danger"></span></label>
                                                <textarea rows="4" id="edit_details_en"
                                                          class="form-control @error('en_details') is-invalid @enderror"
                                                          name="en_details">{{ old('en_details') }}</textarea>
                                                @error('en_details')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <br>

                                        <div class="form-group col-md-12">
                                            <div class="row">
                                                <label class="col-md-5 label-control">{{__('admin.status')}}</label>
                                                <div class="col-md-7 mx-auto">
                                                    <div class="input-group">
                                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                                            <input type="radio" name="status" value="1"
                                                                   class="custom-control-input" checked id="available">
                                                            <label class="custom-control-label cursor-pointer"
                                                                   for="available">{{__('admin.available')}}</label>
                                                        </div>
                                                        <div class="d-inline-block custom-control custom-radio">
                                                            <input type="radio" name="status" value="0"
                                                                   class="custom-control-input" id="un_available">
                                                            <label class="custom-control-label cursor-pointer"
                                                                   for="un_available">{{__('admin.un_available')}}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="col-md-2 btn btn-primary pull-right" id="button_save">
                                    <i class="ft ft-save"></i> {{__('admin.save')}}
                                </button>
                                <a href="{{ url('/app/product')}}" class="col-md-1 btn btn-dark">
                                    <i class="ft-x"></i> {{__('admin.cancel')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        /*ajax falter categories start*/
        {{--$(document).on('change', '#branch_id', function () {--}}
        {{--    let category_id = $(this).val();--}}
        {{--    let category_url = "{{url('/app/product/falter_main_category')}}" + "/" + category_id;--}}
        {{--    let element_product = $('#category_id');--}}
        {{--    if (category_id) {--}}
        {{--        $.ajax({--}}
        {{--            type: "GET",--}}
        {{--            url: category_url,--}}
        {{--            success: function (data) {--}}
        {{--                if (data) {--}}
        {{--                    element_product.empty();--}}
        {{--                    element_product.append('<option value="">{{__("admin.select_option")}}</option>');--}}
        {{--                    $.each(data, function (key, value) {--}}
        {{--                        element_product.append('<option value="' + value.id + '">' + value.name + '</option>');--}}
        {{--                    });--}}
        {{--                } else {--}}
        {{--                    element_product.empty();--}}
        {{--                }--}}
        {{--            }--}}
        {{--        });--}}
        {{--    } else {--}}
        {{--        element_product.empty();--}}
        {{--    }--}}
        {{--});--}}

        /*ajax falter categories end*/

        function delete_img() {
            document.getElementById('input-file').value = null;
            $('#avatar').attr('hidden', true);
            $('#delete-img').attr('hidden', true);
        }

        function loadAvatar(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#avatar').attr('hidden', false);
                    $('#delete-img').attr('hidden', false);
                    var image = document.getElementById('avatar');
                    image.src = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('click', '#button_save', function () {
            document.getElementById('my_form_id').submit();
            $('#button_save').attr('disabled', true);
        });
        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        /*end  message save*/

        $(document).ready(function () {
            @if(count($errors)>0)
            {{--            @if(is_null(old('city.0')) and is_null(old('city.1')))--}}
            {{--            $('#error_day').text('{{__('validation.required')}}');--}}
            {{--            $('#sana_a').iCheck('uncheck');--}}
            {{--            $('#ibb').iCheck('uncheck');--}}
            {{--            @elseif(old('city.0') == 'ibb')--}}
            {{--            $('#ibb').iCheck('check');--}}
            {{--            $('#sana_a').iCheck('uncheck');--}}
            {{--            console.log('ibb');--}}
            {{--            @elseif(old('city.1') == 'sana_a')--}}
            {{--            $('#sana_a').iCheck('check');--}}
            {{--            $('#ibb').iCheck('uncheck');--}}
            {{--            console.log('sana_a');--}}
            {{--            @endif--}}
            @if(old('status')== 1)
            $('#input-radio-15').iCheck('check');
            @elseif(old('status')== 0)
            $('#input-radio-16').iCheck('check');
            @endif

            {{--            console.log('{{dd(session()->getOldInput()) }}');--}}
            {{--            console.log('{{dd(issold('city.1') ) }}');--}}
            console.log('{{old('city.0')}}');
            document.getElementById('category_id').value = "{{old('category_id')}}";
            document.getElementById('branch_id').value = "{{old('branch_id')}}";
            document.getElementById('type').value = "{{old('type')}}";
            @endif
        });


    </script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/checkbox-radio.js')}}"></script>

@endsection
