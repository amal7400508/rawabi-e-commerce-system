<style>
    .dropzone .dz-message {
        margin-top: -155px;
    }

    .dropzone.dz-clickable .dz-message, .dropzone.dz-clickable .dz-message * {
        /*margin-top: -230px;*/
        top: 347px;
    }

    #wizard > div.content {
        margin-right: 0;
        height: fit-content;
        height: -moz-fit-content;
    }

    .wrapper {
        height: auto;
    }

    .wizard > .steps {
        top: 5%;
        right: 55%;
    }

    .wizard > .steps li a {
        margin-left: 78px;
        margin-right: auto;
    }

    .wizard > .steps li a:before {
        left: 22px;
        right: auto;
    }

    #wizard > div.action {
        left: 51px;
        right: auto;
    }

    .actions li:first-child a:before,
    .actions li:last-child a:before,
    .actions li a:before {
        content: '';
    }

    .actions li:nth-child(2) a:hover, .actions li:last-child a:hover,
    .actions li[aria-disabled="false"] ~ li a:hover {
        background-color: #5e56a2 !important;
    }

    .actions li:nth-child(2) a, .actions li:last-child a,
    .actions li:first-child a, .my-finish,
    .actions li[aria-disabled="false"] ~ li a {
        background-color: #342D77 !important;
        align-items: flex-start;
        background-color: rgb(52, 45, 119);
        border-bottom-color: rgb(58, 68, 225);
        border-bottom-left-radius: 2.94px;
        border-bottom-right-radius: 2.94px;
        border-bottom-style: solid;
        border-bottom-width: 1px;
        border-top-left-radius: 2.94px;
        border-top-right-radius: 2.94px;
        /*border-top-style:solid;*/
        border-top-width: 1px;
        box-sizing: border-box;
        color: rgb(255, 255, 255);
        cursor: pointer;
        direction: rtl;
        display: inline-block;
        font-family: Booking;
        font-size: 12.25px;

        font-weight: 400;
        padding-bottom: 7px;
        padding-left: 10.5px;
        padding-right: 10.5px;
        padding-top: 7px;
        text-align: center;
        height: 36px;
        margin-left: 10px;
    }

    .actions li:first-child a,
    .actions li:first-child a:hover {
        background-color: #e9e0cf !important;
        border-bottom-color: rgb(233, 244, 255);
        color: #0b0b0b;
    }

    .wizard > .steps li a:after {
        background: #fff;
    }

    .wizard > .steps li.first a,
    .wizard > .steps li.checked a {
        background-color: #342D77 !important;
    }

    .form-content {
        width: 100%;
        padding-top: 65px;
        padding-left: 51px;
        padding-right: 51px;
    }

    h3 {
        margin-top: 20px;
        letter-spacing: 1px;
    }

    .my-form-content {
        padding-top: 0;
    }

    .remove_image:hover {
        background-color: #ff0c0c !important;
    }

    .remove_image {
        margin-right: -38px;
        margin-top: 8px;
        background-color: #FF4961 !important;
        border-color: #ff0c0c !important;
        position: absolute;
        padding: 7px 8px;
        color: #fff !important;
    }

    @media screen and (max-width: 768px) {
        .modal-wide {
            max-width: 100%;
        }

        .actions li:nth-child(2) a, .actions li:last-child a,
        .actions li:first-child a, .my-finish,
        .actions li[aria-disabled="false"] ~ li a {
            margin-right: -24px;
        }

        .dropzone.dz-clickable .dz-message, .dropzone.dz-clickable .dz-message * {
            margin-top: -230px;
        }

        .dropzone.dz-clickable .dz-message, .dropzone.dz-clickable .dz-message * {
            margin-top: -365px;
        }
    }
</style>
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/extensions/toastr.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/extensions/toastr.css')}}">

<div class="modal fade" id="imagesModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-wide long-mode" role="document">
        <div class="modal-content">
            <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                    style=" @if( app()->getLocale() == 'ar')  margin-right: 472px; @else margin-left: 480px @endif"
                    data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" style="margin: 6px">&times;</span>
            </button>
            <div id="div-images">
                <section>
                    <div class="inner">
                        <div class="form-content my-form-content">
                            <div id="messageSave" class="modal fade text-left" role="dialog">
                                <div class="modal-dialog">
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                                <strong>{{__('admin.successfully_done')}}!</strong>
                                                <p>نم رفع الخدمة بنجاح.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-header">
                                <h3 id="serve-name">إدخل الصور الفرعية للخدمة</h3>
                            </div>
                            <div style="margin-bottom: 30px;">
                                <div class="container-fluid">
                                    <div class="panel panel-default">
                                        <div class="panel-body" id="body-add-imag">
                                            <form id="dropzoneForm" class="dropzone" action="{{ route('dropzone.upload') }}">
                                                @csrf
                                                <input type="hidden" value="" name="_id" id=_id">
                                            </form>
                                            <div align="center" style="    margin-top: 5px;">
{{--                                                <button type="button" class="btn btn-info submit-all" id="submit-all">رفع الصور</button>--}}
                                                <span class="btn btn-primary" id="btn-refresh"><i class="ft-refresh-ccw"> تحديث </i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title span">الصور المرفوعة</h3>
                                        </div>
                                        <div class="panel-body uploaded_image" id="uploaded_image">

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
{{--                    <div style="margin-top: 90px">--}}
{{--                        <div class="actions clearfix">--}}
{{--                            <ul role="menu" aria-label="Pagination" class="span">--}}
{{--                                <span style="width:150px" class="my-finish" id="my-finish">إنهاء</span>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                </section>
            </div>
        </div>
    </div>
</div>
