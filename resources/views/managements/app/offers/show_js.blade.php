<script>
    /*start code show details edit ajax*/
    $(document).on('click', '.show-detail-offer', async function () {
        let offer_id = $(this).attr('id');
        deposit_row = $(this).parent().parent();
        let url = "{{url('app/offers/show')}}" + "/" + offer_id;

        try {
            let data = await responseEditOrShowData(url);

            $("#offer_details_tbody").empty();
            for (i = 0; i < data.offer_details.length; i++) {
                var detail_note = data.offer_details[i].note ? data.offer_details[i].note : '';
                $("#offer_details_table").append("<tr><td>" + data.offer_details[i].price_name + "</td><td>" + data.offer_details[i].quantity + "</td><td>" + detail_note + "</td></tr>");
            }
            if (data.status == 1) {
                $('#show_status').html('<span class="btn btn-primary position-absolute round" style=" margin: 10px; " ><i class="ft-unlock"></i></span>');
            } else {
                $('#show_status').html('<span class="btn btn-danger position-absolute round" style=" margin: 10px; " ><i class="ft-lock"></i></span>');
            }
            var type_name;
            if (data.type_id != 1) {
                type_name = '<button class="btn btn-sm btn-outline-blue" id="name_type">' + data.name_type + '</button>';
            } else {
                type_name = '';
            }
            $('#id').text(data.id);
            $('#old_price').text(data.currency + " " + data.old_price);
            $('#price').text(data.currency + " " + data.price);
            $('#start_date').text(data.start_date);
            $('#end_date').text(data.end_date);
            $('#offer_name').text(data.offer_name);
            $('#name_type').html(type_name);
            $('.created').text(data.created_at);
            $('.updated_at').html(data.updated_at);
            $('#show-image').attr('src', data.image);

            $('#confirmModalShowOffer').modal('show');

        } catch (error) {
            return error;
        }
    });
    /*end code show details edit ajax*/
</script>
