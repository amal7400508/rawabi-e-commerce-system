<script>
    let table_show = $('#table-show');
    let table_count = $('#table-count');

    function delete_img() {
        document.getElementById('edit_image_ar').value = null;
        $('#avatar').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
    }

    function loadAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                var image = document.getElementById('avatar');
                image.src = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    @can('create offer')
    /*start code open add modal*/
    $(document).on('click', '#openAddModal', function () {
        $('#table-offer-detail tbody').empty().append(
            '<tr id="row-not-found">' +
            ' <td colspan="4" class="text-center"><span>{{__('admin.no_data')}}</span><hr></td>' +
            '</tr>'
        );

        edit_row = $(this).parent().parent();
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#form select[name=offer_type]').val('');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);
        delete_img();
        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('offers.store')}}")
            .attr("data_type", "add");
    });
    /*start code open add modal*/
    @endcan

    @can('update offer')
    /*start code edit*/
    let offer_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        offer_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('app/offers/edit')}}" + '/' + offer_id;
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');

        $('#avatar').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('app/offers/update')}}" + '/' + offer_id)
            .attr("data_type", "Update")
            .attr("data_id", offer_id);

        try {
            let data = await responseEditOrShowData(url);

            $('#edit_image_ar').attr('required', false);
            $('#label-image').text("{{__('admin.image')}}");
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            document.getElementById('branch_id').value = data.branch_id;
            document.getElementById('name_ar').value = data.name_ar;
            document.getElementById('name_en').value = data.name_en;
            document.getElementById('old-price').value = data.old_price;
            document.getElementById('new_price').value = data.new_price;
            document.getElementById('start-date').value = data.start_date;
            document.getElementById('end-date').value = data.end_date;
            document.getElementById('currency').value = data.currency;
            document.getElementById('offer_type').value = data.type_id;
            $('#avatar').attr('src', data.image).attr('hidden', false);
            $('#delete-img').attr('hidden', false);

            if (data.status == 1) {
                $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                    .bootstrapSwitch('state', true);
            } else {
                $('#switchBootstrap18').attr('value', 1).attr('checked', false)
                    .bootstrapSwitch('state', false);
            }

            $('#table-offer-detail tbody').empty();
            $.each(data.offer_details, function (key, value) {
                $('#table-offer-detail tbody').append(
                    '<tr>' +
                    '<td>' + value.price_name + '</td>' +
                    '<td>' + value.quantity + '</td>' +
                    '<td>' + value.note + '</td>' +
                    '<td><a class="delete-detail-offer" id="' + value.id + '" data-type="store"><i class="ft-trash-2 color-red"></i></a></td>' +
                    '</tr>'
                );
            });


            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/
    @endcan

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('offer_id', offer_id);

        $('#row-not-found td').attr('style', '').html(
            '<span>{{__('admin.no_data')}}</span>' +
            '<hr>'
        );

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                /*let response_data = JSON.parse((data.responseText)).category;*/
                let response_data = data.offer;

                $('#addModal').modal('hide');

                let status, tr_color_red = '';
                if (response_data.status == 1) {
                    status = "<div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>";
                } else {
                    tr_color_red = 'tr-color-red';
                    status = "<div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>";
                }

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col0 = $("<td class='font-default'>" + response_data.id + "</td>");
                let col1 = $("<td>" + response_data.branch_name + "</td>");
                let col2 = $("<td>" + response_data.offer_type_name + "</td>");
                let col3 = $("<td>" + response_data.name + "</td>");
                let col4 = $("<td>" + response_data.currency_symbol + " " + response_data.old_price + "</td>");
                let col5 = $("<td>" + response_data.currency_symbol + " " + response_data.price + "</td>");
                let col6 = $("<td>" + status + "</td>");
                let col7 = $("<td>" + response_data.actions + "</td>");

                let this_row;
                if (type === 'add') {
                    row.append(col0, col1, col2, col3, col4, col5, col6, col7).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col0, col1, col2, col3, col4, col5, col6, col7);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.name_ar !== undefined) {
                $('#form input[name=name_ar]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.name_ar);
            }
            if (obj.name_en !== undefined) {
                $('#form input[name=name_en]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.name_en);
            }
            if (obj.old_price !== undefined) {
                $('#form input[name=old_price]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.old_price);
            }
            if (obj.new_price !== undefined) {
                $('#form input[name=new_price]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.new_price);
            }
            if (obj.start_date !== undefined) {
                $('#form input[name=start_date]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.start_date);
            }
            if (obj.end_date !== undefined) {
                $('#form input[name=end_date]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.end_date);
            }
            if (obj.image !== undefined) {
                $('#form input[name=image]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.image);
            }
            if (obj.offer_type !== undefined) {
                $('#form select[name=offer_type]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.offer_type);
            }
            if (obj.branch_id !== undefined) {
                $('#form select[name=branch_id]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.branch_id);
            }
            if (obj.currency !== undefined) {
                $('#form select[name=currency]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.currency);
            }
            if (obj.product_price_id !== undefined) {
                $('#table-offer-detail tbody').empty().append(
                    '<tr id="row-not-found">' +
                    ' <td colspan="4" class="text-center"><span>{{__('admin.no_data')}}</span><hr></td>' +
                    '</tr>'
                );
                $('#row-not-found td').attr('style', 'color: red').html(
                    '<span>اضف منتج واحد على الاقل.</span>' +
                    '<hr style="background-color : red">'
                );
            }
        }
    });
    /*end code add or update*/

    @can('delete offer')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('app/offers/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            this_row.remove();
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan

    $(document).on('click', '#add-offer-detail', function () {
        $('#addOfferDetail').modal('show');
    });

    /*ajax falter categories start*/
    $(document).on('change', '#category', function () {
        let category_id = $(this).val();
        let category_url = "{{url('app/offers/falter-product')}}" + "/" + category_id;
        let element_product = $('#product');
        if (category_id) {
            $.ajax({
                type: "GET",
                url: category_url,
                success: function (data) {
                    if (data) {
                        element_product.empty();
                        element_product.append('<option value="">{{__("admin.select_option")}}</option>');
                        $.each(data, function (key, value) {
                            element_product.append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    } else {
                        element_product.empty();
                    }
                }
            });
        } else {
            element_product.empty();
        }
    });
    /*ajax falter categories end*/

    /*ajax falter categories start*/
    $(document).on('change', '#branch_id', function () {
        let category_id = $(this).val();
        let category_url = "{{url('/app/offers/falter_main_category')}}" + "/" + category_id;
        let element_product = $('#category');
        if (category_id) {
            $.ajax({
                type: "GET",
                url: category_url,
                success: function (data) {
                    if (data) {
                        element_product.empty();
                        element_product.append('<option value="">{{__("admin.select_option")}}</option>');
                        $.each(data, function (key, value) {
                            element_product.append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    } else {
                        element_product.empty();
                    }
                }
            });
        } else {
            element_product.empty();
        }
    });

    /*ajax falter categories start*/
    $(document).on('change', '.all-categories', function () {
        var data_number = $(this).attr('data-category-order');
        var branch_id = $("#branch_id").val();
        var category_id = $(this).val();
        console.log(branch_id);
        category_url = "{{url('app/offers/falter-product-discount')}}" + "/" + category_id + '/' + branch_id;
        if (category_id) {
            $.ajax({
                type: "GET",
                url: category_url,
                success: function (data) {
                    if (data) {
                        $('.product-' + data_number).empty();
                        $('.product-' + data_number).append('<option value=""> {{__("admin.select_option")}}  </option>');
                        $.each(data, function (key, value) {
                            $(".product-" + data_number).append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    } else {
                        $('.product-' + data_number).empty();
                    }
                }
            });
        } else {
            $('.product-' + data_number).empty();
        }
    });
    /*ajax falter categories end*/

    /*ajax falter products start*/
    $(document).on('change', '#product', function () {
        let product_id = $(this).val();
        let url = "{{url('app/offers/product-price')}}" + "/" + product_id;
        if (product_id) {
            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function () {
                    $("#loader-price").html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $("#product_price_div").attr('hidden', true);
                },
                success: function (data) {
                    $("#loader-price").empty();
                    $("#product_price_div").attr('hidden', false);
                    $("#product_price_tbody").empty();
                    $.each(data, function (key, value) {
                        $("#product_price_tbody").append(
                            "<tr>" +
                            "<td><a class='blue show-detail' title='{{__('admin.click_here_to_view_product_details')}}' id='" + value.product_id + "'>" + value.product_name + "</a></td>" +
                            "<td>" + value.unit + "</td>" +
                            "<td>" + value.price + "</td>" +
                            "<td>" + value.note_ar + "</td>" +
                            "<td>" +
                            "<button type='button' class='btn btn-sm btn-primary open-quantity' id='" + value.id + "'><i class='ft-plus'></i> {{__('admin.add')}}</button>" +
                            "</td>" +
                            "</tr>"
                        );
                    });
                },
                error: function () {
                    $("#loader-price").html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                }
            });
        } else {

        }
    });

    let product_price_id;
    let priceRow;
    let product_name;
    let unit_name;
    let price;

    $(document).on('click', '.open-quantity', function () {
        product_price_id = $(this).attr('id');
        priceRow = $(this).parent().parent();

        product_name = priceRow.find('td:nth-child(1)').html();
        unit_name = priceRow.find('td:nth-child(2)').html();
        price = priceRow.find('td:nth-child(3)').html();

        $('#submit_add_offer').html('<i class="ft-plus"></i> {{__('admin.add_to_offer')}}');
        $('textarea[name=note]').val('');
        $('input[name=quantity]').val("");
        $('#modalQuantityProduct').modal('show');
    });

    $('#form-add-to-offer').on('submit', async function (event) {
        event.preventDefault();
        $('#row-not-found').attr('hidden', true);

        let input_quantity = $('#input-quantity').val();
        let text_note = $('#text-note').val();
        let btn_type = $('#btn-save').attr('data_type');

        if (btn_type === 'Update') {
            let url = "{{route('offers.delete-detail.store')}}";
            let data_form = new FormData(this);
            data_form.append('product_price_id', product_price_id);
            data_form.append('offer_id', $('#btn-save').attr('data_id'));

            try {
                let data = await addOrUpdate(url, data_form, 'Add', 'submit_add_offer');
            } catch (error) {
                return error;
            }
        }

        appendTable(product_name, unit_name, product_price_id, input_quantity, text_note);
        $('#modalQuantityProduct').modal('hide');

    });

    function appendTable(product_name, unit_name, product_price_id, input_quantity, text_note) {
        $('#table-offer-detail').append(
            '<tr>' +
            '<td>' + product_name + ' | ' + unit_name + ' <input hidden type="number" value="' + product_price_id + '" name="product_price_id[]"></td>' +
            '<td>' + input_quantity + '<input hidden type="number" value="' + input_quantity + '" name="quantity[]"></td>' +
            '<td>' + text_note + '<textarea hidden name="note[]">' + text_note + '</textarea></td>' +
            '<td><a class="remove-detail-offer" data-type="store"><i class="ft-trash-2 color-red"></i></a></td>' +
            '</tr>'
        );
    }

    $(document).on('click', '.remove-detail-offer', function () {
        console.log('ok');
        let data_type = $(this).attr('data-type');
        if (data_type === 'store') {
            swal({
                title: "{{__('admin.do_you_want_to_delete')}}",
                text: "",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "{{__('admin.no')}}",
                        value: null,
                        visible: true,
                        className: "",
                    },
                    confirm: {
                        text: "{{__('admin.yes')}}",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            })
                .then((isConfirm) => {
                    if (isConfirm) {
                        $(this).parent().parent().remove();
                        swal({
                            icon: "success",
                            title: "{{__('admin.deleted_successfully')}}",
                            text: "",
                            button: '{{__('admin.ok')}}'
                        });
                    }
                });
        }
    });


    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('app/offers/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            this_row.remove();
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/

    /*start code Delete ajax*/
    $(document).on('click', '.delete-detail-offer', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('app/offers/delete-detail-offer')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            this_row.remove();
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/
</script>
