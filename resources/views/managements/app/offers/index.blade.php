@php($page_title = __('admin.deal_of_the_day'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.user_application')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.deal_of_the_day')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create offer')
                                            <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.deal_of_the_day')}}</a>
                                        @endcan
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('offers')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="branch" @if($search_type == 'branch') selected @endif>{{__('admin.branch')}}</option>
                                                                    <option value="type" @if($search_type == 'type') selected @endif>{{__('admin.type')}}</option>
                                                                    <option value="name" @if($search_type == 'name') selected @endif>{{__('admin.name')}}</option>
                                                                    <option value="old_price" @if($search_type == 'old_price') selected @endif>{{__('admin.old_price')}}</option>
                                                                    <option value="new_price" @if($search_type == 'new_price') selected @endif>{{__('admin.new_price')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.branch')}}</th>
                                                        <th>{{__('admin.type')}}</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th style="width: 80px">{{__('admin.old_price')}}</th>
                                                        <th style="width: 80px">{{__('admin.new_price')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $offer)
                                                        <tr style="{{$offer->background_color_row}}">
                                                            <td hidden>{{$offer->updated_at}}</td>
                                                            <td>{{$offer->id}}</td>
                                                            <td>{{$offer->branch->name}}</td>
                                                            <td>{{$offer->type->name}}</td>
                                                            <td>{{$offer->name}}</td>
                                                            <td>{{currency($offer->currency) .' '.$offer->old_price}}</td>
                                                            <td>{{currency($offer->currency) .' '.$offer->price}}</td>
                                                            <td>
                                                                @php($status = $offer->status)
                                                                @if($status == 1)
                                                                    <div class='fonticon-wrap'><i class='ft-unlock' style='color:#0679f0'></i></div>
                                                                @else
                                                                    <div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>
                                                                @endif
                                                            </td>
                                                            <td>{!! $offer->actions !!}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    {{--model show modal--}}
    @include('managements.app.offers.show')

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.categories')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements" id="check_show">
                                <input type="checkbox" name="status" value="1"
                                       class="switchBootstrap" id="switchBootstrap18"
                                       data-on-color="primary" data-off-color="danger"
                                       data-on-text="{{__('admin.enable')}}"
                                       data-off-text="{{__('admin.disable')}}" data-color="primary"
                                       data-label-text="{{__('admin.status')}}"
                                       checked/>
                            </div>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6 p-0">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="name_ar">{{__('admin.name')}} (ar)<span class="danger">*</span></label>
                                                    <input type="text" id="name_ar"
                                                           class="form-control"
                                                           name="name_ar"
                                                           required
                                                    >
                                                    <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="name_en">{{__('admin.name')}} (en)<span class="danger">*</span></label>
                                                    <input type="text" id="name_en"
                                                           class="form-control"
                                                           name="name_en"
                                                           required
                                                    >
                                                    <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="edit_image_ar" id="label-image">{{__('admin.image')}}</label>
                                                    <input type="file" accept=".jpg, .jpeg, .png"
                                                           id="edit_image_ar"
                                                           class="form-control"
                                                           name="image" onchange="loadAvatar(this);"
                                                    >
                                                    <button type="button" hidden onclick="delete_img()"
                                                            style="position: static; "
                                                            class="btn btn-sm btn-outline-danger"
                                                            id="delete-img"><i class="ft-trash"></i>
                                                    </button>

                                                    <img id="avatar" style="max-width: 140px; height: auto; margin:10px;">
                                                    <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 p-0">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="currency">{{__('admin.currency')}}<span class="danger">*</span></label>
                                                            <select name="currency" id="currency" class="form-control" required>
                                                                <option value="ريال يمني">YER</option>
                                                                <option value="دولار أمريكي">$</option>
                                                                <option value="ريال سعودي">SAR</option>
                                                            </select>
                                                            <span class="error-message">
                                                                <strong></strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="old-price">{{__('admin.old_price')}}<span class="danger">*</span></label>
                                                            <input type="number" id="old-price"
                                                                   class="form-control"
                                                                   name="old_price"
                                                                   required
                                                            >
                                                            <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="new_price">{{__('admin.new_price')}}<span class="danger">*</span></label>
                                                            <input type="number" id="new_price"
                                                                   class="form-control"
                                                                   name="new_price"
                                                                   required
                                                            >
                                                            <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="branch_id">{{__('admin.branch_name')}}
                                                                <span class="danger">*</span></label>
                                                            <br>
                                                            <select class="form-control"
                                                                    id="branch_id" required name="branch_id">
                                                                <option value="">{{__('admin.select_option')}}</option>
                                                                @foreach($branches as $branch)
                                                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong></strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="offer_type">{{__('admin.offer_type')}}<span class="danger">*</span></label>
                                                            <select name="offer_type" id="offer_type" class="form-control" required>
                                                                <option value="">{{__('admin.select_option')}}</option>
                                                                @foreach($offer_types as $type)
                                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="start-date">{{__('admin.start_date')}}<span class="danger">*</span></label>
                                                            <input type="date" id="start-date"
                                                                   class="form-control"
                                                                   name="start_date"
                                                                   required
                                                            >
                                                            <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="end-date">{{__('admin.end_date')}}<span class="danger">*</span></label>
                                                            <input type="date" id="end-date"
                                                                   class="form-control"
                                                                   name="end_date"
                                                                   required
                                                            >
                                                            <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button style="margin: auto 25px" id="add-offer-detail" type="button" class="btn btn-primary btn-sm">{{__('admin.add')}}  {{__('admin.a_products')}}</button>

                            {{--                            <h5 style="margin: auto 25px">{{__('admin.offer_detail')}}</h5>--}}
                            <hr>
                            <table id="table-offer-detail" width="100%" class="table table-striped table-responsive table-custom-responsive">
                                <thead style="background-color: #f6f6f6">
                                <tr>
                                    <th>{{__('admin.product')}} | {{__('admin.unit')}}</th>
                                    <th>{{__('admin.quantity')}}</th>
                                    <th>{{__('admin.note')}}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id="row-not-found">
                                    <td colspan="4" class="text-center">
                                        {{__('admin.no_data')}}
                                        <hr>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div id="inputHidden" hidden>

                            </div>
                            <div class="modal-footer">
                                <fieldset id="button-container"
                                          class="form-group position-relative has-icon-left mb-0">
                                    <button type="submit" class="btn btn-primary" id="btn-save"><i
                                            class="ft-save"></i> {{__('admin.save')}}</button>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addOfferDetail" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                            style=" @if( app()->getLocale() == 'ar')  margin-right: 472px; @else margin-left: 480px @endif"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin: 6px">&times;</span>
                    </button>
                    <div class="modal-header" style="padding-bottom: 0px;">
                        <h5 class="card-title" id="basic-layout-form" style="padding-top: 5px;">{{__('admin.add')}} {{__('admin.offer_detail')}}</h5>
                    </div>

                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="category">{{__('admin.category')}} <span class="danger">*</span></label>
                                            <select class="form-control" name="category" id="category">
                                                <option value="">{{__('admin.select_option')}}</option>
                                                @foreach($category as $categories)
                                                    <option value="{{$categories->id}}">{{$categories->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="error-message">
                                                <strong></strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="product">{{__('admin.product')}} <span class="danger">*</span></label>
                                            <select class="form-control" name="product" id="product">
                                            </select>
                                            <span class="error-message">
                                                <strong></strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <br><br>
                                <div id="loader-price" style="text-align: center"></div>
                                <div id="product_price_div" hidden>
                                    <table width="100%" class="table table-striped table-responsive table-custom-responsive">
                                        <thead style="background-color: #f6f6f6">
                                        <tr>
                                            <th>{{__('admin.product')}}</th>
                                            <th>{{__('admin.unit')}}</th>
                                            <th>{{__('admin.price')}}</th>
                                            <th>{{__('admin.note')}}</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody id="product_price_tbody">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div id="modalQuantityProduct" class="modal text-left" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content" style="padding: 10px">
                <form id="form-add-to-offer">
                    @csrf
                    <div class="form-body" style="margin: 10px">
                        <div class="form-group">
                            <label for="input-quantity">{{__('admin.quantity')}} <span class="danger">*</span></label>
                            <input id="input-quantity" type="number" class="form-control" required name="quantity">
                        </div>
                        <div class="form-group">
                            <label for="text-note">{{__('admin.note')}}</label>
                            <textarea rows="2" id="text-note" class="form-control" name="note"></textarea>
                        </div>
                    </div>
                    <hr>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary" id="submit_add_offer"><i class="ft-plus"></i> {{__('admin.add_to_offer')}}</button>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>

    @include('managements.app.products.show')

@endsection
@section('script')
    @include('managements.app.offers.show_js')
    @include('managements.app.offers.js')
    @include('managements.app.products.show_js')
@endsection
