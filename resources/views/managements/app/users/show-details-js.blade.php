<script>
    /*start code show details ajax*/
    $(document).on('click', '.show-details', function () {
        let user_id = $(this).attr('id');
        let url = "{{url('app/users/show')}}" + "/" + user_id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType: "json",
            beforeSend: function () {
                $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                $('#confirm-modal-loading-show').modal('show');
            },
            success: function (data) {
                if (data.status == 1) {
                    $('#show_status').html('<i class="la la-unlock-alt color-primary"></i> {{__('admin.active')}}');
                } else if (data.status == 2) {
                    $('#show_status').html(' <button class="btn btn-outline-danger btn-sm">{{__('admin.deleted')}}</button>');
                } else if (data.status == 3) {
                    $('#show_status').html(' <button class="btn btn-outline-dark btn-sm">{{__('admin.black_list')}}</button>');
                } else {
                    $('#show_status').html(' <button class="btn btn-outline-warning btn-sm">{{__('admin.attitude')}}</button>');
                }
                $('#Gender').text(data.gender);
                $('.form-details .name').text(data.name);
                $('.form-details .email').text(data.email);
                $('.form-details .phone').text(data.phone);
                $('#id').text(data.id);
                $('#birth_date').text(data.birth_date);
                $('#address').text(data.address);
                $('#created').text(data.created_at);
                $('#updated_at').html(data.updated_at);
                $('#opening_balance').html(data.opening_balance);
                $('#current_balance').html(data.current_balance);
                $('#confirm-modal-loading-show').modal('hide');
                /*$('#confirmModalShow').modal('show');*/
                $('#confirmModalShow').modal('show');
                if (data.image != null)
                    $('#user-image').attr('src', '{{asset('/storage')}}' + '/' + data.image);
                else
                    $('#user-image').attr('src', '{{asset('/storage/admins/150/avatar.jpg')}}');
            },
            error: function (data) {
                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                $('#confirm-modal-loading-show').modal('show');
            }
        })
    });
    /*end code show details ajax*/
</script>
