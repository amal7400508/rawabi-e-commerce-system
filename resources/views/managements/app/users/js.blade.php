<script>
    let user_id;
    let deposit_row;

    /*start code show details deposit ajax*/
    $(document).on('click', '.deposit', async function () {
        $('#form input:not(:first), #form textarea').val('').removeClass('is-invalid');
        $('#form span strong').empty();
        deposit_row = $(this).parent().parent();
        user_id = $(this).attr('id');
        let url = "{{url('app/users/show')}}" + "/" + user_id;

        try {
            let data = await responseEditOrShowData(url);
            $('.user-display .name').text(data.name);
            $('.user-display .email').text(data.email);
            $('.user-display .phone').text(data.phone);
            $('.opening_balance').text(data.opening_balance);
            $('.current_balance').text(data.current_balance);
            if (data.image != null)
                $('.user-image').attr('src', '{{asset('/storage')}}' + '/' + data.image);
            else $('.user-image').attr('src', '{{asset('/storage/admins/150/avatar.jpg')}}');

            $('#confirmModalDeposit').modal('show');
        } catch (error) {

            return error;
        }
    });
    /*end code show details deposit ajax*/

    /*start code add deposit*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let url = "{{url('app/users/deposit')}}" + "/" + user_id;

        let data_form = new FormData(this);
        try {
            let data = await addOrUpdate(url, data_form, 'add', 'btn-save');
            if (data['status'] == 200) {
                $('#confirmModalDeposit').modal('hide');
                deposit_row.addClass('tr-color-success');
                setTimeout(function () {
                    deposit_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.amount !== undefined) {
                $('#form input[name=amount]').addClass('is-invalid')
                    .parent().parent().find('span strong').text(obj.amount);
            }
            if (obj.note_deposit !== undefined) {
                $('#form input[name=note_deposit]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.note_deposit);
            }
        }
    });
    /*end code add deposit*/



    /*start code show details edit ajax*/
    $(document).on('click', '.edit', async function () {
        user_id = $(this).attr('id');
        deposit_row = $(this).parent().parent();
        let url = "{{url('app/users/show')}}" + "/" + user_id;

        try {
            let data = await responseEditOrShowData(url);
            $('.user-display .name').text(data.name);
            $('.user-display .email').text(data.email);
            $('.user-display .phone').text(data.phone);
            $('.opening_balance').text(data.opening_balance);
            $('.current_balance').text(data.current_balance);
            if (data.image != null)
                $('.user-image').attr('src', '{{asset('/storage')}}' + '/' + data.image);
            else $('.user-image').attr('src', '{{asset('/storage/admins/150/avatar.jpg')}}');

            document.querySelector('input[name="status"][value="' + data.status + '"]').checked = true;

            $('#confirmModalEdit').modal('show');
        } catch (error) {
            return error;
        }
    });

    /*end code show details edit ajax*/

    /*start code edit user status*/
    $('#form-edit').on('submit', async function (event) {
        event.preventDefault();
        let url = "{{url('app/users/update')}}" + "/" + user_id;

        let data_form = new FormData(this);
        try {
            let data = await addOrUpdate(url, data_form, 'Update', 'btn-edit');
            if (data['status'] == 200) {
                $('#confirmModalEdit').modal('hide');
                deposit_row.attr('style', data.data.background_color_row);
                let status = data.data.status;
                let span = '';
                if (status == 1) {
                    span = '<button class="btn btn-outline-primary btn-sm">{{__('admin.active')}}</button>';
                } else if (status == 2) {
                    span = '<button class="btn btn-outline-danger btn-sm">{{__('admin.deleted')}}</button>';
                } else if (status == 3) {
                    span = '<button class="btn btn-outline-dark btn-sm">{{__('admin.black_list')}}</button>';
                } else {
                    span = '<button class="btn btn-outline-warning btn-sm">{{__('admin.attitude')}}</button>';
                }
                deposit_row.find('td:nth-last-child(3)').html(span);

            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.status !== undefined) {
                $('#div-status').find('span strong').text(obj.status);
            }
        }
    });
    /*end code edit user status*/

</script>
