@php($page_title = __('admin.report'))
@php($route = route('users.export'))
@extends('include.report')
@section('content')
    <div class="row">
        <div class="col-12">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-center" style="padding-bottom: 0px;">
                                <h4 class="card-title">
                                    @if(request()->report == 1)
                                        {{__('admin.report')}} {{__('admin.most_requested_users')}}
                                    @else
                                        {{__('admin.user_report')}}
                                    @endif
                                </h4>
                                <hr>
                                <h4 class="card-title">{{__('admin.from')}} {{__('admin.date_1')}}
                                    @if(request()->report == 1)
                                        <span class="font-default color-blue">{{request()->from_2 ?? "_________"}}</span>
                                        {{__('admin.to')}} {{__('admin.date_1')}}
                                        <span class="font-default color-blue">{{request()->to_2 ?? \Carbon\Carbon::now()->toDateString()}}</span>
                                    @else
                                        <span class="font-default color-blue">{{request()->from ?? "_________"}}</span>
                                        {{__('admin.to')}} {{__('admin.date_1')}}
                                        <span class="font-default color-blue">{{request()->to ?? \Carbon\Carbon::now()->toDateString()}}</span>
                                    @endif
                                </h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table width="100%" class="table  zero-configuration">
                                            <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>{{__('admin.name')}}</th>
                                                <th>{{__('admin.phone')}}</th>
                                                <th>{{__('admin.status')}}</th>
                                                @if(request()->report == 1)
                                                    <th>{{__('admin.requests_number')}}</th>
                                                @endif
                                                <th>{{__('admin.Date')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($data ) == 0)
                                                <tr>
                                                    <td colspan="9" class="text-center">
                                                        {{__('admin.no_data')}}
                                                        <hr>
                                                    </td>
                                                </tr>
                                            @endif
                                            @php($sum_debit = 0)
                                            @php($sum_creditor = 0)
                                            @foreach($data as $user)
                                                <tr style="{{$user->background_color_row}}">
                                                    <td hidden>{{$user->updated_at}}</td>
                                                    <td hidden>{{$user->updated_at}}</td>
                                                    <td>{{$user->id}}</td>
                                                    <td>{{$user->name}}</td>
                                                    <td>{{$user->phone}}</td>
                                                    <td>
                                                        @php($status = $user->status)

                                                        @if($status == 1)
                                                            <button
                                                                class="btn btn-outline-primary btn-sm">{{__('admin.active')}}</button>
                                                        @elseif($status == 2)
                                                            <button
                                                                class="btn btn-outline-danger btn-sm">{{__('admin.deleted')}}</button>
                                                        @elseif($status == 3)
                                                            <button
                                                                class="btn btn-outline-dark btn-sm">{{__('admin.black_list')}}</button>
                                                        @else
                                                            <button
                                                                class="btn btn-outline-warning btn-sm">{{__('admin.attitude')}}</button>
                                                        @endif
                                                    </td>
                                                    @if(request()->report == 1)
                                                        <td>{{$user->requests_number}}</td>
                                                    @endif
                                                    <td>{{$user->created_at}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
