{{-- start model show  Message--}}
<div id="confirmModalShow" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="card-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                </button>
                <div class="form-details card-body row">
                    <div class="user-display col-4" style="    padding-right: 30px;    padding-top: 10px;">
                        <img src="" class="rounded-circle img-thumbnail img-user-show-100" id="user-image">
                    </div>
                    <div class="user-display col-8 p-0">
                        <h2 class="name product-title"
                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif padding-top: 25px;"></h2>
                        <h6 class="email product-title font-default"
                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                        <h6 class="phone product-title font-default"
                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                        {{--<div class=" btn-round position-absolute round"
                             style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif font-size: 20px"
                             id="show_status"></div>--}}
                    </div>
                </div>
                <table class="table mb-0">
                    <tr>
                        <th>{{__('admin.id')}}</th>
                        <td id="id"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.address')}}</th>
                        <td id="address"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.gender')}}</th>
                        <td id="Gender"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.birthday')}}</th>
                        <td id="birth_date"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.status')}}</th>
                        <td id="show_status"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.opening_balance')}}</th>
                        <td id="opening_balance"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.current_balance')}}</th>
                        <td id="current_balance"></td>
                    </tr>
                </table>
                <div class="row p-1">
                    <div class="col-sm-12">
                        <span class="details price" style="color: #0679f0"></span>
                    </div>
                    <div class="col-sm-9">
                        <small class="price category-color">{{__('admin.created_at')}}:</small>
                        <small class="price category-color" id="created"></small>
                        <br>
                        <small class="price category-color">{{__('admin.updated_at')}}:</small>
                        <small class="price category-color" id="updated_at"></small>
                    </div>
                    <div class="col-sm-3 text-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model show  Message--}}
