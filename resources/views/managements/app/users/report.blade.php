@php($page_title = __('admin.user_report'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.users')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/')}}"><i class="la la-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('users')}}">{{__('admin.users')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.user_report')}}</li>
                        </ol>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.create')}} {{__('admin.user_report')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('users.report')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <ul class="nav nav-tabs nav-underline nav-justified">
                                                <li class="nav-item">
                                                    <a class="nav-link active" id="activeIcon12-tab1" data-toggle="tab"
                                                       href="#activeIcon12" aria-controls="activeIcon12"
                                                       aria-expanded="true">{{__('admin.options')}}</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="linkIcon12-tab1" data-toggle="tab"
                                                       href="#linkIcon12"
                                                       aria-controls="linkIcon12"
                                                       aria-expanded="false">{{__('admin.extra_options')}}</a>
                                                </li>
                                            </ul>

                                            <div class="tab-content px-1 pt-1">
                                                <div role="tabpanel" class="tab-pane active" id="activeIcon12"
                                                     aria-labelledby="activeIcon12-tab1" aria-expanded="true">
                                                    <br>
                                                    <form method="get">
                                                        <div class="form-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>{{__('admin.from') .' '. __('admin.created_at')}}</label>
                                                                                <input type="date" class="form-control" value="{{request()->from}}" name="from">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>{{__('admin.to') .' '. __('admin.created_at')}}</label>
                                                                                <input type="date" class="form-control" value="{{request()->to}}" name="to">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>{{__('admin.name')}}</label>
                                                                        <input type="text" class="form-control" value="{{request()->name}}" name="name">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.email')}}</label>
                                                                        <input type="email" class="form-control" value="{{request()->email}}" name="email">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.phone')}}</label>
                                                                        <input type="number" class="form-control" value="{{request()->phone}}" name="phone">
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>{{__('admin.gender')}}</label>
                                                                                <select class="form-control" name="gender">
                                                                                    <option
                                                                                        value="">{{__('admin.select_option')}}</option>
                                                                                    <option value="man">{{__('admin.man')}}</option>
                                                                                    <option
                                                                                        value="woman">{{__('admin.woman')}}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>{{__('admin.status')}}</label>
                                                                                <select class="form-control" name="status">
                                                                                    <option
                                                                                        value="">{{__('admin.select_option')}}</option>
                                                                                    <option
                                                                                        value="1">{{__('admin.active')}}</option>
                                                                                    <option
                                                                                        value="2">{{__('admin.deleted')}}</option>
                                                                                    <option
                                                                                        value="3">{{__('admin.black_list')}}</option>
                                                                                    <option
                                                                                        value="0">{{__('admin.attitude')}}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pull-right">
                                                            <button type="submit" formaction="{{route('users.report')}}" class=" btn btn-primary" id="button_preview"
                                                                    style="margin-bottom: 10px; width: 120px">
                                                                <i class="ft-eye"></i> {{__('admin.preview')}}
                                                            </button>
                                                            <button type="submit" formtarget="_blank" formaction="{{route('users.report_export')}}" class=" btn btn-primary" id="button_export"
                                                                    style="margin-bottom: 10px; width: 120px">
                                                                <i class="ft-printer"></i> {{__('admin.report')}}
                                                            </button>
                                                          </div>
                                                    </form>
                                                </div>
                                                <div class="tab-pane" id="linkIcon12" role="tabpanel"
                                                     aria-labelledby="linkIcon12-tab1"
                                                     aria-expanded="false">
                                                    <p>
                                                        {{__('admin.a_note'). ' : '.__('admin.these_entries_are_separate_from_the_others')}}
                                                        .
                                                    </p>
                                                    <br>
                                                    <form id="form-most-request" method="get">
                                                        <input hidden type="number" value="{{request()->report}}" name="report">
                                                        <div class="form-body">
                                                            <div class="col_md_12">
                                                                <div class="form-group">
                                                                    <input type="radio" class="form-control" checked
                                                                           style="float: @if(app()->getLocale() == 'ar') right @else left @endif;width: 21px;margin: -10px 10px;">
                                                                    <label>{{__('admin.most_requested_users')}}.</label>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.from')}}</label>
                                                                        <input type="date" class="form-control" value="{{request()->from_2}}" name="from_2">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.to')}}</label>
                                                                        <input type="date" class="form-control" value="{{request()->to_2}}" name="to_2">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="row">
                                                                        <div class="col-md-5">
                                                                            <div class="form-group">
                                                                                <label>{{__('admin.enter_the_number_of_users_to_display')}}</label>
                                                                                <input type="number" class="form-control" value="{{request()->number_user}}" placeholder="3" name="number_user">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label for="comparison_operations">{{__('admin.comparison_operations')}}</label>
                                                                                <select class="form-control" id="comparison_operations" name="comparison_operations">
                                                                                    <option value="=">{{__('admin.equal_to')}}</option>
                                                                                    <option value=">">{{__('admin.greater_than')}}</option>
                                                                                    <option value="<">{{__('admin.less_than')}}</option>
                                                                                    <option value=">=">{{__('admin.greater_than_or_equal_to')}}</option>
                                                                                    <option value="<=">{{__('admin.less_than_or_equal_to')}}</option>
                                                                                    <option value="!=">{{__('admin.not_equal_to')}}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label>{{__('admin.requests_number')}}</label>
                                                                                <input type="number" class="form-control" value="{{request()->number}}" name="number">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions pull-right">
                                                            <button type="submit" formaction="{{route('users.report')}}" class=" btn btn-primary" id="button_preview"
                                                                    style="margin-bottom: 10px; width: 120px">
                                                                <i class="ft-eye"></i> {{__('admin.preview')}}
                                                            </button>
                                                            <button type="submit" formtarget="_blank" formaction="{{route('users.report_export')}}" class=" btn btn-primary" id="button_export"
                                                                    style="margin-bottom: 10px; width: 120px">
                                                                <i class="ft-printer"></i> {{__('admin.report')}}
                                                            </button>
                                                            {{--
                                                            <button type="button"
                                                                    class="btn btn-primary"
                                                                    id="most-request"
                                                                    style="margin-bottom: 10px; width: 120px"
                                                                    data-report-no="two">
                                                                <i class="ft-eye"></i> {{__('admin.preview')}}
                                                            </button>
                                                            <button type="submit" class=" btn btn-primary"
                                                                    style="margin-bottom: 10px; width: 120px">
                                                                <i class="ft-printer"></i> {{__('admin.export')}}
                                                            </button>--}}
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="content-body" id="div-table-report">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.post_index')}} {{__('admin.users')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('users.report')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                </div>
                                                <table width="100%"
                                                       class="table  zero-configuration"
                                                       id="data_table">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('admin.id')}}</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.image')}}</th>
                                                        <th>{{__('admin.phone')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data ) == 0)
                                                        <tr>
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $user)
                                                        <tr style="{{$user->background_color_row}}">
                                                            <td hidden>{{$user->updated_at}}</td>
                                                            <td>{{$user->id}}</td>
                                                            <td>{{$user->name}}</td>
                                                            <td>{{$user->phone}}</td>
                                                            <td>
                                                                @php($status = $user->status)

                                                                @if($status == 1)
                                                                    <button
                                                                        class="btn btn-outline-primary btn-sm">{{__('admin.active')}}</button>
                                                                @elseif($status == 2)
                                                                    <button
                                                                        class="btn btn-outline-danger btn-sm">{{__('admin.deleted')}}</button>
                                                                @elseif($status == 3)
                                                                    <button
                                                                        class="btn btn-outline-dark btn-sm">{{__('admin.black_list')}}</button>
                                                                @else
                                                                    <button
                                                                        class="btn btn-outline-warning btn-sm">{{__('admin.attitude')}}</button>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($user->image != null)
                                                                    <img style='max-width:40px' class='rounded-circle img-user-show-40'
                                                                         src='{{ url('/').'/storage/' . $user->image}}'
                                                                         onerror="this.src='{{asset('storage/admins/64/avatar.jpg')}}'"
                                                                    />
                                                                @else
                                                                    <img style='max-width:40px' class='rounded-circle img-user-show-40'
                                                                         src='{{ url('/').'/storage/admins/64/avatar.jpg' }}'/>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <button type="button" id="{{$user->id}}" class="show-details btn btn-primary btn-sm" title="{{__('admin.show')}}">
                                                                    <i class="ft ft-eye"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            {{ $data->appends($pagination_links)->links() }}

                                            <span>{{__('admin.show').' '. $data->count() .' '.__('admin.out_of').' '. $data_count . ' '. __('admin.record')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <div class="content-body" id="div-most-request" hidden>
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{__('admin.most_requested_users')}}</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table width="100%"
                                           class="table  zero-configuration"
                                           id="data_table">
                                        <thead>
                                        <tr>
                                            <th>{{__('admin.id')}}</th>
                                            <th>{{__('admin.name')}}</th>
                                            <th>{{__('admin.phone')}}</th>
                                            <th>{{__('admin.status')}}</th>
                                            <th>{{__('admin.image')}}</th>
                                            <th>{{__('admin.requests_number')}}</th>
                                            <th>{{__('admin.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($most_data ) == 0)
                                            <tr>
                                                <td colspan="9" class="text-center">
                                                    {{__('admin.no_data')}}
                                                    <hr>
                                                </td>
                                            </tr>
                                        @endif
                                        @foreach($most_data as $most_user)
                                            <tr style="{{$most_user->background_color_row}}">
                                                <td hidden>{{$most_user->updated_at}}</td>
                                                <td>{{$most_user->id}}</td>
                                                <td>{{$most_user->name}}</td>
                                                <td>{{$most_user->phone}}</td>
                                                {{--                                                            <td>{{$most_user->status}}</td>--}}
                                                <td>
                                                    @php($sta = $most_user->status)

                                                    @if($sta == 1)
                                                        <button
                                                            class="btn btn-outline-primary btn-sm">{{__('admin.active')}}</button>
                                                    @elseif($sta == 2)
                                                        <button
                                                            class="btn btn-outline-danger btn-sm">{{__('admin.deleted')}}</button>
                                                    @elseif($sta == 3)
                                                        <button
                                                            class="btn btn-outline-dark btn-sm">{{__('admin.black_list')}}</button>
                                                    @else
                                                        <button
                                                            class="btn btn-outline-warning btn-sm">{{__('admin.attitude')}}</button>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($most_user->image != null)
                                                        <img style='max-width:40px' class='rounded-circle img-user-show-40'
                                                             src='{{ url('/').'/storage/' . $most_user->image}}'
                                                             onerror="this.src='{{asset('storage/admins/64/avatar.jpg')}}'"
                                                        />
                                                    @else
                                                        <img style='max-width:40px' class='rounded-circle img-user-show-40'
                                                             src='{{ url('/').'/storage/admins/64/avatar.jpg' }}'/>
                                                    @endif
                                                </td>
                                                <td>{{$most_user->requests_number}}</td>
                                                <td>
                                                    <button type="button" id="{{$most_user->id}}" class="show-details btn btn-primary btn-sm" title="{{__('admin.show')}}">
                                                        <i class="ft ft-eye"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{--                                {{ $most_data->appends($pagination_links)->links() }}--}}

                                <span>{{__('admin.show').' '. $most_data->count() .' '.__('admin.out_of').' '. $most_data_count . ' '. __('admin.record')}}</span>
                            </div>
                        </div>

                        {{--                        <div class="card-body">--}}
                        {{--                            <div class="table-responsive">--}}
                        {{--                                <table width="100%"--}}
                        {{--                                       class="table  zero-configuration"--}}
                        {{--                                       id="table-most-request">--}}
                        {{--                                    <thead>--}}
                        {{--                                    <tr>--}}
                        {{--                                        <th>{{__('admin.id')}}</th>--}}
                        {{--                                        <th>{{__('admin.name')}}</th>--}}
                        {{--                                        <th>{{__('admin.status')}}</th>--}}
                        {{--                                        <th>{{__('admin.phone')}}</th>--}}
                        {{--                                        <th>{{__('admin.requests_number')}}</th>--}}
                        {{--                                        <th>{{__('admin.image')}}</th>--}}
                        {{--                                        <th>{{__('admin.action')}}</th>--}}
                        {{--                                    </tr>--}}
                        {{--                                    </thead>--}}
                        {{--                                </table>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('managements.app.users.show-details')

@endsection
@section('script')
    @include('managements.app.users.show-details-js')
    <script>

        $(".show_a.account_statement").parent().attr('hidden', true);
        $("select[name=gender]").val("{{request()->gender}}");
        $("select[name=status]").val("{{request()->status}}");
        document.getElementById('comparison_operations').value = "{!! request()->comparison_operations !!}";

        if ("{{request()->report}}" === "1") {
            $('#div-table-report').attr('hidden', true);
            $('#div-most-request').attr('hidden', false);

            $('#linkIcon12-tab1').addClass('active');
            $('#activeIcon12-tab1').removeClass('active');

            $('#linkIcon12').addClass('active');
            $('#activeIcon12').removeClass('active');
        }
        $(document).on('click', '#activeIcon12-tab1', function () {
            $('input[name=report]').val(0);
            $('#div-table-report').attr('hidden', false);
            $('#div-most-request').attr('hidden', true)
        });

        $(document).on('click', '#linkIcon12-tab1', function () {
            $('input[name=report]').val(1);
            $('#div-table-report').attr('hidden', true);
            $('#div-most-request').attr('hidden', false);
        });


        /*/!*start code show details ajax*!/
        var users_detals_id;
        $(document).on('click', '.showdetail', function () {
            users_detals_id = $(this).attr('id');
            detail_url = "{{url('users/show')}}" + "/" + users_detals_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    if (data.status == 1) {
                        $('#show_status').html(' <button class="btn btn-round btn-primary btn-sm">{{__('admin.active')}}</button>');
                    } else if (data.status == 2) {
                        $('#show_status').html(' <button class="btn btn-round btn-danger btn-sm">{{__('admin.deleted')}}</button>');
                    } else if (data.status == 3) {
                        $('#show_status').html(' <button class="btn btn-round btn-dark btn-sm">{{__('admin.black_list')}}</button>');
                    } else {
                        $('#show_status').html(' <button class="btn btn-round btn-warning btn-sm">{{__('admin.attitude')}}</button>');
                    }
                    $('#Gender').text(data.gender);
                    $('.form-details .name').text(data.name);
                    $('.form-details .email').text(data.email);
                    $('.form-details .phone').text(data.phone);
                    $('#id').text(data.id);
                    $('#birth_date').text(data.birth_date);
                    $('#address').text(data.address);
                    $('#monthly_share').text(data.monthly_share);
                    $('#created').text(data.created_at);
                    $('#updated_at').html(data.updated_at);
                    $('.btn_dele').html(data.btn);
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');
                    if (data.image != null)
                        $('#user-image').attr('src', '{{asset('/storage')}}' + '/' + data.image);
                    else
                        $('#user-image').attr('src', '{{asset('/storage/admins/150/avatar.jpg')}}');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /!*end code show details ajax*!/*/

    </script>
@endsection
