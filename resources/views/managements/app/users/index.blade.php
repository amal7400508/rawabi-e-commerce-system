@php($page_title = __('admin.users'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.user_application')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/')}}"><i class="la la-home"></i></a></li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.users')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('users')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                   @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number"
                                                                            @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="name" @if($search_type == 'name') selected @endif>{{__('admin.name')}}</option>
                                                                    <option value="phone" @if($search_type == 'phone') selected @endif>{{__('admin.phone')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th>{{__('admin.phone')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.image')}}</th>
                                                        <th width="25%">{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data ) == 0)
                                                        <tr>
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $user)
                                                        <tr style="{{$user->background_color_row}}">
                                                            <td hidden>{{$user->updated_at}}</td>
                                                            <td>{{$user->id}}</td>
                                                            <td>{{$user->full_name}}</td>
                                                            <td>{{$user->phone}}</td>
                                                            {{--                                                            <td>{{$user->status}}</td>--}}
                                                            <td>
                                                                @php($status = $user->status)

                                                                @if($status == 1)
                                                                    <button
                                                                        class="btn btn-outline-primary btn-sm">{{__('admin.active')}}</button>
                                                                @elseif($status == 2)
                                                                    <button
                                                                        class="btn btn-outline-danger btn-sm">{{__('admin.deleted')}}</button>
                                                                @elseif($status == 3)
                                                                    <button
                                                                        class="btn btn-outline-dark btn-sm">{{__('admin.black_list')}}</button>
                                                                @else
                                                                    <button
                                                                        class="btn btn-outline-warning btn-sm">{{__('admin.attitude')}}</button>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($user->image != null)
                                                                    <img style='max-width:40px' class='rounded-circle img-user-show-40'
                                                                         src='{{ url('/').'/storage/' . $user->image}}'/>
                                                                @else
                                                                    <img style='max-width:40px' class='rounded-circle img-user-show-40'
                                                                         src='{{ url('/').'/storage/admins/64/avatar.jpg' }}'/>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {!! $user->actions !!}
                                                            </td>
                                                        </tr>

                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    @include('managements.app.users.show-details')

    {{-- start model edit  Message--}}
    <div class="modal fade" id="confirmModalEdit" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                    </button>
                    <form class="form" enctype="multipart/form-data" id="form-edit">
                        @csrf
                        <div class="card-content">
                            <div class="card-body row">
                                <div class="user-display col-4" style="    padding-right: 30px;    padding-top: 10px;">
                                    <img src="" class="user-image rounded-circle img-thumbnail img-user-show-100">
                                </div>
                                <div class="user-display col-8 p-0">
                                    <h2 class="name product-title" style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif padding-top: 25px;"></h2>
                                    <h6 class="email product-title font-default" style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                                    <h6 class="phone product-title font-default" style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                                </div>
                            </div>
                        </div>

                        <div style="border-top: 1px solid #626E82;"></div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="form-group col-12">
                                        <div class="row">
                                            <div class="col-md-2 mx-auto"></div>
                                            <label class="col-md-2 label-control">{{__('admin.status')}}:</label>
                                            <div class="col-md-6" id="div-status">
                                                <fieldset>
                                                    <input type="radio" name="status" id="active" value="1">
                                                    <label for="active" style="color: #0679f0 !important; cursor: pointer;">{{__('admin.active')}}</label>
                                                </fieldset>
                                                <fieldset>
                                                    <input type="radio" name="status" id="deleted" value="2">
                                                    <label for="deleted" style="color: #F44336 !important; cursor: pointer; ">{{__('admin.deleted')}}</label>
                                                </fieldset>
                                                <fieldset>
                                                    <input type="radio" name="status" id="black_list" value="3">
                                                    <label for="black_list" style="cursor: pointer;">{{__('admin.black_list')}}</label>
                                                </fieldset>
                                                <fieldset>
                                                    <input type="radio" name="status" id="attitude" value="0">
                                                    <label for="attitude" style="color: #FFC107 !important; cursor: pointer;">{{__('admin.attitude')}}</label>
                                                </fieldset>
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                            <div class="col-md-2 mx-auto"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <fieldset id="button-container" class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-edit">
                                    <i class="ft-edit"></i> {{__('admin.edit')}}
                                </button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
    {{-- end model edit  Message--}}

    {{-- start model deposit  Message--}}
    <div class="modal fade" id="confirmModalDeposit" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                    </button>
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="card-content">
                            <div class="card-body row">
                                <div class="user-display col-4" style="    padding-right: 30px;    padding-top: 10px;">
                                    <img src="" class="user-image rounded-circle img-thumbnail img-user-show-100">
                                </div>
                                <div class="user-display col-8 p-0">
                                    <h2 class="name product-title" style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif padding-top: 25px;"></h2>
                                    <h6 class="email product-title font-default" style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                                    <h6 class="phone product-title font-default" style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                                </div>
                            </div>
                        </div>

                        <div style="border-top: 1px solid #626E82;"></div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div>
                                                    <span>{{__('admin.current_balance')}} : </span>
                                                    <span class="current_balance font-default" style="margin: 0 10px"></span>
                                                    <strong>YR</strong>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="amount">{{__('admin.amount')}} : <span class="danger">*</span></label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text font-default">RY</span></div>
                                                    <input type="number" id="amount"
                                                           class="form-control"
                                                           name="amount"
                                                           placeholder="{{__('admin.enter_the_amount_to_be_deposited')}}"
                                                           required
                                                    >
                                                    <div class="input-group-append"><span class="input-group-text font-default">.00</span></div>
                                                </div>
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label for="note_deposit">{{__('admin.note')}} : </label>
                                                <textarea id="note_deposit"
                                                       class="form-control"
                                                       name="note_deposit" rows="5"
                                                       placeholder="{{__('admin.note')}}"
                                                ></textarea>
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <fieldset id="button-container" class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save">
                                    <i class="ft-save"></i> {{__('admin.save')}}
                                </button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
    {{-- end model deposit  Message--}}



@endsection
@section('script')
    @include('managements.app.users.js')
    @include('managements.app.users.show-details-js')
@endsection
