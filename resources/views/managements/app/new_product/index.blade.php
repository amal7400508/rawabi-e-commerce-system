@php($page_title = __('admin.newproducts'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.user_application')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.products')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('products')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="name" @if($search_type == 'name') selected @endif>{{__('admin.name')}}</option>
                                                                    <option value="category" @if($search_type == 'category') selected @endif>{{__('admin.category')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table table-striped table-bordered zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">{{__('admin.item_code')}}</th>
                                                        <th>{{__('admin.branch_name')}}</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th>{{__('admin.categories')}}</th>
                                                        <th style="width: 10px">{{__('admin.status')}}</th>
                                                        <th>{{__('admin.image')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $product)
                                                        <tr style="{{$product->background_color_row}}" class="@if($product->is_approved == 3) text-line-td @endif">
                                                            <td hidden>{{$product->updated_at}}</td>
                                                            <td class="font-default">{{$product->number}}</td>
                                                            <td class="font-default">{{$product->branch->name}}</td>
                                                            <td>
                                                                {!! $product->is_new != 1 ? $product->name : $product->name . "<span class='badge badge btn-primary float-right'>". __('admin.new') ."</span>" !!}
                                                            </td>
                                                            <td>{{$product->category->name}}</td>
                                                            <td>
                                                                @if ($product->status == 1)
                                                                    <div class='fonticon-wrap'><i class='ft-unlock' style='color:#002581'></i></div>
                                                                @else
                                                                    <div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <img style="max-height:64px; max-width:64px" src="{{url('/').'/storage/thumbnail/64/'.$product->image}}" onerror=this.src="{{ url('/storage/thumbnail/150/logo.png')}}">
                                                            </td>
                                                            <td>
                                                                {!! $product->actions !!}
{{--                                                                <a><small class="show-detail color-info" id="{{$product->id}}">{{__('admin.detail')}}...</small></a>--}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    @include('managements.app.new_product.show')
@endsection
@section('script')
    @include('managements.app.new_product.show_js')
    @include('managements.app.new_product.js')
@endsection
