<script>

    $(document).on('click', '.accept', async function () {
        let product_id = $(this).attr('id');
        let product_row = $(this).parent().parent().parent();
        try {
            let data = await
                statusChange(
                    product_id,
                    "{{route('products.accept')}}",
                    "{{__('admin.do_you_want_to_accept')}}!",
                    "",//{{__('admin.are_you_sure_you_want_to_accept_the_product')}}?",
                    "warning",
                );
            product_row.attr('style', data.data.background_color_row);
            product_row.find('td:nth-last-child(1)').html(data.data.actions);
            product_row.find('td:nth-last-child(2)').text(data.data.current_balance);

        } catch (e) {
            return e;
        }
    });

    $(document).on('click', '.reject', async function () {
        let product_id = $(this).attr('id');
        let product_row = $(this).parent().parent().parent();
        try {
            let data = await
                statusChange(
                    product_id,
                    "{{route('products.reject')}}",
                    "{{__('admin.do_you_want_to_reject')}}!",
                    "",//{{__('admin.are_you_sure_you_want_to_reject_this_product')}}?",
                    "warning",
                );
            product_row.attr('style', data.data.background_color_row);
            product_row.find('td:nth-last-child(1)').html(data.data.actions);
            product_row.addClass('text-line-td');
        } catch (e) {
            return e;
        }
    });


</script>
