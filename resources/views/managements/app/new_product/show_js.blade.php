<script>
    $(document).on('click', '.show-detail', async function () {
        let product_id = $(this).attr('id');
        let url = "{{url('app/products/show')}}" + "/" + product_id;
        try {
            let data = await responseEditOrShowData(url);
            $("#offer_details_tbody").empty();
            for (i = 0; i < data.product_price.length; i++) {
                variable_coloer = data.product_price[i].order_availability == 1 ? '#fff' : '#feeff0';
                data_unit = data.product_price[i].id_unit != 1 ? data.product_price[i].unit_name : "<span style='color: #c9c9c9'>{{__('provider.no_data')}}</span>";
                data_taste = data.product_price[i].taste_id != 1 ? data.product_price[i].taste : "<span style='color: #c9c9c9'>{{__('provider.no_data')}}</span>";

                if (data.product_price[i].image == null) {
                    data.product_price[i].image = 'thumbnail/150/logo.png';
                }
                {{--$("#offer_details_table").append("<tr style='background-color:" + variable_coloer + "'><td>" + data_unit + "</td><td>" + data_taste + "</td>" +--}}
                {{--    "<td>" + data.product_price[i].price + "</td><td><img  style='max-height:50px; max-width:50px' src='{{ url('/').'/storage/product_price/64/' }}" + data.product_price[i].image + "' /></td></tr>");--}}
                $("#offer_details_table").append(
                    '<tr style="background-color:' + variable_coloer + '">' +
                    '<td>' + data_unit + '</td>' +
                    '<td>' + data_taste + '</td>'+
                    '<td>' + data.product_price[i].price + '</td>' +
                    '<td><img  style="max-height:50px; max-width:50px" src="{{ url('/').'/storage/product_price/64/' }}' + data.product_price[i].image + '" onerror=this.src="{{asset('storage/thumbnail/150/logo.png')}}" /></td>' +
                    '</tr>'
                );
            }
            if (data.status == 1) {
                $('#status').html('<span class="btn btn-primary position-absolute round" style="margin:10px;" ><i class="ft-unlock"></i></span>');
            } else {
                $('#status').html('<span class="btn btn-danger position-absolute round" style="margin:10px;" ><i class="ft-lock"></i></span>');
            }
            $('#show-image').attr('src', '{{asset('/storage/thumbnail/640/')}}' + '/' + data.image);
            $('#name').text(data.name);
            $('#id').text(data.id);
            $(' #details').text(data.details);
            $('#taste').text(data.taste);
            $('#image').text(data.image);
            $(' #is_new').empty();
            if (data.is_new == '1') {
                $(' #is_new').html("<span class='btn btn-sm btn-primary float-right'>{{__('provider.new')}}<span>");
            }
            $('#state_id').empty();
            if ( data.states != null){
                for (let i = 0; i < data.states.length; i++){
                    $('#state_id').append(data.states[i] + ' . ');
                }
            }
            $(' #category_translations_name').text(data.category_translations_name);
            $('#created').text(data.created_at);
            $('#updated_at').html(data.updated_at);
            $(' #btn_dele').html(data.btn);
            $('#confirm-modal-loading-show').modal('hide');
            $('#confirmModalShow').modal('show');

        } catch (e) {
            return e;
        }
    });
</script>
