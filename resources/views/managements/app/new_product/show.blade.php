{{-- start model show  Message--}}
<div id="confirmModalShow" class="modal fade text-left" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="">
                <div class="card-content">
                    <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                            style=" @if( app()->getLocale() == 'ar')  margin-right: 472px; @else margin-left: 480px @endif"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin: 6px">&times;</span>
                    </button>
                    <div class="form-details card-body p-0">
                        <span id="status"></span>
                        <div class="align-items-center" style="height: 300px;overflow: hidden;display: flex; text-align: center;">
                            <img src="" class="img-fluid rounded" alt="Card image cap" id="show-image"
                                 onerror="this.src='{{asset('storage/thumbnail/640/logo.png')}}'" >
                        </div>
                        <div class="row p-1">
                            <div class="col-sm-12">
                                <h2>
                                    <span id="name" style="color: #002581"></span>
                                    <span id="is_new"></span>
                                </h2>
                                <hr>
                                <h4 style=" color: #0C1D2F"><span id="category_translations_name"></span>
                                    <span id="type"
                                          style=" @if( app()->getLocale() == 'ar')  margin-right: 20px; @else margin-left: 20px @endif"></span>
                                </h4>
                                <p id="state_id"></p>
                                <p id="details"></p>
                                <hr>
                            </div>
                            <div class="col-md-12 p-0 m-0">
{{--                                <table width="100%" class="table table-bordered mb-0" id="offer_details_table">--}}
                                <table width="100%" class="table table-striped mb-0" id="offer_details_table">
                                    <thead>
                                    <tr>
                                        <th>{{__('provider.unit')}}</th>
                                        <th>{{__('provider.taste')}}</th>
                                        <th>{{__('provider.price')}}</th>
                                        <th id="product-price-number-th" hidden>{{__('provider.requests_number')}}</th>
                                        <th>{{__('provider.image')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody id="offer_details_tbody">
                                    <tr></tr>
                                    </tbody>
                                </table>
                            </div>
                            <input type="text" hidden>
                            <hr class="col-md-12 p0 m0">
                            <div class="col-sm-9">
                                <small class="price category-color">{{__('provider.created_at')}}:</small>
                                <small class="price category-color" id="created"></small>
                                <br>
                                <small class="price category-color">{{__('provider.updated_at')}}:</small>
                                <small class="price category-color" id="updated_at"></small>
                            </div>
                            <div class="btn_dele col-sm-3 text-center">
                            </div>
                        </div>
                    </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model show  Message--}}
