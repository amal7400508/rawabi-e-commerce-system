<script>

    let table_show = $('#table-show');
    let table_count = $('#table-count');

    $("select[name=table_length]").click(function () {
        let open = $(this).data("is_open");
        if (open) {
            window.location.href = $(this).val()
        }
        $(this).data("is_open", !open);
    });

    @can('create areas')
    $(document).on('click', '#openAddModal', function () {
        $('#form input:not(:first), #form textarea, #from select').val('')
            .removeClass('is-invalid');
        document.getElementById('product').value = '';
        $('#form span strong').empty();
        // $('#switchBootstrap18').attr('value', 1).attr('checked', true)
        //     .bootstrapSwitch('state', true);

        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('Stocks.store')}}")
            .attr("data_type", "add");
    });
    @endcan


    @can('update areas')
    /*start code edit*/
    let stock_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        stock_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('app/Stocks/edit')}}" + '/' + stock_id;
        $('#form input:not(:first), #form textarea, #from select')
            .val('')
            .removeClass('is-invalid');

        // $('#avatar').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('app/Stocks/update')}}" + '/' + stock_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);

            {{--$('#edit_image_ar').attr('required', false);--}}
            {{--$('#label-image').text("{{__('admin.image')}}");--}}
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            // console.log(data.country);
            document.getElementById('product').value = data.product;
            document.getElementById('pricing').value = data.pricing;
            document.getElementById('quantity').value = data.quantity;

            // if (data.status == 1) {
            //     $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            //         .bootstrapSwitch('state', true);
            // } else {
            //     $('#switchBootstrap18').attr('value', 1).attr('checked', false)
            //         .bootstrapSwitch('state', false);
            // }

            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/
    @endcan

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('id', stock_id);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                let response_data = data.stocks;

                $('#addModal').modal('hide');

                let  tr_color_red = '';
                // if (response_data.status == 1) {
                //     status = "<div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>";
                // } else {
                //     tr_color_red = 'tr-color-red';
                //     status = "<div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>";
                // }

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.product_name + "</td>");
                let col3 = $("<td>" + response_data.pricing+ "</td>");
                let col4 = $("<td>" + response_data.quantity + "</td>");
                let col5 = $("<td>" + response_data.actions + "</td>");

                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5).prependTo("#table");
                    this_row = $('#row_' + response_data.id);

                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            // let obj = JSON.parse((error.responseText)).error;
            // if (obj.name_ar !== undefined) {
            //     $('#form input[name=name_ar]').addClass('is-invalid')
            //         .parent().find('span strong').text(obj.name_ar);
            // }
            // if (obj.name_en !== undefined) {
            //     $('#form input[name=name_en]').addClass('is-invalid')
            //         .parent().find('span strong').text(obj.name_en);
            // }
            // if (obj.country !== undefined) {
            //     $('#form select[name=country]').addClass('is-invalid').parent()
            //         .find('span strong').text(obj.country);
            // }
        }
    });
    /*end code add or update*/

    @can('delete areas')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('app/Stocks/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
            this_row.remove();
        } catch (e) {
            this_row.removeClass('tr-color-active');
            console.log(777);
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan

</script>
