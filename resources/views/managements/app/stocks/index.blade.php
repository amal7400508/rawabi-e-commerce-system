@php($page_title = __('admin.opening_balances'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.encodings')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.opening_balances')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create stocks')
                                            <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.opening_balances')}}</a>
                                        @endcan
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('Stocks')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="product_no"
                                                                            @if($search_type == 'product_no') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="pricing_no"
                                                                            @if($search_type == 'pricing_no') selected @endif>{{__('admin.the_area')}}</option>
                                                                    <option value="quantity"
                                                                            @if($search_type == 'quantity') selected @endif>{{__('admin.country')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.product')}}</th>
                                                        <th>{{__('admin.pricing')}}</th>
                                                        <th>{{__('admin.quantity')}}</th>
                                                        @canany(['update areas', 'delete areas'])
                                                            <th>{{__('admin.action')}}</th>
                                                        @endcanany
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $stock)
                                                        <tr style="{{$stock->background_color_row}}">
                                                            <td hidden>{{$stock->updated_at}}</td>
                                                            <td class="font-default">{{$stock->id}}</td>

                                                            <td>{{$stock->product->name}}</td>
                                                            <td>{{$stock->pricing}}</td>
                                                            <td>{{$stock->quantity}}</td>
                                                            @canany(['update stocks', 'delete stocks'])
                                                                <td>
                                                                    {!! $stock->actions !!}
                                                                </td>
                                                            @endcanany
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.opening_balances')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>

                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12 p-0">
                                            <row>


                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="product">{{__('admin.product')}}<span class="danger">*</span></label>
                                                        <select class="form-control" id="product" required name="product">
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($products as $product)
                                                                <option value="{{$product->id}}">{{$product->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div
                                                        class="form-group">
                                                        <label for="pricing">{{__('admin.pricing')}} <span
                                                                class="danger">*</span> </label>
                                                        <br>
                                                        <select class="form-control "
                                                                id="pricing" required name="pricing">
                                                            <option value="">{{__('admin.select_option')}}</option>

                                                        </select>
                                                        @error('pricing')
                                                        <span class="invalid-feedback" role="alert">
                                                   <strong>{{ $message }}</strong>
                                                </span>
                                                        @enderror
                                                    </div>
                                                </div>
{{--                                                <div class="col-md-12">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="pricing">{{__('admin.pricing')}} <span class="danger">*</span></label>--}}
{{--                                                        <input type="number" id="pricing"--}}
{{--                                                               class="form-control"--}}
{{--                                                               name="pricing"--}}
{{--                                                               required--}}
{{--                                                        >--}}
{{--                                                        <span class="error-message">--}}
{{--                                                        <strong></strong>--}}
{{--                                                    </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="quantity">{{__('admin.quantity')}} <span class="danger">*</span></label>
                                                        <input type="number" id="quantity"
                                                               class="form-control"
                                                               name="quantity"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                            </row>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>


@endsection
@section('script')

    @include('managements.app.Stocks.js')




    <script>
        /*ajax falter categories start*/
        $(document).ready(function() {
            $('select[name="product"]').on('change', function() {
                var pricing_id = $(this).val();
                if (pricing_id) {
                    $.ajax({
                        url: "{{url('/app/Stocks/falter_product')}}" + "/" + pricing_id,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            $('select[name="pricing"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="pricing"]').append('<option value="' +
                                    value + '">' + value + '</option>');
                            });
                        },
                    });
                } else {
                    console.log('AJAX load did not work');
                }
            });
        });
        /*ajax falter categories end*/
    </script>
@endsection
