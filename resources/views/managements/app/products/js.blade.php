<script>

        @can('update products')
    let level;
    $(document).on('dblclick', '.change-level', async function () {
        level = $(this);
        level.attr('style', '');
        let level_no = level.attr('data-no');
        level.html('<input type="number" class="change-value" value="' + level_no + '">');
    });

    $(document).on('keypress', '.change-value', function (e) {
        let level_value = $(this).val();
        let id = level.attr('id');
        let route = "{{url('app/products/change-level')}}" + "/" + id;
        if (e.which == 13) {
            $(this).attr('disabled', true);
            $.ajax({
                type: 'POST',
                data: {
                    _token: $('input[name ="_token"]').val(),
                    level: level_value,
                },
                url: route,
                dataType: "json",
                success: function (data) {
                    level.attr('style', '');
                    level.text(level_value).attr('id', level_value);
                    level.attr('data-no', level_value)
                },
                error: function (data) {
                    level.attr('style', 'background: #c30015 !important');
                }
            })
        }
    });

    $(document).on('click', '.accept', async function () {
        let product_id = $(this).attr('id');
        let product_row = $(this).parent().parent().parent();
        try {
            let data = await
                statusChange(
                    product_id,
                    "{{route('products.accept')}}",
                    "{{__('admin.do_you_want_to_accept')}}!",
                    "", {{--{{__('admin.are_you_sure_you_want_to_accept_the_product')}}?",--}}
                        "warning",
                );
            product_row.attr('style', data.data.background_color_row);
            product_row.find('td:nth-last-child(1)').html(data.data.actions);
            product_row.find('td:nth-last-child(2)').text(data.data.current_balance);
            window.checkNotify();
            window.getNotify();
        } catch (e) {
            return e;
        }
    });

    $('.accept-all').on('click', async function () {
        let items_id = $('#table').find("input[name ='items-id[]']:checkbox:checked")
            .map(function () {
                return $(this).val()
            }).get();

        let route = "{{route('products.accept-all')}}";

        try {
            await statusChange(items_id,
                route,
                "{{__('admin.do_you_want_to_accept')}}!",
                "", {{--{{__('admin.are_you_sure_you_want_to_accept_the_product')}}?",--}}
                    "warning",
            );
            window.location.reload();
            $('#table').table().ajax.reload();
            $('#check-all-products').iCheck('uncheck');
            window.checkNotify();
            window.getNotify();
        } catch (e) {
            return e;
        }

    });
    $('.reject-all').on('click', async function () {
        let items_id = $('#table').find("input[name ='items-id[]']:checkbox:checked")
            .map(function () {
                return $(this).val()
            }).get();

        let route = "{{route('products.reject-all')}}";

        try {
            await statusChange(items_id,
                route,
                "{{__('admin.do_you_want_to_reject')}}!",
                "", {{--{{__('admin.are_you_sure_you_want_to_accept_the_product')}}?",--}}
                    "warning",
            );
            window.location.reload();
            $('#table').table().ajax.reload();
            $('#check-all-products').iCheck('uncheck');
            window.checkNotify();
            window.getNotify();
        } catch (e) {
            return e;
        }

    });

    $("#check-all-products").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $(document).on('click', '.reject', async function () {
        let product_id = $(this).attr('id');
        let product_row = $(this).parent().parent().parent();
        try {
            let data = await
                statusChange(
                    product_id,
                    "{{route('products.reject')}}",
                    "{{__('admin.do_you_want_to_reject')}}!",
                    "", {{--{{__('admin.are_you_sure_you_want_to_reject_this_product')}}?",--}}
                        "warning",
                );
            product_row.attr('style', data.data.background_color_row);
            product_row.find('td:nth-last-child(1)').html(data.data.actions);
            product_row.addClass('text-line-td');
            window.checkNotify();
            window.getNotify();
        } catch (e) {
            return e;
        }
    });
    $(document).on('click', '.statusClick', function () {
        let td = $(this).parent().parent().parent();

        let is_new = $(this).attr('data-status');
        let id_row = $(this).attr('data-id-row');
        let route = "{{url('app/products/update')}}" + "/" + id_row;
        $.ajax({
            type: 'POST',
            data: {
                _token: $('input[name ="_token"]').val(),
                is_new: is_new,
            },
            url: route,
            dataType: "json",
            success: function (data) {
                let status_append = statusAppend(data.id, data.is_new);
                td.empty().html(data.name + status_append);
                td.parent().attr('style', data.background_color_row);
            },
        });
    });

    function statusAppend(id, is_new) {

        let status_append = '<div class="float-right btn-group" role="group" aria-label="Button group with nested dropdown">';
        if (is_new == 1) {
            status_append +=
                '<button class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2"' +
                '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                '   {{__('admin.new')}}' +
                '</button>';
        } else {
            status_append +=
                '<button class="btn btn-sm btn-cyan dropdown-toggle dropdown-menu-right box-shadow-2"' +
                '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                '    {{__('admin.old')}}' +
                '</button>';
        }

        status_append +=
            '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
            '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="new">{{__('admin.new')}}</button>' +
            '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="old">{{__('admin.old')}}</button>' +
            '    </div>' +
            '</div>';

        return status_append;
    }
    @endcan

</script>
