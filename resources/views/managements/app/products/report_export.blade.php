@php($page_title = __('admin.report'))
@php($route = route('products.export'))
@extends('include.report')
@section('content')
    <div class="row">
        <div class="col-12">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-center" style="padding-bottom: 0px;">
                                <h4 class="card-title">
                                    {{__('admin.products_report')}}
                                </h4>
                                <hr>
                                <h4 class="card-title">{{__('admin.from')}} {{__('admin.date_1')}}
                                    @if(request()->report == 1)
                                        <span class="font-default color-blue">{{request()->from_2 ?? "_________"}}</span>
                                        {{__('admin.to')}} {{__('admin.date_1')}}
                                        <span class="font-default color-blue">{{request()->to_2 ?? \Carbon\Carbon::now()->toDateString()}}</span>
                                    @else
                                        <span class="font-default color-blue">{{request()->from ?? "_________"}}</span>
                                        {{__('admin.to')}} {{__('admin.date_1')}}
                                        <span class="font-default color-blue">{{request()->to ?? \Carbon\Carbon::now()->toDateString()}}</span>
                                    @endif
                                </h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table width="100%" class="table  zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>{{__('admin.item_code')}}</th>
                                                <th style="min-width: 250px">{{__('admin.name')}}</th>
                                                <th>{{__('admin.category')}}</th>
                                                <th>{{__('admin.type')}}</th>
                                                <th>{{__('admin.status')}}</th>
                                                <th>{{__('admin.image')}}</th>
                                                <th>{{__('admin.requests_number')}}</th>
                                                <th>{{__('admin.Date')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($data ) == 0)
                                                <tr>
                                                    <td colspan="9" class="text-center">
                                                        {{__('admin.no_data')}}
                                                        <hr>
                                                    </td>
                                                </tr>
                                            @endif
                                            @foreach($data as $product)
                                                <tr style="{{$product->background_color_row}}">
                                                    <td hidden>{{$product->updated_at}}</td>
                                                    <td>{{$product->number}}</td>
                                                    <td>
                                                        {{--                                                                {{$product->name}}--}}
                                                        {!! $product->is_new != 1 ? $product->name : $product->name . "<span class='badge badge btn-primary float-right'>". __('admin.new') ."</span>" !!}

                                                    </td>
                                                    <td>{{$product->category->name}}</td>
                                                    <td>
                                                        @if ($product->type == 'public')
                                                            <span class='btn btn-sm btn-outline-primary'>{{__('admin.public')}}</span>
                                                        @else
                                                            <span class='btn btn-sm btn-outline-danger'>{{__('admin.private')}}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($product->status == 1)
                                                            <div class='fonticon-wrap'><i class='ft-unlock' style='color:#0679f0 '></i></div>
                                                        @else
                                                            <div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021 '></i></div>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <img style="max-height:64px; max-width:64px" src="{{url('/').'/storage/thumbnail/64/'. $product->image}}" onerror=this.src="{{ url('/storage/thumbnail/150/logo.png')}}">
                                                    </td>
                                                    <td>
                                                        {{mostProduct($product->id, request()->from_date_requests_number, request()->to_date_requests_number)}}
                                                    </td>
                                                    <td>{{$product->created_at}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
