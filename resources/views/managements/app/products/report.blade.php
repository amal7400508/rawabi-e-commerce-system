@php($page_title = __('admin.products_report'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.product')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/')}}"><i class="la la-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{route('products')}}">{{__('admin.products')}}</a></li>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.products_report')}}</li>
                        </ol>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.create')}} {{__('admin.products_report')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('products.report')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <form method="get">
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.from')}}</label>
                                                                        <input type="date" class="form-control"
                                                                               value="{{request()->from}}" name="from">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.to')}}</label>
                                                                        <input value="{{request()->to}}" type="date" class="form-control" name="to">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.name')}} </label>
                                                                        <input value="{{request()->name}}" type="text" class="form-control" name="name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.id')}}</label>
                                                                        <input value="{{request()->id}}" type="number" class="form-control" name="id"/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label style="margin: 36px 4px 0 18px;">{{__('admin.new')}}</label>
                                                                        <input @if(request()->is_new == 1) checked @endif type="checkbox" value="1" name="is_new">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.status')}}</label>
                                                                        <select class="form-control" name="status">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            <option value="1">{{__('admin.active')}}</option>
                                                                            <option value="0">{{__('admin.attitude')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.type')}}</label>
                                                                        <select class="form-control" name="type">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            <option value="public">{{__('admin.public')}}</option>
                                                                            <option value="private">{{__('admin.private')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
{{--                                                                <div class="col-md-12">--}}
{{--                                                                    <div class="form-group">--}}
{{--                                                                        <label>{{__('admin.providers')}}</label>--}}
{{--                                                                        <select class="form-control" name="provider" id="provider">--}}
{{--                                                                            <option value="">{{__('admin.select_option')}}</option>--}}
{{--                                                                            @foreach($providers as $provider)--}}
{{--                                                                                <option value="{{$provider->id}}">{{$provider->name}}</option>--}}
{{--                                                                            @endforeach--}}
{{--                                                                        </select>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.branch_name')}}</label>
                                                                        <select class="form-control" name="branch_name" id="branch_name">
                                                                            @foreach($branches as $branche)
                                                                                <option value="{{$branche->id}}">{{$branche->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.categories')}}</label>
                                                                        <select class="form-control all-categories" name="category" id="categories">
                                                                            @foreach($categories as $category)
                                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.products')}}</label>
                                                                        <select
                                                                            class="form-control product product-filter"
                                                                            id="product"
                                                                            name="product_id">
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.from') .': '. __('admin.date_1') .' '. __('admin.requests_number')}}</label>
                                                                        <input value="{{request()->from_date_requests_number}}" type="date" class="form-control" name="from_date_requests_number">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.to') .': '. __('admin.date_1') .' '. __('admin.requests_number')}}</label>
                                                                        <input value="{{request()->to_date_requests_number}}" type="date" class="form-control" name="to_date_requests_number">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions pull-right">
{{--                                                    <button type="submit" formaction="{{route('products.report')}}" class=" btn btn-primary" id="button_preview"--}}
{{--                                                            style="margin-bottom: 10px; width: 120px">--}}
{{--                                                        <i class="ft-eye"></i> {{__('admin.preview')}}--}}
{{--                                                    </button>--}}
                                                    <button type="submit" formtarget="_blank" formaction="{{route('products.report_export')}}" class=" btn btn-primary" id="button_export"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-printer"></i> {{__('admin.report')}}
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.post_index')}} {{__('admin.product')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('products.report')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                </div>
                                                <table width="100%" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('admin.item_code')}}</th>
                                                        <th style="min-width: 150px">{{__('admin.name')}}</th>

                                                        <th>{{__('admin.category')}}</th>
                                                        <th>{{__('admin.type')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.image')}}</th>
                                                        <th>{{__('admin.requests_number')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data ) == 0)
                                                        <tr>
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $product)
                                                        <tr style="{{$product->background_color_row}}">
                                                            <td hidden>{{$product->updated_at}}</td>
                                                            <td>{{$product->number}}</td>
                                                            <td>
                                                                {{--                                                                {{$product->name}}--}}
                                                                {!! $product->is_new != 1 ? $product->name : $product->name . "<span class='badge badge btn-primary float-right'>". __('admin.new') ."</span>" !!}

                                                            </td>
                                                            <td>{{$product->category->name}}</td>
                                                            <td>
                                                                @if ($product->type == 'public')
                                                                    <span class='btn btn-sm btn-outline-primary'>{{__('admin.public')}}</span>
                                                                @else
                                                                    <span class='btn btn-sm btn-outline-danger'>{{__('admin.private')}}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($product->status == 1)
                                                                    <div class='fonticon-wrap'><i class='ft-unlock' style='color:#0679f0 '></i></div>
                                                                @else
                                                                    <div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021 '></i></div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <img style="max-height:64px; max-width:64px" src="{{url('/').'/storage/thumbnail/64/'. $product->image}}" onerror=this.src="{{ url('/storage/thumbnail/150/logo.png')}}">
                                                            </td>
                                                            <td>
                                                                {{mostProduct($product->id, request()->from_date_requests_number, request()->to_date_requests_number)}}
                                                            </td>
                                                            <td>
                                                                <button type="button" id="{{$product->id}}" class="show-detail btn btn-primary btn-sm" data-toggle="show" title="{{__("admin.show")}}"><i class="ft ft-eye"></i></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            {{ $data->appends($pagination_links)->links() }}

                                            <span>{{__('admin.show').' '. $data->count() .' '.__('admin.out_of').' '. $data_count . ' '. __('admin.record')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    {{-- start model show  Message--}}
    @include('managements.app.products.show')
    {{-- end model show  Message--}}

@endsection
@section('script')
    {{--    @include('managements.app.products.show_js')--}}

    <script>
        $("select[name=category]").val("{{request()->category}}");
        $("select[name=status]").val("{{request()->status}}");
        $("select[name=type]").val("{{request()->type}}");

        $('#offer_details_table')
            .addClass('table-white-space')
            .addClass('table-responsive')
            .css('display', 'block');

        $('#product-price-number-th').attr('hidden', false);


        $('#product-price-number-th').attr('hidden', false);
        let product_details_id;
        $(document).on('click', '.show-detail', function () {
            product_details_id = $(this).attr('id');
            detail_url = "{{url('app/products/show')}}" + "/" + product_details_id;
            let report_data = {
                start_date: $('input[name=from_date_requests_number]').val(),
                end_date: $('input[name=to_date_requests_number]').val()
            };
            $.ajax({
                type: 'GET',
                url: detail_url,
                data: report_data,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $("#offer_details_tbody").empty();
                    let data_unit;
                    for (i = 0; i < data.product_price.length; i++) {
                        variable_coloer = data.product_price[i].order_availability == 1 ? '#fff' : '#feeff0';
                        data_unit = data.product_price[i].id_unit != 1 ? data.product_price[i].unit_name : "<span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>";

                        if (data.product_price[i].image == null) {
                            data.product_price[i].image = 'thumbnail/150/logo.png';
                        }
                        {{--$("#offer_details_table").append("<tr style='background-color:" + variable_coloer + "'><td>" + data_unit + "</td><td>" + data_taste + "</td>" +--}}
                        {{--    "<td>" + data.product_price[i].price + "</td><td><img  style='max-height:50px; max-width:50px' src='{{ url('/').'/storage/product_price/64/' }}" + data.product_price[i].image + "' /></td></tr>");--}}
                        $("#offer_details_table").append(
                            '<tr style="background-color:' + variable_coloer + '">' +
                            '<td>' + data_unit + '</td>' +
                            '<td>' + data.product_price[i].price + '</td>' +
                            '<td >' + data.product_price[i].request_number + '</td>' +
                            '<td><img  style="max-height:50px; max-width:50px" src="{{ url('/').'/storage/product_price/64/' }}' + data.product_price[i].image + '" onerror=this.src="{{asset('storage/thumbnail/150/logo.png')}}" /></td>' +
                            '</tr>'
                        );
                    }
                    if (data.type == 'public') {
                        $('#type').html("<span class='btn btn-sm btn-outline-primary'>{{__('admin.public')}}</span>")
                    } else {
                        $('#type').html("<span class='btn btn-sm btn-outline-danger'>{{__('admin.private')}}</span>")
                    }
                    if (data.status == 1) {
                        $('#status').html('<span class="btn btn-primary position-absolute round" style="margin:10px;" ><i class="ft-unlock"></i></span>');
                    } else {
                        $('#status').html('<span class="btn btn-danger position-absolute round" style="margin:10px;" ><i class="ft-lock"></i></span>');
                    }
                    $('#show-image').attr('src', '{{asset('/storage/thumbnail/640/')}}' + '/' + data.image);
                    $('#name').text(data.name);
                    $('#id').text(data.id);
                    $(' #details').text(data.details);
                    $(' #taste').text(data.taste);
                    $(' #image').text(data.image);
                    if (data.is_new == '1') {
                        $(' #is_new').html("<span class='btn btn-sm btn-primary float-right'>{{__('admin.new')}}<span>");
                    }
                    data.state_id == 2 ? $(' #state_id').html("{{__('admin.this_product_is_available_in_Ibb')}}.") :
                        $(' #state_id').empty();

                    $(' #category_translations_name').text(data.category_translations_name);
                    $('#created').text(data.created_at);
                    $('#updated_at').html(data.updated_at);
                    $(' #btn_dele').html(data.btn);
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code show details ajax*/

        $(document).on('change', '#provider', function () {
            let provider_id = $(this).val();
            let url = "{{url('app/products/get-branch')}}" + "/" + provider_id;

            let select_branch = $("#branch_name");
            if (provider_id) {
                $.ajax({
                    type: "GET",
                    url: url,
                    beforeSend: function () {
                        select_branch.empty();
                    },
                    success: function (data) {
                        select_branch.html('<option value="">{{__('admin.select_option')}}</option>');
                        $.each(data, function (key, value) {
                            select_branch.append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    },
                });
            }
        });

        /*ajax falter start*/
        $(document).on('change', '#branch_name', function () {
            let category_id = $(this).val();
            let cate = $('#categories');
            let url = "{{url('app/products/get-categories')}}" + "/" + category_id;
            if (category_id) {
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (data) {
                        cate.empty();
                        cate.append('<option value="">{{__("admin.select_option")}}</option>');
                        data.categories.forEach(myFunction);

                        function myFunction(item) {
                            cate.append('<option value="' + item.id + '">' + item.name + '</option>');
                        }
                    }
                });
            } else {
                cate.empty();
            }
        });
        /*ajax falter end*/

        /*ajax falter products start*/
        $(document).on('change', '#categories', function () {
            let product_id = $(this).val();
            let pro = $('#product');
            let url = "{{url('app/products/get-products')}}" + "/" + product_id;
            if (product_id) {
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (data) {
                        pro.empty();
                        pro.append('<option value="">{{__("admin.select_option")}}</option>');
                        data.products.forEach(myFunction);

                        function myFunction(item) {
                            pro.append('<option value="' + item.id + '">' + item.name + '</option>');
                        }
                    }
                });
            } else {
                pro.empty();
            }
        });

        /*ajax falter products end*/
    </script>

@endsection
