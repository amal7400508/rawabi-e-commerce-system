@php($page_title = __('admin.products'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/icheck/custom.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.user_application')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.products')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                {{--                <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">--}}
                {{--                    <button class="btn btn-primary dropdown-menu-right box-shadow-2 px-2 dropdown-toggle "--}}
                {{--                            id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"--}}
                {{--                            aria-expanded="false">--}}
                {{--                        <i class="ft-filter icon-left"></i> {{__('admin.filter')}} {{__('admin.branches')}}--}}
                {{--                    </button>--}}
                {{--                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">--}}
                {{--                        <a href="?filter_name=" class="dropdown-item filterBranchClick" id="">{{__('admin.all')}}</a>--}}
                {{--                        @foreach($branches as $branch)--}}
                {{--                            <a href="?filter_id={{$branch->id}}" class="dropdown-item filterBranchClick" id="{{$branch->id}}">{{$branch->name}}</a>--}}
                {{--                        @endforeach--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>

        </div>

        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('products')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="name" @if($search_type == 'name') selected @endif>{{__('admin.name')}}</option>
                                                                    <option value="category" @if($search_type == 'category') selected @endif>{{__('admin.category')}}</option>
                                                                    <option value="branch_name" @if($search_type == 'branch_name') selected @endif>{{__('admin.branch_name')}}</option>
                                                                    <option value="image" @if($search_type == 'image') selected @endif>{{__('admin.image_null')}}</option>
                                                                    <option value="is_approved" @if($search_type == 'is_approved') selected @endif>{{__('admin.accept_and_reject')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                @can('update products')
                                                    <button type="button" {{--id="button-delete-all"--}} class="accept-all btn btn-sm btn-blue mb-1 span">{{__('admin.accept')}} {{__('admin.all')}}</button>
                                                    <button type="button" {{--id="button-delete-all"--}} class="reject-all btn btn-sm btn-red mb-1 span">{{__('admin.reject')}} {{__('admin.all')}}</button>
                                                @endcan
                                                <table width="100%" id="table" class="table icheck zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        @can('update products')
                                                            <th><input type="checkbox" class="input-chk icheckbox_square-blue" id="check-all-products"></th>
                                                        @endcan
                                                        <th style="width: 10px">{{__('admin.item_code')}}</th>
                                                        <th>{{__('admin.branch_name')}}</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th>{{__('admin.first_pricing')}}</th>
                                                        {{--                                                        <th>{{__('admin.categories')}}</th>--}}
                                                        <th style="width: 10px">{{__('admin.level')}}</th>
                                                        <th style="width: 10px">{{__('admin.status')}}</th>
                                                        <th>{{__('admin.image')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $product)
                                                        <tr style="{{$product->background_color_row}}" class="@if($product->is_approved == 3) text-line-td @endif">
                                                            <td hidden>{{$product->updated_at}}</td>
                                                            @can('update products')
                                                                <td><input name="items-id[]" value={{$product->id}} type="checkbox" class="input-chk icheckbox_square-blue" id="check-all{{$product->id}}"></td>
                                                            @endcan
                                                            <td class="font-default">{{$product->number}}</td>
                                                            <td class="font-default">{{$product->branch->name ?? null}}</td>
                                                            {{--<td>
                                                                {!! $product->is_new != 1 ? $product->name : $product->name . "<button class='is_new badge badge btn-primary float-right'>". __('admin.new') ."</button>" !!}
                                                            </td>--}}
                                                            <td>

                                                                {{$product->name}}
                                                                <div class="btn-group float-right" role="group" aria-label="Button group with nested dropdown">
                                                                    @if($product->is_new == 1)
                                                                        <button @can('update products') @else disabled @endcan class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            {{__('admin.new')}}
                                                                        </button>
                                                                    @else
                                                                        <button @can('update products') @else disabled @endcan class="btn btn-sm btn-cyan dropdown-toggle dropdown-menu-right box-shadow-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            {{__('admin.old')}}
                                                                        </button>
                                                                    @endif
                                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$product->id}}" data-status="new">{{__('admin.new')}}</button>
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$product->id}}" data-status="old">{{__('admin.old')}}</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="font-default">{{$product->productPrice->first()->price ?? null}}</td>
                                                            {{--                                                            <td>{{$product->category->name ?? null}}</td>--}}
                                                            <td>
                                                                <button id="{{$product->id}}" data-no="{{$product->level}}" @can('update products') title="Double click to change" @else disabled @endcan
                                                                class="btn btn-sm btn-primary font-default change-level">
                                                                    {{$product->level}}
                                                                </button>
                                                            </td>
                                                            <td>
                                                                @if ($product->status == 1)
                                                                    <div class='fonticon-wrap'><i class='ft-unlock' style='color:#0679f0'></i></div>
                                                                @else
                                                                    <div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <img style="max-height:64px; max-width:64px" src="{{url('/').'/storage/thumbnail/64/'.$product->image}}" onerror=this.src="{{ url('/storage/thumbnail/150/logo.png')}}">
                                                            </td>
                                                            <td>
                                                                {!! $product->actions !!}
                                                                {{--                                                                <a><small class="show-detail color-info" id="{{$product->id}}">{{__('admin.detail')}}...</small></a>--}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    @include('managements.app.products.show')
@endsection
@section('script')
    @include('managements.app.products.show_js')
    @include('managements.app.products.js')
@endsection
