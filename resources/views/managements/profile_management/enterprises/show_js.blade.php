<script>
    /*start code show details ajax*/
    let provider_id;
    $(document).on('click', '.show-detail-branch', async function () {
        provider_id = $(this).attr('id');
        let url = "{{url('enterprises_management/enterprises/show')}}" + '/' + provider_id;

        try {
            let data = await responseEditOrShowData(url);
            if (data.status == 1) {
                $('#show_status').html('<i class="la la-unlock-alt color-primary"></i> {{__('admin.active')}}');
            } else {
                $('#show_status').html(' <button class="btn btn-outline-danger btn-sm">{{__('admin.attitude')}}</button>');
            }
            $('#Gender').text(data.gender);
            $('.form-details .name').text(data.provider_name);
            $('.form-details .email').text(data.provider_email);
            $('.form-details .phone').text(data.provider_phone);
            $('#image_show').attr('src', data.image);

            $('#id').text(data.id);
            $('#branch_name').text(data.name);
            $('#show_type').text(data.type);
            if (data.type === 'enterprise') {
                $('#show_type').html('{{__('admin.enterprise')}}');
            } else {
                $('#show_type').html('{{__('admin.insurance')}}');
            }
            $('#branch_email').text(data.email);
            $('#branch_phone').text(data.phone);
            // $('#area').text(data.area);
            $('#address').text(data.address);
            // $('#sections').text(data.sections);
            $('.created').text(data.created_at);
            $('.updated_at').html(data.updated_at);

            $('#ModalShowBranch').modal('show');
        } catch (error) {
            return error;
        }
    });
    /*end code show details ajax*/
</script>
