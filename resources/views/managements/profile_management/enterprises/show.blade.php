{{-- start model show  Message--}}
<div id="ModalShowBranch" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="card-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                </button>
                <div class="form-details card-body row">
                    <div class="user-display col-4" style="    padding-right: 30px;    padding-top: 10px;">
                        <img src="" class="rounded-circle img-thumbnail img-user-show-100" id="image_show"
                             onerror="this.src='{{asset('storage/default-carrier.jpg')}}'"
                        >
                    </div>
                    <div class="user-display col-8 p-0">
                        <small class="font-Dinar" style="margin: auto 24px;">{{__('admin.provider_data')}}</small>
                        <h2 class="name product-title"
                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif padding-top: 4px;"></h2>
                        <h6 class="email product-title font-default"
                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                        <h6 class="phone product-title font-default"
                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                    </div>
                </div>
                <h6 style="margin: 10px 25px">{{__('admin.branch_data')}}</h6>
                <table class="table mb-0">
                    <tr>
                        <th>{{__('admin.id')}}</th>
                        <td id="id"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.branch_name')}}</th>
                        <td id="branch_name"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.email')}}</th>
                        <td id="branch_email"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.phone')}}</th>
                        <td id="branch_phone"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.type')}}</th>
                        <td id="show_type"></td>
                    </tr>
                    {{--<tr>
                        <th>{{__('admin.the_area')}}</th>
                        <td id="area"></td>
                    </tr>--}}
                    <tr>
                        <th>{{__('admin.address')}}</th>
                        <td id="address"></td>
                    </tr>
                    {{--<tr>
                        <th>{{__('admin.sections')}}</th>
                        <td id="sections"></td>
                    </tr>--}}
                    <tr>
                        <th>{{__('admin.status')}}</th>
                        <td id="show_status"></td>
                    </tr>
                </table>
                <div class="row p-1">
                    <div class="col-sm-12">
                        <span class="details price" style="color: #002581"></span>
                    </div>
                    <div class="col-sm-9">
                        <small class="price category-color">{{__('admin.created_at')}}:</small>
                        <small class="price category-color created"></small>
                        <br>
                        <small class="price category-color">{{__('admin.updated_at')}}:</small>
                        <small class="price category-color updated_at"></small>
                    </div>
                    <div class="col-sm-3 text-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model show  Message--}}
