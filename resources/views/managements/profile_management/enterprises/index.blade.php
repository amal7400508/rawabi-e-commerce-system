@php($page_title = __('admin.enterprises'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.enterprises_management')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a></li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.enterprises')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.provider')}}</a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('enterprises')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="enterprise" @if($search_type == 'enterprise') selected @endif>{{__('admin.provider')}}</option>
                                                                    <option value="admin_name" @if($search_type == 'admin_name') selected @endif>{{__('admin.admin')}}</option>
                                                                    <option value="address" @if($search_type == 'address') selected @endif>{{__('admin.address')}}</option>
                                                                    <option value="phone" @if($search_type == 'phone') selected @endif>{{__('admin.phone')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.provider')}}</th>
                                                        <th>{{__('admin.admin')}}</th>
                                                        <th>{{__('admin.address')}}</th>
                                                        <th>{{__('admin.phone')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $enterprise)
                                                        <tr style="{{$enterprise->background_color_row}}" class="@if($enterprise->is_approved == 3) text-line-td @endif">
                                                            <td hidden>{{$enterprise->updated_at}}</td>
                                                            <td>{{$enterprise->id}}</td>
                                                            <td>{{$enterprise->name}}</td>
                                                            <td>{{$enterprise->admin_name}}</td>
                                                            <td>{{$enterprise->address ?? null}}</td>
                                                            <td>{{$enterprise->phone}}</td>
                                                            <td>
                                                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                                                    @if($enterprise->status == 1)
                                                                        <button
                                                                            @can('update enterprises')
                                                                            @else disabled
                                                                            @endcan
                                                                            class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-unlock icon-left"></i> {{__('admin.active')}}
                                                                        </button>
                                                                    @else
                                                                        <button
                                                                            @can('update enterprises')
                                                                            @else disabled
                                                                            @endcan
                                                                            class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}
                                                                        </button>
                                                                    @endif
                                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$enterprise->id}}" data-status="active">{{__('admin.active')}}</button>
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$enterprise->id}}" data-status="attitude">{{__('admin.attitude')}}</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>{!! $enterprise->actions !!}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">

                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.provider')}}</h5>
                            <div>
                                <ul class="list-inline mb-0">
                                    <fieldset>
                                        <div class="float-left">
                                            <input type="checkbox" class="switch" id="switch10" data-off-label="false"
                                                   data-on-label="false" data-icon-cls="fa"
                                                   data-off-icon-cls="icon-user-female" data-on-icon-cls="icon-user"
                                                   value="ذكر" checked name="gender"/>
                                        </div>
                                    </fieldset>
                                    <div class="form-group mt-1">
                                    </div>
                                </ul>
                            </div>
                            <div class="heading-elements" id="check_show">
                                <input type="checkbox" name="status" value="1"
                                       class="switchBootstrap" id="switchBootstrap18"
                                       data-on-color="primary" data-off-color="danger"
                                       data-on-text="{{__('admin.enable')}}"
                                       data-off-text="{{__('admin.disable')}}" data-color="primary"
                                       data-label-text="{{__('admin.status')}}"
                                       checked/>
                            </div>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6 p-0">
                                            <row>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="name_ar">{{__('admin.provider')}} <span class="danger">*</span></label>
                                                        <input type="text" id="name_ar"
                                                               class="form-control"
                                                               name="name_ar"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
{{--                                                <div class="col-md-12">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="name_en">{{__('admin.provider')}} (en)<span class="danger">*</span></label>--}}
{{--                                                        <input type="text" id="name_en"--}}
{{--                                                               class="form-control"--}}
{{--                                                               name="name_en"--}}
{{--                                                               required--}}
{{--                                                        >--}}
{{--                                                        <span class="error-message">--}}
{{--                                                        <strong></strong>--}}
{{--                                                    </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="admin_name_ar">{{__('admin.admin_name')}} <span class="danger">*</span></label>
                                                        <input type="text" id="admin_name_ar"
                                                               class="form-control"
                                                               name="admin_name_ar"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
{{--                                                <div class="col-md-12">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="admin_name_en">{{__('admin.admin_name')}} (en)<span class="danger">*</span></label>--}}
{{--                                                        <input type="text" id="admin_name_en"--}}
{{--                                                               class="form-control"--}}
{{--                                                               name="admin_name_en"--}}
{{--                                                               required--}}
{{--                                                        >--}}
{{--                                                        <span class="error-message">--}}
{{--                                                        <strong></strong>--}}
{{--                                                    </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-12">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="address_ar">{{__('admin.address')}} (ar)<span class="danger">*</span></label>--}}
{{--                                                        <input type="text" id="address_ar"--}}
{{--                                                               class="form-control"--}}
{{--                                                               name="address_ar"--}}
{{--                                                               required--}}
{{--                                                        >--}}
{{--                                                        <span class="error-message">--}}
{{--                                                            <strong></strong>--}}
{{--                                                        </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-12">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="address_en">{{__('admin.address')}} (en)<span class="danger">*</span></label>--}}
{{--                                                        <input type="text" id="address_en"--}}
{{--                                                               class="form-control"--}}
{{--                                                               name="address_en"--}}
{{--                                                               required--}}
{{--                                                        >--}}
{{--                                                        <span class="error-message">--}}
{{--                                                        <strong></strong>--}}
{{--                                                    </span>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="email">{{__('admin.email')}}<span class="danger">*</span></label>
                                                        <input type="email" id="email"
                                                               class="form-control"
                                                               name="email"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </row>
                                        </div>
                                        <div class="col-md-6 p-0">
                                            <row>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="password">{{__('admin.password')}}<span class="danger">*</span></label>
                                                        <input type="password" id="password"
                                                               class="form-control"
                                                               name="password"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="password_confirmation">{{__('admin.confirm_password')}}<span class="danger">*</span></label>
                                                        <input type="password" id="password_confirmation"
                                                               class="form-control"
                                                               name="password_confirmation"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="phone">{{__('admin.phone')}}<span class="danger">*</span></label>
                                                        <input type="number" id="phone"
                                                               class="form-control"
                                                               name="phone"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="whats_app">{{__('admin.whats_app')}}<span class="danger">*</span></label>
                                                        <input type="number" id="whats_app"
                                                               class="form-control"
                                                               name="whats_app"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="telephone">{{__('admin.telephone')}}<span class="danger">*</span></label>
                                                        <input type="number" id="telephone"
                                                               class="form-control"
                                                               name="telephone"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-12">
                                                    <label>{{__('admin.approval')}}</label>
                                                    <div class="input-group">
                                                        <div class="row icheck_minimal skin">
                                                            <fieldset style="margin-left: 20px; margin-right: 20px">
                                                                <input type="radio" name="is_approved" id="input-radio-15"
                                                                       checked value="1">
                                                                <label
                                                                    style="color: #002581">{{__('admin.accept')}}</label>
                                                            </fieldset>
                                                            <fieldset style="margin-left: 20px; margin-right: 20px">
                                                                <input type="radio" name="is_approved" id="input-radio-16"
                                                                       value="2">
                                                                <label
                                                                    style="color: #FFC107;">{{__('admin.reject')}}</label>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
{{--                                                <div class="form-group col-12">--}}
{{--                                                    <label>{{__('admin.type')}}</label>--}}
{{--                                                    <div class="input-group">--}}
{{--                                                        <div class="row icheck_minimal skin">--}}
{{--                                                            <fieldset style="margin-left: 20px; margin-right: 20px">--}}
{{--                                                                <input type="radio" name="type" id="input-radio-5"--}}
{{--                                                                       checked value="enterprise">--}}
{{--                                                                <label--}}
{{--                                                                    style="color: #002581">{{__('admin.provider')}}</label>--}}
{{--                                                            </fieldset>--}}
{{--                                                            <fieldset style="margin-left: 20px; margin-right: 20px">--}}
{{--                                                                <input type="radio" name="type" id="input-radio-6"--}}
{{--                                                                       value="insurance">--}}
{{--                                                                <label--}}
{{--                                                                    style="color: #FFC107;">{{__('admin.insurance_companies')}}</label>--}}
{{--                                                            </fieldset>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="image">{{__('admin.image')}} <span
                                                                class="danger">*</span></label>
                                                        <input type="file" accept=".jpg, .jpeg, .png" id="image" required
                                                               class="form-control @error('image') is-invalid @enderror"
                                                               name="image" onchange="loadAvatar(this);" autocomplete="image">
                                                        <br>
                                                        <button type="button" hidden onclick="delete_img()"
                                                                style="position: static; " class="btn btn-sm btn-outline-danger"
                                                                id="delete-img"><i class="ft-trash"></i></button>
                                                        <img id="avatar" style="max-width: 140px; height: auto; margin:10px;">
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>

                                            </row>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset  id="btn-save"
                                      class="form-group position-relative has-icon-left mb-0">

                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    {{--model show modal--}}
    @include('managements.profile_management.enterprises.show')
@endsection
@section('script')
    @include('managements.profile_management.enterprises.show_js')
    <script>

        function delete_img() {
            document.getElementById('image').value = null;
            $('#avatar').attr('hidden', true);
            $('#delete-img').attr('hidden', true);
        }

        function loadAvatar(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#avatar').attr('hidden', false);
                    $('#delete-img').attr('hidden', false);
                    var image = document.getElementById('avatar');
                    image.src = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
        }


        $(document).on('click', '#openAddModal', function () {

            $('#form input:not(:first), #form textarea').val('').removeClass('is-invalid');
            $('#form span strong').empty();
            $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                .bootstrapSwitch('state', true);
            $('#switch10').attr('value', 'ذكر').attr('checked', true);
            $('#input-radio-15').attr('value', 1).attr('checked', true);
            // $('#input-radio-5').attr('value', 'enterprise').attr('checked', true);
            // $('#input-radio-6').attr('value', 'insurance').attr('checked', false);
            $('#input-radio-16').attr('value', 3).attr('checked', false);
            delete_img();
            $('#addModal').modal('show');
            $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
                .attr("data_url", "{{route('profile_managment_provider.store')}}")
                .attr("data_type", "add");
        });

        @can('update enterprises')
        $(document).on('click', '.accept', async function () {
            let deposit_id = $(this).attr('id');
            let deposit_row = $(this).parent().parent().parent();
            try {
                let data = await
                    statusChange(
                        deposit_id,
                        "{{route('enterprises.accept')}}",
                        "{{__('admin.do_you_want_to_accept')}}!",
                        "",
                        "warning",
                    );
                deposit_row.attr('style', data.data.background_color_row);
                deposit_row.find('td:nth-last-child(1)').html(data.data.actions);
                window.checkNotify();
                window.getNotify();
            } catch (e) {
                return e;
            }
        });

        $(document).on('click', '.reject', async function () {
            let deposit_id = $(this).attr('id');
            let deposit_row = $(this).parent().parent().parent();
            try {
                let data = await
                    statusChange(
                        deposit_id,
                        "{{route('enterprises.reject')}}",
                        "{{__('admin.do_you_want_to_reject')}}!",
                        "",
                        "warning",
                    );
                deposit_row.attr('style', data.data.background_color_row);
                deposit_row.find('td:nth-last-child(1)').html(data.data.actions);
                deposit_row.addClass('text-line-td');
                window.checkNotify();
                window.getNotify();
            } catch (e) {
                return e;
            }
        });

        $(document).on('click', '.statusClick', function () {
            let td = $(this).parent().parent().parent();

            let status = $(this).attr('data-status');
            let id_row = $(this).attr('data-id-row');
            let route = "{{url('profile_management/enterprises/update')}}" + "/" + id_row;
            $.ajax({
                type: 'POST',
                data: {
                    _token: $('input[name ="_token"]').val(),
                    status: status,
                },
                url: route,
                dataType: "json",
                success: function (data) {
                    let status_append = statusAppend(data.id, data.status);
                    td.empty().html(status_append);
                    td.parent().attr('style', data.background_color_row);
                },
            });
        });

        let enterprise_id = 0 ;
        $('#form').on('submit', async function (event) {
            event.preventDefault();
            let btn_save = $('#btn-save');
            let url = btn_save.attr('data_url');
            let type = btn_save.attr('data_type');
            let data_form = new FormData(this);
            data_form.append('enterprise_id', enterprise_id);
            try {
                let data = await addOrUpdate(url, data_form, type, 'btn-save');
                if (data['status'] == 200) {
                    let response_data = data.enterprises;

                    $('#addModal').modal('hide');

                    let status, tr_color_red = '';
                    if (response_data.status == 1) {
                        status = "<div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>";
                    } else {
                        tr_color_red = 'tr-color-red';
                        status = "<div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>";
                    }

                    console.log(response_data);
                    let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                    let col1 = $("<td>" + response_data.id + "</td>");
                    let col2 = $("<td>" + response_data.name + "</td>");
                    let col3 = $("<td>" + response_data.admin_name + "</td>");
                    let col4 = $("<td>" + response_data.address + "</td>");
                    let col5 = $("<td>" + response_data.phone + "</td>");
                    let col6 = $("<td>" + statusAppend(response_data.id ,status ) + "</td>");
                    let col7 = $("<td>" + response_data.actions + "</td>");

                    let this_row;
                    if (type === 'add') {
                        row.append(col1, col2, col3, col4, col5, col6, col7).prependTo("#table");
                        this_row = $('#row_' + response_data.id);
                        table_show.text(parseInt(table_show.text()) + 1);
                        table_count.text(parseInt(table_count.text()) + 1);
                    } else {
                        this_row = edit_row;
                        edit_row.attr("class", tr_color_red).attr('style', '');
                        edit_row.empty().append(col1, col2, col3, col4, col5, col6, col7);
                    }

                    this_row.addClass('tr-color-success');
                    setTimeout(function () {
                        this_row.removeClass('tr-color-success');
                    }, 7000);
                }
            } catch (error) {
                let obj = JSON.parse((error.responseText)).error;
                //     if (obj.name_ar !== undefined) {
                //         $('#form input[name=name_ar]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.name_ar);
                //     }
                //     if (obj.name_en !== undefined) {
                //         $('#form input[name=name_en]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.name_en);
                //     }
                //     if (obj.admin_name_ar !== undefined) {
                //         $('#form input[name=admin_name_ar]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.admin_name_ar);
                //     }
                //     if (obj.admin_name_en !== undefined) {
                //         $('#form input[name=admin_name_en]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.admin_name_en);
                //     }
                //     if (obj.address_ar !== undefined) {
                //         $('#form input[name=address_ar]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.address_ar);
                //     }
                //     if (obj.address_en !== undefined) {
                //         $('#form input[name=address_en]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.address_en);
                //     }
                //     if (obj.email !== undefined) {
                //         $('#form input[name=email]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.email);
                //     }
                //     if (obj.password !== undefined) {
                //         $('#form [name=password]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.password);
                //     }
                //     if (obj.image !== undefined) {
                //         $('#form input[name=image]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.image);
                //     }
                //     if (obj.phone !== undefined) {
                //         $('#form input[name=phone]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.phone);
                //     }
                //     if (obj.telephone !== undefined) {
                //         $('#form input[name=telephone]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.telephone);
                //     }
                //     if (obj.whats_app !== undefined) {
                //         $('#form input[name=whats_app]').addClass('is-invalid')
                //             .parent().find('span strong').text(obj.whats_app);
                //     }
            }
        });

        @endcan

        function statusAppend(id, status) {

            let status_append = '<div class="btn-group" role="group" aria-label="Button group with nested dropdown">';
            if (status == 1) {
                status_append +=
                    '<button class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                    '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                    '        aria-expanded="false">' +
                    '    <i class="ft-unlock icon-left"></i> {{__('admin.active')}}' +
                    '</button>';
            } else {
                status_append +=
                    '<button class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                    '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                    '        aria-expanded="false">' +
                    '    <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}' +
                    '</button>';
            }

            status_append +=
                '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
                '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="active">{{__('admin.active')}}</button>' +
                '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="attitude">{{__('admin.attitude')}}</button>' +
                '    </div>' +
                '</div>';

            return status_append;
        }
    </script>

@endsection
