<script>

    let table_show = $('#table-show');
    let table_count = $('#table-count');

    function delete_img() {
        document.getElementById('edit_image_ar').value = null;
        $('#avatar').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
    }

    function loadAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                var image = document.getElementById('avatar');
                image.src = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("select[name=table_length]").click(function () {
        let open = $(this).data("is_open");
        if (open) {
            window.location.href = $(this).val()
        }
        $(this).data("is_open", !open);
    });

    @can('create categories')
    $(document).on('click', '#openAddModal', function () {
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);
        $('#switch10').attr('value', 'ذكر').attr('checked', true);
        $('#input-radio-15').attr('value', 1).attr('checked', true);
        delete_img();
        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('profile_managment_provider.store')}}")
            .attr("data_type", "add");
        $('input[name=level]').val("100");
    });
    @endcan

    /*start code edit*/

    $(document).on('click', '.accept', async function () {
        let deposit_id = $(this).attr('id');
        let deposit_row = $(this).parent().parent().parent();
        try {
            let data = await
                statusChange(
                    deposit_id,
                    "{{route('provider.accept')}}",
                    "{{__('admin.do_you_want_to_accept')}}!",
                    "",
                    "warning",
                );
            deposit_row.attr('style', data.data.background_color_row);
            deposit_row.find('td:nth-last-child(1)').html(data.data.actions);
            window.checkNotify();
            window.getNotify();
        } catch (e) {
            return e;
        }
    });

    $(document).on('click', '.reject', async function () {
        let deposit_id = $(this).attr('id');
        let deposit_row = $(this).parent().parent().parent();
        try {
            let data = await
                statusChange(
                    deposit_id,
                    "{{route('provider.reject')}}",
                    "{{__('admin.do_you_want_to_reject')}}!",
                    "",
                    "warning",
                );
            deposit_row.attr('style', data.data.background_color_row);
            deposit_row.find('td:nth-last-child(1)').html(data.data.actions);
            deposit_row.addClass('text-line-td');
            window.checkNotify();
            window.getNotify();
        } catch (e) {
            return e;
        }
    });

    $(document).on('click', '.statusClick', function () {
        let td = $(this).parent().parent().parent();

        let status = $(this).attr('data-status');
        let id_row = $(this).attr('data-id-row');
        let route = "{{url('profile_management/provider/update')}}" + "/" + id_row;
        $.ajax({
            type: 'POST',
            data: {
                _token: $('input[name ="_token"]').val(),
                status: status,
            },
            url: route,
            dataType: "json",
            success: function (data) {
                let status_append = statusAppend(data.id, data.status);
                td.empty().html(status_append);
                td.parent().attr('style', data.background_color_row);
            },
        });
    });

    let enterprise_id = 0 ;
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('provider_id', enterprise_id);
        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                let response_data = data.provider;

                $('#addModal').modal('hide');

                let status, tr_color_red = '';
                if (response_data.status == 1) {
                    status = "<div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>";
                } else {
                    tr_color_red = 'tr-color-red';
                    status = "<div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>";
                }

                console.log(response_data);
                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.name + "</td>");
                let col3 = $("<td>" + response_data.admin_name + "</td>");

                let col4 = $("<td>" + response_data.phone + "</td>");
                let col5 = $("<td>" + statusAppend(response_data.id ,status ) + "</td>");
                let col6 = $("<td>" + response_data.actions + "</td>");

                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5, col6).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5, col6);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
                if (obj.name !== undefined) {
                    $('#form input[name=name]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.name);
                }

                if (obj.admin_name !== undefined) {
                    $('#form input[name=admin_name]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.admin_name);
                }

                if (obj.email !== undefined) {
                    $('#form input[name=email]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.email);
                }
                if (obj.password !== undefined) {
                    $('#form [name=password]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.password);
                }
                if (obj.image !== undefined) {
                    $('#form input[name=image]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.image);
                }
                if (obj.phone !== undefined) {
                    $('#form input[name=phone]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.phone);
                }
                if (obj.telephone !== undefined) {
                    $('#form input[name=telephone]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.telephone);
                }
                if (obj.whats_app !== undefined) {
                    $('#form input[name=whats_app]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.whats_app);
                }
        }
    });



    function statusAppend(id, status) {

        let status_append = '<div class="btn-group" role="group" aria-label="Button group with nested dropdown">';
        if (status == 1) {
            status_append +=
                '<button class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                '        aria-expanded="false">' +
                '    <i class="ft-unlock icon-left"></i> {{__('admin.active')}}' +
                '</button>';
        } else {
            status_append +=
                '<button class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                '        aria-expanded="false">' +
                '    <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}' +
                '</button>';
        }

        status_append +=
            '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
            '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="active">{{__('admin.active')}}</button>' +
            '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="attitude">{{__('admin.attitude')}}</button>' +
            '    </div>' +
            '</div>';

        return status_append;
    }
    /*end code Delete ajax*/

</script>
