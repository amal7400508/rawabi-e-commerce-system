@php($page_title = __('admin.data').' '.__('admin.Branches'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.data')}} {{__('admin.Branches')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item active">{{__('admin.data')}} {{__('admin.Branches')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                {{--@can('delete branches')
                    <div class="btn-group float-md-right">
                        <a href="{{ url('/'.adminType().'branches/recycle_bin')}}" class="btn btn-primary"
                           style="color: white">{{__('admin.recycle_bin')}} <i class="ft-trash position-right"></i></a>
                    </div>
                @endcan--}}
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="card">
            <div class="card-head">
                <div class="card-header">


                            <a href="{{ route('branches.create')}}" class="btn btn-primary"><i
                                    class="ft-plus white"></i> {{__('admin.create')}} {{__('admin.Branches')}}
                            </a>


                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <table
                            class="table table-white-space table-bordered {{--row-grouping--}} display no-wrap icheck table-middle"
                            id="user_table">
                            <thead>
                            <tr>
                                <th hidden>{{__('admin.updated_at')}}</th>
                                <th>{{__('admin.name')}}</th>

                                <th>{{__('admin.address')}}</th>
                                <th>{{__('admin.sections')}}</th>
                                <th>{{__('admin.status')}}</th>
                                <th>{{__('admin.action')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                </div>
            </div>
        </div>
    </div>
    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center" style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark" data-dismiss="modal">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

    {{-- start model show  Message--}}
    <div id="confirmModalShow" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="  modal-content">
                <div class="card pull-up">
                    <div class="card-content">
                        <div class="card-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                            </button>
                            <h3>{{__('admin.post_index')}} {{__('admin.data')}} {{__('admin.Branches')}}</h3>
                            <hr>
                        </div>
                        <table class="table table-bordered md-0">
                            <tr>
                                <th>{{__('admin.id')}}</th>
                                <td id="id"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.name')}}</th>
                                <td id="name"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.area')}}</th>
                                <td id="area"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.provider')}}</th>
                                <td id="provider_id"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.address')}}</th>
                                <td id="address"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.sections')}}</th>
                                <td id="sections"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.email')}}</th>
                                <td id="email"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.phone')}}</th>
                                <td id="phone"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.lat')}}</th>
                                <td id="lat"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.long')}}</th>
                                <td id="long"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.status')}}</th>
                                <td id="status"></td>
                            </tr>
                            {{--<tr>
                                <th> {{__('admin.lat')}}</th>
                                <td id="lat"></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.long')}}</th>
                                <td id="long"></td>
                            </tr>--}}
                        </table>
                        <div class="form-details card-body ">
                            <div class="col-md-12">
                            </div>
                            <div class="row p-1">
                                <div class="col-sm-9">
                                    <small class="price category-color">{{__('admin.created_at')}}:</small>
                                    <small class="price category-color" id="created"></small>
                                    <br>
                                    <small class="price category-color">{{__('admin.updated_at')}}:</small>
                                    <small class="price category-color" id="updated_at"></small>
                                </div>
                                <div class="btn_dele col-sm-3 text-center">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show  Message--}}

    {{-- start model Don Delete Message--}}
    <div id="messageDonDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                        <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                        <strong id="title-delete">{{__('admin.successfully_done')}}!</strong>
                        <p id="message-delete">{{__('admin.deleted_successfully')}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--end model Don Delete Message--}}
@endsection
@section('script')
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    <!-- BEGIN: Page JS-->
    <script>
        /*start code show all data in datatable ajax*/
        $(document).ready(function () {
            $('#user_table').DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{  route('branches') }}",
                },
                columns: [
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                        class: 'updated_at'
                    },
                    {
                        data: 'name',
                        name: 'name',
                        class: 'name'
                    },
                    {
                        data: 'address',
                        name: 'address',
                        class: 'address'
                    },
                    {
                        data: 'sections',
                        name: 'sections',
                        class: 'sections',
                    },
                    {
                        data: 'status',
                        name: 'status',
                        class: 'status',
                        render: function (data, type, full, meta) {
                            if (data == 1) {
                                return "<div class='fonticon-wrap'><i class='ft-unlock' style='color:#002581 '></i></div>"
                            } else {
                                return "<div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021 '></i></div>"
                            }
                        },
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    }
                ],
                'order': [[0, 'desc']],
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });
        });
        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        /*end  message save*/

        /*start code Delete ajax*/
        var $branches_id;
        $(document).on('click', '.deleteB', function () {
            console.log('click befo');
            $branches_id = $(this).attr('id');
            $('#confirmModalDelete').modal('show');
            console.log($branches_id);
        });
        $('#ok_button').click(function () {
            console.log('click end');
            $.ajax({
                url: "{{ url('/branches')}}" + "/destroy/" + $branches_id,
                beforeSend: function () {
                    console.log('click before send'),
                        $('#ok_button').text('{{__('admin.deleting')}}...');
                },
                success: function (data) {
                    if (data.error_delete != undefined) {
                        $('#title-delete').text("{{__('admin.error_message')}}");
                        $('#message-delete').text(data.error_delete);
                        $('#messageDonDelete').modal('show');
                        setTimeout(function () {
                            $('#messageDonDelete').modal('hide');
                        }, 3000,);
                        $('#ok_button').text('{{__('admin.yes')}}');
                    } else {
                        setTimeout(function () {
                            $('#title-delete').text("{{__('admin.successfully_done')}}");
                            $('#message-delete').text("{{__('admin.deleted_successfully')}}.");
                            $('#confirmModalDelete').modal('hide');
                            $('#user_table').DataTable().ajax.reload();
                            $('#ok_button').text('{{__('admin.yes')}}');
                        }, 500);
                        $('#messageDonDelete').modal('hide');
                        setTimeout(function () {
                            $('#messageDonDelete').modal('show');
                        }, 510,);
                        setTimeout(function () {
                            $('#messageDonDelete').modal('hide');
                        }, 3000,);
                        $('#confirmModalShow').modal('hide');
                    }
                },
                error: function (data) {
                    console.log('click failed')
                }
            })
        });
        /*end code Delete ajax*/

        /*start code show details ajax*/
        var branches_detals_id;
        $(document).on('click', '.showB', function () {
            console.log('don on click');
            branches_detals_id = $(this).attr('id');
            detail_url = "branches/show/" + branches_detals_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#name').text(data.name);
                    $('#area').text(data.area);
                    $('#provider_id').text(data.provider);
                    $('#id').text(data.id);
                    $(' #address').text(data.address);
                    $(' #lat').text(data.lat);
                    $(' #long').text(data.long);
                    $(' #sections').text(data.sections);
                    if (data.status == 1) {
                        $('#status').html('<button class="btn btn-round btn-primary btn-sm">{{__('admin.active')}}</button>');
                        $('#table-desc-admin').attr('style', 'background-color: #fff');
                    } else if (data.status == 0) {
                        $('#status').html('<button class="btn btn-round btn-danger btn-sm">{{__('admin.attitude')}}</button>');
                        $('#table-desc-admin').attr('style', 'background-color: #fefafa');
                    }
                    $(' #email').text(data.email);
                    $(' #phone').text(data.phone);
                    /*$(' #lat').text(data.lat);
                    $(' #long').text(data.long);*/
                    $('#created').text(data.created_at_g);
                    $('#updated_at').html(data.updated_at_g);
                    $(' #btn_dele').html(data.btn);
                    $('#confirmModalShow').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
    </script>
    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
@endsection
