@php($page_title = __('admin.data').' '.__('admin.Branches'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block"> {{__('admin.data')}} {{__('admin.Branches')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a
                                    href="{{ url('/branches')}}">{{__('admin.data')}} {{__('admin.Branches')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.create')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
            </div>
            @if(session('success'))
                <div id="messageSave" class="modal fade text-left" role="dialog">
                    <div class="modal-dialog">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                    <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                    <strong>{{__('admin.successfully_done')}}!</strong>
                                    <p>{{session('success')}}.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if($branches->is_approved == 0)
            <div class="col-md-12">
                <div class="card">
                    <form class="form" action="{{ route('branches.update',['id'=>$branches->id])}}" method="POST"
                          id="my_form_id">
                        @csrf
                        <div class="card-header">
                            <h4 class="card-title"
                                id="from-actions-top-bottom-center">{{__('admin.create')}} {{__('admin.data')}} {{__('admin.Branches')}}</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                            </div>
                        </div>
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('area_id') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.the_area')}}
                                                        <span class="danger">*</span></label>
                                                    <br>
                                                    <select
                                                        class="form-control @error('area_id') is-invalid @enderror"
                                                        id="area_id" required name="area_id">
                                                        <option value="">{{__('admin.select_option')}}</option>
                                                        @foreach($all_areas as $area)
                                                            <option value="{{$area->id}}"
                                                                    @if($branches->area_id == $area->id) selected @endif>{{$area->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('area_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('ar_name_B') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.name')}} (ar) <span
                                                            class="danger">*</span></label>
                                                    <input type="text" id="edit_name_ar"
                                                           value="{{ $branches_ar->name }}"
                                                           class="form-control @error('ar_name_B') is-invalid @enderror"
                                                           name="ar_name_B" autocomplete="ar_name_B" autofocus>
                                                    @error('ar_name_B')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('en_name_B') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.name')}} (en) <span
                                                            class="danger">*</span></label>
                                                    <input type="text" id="edit_name_en"
                                                           value="{{ $branches_en->name }}"
                                                           class="form-control @error('en_name_B') is-invalid @enderror"
                                                           name="en_name_B" autocomplete="en_name_B">
                                                    @error('en_name_B')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('ar_address') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.address')}} (ar) <span
                                                            class="danger">*</span></label>
                                                    <input type="text" id="edit_address_ar"
                                                           value="{{ $branches_ar->address }}"
                                                           class="form-control @error('ar_address') is-invalid @enderror"
                                                           name="ar_address" autocomplete="ar_address" autofocus>
                                                    @error('ar_address')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('en_address') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.address')}} (en) <span
                                                            class="danger">*</span></label>
                                                    <input type="text" id="edit_address_en"
                                                           value="{{ $branches_en->address }}"
                                                           class="form-control @error('en_address') is-invalid @enderror"
                                                           name="en_address" autocomplete="en_address" autofocus>
                                                    @error('en_address')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group col-12 mb-2">
                                                <label>{{__('admin.status')}}</label>
                                                <div class="input-group">
                                                    <div class="row icheck_minimal skin">
                                                        <fieldset style="margin-left: 20px; margin-right: 20px">
                                                            <input type="radio" name="status" id="yes1" required value="1" >
                                                            <label style="color: #002581"
                                                                   for="yes1">{{__('admin.active')}}</label>
                                                        </fieldset>
                                                        <fieldset style="margin-left: 20px; margin-right: 20px">
                                                            <input type="radio" name="status"
                                                                   class="custom-control-input" required value="0" id="no1" >
                                                            <label style="color: #FFC107;"
                                                                   for="no1">{{__('admin.attitude')}}</label>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.phone')}}<span
                                                            class="danger">*</span></label>
                                                    <input type="number" id="edit_phone" value="{{ $branches->phone }}"
                                                           class="form-control @error('phone') is-invalid @enderror"
                                                           name="phone" autocomplete="phone" autofocus>
                                                    @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('ar_sections') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.sections')}} (ar) <span
                                                            class="danger">*</span></label>
                                                    <input type="text" id="edit_sections_ar"
                                                           value="{{ $branches_ar->sections }}"
                                                           class="form-control @error('ar_sections') is-invalid @enderror"
                                                           name="ar_sections" autocomplete="ar_sections" autofocus>
                                                    @error('ar_sections')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('en_sections') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.sections')}} (en) <span
                                                            class="danger">*</span></label>
                                                    <input type="text" id="edit_sections_en"
                                                           value="{{ $branches_en->sections }}"
                                                           class="form-control @error('en_sections') is-invalid @enderror"
                                                           name="en_sections" autocomplete="en_sections" autofocus>
                                                    @error('en_sections')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.email')}}<span
                                                            class="danger">*</span></label>
                                                    <input type="text" id="edit_email" value="{{ $branches->email }}"
                                                           class="form-control @error('email') is-invalid @enderror"
                                                           name="email" autocomplete="email" autofocus>
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('provider_id') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.provider')}}
                                                        <span class="danger">*</span></label>
                                                    <br>
                                                    <select
                                                        class="form-control @error('provider_id') is-invalid @enderror"
                                                        id="provider_id" required name="provider_id">
                                                        <option value="">{{__('admin.select_option')}}</option>
                                                        @foreach($all_provider as $provider)
                                                            <option value="{{$provider->id}}"
                                                                    @if($branches->provider_id == $provider->id) selected @endif>{{$provider->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('provider_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="form-group col-6 {{ $errors->has('lat') ? ' has-error' : '' }}">
                                                        <label for="projectinput2">{{__('admin.lat')}}<span
                                                                class="danger">*</span></label>
                                                        <input type="text" id="edit_lat" value="{{ $branches->lat }}"
                                                               class="form-control @error('lat') is-invalid @enderror"
                                                               name="lat" autocomplete="lat" autofocus>
                                                        @error('lat')
                                                        <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-6 {{ $errors->has('long') ? ' has-error' : '' }}">
                                                        <label for="projectinput2">{{__('admin.long')}}<span
                                                                class="danger">*</span></label>
                                                        <input type="text" id="edit_long" value="{{ $branches->long }}"
                                                               class="form-control @error('long') is-invalid @enderror"
                                                               name="long" autocomplete="long" autofocus>
                                                        @error('long')
                                                        <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="col-md-2 btn btn-primary pull-right">
                                            <i class="ft ft-edit"></i> {{__('admin.edit')}}
                                        </button>
                                        <a href="{{ url('/branches')}}" class="col-md-1 btn btn-dark">
                                            <i class="ft-x"></i> {{__('admin.cancel')}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            @endif
            @if($branches->is_approved == 1)
            <div class="col-md-12">
                <div class="card">
                    <form class="form" action="{{ route('branches.update',['id'=>$branches->id])}}" method="POST"
                          id="my_form_id">
                        @csrf
                        <div class="card-header">
                            <h4 class="card-title"
                                id="from-actions-top-bottom-center">{{__('admin.create')}} {{__('admin.data')}} {{__('admin.Branches')}}</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                            </div>
                        </div>
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('area_id') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.the_area')}}
                                                        <span class="danger">*</span></label>
                                                    <br>
                                                    <select disabled
                                                        class="form-control @error('area_id') is-invalid @enderror"
                                                        id="area_id" required name="area_id">
                                                        <option value="">{{__('admin.select_option')}}</option>
                                                        @foreach($all_areas as $area)
                                                            <option value="{{$area->id}}"
                                                                    @if($branches->area_id == $area->id) selected @endif>{{$area->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('area_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('ar_name_B') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.name')}} (ar) <span
                                                            class="danger">*</span></label>
                                                    <input disabled type="text" id="edit_name_ar"
                                                           value="{{ $branches_ar->name }}"
                                                           class="form-control @error('ar_name_B') is-invalid @enderror"
                                                           name="ar_name_B" autocomplete="ar_name_B" autofocus>
                                                    @error('ar_name_B')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('en_name_B') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.name')}} (en) <span
                                                            class="danger">*</span></label>
                                                    <input disabled type="text" id="edit_name_en"
                                                           value="{{ $branches_en->name }}"
                                                           class="form-control @error('en_name_B') is-invalid @enderror"
                                                           name="en_name_B" autocomplete="en_name_B">
                                                    @error('en_name_B')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('ar_address') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.address')}} (ar) <span
                                                            class="danger">*</span></label>
                                                    <input disabled type="text" id="edit_address_ar"
                                                           value="{{ $branches_ar->address }}"
                                                           class="form-control @error('ar_address') is-invalid @enderror"
                                                           name="ar_address" autocomplete="ar_address" autofocus>
                                                    @error('ar_address')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('en_address') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.address')}} (en) <span
                                                            class="danger">*</span></label>
                                                    <input disabled type="text" id="edit_address_en"
                                                           value="{{ $branches_en->address }}"
                                                           class="form-control @error('en_address') is-invalid @enderror"
                                                           name="en_address" autocomplete="en_address" autofocus>
                                                    @error('en_address')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group col-12 mb-2">
                                                <label>{{__('admin.status')}}</label>
                                                <div class="input-group">
                                                    <div class="row icheck_minimal skin">
                                                        <fieldset style="margin-left: 20px; margin-right: 20px">
                                                            <input type="radio" name="status" id="yes1" required value="1" >
                                                            <label style="color: #002581"
                                                                   for="yes1">{{__('admin.active')}}</label>
                                                        </fieldset>
                                                        <fieldset style="margin-left: 20px; margin-right: 20px">
                                                            <input type="radio" name="status"
                                                                   class="custom-control-input" required value="0" id="no1" >
                                                            <label style="color: #FFC107;"
                                                                   for="no1">{{__('admin.attitude')}}</label>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.phone')}}<span
                                                            class="danger">*</span></label>
                                                    <input disabled type="number" id="edit_phone" value="{{ $branches->phone }}"
                                                           class="form-control @error('phone') is-invalid @enderror"
                                                           name="phone" autocomplete="phone" autofocus>
                                                    @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('ar_sections') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.sections')}} (ar) <span
                                                            class="danger">*</span></label>
                                                    <input disabled type="text" id="edit_sections_ar"
                                                           value="{{ $branches_ar->sections }}"
                                                           class="form-control @error('ar_sections') is-invalid @enderror"
                                                           name="ar_sections" autocomplete="ar_sections" autofocus>
                                                    @error('ar_sections')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('en_sections') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.sections')}} (en) <span
                                                            class="danger">*</span></label>
                                                    <input disabled type="text" id="edit_sections_en"
                                                           value="{{ $branches_en->sections }}"
                                                           class="form-control @error('en_sections') is-invalid @enderror"
                                                           name="en_sections" autocomplete="en_sections" autofocus>
                                                    @error('en_sections')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.email')}}<span
                                                            class="danger">*</span></label>
                                                    <input disabled type="text" id="edit_email" value="{{ $branches->email }}"
                                                           class="form-control @error('email') is-invalid @enderror"
                                                           name="email" autocomplete="email" autofocus>
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div
                                                    class="form-group {{ $errors->has('provider_id') ? ' has-error' : '' }}">
                                                    <label for="projectinput2">{{__('admin.provider')}}
                                                        <span class="danger">*</span></label>
                                                    <br>
                                                    <select
                                                        class="form-control @error('provider_id') is-invalid @enderror"
                                                        id="provider_id" required name="provider_id">
                                                        <option value="">{{__('admin.select_option')}}</option>
                                                        @foreach($all_provider as $provider)
                                                            <option value="{{$provider->id}}"
                                                                    @if($branches->provider_id == $provider->id) selected @endif>{{$provider->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('provider_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="form-group col-6 {{ $errors->has('lat') ? ' has-error' : '' }}">
                                                        <label for="projectinput2">{{__('admin.lat')}}<span
                                                                class="danger">*</span></label>
                                                        <input  type="text" id="edit_lat" value="{{ $branches->lat }}"
                                                                class="form-control @error('lat') is-invalid @enderror"
                                                                name="lat" autocomplete="lat" autofocus>
                                                        @error('lat')
                                                        <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-6 {{ $errors->has('long') ? ' has-error' : '' }}">
                                                        <label for="projectinput2">{{__('admin.long')}}<span
                                                                class="danger">*</span></label>
                                                        <input  type="text" id="edit_long" value="{{ $branches->long }}"
                                                                class="form-control @error('long') is-invalid @enderror"
                                                                name="long" autocomplete="long" autofocus>
                                                        @error('long')
                                                        <span class="invalid-feedback" role="alert">
                                                                  <strong>{{ $message }}</strong>
                                                                </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="col-md-2 btn btn-primary pull-right">
                                            <i class="ft ft-edit"></i> {{__('admin.edit')}}
                                        </button>
                                        <a href="{{ url('/branches')}}" class="col-md-1 btn btn-dark">
                                            <i class="ft-x"></i> {{__('admin.cancel')}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
                @endif
        </div>
        </section>
        <!-- Form actions layout section end -->
    </div>
    </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            @if(count($errors)>0)
            @if(old('status')== 1)
            $('#yes1').iCheck('check');
            @elseif(old('status')== 0)
            $('#no1').iCheck('check');
            @endif

            document.getElementById('edit_name_ar').value = "{{old('ar_name_B')}}";
            document.getElementById('edit_name_en').value = "{{old('en_name_B')}}";
            document.getElementById('edit_address_ar').value = "{{old('ar_address')}}";
            document.getElementById('edit_address_en').value = "{{old('en_address')}}";
            document.getElementById('edit_sections_ar').value = "{{old('ar_sections')}}";
            document.getElementById('edit_sections_en').value = "{{old('en_sections')}}";
            document.getElementById('edit_email').value = "{{old('email')}}";
            document.getElementById('edit_phone').value = "{{old('phone')}}";
            document.getElementById('lat').value = "{{old('lat')}}";
            document.getElementById('long').value = "{{old('long')}}";
            @else
            @if($branches->status== 1)
            $('#yes1').iCheck('check');
            @elseif($branches->status== 0)
            $('#no1').iCheck('check');
            @endif
            document.getElementById('edit_name_ar').value = "{{$branches_ar->name}}";
            document.getElementById('edit_name_en').value = "{{$branches_en->name}}";
            document.getElementById('edit_address_ar').value = "{{$branches_ar->address}}";
            document.getElementById('edit_address_en').value = "{{$branches_en->address}}";
            document.getElementById('edit_sections_ar').value = "{{$branches_ar->sections}}";
            document.getElementById('edit_sections_en').value = "{{$branches_en->sections}}";
            document.getElementById('edit_email').value = "{{$branches->email}}";
            document.getElementById('edit_phone').value = "{{$branches->phone}}";
            document.getElementById('lat').value = "{{old('lat')}}";
            document.getElementById('long').value = "{{old('long')}}";
            @endif
        });
        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        /*end  message save*/
    </script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/checkbox-radio.js')}}"></script>
@endsection
