<script>
    /*start code show details edit ajax*/
    $(document).on('click', '.show-detail', async function () {
        let attention_id = $(this).attr('id');
        deposit_row = $(this).parent().parent();
        let url = "{{url('notify_management/notification_movement/show')}}" + "/" + attention_id;

        try {
            let data = await responseEditOrShowData(url);
            if (data.type === "private"){
                $('#user_name').parent().attr('hidden', false);
            }
            $('#show-image').attr('src', '{{asset('/storage/notification_movements/')}}' + '/' + data.image);
            $('#title').text(data.title);
            $('#content').text(data.content);
            $('#branch_td').text(data.branch);
            $('#user_name').text(data.user_name);
            $('#td_type').text(data.type);
            $('#sending_date').text(data.sending_date);
            $('#td_note').text(data.note);
            $('#created').text(data.created_at);
            $('#updated_at').html(data.updated_at);
            $('#confirm-modal-loading-show').modal('hide');
            $('#confirmModalShow').modal('show');


        } catch (error) {
            return error;
        }
    });
    /*end code show details edit ajax*/
</script>
