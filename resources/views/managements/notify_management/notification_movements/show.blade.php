{{-- start model show modal--}}
<div id="confirmModalShow" class="modal fade text-left" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="">
                <div class="card-content">
                    <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                            style=" @if( app()->getLocale() == 'ar')  margin-right: 472px; @else margin-left: 480px @endif"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin: 6px">&times;</span>
                    </button>
                    <div class="form-details card-body p-0">
                        <div class="text-center">
                            <div class="align-items-center" style="min-height: 100px;overflow: hidden;display: flex; text-align: center;">
                                <img src="" class="img-fluid rounded" alt="Card image cap" id="show-image"
                                     onerror="this.src='{{asset('storage/logo.png')}}'"
                                     style="margin: 0 auto;"
                                >
                            </div>
                        </div>

                        <div class="row p-1">
                            <div class="col-sm-12">
                                <h2>
                                    <span id="title" style="color: #0679f0"></span>
                                </h2>
                                <hr>
                                <p id="content"></p>
                                <hr>
                            </div>
                            <div class="col-md-12 p-0 m-0">
                                <table width="100%" class="table  mb-0" id="offer_details_table">
                                    <tbody>
                                    <tr hidden>
                                        <th>{{__('admin.branch')}}</th>
                                        <td id="branch_td"></td>
                                    </tr>
                                    <tr hidden>
                                        <th>{{__('admin.user_name')}}</th>
                                        <td id="user_name"></td>
                                    </tr>
                                    <tr>
                                        <th>{{__('admin.type')}}</th>
                                        <td id="td_type"></td>
                                    </tr>
                                    <tr hidden>
                                        <th>{{__('admin.sending_date')}}</th>
                                        <td id="sending_date"></td>
                                    </tr>
                                    <tr>
                                        <th>{{__('admin.note')}}</th>
                                        <td id="td_note"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr class="col-md-12 p0 m0">
                            <div class="col-sm-9">
                                <small class="price category-color">{{__('admin.created_at')}}:</small>
                                <small class="price category-color" id="created"></small>
                                <br>
                                <small class="price category-color">{{__('admin.updated_at')}}:</small>
                                <small class="price category-color" id="updated_at"></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model show modal--}}
