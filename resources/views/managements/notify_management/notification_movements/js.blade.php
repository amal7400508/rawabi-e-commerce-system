<script>
    let table_show = $('#table-show');
    let table_count = $('#table-count');

    function delete_img() {
        document.getElementById('edit_image_ar').value = null;
        $('#avatar').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
    }

    function loadAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                var image = document.getElementById('avatar');
                image.src = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    /*start code open add modal*/
    $(document).on('click', '#openAddModal', function () {
        edit_row = $(this).parent().parent();
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#form span strong, #span-check, #span-check span').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);
        delete_img();
        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('notification_movement.store')}}")
            .attr("data_type", "add");

        $('#type').val('public');
        $('#phone').parent().parent().attr('hidden', true)
    });
    /*start code open add modal*/

    $(document).on('change', '#type', function () {
        let phone_input = $('#phone').parent().parent();
        if ($(this).val() === 'public') {
            return phone_input.attr('hidden', true);
        }

        return phone_input.attr('hidden', false);
    });


    /*start code edit*/
    let notification_movement_id = 0;
    let edit_row;
    /*end code edit*/

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('notification_movement_id', notification_movement_id);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                /*let response_data = JSON.parse((data.responseText)).category;*/
                let response_data = data.notification_movement;
                $('#addModal').modal('hide');
                let tr_color_red = '';
                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td class='font-default'>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.title + "</td>");
                let col3 = $("<td>" + response_data.message + "</td>");
                let col4 = $("<td>" + response_data.type + "</td>");
                let col5 = $("<td>" + response_data.buttons_index + "</td>");
                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.notification !== undefined) {
                $('#form select[name=notification]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.notification);
            }
            if (obj.type !== undefined) {
                $('#form select[name=type]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.type);
            }
            if (obj.phone !== undefined) {
                $('#form input[name=phone]').addClass('is-invalid')
                    .parent().parent().find('span strong').text(obj.phone);
                $('#span-check').empty();
            }
            if (obj.image !== undefined) {
                $('#form input[name=image]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.image);
            }
        }
    });
    /*end code add or update*/

    /*this code check phone exit in database*/
    $(document).on('click', '#check', function () {
        $.ajax({
            url: "{{route('notification_movement.check_phone')}}",
            method: "post",
            data: {
                _token: $('input[name=_token]').val(),
                phone: $('input[name=phone]').val(),
            },
            dataType: 'JSON',
            beforeSend: function () {
                $('#check').html('<i class="la la-spinner spinner"></i> {{__('admin.check')}}');
                $('#span-check').text("{{__('admin.phone_verification_is_in_progress').'...'}}" + "<br>");
                $('#form input[name=phone]').removeClass('is-invalid').parent().parent().find('span strong').empty();
            },
            success: function (data) {
                $('#check').text('{{__('admin.check')}}');
                $('#span-check').html(data + "<br>");
            },
            error: function () {
                $('#check').html('{{__('admin.check')}}');
                $('#span-check').html('{{__('admin.loading_failed').'!'}} <i class="ft-alert-triangle color-red"></i>' + "<br>");
            }
        });

    });


</script>
