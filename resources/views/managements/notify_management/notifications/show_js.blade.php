<script>
    /*start code show details edit ajax*/
    $(document).on('click', '.show-detail', async function () {
        let attention_id = $(this).attr('id');
        deposit_row = $(this).parent().parent();
        let url = "{{url('notify_management/notifications/show')}}" + "/" + attention_id;

        try {
            let data = await responseEditOrShowData(url);

            $('#td_title').text(data.title);
            $('#td_content').text(data.content);
            $('#created').text(data.created_at);
            $('#updated_at').html(data.updated_at);
            $('#confirm-modal-loading-show').modal('hide');
            $('#confirmModalShow').modal('show');

        } catch (error) {
            return error;
        }
    });
    /*end code show details edit ajax*/
</script>
