<script>
    let table_show = $('#table-show');
    let table_count = $('#table-count');

    @can('create notifications')
    /*start code open add modal*/
    $(document).on('click', '#openAddModal', function () {
        edit_row = $(this).parent().parent();
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);
        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('notifications.store')}}")
            .attr("data_type", "add");
    });
    /*start code open add modal*/
    @endcan

    @can('update notifications')
    /*start code edit*/
    let notfication_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        notfication_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('notify_management/notifications/edit')}}" + '/' + notfication_id;
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('notify_management/notifications/update')}}" + '/' + notfication_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");
            document.getElementById('title_ar').value = data.title_ar;
            document.getElementById('title_en').value = data.title_en;
            document.getElementById('content_ar').value = data.content_ar;
            document.getElementById('content_en').value = data.content_en;
            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/
    @endcan

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('notfication_id', notfication_id);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                let response_data = data.notification;
                $('#addModal').modal('hide');
                let tr_color_red = '';
                let content = response_data.notification_message == null ? '' : response_data.notification_message;

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td class='font-default'>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.notification_title + "</td>");
                let col3 = $("<td>" + content + "</td>");
                let col4 = $("<td>" + response_data.buttons_index + "</td>");

                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.title_ar !== undefined) {
                $('#form input[name=title_ar]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.title_ar);
            }
            if (obj.title_en !== undefined) {
                $('#form input[name=title_en]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.title_en);
            }
            if (obj.content_ar !== undefined) {
                $('#form textarea[name=content_ar]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.content_ar);
            }
            if (obj.content_en !== undefined) {
                $('#form textarea[name=content_en]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.content_en);
            }
        }
    });
    /*end code add or update*/

    @can('delete notifications')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('notify_management/notifications/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            this_row.remove();
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan
</script>
