@php($page_title = __('admin.notifications'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.notifications')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.notifications')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create notifications')
                                            <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.notification')}}</a>
                                        @endcan
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('notifications')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number"
                                                                            @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="title"
                                                                            @if($search_type == 'title') selected @endif>{{__('admin.title')}}</option>
                                                                    <option value="content"
                                                                            @if($search_type == 'content') selected @endif>{{__('admin.message')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.title')}}</th>
                                                        <th>{{__('admin.message')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $notification)
                                                        <tr style="{{$notification->background_color_row}}">
                                                            <td hidden>{{$notification->updated_at}}</td>
                                                            <td>{{$notification->id}}</td>
                                                            <td>{{$notification->notification_title ?? null}}</td>
                                                            <td>{{$notification->notification_message ?? null}}</td>
                                                            <td>{!! $notification->buttons_index !!}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    {{--model show modal--}}
    @include('managements.notify_management.notifications.show')

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.notification')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>

                            <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                                    style=" @if( app()->getLocale() == 'ar')  margin-right: 472px; @else margin-left: 480px @endif"
                                    data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="margin: 6px">&times;</span>
                            </button>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="title_ar">{{__('admin.title')}} (ar)<span class="danger">*</span></label>
                                                <input type="text" id="title_ar" class="form-control" name="title_ar" required>
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="title_en">{{__('admin.title')}} (en)<span class="danger">*</span></label>
                                                <input type="text" id="title_en" class="form-control" name="title_en" required>
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="content_ar">{{__('admin.content')}} (ar)<span class="danger">*</span></label>
                                                <textarea required id="content_ar" name="content_ar" rows="4" class="form-control"></textarea>
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="content_en">{{__('admin.content')}} (en)<span class="danger">*</span></label>
                                                <textarea required id="content_en" name="content_en" rows="4" class="form-control">
                                                    </textarea>
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

@endsection
@section('script')
    @include('managements.notify_management.notifications.show_js')
    @include('managements.notify_management.notifications.js')
@endsection
