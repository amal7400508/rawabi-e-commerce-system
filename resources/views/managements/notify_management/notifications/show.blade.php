{{-- start model show modal--}}
<div id="confirmModalShow" class="modal fade text-left" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="">
                <div class="card-content">

                    <div class="modal-header" style="padding-bottom: 0px;">
                        <h5 class="card-title">{{__('admin.show')}} {{__('admin.notification')}}</h5>
                        <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                                style=" @if( app()->getLocale() == 'ar')  margin-right: 472px; @else margin-left: 480px @endif"
                                data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="margin: 6px">&times;</span>
                        </button>
                    </div>
                    <div class="form-details card-body p-0">
                        <div class="row p-1">
                            <div class="col-md-12 p-0 m-0">
                                <table width="100%" class="table  mb-0" id="offer_details_table">
                                    <tbody>
                                    <tr>
                                        <th>{{__('admin.title')}}</th>
                                        <td id="td_title"></td>
                                    </tr>
                                    <tr>
                                        <th>{{__('admin.message')}}</th>
                                        <td id="td_content"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr class="col-md-12 p0 m0">
                            <div class="col-sm-9">
                                <small class="price category-color">{{__('admin.created_at')}}:</small>
                                <small class="price category-color" id="created"></small>
                                <br>
                                <small class="price category-color">{{__('admin.updated_at')}}:</small>
                                <small class="price category-color" id="updated_at"></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model show modal--}}
