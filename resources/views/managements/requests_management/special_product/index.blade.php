@php($page_title = __('admin.Special_requests'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.Special_requests')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item active">{{__('admin.Special_requests')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                {{-- <div class="btn-group float-md-right">
                    <a href="{{ url('/'.'special_product/recycle_bin')}}" class="btn btn-primary" style="color: white">{{__('admin.recycle_bin')}}  <i class="ft-trash position-right"></i></a>                </div> --}}
            </div>
        </div>
        <div id="messageSave1" class="modal fade text-left" role="dialog">
            <div class="modal-dialog">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="alert bg-info alert-icon-left mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                            <strong>{{__('admin.successfully_done')}}!</strong>
                            <p id="save-success">{{session('success')}}.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{__('admin.post_index').' '.__('admin.Special_requests')}}</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload" id="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <table width="100%"
                               class="table table-white-space display no-wrap icheck table-middle"
                               id="user_table">
                            <thead>
                            <tr>
                                <th hidden>{{__('admin.updated_at')}}</th>
                                <th>{{__('admin.id')}}</th>
                                <th>{{__('admin.user_name')}}</th>
                                <th>{{__('admin.branch_name')}}</th>
                                <th>{{__('admin.phone')}}</th>
                                <th>{{__('admin.special_name')}}</th>
                                <th>{{__('admin.date_1')}} {{__('admin.request')}} </th>
                                <th>{{__('admin.price')}}</th>
                                <th>{{__('admin.currency')}}</th>
                                <th>{{__('admin.status')}}</th>
                                <th>{{__('admin.action')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                </div>
            </div>
        </div>
    </div>

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

    {{-- start model show  Message--}}
    <div id="confirmModalShow" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="card-content">
                    <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                            style=" @if( app()->getLocale() == 'ar')  margin-right: 472px; @else margin-left: 480px @endif"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin: 6px">&times;</span>
                    </button>
                    <div class="form-details card-body p-0">
                        <span id="show_status"></span>
                        <div class="align-items-center">
                            <img src="" class="img-fluid rounded" alt="Card image cap" id="user-image"
                                 onerror="this.src='{{asset('storage/thumbnail/640/defualt_offer.png')}}'">
                        </div>
                        <div class="row p-1">
                            <div class="col-sm-12" style="padding-bottom: 10px">
                                <h2 class="user-name product-title"></h2>
                                <span id="id" class="btn btn-sm btn-outline-primary float-right"
                                      style="margin-left: 2px; margin-right: 2px" title="{{__('admin.id')}}"></span>
                                <a style="font-size: 16px">
                                    <div class="price-reviews">
                                        <span class="price-box float-left" id="name"
                                              title="{{__('admin.date_1')}}"></span>
                                        <span id="date" title="{{__('admin.date_1')}} {{__('admin.request')}}"
                                              class="float-right btn btn-sm btn-outline-info float-right"
                                              style="margin-left: 2px; margin-right: 2px"></span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-12 p-0 m-0">
                                <table class="table mb-0">
                                    <tr>
                                        <th>{{__('admin.price')}}</th>
                                        <td id="price"></td>
                                    </tr>
                                    <tr>
                                        <th>{{__('admin.detail')}}</th>
                                        <td id="desc"></td>
                                    </tr>
                                    <tr>
                                        <th>{{__('admin.note')}}</th>
                                        <td id="note"></td>
                                    </tr>
                                </table>
                            </div>

                            <hr class="col-md-12 p0 m0">
                            <div class="col-md-12">
                                <small class="price category-color">{{__('admin.created_at')}}:</small>
                                <small class="price category-color" id="created"></small>
                                <br>
                                <small class="price category-color">{{__('admin.updated_at')}}:</small>
                                <small class="price category-color" id="updated_at"></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show  Message--}}

    {{-- start model Don Delete Message--}}
    <div id="messageDonDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                        <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                        <strong>{{__('admin.successfully_done')}}!</strong>
                        <p>{{__('admin.deleted_successfully')}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--end model Don Delete Message--}}

    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center"
                        style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark" data-dismiss="modal">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- start model createStatus  Message--}}
    <div id="specialProductModel" class="modal fade text-left" role="dialog">
        <div class="modal-dialog" style="max-width: 50%;">
            <div class="modal-content">
                <div class="">
                    <div class="card-content">
                        <div class="card-header" style="padding-bottom: 0;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                            </button>
                            <h3>{{__('admin.Special_requests')}}</h3>
                        </div>
                        <hr>
                        <form class="form form-horizontal"
                              action="{{ route('special_product.update',['id'=>1])}}"
                              method="POST" enctype="multipart/form-data" id="my_form_id">
                            @csrf
                            <input type="number" id="product_id_value" name="product_id" value="" hidden>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="col-md-3 label-control">{{__('admin.status')}} <span
                                            class="danger">*</span></label>
                                    <div class="col-md-7 mx-auto">
                                        <div class="row  skin">
                                            <fieldset style="margin-left: 20px; margin-right: 20px">
                                                <input type="radio" name="status" id="input-radio-15" required
                                                       value="1">
                                                <label for="input-radio-15"
                                                       style="color: #002581">{{__('admin.has_been_approved')}}</label>
                                            </fieldset>
                                            <fieldset style="margin-left: 20px; margin-right: 20px">
                                                <input type="radio" name="status" id="input-radio-16" required
                                                       value="0">
                                                <label for="input-radio-16"
                                                       style="color: #FFC107;">{{__('admin.not_approved')}}</label>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mx-auto"></div>
                                </div>
                                <div id="inputs">
                                    <div class="form-group row">
                                        <label>{{__('admin.currency')}}</label>
                                        <select class="form-control" data-typeUnit-order="" required id="currency"
                                                name="currency">
                                            <option value="">{{__('admin.select')}}</option>
                                            <option value="ريال يمني">{{__('admin.yemeni_riyal')}}</option>
                                            <option value="دولار أمريكي">{{__('admin.american_dollar')}}</option>
                                            <option value="ريال سعودي">{{__('admin.saudi_riyal')}}</option>
                                        </select>
                                        <span class="error-message">
                                            <strong></strong>
                                        </span>
                                    </divn value="دولار أمريكي">{{__('admin.american_dollar')}}</option>
                                            <option value="ريال سعودي">{{__('admin.saudi_riyal')}}</option>
                                        </select>
                                        <span class=branchor-message">
                                            <strong></strong>
                                        </span>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control">{{__('admin.price')}} <span
                                                class="danger">*</span></label>
                                        <div class="col-md-7 mx-auto">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">RY</span>
                                                </div>
                                                <input type="number" class="form-control" id="edit_price" name="price">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div>
                                            <span style="color: red; font-size: 11px">
                                            <strong id="data-erorr"></strong>
                                        </span>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                    <div class="form-group row">
                                        <label
                                            class="col-md-3 label-control">{{__('admin.notification_message')}}</label>
                                        <div class="col-md-7 mx-auto">
                                            <textarea rows="4" class="form-control" name="admin_message"></textarea>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="col-md-12 btn btn-primary" id="save-button">
                                            <i class="fa-save"></i> {{__('admin.save')}}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model createStatus Message--}}
@endsection
@section('script')
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    <!-- BEGIN: Page JS-->
    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script>
        /*Reload the data and display it in the datatable*/
        @include('include.reload-datatable')

        /*start code show all data in datatable ajax*/
        $(document).ready(function () {
            $('#user_table').DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{  route('special_product') }}",
                },
                columns: [
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                    },
                    {
                        data: 'id',
                        name: 'id',
                    },
                    {
                        data: 'user_name',
                        name: 'user_name',
                    },
                    {
                        data: 'branch_name',
                        name: 'branch_name',
                    },
                    {
                        data: 'phone',
                        name: 'phone',
                    },
                    {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'date',
                        name: 'date',
                    },
                    {
                        data: 'price',
                        name: 'price',
                    },
                    {
                        data: 'currency',
                        name: 'currency',
                        render: function (data, type, full, meta) {
                            if (full['price'] == null) {
                                return null
                            } else {
                                return data
                            }
                        }
                    },

                    {
                        data: 'status', name: 'status',
                        render: function (data, type, full, meta) {
                            if (data == 1) {
                                return "<span class='btn btn-sm btn-outline-success'>{{__('admin.completedRequest')}}</span>"
                            } else if (data == 0) {
                                return "<button id='" + full['id'] + "' class='specialProduct btn btn-sm btn-outline-danger'>{{__('admin.waiting')}}</button>"
                            } else if (data == 2) {
                                return "<button id='" + full['id'] + "' class='specialProduct btn btn-sm btn-outline-black'>{{__('admin.canceled')}}</button>"
                            } else if (data == 3) {
                                return "<span class='btn btn-sm btn-outline-blue'>{{__('admin.deferred')}}</span>"
                            } else if (data == 4) {
                                return "<button id='" + full['id'] + "' data-status='4' class='btn btn-sm btn-outline-warning'>{{__('admin.requested')}}</button>"
                            } else {
                                return "<span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>"
                            }
                        },
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    },
                ],
                'order': [[6, 'asc'], [1, 'desc']],
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });
        });
        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        /*end  message save*/
        $('input[name=status]').change(function () {
            /* if ($(this).val() === 1) {
                 $('#edit_price').attr('disabled', false)
             } else {
                 $('#edit_price').attr('disabled', true)
             }*/
            checkStatus($(this).val());
        });
        /*start code Delete ajax*/
        var $carrier_id;
        $(document).on('click', '.delete', function () {
            $carrier_id = $(this).attr('id');
            $('#confirmModalDelete').modal('show');
        });
        $('#ok_button').click(function () {
            $.ajax({
                url: "special_product/destroy/" + $carrier_id,
                beforeSend: function () {
                    $('#ok_button').text('{{__('admin.deleting')}}...');
                },
                success: function (data) {
                    setTimeout(function () {
                        $('#confirmModalDelete').modal('hide');
                        $('#user_table').DataTable().ajax.reload();
                        $('#ok_button').text('{{__('admin.yes')}}');
                    }, 500);
                    $('#messageDonDelete').modal('hide');
                    setTimeout(function () {
                        $('#messageDonDelete').modal('show');
                    }, 510,);
                    setTimeout(function () {
                        $('#messageDonDelete').modal('hide');
                    }, 3000,);
                    $('#confirmModalShow').modal('hide');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });

        /*end code Delete ajax*/

        function checkStatus(status) {
            if (status == 1) {
                inputs =
                    ' <div class="form-group row">' +
                    '<label class="col-md-3 label-control">{{__('admin.currency')}}<span' +
                    '            class="danger">*</span></label>' +
                    '<div class="col-md-7 mx-auto">' +
                    '<select class="form-control " data-typeUnit-order="" required id="currency" name="currency">' +
                    '<option value="">{{__('admin.select')}}</option>' +
                    '<option value="ريال يمني">{{__('admin.yemeni_riyal')}}</option>' +
                    '<option value="دولار أمريكي">{{__('admin.american_dollar')}}</option>' +
                    '<option value="ريال سعودي">{{__('admin.saudi_riyal')}}</option>' +
                    '</select>' +
                    '<span class="error-message">' +
                    '<strong></strong>' +
                    '</span>' +
                    '</div>' +
                    '    <div class="col-md-2 mx-auto"></div>' +
                    '</div>' +
                    ' <div class="form-group row">' +
                    '    <label class="col-md-3 label-control">{{__('admin.price')}} <span' +
                    '            class="danger">*</span></label>' +
                    '    <div class="col-md-7 mx-auto">' +
                    '        <div class="input-group">' +
                    '            <div class="input-group-prepend">' +
                    '                <span id="currency_span" class="input-group-text"></span>' +
                    '            </div>' +
                    '            <input type="number" class="form-control" id="edit_price" name="price">' +
                    '            <div class="input-group-append">' +
                    '                <span class="input-group-text">.00</span>' +
                    '            </div>' +
                    '        </div>' +
                    '        <span style="color: red; font-size: 11px">' +
                    '        <strong id="data-erorr"></strong>' +
                    '    </span>' +
                    '    </div>' +
                    '    <div class="col-md-2 mx-auto"></div>' +
                    '</div>' +
                    '<div class="form-group row">' +
                    '    <label class="col-md-3 label-control">{{__('admin.note')}}</label>' +
                    '    <div class="col-md-7 mx-auto">' +
                    '        <textarea rows="4" class="form-control" name="admin_message"></textarea>' +
                    '    </div>' +
                    '    <div class="col-md-2 mx-auto"></div>' +
                    '</div>';
            } else {
                inputs =
                    '<div class="form-group row">' +
                    '    <label class="col-md-3 label-control">{{__('admin.notification_message')}}</label>' +
                    '    <div class="col-md-7 mx-auto">' +
                    '        <textarea rows="4" class="form-control" name="admin_message"></textarea>' +
                    '    </div>' +
                    '    <div class="col-md-2 mx-auto"></div>' +
                    '</div>';
            }
            $('#inputs').html(inputs);
        }

        $(document).on('change', '#currency', function () {
            let data = $(this).val();
            if (data === 'ريال يمني') {
                return $('#currency_span').text('REY');
            } else if (data === 'دولار أمريكي') {
                return $('#currency_span').text('$');
            } else if (data === 'ربال سعودي') {
                return $('#currency_span').text('SAR');
            }

        });
        var urlByPost;
        @can('update special_product')
        /*start code edit*/
        var frm = $('#my_form_id');
        var create_id;
        $(document).on('click', '.specialProduct', function () {
            $('#data-erorr').text('');
            $('#edit_price').removeClass('is-invalid');
            create_id = $(this).attr('id');
            detail_url = "{{ url('/requests_management/special_product')}}/edit/" + create_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $('#button_save').html("<i class='ft-edit'></i> {{__('admin.save')}}");
                    checkStatus(data.status);
                    if (data.status == 1) {
                        document.getElementById('edit_price').value = data.price;
                        $('#input-radio-15').iCheck('check')
                    } else {
                        $('#input-radio-16').iCheck('check');
                    }
                    urlByPost = "{{ url('/requests_management/special_product/update')}}" + "/" + data.id;
                    frm.attr('action', '{{ url('/requests_management/special_product/update')}}' + '/' + data.id);
                    frm.attr('method', 'POST');
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#specialProductModel').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code edit*/
        @endcan

        /*start  SaveRequest ajax*/
        $('#save-button').click(function () {

            $.ajax({
                type: 'post',
                url: urlByPost,
                dataType: "json",
                data: {
                    status: $("input[name=status]:checked").val(),
                    currency: $("select[name=currency] :selected").val(),
                    price: $("input[name=price]").val(),
                    admin_message: $("textarea[name=admin_message]").val(),
                    _token: $("input[name=_token]").val()
                },
                beforeSend: function () {
                    $('#my_form_id input, #my_form_id select').removeClass('is-invalid');
                    $('#my_form_id span strong').empty();
                    $('#save-button').text('{{__('admin.saving')}}...');
                },
                success: function (data) {
                    $('#data-erorr').text('');
                    $('#edit_price').removeClass('is-invalid');
                    $('#currency').removeClass('is-invalid');
                    setTimeout(function () {
                        $('#specialProductModel').modal('hide');
                        $('#save-success').text(data.success + '.');
                    }, 500);
                    $('#messageSave1').modal('hide');
                    setTimeout(function () {
                        $('#messageSave1').modal('show');
                    }, 510,);
                    setTimeout(function () {
                        $('#messageSave1').modal('hide');
                    }, 3000,);
                    $('#messageSave1').modal('hide');
                    setTimeout(function () {
                        $('#save-button').text('{{__('admin.save')}}');
                    }, 400,);
                    $('#user_table').DataTable().ajax.reload();
                    window.checkNotify();
                },
                error: function (error) {
                    if (error.status !== 401) {
                        $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                        $('#confirm-modal-loading-show').modal('show');
                    }

                    $('#save-button').text('{{__('admin.save')}}');
                    let obj = JSON.parse((error.responseText)).error;
                    if (obj.currency !== undefined) {
                        console.log(obj.currency);
                        $('#my_form_id select[name=currency]').addClass('is-invalid')
                            .parent().find('span strong').text(obj.currency);
                    }
                    if (obj.price !== undefined) {
                        console.log(obj.price);
                        $('#my_form_id input[name=price]').addClass('is-invalid')
                            .parent().parent().find('span strong').text(obj.price);
                    }

                    /*$('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
*/
                }
            })
        });
        /*end  SaveRequest ajax*/

        /*start code show details ajax*/
        var carrier_detals_id;
        $(document).on('click', '.showB', function () {
            carrier_detals_id = $(this).attr('id');
            detail_url = "{{ url('/requests_management/special_product')}}" + "/show/" + carrier_detals_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    if (data.status == 1) {
                        $('#show_status').html('<span class="btn btn-sm btn-primary position-absolute round" style=" margin: 10px; " >{{__("admin.has_been_approved")}}</span>');
                    } else {
                        $('#show_status').html('<span class="btn btn-sm btn-danger position-absolute round" style=" margin: 10px; " >{{__("admin.not_approved")}}</span>');
                    }
                    if (data.image == "") {
                        $('#user-image').attr('src', '{{asset('/storage/no-image.jpg')}}');

                    } else {
                        $('#user-image').attr('src', data.image);
                    }
                    $('.form-details .user-name').text(data.user_name);
                    $('#name').text(data.name);
                    $('.imag').text(data.name);
                    $('#id').text(data.id);
                    $('#date').text(data.date);
                    $(' #price').text(data.price);
                    $(' #desc').text(data.desc);
                    $(' #note').text(data.note);
                    $('#created').text(data.created_at);
                    $('#updated_at').html(data.updated_at);
                    $(' .btn_dele').html(data.btn);
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');

                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });

    </script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
@endsection
