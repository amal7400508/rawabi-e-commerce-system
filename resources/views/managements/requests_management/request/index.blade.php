@php($page_title = __('admin.requests'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
    <style>
        .modal-backdrop {
            opacity: 0 !important;
        }

        .modal-backdrop:last-of-type {
            opacity: 0.5 !important;
        }

        .modal-content {
            -webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, .16), 0 3px 6px rgba(0, 0, 0, .23);
            box-shadow: 0 3px 6px rgba(0, 0, 0, .16), 0 3px 6px rgba(0, 0, 0, .23);
        }
    </style>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.requests')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.requests').' '.__('admin.from_delivery')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div id="messageSave1" class="modal fade text-left" role="dialog">
            <div class="modal-dialog">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="alert bg-info alert-icon-left mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                            <strong>{{__('admin.successfully_done')}}!</strong>
                            <p id="succss-message">{{session('success')}}.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p id="succss-message">{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{__('admin.post_index')}} {{__('admin.requests')}}</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload" id="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-head">
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <div class="row">
                            <div class="col-sm-12 col-md-3">
                                <label>{{__('admin.show')}} </label>
                                @php($table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10)
                                <select style="width: 100px" name="table_length" aria-controls="user_table"
                                        class="custom-select custom-select-sm form-control form-control-sm">
                                    {{--                                    @php($filter_name_parameter = isset($_GET['filter_name']) ? '?filter_name='.$_GET['filter_name'].'&' : '?')--}}
                                    @php($filter_name_parameter = url()->current() == url()->full()?url()->current().'?': url()->full(). '&')
                                    <option value="{{ $filter_name_parameter }}table_length=10"
                                            @if($table_length == 10) selected @endif>10
                                    </option>
                                    <option value="{{ $filter_name_parameter }}table_length=25"
                                            @if($table_length == 25) selected @endif>25
                                    </option>
                                    <option value="{{ $filter_name_parameter }}table_length=50"
                                            @if($table_length == 50) selected @endif>50
                                    </option>
                                    <option value="{{ $filter_name_parameter }}table_length=100"
                                            @if($table_length == 100) selected @endif>100
                                    </option>
                                </select>
                            </div>
                            <div class="col-sm-12 col-md-9">
                                <form style="display: flex;justify-content: start;">
                                    @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                    <div class="col-sm-12 col-md-4">
                                        <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                            <option value="number"
                                                    @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                            <option value="name"
                                                    @if($search_type == 'name') selected @endif>{{__('admin.name')}}</option>
                                            <option value="phone"
                                                    @if($search_type == 'phone') selected @endif>{{__('admin.phone')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <input type="search" class="form-control form-control-sm"
                                               placeholder="{{__('admin.search')}}"
                                               name="query"
                                               aria-controls="user_table"
                                               value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <table width="100%" class="table  dataTable" id="user_table">
                            <thead>
                            <tr>
                                <th hidden>{{__('admin.updated_at')}}</th>
                                <th>{{__('admin.id')}}</th>
                                <th>{{__('admin.name')}}</th>
                                <th>{{__('admin.branch')}}</th>
                                <th>{{__('admin.phone')}}</th>
                                <th>{{__('admin.request')}}</th>
                                <th>{{__('admin.cache_payment')}}</th>
                                <th>{{__('admin.payment_type')}}</th>
                                <th>{{__('admin.booking_date')}}</th>
                                <th width="120">{{__('admin.status')}}</th>

                                <th>{{__('admin.action')}}</th>
                                <th>{{__('admin.time')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($tracking_requests ) == 0)
                                <tr>
                                    <td colspan="9" class="text-center">
                                        {{__('admin.no_data')}}
                                        <hr>
                                    </td>
                                </tr>
                            @endif
                            @foreach($tracking_requests as $tracking_request)
                                <tr style="{{$tracking_request->background_color_row}}">
                                    <td hidden>{{$tracking_request->updated_at}}</td>
                                    <td>{{$tracking_request->request_number}}</td>
                                    <td>{{$tracking_request->cart->user->name}}</td>
                                    <td>{{$tracking_request->branch->name}}</td>
                                    <td>{{$tracking_request->cart->user->phone}}</td>
                                    <td>{{$tracking_request->user_request_number}}</td>
                                    <td>
                                        {{$tracking_request->cache_payment}}
                                    </td>
                                    <td>
                                        @if ($tracking_request->payment_type == 'cash')
                                            <i class='la la-money'></i> {{__('admin.cash')}}
                                        @elseif ($tracking_request->cache_payment == 'fromWallet')
                                            {{__('admin.from_wallet')}}
                                        @else
                                            <span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>
                                        @endif
                                    </td>
                                    <td>{{$tracking_request->booking_date}}</td>
                                    <td>
                                        @php($status = $tracking_request->status)

                                        @if ($status == 'requested')
                                            <button id='{{$tracking_request->id}}' class='editRequestClick btn btn-sm btn-danger' title='{{__("admin.click_here_to_approve_the_request")}}'>{{__('admin.requested')}}</button>
                                        @else
                                            <span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>
                                        @endif
                                    </td>


                                    <td widtd="110">
                                        <a class="showB"  id="{{$tracking_request->id}}"><span style="font-size:12px;color: #0092e2"
                                                                                              title="{{__('admin.click_here_to_view_details')}}">{{__('admin.detail')}}...</span></a>
                                    </td>
                                    <td><small>{{$tracking_request->created_at->diffForHumans()}}</small></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                    {{ $tracking_requests->appends($pagination_links)->links() }}
                    {{--{{ $tracking_requests->links() }}--}}

                    <span>{{__('admin.show').' '. $tracking_requests->count() .' '.__('admin.out_of').' '. $tracking_request_count . ' '. __('admin.record')}}</span>
                </div>
            </div>
        </div>
    </div>


    {{-- start model editRequestModel  Message--}}
    <div id="editRequestModel" class="modal fade text-left" role="dialog">
        <div class="modal-dialog" style="max-width: 50%;">
            <div class="modal-content">
                <div class="">
                    <div class="card-content">
                        <div class="card-header" style="padding-bottom: 0;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                            </button>
                            <h3>{{__('admin.Assign_request_to_branch')}}</h3>
                        </div>
                        <hr>
                        <div id="error-cancel"></div>
                        <form class="form form-horizontal" action="{{route('request.update',['id'=>52])}}" method="POST"
                              enctype="multipart/form-data" id="editRequestForm">
                            @csrf
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="col-md-3 label-control">{{__('admin.request')}}</label>
                                    <div class="col-md-7 mx-auto">
                                        <div class="row skin radio-input-group">
                                            <fieldset style="margin-left: 20px; margin-right: 20px">
                                                <input type="radio" name="available" id="input-radio-15"
                                                       class="rabio-bttuon-big" required value="1">
                                                <label for="input-radio-15" style="color: #0679f0;">{{__('admin.agree_request')}}</label>
                                            </fieldset>
                                            <fieldset style="margin-left: 20px; margin-right: 20px">
                                                <input type="radio" name="available" id="input-radio-16"
                                                       class="rabio-bttuon-big" required value="2">
                                                <label for="input-radio-16" style="color: #FFC107;">{{__('admin.not_agree_request')}}</label>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mx-auto"></div>
                                </div>
                                {{--                                <div class="form-group row">--}}
                                {{--                                    <label class="col-md-3 label-control">{{__('admin.Branch')}}</label>--}}
                                {{--                                    <div class="col-md-7 mx-auto">--}}
                                {{--                                        <select class="form-control" id="branch" name="branch_id" required>--}}
                                {{--                                            <option value="">{{__('admin.select_option')}}</option>--}}
                                {{--                                            @foreach($branch as $branchs)--}}
                                {{--                                                <option value="{{$branchs->id}}">{{$branchs->name}}</option>--}}
                                {{--                                            @endforeach--}}
                                {{--                                        </select>--}}
                                {{--                                        <span id="message-error-branch" class="error-massege"></span>--}}
                                {{--                                        <span id="message-error-branch" class="invalid-feedback" role="alert"></span>--}}
                                {{--                                    </div>--}}
                                {{--                                    <div class="col-md-2 mx-auto"></div>--}}
                                {{--                                </div>--}}
                                <div id="inputs-form">
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control">{{__('admin.carrier_status')}}</label>
                                        <div class="col-md-7 mx-auto">
                                            <select class="form-control" id="carrier_status" name="carrier_status">
                                                <option value="">{{__('admin.all')}}</option>
                                                <option value="on_the_way">{{__('admin.on_way_back')}}</option>
                                                <option value="on_branch">{{__('admin.in_the_branch')}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control">{{__('admin.delivery_driver')}}</label>
                                        <div class="col-md-7 mx-auto">
                                            <select class="form-control" id="carrier_id" name="carrier_id">
                                                <option value="">{{__('admin.select_option')}}</option>
                                                @foreach($carriers as $carrier)
                                                    <option value="{{$carrier->id}}">{{$carrier->name}}</option>
                                                @endforeach
                                            </select>
                                            <span id="message-error-carrier" class="error-massege"></span>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" id="save-request" class="col-md-12 btn btn-primary">
                                            <i class="fa-save"></i> {{__('admin.save')}}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                    0
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model editRequestModel Message--}}

    {{--    show details this request--}}
    @include('managements.requests_management.request.show_details')

    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center" style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark cancel_button">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- start model show offer Message--}}
    @include('managements.app.offers.show')
    {{-- end model show offer Message--}}
    @include('managements.app.product.show')

    @include('include.message-don-delete')

    @include('managements.requests_management.request.cart.modal-add')

    @include('include.modal_loading_show')
@endsection
@section('script')
    {{--    @include('managements.requests_management.request.cart.js')--}}
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>

    <script>
        $('input[name=available]').change(function () {
            var div_inputs = $('#inputs-form');
            div_inputs.empty();
            if ($(this).val() == '1') {
                buildInputs();
            } else {
                div_inputs.append('' +
                    ' <div class="form-group row">' +
                    '     <label class="col-md-3 label-control">{{__('admin.cancel_text')}}</label>' +
                    '     <div class="col-md-7 mx-auto">' +
                    '      <textarea class="form-control" rows="5" name="cancel_text"></textarea>' +
                    '     </div>' +
                    '     <div class="col-md-2 mx-auto"></div>' +
                    ' </div>'
                );
            }
        });

        function buildInputs() {
            $('#inputs-form').append('' +
                ' <div class="form-group row">' +
                '         <label class="col-md-3 label-control">{{__('admin.carrier_status')}}</label>' +
                '         <div class="col-md-7 mx-auto">' +
                '             <select class="form-control" id="carrier_status" name="carrier_status">' +
                '                 <option value="">{{__('admin.all')}}</option>' +
                '                 <option value="on_the_way">{{__('admin.on_way_back')}}</option>' +
                '                 <option value="on_branch">{{__('admin.in_the_branch')}}</option>' +
                '             </select>' +
                '         </div>' +
                '         <div class="col-md-2 mx-auto"></div>' +
                '     </div>' +
                '     <div class="form-group row">' +
                '     <label class="col-md-3 label-control">{{__('admin.delivery_driver')}}</label>' +
                '     <div class="col-md-7 mx-auto">' +
                '         <select class="form-control" id="carrier_id" name="carrier_id">' +
                '             <option value="" >{{__('admin.select_option')}}</option>' +
                '             @foreach($carriers as $carrier)' +
                '                 <option value="{{$carrier->id}}">{{$carrier->name}}</option>' +
                '             @endforeach' +
                '         </select>' +
                '         <span id="message-error-carrier" class="error-massege"></span>' +
                '     </div>' +
                '     <div class="col-md-2 mx-auto"></div>' +
                ' </div>'
            );
        }
        @can('update request')
        /*start editRequest save*/
        let row;
        let urlRequestPost;
        $(document).on('click', '.editRequestClick', function () {
            row = $(this).parent().parent();
            let available = $('input[name=available]:checked').val();
            if (available != 1) {
                $('#branch').attr('disabled', true);
                $('#carrier_id').attr('disabled', true);
                $('#carrier_status').attr('disabled', true);
            }
            edit_id = $(this).attr('id');
            detail_url = "{{url('requests_management/request')}}/edit/" + edit_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $('#error-cancel').text('');
                    $('#branch').removeClass('is-invalid');
                    $('#carrier_id').removeClass('is-invalid');

                    $('#message-error-branch').html('');
                    $('#message-error-carrier').html('');

                    $('#save-request').text('{{__('admin.save')}}').attr('disabled', false);

                    /*document.getElementById('carrier_status').value = '';*/
                    if (data.is_active === 1) {
                        document.getElementById('input-radio-15').checked = true;
                        document.getElementById('input-radio-16').checked = false;
                        $('#inputs-form').empty();
                        buildInputs();
                        filtter();
                    } else {
                        document.getElementById('input-radio-16').checked = true;
                        document.getElementById('input-radio-15').checked = false;
                    }
                    $('input[name=available]').attr('checked', "{{url('requests_management/request/update')}}" + "/" + data.id);
                    $('#editRequestForm').attr('action', "{{url('requests_management/request/update')}}" + "/" + data.id);
                    urlRequestPost = "{{url('requests_management/request/update')}}" + "/" + data.id;
                    document.getElementById('branch').value = data.branch_id;
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#editRequestModel').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end  editRequest save*/
        @endcan

        /*start  SaveRequest ajax*/
        $('#save-request').click(function () {
            var available = $("input[name=available]:checked").val();
            var branch_id = $("select[name=branch_id] :selected").val();
            var carrier_id = $("select[name=carrier_id] :selected").val();
            var cancel_text = $("textarea[name=cancel_text]").val();
            var _token = $("input[name=_token]").val();
            $.ajax({
                type: 'post',
                url: urlRequestPost,
                dataType: "json",
                data: {
                    available: available,
                    branch_id: branch_id,
                    carrier_id: carrier_id,
                    cancel_text: cancel_text,
                    _token: _token
                },
                beforeSend: function () {
                    $('#save-request').text('{{__('admin.saving')}}...').attr('disabled', true);
                },
                success: function (data) {
                    $('#error-cancel').text('');
                    $('#save-request').text('{{__('admin.save')}}').attr('disabled', false);
                    if (data.success === undefined) {
                        $('#error-cancel').html(
                            '<div class="text-center" style="margin-bottom:30px">' +
                            '<span class="alert alert-danger">' +
                            '<strong>' + data._error + '</strong>' +
                            '</span>' +
                            '</div>'
                        )
                    } else {
                        setTimeout(function () {
                            $('#editRequestModel').modal('hide');
                            row.remove();
                            $('#succss-message').text(data.success + '.');
                        }, 500);
                        $('#messageSave1').modal('hide');
                        setTimeout(function () {
                            $('#messageSave1').modal('show');
                        }, 510,);
                        setTimeout(function () {
                            $('#messageSave1').modal('hide');
                        }, 3000,);
                        $('#messageSave1').modal('hide');
                        window.checkNotify();
                    }
                },
                error: function (data) {
                    var obj = JSON.parse(data.responseText);

                    obj.error.branch_id === undefined ? $('#branch').removeClass('is-invalid') : $('#branch').addClass('is-invalid');
                    obj.error.carrier_id === undefined ? $('#carrier_id').removeClass('is-invalid') : $('#carrier_id').addClass('is-invalid');

                    obj.error.branch_id = obj.error.branch_id === undefined ? '' : obj.error.branch_id;
                    obj.error.carrier_id = obj.error.branch_id === undefined ? '' : obj.error.carrier_id;

                    $('#message-error-branch').html('<strong>' + obj.error.branch_id + '</strong>');
                    $('#message-error-carrier').html('<strong>' + obj.error.carrier_id + '</strong>');

                    $('#save-request').text('{{__('admin.save')}}').attr('disabled', false);
                }
            });
            window.checkNotify();
        });

        /*end SaveRequest ajax*/
        function filtter() {
            /*start code filter carrier ajax*/
            $('#carrier_status').change(function () {
                var status_carrier = $(this).val();
                var id_branch = 1;
                if (id_branch) {
                    $.ajax({
                        type: "GET",
                        url: "{{url('/requests_management/request/falterStatus')}}" + "/" + status_carrier,
                        success: function (data) {
                            if (data) {
                                $("#carrier_id").empty();
                                $("#carrier_id").append('<option value=""> {{__("admin.select_option")}}  </option>');
                                $.each(data, function (key, value) {
                                    $("#carrier_id").append('<option value="' + value.id + '">' + value.name + ' ' + value.cache_payment + '</option>');
                                });
                            } else {
                                $("#carrier_id").empty();
                            }
                        }
                    });
                } else {
                    $("#carrier_id").empty();
                }
            });
            /*end code filter carrier ajax*/
        }

        /*start code filter carrier ajax*/
        /*end code filter carrier ajax*/

        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        /*end  message save*/

        /*start code show details ajax*/
        let carrier_detals_id;
        $(document).on('click', '.showB', function () {
            carrier_detals_id = $(this).attr('id');

            showDetails(carrier_detals_id);
        });

        function showDetails(carrier_detals_id) {
            detail_url = "{{url('requests_management/request/fromBranch/')}}" + "/showDetails/" + carrier_detals_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $("#details_tbody").empty();
                    $("#tbody__").empty();
                    var request__type = '';
                    for (var i = 0; i < data.cart_detail.length; i++) {
                        let unit = '';
                        let product_note = '';
                        if (data.unit[i + 1] !== '')
                            unit = ' (<span class="color-blue">' + data.unit[i + 1] + '</span>)';

                        if (data.product_note[i + 1] !== '')
                            product_note = ' (<span class="color-blue">' + data.product_note[i + 1] + '</span>)';

                        $("#details_table").append("<tr>" +
                            "<td>" + data.product_number[i + 1] + "</td>" +
                            "<td><span class='color-blue'> " + data.request_type[i + 1] + "</span>" + " | " + data.product[i + 1] + unit + " | " + product_note + "</td>" +
                            "<td>" + data.cart_detail[i].quantity + "</td>" +
                            "<td>" + data.product_discount_id[i + 1] + "</td>" +
                            "<td>" + data.product_price[i + 1] + "</td>" +
                            "<td>" + data.total_quantity[i + 1] + "</td>" +
                            "<td>" +
                            "<a class='action-item delete-row-from-cart' data-request-id='" + data.request_id + "' data-cart-detail-id='" + data.cart_detail_id[i + 1] + "'><i class='la la-trash color-red'></i></a>" +
                            "</td>" +
                            "</tr>");
                    }
                    $('.hide-in-tracking').attr('hidden', false);

                    $("#details_table").append(
                        "<tr style='background-color: #f1f3ff;'>" +
                        "<td colspan='2'>{{__('admin.total')}}</td>" +
                        "<td>" + data.sum + "</td>" +
                        "<td></td>" +
                        "<td></td>" +
                        "<td>" + data.price_sum + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td colspan='6'></td>" +
                        "</tr>"
                    );

                    if (data.insurance_company_amount !== 0) {
                        $("#details_table").append(
                            "<tr>" +
                            "<td colspan='5'>{{__('admin.insurance_company_amount')}}</td>" +
                            "<td>" + data.insurance_company_amount + ' ' + data.currency + "</td>" +
                            "</tr>"
                        );
                    }
                    if (data.coupon !== null) {
                        let coupon_value_type = (data.coupon_value_type === 'percentage') ? ' % ' : ' YER ';
                        $("#details_table").append(
                            "<tr>" +
                            "<td colspan='5'>{{__('admin.coupon_reduction')}}</td>" +
                            "<td>" + data.coupon + coupon_value_type + "</td>" +
                            "</tr>"
                        );
                    }
                    if (data.discount_value !== null) {
                        let discount_value_type = (data.discount_value_type === 'percentage') ? ' % ' : ' YER ';
                        $("#details_table").append(
                            "<tr>" +
                            "<td colspan='5'>{{__('admin.volume_discount')}}</td>" +
                            "<td>" + data.discount_value + discount_value_type + "</td>" +
                            "</tr>"
                        );
                    }

                    $("#details_table").append(
                        "<tr>" +
                        "<td colspan='5'>{{__('admin.remaining_amount')}}</td>" +
                        "<td style='background-color: #f4f9da;'>" + data.remaining_amount + "</td>" +
                        "</tr>"
                    );

                    $("#details_table").append(
                        "<tr>" +
                        "<td colspan='5'>{{__('admin.carrier_price')}}</td>" +
                        "<td style='background-color: #f4f9da;'>" + data.carrier_price + "</td>" +
                        "</tr>"
                    );

                    var carrier = data.carrier === null ? '' : data.carrier;
                    $("#tbody__").append(
                        "<tr>" +
                        "<th colspan='2'>{{__('admin.address')}}:</td>" +
                        "<td style='color: dodgerblue'>" + data.address + " - " + data.desc + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<th colspan='2'>{{__('admin.the_name')}} {{__('admin.delivery_driver')}}:</td>" +
                        "<td style='color: dodgerblue'>" + carrier + "</td>" +
                        "</tr>"
                    );

                    $('#user-name').text(data.user_name);
                    $('#request_no').text(data.request_no);
                    $('#note').text(data.note);

                    if (data.payment_type == "fromWallet") {
                        $('#payment_method').text("{{__('admin.from_wallet')}}");
                    } else if (data.payment_type === "cash") {
                        $('#payment_method').text("{{__('admin.cash')}}");
                    } else {
                        $('#payment_method').text("error");
                    }

                    $('#the_amount_to_be_paid').text(data.cache_payment);
                    $('#branch').text(data.branch);
                    $('#created').text(data.created_at + ' | ' + data.created_at_diffForHumans);

                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');

                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        }

        /*delete row from cart*/
        let request_no;
        let cart_detail_id;
        $(document).on('click', '.delete-row-from-cart', function () {
            request_no = $(this).attr('data-request-id');
            cart_detail_id = $(this).attr('data-cart-detail-id');
            $('#confirmModalDelete').modal(
                {
                    backdrop: 'static',
                    keyboard: true
                }
            );
            /*alert('هل انت متأكد من انك تريد حذف هذا السطر من السلة؟');
        */
        });

        $('#ok_button').click(function () {
            $.ajax({
                url: "{{url('requests_management/request')}}" + "/cart-delete/" + cart_detail_id,
                type: 'delete',
                data: {
                    _token: '{{@csrf_token()}}',
                    _method: 'delete'
                },
                beforeSend: function () {
                    $('#ok_button').text('{{__('admin.deleting')}}...');
                },
                success: function (data) {
                    $('#error-deleting').text("{{__('admin.deleted_successfully')}}.");
                    $('#title-error').text("{{__('admin.successfully_done')}}!");
                    setTimeout(function () {
                        $('#confirmModalDelete').modal('hide');
                        $('#ok_button').text('{{__('admin.yes')}}');
                        /*refresh*/
                        showDetails(request_no);
                    }, 500);
                    setTimeout(function () {
                        $('#messageDonDelete').modal('show');
                    }, 510,);

                    setTimeout(function () {
                        $('#messageDonDelete').modal('hide');
                    }, 3000,);
                },
                error: function (data) {
                    let message_error = data.responseJSON.error;
                    if (message_error !== undefined) {
                        $('#error-deleting').text(message_error);
                        $('#title-error').text("{{__('admin.error_message')}}!");
                        $('#messageDonDelete').modal('show');
                        setTimeout(function () {
                            $('#ok_button').text('{{__('admin.yes')}}');
                        }, 200,);
                        setTimeout(function () {
                            $('#messageDonDelete').modal('hide');
                        }, 3000,);
                    } else {
                        $('#ok_button').text('{{__('admin.yes')}}');
                        $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                        $('#confirm-modal-loading-show').modal('show');
                    }
                }
            })
        });


        $('.cancel_button').click(function () {
            $('#confirmModalDelete').modal('hide');
        });


        /*start code Delete ajax*/
        var $product_id;
        $(document).on('click', '.deleteB', function () {
            $product_id = $(this).attr('id');
            $('#confirmModalDelete').modal('show');
        });
        /*end code Delete ajax*/
    </script>
    @include('managements.requests_management.request.cart.js')
    @include('managements.app.offers.show_js')
    @include('managements.app.product.js_show')


    <!-- BEGIN: Page JS-->
    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/checkbox-radio.js')}}"></script>
@endsection
