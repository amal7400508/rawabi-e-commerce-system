{{-- start modal add for cart--}}
<div id="modalAddForCart" class="modal fade text-left" role="dialog">
    <div class="modal-dialog long-mode">
        <div class="modal-content">
            <div class="card-content">
                <div class="modal-header danger">
                    <h5 class="modal-title" id="myModalLabel1">
                        <i class="ft-shopping-cart" style="margin: 0 8px;"></i> {{__('admin.cart')}}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div
                            class="form-group mb-1 col-sm-12 col-md-6 {{ $errors->has('categories[]') ? ' has-error' : '' }}">
                            <label for="pass">{{__('admin.branch')}} <span
                                    class="danger">*</span></label>
                            <br>
                            <select class="form-control all-categories" data-category-order="0" required
                                    name="categories">
                                <option value="">{{__('admin.select_option')}}</option>
                                @foreach($branch as $branches)
                                    <option value="{{$branches->id}}">{{$branches->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div
                            class="form-group mb-1 col-sm-12 col-md-6 {{ $errors->has('product_id[]') ? ' has-error' : '' }}">
                            <label for="pass">{{__('admin.product')}} <span
                                    class="danger">*</span></label>
                            <br>
                            <select class="form-control product-0" id="select-product" required
                                    name="product_id">
                            </select>

                        </div>
                    </div>
                    <br><br>
                    <div id="loader-price" style="text-align: center"></div>
                    <div id="product_price_div" hidden>
                        <table width="100%" class="table table-striped table-responsive table-custom-responsive">
                            <thead style="background-color: #f6f6f6">
                            <tr>
                                <th>{{__('admin.product').' ('. __('admin.unit') .')'}}</th>
                                <th>{{__('admin.price')}}</th>
                                <th>{{--<i class="la la-cart-plus color-blue"></i>--}}</th>
                            </tr>
                            </thead>
                            <tbody id="product_price_tbody">
                            </tbody>
                        </table>
                    </div>


                </div>
                <hr class="p0 m0">
                <br>
            </div>
        </div>
    </div>
</div>
{{-- end  modal add for cart--}}

<div id="modalQuantityProduct" class="modal text-left" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" style="padding: 10px">
            <form id="form-add-to-cart">
                @csrf
                <label for="pass">{{__('admin.quantity')}} <span class="danger">*</span></label>
                <input type="number" class="form-control" required name="quantity">
                <br>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary" id="submit_cart"><i class="la la-cart-plus"></i> {{__('admin.add_to_cart')}}</button>
                </div>
                <br>
            </form>
        </div>
    </div>
</div>
