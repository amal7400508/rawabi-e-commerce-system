<script>
    $(document).on('click', '#add-for-cart', function () {
        $('#modalAddForCart').find("select").val("");
        $("#product_price_div").attr('hidden', true);
        $('#modalAddForCart').modal('show');
    });

    let products_price_id;
    $(document).on('click', '.add-for-card', function () {
        products_price_id = $(this).attr('id');
        $('input[name=quantity]').val("");
        $('#modalQuantityProduct').modal('show');
    });

    /*ajax falter categories start*/
    $(document).on('change', '.all-categories', function () {
        var data_number = $(this).attr('data-category-order');
        var category_id = $(this).val();
        category_url = "{{url('app/offers/falter-product-request')}}" + "/" + category_id;
        if (category_id) {
            $.ajax({
                type: "GET",
                url: category_url,
                success: function (data) {
                    if (data) {
                        $('.product-' + data_number).empty();
                        $('.product-' + data_number).append('<option value=""> {{__("admin.select_option")}}  </option>');
                        $.each(data, function (key, value) {
                            $(".product-" + data_number).append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    } else {
                        $('.product-' + data_number).empty();
                    }
                }
            });
        } else {
            $('.product-' + data_number).empty();
        }
    });
    /*ajax falter categories end*/

    /*ajax falter products start*/
    $(document).on('change', '#select-product', function () {
        let product_id = $(this).val();
        let url = "{{url('requests_management/request/product-price')}}" + "/" + product_id;
        if (product_id) {
            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function(){
                    $("#loader-price").html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $("#product_price_div").attr('hidden', true);
                },
                success: function (data) {
                    $("#loader-price").empty();
                    $("#product_price_div").attr('hidden', false);
                    $("#product_price_tbody").empty();
                    for (var i = 0; i < data.data.length; i++) {
                        $("#product_price_tbody").append("<tr>" +
                            "<td>" + data.product_name + ' (<span class="color-blue">' + data.data[i].unitName + " | " + data.data[i].note_ar + '</span>) ' + "</td>" +
                            "<td>" + data.data[i].price + "</td>" +
                            "<td>" +
                            "<a class='action-item add-for-card' id='" + data.data[i].id + "'><i class='la la-cart-plus color-blue'></i></a>" +
                            "</td>" +
                            "</tr>");
                    }

                },
                error: function () {
                    $("#loader-price").html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                }
            });
        } else {

        }
    });
    /*ajax falter products end*/


    $('#form-add-to-cart').on('submit', function (event) {
        route = "{{ route('request.add-to-cart')}}";
        var formData = new FormData(this);
        formData.append('request_id', carrier_detals_id);
        formData.append('products_price_id', products_price_id);
        event.preventDefault();
        saveAndUpdate(formData, route);
    });

    function saveAndUpdate(data, route) {
        $.ajax({
            url: route,
            method: "POST",
            data: data,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#submit_cart').html('<i class="la la-cart-plus"></i> {{__('admin.add_to_cart')}} <i class="la la-spinner spinner"></i>')
            },
            success: function (data) {
                $('#submit_cart').html('<i class="la la-cart-plus"></i> {{__('admin.add_to_cart')}} ');
                $('#modalQuantityProduct').modal('hide');
                $('#modalAddForCart').modal('hide');
                showDetails(carrier_detals_id);
            },
            error: function (data) {
                $('#submit_cart').html('<i class="la la-cart-plus"></i> {{__('admin.add_to_cart')}}');
            }

        });
    }

</script>
