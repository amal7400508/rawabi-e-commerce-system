@php($page_title = __('admin.report'))
@php($route = route('request.export'))
@extends('include.report')
@section('content')
    <div class="row">
        <div class="col-12">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-center" style="padding-bottom: 0px;">
                                <h4 class="card-title">{{__('admin.requests_report')}}
                                    <span class="color-blue"></span>
                                </h4>
                                <hr>
                                <h4 class="card-title">{{__('admin.from')}} {{__('admin.date_1')}} <span class="font-default color-blue">{{request()->from ?? "_________"}}</span> {{__('admin.to')}} {{__('admin.date_1')}} <span class="font-default color-blue">{{request()->to ?? \Carbon\Carbon::now()->toDateString()}}</span></h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table width="100%" class="table  zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>{{__('admin.id')}}</th>
                                                <th>{{__('admin.user_name')}}</th>
                                                <th>{{__('admin.phone')}}</th>
                                                <th>{{__('admin.request')}}</th>
                                                <th>{{__('admin.cache_payment')}}</th>
                                                <th>{{__('admin.payment_type')}}</th>
                                                <th width="110">{{__('admin.status')}}</th>
                                                <th>{{__('admin.action')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($data ) == 0)
                                                <tr>
                                                    <td colspan="9" class="text-center">
                                                        {{__('admin.no_data')}}
                                                        <hr>
                                                    </td>
                                                </tr>
                                            @endif
                                            @foreach($data as $request)
                                                <tr style="{{$request->background_color_row}}">
                                                    <td hidden>{{$request->updated_at}}</td>
                                                    <td>{{$request->request_number}}</td>
                                                    <td>{{$request->cart->user->full_name}}</td>
                                                    <td>{{$request->cart->user->phone}}</td>
                                                    <td>{{$request->user_request_number}}</td>
                                                    <td>{{$request->cache_payment}}</td>
                                                    <td>
                                                        @if($request->payment_type == 'cash')
                                                            <i class='la la-money'></i> {{__('admin.cash')}}
                                                        @elseif ($request->payment_type == 'fromWallet')
                                                            {{__('admin.from_wallet')}}
                                                        @else
                                                            <span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @php($status = $request->status)

                                                        @if ($status == 'requested')
                                                            <span class='btn btn-sm btn-danger'>{{__('admin.requested')}}</span>
                                                        @elseif ($status == 'repair')
                                                            <a><span class='btn btn-sm btn-warning'>{{__('admin.repair')}}</span></a>
                                                        @elseif ($status == 'deliver')
                                                            <a><span class='btn-sm btn-primary'>{{__('admin.deliver')}}</span></a>
                                                        @elseif ($status == 'delivered')
                                                            <span class='btn btn-sm btn-success'>{{__('admin.delivered')}}</span>
                                                        @elseif ($status == 'canceled')
                                                            <span class='btn btn-sm btn-dark'>{{__('admin.canceled')}}</span>
                                                        @elseif ($status == 'reviewed')
                                                            <a><span class='btn btn-sm btn-cyan'>{{__('admin.reviewed')}}</span></a>
                                                        @elseif ($status == 'received')
                                                            <a><span class='btn btn-sm btn-cyan'>{{__('admin.received')}}</span></a>
                                                        @elseif ($status == 'on_the_way')
                                                            <span class='btn btn-sm btn-success'>{{__('admin.delivered')}}</span>
                                                        @elseif ($status == 'on_branch')
                                                            <span class='btn btn-sm btn-success'>{{__('admin.delivered')}}</span>
                                                        @elseif ($status == 'completedRequest')
                                                            <span class='btn btn-sm btn-primary'>{{__('admin.completedRequest')}}</span>
                                                        @elseif ($status === 'done')
                                                            <span class='btn btn-sm btn-success'>{{__('admin.deliverySuccessful')}}</span>
                                                        @else
                                                            <span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a class="showB" id="{{$request->id}}"><span style="font-size:12px;color: #0092e2" title="{{__('admin.click_here_to_view_details')}}">{{__('admin.detail')}}...</span></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    {{ $data->appends($pagination_links)->links() }}

                                    <span>
                                        {{__('admin.show')}}
                                        <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                        {{__('admin.out_of')}}
                                        <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                        {{__('admin.record')}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
