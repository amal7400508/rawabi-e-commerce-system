<script>
    /*start code show details ajax*/
    var carrier_detals_id;
    $(document).on('click', '.showB', function () {
        carrier_detals_id = $(this).attr('id');
        detail_url = "{{url('/request/fromBranch/')}}" + "/showDetails/" + carrier_detals_id;
        $.ajax({
            type: 'GET',
            url: detail_url,
            dataType: "json",
            beforeSend: function () {
                $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                $('#confirm-modal-loading-show').modal('show');
            },
            success: function (data) {
                $("#details_tbody").empty();
                $("#tbody__").empty();
                var request__type = '';
                for (var i = 0; i < data.cart_detail.length; i++) {
                    let unit = '';
                    if (data.unit[i + 1] === '')
                        unit = ' (<span class="color-blue">' + data.unit[i + 1] + '</span>)';

                    $("#details_table").append("<tr>" +
                        "<td>" + data.product_number[i + 1] + "</td>" +
                        "<td><span class='color-blue'> " + data.request_type[i + 1] + "</span>" + " | " + data.product[i + 1] + unit + "</td>" +
                        "<td>" + data.cart_detail[i].quantity + "</td>" +
                        "<td>" + data.product_price[i + 1] + "</td>" +
                        "<td style='position: relative'>" +
                        + data.product_price[i + 1] +
                        "</td>" +
                        "<td>" +
                        "<a class='action-item'><i class='la la-edit color-blue'></i></a>" +
                        "<a class='action-item'><i class='la la-trash color-red'></i></a>" +
                        "</td>" +
                        "</tr>");
                }
                $("#details_table").append(
                    "<tr style='background-color: #f1f3ff;'>" +
                    "<td colspan='3'>{{__('admin.total')}}</td>" +
                    "<td>" + data.sum + "</td>" +
                    "<td></td>" +
                    "<td>" + data.price_sum + "</td>" +
                    "</tr>" +
                    "<tr><td colspan='6'></td></tr>"
                );

                if (data.coupon !== null) {
                    $("#details_table").append(
                        "<tr>" +
                        "<td colspan='5'>{{__('admin.coupon_reduction')}}</td>" +
                        "<td>" + data.coupon + "</td>" +
                        "</tr>"
                    );
                }

                $("#details_table").append(
                    "<tr>" +
                    "<td colspan='5' >{{__('admin.remaining_amount')}}</td>" +
                    "<td style='background-color: #f1f3ff;'>" + data.remaining_amount + "</td>" +
                    "</tr>"
                );
                if (data.receiving_type === 'delivery') {
                    var carrier = data.carrier === null ? '' : data.carrier;
                    $("#tbody__").append(
                        "<tr>" +
                        "<th colspan='2'>{{__('admin.address')}}:</td>" +
                        "<td style='color: dodgerblue'>" + data.address + " - " + data.desc + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<th colspan='2'>{{__('admin.the_name')}} {{__('admin.delivery_driver')}}:</td>" +
                        "<td style='color: dodgerblue'>" + carrier + "</td>" +
                        "</tr>"
                    );
                }
                var cancel_note = data.cancel_note === null ? '' : data.cancel_note;
                $("#tbody__").append(
                    "<tr>" +
                    "<th colspan='2'>{{__('admin.cancel_note')}}:</td>" +
                    "<td style='color: dodgerblue'>" + cancel_note + "</td>" +
                    "</tr>"
                );
                if (data.hospitality !== null) {
                    $("#tbody__").append(
                        "<tr>" +
                        "<th colspan='2'>{{__('admin.date_1')}} {{__('admin.Hospitality')}}:</td>" +
                        "<td style='color: dodgerblue'>" + data.hospitality + "</td>" +
                        "</tr>"
                    );
                }
                $('#user-name').text(data.user_name);
                $('#request_no').text(data.request_no);
                $('#note').text(data.note);

                if (data.payment_type == "fromWallet") {
                    $('#payment_method').text("{{__('admin.from_wallet')}}");
                } else if (data.payment_type === "cash") {
                    $('#payment_method').text("{{__('admin.cash')}}");
                } else {
                    $('#payment_method').text("error");
                }

                $('#the_amount_to_be_paid').text(data.cache_payment);
                $('#branch').text(data.branch);
                var updated_by = data.updated_by === null ? '{{__('admin.from_delivery')}}' : data.updated_by;
                $('#created').text(data.created_at + ' | ' + data.created_at_diffForHumans + '     |     ' + '{{__('admin.updated_by')}}' + ' : ' + updated_by);

                $('#confirm-modal-loading-show').modal('hide');
                $('#confirmModalShow').modal('show');
                buttonWidget();

            },
            error: function (data) {
                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                $('#confirm-modal-loading-show').modal('show');
            }
        })
    });
    /*end code show details ajax*/
</script>
