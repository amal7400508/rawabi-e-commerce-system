@php($page_title = __('admin.requests_report'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.requests')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            <li class="breadcrumb-item"><a href="{{route('tracking')}}">{{__('admin.requests')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.requests_report')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.create')}} {{__('admin.requests_report')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('request.report')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <form method="get">
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.from')}}</label>
                                                                        <input type="date" class="form-control"
                                                                               value="{{request()->from}}" name="from">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.to')}}</label>
                                                                        <input type="date" class="form-control"
                                                                               value="{{request()->to}}" name="to">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.name')}}</label>
                                                                        <input type="text" class="form-control"
                                                                               value="{{request()->name}}" name="name">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.phone')}}</label>
                                                                        <input type="number" class="form-control"
                                                                               value="{{request()->phone}}" name="phone">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.request_no')}}</label>
                                                                        <input type="number" class="form-control"
                                                                               value="{{request()->request_no}}" name="request_no"/>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.payment_type')}}</label>
                                                                        <select class="form-control"
                                                                                name="payment_type">
                                                                            <option
                                                                                value="">{{__('admin.select_option')}}</option>
                                                                            <option
                                                                                value="cash">{{__('admin.cash')}}</option>
                                                                            <option
                                                                                value="fromWallet">{{__('admin.from_wallet')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.request_status')}}</label>
                                                                        <select class="form-control" name="status">
                                                                            <option
                                                                                value="">{{__('admin.select_option')}}</option>
                                                                            <option
                                                                                value="requested">{{__('admin.requested')}}</option>
                                                                            <option
                                                                                value="canceled">{{__('admin.canceled_requests')}}</option>
                                                                            <option
                                                                                value="received">{{__('admin.received')}}</option>
                                                                            <option
                                                                                value="repair">{{__('admin.repair')}}</option>
                                                                            <option
                                                                                value="deliver">{{__('admin.deliver')}}</option>
                                                                            <option
                                                                                value="delivered">{{__('admin.delivered')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.carrier')}}</label>
                                                                        <select class="form-control" name="carrier">
                                                                            <option
                                                                                value="">{{__('admin.select_option')}}</option>
                                                                            @foreach($carriers as $carrier)
                                                                                <option value="{{$carrier->id}}">{{$carrier->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
{{--                                                                <div class="col-md-12">--}}
{{--                                                                    <div class="form-group">--}}
{{--                                                                        <label>{{__('admin.providers')}}</label>--}}
{{--                                                                        <select class="form-control" name="provider" id="provider">--}}
{{--                                                                            <option value="">{{__('admin.select_option')}}</option>--}}
{{--                                                                            @foreach($providers as $provider)--}}
{{--                                                                                <option value="{{$provider->id}}">{{$provider->name}}</option>--}}
{{--                                                                            @endforeach--}}
{{--                                                                        </select>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.branch_name')}}</label>
                                                                        <select class="form-control" name="branch_name" id="branch_name">
                                                                            @foreach($branches as $branche)
                                                                                <option value="{{$branche->id}}">{{$branche->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.gender')}}</label>
                                                                        <select class="form-control" name="gender">
                                                                            <option
                                                                                value="">{{__('admin.select_option')}}</option>
                                                                            <option
                                                                                value="man">{{__('admin.man')}}</option>
                                                                            <option
                                                                                value="woman">{{__('admin.woman')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.quantity')}}</label>
                                                                        <input type="number" name="quantity"
                                                                               value="{{request()->quantity}}" class="form-control">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.categories')}}</label>
                                                                        <select class="form-control all-categories" name="category" id="categories">
                                                                            @foreach($categories as $category)
                                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.products')}}</label>
                                                                        <select
                                                                            class="form-control product product-filter"
                                                                            id="product"
                                                                            name="product_id">
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                {{--<div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.product').' ('. __('admin.unit') .')'}}
                                                                            | {{__('admin.taste')}}</label>
                                                                        <select class="form-control product_price"
                                                                                id="product_price" name="product_price">
                                                                        </select>
                                                                    </div>
                                                                </div>--}}
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.request_type')}}</label>
                                                                        <select class="form-control"
                                                                                name="request_type">
                                                                            <option
                                                                                value="">{{__('admin.select_option')}}</option>
                                                                            <option
                                                                                value="product">{{__('admin.products')}}</option>
                                                                            <option
                                                                                value="offer">{{__('admin.Offers')}}</option>
                                                                            <option
                                                                                value="special">{{__('admin.Special_requests')}}</option>
                                                                            <option
                                                                                value="coupons">{{__('admin.coupons')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.coupon_no')}}</label>
                                                                        <select class="form-control" name="coupon">
                                                                            <option
                                                                                value="">{{__('admin.select_option')}}</option>
                                                                            @foreach($coupons as $coupon)
                                                                                <option
                                                                                    value="{{$coupon->id}}">{{$coupon->number}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions pull-right">
                                                    <button type="submit" formtarget="_blank" formaction="{{route('request.report_export')}}" class=" btn btn-primary" id="button_export"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-printer"></i> {{__('admin.report')}}
                                                    </button>
{{--                                                    <button type="submit" formaction="{{route('request.report')}}" class=" btn btn-primary" id="button_preview"--}}
{{--                                                            style="margin-bottom: 10px; width: 120px">--}}
{{--                                                        <i class="ft-eye"></i> {{__('admin.preview')}}--}}
{{--                                                    </button>--}}
                                                    {{--<button type="submit" formaction="{{'request.export')}}" class=" btn btn-primary" id="button_export"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-printer"></i> {{__('admin.export')}}
                                                    </button>--}}
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.post_index')}} {{__('admin.requests')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('request.report')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                </div>
                                                <table width="100%" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('admin.id')}}</th>
                                                        <th>{{__('admin.user_name')}}</th>
                                                        <th>{{__('admin.phone')}}</th>
                                                        <th>{{__('admin.request')}}</th>
                                                        <th>{{__('admin.cache_payment')}}</th>
                                                        <th>{{__('admin.payment_type')}}</th>
                                                        <th width="110">{{__('admin.status')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data ) == 0)
                                                        <tr>
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $request)
                                                        <tr style="{{$request->background_color_row}}">
                                                            <td hidden>{{$request->updated_at}}</td>
                                                            <td>{{$request->request_number}}</td>
                                                            <td>{{$request->cart->user->full_name}}</td>
                                                            <td>{{$request->cart->user->phone}}</td>
                                                            <td>{{$request->user_request_number}}</td>
                                                            <td>{{$request->cache_payment}}</td>
                                                            <td>
                                                                @if($request->payment_type == 'cash')
                                                                    <i class='la la-money'></i> {{__('admin.cash')}}
                                                                @elseif ($request->payment_type == 'fromWallet')
                                                                    {{__('admin.from_wallet')}}
                                                                @else
                                                                    <span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @php($status = $request->status)

                                                                @if ($status == 'requested')
                                                                    <span class='btn btn-sm btn-danger'>{{__('admin.requested')}}</span>
                                                                @elseif ($status == 'repair')
                                                                    <a><span class='btn btn-sm btn-warning'>{{__('admin.repair')}}</span></a>
                                                                @elseif ($status == 'deliver')
                                                                    <a><span class='btn-sm btn-primary'>{{__('admin.deliver')}}</span></a>
                                                                @elseif ($status == 'delivered')
                                                                    <span class='btn btn-sm btn-success'>{{__('admin.delivered')}}</span>
                                                                @elseif ($status == 'canceled')
                                                                    <span class='btn btn-sm btn-dark'>{{__('admin.canceled')}}</span>
                                                                @elseif ($status == 'reviewed')
                                                                    <a><span class='btn btn-sm btn-cyan'>{{__('admin.reviewed')}}</span></a>
                                                                @elseif ($status == 'received')
                                                                    <a><span class='btn btn-sm btn-cyan'>{{__('admin.received')}}</span></a>
                                                                @elseif ($status == 'on_the_way')
                                                                    <span class='btn btn-sm btn-success'>{{__('admin.delivered')}}</span>
                                                                @elseif ($status == 'on_branch')
                                                                    <span class='btn btn-sm btn-success'>{{__('admin.delivered')}}</span>
                                                                @elseif ($status == 'completedRequest')
                                                                    <span class='btn btn-sm btn-primary'>{{__('admin.completedRequest')}}</span>
                                                                @elseif ($status === 'done')
                                                                    <span class='btn btn-sm btn-success'>{{__('admin.deliverySuccessful')}}</span>
                                                                @else
                                                                    <span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <a class="showB" id="{{$request->id}}"><span style="font-size:12px;color: #0092e2" title="{{__('admin.click_here_to_view_details')}}">{{__('admin.detail')}}...</span></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            {{ $data->appends($pagination_links)->links() }}

                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    @include('managements.requests_management.request.show_details')

    {{-- start model show offer Message--}}
    @include('managements.app.offers.show')
    {{-- end model show offer Message--}}


@endsection
@section('script')
    @include('managements.app.offers.show_js')
    <script>
        $("select[name=payment_type]").val("{{request()->payment_type}}");
        $("select[name=branch_name]").val("{{request()->branch_name}}");

        $("select[name=status]").val("{{request()->status}}");
        $("select[name=coupon]").val("{{request()->coupon}}");
        $("select[name=gender]").val("{{request()->gender}}");
        $("select[name=carrier]").val("{{request()->carrier}}");
        $("select[name=category]").val("{{request()->category}}");
        $("select[name=product_id]").val("{{request()->product_id}}");
        $("select[name=product_price]").val("{{request()->product}}");
        $("select[name=request_type]").val("{{request()->request_type}}");

        $("select[name=product_id]").html("<option value='{{request()->product_id}}'>{{request()->product_id}}</option>");
        $("select[name=product_price]").html("<option value='{{request()->product_price}}'>{{request()->product_price}}</option>");


        /*start code show details ajax*/
        var carrier_detals_id;
        $(document).on('click', '.showB', function () {
            carrier_detals_id = $(this).attr('id');
            detail_url = "{{url('/requests_management/request/fromBranch/')}}" + "/showDetails/" + carrier_detals_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $("#details_tbody").empty();
                    $("#tbody__").empty();
                    var request__type = '';
                    for (var i = 0; i < data.cart_detail.length; i++) {
                        let unit = '';
                        if (data.unit[i + 1] === '')
                            unit = ' (<span class="color-blue">' + data.unit[i + 1] + '</span>)';

                        $("#details_table").append("<tr>" +
                            "<td>" + data.product_number[i + 1] + "</td>" +
                            "<td><span class='color-blue'> " + data.request_type[i + 1] + "</span>" + " | " + data.product[i + 1] + unit + "</td>" +
                            "<td>" + data.cart_detail[i].quantity + "</td>" +
                            "<td>" + data.product_discount_id[i + 1] + "</td>" +
                            "<td>" + data.product_price[i + 1] + "</td>" +
                            "<td>" + data.total_quantity[i + 1] + "</td>" +
                            "</tr>");
                    }
                    $("#details_table").append(
                        "<tr style='background-color: #f1f3ff;'>" +
                        "<td colspan='2'>{{__('admin.total')}}</td>" +
                        "<td>" + data.sum + "</td>" +
                        "<td></td>" +
                        "<td></td>" +
                        "<td>" + data.price_sum + "</td>" +
                        "</tr>" +
                        "<tr><td colspan='7'></td></tr>"
                    );

                    if (data.insurance_company_amount !== 0) {
                        $("#details_table").append(
                            "<tr>" +
                            "<td colspan='5'>{{__('admin.insurance_company_amount')}}</td>" +
                            "<td>" + data.insurance_company_amount + ' ' + data.currency + "</td>" +
                            "</tr>"
                        );
                    }

                    if (data.coupon !== null) {
                        let coupon_value_type = (data.coupon_value_type === 'percentage')? ' % ' : ' YER ';
                        $("#details_table").append(
                            "<tr>" +
                            "<td colspan='5'>{{__('admin.coupon_reduction')}}</td>" +
                            "<td>" + data.coupon + coupon_value_type + "</td>" +
                            "</tr>"
                        );
                    }

                    if (data.discount_value !== null) {
                        let discount_value_type = (data.discount_value_type === 'percentage')? ' % ' : ' YER ';
                        $("#details_table").append(
                            "<tr>" +
                            "<td colspan='5'>{{__('admin.volume_discount')}}</td>" +
                            "<td>" + data.discount_value + discount_value_type + "</td>" +
                            "</tr>"
                        );
                    }
                    $("#details_table").append(
                        "<tr>" +
                        "<td colspan='5' >{{__('admin.remaining_amount')}}</td>" +
                        "<td style='background-color: #f4f9da;'>" + data.remaining_amount + "</td>" +
                        "</tr>"
                    );

                    $("#details_table").append(
                        "<tr>" +
                        "<td colspan='5'>{{__('admin.carrier_price')}}</td>" +
                        "<td style='background-color: #f4f9da;'>" + data.carrier_price + "</td>" +
                        "</tr>"
                    );

                    if (data.receiving_type === 'delivery') {
                        var carrier = data.carrier === null ? '' : data.carrier;
                        $("#tbody__").append(
                            "<tr>" +
                            "<th colspan='2'>{{__('admin.address')}}:</td>" +
                            "<td style='color: dodgerblue'>" + data.address + " - " + data.desc + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<th colspan='2'>{{__('admin.the_name')}} {{__('admin.delivery_driver')}}:</td>" +
                            "<td style='color: dodgerblue'>" + carrier + "</td>" +
                            "</tr>"
                        );
                    }
                    var cancel_note = data.cancel_note === null ? '' : data.cancel_note;
                    $("#tbody__").append(
                        "<tr>" +
                        "<th colspan='2'>{{__('admin.cancel_note')}}:</td>" +
                        "<td style='color: dodgerblue'>" + cancel_note + "</td>" +
                        "</tr>"
                    );

                    $('#user-name').text(data.user_name);
                    $('#request_no').text(data.request_no);
                    $('#note').text(data.note);

                    if (data.payment_type == "fromWallet") {
                        $('#payment_method').text("{{__('admin.from_wallet')}}");
                    } else if (data.payment_type === "cash") {
                        $('#payment_method').text("{{__('admin.cash')}}");
                    } else {
                        $('#payment_method').text("error");
                    }

                    $('#the_amount_to_be_paid').text(data.cache_payment);
                    $('#branch').text(data.branch);
                    var updated_by = data.updated_by === null ? '{{__('admin.from_delivery')}}' : data.updated_by;
                    $('#created').text(data.created_at + ' | ' + data.created_at_diffForHumans + '     |     ' + '{{__('admin.updated_by')}}' + ' : ' + updated_by);

                    /*hidden column action from header*/
                    $('.hide-in-tracking').attr('hidden', true);

                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');
                    buttonWidget();

                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code show details ajax*/

        /*ajax falter product start*/
        $(document).on('change', '.product-filter', function () {
            let product_id = $(this).val();
            let product_url = "{{url('offer/falter-price')}}" + "/" + product_id;
            if (product_id) {
                $.ajax({
                    type: "GET",
                    url: product_url,
                    success: function (data) {
                        data = data.data;
                        if (data) {
                            $('#product_price').empty().append('<option value=""> {{__("admin.select_option")}}  </option>');
                            for (let i = 0; i < data.length; i++) {
                                $("#product_price").append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                            }
                        } else {
                            $('#product_price').empty();
                        }
                    }
                });
            } else {
                $('#product_price').empty();
            }
        });

        /*ajax falter product end*/

        function spanStatus(status) {
            if (status === 'requested') {
                return "<span class='btn btn-sm btn-danger'>{{__('admin.requested')}}</span>"
            } else if (status === 'repair') {
                return "<span class='btn btn-sm btn-warning'>{{__('admin.repair')}}</span>"
            } else if (status === 'deliver') {
                return "<span class='btn btn-sm btn-primary'>{{__('admin.deliver')}}</span>"
            } else if (status === 'delivered') {
                return "<span class='btn btn-sm btn-success'>{{__('admin.delivered')}}</span>"
            } else if (status === 'canceled') {
                return "<span class='btn btn-sm btn-dark'>{{__('admin.canceled')}}</span>"
            } else if (status === 'reviewed') {
                return "<span class='editRequestClick btn btn-sm btn-cyan' title='{{__("admin.click_here_to_change_the_carrier")}}'>{{__('admin.reviewed')}}</span>"
            } else if (status === 'received') {
                return "<span class='editRequestClick btn btn-sm btn-cyan' title='{{__("admin.click_here_to_change_the_carrier")}}'>{{__('admin.received')}}</span>"
            } else if (status === '0') {
                return "<span class='btn btn-sm btn-success'>{{__('admin.delivered')}}</span>"
            } else if (status === 'on_branch') {
                return "<span class='btn btn-sm btn-success'>{{__('admin.delivered')}}</span>"
            } else if (status === 'completedRequest') {
                return "<span class='btn btn-sm btn-primary'>{{__('admin.completedRequest')}}</span>"
            } else if (status === '0') {
                return "<span class='btn btn-sm btn-success'>{{__('admin.deliverySuccessful')}}</span>"
            } else {
                return "<span style='color: #c9c9c9'>{{__('admin.no_data')}}</span>"
            }
        }
    </script>

    <script>
        $(document).on('change', '#provider', function () {
            let provider_id = $(this).val();
            let url = "{{url('app/products/get-branch')}}" + "/" + provider_id;

            let select_branch = $("#branch_name");
            if (provider_id) {
                $.ajax({
                    type: "GET",
                    url: url,
                    beforeSend: function () {
                        select_branch.empty();
                    },
                    success: function (data) {
                        select_branch.html('<option value="">{{__('admin.select_option')}}</option>');
                        $.each(data, function (key, value) {
                            select_branch.append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    },
                });
            }
        });

        /*ajax falter start*/
        $(document).on('change', '#branch_name', function () {
            let category_id = $(this).val();
            let cate = $('#categories');
            let url = "{{url('app/products/get-categories')}}" + "/" + category_id;
            if (category_id) {
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (data) {
                        cate.empty();
                        cate.append('<option value="">{{__("admin.select_option")}}</option>');
                        data.categories.forEach(myFunction);

                        function myFunction(item) {
                            cate.append('<option value="' + item.id + '">' + item.name + '</option>');
                        }
                    }
                });
            } else {
                cate.empty();
            }
        });
        /*ajax falter end*/

        /*ajax falter products start*/
        $(document).on('change', '#categories', function () {
            let product_id = $(this).val();
            let pro = $('#product');
            let url = "{{url('app/products/get-products')}}" + "/" + product_id;
            if (product_id) {
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (data) {
                        pro.empty();
                        pro.append('<option value="">{{__("admin.select_option")}}</option>');
                        data.products.forEach(myFunction);

                        function myFunction(item) {
                            pro.append('<option value="' + item.id + '">' + item.name + '</option>');
                        }
                    }
                });
            } else {
                pro.empty();
            }
        });

        /*ajax falter products end*/
    </script>
@endsection
