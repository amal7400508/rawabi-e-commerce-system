{{-- start model show--}}
<div id="confirmModalShow" class="modal fade text-left" role="dialog">
    <div class="modal-dialog long-mode">
        <div class="modal-content">
            <div class="card-content">
                <div class="modal-header danger">
                    <h5 class="modal-title" id="myModalLabel1">{{__('admin.request_no')}}:
                        <span id="request_no" class="color-blue font-default"></span> &nbsp | &nbsp {{__('admin.user_name')}}:
                        <span id="user-name" class="color-blue"></span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span class="text-bold-600">{{__('admin.post_index')}} {{__('admin.request')}}</span>
                    <div class="hide-in-tracking pull-right">
                        <a class="hide-in-tracking" id="add-for-cart">
                            <i style="font-size: 2rem;" class="la la-cart-plus color-blue"></i>
                        </a>
                    </div>
                    <hr>
                    <table width="100%" class="table table-striped table-responsive table-custom-responsive" id="details_table">
                        <thead id="table-details-head" style="background: linear-gradient(45deg, #0e71c8, #0679f0); color: white">
                        <tr>
                            <th>{{__('admin.id')}}</th>
                            <th>{{__('admin.product').' ('. __('admin.unit') .')'}}</th>
                            <th>{{__('admin.quantity')}}</th>
                            <th>{{__('admin.discount')}}</th>
                            <th>{{__('admin.price')}}</th>
                            <th>{{__('admin.total')}}</th>
                            <th>{{__('admin.action')}}</th>
                        </tr>
                        </thead>
                        <tbody id="details_tbody">
                        </tbody>
                    </table>
                    <br>
                    <span class="text-bold-600">{{__('admin.detail')}}</span>
                    <a class="pull-right" hidden id="status-audit" style="color: #0C84D1">{{__('admin.tracking_request_cases')}}</a>
                    <hr>
                    <table class="table p-0" width="100%" id="table__">
                        <tr>
                            <th width="20%">{{__('admin.branch_name')}}:</th>
                            <td colspan="3" id="branch" style="color: dodgerblue"></td>
                        </tr>
                        <tr>
                            <th>{{__('admin.payment_method')}}:</th>
                            <td colspan="3" id="payment_method" style="color: dodgerblue"></td>
                        </tr>
                        <tr>
                            <th>{{__('admin.note')}}:</th>
                            <td colspan="3" id="note" style="color: dodgerblue"></td>
                        </tr>
                        <tbody id="tbody__">

                        </tbody>
                    </table>
                </div>
                <hr class="p0 m0">
                <div class="col-md-12">
                    <small class="price category-color">{{__('admin.date_1')}} {{__('admin.request')}}
                        :</small>
                    <small class="price category-color" id="created"></small>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
{{-- end model show--}}
