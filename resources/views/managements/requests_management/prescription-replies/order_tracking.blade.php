@php($page_title = __('provider.tracking_request'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
    <style>
        .modal-backdrop {
            opacity: 0 !important;
        }

        .modal-backdrop:last-of-type {
            opacity: 0.5 !important;
        }

        .modal-content {
            -webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, .16), 0 3px 6px rgba(0, 0, 0, .23);
            box-shadow: 0 3px 6px rgba(0, 0, 0, .16), 0 3px 6px rgba(0, 0, 0, .23);
        }
    </style>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('provider.requests')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/'.providerType().'dashboard')}}">{{__('provider.Dashboard')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('provider.requests')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                    <button class="btn btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                            id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" style="background:#002582 !important;">
                        <i class="ft-filter icon-left"></i> {{__('provider.filter')}}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <a href="?filter_name=" class="dropdown-item filterRequestClick" id="">{{__('provider.all')}}</a>
                        <a href="?filter_name=requested" class="dropdown-item filterRequestClick"
                           id="requested">{{__('provider.requested')}}</a>
                        <a href="?filter_name=received" class="dropdown-item filterRequestClick"
                           id="received">{{__('provider.received')}}</a>
                        <a href="?filter_name=repair" class="dropdown-item filterRequestClick"
                           id="repair">{{__('provider.repair')}}</a>
                        <a href="?filter_name=deliver" class="dropdown-item filterRequestClick"
                           id="deliver">{{__('provider.deliver')}}</a>
                        <a href="?filter_name=delivered" class="dropdown-item filterRequestClick"
                           id="delivered">{{__('provider.delivered')}}</a>
                        <a href="?filter_name=canceled" class="dropdown-item filterRequestClick"
                           id="canceled">{{__('provider.canceled_requests')}}</a>
                    </div>
                    {{--<a href="{{ route(providerType().'request.report')}}" class="btn btn-primary"
                       style="background:#002582 !important;">
                        <i class="ft ft-printer"></i>
                        {{__('provider.requests_report')}}
                    </a>--}}
                </div>
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('provider.successfully_done')}}!</strong>
                                <p id="success-message">{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{__('provider.post_index')}} {{__('provider.requests')}}</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
{{--                        <li><a data-action="reload" id="reload"><i class="ft-rotate-cw"></i></a></li>--}}
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-head">
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <div class="row">
                            <div class="col-sm-12 col-md-3">
                                <label>{{__('provider.show')}} </label>
                                @php($table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10)
                                <select style="width: 100px" name="table_length" aria-controls="user_table"
                                        class="custom-select custom-select-sm form-control form-control-sm">
                                    {{--                                    @php($filter_name_parameter = isset($_GET['filter_name']) ? '?filter_name='.$_GET['filter_name'].'&' : '?')--}}
                                    @php($filter_name_parameter = url()->current() == url()->full()?url()->current().'?': url()->full(). '&')
                                    <option value="{{ $filter_name_parameter }}table_length=10"
                                            @if($table_length == 10) selected @endif>10
                                    </option>
                                    <option value="{{ $filter_name_parameter }}table_length=25"
                                            @if($table_length == 25) selected @endif>25
                                    </option>
                                    <option value="{{ $filter_name_parameter }}table_length=50"
                                            @if($table_length == 50) selected @endif>50
                                    </option>
                                    <option value="{{ $filter_name_parameter }}table_length=100"
                                            @if($table_length == 100) selected @endif>100
                                    </option>
                                </select>
                            </div>
                            <div class="col-sm-12 col-md-9">
                                <form style="display: flex;justify-content: start;">
                                    @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                    <div class="col-sm-12 col-md-4">
                                        <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                            <option value="number"
                                                    @if($search_type == 'number') selected @endif>{{__('provider.id')}}</option>
                                            <option value="name"
                                                    @if($search_type == 'name') selected @endif>{{__('provider.name')}}</option>
                                            <option value="phone"
                                                    @if($search_type == 'phone') selected @endif>{{__('provider.phone')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <input type="search" class="form-control form-control-sm"
                                               placeholder="{{__('provider.search')}}"
                                               name="query"
                                               aria-controls="user_table"
                                               value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <button type="submit" class="btn btn-sm btn-primary col-12">{{__('provider.search')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <table width="100%" class="table dataTable" id="user_table">
                            <thead>
                            <tr>
                                <th hidden>{{__('provider.updated_at')}}</th>
                                <th>{{__('provider.id')}}</th>
                                <th>{{__('provider.name')}}</th>
                                <th>{{__('provider.phone')}}</th>
                                <th>{{__('provider.request')}}</th>
                                <th>{{__('provider.cache_payment')}}</th>
                                <th>{{__('provider.payment_type')}}</th>
                                <th width="120">{{__('provider.status')}}</th>
                                <th>{{__('provider.action')}}</th>
                                <th>{{__('provider.time')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($tracking_requests ) == 0)
                                <tr>
                                    <td colspan="9" class="text-center">
                                        {{__('provider.no_data')}}
                                        <hr>
                                    </td>
                                </tr>
                            @endif
                            @foreach($tracking_requests as $tracking_request)
                                <tr style="{{$tracking_request->background_color_row}}">
                                    <td hidden>{{$tracking_request->updated_at}}</td>
                                    <td>{{$tracking_request->request_number}}</td>
                                    <td>{{$tracking_request->cart->user->name}}</td>
                                    <td>{{$tracking_request->cart->user->phone}}</td>
                                    <td>{{$tracking_request->user_request_number}}</td>
                                    <td>
                                        {{$tracking_request->cache_payment}}
                                    </td>
                                    <td>
                                        @if ($tracking_request->payment_type == 'cash')
                                            <i class='la la-money'></i> {{__('provider.cash')}}
                                        @elseif ($tracking_request->cache_payment == 'fromWallet')
                                            {{__('provider.from_wallet')}}
                                        @else
                                            <span style='color: #c9c9c9'>{{__('provider.no_data')}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @php($status = $tracking_request->status)

                                        @if ($status == 'requested')
                                            <span class='btn btn-sm btn-danger'>{{__('provider.requested')}}</span>
                                        @elseif ($status == 'repair')
                                            <a><span id='{{$tracking_request->id}}'
                                                     class='btn btn-sm btn-warning'
                                                     title='{{__("provider.click_here_to_change_the_carrier")}}'>{{__('provider.repair')}}</span></a>
                                        @elseif ($status == 'deliver')
                                            <a><span id='{{$tracking_request->id}}'
                                                     class='btn-sm btn-primary'
                                                     title='{{__("provider.click_here_to_change_the_carrier")}}'>{{__('provider.deliver')}}</span></a>
                                        @elseif ($status == 'delivered')
                                            <span class='btn btn-sm btn-success'>{{__('provider.delivered')}}</span>
                                        @elseif ($status == 'canceled')
                                            <span class='btn btn-sm btn-dark'>{{__('provider.canceled')}}</span>
                                        @elseif ($status == 'reviewed')
                                            <a><span id='{{$tracking_request->id}}'
                                                     class='btn btn-sm btn-cyan'
                                                     title='{{__("provider.click_here_to_change_the_carrier")}}'>{{__('provider.reviewed')}}</span></a>
                                        @elseif ($status == 'received')
                                            <a><span id='{{$tracking_request->id}}'
                                                     class='editRequestClick btn btn-sm btn-cyan'
                                                     title='{{__("provider.click_here_to_change_the_carrier")}}'>{{__('provider.received')}}</span></a>
                                        @elseif ($status == 'on_the_way')
                                            <span class='btn btn-sm btn-success'>{{__('provider.delivered')}}</span>
                                        @elseif ($status == 'on_branch')
                                            <span class='btn btn-sm btn-success'>{{__('provider.delivered')}}</span>
                                        @elseif ($status == 'completedRequest')
                                            <span class='btn btn-sm btn-primary'>{{__('provider.completedRequest')}}</span>
                                        @elseif ($status === 'done')
                                            <span
                                                class='btn btn-sm btn-success'>{{__('provider.deliverySuccessful')}}</span>
                                        @else
                                            <span style='color: #c9c9c9'>{{__('provider.no_data')}}</span>
                                        @endif
                                    </td>
                                    <td widtd="110">
                                        <a class="showB" id="{{$tracking_request->id}}"><span style="font-size:12px;color: #0092e2"
                                                title="{{__('provider.click_here_to_view_details')}}">{{__('provider.detail')}}...</span></a>
                                    </td>
                                    <td><small>{{$tracking_request->created_at->diffForHumans()}}</small></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                    {{ $tracking_requests->appends($pagination_links)->links() }}
                    {{--{{ $tracking_requests->links() }}--}}

                    <span>{{__('provider.show').' '. $tracking_requests->count() .' '.__('provider.out_of').' '. $tracking_request_count . ' '. __('provider.record')}}</span>
                </div>
            </div>
        </div>
    </div>

    <div id="messageSave1" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="alert bg-info alert-icon-left mb-2" role="alert">
                        <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                        <strong>{{__('provider.successfully_done')}}!</strong>
                        <p id="success-message">{{session('success')}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- start model editRequestModel  Message--}}
    <div id="editRequestModel" class="modal fade text-left" role="dialog">
        <div class="modal-dialog" style="max-width: 50%;">
            <div class="modal-content">
                <div class="">
                    <div class="card-content">
                        <div class="card-header" style="padding-bottom: 0;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                            </button>
                            <h3>{{__('provider.Assign_request_to_branch')}}</h3>
                        </div>
                        <hr>
                        <div id="error-cancel"></div>
                        {{--<form class="form form-horizontal" action="{{ route(providerType().'request.update',['id'=>52])}}" method="POST"
                              enctype="multipart/form-data" id="editRequestForm">
                            @csrf
                            <div class="form-body">
                                <div id="inputs-form">

                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" id="save-request" class="col-md-12 btn btn-primary">
                                            <i class="fa-save"></i> {{__('provider.save')}}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                        </form>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model editRequestModel Message--}}

    @include('request.show_details')

    {{-- start modal show status audit --}}
    <div id="modalStatusAudit" class="modal fade text-left" role="dialog">
        <div class="modal-dialog long-mode modal-dialog-centered">
            <div class="modal-content">
                <div class="card-content">
                    <div class="modal-header danger">
                        <h5 class="modal-title" id="myModalLabel1">{{__('provider.tracking_request_cases')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table width="100%" id="table-audit" class="table table-responsive table-custom-responsive">
                            <thead style="background-color: #f3f3f3">
                            <tr>
                                <th>{{__('provider.old_status')}}</th>
                                <th>{{__('provider.new_status')}}</th>
                                <th>{{__('provider.updated_by')}}</th>
                                <th>{{__('provider.time')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <hr class="p0 m0">
                    <br>
                </div>
            </div>
        </div>
    </div>
    {{-- end  modal show status audit --}}

    @include('include.modal_loading_show')
@endsection
@section('script')
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    <script>
        $("select[name=table_length]").click(function () {

            var open = $(this).data("isopen");
            if (open) {
                window.location.href = $(this).val()
            }
            /*set isopen to opposite so next time when use clicked select box*/
            /*it wont trigger this event*/
            $(this).data("isopen", !open);
        });

        var filter;




        /*start function reload data table*/
        $('#reload').click(function () {
            setTimeout(function () {
                $('#confirmModalDelete').modal('hide');
                $('#ok_button').text('{{__('provider.yes')}}');
            }, 500);
        });
        /*end function reload data table*/

        /*start code show details ajax*/
        var carrier_detals_id;
        $(document).on('click', '.showB', function () {
            carrier_detals_id = $(this).attr('id');
            detail_url = "{{ url('/'.providerTypeUrl())}}" + "/showDetails/" + carrier_detals_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('provider.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $("#details_tbody").empty();
                    $("#tbody__").empty();
                    var request__type = '';
                    for (var i = 0; i < data.cart_detail.length; i++) {
                        if (data.request_type[i + 1] === 'product')
                            request__type = "{{__('provider.a_product')}}";
                        else if (data.request_type[i + 1] === 'special')
                            request__type = "{{__('provider.a_special_product')}}";
                        else if (data.request_type[i + 1] === 'offer')
                            request__type = "{{__('provider.offer')}}";
                        else if (data.request_type[i + 1] === 'hospitality')
                            request__type = "{{__('provider.a_hospitality')}}";
                        else
                            request__type = data.request_type[i + 1];

                        /*"<td>" + request__type + "</td>" +*/

                        $("#details_table").append("<tr>" +
                            "<td>" + data.product_number[i + 1] + "</td>" +
                            "<td>" + data.product[i + 1] + ' (<span class="color-blue">' + data.unit[i + 1] + '</span>) ' + "</td>" +

                            "<td>" + data.cart_detail[i].quantity + "</td>" +
                            "<td>" + data.product_discount_id[i + 1] + "</td>" +
                            "<td>" + data.product_price[i + 1] + "</td>" +
                            "<td>" + data.total_quantity[i + 1] + "</td>" +
                            "</tr>");
                    }
                    $("#details_table").append(
                        "<tr style='background-color: #f1f3ff;'>" +
                        "<td colspan='2'>{{__('provider.total')}}</td>" +
                        "<td>" + data.sum + "</td>" +
                        "<td></td>" +
                        "<td></td>" +
                        "<td>" + data.price_sum + "</td>" +
                        "</tr>" +
                        "<tr><td colspan='7'></td></tr>"
                    );

                    if (data.coupon !== null) {
                        let coupon_value_type = (data.coupon_value_type === 'percentage')? ' % ' : ' YER ';
                        $("#details_table").append(
                            "<tr>" +
                            "<td colspan='5'>{{__('provider.coupon_reduction')}}</td>" +
                            "<td>" + data.coupon + coupon_value_type + "</td>" +
                            "</tr>"
                        );
                    }

                    if (data.discount_value !== null) {
                        let discount_value_type = (data.discount_value_type === 'percentage')? ' % ' : ' YER ';
                        $("#details_table").append(
                            "<tr>" +
                            "<td colspan='5'>{{__('provider.volume_discount')}}</td>" +
                            "<td>" + data.discount_value + discount_value_type + "</td>" +
                            "</tr>"
                        );
                    }
                    $("#details_table").append(
                        "<tr>" +
                        "<td colspan='5' >{{__('provider.remaining_amount')}}</td>" +
                        "<td style='background-color: #f1f3ff;'>" + data.remaining_amount + "</td>" +
                        "</tr>"
                    );

                        var carrier = data.carrier === null ? '' : data.carrier;
                        $("#tbody__").append(
                            "<tr>" +
                            "<th colspan='2'>{{__('provider.address')}}:</td>" +
                            "<td style='color: dodgerblue'>" + data.address + " - " + data.desc + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<th colspan='2'>{{__('provider.the_name')}} {{__('provider.delivery_driver')}}:</td>" +
                            "<td style='color: dodgerblue'>" + carrier + "</td>" +
                            "</tr>"
                        );
                    var cancel_note = data.cancel_note === null ? '' : data.cancel_note;
                    $("#tbody__").append(
                        "<tr>" +
                        "<th colspan='2'>{{__('provider.cancel_note')}}:</td>" +
                        "<td style='color: dodgerblue'>" + cancel_note + "</td>" +
                        "</tr>"
                    );
                    if (data.hospitality !== null) {
                        $("#tbody__").append(
                            "<tr>" +
                            "<th colspan='2'>{{__('provider.date_1')}} {{__('provider.Hospitality')}}:</td>" +
                            "<td style='color: dodgerblue'>" + data.hospitality + "</td>" +
                            "</tr>"
                        );
                    }
                    $('#user-name').text(data.user_name);
                    $('#request_no').text(data.request_no);
                    $('#note').text(data.note);

                    if (data.payment_type == "fromWallet") {
                        $('#payment_method').text("{{__('provider.from_wallet')}}");
                    } else if (data.payment_type === "cash") {
                        $('#payment_method').text("{{__('provider.cash')}}");
                    } else {
                        $('#payment_method').text("error");
                    }

                    $('#the_amount_to_be_paid').text(data.cache_payment);
                    $('#branch').text(data.branch);
                    var updated_by = data.updated_by === null ? '{{__('provider.from_delivery')}}' : data.updated_by;
                    $('#created').text(data.created_at + ' | ' + data.created_at_diffForHumans + '     |     ' + '{{__('provider.updated_by')}}' + ' : ' + updated_by);

                    /*hidden column action from header*/
                    $('.hide-in-tracking').attr('hidden', true);

                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');
                    buttonWidget();

                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('provider.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code show details ajax*/

        @can('show prescription-replies')

        /*start editRequest save*/
        let urlRequestPost;
        let status;
        let element_td;
        $(document).on('click', '.editRequestClick', function () {
            var available = $('input[name=available]:checked').val();
            element_td = $(this).parent();
            edit_id = $(this).attr('id');
            detail_url = "{{ url('/'.providerType().'request')}}/edit/" + edit_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('provider.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    status = data.status;
                    $('#inputs-form').empty().append(
                        '<div class="form-group row">' +
                        '    <label class="col-md-3 label-control">{{__('provider.request')}}</label>' +
                        '    <div class="col-md-7 mx-auto">' +
                        '        <div class="row skin radio-input-group">' +
                        '            <fieldset style="margin-left: 20px; margin-right: 20px">' +
                        '                <input type="radio" name="available" id="input-radio-15"' +
                        '                       class="rabio-bttuon-big" checked required value="1">' +
                        '                <label for="input-radio-15" style="color: #002581;">{{__('provider.agree_request')}}</label>' +
                        '            </fieldset>' +
                        '            <fieldset style="margin-left: 20px; margin-right: 20px">' +
                        '                <input type="radio" name="available" id="input-radio-16"' +
                        '                       class="rabio-bttuon-big" required value="2">' +
                        '                <label for="input-radio-16" style="color: #FFC107;">{{__('provider.not_agree_request')}}</label>' +
                        '            </fieldset>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="col-md-2 mx-auto"></div>' +
                        '</div>'
                    );

                    filtter();
                    urlRequestPost = "{{ url('/'.providerType().'request/edit-status')}}" + "/" + data.id;
                    if (status === 'requested' || status === 'received') {
                        if ($('#carrier_id').length) {
                            document.getElementById('carrier_id').value = data.carrier_id;
                        }
                    }
                    if ($('#request_status').length) {
                        document.getElementById('request_status').value = data.status;
                    }
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#editRequestModel').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('provider.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });

        /*end  editRequest save*/


        function filtter() {
            /*start code filter carrier ajax*/
            $('#carrier_status').change(function () {
                var status_carrier = $(this).val();
                var id_branch = 1;
                if (id_branch) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('/'.providerType().'request/falterStatus')}}" + "/" + id_branch + "?status_carrier=" + status_carrier,
                        success: function (data) {
                            if (data) {
                                $("#carrier_id").empty();
                                $("#carrier_id").append('<option value=""> {{__("provider.select_option")}}  </option>');
                                $.each(data, function (key, value) {
                                    $("#carrier_id").append('<option value="' + key + '">' + value + '</option>');
                                });
                            } else {
                                $("#carrier_id").empty();
                            }
                        }
                    });
                } else {
                    $("#carrier_id").empty();
                }
            });
            /*end code filter carrier ajax*/

            /*start  SaveRequest ajax*/
            $('#save-request').click(function () {
                $.ajax({
                    type: 'post',
                    url: urlRequestPost,
                    dataType: "json",
                    data: {
                        available: $("input[name=available]:checked").val(),
                        carrier_id: $("select[name=carrier_id] :selected").val(),
                        status: $("select[name=request_status] :selected").val(),
                        cancel_text: $("textarea[name=cancel_text]").val(),
                        _token: $("input[name=_token]").val()
                    },
                    beforeSend: function () {
                        $('#save-request').text('{{__('provider.saving')}}...').attr('disabled', true);
                    },
                    success: function (data) {
                        $('#error-cancel').text('');
                        if (data.success !== undefined) {
                            setTimeout(function () {
                                $('#editRequestModel').modal('hide');
                                $('#success-message').text(data.success + '.');
                            }, 500);
                            $('#messageSave1').modal('hide');
                            setTimeout(function () {
                                $('#messageSave1').modal('show');
                            }, 510,);
                            setTimeout(function () {
                                $('#messageSave1').modal('hide');
                            }, 3000,);
                            $('#messageSave1').modal('hide');
                            /*window._initTimer();*/


                            /*==============================*/

                            let request_status = data.status;
                            let request_id = data.id;
                            let span;

                            if (request_status === 'requested') {
                                span = "<span class='btn btn-sm btn-danger'>{{__('provider.requested')}}</span>";
                            } else if (request_status === 'repair') {
                                span = "<a>" +
                                    "<span id='" + request_id + "' class='editRequestClick btn btn-sm btn-warning'" +
                                    " title = '{{__("provider.click_here_to_change_the_carrier")}}'>{{__('provider.repair')}}" +
                                    " </span></a>";
                            } else if (request_status === 'canceled') {
                                span = "<span class='btn btn-sm btn-dark'>{{__('provider.canceled')}}</span>";
                            } else if (request_status === 'received') {
                                span = "<a><span id='" + request_id + "' class='editRequestClick btn btn-sm btn-cyan'" +
                                    "title='{{__("provider.click_here_to_change_the_carrier")}}'>{{__('provider.received')}}</span></a>";
                            } else {
                                span = "<span style='color: #c9c9c9'>{{__('provider.no_data')}}</span>";
                            }

                            element_td.parent().parent().attr('style', data.background_color_row);
                            element_td.html(span);
                        }
                        $('#save-request').text('{{__('provider.save')}}').attr('disabled', false);
                    },
                    error: function (data) {
                        var obj = JSON.parse(data.responseText);

                        obj.error.branch_id === undefined ? $('#branch').removeClass('is-invalid') : $('#branch').addClass('is-invalid');
                        obj.error.carrier_id === undefined ? $('#carrier_id').removeClass('is-invalid') : $('#carrier_id').addClass('is-invalid');

                        obj.error.branch_id = obj.error.branch_id === undefined ? '' : obj.error.branch_id;
                        obj.error.carrier_id = obj.error.branch_id === undefined ? '' : obj.error.carrier_id;

                        $('#message-error-branch').html('<strong>' + obj.error.branch_id + '</strong>');
                        $('#message-error-carrier').html('<strong>' + obj.error.carrier_id + '</strong>');

                        $('#save-request').text('{{__('provider.save')}}').attr('disabled', false);
                    }
                });
               /* window._initTimer();*/
            });
            /*end SaveRequest ajax*/
        }

        @endcan

        $('#status-audit').attr('hidden', false);
        $(document).on('click', '#status-audit', function () {
            detail_url = "{{ url('/'.providerType().'request/audit')}}" + "/" + carrier_detals_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('provider.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $('#table-audit tbody').empty();
                    data = data.data;
                    let html = '';
                    for (let i = 0; i < data.length; i++) {
                        if (data[i] != null) {
                            html += '<tr>' +
                                '<td>' + getStatusName(data[i].old_status) + '</td>' +
                                '<td>' + getStatusName(data[i].status) + '</td>' +
                                '<td>' + data[i].admins_type + ' | ' + data[i].admin_name + '</td>' +
                                '<td><small title="' + data[i].created_at + '">' + data[i].diffForHumans + '</small></td>' +
                                '</tr>';
                        }
                    }

                    $('#table-audit tbody').append(html);
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#modalStatusAudit').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('provider.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });

        function getStatusName(data_status) {
            let status = '';
            if (data_status === 'requested') {
                status = "<span class='danger'>{{__('provider.requested')}}</span>";
            } else if (data_status === 'received') {
                status = "<span class='cyan'>{{__('provider.received')}}</span>";
            } else if (data_status === 'repair') {
                status = "<span class='warning'>{{__('provider.repair')}}</span>";
            } else if (data_status === 'deliver') {
                status = "<span class='primary'>{{__('provider.deliver')}}</span>";
            } else if (data_status === 'delivered') {
                status = "<span class='success'>{{__('provider.delivered')}}</span>";
            } else if (data_status === 'canceled') {
                status = "<span>{{__('provider.canceled')}}</span>";
            }

            return status;
        }
    </script>
    <script>
        function buttonWidget() {
            $(document).ready(function () {
                $(".widget").addClass("animation-reveal");
                $(".widget").css("opacity", "0");
                setTimeout(() => {
                    $(".widget").removeClass("animation-reveal");
                    $(".widget").css("opacity", "1");
                }, 1000);
            });

            $(document).on('click', '.toggle', function () {
                let widget = $(this).parent().parent();
                if (widget.hasClass('active') === true) {
                    widget.removeClass("active");
                } else {
                    widget.addClass("active");
                }
            });
        }
    </script>

    <!-- BEGIN: Page JS-->
    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/checkbox-radio.js')}}"></script>
@endsection
