@php($page_title = __('provider.Special_requests'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('provider.Special_requests')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a
                                    href="{{ url('/special_product')}}">{{__('provider.Special_requests')}}</a></li>
                            <li class="breadcrumb-item active">{{__('provider.edit')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('provider.successfully_done')}}!</strong>
                                <p>{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form form-horizontal"
                          action="{{ route(providerType().'special_product.update', ['id' => $special_product->id])}}" method="POST">
                        @csrf
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <div class="form-details card-body row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        @if($special_product->image==null)
                                            <img src="{{asset('storage/special_product/No-Product-Image-v2.png')}}"
                                                 class="rounded-circle  height-150 img-thumbnail" id="user-image">
                                        @else
                                            <img src="{{asset('/storage').'/'.$special_product->image}}"
                                                 class="rounded-circle  height-150 img-thumbnail" id="user-image">
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <h2 class="name product-title"
                                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif padding-top: 25px;">{{$special_product->user_name}}</h2>
                                        <h6 class="email product-title"
                                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif">{{$special_product->name}}</h6>
                                        <h6 class="phone product-title"
                                            style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif">{{$special_product->date}}</h6>
                                        <div class=" btn-round position-absolute round"
                                             style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif font-size: 20px">
                                            @if($special_product->status==1)
                                                <span
                                                    class="btn btn-round btn-primary btn-sm">{{__('provider.has_been_approved')}}</span>
                                            @else
                                                <span
                                                    class="btn btn-round btn-danger btn-sm">{{__('provider.not_approved')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="col-md-3 label-control">{{__('provider.detail')}}</label>
                                    <div class="col-md-6 mx-auto">
                                        {{$special_product->desc}}
                                        <hr>
                                    </div>
                                    <div class="col-md-3 mx-auto"></div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control">{{__('provider.note')}}</label>
                                    <div class="col-md-6 mx-auto">
                                        {{$special_product->note}}
                                        <hr>
                                    </div>
                                    <div class="col-md-3 mx-auto"></div>
                                </div>
                                <div class="form-group row {{ $errors->has('price') ? ' has-error' : '' }}">
                                    <label class="col-md-3 label-control">{{__('provider.price')}} <span
                                            class="danger">*</span></label>
                                    <div class="col-md-6 mx-auto">
                                        <input type="number" class="form-control @error('price') is-invalid @enderror"
                                               name="price" value="{{$special_product->price}}">
                                        @error('price')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-3 mx-auto"></div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control"
                                           style="margin-top: 20px">{{__('provider.status')}} <span class="danger">*</span></label>
                                    <div class="col-md-6 mx-auto">
                                        <div class="row icheck_minimal skin">
                                            <fieldset style="margin: 20px">
                                                <input type="radio" name="status" id="input-radio-15" value="1"
                                                       @if($special_product->status==1) checked @endif>
                                                <label style="color: #002581">{{__('provider.has_been_approved')}}</label>
                                            </fieldset>
                                            <fieldset style="margin: 20px">
                                                <input type="radio" name="status" id="input-radio-16" value="0"
                                                       @if($special_product->status==0) checked @endif>
                                                <label style="color: #FC0021;">{{__('provider.not_approved')}}</label>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mx-auto"></div>
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <a href="javascript:window.history.back()"
                                           class="col-md-9 btn btn-dark">{{__('provider.cancel')}}</a>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="col-md-12 btn btn-primary">
                                            <i class="ft-save"></i> {{__('provider.save')}}
                                        </button>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/checkbox-radio.js')}}"></script>
@endsection
