@php($page_title = __('admin.prescription'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.prescription')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">{{__('admin.prescription')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                {{-- <div class="btn-group float-md-right">
                    <a href="{{ url('/'.'special_product/recycle_bin')}}" class="btn btn-primary" style="color: white">{{__('admin.recycle_bin')}}  <i class="ft-trash position-right"></i></a>                </div> --}}
            </div>
        </div>
        <div id="messageSave" class="modal fade text-left" role="dialog">
            <div class="modal-dialog">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="alert bg-info alert-icon-left mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                            <strong>{{__('admin.successfully_done')}}!</strong>
                            <p id="save-success">{{session('success')}}.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--@if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('admin.successfully_done')}}!</strong>
                                <p>{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif--}}
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{__('admin.post_index').' '.__('admin.prescription')}}</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload" id="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <table width="100%"
                               class="table table-white-space display no-wrap icheck table-middle"
                               id="user_table">
                            <thead>
                            <tr>
                                <th hidden>{{__('admin.updated_at')}}</th>
                                <th>{{__('admin.id')}}</th>
                                <th>{{__('admin.user_name')}}</th>
                                <th>{{__('admin.phone')}}</th>
                                <th>{{__('admin.branch_name')}}</th>
                                <th>{{__('admin.insurance_company')}}</th>
                                <th>{{__('admin.insurance_number')}}</th>
                                <th>{{__('admin.insurance_rate')}}</th>
                                <th>{{__('admin.image')}}</th>
                                <th>{{__('admin.note')}}</th>
                                <th>{{__('admin.action')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                </div>
            </div>
        </div>
    </div>

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

    {{-- start model show  Message--}}
    <div id="confirmModalShow" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="card-content">
                    <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                            style=" @if( app()->getLocale() == 'ar')  margin-right: 472px; @else margin-left: 480px @endif"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin: 6px">&times;</span>
                    </button>
                    <div class="form-details card-body p-0">
                        <span id="show_status"></span>
                        <div class="align-items-center text-center">
                            <img src="" class="img-fluid rounded" alt="Card image cap" id="prescription_image"
                            onerror="this.src='{{asset('storage/thumbnail/640/defualt_product.png')}}'">
                        </div>
                        <div class="row p-1">
                            <div class="col-sm-12" style="padding-bottom: 10px">
                                <h2 class="user-name product-title" title="{{__('admin.user_name')}}"></h2>
                                <span id="id" class="btn btn-sm btn-outline-info float-right"
                                      style="margin-left: 2px; margin-right: 2px" title="{{__('admin.id')}}"></span>
                                <a style="font-size: 16px">
                                    <div class="price-reviews">
                                        <span class="price-box float-left" id="name"
                                              title="{{__('admin.insurance_company')}}"></span>
                                        <span id="insurance_company_number"
                                              title="{{__('admin.The_customers_insurance_number')}}"
                                              class="float-right btn btn-sm btn-outline-info float-right"
                                              style="margin-left: 2px; margin-right: 2px"></span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-12 p-0 m-0">
                                <table class="table mb-0">
                                    <tr>
                                        <th>{{__('admin.note')}}</th>
                                        <td id="note"></td>
                                    </tr>
                                </table>
                            </div>

                            <hr class="col-md-12 p0 m0">
                            <div class="col-md-12">
                                <small class="price category-color">{{__('admin.created_at')}}:</small>
                                <small class="price category-color" id="created"></small>
                                <br>
                                <small class="price category-color">{{__('admin.updated_at')}}:</small>
                                <small class="price category-color" id="updated_at"></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show  Message--}}

    {{-- start model show  Message--}}
    <div id="ModalShowImageNumber" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="card-content">
                    <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                            style=" @if( app()->getLocale() == 'ar') margin-right: 472px; @else margin-left: 480px @endif"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin: 6px">&times;</span>
                    </button>
                    <div class="form-details card-body p-0">
                        <span id="show_status"></span>
                        <div class="align-items-center text-center">
                            <img src="" title="{{__('admin.prescription_image')}}" class="img-fluid rounded"
                                 alt="Card image cap" id="prescription_image_number"
                                 onerror="this.src='{{asset('storage/thumbnail/640/defualt_product.png')}}'">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show  Message--}}

    {{-- start model Don Delete Message--}}
    {{--<div id="messageDonDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                        <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                        <strong>{{__('admin.successfully_done')}}!</strong>
                        <p>{{__('admin.deleted_successfully')}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    {{--end model Don Delete Message--}}

    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center"
                        style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark" data-dismiss="modal">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- start model createStatus  Message--}}
    @include('managements.requests_management.prescription.prescription_replies')
    {{-- end model createStatus Message--}}
    {{-- start model check Delete product price Message--}}
    <div id="confirmModalDeletePrice" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center" style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button_price" id="ok_button_price"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark"
                            id="cancel_button_price" {{--data-dismiss="modal"--}}>{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete product price Message--}}

    @include('include.message-don-delete')
    {{-- start model createStatus  Message--}}
    {{--<div id="specialProductModel" class="modal fade text-left" role="dialog">
        <div class="modal-dialog" style="max-width: 50%;">
            <div class="modal-content">
                <div class="">
                    <div class="card-content">
                        <div class="card-header" style="padding-bottom: 0;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                            </button>
                            <h3>{{__('admin.prescription')}}</h3>
                        </div>
                        <hr>
                        <form class="form form-horizontal"
                              action="{{ route('prescription.store')}}"
                              method="POST" enctype="multipart/form-data" id="my_form_id">0
                            @csrf
                            <input type="number" id="prescription_id_value" name="prescription_id" value="" hidden>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="col-md-3 label-control">{{__('admin.status')}} <span
                                            class="danger">*</span></label>
                                    <div class="col-md-7 mx-auto">
                                        <div class="row  skin">
                                            <fieldset style="margin-left: 20px; margin-right: 20px">
                                                <input type="radio" name="status" id="input-radio-15" required
                                                       value="1">
                                                <label for="input-radio-15"
                                                       style="color: #002581">{{__('admin.has_been_approved')}}</label>
                                            </fieldset>
                                            <fieldset style="margin-left: 20px; margin-right: 20px">
                                                <input type="radio" name="status" id="input-radio-16" required
                                                       value="0">
                                                <label for="input-radio-16"
                                                       style="color: #FFC107;">{{__('admin.not_approved')}}</label>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mx-auto"></div>
                                </div>
                                <div id="inputs">
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control">{{__('admin.price')}} <span
                                                class="danger">*</span></label>
                                        <div class="col-md-7 mx-auto">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">RY</span>
                                                </div>
                                                <input type="number" class="form-control" id="edit_price" name="price">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div>
                                            <span style="color: red; font-size: 11px">
                                            <strong id="data-erorr"></strong>
                                        </span>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                    <div class="form-group row">
                                        <label
                                            class="col-md-3 label-control">{{__('admin.notification_message')}}</label>
                                        <div class="col-md-7 mx-auto">
                                            <textarea rows="4" class="form-control" name="admin_message"></textarea>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="col-md-12 btn btn-primary" id="save-button">
                                            <i class="fa-save"></i> {{__('admin.save')}}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    {{-- end model createStatus Message--}}
@endsection
@section('script')
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    <!-- BEGIN: Page JS-->
    <script type="text/javascript">
        var lock = "<i class='ft-lock'></i>",
            unlock = "<i class='ft-unlock'></i>";

        var counter = 0;  /*created for image in product price*/
        var unit_count = 0;
        var count = 1;
        $(document).on('click', '#addRow', function () {
            console.log($(this));
            unit_count++;
            dynamic_field(unit_count);
            $('.switchBootstrap').bootstrapSwitch();
        });

        function dynamic_field(unit_count) {
            counter++;
            var div;
            div = '<div class="row">' +
                '<div class="form-group col-sm-12 col-md-12 p-0" style="margin-bottom: 23px;"><hr></div>' +
                '<div class="form-group col-sm-12 col-md-3">' +
                '<label>{{__('admin.medicine_name')}} <span class="danger">*</span></label>' +
                '<br>' +
                '<input type="text" name="medicine_name[]" required class="form-control">' +
                '@error("medicine_name[]")' +
                '<span class="invalid-feedback" role="alert">' +
                '<strong>{{ $message }}</strong>' +
                '</span>' +
                '@enderror' +
                '</div>' +
                '<div class="form-group col-sm-12 col-md-2">' +
                '<label>{{__('admin.currency')}}</label>' +
                '<br>' +
                '<select class="form-control" data-typeUnit-order="' + unit_count + '" required name="currency[]">' +
                '<option value="">{{__('admin.select')}}</option>' +
                '<option value="ريال يمني">{{__('admin.yemeni_riyal')}}</option>' +
                '<option value="دولار أمريكي">{{__('admin.american_dollar')}}</option>' +
                '<option value="ريال سعودي">{{__('admin.saudi_riyal')}}</option>' +
                '</select>' +
                '</div>' +
                '<div class="form-group col-sm-12 col-md-2">' +
                '<label>{{__('admin.price')}} <span class="danger">*</span></label>' +
                '<br>' +
                '<input type="number"  class="form-control " value="{{ old('medicine_price[]') }}" required name="medicine_price[]">' +
                '<span class="error-massege">' +
                '<strong></strong>' +
                '</span>' +
                '</div>' +
                '<div class="form-group col-sm-12 col-md-2">' +
                '<label>{{__('admin.note')}} </label>' +
                '<br>' +
                '<textarea class="form-control" name="note[]" rows="3" cols="4" data-typeUnit-order="0" >' +
                '</textarea>' +
                '</div>' +
                '<div class="form-group col-sm-12 col-md-1 p-0">' +
                '<ul class="list-inline mb-0" style="margin-top: 10px;">' +
                '<br>' +
                '<label for="switcherySize2" class="font-medium-2 text-bold-600 ml-1"' +
                'style="margin-left: 10px;margin-right: 10px;">{{__('admin.alternative')}}</label>' +
                '<input type="checkbox" id="switcherySize2" style="width: 17px" value="1" class="switchery" data-size="sm"' +
                'data-color="primary" name="is_alternative[]"/>' +
                '<div class="form-group mt-1">' +
                '</div>' +
                '</ul>' +
                '</div>';
            if (unit_count >= 0) {
                div += '<div class="form-group mb-1 col-sm-12 col-md-2 p-2">' +
                    '<div style="padding-top: 11px"></div>' +
                    '<button type="button" class="remove btn btn-sm btn-danger" style="border-radius: 5px; padding: 9px"><i class="la la-close font-small-3" ></i>  {{__('admin.delete')}}</button>' +
                    '</div>';
            } else {
                div += '</div>';
            }
            $('.form-body-rep').append(div);
        }


        $('.form-body-rep').on('click', '.remove', function () {
            $(this).parent().parent().remove();
        });

        var id_delete;
        $('.form-body-rep').on('click', '.final-delete', function () {
            id_delete = $(this).attr('data-id-delete');
            var result = confirm("{{__('admin.are_you_sure_you_want_to_remove_this_data')}}");
            if (result == true) {
                $.ajax({
                    url: "{{ url('/requests_management/prescription/delete')}}" + '/' + id_delete,
                    beforeSend: function () {
                        console.log('click before send');
                    },
                    success: function (data) {
                        console.log(id_delete);
                        if (data.error_deleting != undefined) {
                            $('#messageDonDelete').modal('hide');
                            setTimeout(function () {
                                $('#messageDonDelete').modal('show');
                            }, 510,);
                            setTimeout(function () {
                                $('#messageDonDelete').modal('hide');
                            }, 3000,);
                            $('#error-deleting').text(data.error_deleting);
                            $('#title-error').text("{{__('admin.error_message')}}!");
                        } else {
                            $('.form-body-rep').find("[data-id-delete='" + id_delete + "']").parent().parent().remove();
                        }
                    },
                    error: function (data) {
                    },
                    complete: function (data) {

                    }
                })
            }
        });
        $('#cancel_button_price').click(function () {
            $('#confirmModalDeletePrice').modal('hide');
        });
    </script>
    <script>
        /*start createPrice save*/
        let row_price;
        let prescription_id;
        $(document).on('click', '.createPriceClick', function () {
            $('.createPriceClick').parent().parent().css("background-color", "#fff");
            row_price = $(this).parent().parent();
            prescription_id = $(this).attr('id');
            let product_id_value = $('#prescription_id_value');
            product_id_value.attr('value', prescription_id);
            console.log(prescription_id);
            $.ajax({
                type: 'GET',
                url: "{{ url('/requests_management/prescription')}}" + "/edit/" + prescription_id,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    unit_count = 0;
                    $('.form-body-rep').empty();
                    if (data.prescription_data.length == 0) {
                        unit_count++;
                        dynamic_field(unit_count);
                        $('.switchBootstrap').bootstrapSwitch();
                    }

                    let price_note;
                    for (i = 0; i < data.prescription_data.length; i++) {
                        counter++;
                        let hidden = data.prescription_data[i].image == null ? "hidden" : "";
                        var div = '<div class="row">';
                        div += '<input type="text" ' + data.prescription_data[i].edit_disabled + ' hidden value="' + data.prescription_data[i].id + '" name="prescription_id[]">';
                        if (unit_count > 0) {
                            div += '<div class="form-group col-sm-12 col-md-12 p-0" style="margin-bottom: 23px;"><hr></div>';
                        }
                        var px = unit_count > 0 ? "21px" : "-28px";
                        var is_alternative = data.prescription_data[i].is_alternative == 1 ? ' checked ' : '';
                        let currency = data.prescription_data[i].currency;
                        price_note = '';
                        if (data.prescription_data[i].note != null) {
                            price_note = data.prescription_data[i].note;
                        }
                        div += '<div class="form-group col-sm-12 col-md-3">' +
                            '<label>{{__('admin.medicine_name')}} <span class="danger">*</span></label>' +
                            '<br>' +
                            '<input type="text" ' + data.prescription_data[i].edit_disabled + ' value="' + data.prescription_data[i].medicine_name + '" name="medicine_name[]" class="form-control" required>' +
                            '@error("medicine_name[]")' +
                            '<span class="invalid-feedback" role="alert">' +
                            '<strong>{{ $message }}</strong>' +
                            '</span>' +
                            '@enderror' +
                            '</div>' +
                            '<div class="form-group col-sm-12 col-md-2">' +
                            '<label>{{__('admin.currency')}}</label>' +
                            '<br>' +
                            '<select ' + data.prescription_data[i].edit_disabled + ' class="form-control " data-typeUnit-order="' + unit_count + '" required name="currency[]">' +
                            '<option value="">{{__('admin.select')}}</option>' +
                            '<option value="ريال يمني" ' + checkSelected(currency, "ريال يمني") + '>{{__('admin.yemeni_riyal')}}</option>' +
                            '<option value="دولار أمريكي" ' + checkSelected(currency, "دولار أمريكي") + '>{{__('admin.american_dollar')}}</option>' +
                            '<option value="ريال سعودي" ' + checkSelected(currency, "ريال سعودي") + '>{{__('admin.saudi_riyal')}}</option>' +
                            '</select>' +
                            '</div>' +
                            '<div class="form-group col-sm-12 col-md-2">' +
                            '<label>{{__('admin.price')}} <span class="danger">*</span></label>' +
                            '<br>' +
                            '<input type="number" ' + data.prescription_data[i].edit_disabled + ' class="form-control" value="' + data.prescription_data[i].medicine_price + '" required name="medicine_price[]">' +
                            '<span class="error-massege">' +
                            '<strong></strong>' +
                            '</span>' +
                            '</div>' +
                            '<div class="form-group col-sm-12 col-md-2">' +
                            '<label>{{__('admin.note')}} </label>' +
                            '<br>' +
                            '<textarea ' + data.prescription_data[i].edit_disabled + ' class="form-control" id="note" name="note[]" rows="3" cols="4" >' + data.prescription_data[i].note + '</textarea>' +
                            '</div>' +
                            '<div class="form-group col-sm-12 col-md-1 p-0">' +
                            '<ul class="list-inline mb-0" style="margin-top: 10px;">' +
                            '<br>' +
                            '<label for="switcherySize2" class="font-medium-2 text-bold-600 ml-1"' +
                            'style="margin-left: 10px;margin-right: 10px;">{{__('admin.alternative')}}</label>' +
                            '<input ' + data.prescription_data[i].edit_disabled + ' type="checkbox" id="switcherySize2" style="width: 17px"  value="1" class="switchery" data-size="sm"' +
                            'data-default-value="' + unit_count + '"' +
                            is_alternative +
                            'data-color="primary" name="is_alternative[]"/>' +
                            '<div class="form-group mt-1">' +
                            '</div>' +
                            '</ul>' +
                            '</div>';
                        if (data.prescription_data[i].available == 1) {
                            div += ' checked ';
                        }
                        @can('update prescription')
                        if (unit_count >= 1) {
                            div += '<div class="form-group col-sm-12 col-md-2 p-2">' +
                                '<div style="padding-top: 11px"></div>' +
                                '<button type="button" class="final-delete btn btn-sm btn-danger"' +
                                'data-id-delete="' + data.prescription_data[i].id + '" style="border-radius: 5px; padding: 9px">' +
                                '<i class="la la-close font-small-3" ></i>  {{__('admin.delete')}}</button>' +
                                '</div>' +
                                '</div>';
                        }
                        div += '</div></div>';

                        @endcan
                            unit_count++;
                        $('.form-body-rep').append(div);
                        $('.switchBootstrap').bootstrapSwitch();
                    }
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#specialProductModel').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end createPrice save*/

        /*Reload the data and display it in the datatable*/
        @include('include.reload-datatable')

        @if(count($errors)>0)
        $('#specialProductModel').modal('show');
        @endif
        /*start code show all data in datatable ajax*/
        $(document).ready(function () {
            $('#user_table').DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{  route('prescription') }}",
                },
                columns: [
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                    },
                    {
                        data: 'id',
                        name: 'id',
                    },
                    {
                        data: 'user_name',
                        name: 'user_name',
                    },
                    {
                        data: 'phone',
                        name: 'phone',
                    },
                    {
                        data: 'branch_name',
                        name: 'branch_name',
                    },
                    {
                        data: 'insurance_company',
                        name: 'insurance_company',
                        render: function (data, type, full, meta) {
                            if (data == null) {
                                return "<span style='color: #c9c9c92e'>{{__('admin.no_data')}}</span>"
                            } else {
                                return "<span style='color:#6B6F82 '>" + data + "</span>"
                            }
                        },
                    },
                    {
                        data: 'insurance_company_number',
                        name: 'insurance_company_number',
                        render: function (data, type, full, meta) {
                            if (data == null) {
                                return "<span style='color: #c9c9c92e'>{{__('admin.no_data')}}</span>"
                            } else {
                                var img = '<img  style="max-height:64px; max-width:64px" ' +
                                    'alt="' + data + '"' +
                                    'src="' + full['insurance_company_number_path'] + '"';
                                img += ' onerror=this.src="{{ url('/storage/thumbnail/150/defualt_product.png')}}">';
                                return img;
                            }
                        },
                    },
                    {
                        data: 'insurance_rate',
                        name: 'insurance_rate',
                    },
                    {
                        data: 'prescription_image',
                        name: 'prescription_image',
                        render: function (data, type, full, meta) {
                            var img = '<img  style="max-height:64px; max-width:64px"' +
                                'src="{{url('/').'/storage/'}}' + data + '"';
                            img += ' onerror=this.src="{{ url('/storage/thumbnail/150/defualt_product.png')}}">';
                            return img;
                        },
                        orderable: false
                    },
                    {
                        data: 'note',
                        name: 'note',
                        render: function (data, type, full, meta) {
                            if (data == null) {
                                return "<span style='color: #c9c9c92e'>{{__('admin.no_note')}}</span>"
                            } else {
                                return "<span style='color:#6B6F82 '>" + data + "</span>"
                            }
                        },
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    },
                ],
                'order': [[6, 'asc'], [1, 'desc']],
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });
        });

        /*start message save*/
        @if(session('success'))
        $('#message').text("{{session('success')}}.");
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        @endif
        /*end  message save*/

        $('#my_form_id').on('submit', function (event) {
            prescription_id = $(this).attr('id');
            var route = "{{  route('prescription.store')}}";
            /*var route = "{{  route('prescription.update',['id'=>"+prescription_id+"])}}";
            var route =  "{{ url('/requests_management/prescription')}}" + "/update/" + prescription_id;
            */
            /*
            var formData = new FormData(this);
            formData.append('no', 1);
            */
            var formData = new FormData($('#my_form_id')[0]);
            event.preventDefault();

            var type = "حفظ";
            var _type = "button_save";

            $.ajax({
                url: route,
                method: "POST",
                data: formData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $('#' + _type).html('<i class="la la-spinner spinner"></i><span>جاري ال' + type + '...</span>')
                },
                success: function (data) {
                    window.checkNotify();
                    $('#' + _type + '').text(type);
                    $('#specialProductModel').modal('hide');
                    $('#message').text(data.message + ".");
                    $('#messageSave').modal('show');
                    setTimeout(function () {
                        $('#messageSave').modal('hide');
                    }, 3000);
                    row_price.css("background-color", "#f2ffeb");
                    setTimeout(function () {
                        row_price.css("background-color", "#fff");
                    }, 11000);
                },
                error: function (error) {
                    console.log('M');
                    let obj = JSON.parse((error.responseText)).error;
                    console.log('O');
                    if (obj.medicine_name !== undefined) {
                        $('#my_form_id input[name=medicine_name]').addClass('is-invalid')
                            .parent().find('span strong').text(obj.medicine_name);
                    }
                    if (obj.currency !== undefined) {
                        $('#my_form_id input[name=currency]').addClass('is-invalid')
                            .parent().find('span strong').text(obj.currency);
                    }
                    if (obj.medicine_price !== undefined) {
                        console.log('MOH');
                        $('#my_form_id input[name=medicine_price]').addClass('is-invalid')
                            .parent().parent().find('span strong').text(obj.medicine_price);
                    }
                    $('#' + _type + '').text(type);
                }

            });
        });

        /*start code show details ajax*/
        var carrier_detals_id;
        $(document).on('click', '.showB', function () {
            carrier_detals_id = $(this).attr('id');
            detail_url = "{{ url('/requests_management/prescription')}}" + "/show/" + carrier_detals_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    /*if (data.status == 1) {
                        $('#show_status').html('<span class="btn btn-sm btn-primary position-absolute round" style=" margin: 10px; " >{{__("admin.has_been_approved")}}</span>');
                    } else {
                        $('#show_status').html('<span class="btn btn-sm btn-danger position-absolute round" style=" margin: 10px; " >{{__("admin.not_approved")}}</span>');
                    }*/
                    if (data.prescription_image == null) {
                        $('#prescription_image').attr('src', '{{asset('/storage/no-image.jpg')}}');

                    } else {
                        $('#prescription_image').attr('src', '{{asset('/storage')}}' + '/' + data.prescription_image);
                    }
                    if (data.name === " ") {
                        $('#name').text('');
                    } else {
                        $('#name').text(data.name);
                    }
                    if (data.insurance_company_number === "") {
                        $('#insurance_company_number').attr('hidden', true);
                    } else {
                        $('#insurance_company_number').attr('hidden', false)
                            .attr('data-image', data.insurance_company_number_path)
                            .html('<i class="ft ft-image"></i>');
                    }
                    $('.form-details .user-name').text(data.user_name);
                    $('#name').text(data.name);
                    $('.imag').text(data.name);
                    $('#id').text(data.id);
                    /*$(' #price').text(data.price);*/
                    $(' #note').text(data.note);
                    $('#created').text(data.created_at);
                    $('#updated_at').html(data.updated_at);
                    $(' .btn_dele').html(data.btn);
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');

                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        function checkSelected(currency, s) {
            let selected = '';
            if(currency === s ) selected = "selected";

            return selected;
        }

        $(document).on('click', '#insurance_company_number', function () {
            $('#prescription_image_number').attr('src', $(this).attr('data-image'));
            $('#ModalShowImageNumber').modal('show')
        });
    </script>
    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/checkbox-radio.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}"></script>
@endsection
