@php($page_title = __('provider.deferred_special_requests'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('provider.Special_requests')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">

                            <li class="breadcrumb-item active">{{__('provider.deferred_special_requests')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                {{-- <div class="btn-group float-md-right">
                    <a href="{{ url('/'.providerType().'special_product/recycle_bin')}}" class="btn btn-primary" style="color: white">{{__('provider.recycle_bin')}}  <i class="ft-trash position-right"></i></a>                </div> --}}
            </div>
        </div>
        <div id="messageSave1" class="modal fade text-left" role="dialog">
            <div class="modal-dialog">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="alert bg-info alert-icon-left mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                            <strong>{{__('provider.successfully_done')}}!</strong>
                            <p id="save-success">{{session('success')}}.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(session('success'))
            <div id="messageSave" class="modal fade text-left" role="dialog">
                <div class="modal-dialog">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="alert bg-info alert-icon-left mb-2" role="alert">
                                <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                                <strong>{{__('provider.successfully_done')}}!</strong>
                                <p>{{session('success')}}.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{__('provider.post_index').' '.__('provider.deferred_special_requests')}}</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload" id="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <table width="100%"
                               class="table table-white-space {{--row-grouping--}} display no-wrap icheck table-middle"
                               id="user_table">
                            <thead>
                            <tr>
                                <th hidden>{{__('provider.updated_at')}}</th>
                                <th>{{__('provider.id')}}</th>
                                <th>{{__('provider.user_name')}}</th>
                                <th>{{__('provider.phone')}}</th>
                                <th>{{__('provider.special_name')}}</th>
                                <th>{{__('provider.date_1')}} {{__('provider.request')}} </th>
                                <th>{{__('provider.price')}}</th>
                                <th>{{__('provider.status')}}</th>
                                <th>{{__('provider.action')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                </div>
            </div>
        </div>
    </div>



    {{-- start model show  Message--}}
    <div id="confirmModalShow" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="card-content">
                    <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                            style=" @if( app()->getLocale() == 'ar')  margin-right: 472px; @else margin-left: 480px @endif"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin: 6px">&times;</span>
                    </button>
                    <div class="form-details card-body p-0">
                        <span id="show_status"></span>
                        <div class="align-items-center text-center">
                            <img src="" class="img-fluid rounded" alt="Card image cap" id="user-image"
                                 onerror="this.src='{{asset('storage/thumbnail/640/defualt_offer.png')}}'">
                        </div>
                        <div class="row p-1">
                            <div class="col-sm-12" style="padding-bottom: 10px">
                                <h2 class="user-name product-title"></h2>
                                <span id="id" class="btn btn-sm btn-outline-primary float-right"
                                      style="margin-left: 2px; margin-right: 2px" title="{{__('provider.id')}}"></span>
                                <a style="font-size: 16px">
                                    <div class="price-reviews">
                                        <span class="price-box float-left" id="name" title="{{__('provider.date_1')}}"></span>
                                        <span id="date" title="{{__('provider.date_1')}} {{__('provider.request')}}" class="float-right btn btn-sm btn-outline-info float-right"
                                              style="margin-left: 2px; margin-right: 2px"></span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-12 p-0 m-0">
                                <table class="table mb-0">
                                    <tr>
                                        <th>{{__('provider.price')}}</th>
                                        <td id="price"></td>
                                    </tr>
                                    <tr>
                                        <th>{{__('provider.detail')}}</th>
                                        <td id="desc"></td>
                                    </tr>
                                    <tr>
                                        <th>{{__('provider.note')}}</th>
                                        <td id="note"></td>
                                    </tr>
                                </table>
                            </div>

                            <hr class="col-md-12 p0 m0">
                            <div class="col-md-12">
                                <small class="price category-color">{{__('provider.created_at')}}:</small>
                                <small class="price category-color" id="created"></small>
                                <br>
                                <small class="price category-color">{{__('provider.updated_at')}}:</small>
                                <small class="price category-color" id="updated_at"></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show  Message--}}

    {{-- start model Don Delete Message--}}
    <div id="messageDonDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                        <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                        <strong>{{__('provider.successfully_done')}}!</strong>
                        <p>{{__('provider.deleted_successfully')}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--end model Don Delete Message--}}

    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('provider.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center" style="margin:0;">{{__('provider.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('provider.yes')}}</button>
                    <button type="button" class="btn btn-dark" data-dismiss="modal">{{__('provider.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- start model createStatus  Message--}}
    <div id="specialProductModel" class="modal fade text-left" role="dialog">
        <div class="modal-dialog" style="max-width: 50%;">
            <div class="modal-content">
                <div class="">
                    <div class="card-content">
                        <div class="card-header" style="padding-bottom: 0;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                            </button>
                            <h3>{{__('provider.Special_requests')}}</h3>
                        </div>
                        <hr>
                        <form class="form form-horizontal" action="{{ route(providerType().'special_product.update',['id'=>1])}}"
                              method="POST" enctype="multipart/form-data" id="my_form_id">
                            @csrf
                            <input type="number" id="product_id_value" name="product_id" value="" hidden>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="col-md-3 label-control">{{__('provider.status')}} <span
                                            class="danger">*</span></label>
                                    <div class="col-md-7 mx-auto">
                                        <div class="row  skin">
                                            <fieldset style="margin-left: 20px; margin-right: 20px">
                                                <input type="radio" name="status" id="input-radio-15" required
                                                       value="1">
                                                <label style="color: #002581">{{__('provider.has_been_approved')}}</label>
                                            </fieldset>
                                            <fieldset style="margin-left: 20px; margin-right: 20px">
                                                <input type="radio" name="status" id="input-radio-16" required
                                                       value="0">
                                                <label style="color: #FFC107;">{{__('provider.not_approved')}}</label>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mx-auto"></div>
                                </div>
                                <div id="inputs">
                                    <div class="form-group row">
                                        <div class="col-md-3 "></div>
                                        <div class="col-md-7 mx-auto">
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                    <div class="form-group row" hidden>
                                        <label class="col-md-3 label-control">{{__('provider.notification_message')}}</label>
                                        <div class="col-md-7 mx-auto">
                                            <textarea  rows="4" class="form-control" name="admin_message"></textarea>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="col-md-12 btn btn-primary" id="save-button">
                                            <i class="fa-save"></i> {{__('provider.save')}}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model createStatus Message--}}


    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')
@endsection
@section('script')
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    <!-- BEGIN: Page JS-->
    <script>
        /*Reload the data and display it in the datatable*/
        @include('include.reload-datatable')

        /*start code show all data in datatable ajax*/
        $(document).ready(function () {
            $('#user_table').DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{  route(providerType().'special_product.deferred') }}",
                },
                columns: [
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                        class: 'updated_at'
                    },
                    {
                        data: 'id',
                        name: 'id',
                    },
                    {
                        data: 'user_name',
                        name: 'user_name',
                        class: 'user_name'
                    },
                    {
                        data: 'phone',
                        name: 'phone',
                    },
                    {
                        data: 'name',
                        name: 'name',
                        class: 'name'
                    },
                    {
                        data: 'date',
                        name: 'date',
                        class: 'date',
                    },
                    {
                        data: 'price',
                        name: 'price',
                        class: 'price',
                    },

                    {
                        data: 'status', name: 'status',
                        render: function (data, type, full, meta) {
                            if (data == '3') {
                                return "<button id='" + full['id'] + "' data-status='3' class='specialProduct btn btn-sm btn-outline-blue'>{{__('provider.deferred')}}</button>"
                            } else if (data == '5') {
                                return "<button id='" + full['id'] + "' data-status='5' class='specialProduct btn btn-sm btn-outline-success'>{{__('provider.confirmed')}}</button>"
                            } else {
                                return "<span style='color: #c9c9c9'>{{__('provider.no_data')}}</span>"
                            }
                        },
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    },
                ],
                'order': [[6, 'asc'], [1, 'desc']],
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });
        });
        /*start message save*/
        $('#messageSave').modal('show');
        setTimeout(function () {
            $('#messageSave').modal('hide');
        }, 3000);
        /*end  message save*/
        $('input[name=status]').change(function () {
            if ($(this).val() == 1) {
                $('#edit_price').attr('disabled', false)
            } else {
                $('#edit_price').attr('disabled', true)
            }
        });
        /*start code Delete ajax*/
        var $carrier_id;
        $(document).on('click', '.delete', function () {
            $carrier_id = $(this).attr('id');
            $('#confirmModalDelete').modal('show');
        });
        $('#ok_button').click(function () {
            $.ajax({
                url: "special_product/destroy/" + $carrier_id,
                beforeSend: function () {
                    $('#ok_button').text('{{__('provider.deleting')}}...');
                },
                success: function (data) {
                    setTimeout(function () {
                        $('#confirmModalDelete').modal('hide');
                        $('#user_table').DataTable().ajax.reload();
                        $('#ok_button').text('{{__('provider.yes')}}');
                    }, 500);
                    $('#messageDonDelete').modal('hide');
                    setTimeout(function () {
                        $('#messageDonDelete').modal('show');
                    }, 510,);
                    setTimeout(function () {
                        $('#messageDonDelete').modal('hide');
                    }, 3000,);
                    $('#confirmModalShow').modal('hide');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('provider.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code Delete ajax*/

        var urlByPost;
        @can('update prescription')
        /*start code edit*/
        var frm = $('#my_form_id');
        var create_id;
        var is_confirmed;
        $(document).on('click', '.specialProduct', function () {
            $('#save-button').text('{{__('provider.completedRequest')}}');
            $('#input-radio-15').iCheck('check');
            $('#input-radio-16').attr('disabled', false);
            $('#note-cancel').attr('hidden', true);
            $('#specialProductModel').modal('show');
            create_id = $(this).attr('id');
            is_confirmed = $(this).attr('data-status');
            if(is_confirmed == 5){
                $('#input-radio-16').attr('disabled', true);
            }
            urlByPost = "{{ url('/special_product')}}/completedRequest/" + create_id;
        });
        /*end code edit*/
        @endcan
        /*start SaveRequest ajax*/
        $('#save-button').click(function () {
            var status = $("input[name=status]:checked").val();
            var text = '{{__('provider.cancel_request')}}';
            if (status == 1) {
                text = '{{__('provider.completedRequest')}}';
            }
            $.ajax({
                type: 'post',
                url: urlByPost,
                dataType: "json",
                data: {
                    status: $("input[name=status]:checked").val(),
                    admin_message: $("textarea[name=admin_message]").val(),
                    _token: $("input[name=_token]").val()
                },
                beforeSend: function () {
                    $('#save-button').append('<i class="la la-spinner spinner"></i>');
                },
                success: function (data) {
                    if (data.error != null) {
                        $('#data-erorr').text(data.error);
                        $('#save-button').text('{{__('provider.completedRequest')}}');
                        $('#edit_price').addClass(' is-invalid');
                    } else {
                        $('#data-erorr').text('');
                        $('#edit_price').removeClass('is-invalid');
                        setTimeout(function () {
                            $('#specialProductModel').modal('hide');
                            $('#user_table').DataTable().ajax.reload();
                                $('#save-success').text(data.success + '.');
                        }, 500);
                        $('#messageSave1').modal('hide');
                        setTimeout(function () {
                            $('#messageSave1').modal('show');
                        }, 510,);
                        setTimeout(function () {
                            $('#messageSave1').modal('hide');
                        }, 3000,);
                        $('#messageSave1').modal('hide');
                        setTimeout(function () {
                            $('#save-button').text(text);
                        }, 400,);
                        window._initTimer();

                    }
                },
                error: function (data) {
                    $('#save-button').text(text);
                    alert('{{__('provider.loading_failed')}}');
                }
            })
        });
        /*end  SaveRequest ajax*/

        /*start code show details ajax*/
        var carrier_detals_id;
        $(document).on('click', '.showB', function () {
            carrier_detals_id = $(this).attr('id');
            detail_url = "{{ url('/'.providerTypeUrl().'/special_product')}}/show/" + carrier_detals_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('provider.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    if (data.status == 1) {
                        $('#show_status').html('<span class="btn btn-sm btn-primary position-absolute round" style=" margin: 10px; " >{{__("provider.has_been_approved")}}</span>');
                    } else {
                        $('#show_status').html('<span class="btn btn-sm btn-danger position-absolute round" style=" margin: 10px; " >{{__("provider.not_approved")}}</span>');
                    }
                    if (data.image == "") {
                        $('#user-image').attr('src', '{{asset('/storage/special_product/No-Product-Image-v2.png')}}');

                    } else {
                        $('#user-image').attr('src',  data.image);
                    }
                    $('.form-details .user-name').text(data.user_name);
                    $('#name').text(data.name);
                    $('.imag').text(data.name);
                    $('#id').text(data.id);
                    $('#date').text(data.date);
                    $(' #price').text(data.price);
                    $(' #desc').text(data.desc);
                    $(' #note').text(data.note);
                    $('#created').text(data.created_at);
                    $('#updated_at').html(data.updated_at);
                    $(' .btn_dele').html(data.btn);
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#confirmModalShow').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('provider.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });


        $('input[name=status]').change(function () {
            checkStatus($(this).val());
        });
        function checkStatus(status) {
            if (is_confirmed != 5){
                if (status == 1) {
                    $('#save-button').text('{{__('provider.completedRequest')}}');
                    inputs = '';
                } else {
                    $('#save-button').text('{{__('provider.cancel_request')}}');
                    inputs =
                        '<div class="form-group row" id="note-cancel">' +
                        '    <label class="col-md-3 label-control">{{__('provider.notification_message')}}</label>' +
                        '    <div class="col-md-7 mx-auto">' +
                        '        <textarea rows="4" class="form-control" name="admin_message"></textarea>' +
                        '    </div>' +
                        '    <div class="col-md-2 mx-auto"></div>' +
                        '</div>'
                    ;
                }
                $('#inputs').html(inputs);
                $('#inputs').html();
            }
        }
    </script>
    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
@endsection
