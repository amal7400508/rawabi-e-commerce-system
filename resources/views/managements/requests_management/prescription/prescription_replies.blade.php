{{-- start model createPrice  Message--}}
<style>
    .switchery.switchery-small.switchery-default{
        /*box-shadow: rgb(54 41 85) 0px 0px 0px 11px inset !important;*/
    }
</style>
<div id="specialProductModel" class="modal fade text-left" role="dialog">
    <div class="modal-dialog" style="max-width: 60%;">
        <div class="modal-content">
            <div class="">
                <div class="card-content">
                    <div class="card-header" style="padding-bottom: 0;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                        </button>
                        <h3>{{__('admin.prescription_replies')}}</h3>
                    </div>
                    <hr>
                    <form class="form form-horizontal" action="{{ route('prescription.store')}}" method="POST"
                          id="my_form_id" enctype="multipart/form-data">
                        @csrf
                        <input type="number" id="prescription_id_value" name="prescription_id_value" value="" hidden>
                        <div class="form-body-rep card-body">
                            <div class="row">
                                <div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.medicine_name')}} <span class="danger">*</span></label>
                                    <br>
                                    <input type="text" name="medicine_name[]" class="form-control" required>
                                    @error('medicine_name[]')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.price')}} <span class="danger">*</span></label>
                                    <br>
                                    <input type="number" class="form-control @error('medicine_price[]') is-invalid @enderror"
                                           value="{{ old('medicine_price[]') }}" required name="medicine_price[]" min="0"
                                           max="99999999.99">
                                    @error('medicine_price[]')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.note')}} </label>
                                    <br>
                                    <textarea class="form-control" name="note[]" rows="3" cols="4" data-typeUnit-order="0">
                                    </textarea>
                                </div>
                                <div class="form-group col-sm-12 col-md-1 p-0">
                                    <ul class="list-inline mb-0" style="margin-top: 10px;">
                                        <br>
                                        <label for="switcherySize2" class="font-medium-2 text-bold-600 ml-1"
                                               style="margin-left: 10px;margin-right: 10px;">{{__('admin.alternative')}}</label>
                                        <input type="checkbox" id="switcherySize2" value="1" class="switchery" data-size="sm" style="width: 17px"
                                               data-color="primary" name="is_alternative[]"/>
                                        <div class="form-group mt-1">
                                        </div>
                                    </ul>
                                    {{--<div class="heading-elements" id="check_show" style="margin: 5px">
                                        <br>
                                        <div style="padding-top: 6px"></div>
                                        <input type="checkbox" name="status[]" value="1" class="switchBootstrap"
                                               id="switchBootstrap18" data-on-color="primary" data-off-color="danger"
                                               data-on-text="{{__('admin.yes')}}"
                                               data-off-text="{{__('admin.no')}}" data-color="primary"
                                               data-label-text="{{__('admin.alternative')}}" checked/>
                                    </div>--}}
                                </div>
                                {{--<div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.medicine_name')}} <span class="danger">*</span></label>
                                    <br>
                                    <select class="form-control all-typeUnit" data-typeUnit-order="0" required>
                                        <option value="">{{__('admin.select')}}</option>
                                        <option value="1">{{__('admin.size')}}</option>
                                        <option value="2">{{__('admin.ball')}}</option>
                                        <option value="3">{{__('admin.weight')}}</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.unit')}} <span class="danger">*</span></label>
                                    <br>
                                    <select class="form-control" id="unitAll-0" required name="unit_id[]">
                                    </select>
                                </div>

                                <div class="form-group col-sm-12 col-md-3">
                                    <label>{{__('admin.taste')}} <span class="danger">*</span></label>
                                    <br>
                                    --}}{{--<select class="form-control" id="profession" required name="--}}{{----}}{{--taste[]--}}{{----}}{{--">
                                        <option value="">{{__('admin.select')}}</option>
--}}{{----}}{{--                                        @foreach($taste as $tastea)--}}{{----}}{{--
--}}{{----}}{{--                                            @if($tastea->id != 1)--}}{{----}}{{--
                                                <option value="--}}{{----}}{{--{{$tastea->id}}--}}{{----}}{{--">--}}{{----}}{{--{{$tastea->taste}}--}}{{----}}{{--</option>
--}}{{----}}{{--                                            @endif--}}{{----}}{{--
--}}{{----}}{{--                                        @endforeach--}}{{----}}{{--
                                    </select>--}}{{--
                                </div>--}}
                            </div>
                        </div>
                        @can('create prescription')
                            <div class="col-12">
                                <button type="button" id="addRow" class="addRow btn btn-primary"><i
                                        class="ft-plus"></i> {{__('admin.add')}}</button>
                            </div>
                        @endcan
                        <div class="form-actions text-right">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="col-md-12 btn btn-primary" id="button_save">
                                        <i class="fa-save"></i> {{__('admin.save')}}
                                    </button>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model createPrice Message--}}
