@php($page_title = __('admin.branches'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.providers_management')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a></li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.branches')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('branches')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="provider" @if($search_type == 'provider') selected @endif>{{__('admin.the_provider')}}</option>
                                                                    <option value="branch" @if($search_type == 'branch') selected @endif>{{__('admin.branch')}}</option>
                                                                    <option value="phone" @if($search_type == 'phone') selected @endif>{{__('admin.phone')}}</option>
                                                                    <option value="area" @if($search_type == 'area') selected @endif>{{__('admin.the_area')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.the_provider')}}</th>
                                                        <th>{{__('admin.branch')}}</th>
                                                        <th>{{__('admin.phone')}}</th>
                                                        <th>{{__('admin.request_arrival_time')}}</th>
                                                        <th>{{__('admin.level')}}</th>
{{--                                                        <th>{{__('admin.the_area')}}</th>--}}
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $branch)
                                                        <tr style="{{$branch->background_color_row}}" class="@if($branch->is_approved == 3) text-line-td @endif">
                                                            <td hidden>{{$branch->updated_at}}</td>
                                                            <td>{{$branch->id}}</td>
                                                            <td>{{$branch->provider->name ?? null}}</td>
                                                            <td>{{$branch->name}}</td>
                                                            <td>{{$branch->phone}}</td>
                                                            <td>{{$branch->request_arrival_time}}</td>
                                                            <td>
                                                                <button id="{{$branch->id}}" data-no="{{$branch->level}}" @can('update branches') title="Double click to change" @else disabled @endcan
                                                                class="btn btn-sm btn-primary font-default change-level">
                                                                    {{$branch->level}}
                                                                </button>
                                                            </td>
{{--                                                            <td>{{$branch->area->name ?? null}}</td>--}}
                                                            <td>
                                                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                                                    @if($branch->status == 1)
                                                                        <button @can('update branches') @else disabled @endcan class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-unlock icon-left"></i> {{__('admin.active')}}
                                                                        </button>
                                                                    @else
                                                                        <button @can('update branches') @else disabled @endcan class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}
                                                                        </button>
                                                                    @endif
                                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$branch->id}}" data-status="active">{{__('admin.active')}}</button>
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$branch->id}}" data-status="attitude">{{__('admin.attitude')}}</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>{!! $branch->actions !!}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    {{--model show modal--}}
    @include('managements.providers_management.branches.show')
@endsection
@section('script')
    @include('managements.providers_management.branches.show_js')
    <script>
        @can('update branches')
        let level;
        $(document).on('dblclick', '.change-level', async function () {
            level = $(this);
            level.attr('style', '');
            let level_no = level.attr('data-no');
            level.html('<input type="number" class="change-value" value="' + level_no + '">');
        });

        $(document).on('keypress', '.change-value', function (e) {
            let level_value = $(this).val();
            let id = level.attr('id');
            let route = "{{url('providers_management/branches/change-level')}}" + "/" + id;
            if (e.which == 13) {
                $(this).attr('disabled', true);
                $.ajax({
                    type: 'POST',
                    data: {
                        _token: $('input[name ="_token"]').val(),
                        level: level_value,
                    },
                    url: route,
                    dataType: "json",
                    success: function (data) {
                        level.attr('style', '');
                        level.text(level_value).attr('id', level_value);
                        level.attr('data-no', level_value)
                    },
                    error: function (data) {
                        level.attr('style', 'background: #c30015 !important');
                    }
                })
            }
        });

        $(document).on('click', '.accept', async function () {
            let deposit_id = $(this).attr('id');
            let deposit_row = $(this).parent().parent().parent();
            try {
                let data = await
                    statusChange(
                        deposit_id,
                        "{{route('branches.accept')}}",
                        "{{__('admin.do_you_want_to_accept')}}!",
                        "",
                        "warning",
                    );
                deposit_row.attr('style', data.data.background_color_row);
                deposit_row.find('td:nth-last-child(1)').html(data.data.actions);
                window.checkNotify();
                window.getNotify();
            } catch (e) {
                return e;
            }
        });

        $(document).on('click', '.reject', async function () {
            let deposit_id = $(this).attr('id');
            let deposit_row = $(this).parent().parent().parent();
            try {
                let data = await
                    statusChange(
                        deposit_id,
                        "{{route('branches.reject')}}",
                        "{{__('admin.do_you_want_to_reject')}}!",
                        "",
                        "warning",
                    );
                deposit_row.attr('style', data.data.background_color_row);
                deposit_row.find('td:nth-last-child(1)').html(data.data.actions);
                deposit_row.addClass('text-line-td');
                window.checkNotify();
                window.getNotify();
            } catch (e) {
                return e;
            }
        });

        $(document).on('click', '.statusClick', function () {
            let td = $(this).parent().parent().parent();

            let status = $(this).attr('data-status');
            let id_row = $(this).attr('data-id-row');
            let route = "{{url('providers_management/branches/update')}}" + "/" + id_row;
            $.ajax({
                type: 'POST',
                data: {
                    _token: $('input[name ="_token"]').val(),
                    status: status,
                },
                url: route,
                dataType: "json",
                success: function (data) {
                    let status_append = statusAppend(data.id, data.status);
                    td.empty().html(status_append);
                    td.parent().attr('style', data.background_color_row);
                },
            });
        });

        @endcan

        function statusAppend(id, status) {

            let status_append = '<div class="btn-group" role="group" aria-label="Button group with nested dropdown">';
            if (status == 1) {
                status_append +=
                    '<button class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                    '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                    '        aria-expanded="false">' +
                    '    <i class="ft-unlock icon-left"></i> {{__('admin.active')}}' +
                    '</button>';
            } else {
                status_append +=
                    '<button class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                    '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                    '        aria-expanded="false">' +
                    '    <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}' +
                    '</button>';
            }

            status_append +=
                '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
                '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="active">{{__('admin.active')}}</button>' +
                '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="attitude">{{__('admin.attitude')}}</button>' +
                '    </div>' +
                '</div>';

            return status_append;
        }
    </script>
@endsection
