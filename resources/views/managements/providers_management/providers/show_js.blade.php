<script>
    /*start code show details ajax*/
    let provider_id;
    $(document).on('click', '.show-detail-provider', async function () {
        provider_id = $(this).attr('id');
        let url = "{{url('providers_management/providers/show')}}" + '/' + provider_id;

        try {
            let data = await responseEditOrShowData(url);
            if (data.status == 1) {
                $('#show_status').html('<i class="la la-unlock-alt color-primary"></i> {{__('admin.active')}}');
            } else {
                $('#show_status').html(' <button class="btn btn-outline-warning btn-sm">{{__('admin.attitude')}}</button>');
            }
            $('#Gender').text(data.gender);
            $('.form-details .name').text(data.name);
            $('.form-details .email').text(data.email);
            $('.form-details .phone').text(data.phone);
            $('#id').text(data.id);
            $('#type').text(data.type);
            $('#telephone').text(data.telephone);
            $('#show_status').text(data.show_status);
            $('.created').text(data.created_at);
            $('.updated_at').html(data.updated_at);
            $('#image_show').attr('src', data.image);
            $('#ModalShowProvider').modal('show');
        } catch (error) {
            return error;
        }
    });
    /*end code show details ajax*/
</script>
