@php($page_title = __('admin.providers'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.providers_management')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a></li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.providers')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('providers')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number"
                                                                            @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="name" @if($search_type == 'name') selected @endif>{{__('admin.name')}}</option>
                                                                    <option value="phone"
                                                                            @if($search_type == 'phone') selected @endif>{{__('admin.phone')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th>{{__('admin.phone')}}</th>
                                                        <th>{{__('admin.type')}}</th>
                                                        <th>{{__('admin.level')}}</th>
                                                        {{--                                                        <th>{{__('admin.commercial_register')}}</th>--}}
                                                        <th>{{__('admin.image')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.provider_available')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $provider)
                                                        <tr style="{{$provider->background_color_row}}" class="@if($provider->is_approved == 3) text-line-td @endif">
                                                            <td hidden>{{$provider->updated_at}}</td>
                                                            <td>{{$provider->id}}</td>
                                                            <td>{{$provider->name}}</td>
                                                            <td>{{$provider->phone}}</td>
                                                            <td>{{$provider->type_name}}</td>
                                                            {{-- <td>{{$provider->log_no}}</td>--}}
                                                            <td>
                                                                <button id="{{$provider->id}}" data-no="{{$provider->level}}" @can('update provider') title="Double click to change" @else disabled @endcan
                                                                class="btn btn-sm btn-primary font-default change-level">
                                                                    {{$provider->level}}
                                                                </button>
                                                            </td>
                                                            <td>
                                                                @if ($provider->image != null)
                                                                    <img style='max-width:40px' class='rounded-circle img-user-show-40'
                                                                         src='{{ asset('storage').'/' . $provider->image}}'
                                                                         onerror="this.src='{{asset('storage/admins/64').'/avatar.jpg'}}'"
                                                                    />
                                                                @else
                                                                    <img style='max-width:40px' class='rounded-circle img-user-show-40'
                                                                         src='{{ url('/').'/storage/admins/64/avatar.jpg' }}'/>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                                                    @if($provider->status == 1)
                                                                        <button @can('update provider') @else disabled @endcan class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-unlock icon-left"></i> {{__('admin.active')}}
                                                                        </button>
                                                                    @else
                                                                        <button @can('update provider') @else disabled @endcan class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}
                                                                        </button>
                                                                    @endif
                                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$provider->id}}" data-status="active">{{__('admin.active')}}</button>
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$provider->id}}" data-status="attitude">{{__('admin.attitude')}}</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                                                    @if($provider->available == 1)
                                                                        <button @can('update provider') @else disabled @endcan class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-unlock icon-left"></i> {{__('admin.active')}}
                                                                        </button>
                                                                    @else
                                                                        <button @can('update provider') @else disabled @endcan class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}
                                                                        </button>
                                                                    @endif
                                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                                        <button class="dropdown-item availableClick" data-id-row="{{$provider->id}}" data-available="active">{{__('admin.active')}}</button>
                                                                        <button class="dropdown-item availableClick" data-id-row="{{$provider->id}}" data-available="attitude">{{__('admin.attitude')}}</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>{!! $provider->actions !!}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    {{--model show modal--}}
    @include('managements.providers_management.providers.show')
@endsection
@section('script')
    @include('managements.providers_management.providers.show_js')
    <script>
            @can('update provider')
        let level;
        $(document).on('dblclick', '.change-level', async function () {
            level = $(this);
            level.attr('style', '');
            let level_no = level.attr('data-no');
            level.html('<input type="number" class="change-value" value="' + level_no + '">');
        });

        $(document).on('keypress', '.change-value', function (e) {
            let level_value = $(this).val();
            let id = level.attr('id');
            let route = "{{url('providers_management/providers/change-level')}}" + "/" + id;
            if (e.which == 13) {
                $(this).attr('disabled', true);
                $.ajax({
                    type: 'POST',
                    data: {
                        _token: $('input[name ="_token"]').val(),
                        level: level_value,
                    },
                    url: route,
                    dataType: "json",
                    success: function (data) {
                        level.attr('style', '');
                        level.text(level_value).attr('id', level_value);
                        level.attr('data-no', level_value)
                    },
                    error: function (data) {
                        level.attr('style', 'background: #c30015 !important');
                    }
                })
            }
        });

        $(document).on('click', '.accept', async function () {
            let deposit_id = $(this).attr('id');
            let deposit_row = $(this).parent().parent().parent();
            try {
                let data = await
                    statusChange(
                        deposit_id,
                        "{{route('providers.accept')}}",
                        "{{__('admin.do_you_want_to_accept')}}!",
                        "",
                        "warning",
                    );
                deposit_row.attr('style', data.data.background_color_row);
                deposit_row.find('td:nth-last-child(1)').html(data.data.actions);

            } catch (e) {
                return e;
            }
        });

        $(document).on('click', '.reject', async function () {
            let deposit_id = $(this).attr('id');
            let deposit_row = $(this).parent().parent().parent();
            try {
                let data = await
                    statusChange(
                        deposit_id,
                        "{{route('providers.reject')}}",
                        "{{__('admin.do_you_want_to_reject')}}!",
                        "",
                        "warning",
                    );
                deposit_row.attr('style', data.data.background_color_row);
                deposit_row.find('td:nth-last-child(1)').html(data.data.actions);
                deposit_row.addClass('text-line-td');
            } catch (e) {
                return e;
            }
        });

        $(document).on('click', '.statusClick', function () {
            let td = $(this).parent().parent().parent();

            let status = $(this).attr('data-status');
            let id_row = $(this).attr('data-id-row');
            let route = "{{url('providers_management/providers/update')}}" + "/" + id_row;
            $.ajax({
                type: 'POST',
                data: {
                    _token: $('input[name ="_token"]').val(),
                    status: status,
                },
                url: route,
                dataType: "json",
                success: function (data) {
                    let status_append = statusAppend(data.id, data.status);
                    td.empty().html(status_append);
                    td.parent().attr('style', data.background_color_row);
                },
            });
        });
        $(document).on('click', '.availableClick', function () {
            let td = $(this).parent().parent().parent();

            let available = $(this).attr('data-available');
            let id_row = $(this).attr('data-id-row');
            let route = "{{url('providers_management/providers/update_available')}}" + "/" + id_row;
            $.ajax({
                type: 'POST',
                data: {
                    _token: $('input[name ="_token"]').val(),
                    available: available,
                },
                url: route,
                dataType: "json",
                success: function (data) {
                    let available_append = availableAppend(data.id, data.available);
                    td.empty().html(available_append);
                    td.parent().attr('style', data.background_color_row);
                },
            });
        });

        @endcan

        function statusAppend(id, status) {

            let status_append = '<div class="btn-group" role="group" aria-label="Button group with nested dropdown">';
            if (status == 1) {
                status_append +=
                    '<button class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                    '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                    '        aria-expanded="false">' +
                    '    <i class="ft-unlock icon-left"></i> {{__('admin.active')}}' +
                    '</button>';
            } else {
                status_append +=
                    '<button class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                    '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                    '        aria-expanded="false">' +
                    '    <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}' +
                    '</button>';
            }

            status_append +=
                '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
                '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="active">{{__('admin.active')}}</button>' +
                '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="attitude">{{__('admin.attitude')}}</button>' +
                '    </div>' +
                '</div>';

            return status_append;
        }
        function availableAppend(id, available) {

            let available_append = '<div class="btn-group" role="group" aria-label="Button group with nested dropdown">';
            if (available == 1) {
                available_append +=
                    '<button class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                    '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                    '        aria-expanded="false">' +
                    '    <i class="ft-unlock icon-left"></i> {{__('admin.active')}}' +
                    '</button>';
            } else {
                available_append +=
                    '<button class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                    '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                    '        aria-expanded="false">' +
                    '    <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}' +
                    '</button>';
            }

            available_append +=
                '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
                '        <button class="dropdown-item availableClick" data-id-row="' + id + '" data-available="active">{{__('admin.active')}}</button>' +
                '        <button class="dropdown-item availableClick" data-id-row="' + id + '" data-available="attitude">{{__('admin.attitude')}}</button>' +
                '    </div>' +
                '</div>';

            return available_append;
        }
    </script>
@endsection
