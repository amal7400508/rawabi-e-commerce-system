@php($page_title = __('admin.carrier_report'))
@extends('layouts.main')
@section('content')
    <style>
        .progress-primary {
            background-color: rgb(0, 37, 129);
        }
    </style>
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.delivery_driver')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/')}}"><i class="la la-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{url('carrier')}}">{{__('admin.delivery_driver')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin.carrier_report')}}</li>
                        </ol>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.create')}} {{__('admin.carrier_report')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <form action="{{route('carriers.export')}}" method="get">
                                                <div class="form-body">
                                                    <div class="col-md-6" style="float: right">
                                                        <div class="col-md-6"
                                                             style="@if(app()->getLocale() == 'en') padding-left: 0; float: left; @else padding-right: 0; float: right; @endif">
                                                            <div class="form-group">
                                                                <label>{{__('admin.from')}}</label>
                                                                <input type="date" class="form-control"
                                                                       name="from">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6"
                                                             style=" padding: 0; @if(app()->getLocale() == 'en') float: left @else  float: right @endif">
                                                            <div class="form-group">
                                                                <label>{{__('admin.to')}}</label>
                                                                <input type="date" class="form-control" name="to">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>{{__('admin.name')}}</label>
                                                            <input type="text" class="form-control" name="name">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>{{__('admin.email')}}</label>
                                                            <input type="email" class="form-control" name="email">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="float: left">
                                                        <div class="col-md-6"
                                                             style="@if(app()->getLocale() == 'en') padding-left: 0; float: left; @else padding-right: 0; float: right; @endif">
                                                            <div class="form-group">
                                                                <label>{{__('admin.phone')}}</label>
                                                                <input type="number" class="form-control"
                                                                       name="phone"/>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6"
                                                             style=" padding: 0; @if(app()->getLocale() == 'en') float: left @else  float: right @endif">
                                                            <div class="form-group">
                                                                <label>{{__('admin.gender')}}</label>
                                                                <select class="form-control" name="gender">
                                                                    <option value="">{{__('admin.select_option')}}</option>
                                                                    <option value="man">{{__('admin.man')}}</option>
                                                                    <option value="woman">{{__('admin.woman')}}</option>
                                                                </select></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>{{__('admin.status')}}</label>
                                                            <select class="form-control" name="status">
                                                                <option value="">{{__('admin.select_option')}}</option>
                                                                <option value="1">{{__('admin.active')}}</option>
                                                                <option value="0">{{__('admin.attitude')}}</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6"
                                                             style="@if(app()->getLocale() == 'en') padding-left: 0; float: left; @else padding-right: 0; float: right; @endif">
                                                            <div class="form-group">
                                                                <label>{{__('admin.from') .': '. __('admin.date_1') .' '. __('admin.Evaluation')}}</label>
                                                                <input type="date" class="form-control" name="from_date_evaluation">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6"
                                                             style=" padding: 0; @if(app()->getLocale() == 'en') float: left @else  float: right @endif">
                                                            <div class="form-group">
                                                                <label>{{__('admin.to') .': '. __('admin.date_1') .' '. __('admin.Evaluation')}}</label>
                                                                <input type="date" class="form-control" name="to_date_evaluation">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions pull-right">
                                                    <button type="button" class=" btn btn-primary" id="button_preview"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-eye"></i> {{__('admin.preview')}}
                                                    </button>
                                                    <button type="submit" class=" btn btn-primary" id="button_export"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-printer"></i> {{__('admin.export')}}
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.post_index')}} {{__('admin.delivery_driver')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload" id="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <table width="100%"
                                                       class="table  zero-configuration"
                                                       id="data_table">
                                                    <thead>
                                                    <tr>
                                                        <th hidden>{{__('admin.updated_at')}}</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th>{{__('admin.phone')}}</th>
                                                        <th>{{__('admin.requests_number')}}</th>
                                                        <th>{{__('admin.Evaluation')}}</th>
                                                        <th width="100">{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

    {{-- start model show  Message--}}
    <div id="confirmModalShow" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="  modal-content">
                <div class="card-content">
                    <div class="card-header" style="padding: 13px 15px;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                        </button>
                        <h3>{{__('admin.post_index')}} {{__('admin.delivery_driver')}}</h3>
                    </div>
                    <table class="table table-striped table-responsive table-custom-responsive">
                        <tr>
                            <th>{{__('admin.id')}}</th>
                            <td id="id"></td>
                        </tr>
                        <tr>
                            <th> {{__('admin.email')}}</th>
                            <td id="email"></td>
                        </tr>
                        <tr>
                            <th> {{__('admin.gender')}}</th>
                            <td id="gender"></td>
                        </tr>
                        <tr>
                            <th> {{__('admin.phone')}}</th>
                            <td id="phone"></td>
                        </tr>
                        <tr>
                            <th> {{__('admin.status')}}</th>
                            <td id="status"></td>
                        </tr>
                        <tr>
                            <th style="max-width: 140px;"> {{__('admin.requests_number')}}</th>
                            <td id="requests_number"></td>
                        </tr>
                        <tr>
                            <th> {{__('admin.Evaluation')}}</th>
                            <td id="evaluation"></td>
                        </tr>
                    </table>
                    <div>
                        <div class="row p-1" style="padding-top: 1px !important">
                            <div class="col-sm-9">
                                <small class="price category-color">{{__('admin.created_at')}}:</small>
                                <small class="price category-color" id="created"></small>
                                <br>
                                <small class="price category-color">{{__('admin.updated_at')}}:</small>
                                <small class="price category-color" id="updated_at"></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- end model show  Message--}}

@endsection
@section('script')
    <script>
        var confirm = 0;
        /*start code show all data in datatable ajax*/
        var data_table = $('#data_table');
        $(document).ready(function () {
            function dataTable() {
                data_table.DataTable({
                    @if(app()->getLocale() == 'ar')
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                    },
                    @endif
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{route('carriers.report')}}",
                        method: 'GET',
                        data: function (data) {
                            /* todo:: custom filter goes here*/
                            /* they are the parameters of the url*/
                            if (confirm === 1) {
                                data.confirm = confirm;
                                data.from = $('input[name=from]').val();
                                data.to = $('input[name=to]').val();
                                data.name = $('input[name=name]').val();
                                data.email = $('input[name=email]').val();
                                data.phone = $('input[name=phone]').val();
                                data.gender = $('select[name=gender]').val();
                                data.status = $('select[name=status]').val();
                                data.branch_id = $('select[name=branch_id]').val();
                                data.from_date_evaluation = $('input[name=from_date_evaluation]').val();
                                data.to_date_evaluation = $('input[name=to_date_evaluation]').val();
                            }
                        },
                    },
                    columns: [
                        {
                            data: 'updated_at',
                            name: 'updated_at',
                            class: 'updated_at'
                        },
                        {
                            data: 'name',
                            name: 'name',
                            class: 'name'
                        },
                        {
                            data: 'phone',
                            name: 'phone',
                            class: 'phone',
                        },
                        {
                            data: 'number_requests',
                            name: 'number_requests',
                        },
                        {
                            data: 'rate',
                            name: 'rate',
                            render: function (data, type, full, meta) {
                                return evaluationStar(data);
                            },
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },
                    ],

                    'order': [[0, 'desc']],
                    "columnDefs": [
                        {
                            "targets": [0],
                            "visible": false,
                            "searchable": false
                        }
                    ]
                });
            }

            dataTable();

            $('#button_preview').click(function () {
                confirm = 1;
                data_table.DataTable().draw();
            });
        });

        /*end code show all data in datatable ajax*/
    </script>

    {{--start code show details ajax--}}
    <script>
        /*start code show details ajax*/
        var carrier_detals_id;
        $(document).on('click', '.showB', function () {
            carrier_detals_id = $(this).attr('id');
            let detail_url = "{{url('carrier_management/carriers/show')}}" + "/" + carrier_detals_id;
            let data = {
                from_date_evaluation: $('input[name=from_date_evaluation]').val(),
                to_date_evaluation: $('input[name=to_date_evaluation]').val(),
            };

            $.ajax({
                type: 'GET',
                url: detail_url,
                data: data,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $('#confirm-modal-loading-show').modal('hide');
                    $('#name').text(data.name);
                    $('#id').text(data.id);
                    $('#email').text(data.email);
                    $('#gender').text(data.gender);
                    $('#phone').text(data.phone);
                    $('#status').text(data.status);
                    $('#password').text(data.password);
                    $('#created').text(data.created_at);
                    $('#updated_at').html(data.updated_at);
                    $('#btn_dele').html(data.btn);
                    $('#requests_number').html(data.number_requests);

                    $('#evaluation').html(appendRate(data)).css('width', '380px');
                    $('#confirmModalShow').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });

        function appendRate(data) {
            let total = data.rate_total;
            let td =
                '    <div class="row">' +
                '        <div class="col-4">' +
                '            <h1 class="text-bold-600" style="font-size: 43px;margin-bottom: 0;">' + data.average_rate + '</h1>' + evaluationStar(data.average_rate) +
                '<small>' + 'عدد الطلبات المقيمة: ' + total + '</small>' +
                '        </div>' +
                '        <div class="col-8">' +
                '            <div class="row" style="height: 20px">' +
                '                <span style="margin: auto 16px">5</span>' +
                '                <div style="width: 75%;float: left;margin-top: 10px;">' +
                '                    <div class="progress" style="height: 10px;">' +
                '                        <div class="progress-bar progress-primary" role="progressbar" aria-valuenow="' + (data.rate_5 / total) * 100 + '" aria-valuemin="' + (data.rate_5 / total) * 100 + '" aria-valuemax="100" style="width:' + (data.rate_5 / total) * 100 + '%" aria-describedby="example-caption-2"></div>' +
                '                    </div>' +
                '                </div>' +
                '            </div>' +
                '            <div class="row" style="height: 20px">' +
                '                <span style="margin: auto 16px">4</span>' +
                '                <div style="width: 75%;float: left;margin-top: 10px;">' +
                '                    <div class="progress" style="height: 10px;">' +
                '                        <div class="progress-bar progress-primary" role="progressbar" aria-valuenow="' + (data.rate_4 / total) * 100 + '" aria-valuemin="' + (data.rate_4 / total) * 100 + '" aria-valuemax="100" style="width:' + (data.rate_4 / total) * 100 + '%" aria-describedby="example-caption-2"></div>' +
                '                    </div>' +
                '                </div>' +
                '            </div>' +
                '            <div class="row" style="height: 20px">' +
                '                <span style="margin: auto 16px">3</span>' +
                '                <div style="width: 75%;float: left;margin-top: 10px;">' +
                '                    <div class="progress" style="height: 10px;">' +
                '                        <div class="progress-bar progress-primary" role="progressbar" aria-valuenow="' + (data.rate_3 / total) * 100 + '" aria-valuemin="' + (data.rate_3 / total) * 100 + '" aria-valuemax="100" style="width:' + (data.rate_3 / total) * 100 + '%" aria-describedby="example-caption-2"></div>' +
                '                    </div>' +
                '                </div>' +
                '            </div>' +
                '            <div class="row" style="height: 20px">' +
                '                <span style="margin: auto 16px">2</span>' +
                '                <div style="width: 75%;float: left;margin-top: 10px;">' +
                '                    <div class="progress" style="height: 10px;">' +
                '                        <div class="progress-bar progress-primary" role="progressbar" aria-valuenow="' + (data.rate_2 / total) * 100 + '" aria-valuemin="' + (data.rate_2 / total) * 100 + '" aria-valuemax="100" style="width:' + (data.rate_2 / total) * 100 + '%" aria-describedby="example-caption-2"></div>' +
                '                    </div>' +
                '                </div>' +
                '            </div>' +
                '            <div class="row" style="height: 20px">' +
                '                <span style="margin: auto 16px">1</span>' +
                '                <div style="width: 75%;float: left;margin-top: 10px;">' +
                '                    <div class="progress" style="height: 10px;">' +
                '                        <div class="progress-bar progress-primary" role="progressbar" aria-valuenow="' + (data.rate_1 / total) * 100 + '" aria-valuemin="' + (data.rate_1 / total) * 100 + '" aria-valuemax="100" style="width:' + (data.rate_1 / total) * 100 + '%" aria-describedby="example-caption-2"></div>' +
                '                    </div>' +
                '                </div>' +
                '            </div>' +
                '        </div>' +
                '    </div>'
            ;

            return td + "<h6 style='margin-top: 10px'>عدد الطلبات التي تحتوي على 5 نجمات : " + data.rate_5 + "</h6>";

        }

        function evaluationStar(data) {
            let star_half = @if(app()->getLocale() == 'ar') "star-half-right-2.png";
            @else "star-half-2.png";
            @endif
            if (data == 5) {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '</div>';
            } else if (data < 5 && data >= 4.5) {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty')}}/' + star_half + '">' +
                    '</div>';
            } else if (data < 4.5 && data >= 4) {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '</div>';
            } else if (data < 4 && data >= 3.5) {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty')}}/' + star_half + '">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '</div>';
            } else if (data < 3.5 && data >= 3) {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '</div>';
            } else if (data < 3 && data >= 2.5) {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty')}}/' + star_half + '">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '</div>';
            } else if (data < 2.5 && data >= 2) {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '</div>';
            } else if (data < 2 && data >= 1.5) {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty')}}/' + star_half + '">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '</div>';
            } else if (data < 1.5 && data >= 1) {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty/star-on-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '</div>';
            } else if (data < 1 && data >= 0.5) {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty')}}/' + star_half + '">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '</div>';
            } else {
                return '<div class="fonticon-wrap">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '<img src="{{asset('app-assets/images/raty/star-off-2.png')}}">' +
                    '</div>';
            }
        }
    </script>


@endsection
