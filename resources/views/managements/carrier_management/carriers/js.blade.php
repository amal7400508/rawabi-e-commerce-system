<script>
    let table_show = $('#table-show');
    let table_count = $('#table-count');

    function delete_img() {
        document.getElementById('edit_image_ar').value = null;
        $('#avatar').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
    }

    function loadAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                var image = document.getElementById('avatar');
                image.src = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    @can('create carriers')
    $(document).on('click', '#openAddModal', function () {
        $('#form input:not(:first), #form textarea, #form select')
            .val('')
            .removeClass('is-invalid');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);
        delete_img();
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('carriers.store')}}")
            .attr("data_type", "add");

        $('#append-password').html(
            '<div class="form-group">' +
            '    <label for="password">{{__('admin.password')}}<span class="danger">*</span></label>' +
            '    <input type="password"' +
            '           id="password"' +
            '           class="form-control"' +
            '           name="password"' +
            '           autocomplete="password"' +
            '           required' +
            '    >' +
            '    <span class="error-message">' +
            '        <strong></strong>' +
            '    </span>' +
            '</div>');
        $('#addModal').modal('show');
    });
    @endcan
    /*start code show details ajax*/
    let carrier_details_id;
    $(document).on('click', '.show-detail', async function () {
        carrier_details_id = $(this).attr('id');
        let url = "{{url('carrier_management/carriers/show')}}" + '/' + carrier_details_id;

        try {
            let data = await responseEditOrShowData(url);
            if (data.status == 1) {
                $('#show_status').html('<i class="la la-unlock-alt color-primary"></i> {{__('admin.active')}}');
            } else {
                $('#show_status').html(' <button class="btn btn-outline-warning btn-sm">{{__('admin.attitude')}}</button>');
            }
            $('#Gender').text(data.gender);
            $('.form-details .name').text(data.name);
            $('.form-details .email').text(data.email);
            $('.form-details .phone').text(data.phone);
            $('#id').text(data.id);
            $('#created').text(data.created_at);
            $('#updated_at').html(data.updated_at);
            $('#image_show').attr('src', data.image);
            $('#confirmModalShow').modal('show');
        } catch (error) {
            return error;
        }
    });
    /*end code show details ajax*/

    @can('update carriers')
    /*start code edit*/
    let carrier_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        carrier_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('carrier_management/carriers/edit')}}" + '/' + carrier_id;
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#avatar').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('carrier_management/carriers/update')}}" + '/' + carrier_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);
            $('#append-password').empty();
            $('#edit_image_ar').attr('required', false);
            $('#label-image').text("{{__('admin.image')}}");
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            if (data.status == 1) {
                $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                    .bootstrapSwitch('state', true);
            } else {
                $('#switchBootstrap18').attr('value', 1).attr('checked', false)
                    .bootstrapSwitch('state', false);
            }

            document.getElementById('name').value = data.name;
            document.getElementById('phone').value = data.phone;
            document.getElementById('email').value = data.email;
            document.getElementById('gender').value = data.gender;

            if (data.image === null) {
                $('#avatar').attr('hidden', true);
                $('#delete-img').attr('hidden', true);

            } else {
                $('#avatar').attr('src', data.image).attr('hidden', false);
                $('#delete-img').attr('hidden', false);
            }

            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/
    @endcan

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('carrier_id', carrier_id);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                /*let response_data = JSON.parse((data.responseText)).carrier;*/
                let response_data = data.carrier;

                $('#addModal').modal('hide');

                let tr_color_red = response_data.class_color_row;
                let status;

                if (response_data.status == 1) {
                    status = "<div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>";
                } else {
                    tr_color_red = 'tr-color-red';
                    status = "<div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>";
                }

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td class='font-default'>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.name + "</td>");
                let col3 = $("<td>" + response_data.phone + "</td>");
                let col4 = $('<td><img class="rounded-circle img-user-show-40" src="' + response_data.image_path + '"' +
                    ' onerror=this.src="{{asset('storage')}}/default-carrier.jpg"/></td>'
                );
                let col5 = $("<td>" + status + "</td>");
                let col6 = $("<td>" + response_data.actions + "</td>");

                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5, col6).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5, col6);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.name !== undefined) {
                $('#form input[name=name]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.name);
            }
            if (obj.phone !== undefined) {
                $('#form input[name=phone]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.phone);
            }
            if (obj.image !== undefined) {
                $('#form input[name=image]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.image);
            }
            if (obj.email !== undefined) {
                $('#form input[name=email]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.email);
            }
            if (obj.branch !== undefined) {
                $('#form select[name=branch]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.branch);
            }
            if (obj.gender !== undefined) {
                $('#form select[name=gender]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.gender);
            }
            if (obj.password !== undefined) {
                $('#form input[name=password]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.password);
            }
            if (obj.confirm_password !== undefined) {
                $('#form input[name=confirm_password]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.confirm_password);
            }
        }
    });
    /*end code add or update*/

    @can('delete carriers')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('carrier_management/carriers/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            this_row.remove();
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan
</script>
