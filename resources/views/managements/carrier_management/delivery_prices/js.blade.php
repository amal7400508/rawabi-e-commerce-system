<script>

    let table_show = $('#table-show');
    let table_count = $('#table-count');

    $("select[name=table_length]").click(function () {
        let open = $(this).data("is_open");
        if (open) {
            window.location.href = $(this).val()
        }
        $(this).data("is_open", !open);
    });

    @can('create delivery_prices')
    $(document).on('click', '#openAddModal', function () {
        $('#form input:not(:first), #form textarea, #form select').val('')
            .removeClass('is-invalid');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);

        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('delivery_prices.store')}}")
            .attr("data_type", "add");
    });
    @endcan

    @can('update delivery_prices')
    /*start code edit*/
    let delivery_price_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        delivery_price_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('carrier_management/delivery_prices/edit')}}" + '/' + delivery_price_id;
        $('#form input:not(:first), #form textarea, #from select')
            .val('')
            .removeClass('is-invalid');

        $('#avatar').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('carrier_management/delivery_prices/update')}}" + '/' + delivery_price_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);

            $('#edit_image_ar').attr('required', false);
            $('#label-image').text("{{__('admin.image')}}");
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            document.getElementById('from').value = data.from;
            document.getElementById('to').value = data.to;
            document.getElementById('price').value = data.price;
            document.getElementById('branch_count').value = data.branch_count;
            document.getElementById('type').value = data.type;

            if (data.status == 1) {
                $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                    .bootstrapSwitch('state', true);
            } else {
                $('#switchBootstrap18').attr('value', 1).attr('checked', false)
                    .bootstrapSwitch('state', false);
            }

            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/
    @endcan

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('delivery_price_id', delivery_price_id);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                let response_data = data.area;

                $('#addModal').modal('hide');

                let status, tr_color_red = '';
                if (response_data.status == 1) {
                    status = "<div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>";
                } else {
                    tr_color_red = 'tr-color-red';
                    status = "<div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>";
                }

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.area_from_name + "</td>");
                let col3 = $("<td>" + response_data.area_to_name + "</td>");
                let col4 = $("<td>" + response_data.type + "</td>");
                let col5 = $("<td>" + response_data.price + "</td>");
                let col6 = $("<td>" + response_data.branch_count + "</td>");
                let col7 = $("<td>" + status + "</td>");
                let col8 = $("<td>" + response_data.actions + "</td>");

                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5, col6, col7, col8).prependTo("#table");
                    this_row = $('#row_' + response_data.id);

                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5, col6, col7, col8);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.from !== undefined) {
                $('#form input[name=from]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.from);
            }
            if (obj.to !== undefined) {
                console.log(obj.to);
                $('#to').addClass('is-invalid').parent().find('span strong').text(obj.to);
            }
            if (obj.price !== undefined) {
                $('#form input[name=price]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.price);
            }
            if (obj.branch_count !== undefined) {
                $('#form input[name=branch_count]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.branch_count);
            }
            if (obj.type !== undefined) {
                $('#form select[name=type]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.type);
            }
        }
    });
    /*end code add or update*/

    @can('delete delivery_prices')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('carrier_management/delivery_prices/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
            this_row.remove();
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan
</script>
