@php($page_title = __('admin.delivery_prices'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.carrier_management')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.delivery_prices')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create delivery_prices')
                                            <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.delivery_prices')}}</a>
                                        @endcan
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('delivery_prices')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number"
                                                                            @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="price"
                                                                            @if($search_type == 'price') selected @endif>{{__('admin.price')}}</option>
                                                                    <option value="from" @if($search_type == 'from') selected @endif>{{__('admin.from')}}</option>
                                                                    <option value="to" @if($search_type == 'to') selected @endif>{{__('admin.to')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.from')}}</th>
                                                        <th>{{__('admin.to')}}</th>
                                                        <th>{{__('admin.type')}}</th>
                                                        <th>{{__('admin.price')}}</th>
                                                        <th>{{__('admin.branches_number')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        @canany(['update delivery_prices', 'delete delivery_prices'])
                                                            <th>{{__('admin.action')}}</th>
                                                        @endcan
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $delivery_price)
                                                        <tr style="{{$delivery_price->background_color_row}}">
                                                            <td hidden>{{$delivery_price->updated_at}}</td>
                                                            <td class="font-default">{{$delivery_price->id}}</td>
                                                            <td>{{$delivery_price->areaFrom->name}}</td>
                                                            <td>{{$delivery_price->areaTo->name}}</td>
                                                            <td>{{$delivery_price->type}}</td>
                                                            <td>{{$delivery_price->price}}</td>
                                                            <td>{{$delivery_price->branch_count}}</td>
                                                            <td>
                                                                @if($delivery_price->status == 1)
                                                                    <div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>
                                                                @else
                                                                    <div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>
                                                                @endif
                                                            </td>
                                                            @canany(['update delivery_prices', 'delete delivery_prices'])
                                                                <td>
                                                                    {!! $delivery_price->actions !!}
                                                                </td>
                                                            @endcanany
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.delivery_prices')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements" id="check_show">
                                <input type="checkbox" name="status" value="1"
                                       class="switchBootstrap" id="switchBootstrap18"
                                       data-on-color="primary" data-off-color="danger"
                                       data-on-text="{{__('admin.enable')}}"
                                       data-off-text="{{__('admin.disable')}}" data-color="primary"
                                       data-label-text="{{__('admin.status')}}"
                                       checked/>
                            </div>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12 p-0">
                                            <row>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="from">{{__('admin.from')}}<span class="danger">*</span></label>
                                                        <select class="form-control" id="from" required name="from">
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($areas as $area)
                                                                <option value="{{$area->id}}">{{$area->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="to">{{__('admin.to')}}<span class="danger">*</span></label>
                                                        <select class="form-control" id="to" required name="to">
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($areas as $area)
                                                                <option value="{{$area->id}}">{{$area->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="type">{{__('admin.type')}}<span class="danger">*</span></label>
                                                        <select class="form-control" id="type" required name="type">
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            <option value="متر">متر</option>
                                                            <option value="تاكسي">تاكسي</option>
                                                            <option value="نقل">نقل</option>

                                                        </select>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="price">{{__('admin.price')}}<span class="danger">*</span></label>
                                                        <input type="number" id="price"
                                                               class="form-control"
                                                               name="price"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="branch_count">{{__('admin.branches_number')}}</label>
                                                        <input type="number" id="branch_count"
                                                               class="form-control"
                                                               name="branch_count"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                            </row>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>


@endsection
@section('script')
    @include('managements.carrier_management.delivery_prices.js')
@endsection
