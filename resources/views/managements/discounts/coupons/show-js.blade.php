<script>
    $(document).on('click', '.show-detail', async function () {
        let id = $(this).attr('id');
        let url = "{{url('discounts/coupons/show')}}" + "/" + id;
        try {
            let data_response = await responseEditOrShowData(url);
            $('#td_id').text(data_response.id);
            $('#td_coupon_type').text(data_response.coupon_type);
            $('#td_number').text(data_response.number);
            $('#td_price').text(data_response.price_with_type);
            $('#td_start_date').text(data_response.start_date);
            $('#td_end_date').text(data_response.end_date);
            $('#td_branch').text(data_response.branch_name);

            if (data_response.status == 1) {
                $('#td_status').html('<i class="la la-unlock-alt color-primary"></i> {{__('admin.active')}}');
            } else {
                $('#td_status').html(' <button class="btn btn-outline-warning btn-sm">{{__('admin.attitude')}}</button>');
            }

            $('#created').text(data_response.created_at);
            $('#updated_at').html(data_response.updated_at);

            $('#confirmModalShow').modal('show');

        } catch (e) {
            return e;
        }
    });
</script>
