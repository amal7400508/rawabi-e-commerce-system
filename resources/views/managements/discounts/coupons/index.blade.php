@php($page_title = __('admin.coupons'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.discounts')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.coupons')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create coupons')
                                            <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.coupon')}}</a>
                                        @endcan
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('coupons')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="coupon_type" @if($search_type == 'coupon_type') selected @endif>{{__('admin.coupon_type')}}</option>
                                                                    <option value="coupon" @if($search_type == 'coupon') selected @endif>{{__('admin.coupon')}}</option>
                                                                    <option value="price" @if($search_type == 'price') selected @endif>{{__('admin.price')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.coupon_type')}}</th>
                                                        <th>{{__('admin.coupon')}}</th>
                                                        <th>{{__('admin.price')}}</th>
                                                        <th>{{__('admin.start_date')}}</th>
                                                        <th>{{__('admin.end_date')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $coupon)
                                                        <tr style="{{$coupon->background_color_row}}">
                                                            <td hidden>{{$coupon->updated_at}}</td>
                                                            <td class="font-default">{{$coupon->id}}</td>
                                                            <td>{{$coupon->couponType->name ?? null}}</td>
                                                            <td>{{$coupon->number}}</td>
                                                            <td>{{$coupon->price_with_type}}</td>
                                                            <td>{{$coupon->start_date}}</td>
                                                            <td>{{$coupon->end_date}}</td>
                                                            <td>
                                                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                                                    @if($coupon->status == 1)
                                                                        <button @can('update coupons') @else disabled @endcan class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-unlock icon-left"></i> {{__('admin.active')}}
                                                                        </button>
                                                                    @else
                                                                        <button @can('update coupons') @else disabled @endcan class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}
                                                                        </button>
                                                                    @endif
                                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$coupon->id}}" data-status="active">{{__('admin.active')}}</button>
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$coupon->id}}" data-status="attitude">{{__('admin.attitude')}}</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                {!! $coupon->actions !!}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.coupon')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements" id="check_show">
                                <input type="checkbox" name="status" value="1"
                                       class="switchBootstrap" id="switchBootstrap18"
                                       data-on-color="primary" data-off-color="danger"
                                       data-on-text="{{__('admin.enable')}}"
                                       data-off-text="{{__('admin.disable')}}" data-color="primary"
                                       data-label-text="{{__('admin.status')}}"
                                       checked/>
                            </div>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="coupon_type">{{__('admin.coupon_type')}}<span class="danger">*</span></label>
                                                        <select id="coupon_type" class="form-control" name="coupon_type" required>
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($coupon_types as $coupon_type)
                                                                <option value="{{$coupon_type->id}}">{{$coupon_type->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="number">{{__('admin.number')}}<span class="danger">*</span></label>
                                                        <input type="text" id="number"
                                                               class="form-control"
                                                               name="number"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                        <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="value_type">{{__('admin.value_discounts')}}<span class="danger">*</span></label>
                                                        <select class="form-control"
                                                                required id="value_type" name="value_type">
                                                            <option value="price">{{__('admin.a_price')}}</option>
                                                            <option value="percentage">{{__('admin.a_percentage')}}</option>
                                                        </select>
                                                        <span class="error-message">
                                                                <strong></strong>
                                                            </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="price">{{__('admin.price')}}<span class="danger">*</span></label>
                                                        <input type="number" id="price" required class="form-control" name="price">
                                                        <span class="error-message">
                                                                <strong></strong>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="start_date">{{__('admin.start_date')}}<span class="danger">*</span></label>
                                                        <input type="date" id="start_date"
                                                               class="form-control"
                                                               name="start_date"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="end_date">{{__('admin.end_date')}}</label>
                                                        <input type="date" id="end_date"
                                                               class="form-control"
                                                               name="end_date"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    @include('managements.discounts.coupons.show')
@endsection
@section('script')
    @include('managements.discounts.coupons.js')
    @include('managements.discounts.coupons.show-js')
@endsection
