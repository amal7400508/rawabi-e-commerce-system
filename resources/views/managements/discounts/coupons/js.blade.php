<script>

    let table_show = $('#table-show');
    let table_count = $('#table-count');

    $("select[name=table_length]").click(function () {
        let open = $(this).data("is_open");
        if (open) {
            window.location.href = $(this).val()
        }
        $(this).data("is_open", !open);
    });

    @can('create coupons')
    $(document).on('click', '#openAddModal', function () {
        $('#form input:not(:first), #form textarea, #from select').val('')
            .removeClass('is-invalid');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);

        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('coupons.store')}}")
            .attr("data_type", "add");
    });
    @endcan


    @can('update coupons')
    /*start code edit*/
    let area_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        area_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('discounts/coupons/edit')}}" + '/' + area_id;
        $('#form input:not(:first), #form textarea, #from select')
            .val('')
            .removeClass('is-invalid');

        $('#avatar').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('discounts/coupons/update')}}" + '/' + area_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            document.getElementById('name').value = data.name;
            document.getElementById('note').value = data.note;

            if (data.status == 1) {
                $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                    .bootstrapSwitch('state', true);
            } else {
                $('#switchBootstrap18').attr('value', 1).attr('checked', false)
                    .bootstrapSwitch('state', false);
            }

            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/
    @endcan

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
            event.preventDefault();
            let btn_save = $('#btn-save');
            let url = btn_save.attr('data_url');
            let type = btn_save.attr('data_type');
            let data_form = new FormData(this);
            data_form.append('area_id', area_id);

            try {
                let data = await addOrUpdate(url, data_form, type, 'btn-save');
                if (data['status'] == 200) {
                    let response_data = data.coupon;
                    let status, tr_color_red = '';

                    $('#addModal').modal('hide');

                    let status_append = statusAppend(response_data.id, response_data.status);

                    /*if (response_data.status == 1) {

                        status = "<div class='fonticon-wrap'><i class='ft-unlock color-primary'></i></div>";
                    } else {
                        tr_color_red = 'tr-color-red';
                        status = "<div class='fonticon-wrap'><i class='ft-lock color-red'></i></div>";
                    }*/

                    let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                    let col1 = $("<td>" + response_data.id + "</td>");
                    let col2 = $("<td>" + response_data.coupon_type_name + "</td>");
                    let col3 = $("<td>" + response_data.number + "</td>");
                    let col4 = $("<td>" + response_data.price_with_type + "</td>");
                    let col5 = $("<td>" + response_data.start_date + "</td>");
                    let col6 = $("<td>" + response_data.end_date + "</td>");
                    let col7 = $("<td>" + status_append + "</td>");
                    let col8 = $("<td>" + response_data.actions + "</td>");

                    let this_row;
                    if (type === 'add') {
                        row.append(col1, col2, col3, col4, col5, col6, col7, col8).prependTo("#table");
                        this_row = $('#row_' + response_data.id);

                        table_show.text(parseInt(table_show.text()) + 1);
                        table_count.text(parseInt(table_count.text()) + 1);
                    } else {
                        this_row = edit_row;
                        edit_row.attr("class", tr_color_red).attr('style', '');
                        edit_row.empty().append(col1, col2, col3, col4, col5, col6, col7, col8);
                    }

                    this_row.addClass('tr-color-success');
                    setTimeout(function () {
                        this_row.removeClass('tr-color-success');
                    }, 7000);
                }
            } catch
                (error) {
                let obj = JSON.parse((error.responseText)).error;
                if (obj.coupon_type !== undefined) {
                    $('#form select[name=coupon_type]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.coupon_type);
                }
                if (obj.number !== undefined) {
                    $('#form input[name=number]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.number);
                }
                if (obj.value_type !== undefined) {
                    $('#form select[name=value_type]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.value_type);
                }
                if (obj.price !== undefined) {
                    $('#form input[name=price]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.price);
                }
                if (obj.start_date !== undefined) {
                    $('#form input[name=start_date]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.start_date);
                }
                if (obj.end_date !== undefined) {
                    $('#form input[name=end_date]').addClass('is-invalid')
                        .parent().find('span strong').text(obj.end_date);
                }


            }
        }
    );
    /*end code add or update*/

    @can('delete coupons')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('discounts/coupons/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
            this_row.remove();
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan

    $(document).on('change', '#value_type', function () {
        let val = $(this).val();
        if (val === 'percentage')
            $('#price').parent().find('label').html('{{__('admin.percentage')}}<span class="danger">*</span>');
        else {
            $('#price').parent().find('label').html('{{__('admin.price')}}<span class="danger">*</span>');
        }

    });

    @can('update coupons')
    $(document).on('click', '.statusClick', function () {
        let td = $(this).parent().parent().parent();

        let status = $(this).attr('data-status');
        let id_row = $(this).attr('data-id-row');
        let route = "{{url('discounts/coupons/update')}}" + "/" + id_row;
        $.ajax({
            type: 'POST',
            data: {
                _token: $('input[name ="_token"]').val(),
                status: status,
            },
            url: route,
            dataType: "json",
            success: function (data) {
                let status_append = statusAppend(data.id, data.status);
                td.empty().html(status_append);
                td.parent().attr('style', data.background_color_row);
            },
        });
    });

    @endcan

    function statusAppend(id, status) {

        let status_append = '<div class="btn-group" role="group" aria-label="Button group with nested dropdown">';
        if (status == 1) {
            status_append +=
                '<button class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                '        aria-expanded="false">' +
                '    <i class="ft-unlock icon-left"></i> {{__('admin.active')}}' +
                '</button>';
        } else {
            status_append +=
                '<button class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                '        aria-expanded="false">' +
                '    <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}' +
                '</button>';
        }

        status_append +=
            '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
            '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="active">{{__('admin.active')}}</button>' +
            '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="attitude">{{__('admin.attitude')}}</button>' +
            '    </div>' +
            '</div>';

        return status_append;
    }


</script>
