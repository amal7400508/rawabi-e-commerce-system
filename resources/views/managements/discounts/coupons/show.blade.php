{{-- start model show  Message--}}
<div id="confirmModalShow" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="card-content">
                <div class="modal-header" style="padding-bottom: 0px;">
                    <h5 class="card-title" id="basic-layout-form" style="padding-top: 5px;">
                        {{__('admin.show')}} {{__('admin.coupon')}}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                    </button>
                </div>

                <table class="table mb-0">
                    <tr>
                        <th>{{__('admin.id')}}</th>
                        <td id="td_id"></td>
                    </tr>
                    <tr hidden>
                        <th>{{__('admin.branch')}}</th>
                        <td id="td_branch"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.coupon_type')}}</th>
                        <td id="td_coupon_type"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.coupon')}}</th>
                        <td id="td_number"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.price')}}</th>
                        <td id="td_price"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.start_date')}}</th>
                        <td id="td_start_date"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.end_date')}}</th>
                        <td id="td_end_date"></td>
                    </tr>
                    <tr>
                        <th>{{__('admin.status')}}</th>
                        <td id="td_status"></td>
                    </tr>
                </table>
                <div class="row p-1">
                    <div class="col-sm-12">
                        <span class="details price" style="color: #0679f0"></span>
                    </div>
                    <div class="col-sm-9">
                        <small class="price category-color">{{__('admin.created_at')}}:</small>
                        <small class="price category-color" id="created"></small>
                        <br>
                        <small class="price category-color">{{__('admin.updated_at')}}:</small>
                        <small class="price category-color" id="updated_at"></small>
                    </div>
                    <div class="col-sm-3 text-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model show  Message--}}
