@php($page_title = __('admin.coupons_branches'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.discounts')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a></li>
                            <li class="breadcrumb-item span"><a href="{{route('coupons')}}">{{__('admin.coupons')}}</a></li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.coupons_branches')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('coupons_branches')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="coupon_type" @if($search_type == 'coupon_type') selected @endif>{{__('admin.coupon_type')}}</option>
                                                                    <option value="coupon" @if($search_type == 'coupon') selected @endif>{{__('admin.coupon')}}</option>
                                                                    <option value="price" @if($search_type == 'price') selected @endif>{{__('admin.price')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.coupon_type')}}</th>
                                                        <th>{{__('admin.coupon')}}</th>
                                                        <th>{{__('admin.price')}}</th>
                                                        <th>{{__('admin.start_date')}}</th>
                                                        <th>{{__('admin.end_date')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $coupon)
                                                        <tr style="{{$coupon->background_color_row_branches}}" class="@if($coupon->is_approved == 3) text-line-td @endif">
                                                            <td hidden>{{$coupon->updated_at}}</td>
                                                            <td class="font-default">{{$coupon->id}}</td>
                                                            <td>{{$coupon->couponType->name ?? null}}</td>
                                                            <td>{{$coupon->number}}</td>
                                                            <td>{{$coupon->price_with_type}}</td>
                                                            <td>{{$coupon->start_date}}</td>
                                                            <td>{{$coupon->end_date}}</td>
                                                            <td>
                                                                {!! $coupon->actions_branches !!}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

    </div>

    @include('managements.discounts.coupons.show')
@endsection
@section('script')
    <script>
        $('#td_branch').parent().attr('hidden', false);
        @can('update coupons')
        $(document).on('click', '.accept', async function () {
            let deposit_id = $(this).attr('id');
            let deposit_row = $(this).parent().parent().parent();
            try {
                let data = await
                    statusChange(
                        deposit_id,
                        "{{route('coupons.accept')}}",
                        "{{__('admin.do_you_want_to_accept')}}!",
                        "",
                        "warning",
                    );
                deposit_row.attr('style', data.data.background_color_row_branches);
                deposit_row.find('td:nth-last-child(1)').html(data.data.actions_branches);
                window.checkNotify();
                window.getNotify();
            } catch (e) {
                return e;
            }
        });

        $(document).on('click', '.reject', async function () {
            let deposit_id = $(this).attr('id');
            let deposit_row = $(this).parent().parent().parent();
            try {
                let data = await
                    statusChange(
                        deposit_id,
                        "{{route('coupons.reject')}}",
                        "{{__('admin.do_you_want_to_reject')}}!",
                        "",
                        "warning",
                    );
                deposit_row.attr('style', data.data.background_color_row_branches);
                deposit_row.find('td:nth-last-child(1)').html(data.data.actions_branches);
                deposit_row.addClass('text-line-td');
                window.checkNotify();
                window.getNotify();
            } catch (e) {
                return e;
            }
        });
        @endcan
    </script>
    @include('managements.discounts.coupons.show-js')
@endsection
