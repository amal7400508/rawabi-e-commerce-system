@php($page_title = __('admin.product_discounts'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.product_discounts')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">{{__('admin.Dashboard')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.product_discounts')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div id="messageSave" class="modal fade text-left" role="dialog">
            <div class="modal-dialog">
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="alert bg-info alert-icon-left mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-pencil-square"></i></span>
                            <strong>{{__('admin.successfully_done')}}!</strong>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-head">
                @can('create product_discounts')
                <div class="card-header">
                        <button type="button" id="add-product-discounts" class="btn btn-primary"><i
                                class="ft-plus white"></i> {{__('admin.create')}} {{__('admin.product_discounts')}} </button>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                    <div class="heading-elements">
                    </div>
                </div>
                    @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Invoices List table -->
                    <div class="table-responsive">
                        <table
                            class="table  {{--row-grouping--}} display no-wrap icheck table-middle"
                            id="data_table" width="100%">
                            <thead>
                            <tr>
                                <th hidden>{{__('admin.updated_at')}}</th>
                                <th>#</th>
                                <th>{{__('admin.branch')}}</th>
                                <th>
                                    {{__('admin.product')}}
                                    (<span style='color: #f1c40f'>{{__('admin.unit')}}</span>)
                                </th>
                                <th>{{__('admin.value_discounts')}}</th>
                                <th>{{__('admin.price')}}</th>
                                <th>{{__('admin.status')}}</th>
                                <th>{{__('admin.start_date')}}</th>
                                <th>{{__('admin.end_date')}}</th>
                                <th>{{__('admin.action')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!--/ Invoices table -->
                </div>
            </div>
        </div>
    </div>

    {{-- start model check Delete Message--}}
    <div id="confirmModalDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{__('admin.message_alerte')}} !</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 align="center" style="margin:0;">{{__('admin.are_you_sure_you_want_to_remove_this_data')}}</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button"
                            class="btn btn-danger">{{__('admin.yes')}}</button>
                    <button type="button" class="btn btn-dark" data-dismiss="modal">{{__('admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end model check Delete Message--}}

    {{-- start model Don Delete Message--}}
    <div id="messageDonDelete" class="modal fade text-left" role="dialog">
        <div class="modal-dialog">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="alert bg-danger alert-icon-left mb-2" role="alert">
                        <span class="alert-icon"><i class="ft ft-trash-2"></i></span>
                        <strong id="title-error">{{__('admin.successfully_done')}}!</strong>
                        <p id="error-deleting">{{__('admin.deleted_successfully')}}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--end model Don Delete Message--}}

    {{-- start modal add for cart--}}
    <div id="modalAddForCart" class="modal fade text-left" role="dialog">
        <div class="modal-dialog long-mode">
            <div class="modal-content">
                <div class="card-content">
                    <form id="form-add">
                        @csrf
                        <div class="modal-header danger">
                            <h4 class="modal-title" id="myModalLabel1">
                                {{__('admin.create')}} {{__('admin.product_discounts')}}
                            </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div
                                    class="form-group mb-1 col-sm-12 col-md-3 {{ $errors->has('branch_id[]') ? ' has-error' : '' }}">
                                    <label for="projectinput2">{{__('admin.branch_name')}}
                                        <span class="danger">*</span></label>
                                    <br>
                                    <select
                                        class="form-control @error('branch_id') is-invalid @enderror"
                                        id="branch_id" required name="branch_id">
                                        <option value="">{{__('admin.select_option')}}</option>
                                        @foreach($branches as $branch)
                                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('branch_id')
                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                    @enderror
                                </div>
                                <div
                                    class="form-group mb-1 col-sm-12 col-md-3 {{ $errors->has('type[]') ? ' has-error' : '' }}">
                                    <label for="pass">{{__('admin.type')}} <span
                                            class="danger">*</span></label>
                                    <br>
                                    <select class="form-control all-type" data-category-orders="0" required
                                            name="type" id="type">
                                        <option value="">{{__('admin.select_option')}}</option>
                                        <option value="product">{{__('admin.product')}}</option>
                                        <option value="all">{{__('admin.all')}}</option>
                                    </select>
                                </div>
                                <div
                                    class="form-group mb-1 col-sm-12 col-md-3 {{ $errors->has('categories[]') ? ' has-error' : '' }}">
                                    <label for="pass">{{__('admin.categories')}} <span
                                            class="danger">*</span></label>
                                    <br>
                                    <select disabled class="form-control all-categories"  data-category-order="0" required
                                            name="categories" id="categories">
                                        <option value="">{{__('admin.select_option')}}</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div
                                    class="form-group mb-1 col-sm-12 col-md-3 {{ $errors->has('product_id[]') ? ' has-error' : '' }}">
                                    <label for="pass">{{__('admin.product')}} <span
                                            class="danger">*</span></label>
                                    <br>
                                    <select disabled class="form-control product-0" id="select-product" required
                                            name="product_id" >
                                    </select>

                                </div>
                            </div>
                            <br><br>
                            <div id="loader-price" style="text-align: center"></div>
                            <div id="product_price_div">
                                <table width="100%"
                                       class="table  table-responsive table-custom-responsive">
                                    <thead style="background-color: #69696a26">
                                    <tr>
                                        <th>{{__('admin.product').' ('. __('admin.unit') .')'}}</th>
                                        <th>{{__('admin.price')}}</th>
                                        <th>{{--<i class="la la-cart-plus color-blue"></i>--}}</th>
                                    </tr>
                                    </thead>
                                    <tbody id="product_price_tbody">
                                    <tr class="text-center">
                                        <td colspan="4">{{__('admin.no_data')}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <span id="product_price-error" class="error-massege">
                                      <strong></strong>
                                    </span>
                            <div class="row" style="margin-top: 50px">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="start_date">{{__('admin.start_date')}}<span class="danger">*</span></label>
                                        <input type="date" id="start_date" required
                                               class="form-control"
                                               name="start_date">
                                        <span class="error-massege">
                                        <strong></strong>
                                    </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div
                                        class="form-group">
                                        <label for="end_date">{{__('admin.end_date')}}<span
                                                class="danger">*</span></label>
                                        <input type="date" id="end_date" required
                                               class="form-control"
                                               name="end_date">
                                        <span class="error-massege">
                                        <strong></strong>
                                    </span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="value_type">{{__('admin.value_discounts')}}<span
                                                class="danger">*</span></label>
                                        <select class="form-control" required id="value_type" name="value_type">
                                            <option value="price" selected>{{__('admin.a_price')}}</option>
                                            <option value="percentage">{{__('admin.a_percentage')}}</option>
                                        </select>
                                        <span class="error-massege">
                                        <strong></strong>
                                    </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="price">{{__('admin.price')}}<span class="danger">*</span></label>
                                    <div class="input-group">
                                        <input type="number" id="price" required class="form-control" name="price">
                                        <div class="input-group-append"><span class="input-group-text">.00</span></div>
                                    </div>
                                    <span class="error-massege">
                                      <strong></strong>
                                    </span>
                                </div>
                            </div>

                        </div>
                        <hr class="p0 m0">
                        <div class="pull-right m-1">
                            <button style="width: 150px; margin-bottom: 5px"
                                    type="submit"
                                    class="btn btn-primary"
                                    id="submit-form"
                            >
                                <i class="ft-save"></i> {{__('admin.save')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end  modal add for cart--}}

    {{-- start model edit status in product discount --}}
    <div id="editModel" class="modal fade text-left" role="dialog">
        <div class="modal-dialog" style="max-width: 50%;">
            <div class="modal-content">
                <div class="">
                    <div class="card-content">
                        <div class="card-header" style="padding-bottom: 0;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                            </button>
                            <h4>{{__('admin.product_discounts')}} : {{__('admin.edit')}} {{__('admin.status')}}</h4>
                        </div>
                        <hr>
                        <form class="form form-horizontal" id="form-edit">
                            @csrf
                            <div class="form-body">
                                <table width="100%" class="table  table-responsive table-custom-responsive ">
                                    <tbody class="text-center">
                                        <tr>
                                            <th>#</th>
                                            <td id="id_val"></td>
                                        </tr>
                                        <tr>
                                            <th>{{__('admin.product')}}</th>
                                            <td id="product_val"></td>
                                        </tr>
                                        <tr id="price_val">
                                            <th>{{__('admin.price')}}</th>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th>{{__('admin.start_date')}}</th>
                                            <td id="start_date_val"></td>
                                        </tr>
                                        <tr>
                                            <th>{{__('admin.end_date')}}</th>
                                            <td id="end_date_val"></td>
                                        </tr>
                                        <tr>
                                            <th>{{__('admin.status')}}<span class="danger">*</span></th>
                                            <td>
                                                <div class="row icheck_minimal skin" style="display: flex;justify-content: center;">
                                                    <fieldset style="margin-left: 20px; margin-right: 20px">
                                                        <input type="radio" name="status" id="input-radio-15" required
                                                               value="1">
                                                        <label style="color: #0072ff">{{__('admin.active')}}</label>
                                                    </fieldset>
                                                    <fieldset style="margin-left: 20px; margin-right: 20px">
                                                        <input type="radio" name="status" id="input-radio-16" required
                                                               value="0">
                                                        <label style="color: #FFC107;">{{__('admin.attitude')}}</label>
                                                    </fieldset>
                                                </div>
                                                <span id="status-error" class="error-massege">
                                                    <strong></strong>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-actions text-right">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-primary" id="button_update">
                                            <i class="ft-edit"></i> {{__('admin.edit')}}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model edit status in product discount --}}

    <style>
        .price-tr:hover{
            cursor: pointer;
            background-color: #e2e4ff !important;
        }
        .bg-color-tr{
            background-color: #e2e4ff !important;
        }
    </style>
    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')

@endsection
@section('script')
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    <!-- BEGIN: Page JS-->
    <script>
        /*start code show all data in datatable ajax*/
        $(document).ready(function () {
            $('#data_table').DataTable({
                @if(app()->getLocale() == 'ar')
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
                },
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('product_discounts') }}",
                },
                columns: [
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                    }, {
                        data: 'id',
                        name: 'id',
                    }, {
                        data: 'branch_name',
                        name: 'branch_name',
                    }, {
                        data: 'product_name',
                        name: 'product_name',
                    }, {
                        data: 'value_type',
                        name: 'value_type',
                        render: function (data, type, full, meta) {
                            if (data === 'percentage')
                                return "{{__('admin.a_percentage')}}";

                            return "{{__('admin.a_price')}}";
                        },
                    }, {
                        data: 'value',
                        name: 'value',
                        render: function (data, type, full, meta) {
                            if (full['value_type'] === 'percentage')
                                return "% " + data;

                            return data;
                        },
                    }, {
                        data: 'is_active',
                        name: 'is_active',
                        render: function (data, type, full, meta) {
                            if (data == 1) {
                                return "<div class='fonticon-wrap'><i class='ft-unlock' style='color:#0072ff'></i></div>"
                            } else {
                                return "<div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>"
                            }
                        },
                    }, {
                        data: 'start_date_carbon',
                        name: 'start_date_carbon',
                    }, {
                        data: 'end_date_carbon',
                        name: 'end_date_carbon',
                    }, {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    }
                ],
                'order': [[0, 'desc']],
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });
        });
        /*start message save*/

        /*start code edit*/
        let frm = $('#my_form_id');
        let product_discount_id;
        $(document).on('click', '.edit-table-row', function () {
            $('#status-error strong').empty();
            product_discount_id = $(this).attr('id');
            let detail_url = "{{url('discounts/product_discounts/edit')}}" + "/" + product_discount_id;
            $.ajax({
                type: 'GET',
                url: detail_url,
                dataType: "json",
                beforeSend: function () {
                    $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                },
                success: function (data) {
                    $('#confirm-modal-loading-show').modal('hide');

                    if (data.value_type === 'percentage')
                        $('#price_val th').html('{{__('admin.percentage')}}');
                    else{
                        $('#price_val th').html('{{__('admin.price')}}');
                    }

                    $('#id_val').text(data.id);
                    $('#product_val').html(data.product_name);
                    $('#price_val td').text(data.price);
                    $('#start_date_val').text(data.start_date);
                    $('#end_date_val').text(data.end_date);

                    if (data.is_active == 1) {
                        $('#input-radio-15').iCheck('check');
                    } else {
                        $('#input-radio-16').iCheck('check');
                    }

                    $('#editModel').modal('show');
                },
                error: function (data) {
                    $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    $('#confirm-modal-loading-show').modal('show');
                }
            })
        });
        /*end code edit*/
    </script>
    <script>
        $(document).on('click', '#add-product-discounts', function () {
            let modal_add = $('#modalAddForCart');
            modal_add.find("select").val("").removeClass('is-invalid');
            modal_add.find('input[type=number]').val("").removeClass('is-invalid');
            modal_add.find('input[type=date]').val("").removeClass('is-invalid');
            modal_add.find('span strong').empty();
            modal_add.modal('show');
            $('#product_price_tbody').html(
                '<tr class="text-center">' +
                '<td colspan="4">{{__('admin.no_data')}}</td>' +
                '</tr>'
            );
        });

        /*ajax falter categories start*/
        $(document).on('change', '#branch_id', function () {
            let category_id = $(this).val();
            let category_url = "{{url('/app/offers/falter_main_category')}}" + "/" + category_id;
            let element_product = $('#categories');
            if (category_id) {
                $.ajax({
                    type: "GET",
                    url: category_url,
                    success: function (data) {
                        if (data) {
                            element_product.empty();
                            element_product.append('<option value="">{{__("admin.select_option")}}</option>');
                            $.each(data, function (key, value) {
                                element_product.append('<option value="' + value.id + '">' + value.name + '</option>');
                            });
                        } else {
                            element_product.empty();
                        }
                    }
                });
            } else {
                element_product.empty();
            }
        });

        /*ajax falter categories end*/
        {{--$(document).on('change', '#category', function () {--}}
        {{--    let category_id = $(this).val();--}}
        {{--    let category_url = "{{url('app/offers/falter-product')}}" + "/" + category_id;--}}
        {{--    let element_product = $('#product_id');--}}
        {{--    if (category_id) {--}}
        {{--        $.ajax({--}}
        {{--            type: "GET",--}}
        {{--            url: category_url,--}}
        {{--            success: function (data) {--}}
        {{--                if (data) {--}}
        {{--                    element_product.empty();--}}
        {{--                    element_product.append('<option value="">{{__("admin.select_option")}}</option>');--}}
        {{--                    $.each(data, function (key, value) {--}}
        {{--                        element_product.append('<option value="' + value.id + '">' + value.name + '</option>');--}}
        {{--                    });--}}
        {{--                } else {--}}
        {{--                    element_product.empty();--}}
        {{--                }--}}
        {{--            }--}}
        {{--        });--}}
        {{--    } else {--}}
        {{--        element_product.empty();--}}
        {{--    }--}}
        {{--});--}}
        /*ajax falter categories start*/
        $(document).on('change', '.all-categories', function () {
            var data_number = $(this).attr('data-category-order');
            var branch_id = $("#branch_id").val();
            var category_id = $(this).val();
            console.log(branch_id);
            category_url = "{{url('app/offers/falter-product-discount')}}" + "/" + category_id + '/' + branch_id;
            if (category_id) {
                $.ajax({
                    type: "GET",
                    url: category_url,
                    success: function (data) {
                        if (data) {
                            $('.product-' + data_number).empty();
                            $('.product-' + data_number).append('<option value=""> {{__("admin.select_option")}}  </option>');
                            $.each(data, function (key, value) {
                                $(".product-" + data_number).append('<option value="' + value.id + '">' + value.name + '</option>');
                            });
                        } else {
                            $('.product-' + data_number).empty();
                        }
                    }
                });
            } else {
                $('.product-' + data_number).empty();
            }
        });
        /*ajax falter categories end*/
        $(document).on('change', '#categories', function () {
            let category_id = $(this).val();
            let category_url = "{{url('app/offers/falter-product')}}" + "/" + category_id;
            let element_product = $('#select-product');
            if (category_id) {
                $.ajax({
                    type: "GET",
                    url: category_url,
                    success: function (data) {
                        if (data) {
                            element_product.empty();
                            element_product.append('<option value="">{{__("admin.select_option")}}</option>');
                            $.each(data, function (key, value) {
                                element_product.append('<option value="' + value.id + '">' + value.name + '</option>');
                            });
                        } else {
                            element_product.empty();
                        }
                    }
                });
            } else {
                element_product.empty();
            }
        });
        /*ajax falter products start*/
        $(document).on('change', '#select-product', function () {
            let product_id = $(this).val();
            let url = "{{url('requests_management/request/product-price')}}" + "/" + product_id;
            if (product_id) {
                $.ajax({
                    type: "GET",
                    url: url,
                    beforeSend: function(){
                        $("#loader-price").html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                        $("#product_price_div").attr('hidden', true);
                    },
                    success: function (data) {
                        $("#loader-price").empty();
                        $("#product_price_div").attr('hidden', false);
                        $("#product_price_tbody").empty();
                        for (var i = 0; i < data.data.length; i++) {
                            $("#product_price_tbody").append("<tr class='price-tr'>" +
                                "<td>" + data.product_name + ' (<span class="color-blue">' + data.data[i].unitName + '</span>) ' + "</td>" +
                                "<td>" + data.data[i].price + "</td>" +
                                "<td>" +
                                "<input type='checkbox' name='product_price_val[]' id='product_price_val' class='action-item' value='" + data.data[i].id + "'>" +
                                "</td>" +
                                "</tr>");
                        }

                    },
                    error: function () {
                        $("#loader-price").html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    }
                });
            } else {

            }
        });
        /*ajax falter products end*/

        $(document).on('click', '.price-tr', function () {
            let checkbox = $(this).find('input[type=checkbox]');

            if(checkbox.prop('checked')== false){
                $(this).addClass('bg-color-tr');
                checkbox.prop('checked', true);
            }
            else{
                $(this).removeClass('bg-color-tr');
                checkbox.prop('checked', false);
            }
        });

        $(document).on('change', '#value_type', function () {
            let val = $(this).val();
            let form_group = $('#price').parent().parent();
            if (val === 'percentage'){
                form_group.find('label').html('{{__('admin.percentage')}}<span class="danger">*</span>');
                form_group.find('.input-group-text').text('%');
            } else{
                form_group.find('label').html('{{__('admin.price')}}<span class="danger">*</span>');
                form_group.find('.input-group-text').text('.00');
            }
        });

        $(document).on('change', '#type', async function () {
            let type = $(this).val();
            if (type === 'product'){
                $("#select-product").attr('disabled', false);
                $("#categories").attr('disabled', false);
            } else {
                $("#select-product").attr('disabled', true);
                $("#categories").attr('disabled', true);
            }
        });

        $('#form-add').on('submit', function (event) {
            event.preventDefault();
            let btn = $('#submit-form');
            let formData = new FormData(this);

            $.ajax({
                url: "{{route('product_discounts.store')}}",
                method: "POST",
                data: formData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#form-add').find('span strong').empty();
                    btn.html('<i class="la la-spinner spinner"></i><span> {{__('admin.adding')}}...</span>');
                    $('#product_price_div').css('border', 'none');
                },
                success: function (data) {
                    btn.html('<i class="ft-save"></i> {{__('admin.add')}}');
                    $('#modalAddForCart').modal('hide');

                    $('#data_table').DataTable().ajax.reload();

                    /*reloadTable(data.category_id);

                    console.log(data.success);*/
                    $('#messageSave p').text(data.success + '.');
                    $('#messageSave').modal('show');
                    setTimeout(function () {
                        $('#messageSave').modal('hide');
                    }, 3000);
                },
                error: function (data) {
                    btn.html('<i class="ft-save"></i> {{__('admin.add')}}');

                    let obj = JSON.parse((data.responseText));
                    let price = undefined;
                    let value_type = undefined;
                    let start_date = undefined;
                    let end_date = undefined;
                    let product_price_val_error = data.responseJSON.product_price_val;


                    if(obj.error !== undefined){
                        price = obj.error.price;
                        value_type = obj.error.value_type;
                        start_date = obj.error.start_date;
                        end_date = obj.error.end_date;
                    }

                    if(product_price_val_error !== undefined){
                        $('#product_price_div').css('border', '1px solid red');
                        $('#product_price-error strong').text(product_price_val_error);
                    }
                    else if (
                        price !== undefined || value_type !== undefined
                        || start_date !== undefined || end_date !== undefined
                    ) {
                        if (price !== undefined) {
                            $('#price').addClass('is-invalid').parent().parent().find('span strong').text(price)
                        }

                        if (value_type !== undefined) {
                            $('#value_type').addClass('is-invalid').parent().find('span strong').text(value_type)
                        }

                        if (start_date !== undefined) {
                            $('#start_date').addClass('is-invalid').parent().find('span strong').text(start_date)
                        }

                        if (end_date !== undefined) {
                            $('#end_date').addClass('is-invalid').parent().find('span strong').text(end_date)
                        }
                    }
                    else {
                        $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                        $('#confirm-modal-loading-show').modal('show');
                    }
                }
            });
        });

        $('#form-edit').on('submit', function (event) {
            event.preventDefault();
            let btn = $('#button_update');
            let formData = new FormData(this);

            $.ajax({
                url: "{{url('discounts/product_discounts/update')}}" + "/" + product_discount_id,
                method: "POST",
                data: formData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#status-error strong').empty();
                    btn.html('<i class="la la-spinner spinner"></i><span> {{__('admin.updating')}}...</span>');
                },
                success: function (data) {
                    btn.html('<i class="ft-save"></i> {{__('admin.edit')}}');
                    $('#editModel').modal('hide');

                    $('#data_table').DataTable().ajax.reload();

                    $('#messageSave p').text(data.success + '.');
                    $('#messageSave').modal('show');
                    setTimeout(function () {
                        $('#messageSave').modal('hide');
                    }, 3000);
                },
                error: function (data) {
                    btn.html('<i class="ft-save"></i> {{__('admin.edit')}}');

                    let status_error = data.responseJSON.status_error;

                    if(status_error !== undefined){
                        $('#status-error strong').text(status_error);
                    }
                    else{
                        $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                        $('#confirm-modal-loading-show').modal('show');
                    }
                }
            });
        });

    </script>

    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/checkbox-radio.js')}}"></script>
@endsection
