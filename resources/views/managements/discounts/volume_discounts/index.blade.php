@php($page_title = __('admin.volume_discount'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.discounts')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a></li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.volume_discount')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <button type="button" id="create-discounts" class="btn btn-primary"><i
                                                class="ft-plus white"></i> {{__('admin.create')}} {{__('admin.volume_discount')}} </button>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>

                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('volume_discounts')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="branch" @if($search_type == 'branch') selected @endif>{{__('admin.branch')}}</option>
                                                                    <option value="price_from" @if($search_type == 'price_from') selected @endif>{{__('admin.lowest_price_requesting')}}</option>
                                                                    <option value="price_to" @if($search_type == 'price_to') selected @endif>{{__('admin.top_price_requesting')}}</option>
                                                                    <option value="discount" @if($search_type == 'discount') selected @endif>{{__('admin.discount')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.branch')}}</th>
                                                        <th>{{__('admin.lowest_price_requesting')}}</th>
                                                        <th>{{__('admin.top_price_requesting')}}</th>
                                                        <th>{{__('admin.discount')}}</th>
                                                        <th>{{__('admin.status')}}</th>
                                                        <th>{{__('admin.type')}}</th>
{{--                                                        <th>{{__('admin.start_date')}}</th>--}}
{{--                                                        <th>{{__('admin.end_date')}}</th>--}}
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $volume_discount)
                                                        <tr style="{{$volume_discount->background_color_row}}" class="@if($volume_discount->is_approved == 3) text-line-td @endif">
                                                            <td hidden>{{$volume_discount->updated_at}}</td>
                                                            <td>{{$volume_discount->id}}</td>
                                                            <td>{{$volume_discount->branch->name ?? null}}</td>
                                                            <td>{{$volume_discount->price_from}}</td>
                                                            <td>{{$volume_discount->price_to}}</td>
                                                            <td>{{$volume_discount->price_with_type}}</td>
                                                            <td>
                                                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                                                    @if($volume_discount->is_active == 1)
                                                                        <button @can('update volume_discounts') @else disabled @endcan class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-unlock icon-left"></i> {{__('admin.active')}}
                                                                        </button>
                                                                    @else
                                                                        <button @can('update volume_discounts') @else disabled @endcan class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}
                                                                        </button>
                                                                    @endif
                                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$volume_discount->id}}" data-status="active">{{__('admin.active')}}</button>
                                                                        <button class="dropdown-item statusClick" data-id-row="{{$volume_discount->id}}" data-status="attitude">{{__('admin.attitude')}}</button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                                                    @if($volume_discount->type == 'delivery')
                                                                        <button class="btn btn-sm btn-primary dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            {{__('admin.delivery')}}
                                                                        </button>
                                                                    @elseif($volume_discount->type == 'minimum')
                                                                        <button class="btn btn-sm btn-danger dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            {{__('admin.minimum')}}
                                                                        </button>
                                                                    @elseif($volume_discount->type == 'balance')
                                                                        <button class="btn btn-sm btn-danger dropdown-menu-right box-shadow-2 px-2"
                                                                                id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                                                                                aria-expanded="false">
                                                                            {{__('admin.balance')}}
                                                                        </button>
                                                                    @endif
                                                                </div>
                                                            </td>
{{--                                                            <td>{{$volume_discount->start_date}}</td>--}}
{{--                                                            <td>{{$volume_discount->end_date}}</td>--}}

                                                            <td>{!! $volume_discount->actions !!}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    {{-- start modal add for cart--}}
    <div id="modalAddForCart" class="modal fade text-left" role="dialog">
        <div class="modal-dialog long-mode">
            <div class="modal-content">
                <div class="card-content">
                    <form id="form-add">
                        @csrf
                        <div class="modal-header danger">
                            <h4 class="modal-title" id="myModalLabel1">
                                {{__('admin.create')}} {{__('admin.volume_discount')}}
                            </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group mb-1 col-md-3">
                                    <div
                                        class="form-group {{ $errors->has('branch_id') ? ' has-error' : '' }}">
                                        <label
                                            for="projectinput2">{{__('admin.branch_name')}}
                                            <span class="danger">*</span></label>
                                        <br>
                                        <select
                                            class="form-control @error('branch_id') is-invalid @enderror"
                                            id="branch_id"  name="branch_id">
                                            <option
                                                value="">{{__('admin.select_option')}}</option>
                                            @foreach($branches as $branch)
                                                <option
                                                    value="{{$branch->id}}">{{$branch->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('branch_id')
                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mb-1 col-md-3">
                                    <label for=type">{{__('admin.type')}}<span
                                            class="danger">*</span></label>
                                    <select class="form-control" required id="type" name="type">
                                        <option value="minimum" selected>{{__('admin.minimum')}}</option>
                                        <option value="balance">{{__('admin.balance')}}</option>
                                        <option value="delivery">{{__('admin.delivery')}}</option>
                                    </select>
                                    <span class="error-massege">
                                                        <strong></strong>
                                                    </span>
                                </div>
                                <div class="form-group mb-1 col-md-3">
                                    <label for="price_from">{{__('admin.lowest_price_requesting')}} <span class="danger">*</span></label>
                                    <br>
                                    <input type="number" class="form-control" name="price_from" id="price_from" required>
                                    <span class="error-massege">
                                        <strong></strong>
                                    </span>
                                </div>
                                <div class="form-group mb-1 col-md-3">
                                    <label for="price_to">{{__('admin.top_price_requesting')}}</label>
                                    <br>
                                    <input type="number" class="form-control" name="price_to" id="price_to">
                                    <span class="error-massege">
                                        <strong></strong>
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="start_date">{{__('admin.start_date')}}<span class="danger">*</span></label>
                                        <input type="date" id="start_date" required
                                               class="form-control"
                                               name="start_date">
                                        <span class="error-massege">
                                        <strong></strong>
                                    </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div
                                        class="form-group">
                                        <label for="end_date">{{__('admin.end_date')}}<span class="danger">*</span></label>
                                        <input required type="date" id="end_date" class="form-control" name="end_date">
                                        <span class="error-massege">
                                        <strong></strong>
                                    </span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md12">
                                                <div class="form-group">
                                                    <label for="value_type">{{__('admin.value_discounts')}}<span
                                                            class="danger">*</span></label>
                                                    <select class="form-control" required id="value_type" name="value_type">
                                                        <option value="price" selected>{{__('admin.a_price')}}</option>
                                                        <option value="percentage">{{__('admin.a_percentage')}}</option>
                                                    </select>
                                                    <span class="error-massege">
                                                        <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md12">
                                                <label for="price">{{__('admin.price')}}<span class="danger">*</span></label>
                                                <div class="input-group">
                                                    <input type="number" id="price" required class="form-control" name="price">
                                                    <div class="input-group-append"><span class="input-group-text">.00</span></div>
                                                </div>
                                                <span class="error-massege">
                                                   <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="note">{{__('admin.note')}}</label>
                                            <textarea id="not" name="not" class="form-control" rows="6"></textarea>
                                            <span class="error-massege">
                                                <strong></strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr class="p0 m0">
                        <div class="pull-right m-1">
                            <button style="width: 150px; margin-bottom: 5px"
                                    type="submit"
                                    class="btn btn-primary"
                                    id="submit-form"
                            >
                                <i class="ft-save"></i> {{__('admin.save')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end  modal add for cart--}}

    {{--model show modal--}}
    @include('managements.discounts.volume_discounts.show')
@endsection
@section('script')
    @include('managements.discounts.volume_discounts.show-js')
    <script>
            @can('update volume_discounts')
        let modal_add = $('#modalAddForCart');
        $(document).on('click', '#create-discounts', function () {
            modal_add.find("select").val("").removeClass('is-invalid');
            modal_add.find('input[type=number]').val("").removeClass('is-invalid');
            modal_add.find('input[type=date]').val("").removeClass('is-invalid');
            modal_add.find('span strong').empty();
            modal_add.modal('show');
            $('#product_price_tbody').html(
                '<tr class="text-center">' +
                '<td colspan="4">{{__('admin.no_data')}}</td>' +
                '</tr>'
            );
        });
            $(document).on('click', '.price-tr', function () {
                let checkbox = $(this).find('input[type=checkbox]');

                if (checkbox.prop('checked') == false) {
                    $(this).addClass('bg-color-tr');
                    checkbox.prop('checked', true);
                } else {
                    $(this).removeClass('bg-color-tr');
                    checkbox.prop('checked', false);
                }
            });

            $(document).on('change', '#type', function () {
                let val = $(this).val();
                if (val === 'delivery'){
                    $('#value_type').html('<select class="form-control" required id="value_type" name="value_type">' +
                        '<option value="price" selected>{{__('admin.a_price')}}</option>' +
                        '</select>');
                } else {
                    $('#value_type').html('<select class="form-control" required id="value_type" name="value_type">' +
                        '<option value="price" selected>{{__('admin.a_price')}}</option>' +
                        '<option value="percentage">{{__('admin.a_percentage')}}</option>' +
                        '</select>');
                }
            });

            $(document).on('change', '#value_type', function () {
                let val = $(this).val();
                let form_group = $('#price').parent().parent();
                if (val === 'percentage') {
                    form_group.find('label').html('{{__('admin.percentage')}}<span class="danger">*</span>');
                    form_group.find('.input-group-text').text('%');
                } else {
                    form_group.find('label').html('{{__('admin.price')}}<span class="danger">*</span>');
                    form_group.find('.input-group-text').text('.00');
                }
            });

            $('#form-add').on('submit', function (event) {
                event.preventDefault();
                let btn = $('#submit-form');
                let formData = new FormData(this);

                $.ajax({
                    url: "{{route('volume_discounts.store')}}",
                    method: "POST",
                    data: formData,
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    type: 'post',
                    beforeSend: function () {
                        $('#form-add').find('span strong').empty();
                        btn.html('<i class="la la-spinner spinner"></i><span> {{__('admin.adding')}}...</span>');
                        $('#product_price_div').css('border', 'none');
                        modal_add.find("select").removeClass('is-invalid');
                        modal_add.find('input[type=number]').removeClass('is-invalid');
                        modal_add.find('input[type=date]').removeClass('is-invalid');
                    },
                    success: function (data) {
                        btn.html('<i class="ft-save"></i> {{__('admin.add')}}');
                        $('#modalAddForCart').modal('hide');

                        /*$('#data_table').DataTable().ajax.reload();*/

                        let price_p = data.volume_discount.value;
                        let price_to = data.volume_discount.price_to;
                        if (price_to == null) price_to = "";

                        let end_date = data.volume_discount.end_date;
                        if (end_date == null) end_date = "";

                        if (data.volume_discount.value_type === 'percentage') {
                            price_p = "% " + data.volume_discount.value;
                        }
                        let status_append = statusAppend(data.volume_discount.id, data.volume_discount.is_active);

                        let type;
                        if (data.volume_discount.type == 'minimum') {
                            type = "<button class='btn btn-sm btn-danger dropdown-menu-right box-shadow-2 px-2'" +
                                "id='btnGroupDrop1' type='button' data-toggle='dropdown' aria-haspopup='true'" +
                                "aria-expanded='false'>" +
                                "{{__('admin.minimum')}}" +
                                "</button>"
                        } else if (data.volume_discount.type == 'delivery') {
                            type = "<button class='btn btn-sm btn-primary dropdown-menu-right box-shadow-2 px-2'" +
                                "id='btnGroupDrop1' type='button' data-toggle='dropdown' aria-haspopup='true'" +
                                "aria-expanded='false'>" +
                                "{{__('admin.delivery')}}" +
                                "</button>"
                        } else if (data.volume_discount.type == 'balance') {
                            type = "<button class='btn btn-sm btn-danger dropdown-menu-right box-shadow-2 px-2'" +
                                "id='btnGroupDrop1' type='button' data-toggle='dropdown' aria-haspopup='true'" +
                                "aria-expanded='false'>" +
                                "{{__('admin.delivery')}}" +
                                "</button>"
                        }

                        let actions = '<a class="action-item edit-table-row" id="' + data.volume_discount.id + '"><i class="ft ft-edit color-blue"></i></a>';

                        let row = $("<tr></tr>");
                        let col0 = $("<td>" + data.volume_discount.id + "</td>");
                        let col1 = $("<td>" + data.volume_discount.branch_name + "</td>");
                        let col2 = $("<td>" + data.volume_discount.price_from + "</td>");
                        let col3 = $("<td>" + price_to + "</td>");
                        let col4 = $("<td>" + price_p + "</td>");
                        let col5 = $("<td>" + status_append + "</td>");
                        let col6 = $("<td>" + type + "</td>");
                        let col7 = $("<td>" + data.volume_discount.actions + "</td>");

                        row.append(col0, col1, col2, col3, col4, col5, col6, col7).prependTo("#table");

                        /*reloadTable(data.category_id);

                        console.log(data.success);*/
                        $('#messageSave p').text(data.success + '.');
                        $('#messageSave').modal('show');
                        setTimeout(function () {
                            $('#messageSave').modal('hide');
                        }, 3000);
                    },
                    error: function (data) {
                        btn.html('<i class="ft-save"></i> {{__('admin.add')}}');

                        let obj = JSON.parse((data.responseText));
                        let price = undefined;
                        let value_type = undefined;
                        let type = undefined;
                        let start_date = undefined;
                        let end_date = undefined;
                        let price_from = undefined;
                        let price_to = undefined;
                        let product_price_val_error = data.responseJSON.product_price_val;


                        if (obj.error !== undefined) {
                            price_from = obj.error.price_from;
                            price_to = obj.error.price_to;
                            price = obj.error.price;
                            value_type = obj.error.value_type;
                            type = obj.error.type;
                            start_date = obj.error.start_date;
                            end_date = obj.error.end_date;
                        }

                        if (product_price_val_error !== undefined) {
                            $('#product_price_div').css('border', '1px solid red');
                            $('#product_price-error strong').text(product_price_val_error);
                        } else if (
                            price !== undefined || value_type !== undefined
                            || start_date !== undefined || end_date !== undefined
                            || price_from !== undefined || price_to !== undefined
                        ) {
                            if (price_from !== undefined) {
                                $('#price_from').addClass('is-invalid').parent().find('span strong').text(price_from)
                            }

                            if (price_to !== undefined) {
                                $('#price_to').addClass('is-invalid').parent().find('span strong').text(price_to)
                            }

                            if (price !== undefined) {
                                $('#price').addClass('is-invalid').parent().parent().find('span strong').text(price)
                            }

                            if (value_type !== undefined) {
                                $('#value_type').addClass('is-invalid').parent().find('span strong').text(value_type)
                            }

                            if (type !== undefined) {
                                $('#type').addClass('is-invalid').parent().find('span strong').text(type)
                            }

                            if (start_date !== undefined) {
                                $('#start_date').addClass('is-invalid').parent().find('span strong').text(start_date)
                            }

                            if (end_date !== undefined) {
                                $('#end_date').addClass('is-invalid').parent().find('span strong').text(end_date)
                            }
                        } else {
                            $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                            $('#confirm-modal-loading-show').modal('show');
                        }
                    }
                });
            });

            $(document).on('click', '.accept', async function () {
            let deposit_id = $(this).attr('id');
            let deposit_row = $(this).parent().parent().parent();
            try {
                let data = await
                    statusChange(
                        deposit_id,
                        "{{route('volume_discounts.accept')}}",
                        "{{__('admin.do_you_want_to_accept')}}!",
                        "",
                        "warning",
                    );
                deposit_row.attr('style', data.data.background_color_row);
                deposit_row.find('td:nth-last-child(1)').html(data.data.actions);

            } catch (e) {
                return e;
            }
        });

        $(document).on('click', '.reject', async function () {
            let deposit_id = $(this).attr('id');
            let deposit_row = $(this).parent().parent().parent();
            try {
                let data = await
                    statusChange(
                        deposit_id,
                        "{{route('volume_discounts.reject')}}",
                        "{{__('admin.do_you_want_to_reject')}}!",
                        "",
                        "warning",
                    );
                deposit_row.attr('style', data.data.background_color_row);
                deposit_row.find('td:nth-last-child(1)').html(data.data.actions);
                deposit_row.addClass('text-line-td');
            } catch (e) {
                return e;
            }
        });

        $(document).on('click', '.statusClick', function () {
            let td = $(this).parent().parent().parent();

            let status = $(this).attr('data-status');
            let id_row = $(this).attr('data-id-row');
            let route = "{{url('discounts/volume_discounts/update')}}" + "/" + id_row;
            $.ajax({
                type: 'POST',
                data: {
                    _token: $('input[name ="_token"]').val(),
                    status: status,
                },
                url: route,
                dataType: "json",
                success: function (data) {
                    let status_append = statusAppend(data.id, data.status);
                    td.empty().html(status_append);
                    td.parent().attr('style', data.background_color_row);
                },
            });
        });

        function statusAppend(id, status) {

            let status_append = '<div class="btn-group" role="group" aria-label="Button group with nested dropdown">';
            if (status == 1) {
                status_append +=
                    '<button class="btn btn-sm btn-primary dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                    '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                    '        aria-expanded="false">' +
                    '    <i class="ft-unlock icon-left"></i> {{__('admin.active')}}' +
                    '</button>';
            } else {
                status_append +=
                    '<button class="btn btn-sm btn-danger dropdown-toggle dropdown-menu-right box-shadow-2 px-2"' +
                    '        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"' +
                    '        aria-expanded="false">' +
                    '    <i class="ft-lock icon-left"></i> {{__('admin.attitude')}}' +
                    '</button>';
            }

            status_append +=
                '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
                '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="active">{{__('admin.active')}}</button>' +
                '        <button class="dropdown-item statusClick" data-id-row="' + id + '" data-status="attitude">{{__('admin.attitude')}}</button>' +
                '    </div>' +
                '</div>';

            return status_append;
        }
        @endcan
    </script>
@endsection
