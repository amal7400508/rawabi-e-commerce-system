@php($page_title = __('admin.statistics'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.statistics')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.statistics')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.create')}} {{__('admin.requests_report')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('statistics')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <form method="get">
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.from')}}</label>
                                                                        <input type="date" class="form-control"
                                                                               value="{{request()->from}}" name="from">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.to')}}</label>
                                                                        <input type="date" class="form-control"
                                                                               value="{{request()->to}}" name="to">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.providers')}}</label>
                                                                        <select class="form-control" name="provider" id="provider">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            @foreach($providers as $provider)
                                                                                <option value="{{$provider->id}}">{{$provider->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.branch_name')}}</label>
                                                                        <select class="form-control" name="branch_name" id="branch_name">
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.click_type')}}</label>
                                                                        <select class="form-control" name="click_type">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            <option value="search">{{__('admin.search')}}</option>
                                                                            <option value="category">{{__('admin.sub_categories')}}</option>
                                                                            <option value="main_category">{{__('admin.main_categories')}}</option>
                                                                            <option value="product">{{__('admin.product')}}</option>
                                                                            <option value="offer">{{__('admin.offer')}}</option>
                                                                            <option value="branch">{{__('admin.branch')}}</option>
                                                                            <option value="provider">{{__('admin.provider')}}</option>
                                                                            <option value="advertisement_image">{{__('admin.advertisement_image')}}</option>
                                                                            <option value="advertisement_video">{{__('admin.advertisement_video')}}</option>
                                                                            <option value="advertisement_popup">{{__('admin.popup')}}</option>
                                                                            <option value="advertisement_slider">{{__('admin.slider_img')}}</option>
                                                                            <option value="cart">{{__('admin.cart')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.search')}}</label>
                                                                        <input type="text" class="form-control"
                                                                               value="{{request()->search}}" name="search">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.user_name')}}</label>
                                                                        <input type="text" class="form-control" value="{{request()->user_name}}" name="user_name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.phone')}}</label>
                                                                        <input type="text" class="form-control" value="{{request()->phone}}" name="phone">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.gender')}}</label>
                                                                        <select class="form-control" name="gender">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            <option value="man">{{__('admin.man')}}</option>
                                                                            <option value="woman">{{__('admin.woman')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.type')}}</label>
                                                                        <select class="form-control" name="type">
                                                                            <option value="">{{__('admin.detailed')}}</option>
                                                                            <option value="total">{{__('admin.a_total')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div id="options" class="col-md-12" @if(request()->type != 'total') hidden @endif>
                                                                    <div class="form-group">
                                                                        <label>تجميع على حسب</label>
                                                                        <div class="row" style="margin: 5px 0px;border: 1px solid #c5cbd9;padding: 13px;border-radius: 5px;">
                                                                            <div class="d-inline-block custom-control custom-checkbox col-md-4" style="margin-bottom: 5px ">
                                                                                <input type="checkbox" name="option[]" @if(in_array('user',request()->option ?? [])) checked @endif value="user" class="custom-control-input" id="check_user">
                                                                                <label class="custom-control-label cursor-pointer" for="check_user">{{__('admin.users')}}</label>
                                                                            </div>
                                                                            <div class="d-inline-block custom-control custom-checkbox col-md-4" style="margin-bottom: 5px ">
                                                                                <input type="checkbox" name="option[]" value="click_item" @if(in_array('click_item',request()->option ?? [])) checked @endif class="custom-control-input" id="check_click_item">
                                                                                <label class="custom-control-label cursor-pointer" for="check_click_item">العنصر</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions pull-right">
                                                    <button type="submit" formaction="{{route('statistics')}}" class=" btn btn-primary" id="button_preview"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-eye"></i> {{__('admin.preview')}}
                                                    </button>
                                                    <button type="submit" formaction="{{route('statistics.export')}}" class=" btn btn-primary" id="button_export"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-printer"></i> {{__('admin.export')}}
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.post_index')}} {{__('admin.requests')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('statistics')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                </div>
                                                <table width="100%" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        @if(is_null(request()->type) or in_array('user',request()->option ?? []))
                                                            <th>{{__('admin.user_name')}}</th>
                                                            <th>{{__('admin.phone')}}</th>
                                                        @endif
                                                        <th>{{__('admin.click_type')}}</th>
                                                        @if(in_array('click_item',request()->option ?? []) or is_null(request()->option))
                                                            <th>{{__('admin.name')}}</th>
                                                        @endif
                                                        <th>{{__('admin.number')}}</th>
                                                        @if(is_null(request()->type))
                                                            <th>{{__('admin.created_at')}}</th>
                                                        @endif
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data ) == 0)
                                                        <tr>
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $statistic)
                                                        <tr>
                                                            <td hidden>{{$statistic->updated_at}}</td>
                                                            @if(is_null(request()->type) or in_array('user',request()->option ?? []))
                                                                <td>{{$statistic->user->full_name}}</td>
                                                                <td>{{$statistic->user->phone}}</td>
                                                            @endif
                                                            <td>{!! $statistic->click !!}</td>
                                                            @if(in_array('click_item',request()->option ?? []) or is_null(request()->option))
                                                                @if($statistic->statisticable_type == 'App\Models\Product'
                                                                    or $statistic->statisticable_type == 'App\Models\Offer'
                                                                    or $statistic->statisticable_type == 'App\Models\Category'
                                                                    or $statistic->statisticable_type == 'App\Models\MainCategory'
                                                                    or $statistic->statisticable_type == 'App\Models\Branch'
                                                                    or $statistic->statisticable_type == 'App\Models\Provider')
                                                                    <td>{{$statistic->statisticable->name}}</td>
                                                                @elseif($statistic->statisticable_type == 'App\Models\PaidAdvertisement')
                                                                    <td>{{$statistic->statisticable->title}}</td>
                                                                @elseif($statistic->statisticable_type == 'App\Models\Advertisement')
                                                                    <td>{{$statistic->statisticable->desc}}</td>
                                                                @elseif($statistic->click_type == 'search')
                                                                    <td>{{$statistic->text}}</td>
                                                                @else
                                                                    <td></td>
                                                                @endif
                                                            @endif
                                                            <td>{{$statistic->number}}</td>
                                                            @if(is_null(request()->type))
                                                                <td>{{$statistic->created_at}}</td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            {{ $data->appends(request()->all())->links() }}

                                            <span>{{__('admin.show').' '. $data->count() .' '.__('admin.out_of').' '. $data_count . ' '. __('admin.record')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    {{-- start model show offer Message--}}
    @include('managements.app.offers.show')
    {{-- end model show product Message--}}
    @include('managements.app.products.show')
    @include('managements.encoding.categories.show')
    @include('managements.advertisements.paid_advertisements.show')
    @include('managements.advertisements.advertisements.show')
    @include('managements.providers_management.providers.show')
    @include('managements.providers_management.branches.show')
@endsection
@section('script')
    @if(in_array('click_item',request()->option ?? []) or is_null(request()->option))
        @include('managements.app.offers.show_js')
        @include('managements.app.products.show_js')
        @include('managements.encoding.categories.show_js')
        @include('managements.advertisements.paid_advertisements.show_js')
        @include('managements.advertisements.advertisements.show_js')
        @include('managements.providers_management.providers.show_js')
        @include('managements.providers_management.branches.show_js')
    @endif

    <script>
        $("select[name=payment_type]").val("{{request()->payment_type}}");
        $("select[name=status]").val("{{request()->status}}");
        $("select[name=coupon]").val("{{request()->coupon}}");
        $("select[name=gender]").val("{{request()->gender}}");
        $("select[name=type]").val("{{request()->type}}");
        $("select[name=click_type]").val("{{request()->click_type}}");
        $("select[name=provider]").val("{{request()->provider}}");
        $("select[name=branch_name]").val("{{request()->branch_name}}");
        $("select[name=product_id]").html("<option value='{{request()->product_id}}'>{{request()->product_id}}</option>");
        $("select[name=product_price]").html("<option value='{{request()->product_price}}'>{{request()->product_price}}</option>");

        $(document).on('change', '#provider', function () {
            let provider_id = $(this).val();
            let url = "{{url('app/products/get-branch')}}" + "/" + provider_id;

            let select_branch = $("#branch_name");
            if (provider_id) {
                $.ajax({
                    type: "GET",
                    url: url,
                    beforeSend: function () {
                        select_branch.empty();
                    },
                    success: function (data) {
                        select_branch.html('<option value="">{{__('admin.select_option')}}</option>');
                        $.each(data, function (key, value) {
                            select_branch.append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    },
                });
            }
        });

        if ($('select[name=type]').val() !== 'total') {
            $('#check_click_item').prop('checked', true);
        }


        /*ajax falter product start*/
        $(document).on('change', 'select[name=type]', function () {
            let val = $(this).val();
            if (val === 'total') {
                $('#check_click_item').prop('checked', false);
                return $('#options').attr('hidden', false)
            }
            $('#check_click_item').prop('checked', true);
            return $('#options').attr('hidden', true)
        });

        /*ajax falter product end*/
    </script>
@endsection
