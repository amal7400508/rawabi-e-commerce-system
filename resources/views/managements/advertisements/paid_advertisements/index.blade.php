@php($page_title = __('admin.paid_advertisements'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.advertisements')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.paid_advertisements')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create paid_advertisements')
                                            <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.paid_advertisement')}}</a>
                                        @endcan
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('paid_advertisements')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number"
                                                                            @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="type" @if($search_type == 'type') selected @endif>{{__('admin.type')}}</option>
                                                                    <option value="description" @if($search_type == 'description') selected @endif>{{__('admin.description')}}</option>
                                                                    <option value="end_date" @if($search_type == 'end_date') selected @endif>{{__('admin.end_date')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.branch_name')}}</th>
                                                        <th>{{__('admin.type')}}</th>
                                                        <th>{{__('admin.format')}}</th>
                                                        <th>{{__('admin.title')}}</th>
                                                        <th>{{__('admin.price')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $paid_advertisements)
                                                        <tr style="{{$paid_advertisements->background_color_row}}">
                                                            <td hidden>{{$paid_advertisements->updated_at}}</td>
                                                            <td>{{$paid_advertisements->id}}</td>
                                                            <td>{{$paid_advertisements->branch->name}}</td>
                                                            <td>{{$paid_advertisements->advertisementType->name}}</td>
                                                            <td>{{$paid_advertisements->type}}</td>
                                                            <td>{{$paid_advertisements->title}}</td>
                                                            <td>{{$paid_advertisements->price}}</td>
                                                            <td>
                                                                {!! $paid_advertisements->actions !!}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.paid_advertisements')}}</h5>
                            <a class="heading-elements-toggle"><i
                                    class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements" id="check_show">
                                <input type="checkbox" name="status" value="1"
                                       class="switchBootstrap" id="switchBootstrap18"
                                       data-on-color="primary" data-off-color="danger"
                                       data-on-text="{{__('admin.enable')}}"
                                       data-off-text="{{__('admin.disable')}}" data-color="primary"
                                       data-label-text="{{__('admin.status')}}"
                                       checked/>
                            </div>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="type">{{__('admin.type')}} <span class="danger">*</span></label>
                                                        <select name="type" id="type" class="form-control" required>
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($advertisement_types as $advertisement_type)
                                                                <option value="{{$advertisement_type->id}}">{{$advertisement_type->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="branch_name">{{__('admin.branch_name')}} <span class="danger">*</span></label>
                                                        <select name="branch_name" id="branch_name" class="form-control" required>
                                                            <option value="">{{__('admin.select_option')}}</option>
                                                            @foreach($branches as $branch)
                                                                <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                {{--
                                                                                                <div class="col-md-12" hidden>
                                                                                                    <div class="form-group">
                                                                                                        <label for="edit_image_ar"
                                                                                                               id="label-image">{{__('admin.image')}} <span
                                                                                                                class="danger">*</span></label>
                                                                                                        <input type="file" accept=".jpg, .jpeg, .png"
                                                                                                               id="edit_image_ar"
                                                                                                               class="form-control"
                                                                                                               name="image" onchange="loadAvatar(this);"
                                                                                                               required
                                                                                                        >
                                                                                                        <button type="button" hidden onclick="delete_img()"
                                                                                                                style="position: static; "
                                                                                                                class="btn btn-sm btn-outline-danger"
                                                                                                                id="delete-img"><i class="ft-trash"></i>
                                                                                                        </button>

                                                                                                        <img id="avatar" style="max-width: 140px; height: auto; margin:10px;">
                                                                                                        <span class="error-message">
                                                                                                            <strong></strong>
                                                                                                        </span>
                                                                                                    </div>
                                                                                                </div>
                                                --}}
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="start-date">{{__('admin.start_date')}}<span class="danger">*</span></label>
                                                        <input type="date" id="start-date"
                                                               class="form-control"
                                                               name="start_date"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="end-date">{{__('admin.end_date')}}</label>
                                                        <input type="date" id="end-date"
                                                               class="form-control"
                                                               name="end_date"
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="price">{{__('admin.price')}}<span class="danger">*</span></label>
                                                        <input type="number"
                                                               id="price"
                                                               class="form-control"
                                                               name="price"
                                                               required
                                                        >
                                                        <span class="error-message">
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="level">{{__('admin.level')}} <span class="danger">*</span></label>
                                                <select name="level" id="level" class="form-control" required>
                                                    <option value="">{{__('admin.select_option')}}</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>

                                            <div class="form-group">
                                                <label for="format">{{__('admin.format')}} <span class="danger">*</span></label>
                                                <select name="format" id="format" class="form-control" required>
                                                    <option value="">{{__('admin.select_option')}}</option>
                                                    <option value="text">{{__('admin.text')}}</option>
                                                    <option value="image">{{__('admin.a_image')}}</option>
                                                    <option value="video">{{__('admin.video')}}</option>
                                                </select>
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label for="title">{{__('admin.title')}} <span class="danger">*</span></label>
                                                <input type="text"
                                                       id="title"
                                                       class="form-control"
                                                       name="title"
                                                       required
                                                >
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                            <div class="form-group" id="advertisement_content">
                                                <label for="content">{{__('admin.content')}} <span class="danger">*</span></label>
                                                <textarea id="content" name="content" rows="6" class="form-control" required>
                                                </textarea>
                                                <span class="error-message">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="progress-wrp">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated"></div>
                                        <div class="status">0%</div>
                                    </div>
                                    <style>

                                        #progress-wrp {
                                            padding: 1px;
                                            position: relative;
                                            border-radius: 3px;
                                            margin: 10px;
                                            text-align: left;
                                            background: #fff;
                                            box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
                                        }

                                        #progress-wrp .progress-bar {
                                            height: 20px;
                                            border-radius: 3px;
                                            background-color: #214679;
                                            width: 0;
                                            box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
                                        }

                                        #progress-wrp .status {
                                            top: 3px;
                                            left: 50%;
                                            position: absolute;
                                            display: inline-block;
                                            color: #fff;
                                        }
                                    </style>
                                    {{--<div class="progress">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100" style="width:20%"></div>
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
    @include('managements.advertisements.paid_advertisements.show')

@endsection
@section('script')
    @include('managements.advertisements.paid_advertisements.js')
    @include('managements.advertisements.paid_advertisements.show_js')
@endsection
