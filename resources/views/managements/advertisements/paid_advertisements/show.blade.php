{{-- start model show  Message--}}
<div id="ModalShowPaidAdvertisement" class="modal fade text-left" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="">
                <div class="card-content">
                    <button type="button" class="btn btn-toolbar-danger  close position-absolute ranges"
                            style=" @if( app()->getLocale() == 'ar')  margin-right: 472px; @else margin-left: 480px @endif"
                            data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin: 6px">&times;</span>
                    </button>
                    <div class="form-details card-body p-0">
                        <span id="show_status"></span>
                        <div class="text-center">
                            <div class="align-items-center" style="min-height: 100px;overflow: hidden;display: flex; text-align: center;">
                                <iframe id="show-video" controls autoplay="0" style="margin:0 auto; height: auto; min-height: 300px; width: 100%; border: 0" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>

                                <img src="" class="img-fluid rounded" hidden alt="Card image cap" id="show-image-paid-advertisement"
                                     onerror="this.src='{{asset('storage/logo.png')}}'"
                                     style="margin: 0 auto;"
                                >
                            </div>
                        </div>
                        <div class="row p-1">
                            <div class="col-sm-12">
                                <h2 class="product-title" id="offer_name">Card title</h2>
                                <span id="id" class="btn btn-sm btn-outline-primary float-right font-default" title="{{__('admin.id')}}" style="margin-left: 2px; margin-right: 2px"></span>
                                <a style="font-size: 16px">
                                    <div class="price-reviews">
                                                    <span class="price-box float-left">
                                                        <span class="price font-default" id="span-price" style="color: #00b0ff"></span>
                                                        <span class="old-price font-default" id="old_price"></span>
                                                    </span>
                                        <span class="float-right" id="name_type" title="{{__('admin.type')}}"></span>
                                    </div>
                                </a>
                                <p>{{__('admin.from')}} {{__('admin.date_1')}} <span class="font-default" id="start_date"></span> {{__('admin.to')}} {{__('admin.date_1')}}
                                    <span class="font-default" id="end_date"></span></p>
                                <p id="content-text"></p>
                            </div>
                            <hr class="col-md-12 p0 m0">
                            <div class="col-md-12">
                                <small class="price category-color">{{__('admin.created_at')}}:</small>
                                <small class="price category-color created"></small>
                                <br>
                                <small class="price category-color">{{__('admin.updated_at')}}:</small>
                                <small class="price category-color updated_at"></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model show  Message--}}
