<script>
    let table_show = $('#table-show');
    let table_count = $('#table-count');

    function delete_img() {
        document.getElementById('edit_image_ar').value = null;
        $('#avatar').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
    }

    function loadAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                var image = document.getElementById('avatar');
                image.src = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function delete_video() {
        document.getElementById('content-video').value = null;
        $('#source-video').attr('hidden', true).attr('src', '');
        $('#delete-video').attr('hidden', true);
    }

    function loadVideo(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#source-video').attr('hidden', false);
                $('#delete-video').attr('hidden', false);
                let video = document.getElementById('source-video');
                video.src = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
        }

        let vid = document.getElementById("source-video");
        vid.muted = true;
    }

    function emptyProgress() {
        $('#progress-wrp').attr('hidden', true);
        $(".progress-bar").css("width", +0 + "%");
        $(".status").text(0 + "%");
    }

    @can('create paid_advertisements')
    $(document).on('click', '#openAddModal', function () {
        $('#format').attr('disabled', false);
        emptyProgress();
        $('#advertisement_content').html(appendContentByFormat());
        $('#form input:not(:first), #form textarea, #form select')
            .val('')
            .removeClass('is-invalid');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);
        /*delete_img();*/
        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('paid_advertisements.store')}}")
            .attr("data_type", "add");
    });
    @endcan

    @can('update paid_advertisements')
    /*start code edit*/
    let advertisement_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        advertisement_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('advertisements/paid_advertisements/edit')}}" + '/' + advertisement_id;
        $('#form input:not(:first), #form textarea,  #form select')
            .val('')
            .removeClass('is-invalid');
        $('#avatar').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('advertisements/paid_advertisements/update')}}" + '/' + advertisement_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);

            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            let format = data.type;
            emptyProgress();
            document.getElementById('type').value = data.advertisement_type_id;
            document.getElementById('branch_name').value = data.branch_id;
            document.getElementById('price').value = data.price;
            document.getElementById('level').value = data.rank;
            document.getElementById('format').value = format;
            $('#format').attr('disabled', true);
            document.getElementById('title').value = data.title;
            document.getElementById('start-date').value = data.start_date;
            document.getElementById('end-date').value = data.end_date;
            $('#advertisement_content').html(appendContentByFormat(format));

            if (format === 'text') {
                document.getElementById('content').value = data.content;
            } else if (format === 'image') {
                $('#avatar').attr('src', "{{asset('storage/paid_advertisements')}}" + "/" + data.content).attr('hidden', false);
                $('#delete-img').attr('hidden', false);
            } else if (format === 'video') {
                $('#source-video').attr('src', "{{asset('storage/paid_advertisements')}}" + "/" + data.content).attr('hidden', false);
                $('#delete-video').attr('hidden', false);
            }

            if (data.status == 1) {
                $('#switchBootstrap18').attr('value', 1).attr('checked', true)
                    .bootstrapSwitch('state', true);
            } else {
                $('#switchBootstrap18').attr('value', 1).attr('checked', false)
                    .bootstrapSwitch('state', false);

            }


            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/
    @endcan

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('advertisement_id', advertisement_id);

        try {
            let data = await addOrUpdateOverride(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                /*let response_data = JSON.parse((data.responseText)).advertisement;*/
                let response_data = data.paid_advertisement;

                $('#addModal').modal('hide');

                let tr_color_red = response_data.class_color_row;
                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td>" + response_data.id + "</td>");
                let col2 = $("<td>" + response_data.branch_name + "</td>");
                let col3 = $("<td>" + response_data.advertisement_type_name + "</td>");
                let col4 = $("<td>" + response_data.type + "</td>");
                let col5 = $("<td>" + response_data.title + "</td>");
                let col6 = $("<td>" + response_data.price + "</td>");
                let col7 = $("<td>" + response_data.actions + "</td>");
                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5, col6, col7).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5, col6, col7);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.type !== undefined) {
                $('#form select[name=type]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.type);
            }
            if (obj.branch_name !== undefined) {
                $('#form select[name=branch_name]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.branch_name);
            }
            if (obj.start_date !== undefined) {
                $('#form input[name=start_date]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.start_date);
            }
            if (obj.end_date !== undefined) {
                $('#form input[name=end_date]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.end_date);
            }
            if (obj.price !== undefined) {
                $('#form input[name=price]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.price);
            }
            if (obj.level !== undefined) {
                $('#form select[name=level]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.level);
            }
            if (obj.format !== undefined) {
                $('#form select[name=format]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.format);
            }
            if (obj.title !== undefined) {
                $('#form input[name=title]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.title);
            }
            if (obj.content !== undefined) {
                $('#advertisement_content input , #advertisement_content textarea').addClass('is-invalid')
                    .parent().find('span strong').text(obj.content);
            }
        }
    });
    /*end code add or update*/

    @can('delete paid_advertisements')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        console.log(id_row);
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('advertisements/paid_advertisements/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            this_row.remove();
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan

    $(document).on('change', '#format', async function () {
        let format = $(this).val();
        $('#advertisement_content').html(appendContentByFormat(format));
    });

    function appendContentByFormat(format = 'text') {
        let span = '<span class="error-message"><strong></strong></span>';
        let content;
        if (format === 'image') {
            content =
                '<label for="edit_image_ar" id="label-image">{{__('admin.image')}} <span class="danger">*</span></label>' +
                '<input type="file" accept=".jpg, .jpeg, .png" id="edit_image_ar" class="form-control"' +
                '  name="content" onchange="loadAvatar(this);" >' +
                '<button type="button" hidden onclick="delete_img()" style="position: static; "' +
                '  class="btn btn-sm btn-outline-danger"' +
                '  id="delete-img"><i class="ft-trash"></i>' +
                '</button>' +
                '<img id="avatar" style="max-width: 140px; height: auto; margin:10px;">'
            ;
            return content + span;
        } else if (format === 'video') {
            content =
                '<label for="content-video" id="label-video">{{__('admin.video')}} <span class="danger">*</span></label>' +
                '<input type="file" accept="video/*" id="content-video" class="form-control"' +
                '  name="content" onchange="loadVideo(this);"' +
                '  >' +
                '<button type="button" hidden onclick="delete_video()" style="position: static;margin-top: -286px;"' +
                '  class="btn btn-sm btn-outline-danger"' +
                '  id="delete-video"><i class="ft-trash"></i>' +
                '</button>' +
                '<iframe hidden controls autoplay="0" style="height: auto; margin:10px;" id="source-video"' +
                'src="https://www.youtube.com/embed/tgbNymZ7vqY"' +
                '></iframe>';

            return content + span;
        }

        content =
            '<label for="content">{{__('admin.content')}} <span class="danger">*</span></label>' +
            '<textarea id="content" name="content" rows="6" class="form-control" required>' +
            '</textarea>';

        return content + span

    }

    /**
     * Adding new data or update existing data.
     * The data is sent in a POST method.
     * Override
     *
     * @param  url  string => url path
     * @param  data array => the data http request
     * @param  type string => Add or Update
     * @param  id_button string
     * @returns  {Promise<unknown>}
     */
    function addOrUpdateOverride(url, data, type, id_button) {
        let btn_lang_before = "<?php echo e(__('admin.adding')); ?>";
        let btn_lang = '<i class="ft-save"></i> ' + "<?php echo e(__('admin.add')); ?>";
        if (type === 'Update') btn_lang_before = "<?php echo e(__('admin.updating')); ?>";
        if (type === 'Update') btn_lang = '<i class="ft-edit"></i> ' + "<?php echo e(__('admin.edit')); ?>";

        $('#progress-wrp').attr('hidden', false);

        return new Promise((resolve, reject) => {
            $.ajax({
                url: url,
                method: "POST",
                data: data,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $('#' + id_button).html('<i class="la la-spinner spinner"></i><span>' + btn_lang_before + '...</span>');
                    $('input, select').removeClass('is-invalid');
                    $('span strong').empty();
                },
                xhr: function () {
                    /*upload Progress*/
                    var xhr = $.ajaxSettings.xhr();
                    if (xhr.upload) {
                        xhr.upload.addEventListener('progress', function (event) {
                            var percent = 0;
                            var position = event.loaded || event.position;
                            var total = event.total;
                            if (event.lengthComputable) {
                                percent = Math.ceil(position / total * 100);
                            }
                            /*update progressbar*/
                            $(".progress-bar").css("width", +percent + "%");
                            $(".status").text(percent + "%");
                        }, true);
                    }
                    return xhr;
                },
                success: function (data) {
                    emptyProgress();
                    $('#' + id_button).html(btn_lang);
                    $('#row-not-found').remove();
                    let myhtml = document.createElement("div");
                    myhtml.innerHTML = data.message;
                    swal({
                        icon: "success",
                        title: data.title + "!",
                        /*text: data.message + ".",*/
                        content: myhtml,
                        button: '<?php echo e(__('admin.ok')); ?>'
                    });
                    resolve(data);
                },

                error: function (data) {
                    emptyProgress();
                    if (data.status !== 401) {
                        $('#message-loading-or-error').html('<?php echo e(__('admin.loading_failed')); ?> <i class="ft-alert-triangle color-red"></i>');
                        $('#confirm-modal-loading-show').modal('show');
                    }
                    $('#' + id_button).html(btn_lang);
                    reject(data);
                }
            });
        });
    }


</script>
