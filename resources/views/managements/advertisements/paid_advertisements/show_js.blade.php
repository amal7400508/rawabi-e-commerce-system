<script>
    /*start code show details ajax*/
    let advertisement_details_id;
    $(document).on('click', '.show-detail-paid-advertisement', async function () {
        advertisement_details_id = $(this).attr('id');
        let url = "{{url('advertisements/paid_advertisements/show')}}" + '/' + advertisement_details_id;

        try {
            let data = await responseEditOrShowData(url);
            if (data.status == 1) {
                console.log(1 + ' : 1');
                $('#show_status').html('<span class="btn btn-primary position-absolute round" style=" margin: 10px; " ><i class="ft-unlock"></i></span>');
            } else {
                $('#show_status').html('<span class="btn btn-danger position-absolute round" style=" margin: 10px; " ><i class="ft-lock"></i></span>');
            }
            let format = data.format;

            $('#id').text(data.id);
            $('#span-price').text("RY" + data.price);
            $('#start_date').text(data.start_date);
            $('#end_date').text(data.end_date);
            $('#offer_name').text(data.title);
            $('#name_type').html('<button class="btn btn-sm btn-outline-blue" id="name_type">' + data.advertisement_type + '</button>');
            $('.created').text(data.created_at);
            $('.updated_at').html(data.updated_at);
            $('#content-text').text(data.text);
            if (format === 'text') {
                $('#show-image-paid-advertisement').attr('hidden', true);
                $('#show-video').attr('hidden', true);
            } else if (format === 'image') {
                $('#show-video').attr('hidden', true);
                $('#show-image-paid-advertisement').attr('hidden', false).attr('src', data.content);
            } else if (format === 'video') {
                $('#show-image-paid-advertisement').attr('hidden', true);
                $('#show-video').attr('hidden', false).attr('src', data.content);

            }
            $('#ModalShowPaidAdvertisement').modal('show');
        } catch (error) {
            return error;
        }
    });
    /*end code show details ajax*/
</script>
