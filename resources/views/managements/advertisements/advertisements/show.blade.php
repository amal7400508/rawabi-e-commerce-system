{{-- start model show  Message--}}
<div id="ModalShowAdvertisement" class="modal fade text-left" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="">
                <div class="card-content">
                    <div class="form-details card-body p-0">
                        <div class="align-items-center">
                            <button type="button" class="close badge badge-danger position-absolute round"
                                    style="float: left" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="">&times;</span>
                            </button>
                            <img src="" class="img-fluid rounded image_show" alt="Card image cap"
                                 onerror="this.src='{{asset('storage/thumbnail/640/defualt_offer.png')}}'">
                        </div>
                        <div class="row p-1">
                            <div class="col-sm-12">
                                <h3 id="type-text"></h3>
                                <span class="desc price" style="color: #0072ff"></span>
                                <p id="state_id"></p>
                                <hr>
                                <h6 class="end_date product-title" style=" color: #0072ff"></h6>
                                <hr>
                            </div>
                            <div class="col-sm-9">
                                <small class="price category-color">{{__('admin.created_at')}}:</small>
                                <small class="price category-color created"></small>
                                <br>
                                <small class="price category-color">{{__('admin.updated_at')}}:</small>
                                <small class="price category-color updated_at"></small>
                            </div>
                            <div class="btn_dele col-sm-3 text-center">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end model show  Message--}}
