<script>
    /*start code show details ajax*/
    var advertisement_details_id;
    $(document).on('click', '.show_advertisement', function () {
        advertisement_details_id = $(this).attr('id');
        $.ajax({
            type: 'GET',
            url: "{{url('/advertisements/advertisements/show')}}" + "/" + advertisement_details_id,
            dataType: "json",
            beforeSend: function () {
                $('#message-loading-or-error').html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                $('#confirm-modal-loading-show').modal('show');
            },
            success: function (data) {
                $('#confirm-modal-loading-show').modal('hide');
                $('.form-details #type-text').text(
                    data.type === 'popup' ? "{{__('admin.popup')}}"
                        : "{{__('admin.slider_img')}}"
                );
                {{--if ( data.states != null){--}}
                {{--    $('#state_id').html("{{__('admin.available_in_city')}} : ");--}}
                {{--    for (let i = 0; i < data.states.length; i++){--}}
                {{--        $('#state_id').append(data.states[i].state_name + ' . ');--}}
                {{--    }--}}
                {{--}--}}
                $('.form-details .desc').text(data.desc);
                $('.form-details .id').text(data.id);
                $('.form-details .end_date').html("{{__('admin.end_date')}}: " + data.end_date);
                $('.form-details .image').text(data.image);
                $('.created').text(data.created_at);
                $('.updated_at').html(data.updated_at);
                $('.btn_dele').html(data.btn);
                $('#ModalShowAdvertisement').modal('show');
                $('.image_show').attr('src', '{{asset('/storage')}}' + '/' + data.image);
            },
            error: function (data) {
                $('#message-loading-or-error').html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                $('#confirm-modal-loading-show').modal('show');
            }
        })
    });
    /*end code show details ajax*/
</script>
