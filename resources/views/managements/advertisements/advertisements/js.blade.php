<script>
    let table_show = $('#table-show');
    let table_count = $('#table-count');

    function delete_img() {
        document.getElementById('edit_image_ar').value = null;
        $('#avatar').attr('hidden', true);
        $('#delete-img').attr('hidden', true);
    }

    function loadAvatar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar').attr('hidden', false);
                $('#delete-img').attr('hidden', false);
                var image = document.getElementById('avatar');
                image.src = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    @can('create advertisements')
    $(document).on('click', '#openAddModal', function () {
        $('#form input:not(:first), #form textarea, #form select')
            .val('')
            .removeClass('is-invalid');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);
        delete_img();
        $('#addModal').modal('show');
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('advertisements.store')}}")
            .attr("data_type", "add");
    });
    @endcan

    @can('update advertisements')
    /*start code edit*/
    let advertisement_id = 0;
    let edit_row;
    $(document).on('click', '.edit-table-row', async function () {
        advertisement_id = $(this).attr('id');
        edit_row = $(this).parent().parent();
        let url = "{{url('advertisements/advertisements/edit')}}" + '/' + advertisement_id;
        $('#form input:not(:first), #form textarea')
            .val('')
            .removeClass('is-invalid');
        $('#avatar').attr('hidden', true);
        $('#form span strong').empty();
        $('#btn-save').html('<i class="ft-edit"></i> ' + "{{__('admin.edit')}}")
            .attr("data_url", "{{url('advertisements/advertisements/update')}}" + '/' + advertisement_id)
            .attr("data_type", "Update");

        try {
            let data = await responseEditOrShowData(url);

            $('#edit_image_ar').attr('required', false);
            $('#label-image').text("{{__('admin.image')}}");
            $('#button_save').html("<i class='ft-edit'></i> {{__('admin.edit')}}");

            document.getElementById('description_ar').value = data.desc_ar;
            document.getElementById('description_en').value = data.desc_en;
            document.getElementById('end_date').value = data.end_date;
            document.getElementById('slide_number').value = data.slide_number;
            document.getElementById('type').value = data.type;

            $('#avatar').attr('src', data.image).attr('hidden', false);
            $('#delete-img').attr('hidden', false);

            $('#addModal').modal('show');
        } catch (error) {
            return error;
        }

    });
    /*end code edit*/
    @endcan

    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);
        data_form.append('advertisement_id', advertisement_id);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                /*let response_data = JSON.parse((data.responseText)).advertisement;*/
                let response_data = data.advertisement;

                $('#addModal').modal('hide');

                let tr_color_red = response_data.class_color_row;
                let desc = response_data.desc == null ? '' : response_data.desc;
                let end_date = response_data.end_date == null ? '' : response_data.end_date;
                let slide_number = response_data.slide_number == null ? '' : response_data.slide_number;
                let adv_type = response_data.type === 'slider' ? '{{__('admin.slider_img')}}' : '{{__('admin.popup')}}';

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td class='font-default'>" + response_data.id + "</td>");
                let col2 = $("<td>" + adv_type + "</td>");
                let col3 = $("<td>" + desc + "</td>");
                let col4 = $("<td>" + end_date + "</td>");
                let col5 = $("<td>" + slide_number + "</td>");
                let col6 = $('<td><img class="img-index-40" src="' + response_data.image_path + '"' +
                    'onerror=this.src="{{asset('storage')}}/icon.png"/></td>'
                );
                let col7 = $("<td>" + response_data.actions + "</td>");

                let this_row;
                if (type === 'add') {
                    row.append(col1, col2, col3, col4, col5, col6,col7).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5, col6,col7);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.description_ar !== undefined) {
                $('#form textarea[name=description_ar]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.description_ar);
            }
            if (obj.description_en !== undefined) {
                $('#form textarea[name=description_en]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.description_en);
            }
            if (obj.image !== undefined) {
                $('#form input[name=image]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.image);
            }
            if (obj.end_date !== undefined) {
                $('#form input[name=end_date]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.end_date);
            }
            if (obj.type !== undefined) {
                $('#form select[name=type]').addClass('is-invalid').parent()
                    .find('span strong').text(obj.type);
            }
        }
    });
    /*end code add or update*/

    @can('delete advertisements')
    /*start code Delete ajax*/
    $(document).on('click', '.delete', async function () {
        let id_row = $(this).attr('id');
        let this_row = $(this).parent().parent();
        this_row.addClass('tr-color-active');
        let route = "{{url('advertisements/advertisements/destroy')}}" + "/" + id_row;
        try {
            await deletedItems(id_row, route);
            this_row.remove();
            table_show.text(parseInt(table_show.text()) - 1);
            table_count.text(parseInt(table_count.text()) - 1);
        } catch (e) {
            this_row.removeClass('tr-color-active');
            return e;
        }
    });
    /*end code Delete ajax*/
    @endcan

</script>
