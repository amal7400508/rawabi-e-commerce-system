@php($page_title = __('admin.Evaluation'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.customer_reviews')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.Evaluation')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('rates')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="request_number"
                                                                            @if($search_type == 'request_number') selected @endif>{{__('admin.request_no')}}</option>
                                                                    <option value="user_name"
                                                                            @if($search_type == 'user_name') selected @endif>{{__('admin.user_name')}}</option>
                                                                    <option value="phone" @if($search_type == 'phone') selected @endif>{{__('admin.phone')}}</option>
                                                                    <option value="carrier" @if($search_type == 'carrier') selected @endif>{{__('admin.carrier')}}</option>
                                                                    <option value="branch" @if($search_type == 'branch') selected @endif>{{__('admin.branch')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table table-white-space  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        {{--                                                        <th style="width: 10px">#</th>--}}
                                                        <th>{{__('admin.request')}}</th>
                                                        <th>{{__('admin.user_name')}}</th>
                                                        <th>{{__('admin.phone')}}</th>
                                                        <th>{{__('admin.delivery_driver')}}</th>
                                                        <th>{{__('admin.branch')}}</th>
                                                        <th width="100px">{{__('admin.service')}}</th>
                                                        <th width="100px">{{__('admin.branch')}}</th>
                                                        <th width="100px">{{__('admin.delivery_driver')}}</th>
                                                        <th width="150px">{{__('admin.the_comment')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $rates)
                                                        <tr style="{{$rates->background_color_row}}">
                                                            <td hidden>{{$rates->updated_at}}</td>
                                                            <td>{{$rates->request->request_number}}</td>
                                                            <td>{{$rates->user->full_name ?? null}}</td>
                                                            <td>{{$rates->user->phone ?? null}}</td>
                                                            <td>{{$rates->carrier->name ?? null}}</td>
                                                            <td>{{$rates->branch->name ?? null}}</td>
                                                            <td>{!! appendRate(getRateTypeAndRateId('3', $rates->id)) !!}</td>
                                                            <td>{!! appendRate(getRateTypeAndRateId('2', $rates->id)) !!}</td>
                                                            <td>{!! appendRate(getRateTypeAndRateId('1', $rates->id)) !!}</td>
                                                            <td>{{$rates->comment}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="{{asset('app-assets/js/scripts/pages/invoices-list.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
@endsection
