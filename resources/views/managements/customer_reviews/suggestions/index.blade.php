@php($page_title = __('admin.Suggestions_Complaints'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.customer_reviews')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.Suggestions_Complaints')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('suggestions')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="name" @if($search_type == 'name') selected @endif>{{__('admin.name')}}</option>
                                                                    <option value="phone" @if($search_type == 'phone') selected @endif>{{__('admin.phone')}}</option>
                                                                    <option value="branch" @if($search_type == 'branch') selected @endif>{{__('admin.branch')}}</option>
                                                                    <option value="text" @if($search_type == 'text') selected @endif>{{__('admin.complaint')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.name')}}</th>
                                                        <th>{{__('admin.phone')}}</th>
                                                        <th>{{__('admin.branch')}}</th>
                                                        <th>{{__('admin.complaint')}}</th>
                                                        <th>{{__('admin.action')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $suggestions)
                                                        <tr style="{{$suggestions->background_color_row}}">
                                                            <td hidden>{{$suggestions->updated_at}}</td>
                                                            <td class="font-default">{{$suggestions->id}}</td>
                                                            <td>{{$suggestions->user->full_name}}</td>
                                                            <td>{{$suggestions->user->phone}}</td>
                                                            <td>{!!$suggestions->branch->name ?? "<span class='font-default' style='color:#c9c9c9;'>" . __('admin.no_data') . "<span>"!!}</td>
                                                            <td>{{$suggestions->text}}</td>
                                                            <td>
                                                                <button type="button" class="show-detail btn btn-sm btn-outline-primary" id="{{$suggestions->id}}">{{__('admin.detail')}}...</button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>


    {{-- start model show  Message--}}
    <div id="confirmModalShow" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="card-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                    </button>
                    <div class="form-details card-body row">
                        <div class="align-items-center" style="height: 300px;overflow: hidden;display: flex; text-align: center;">
                            <img src="" class="img-fluid rounded" alt="Card image cap" id="show-image"
                                 onerror="this.src='{{asset('storage/logo.png')}}'" >
                        </div>
                    </div>
                    <table class="table mb-0">
                        <tr>
                            <th>{{__('admin.id')}}</th>
                            <td id="id"></td>
                        </tr>
                        <tr>
                            <th>{{__('admin.name')}}</th>
                            <td id="name"></td>
                        </tr>
                        <tr>
                            <th>{{__('admin.phone')}}</th>
                            <td id="phone"></td>
                        </tr>
                        <tr>
                            <th>{{__('admin.branch_name')}}</th>
                            <td id="branch_name"></td>
                        </tr>
                        <tr>
                            <th>{{__('admin.complaint')}}</th>
                            <td id="text"></td>
                        </tr>
                    </table>
                    <div class="row p-1">
                        <div class="col-sm-12">
                            <span class="details price" style="color: #0679f0"></span>
                        </div>
                        <div class="col-sm-9">
                            <small class="price category-color">{{__('admin.created_at')}}:</small>
                            <small class="price category-color" id="created"></small>
                            <br>
                            <small class="price category-color">{{__('admin.updated_at')}}:</small>
                            <small class="price category-color" id="updated_at"></small>
                        </div>
                        <div class="col-sm-3 text-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show  Message--}}


@endsection
@section('script')
    <script>
        $(document).on('click', '.show-detail', async function () {
            let suggestion_id = $(this).attr('id');
            let url = "{{url('suggestions/show')}}" + "/" + suggestion_id;
            try {
                let data = await responseEditOrShowData(url);

                $('#show-image').attr('src', '{{asset('/storage')}}' + '/' + data.image);
                $('#id').text(data.id);
                $('#name').text(data.name);
                $('#phone').text(data.phone);
                $('#branch_name').text(data.branch_name);
                $('#text').text(data.text);
                $('#image_show').text(data.image);
                $('#created').text(data.created_at);
                $('#updated_at').html(data.updated_at);
                $('#confirmModalShow').modal('show');

            } catch (e) {
                return e;
            }
        });
    </script>

@endsection
