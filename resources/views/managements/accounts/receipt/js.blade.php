<script>
    let table_show = $('#table-show');
    let table_count = $('#table-count');

    @can('create receipt')
    $(document).on('click', '#openAddModal', function () {
        accountChange('account');
        $('#form input:not(:first), #form textarea, #form select').val('').removeClass('is-invalid');
        $('select[name=currency]').val('ريال يمني');
        $('select[name=type]').val('account');
        $('#form span strong').empty();
        $('#switchBootstrap18').attr('value', 1).attr('checked', true)
            .bootstrapSwitch('state', true);
        $('#btn-save').html('<i class="ft-save"></i> ' + "{{__('admin.add')}}")
            .attr("data_url", "{{route('receipt.store')}}")
            .attr("data_type", "add");

        $('#addModal').modal('show');
    });
    @endcan
    /*start code show details ajax*/
    let carrier_details_id;
    $(document).on('click', '.show-detail', async function () {
        carrier_details_id = $(this).attr('id');
        let url = "{{url('carrier_management/receipt/show')}}" + '/' + carrier_details_id;

        try {
            let data = await responseEditOrShowData(url);
            if (data.status == 1) {
                $('#show_status').html('<i class="la la-unlock-alt color-primary"></i> {{__('admin.active')}}');
            } else {
                $('#show_status').html(' <button class="btn btn-outline-warning btn-sm">{{__('admin.attitude')}}</button>');
            }
            $('#Gender').text(data.gender);
            $('.form-details .name').text(data.name);
            $('.form-details .email').text(data.email);
            $('.form-details .phone').text(data.phone);
            $('#id').text(data.id);
            $('#created').text(data.created_at);
            $('#updated_at').html(data.updated_at);
            $('#image_show').attr('src', data.image);
            $('#confirmModalShow').modal('show');
        } catch (error) {
            return error;
        }
    });
    /*end code show details ajax*/


    @can('create receipt')
    /*start code add or update*/
    $('#form').on('submit', async function (event) {
        event.preventDefault();
        let btn_save = $('#btn-save');
        let url = btn_save.attr('data_url');
        let type = btn_save.attr('data_type');
        let data_form = new FormData(this);

        try {
            let data = await addOrUpdate(url, data_form, type, 'btn-save');
            if (data['status'] == 200) {
                /*let response_data = JSON.parse((data.responseText)).carrier;*/
                let response_data = data.data;

                $('#addModal').modal('hide');

                let account_type = "{{__('admin.branch')}}";
                if (data.balance_transactionable_type === "App\\Models\\Account") {
                    account_type = "{{__('admin.account')}}";
                }

                let tr_color_red = response_data.class_color_row;

                let row = $("<tr id='row_" + response_data.id + "' class='" + tr_color_red + "'></tr>");
                let col1 = $("<td>" + response_data.operation_type.name + "</td>");
                let col2 = $("<td>" + account_type + "</td>");
                let col3 = $("<td>" + response_data.balance_transactionable.name + "</td>");
                let col4 = $("<td>" + response_data.amount + "</td>");

                /*let col6 = $("<td>" + response_data.actions + "</td>");*/

                let this_row;

                if (type === 'add') {
                    row.append(col1, col2, col3, col4).prependTo("#table");
                    this_row = $('#row_' + response_data.id);
                    table_show.text(parseInt(table_show.text()) + 1);
                    table_count.text(parseInt(table_count.text()) + 1);
                } else {
                    this_row = edit_row;
                    edit_row.attr("class", tr_color_red).attr('style', '');
                    edit_row.empty().append(col1, col2, col3, col4, col5, col6);
                }

                this_row.addClass('tr-color-success');
                setTimeout(function () {
                    this_row.removeClass('tr-color-success');
                }, 7000);
            }
        } catch (error) {
            let obj = JSON.parse((error.responseText)).error;
            if (obj.operation_type !== undefined) {
                $('#form select[name=operation_type]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.operation_type);
            }
            if (obj.type !== undefined) {
                $('#form select[name=type]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.type);
            }
            if (obj.account !== undefined) {
                $('#form select[name=account]').addClass('is-invalid')
                    .parent().find('span strong').text(obj.account);
            }
            if (obj.amount !== undefined) {
                $('#form input[name=amount]').addClass('is-invalid')
                    .parent().parent().find('span strong').text(obj.amount);
            }
        }
    });
    /*end code add or update*/
    @endcan

    /*ajax falter products start*/
    $(document).on('change', '#type', function () {
        let type = $(this).val();
        accountChange(type);
    });

    function accountChange(type) {
        let url = "{{url('receipt/wallet-filter')}}" + "/" + type;
        if (type) {
            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function () {
                    $("#span_loader").html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    $("#account").empty();
                },
                success: function (data) {
                    $("#span_loader").empty();
                    $("#account").html('<option value="">{{__('admin.select_option')}}</option>');
                    $.each(data, function (key, value) {
                        $("#account").append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                },
                error: function () {
                    $("#span_loader").html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                }
            });
        } else {

        }
    }

</script>

