@php($page_title = __('admin.bonds'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/selects/select2.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.accounts')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active font-Dinar">{{__('admin.bonds')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        @can('create receipt')
                                            <a class="btn btn-primary" id="openAddModal"><i class="ft-plus position-right"></i> {{__('admin.create')}} {{__('admin.receipt')}}</a>
                                        @endcan
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('carriers')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a>
                                                </li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                    <div class="col-sm-12 col-md-9">
                                                        <form style="display: flex;justify-content: start;">
                                                            @php($search_type = isset($_GET['search_type']) ? $_GET['search_type']: '')
                                                            <div class="col-sm-12 col-md-4">
                                                                <select style="width: 100px" name="search_type" aria-controls="user_table"
                                                                        class="custom-select custom-select-sm form-control form-control-sm pull-right">
                                                                    <option value="number" @if($search_type == 'number') selected @endif>{{__('admin.id')}}</option>
                                                                    <option value="name" @if($search_type == 'name') selected @endif>{{__('admin.name')}}</option>
                                                                    <option value="phone" @if($search_type == 'phone') selected @endif>{{__('admin.phone')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6">
                                                                <input type="search" class="form-control form-control-sm"
                                                                       placeholder="{{__('admin.search')}}"
                                                                       name="query"
                                                                       aria-controls="user_table"
                                                                       value="{{isset($_GET['query']) ? $_GET['query']: ''}}">
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <button type="submit" class="btn btn-sm btn-primary col-12">{{__('admin.search')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                                <table width="100%" id="table" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        {{--                                                        <th style="width: 10px">#</th>--}}
                                                        <th>{{__('admin.receipt_type')}}</th>
                                                        <th>{{__('admin.type')}}</th>
                                                        <th>{{__('admin.account')}}</th>
                                                        <th>{{__('admin.amount')}}</th>
{{--                                                        <th>{{__('admin.receipt')}}</th>--}}
                                                        {{--                                                        <th>{{__('admin.action')}}</th>--}}
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data) == 0)
                                                        <tr id="row-not-found">
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $receipt)
                                                        <tr style="{{$receipt->background_color_row}}">
                                                            <td hidden>{{$receipt->updated_at}}</td>
                                                            {{--                                                            <td class="font-default">{{$receipt->id}}</td>--}}
                                                            <td>{{$receipt->operationType->name}}</td>
                                                            <td>{{$receipt->balance_transactionable_type == 'App\Models\Account' ?  __('admin.account') : __('admin.branch')}} </td>
                                                            <td>{{$receipt->balanceTransactionable->name ?? null}}</td>
                                                            <td>{{currency($receipt->currency).' '.$receipt->amount}}</td>
{{--                                                            <td>{!! $receipt->actions !!}</td>--}}
                                                            {{-- <td>
                                                                 {!! $receipt->actions !!}
                                                             </td>--}}
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--/ Invoices table -->
                                            {{ $data->appends($pagination_links)->links() }}
                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <form class="form" enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="modal-header" style="padding-bottom: 0px;">
                            <h5 class="card-title"
                                id="basic-layout-form" style="padding-top: 5px;">{{__('admin.create')}} {{__('admin.receipt')}}</h5>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="form-body">
                                    <div class="form-group row">
                                        <div class="col-md-1 mx-auto"></div>
                                        <label class="col-md-2 label-control">{{__('admin.operation_type')}} <span class="danger">*</span></label>
                                        <div class="col-md-7 mx-auto">
                                            <select class="form-control bg-cyan" id="operation_type" name="operation_type" required>
                                                <option value="">{{__('admin.select_option')}}</option>
                                                <option value="receipt">{{__('admin.receipt')}} @if(app()->getLocale() == 'ar') صرف @endif</option>
                                                <option value="catch_receipt">{{__('admin.catch_receipt')}}</option>
                                            </select>
                                            <span class="error-message">
                                                <strong></strong>
                                            </span>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-1 mx-auto"></div>
                                        <label class="col-md-2 label-control">{{__('admin.type')}} <span class="danger">*</span></label>
                                        <div class="col-md-7 mx-auto">
                                            <select class="form-control" id="type" name="type" required>
                                                <option value="account">{{__('admin.account')}}</option>
                                                <option value="branch">{{__('admin.a_branch')}}</option>
                                            </select>
                                            <span class="error-message">
                                                <strong></strong>
                                            </span>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-1 mx-auto"></div>
                                        <label class="col-md-2 label-control">{{__('admin.accounts')}} <span class="danger">*</span></label>
                                        <div class="col-md-7 mx-auto">
                                            <select class="select2 form-control font-default" id="account" name="account" required>
                                                <option value="">{{__('admin.select_option')}}</option>
                                                @foreach($accounts as $account)
                                                    <option value="{{$account->id}}">{{$account->name}}</option>
                                                @endforeach
                                            </select>
                                            <span id="span_loader" style="font-size: 12px; margin: auto 5px"></span>
                                            <span class="error-message">
                                                <strong></strong>
                                            </span>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-1 mx-auto"></div>
                                        <label class="col-md-2 label-control">{{__('admin.currency')}} <span class="danger">*</span></label>
                                        <div class="col-md-7 mx-auto">
                                            <select class="select2 form-control font-default" id="currency" name="currency" required>
                                                <option value="ريال يمني">ريال يمني</option>
                                                <option value="دولار أمريكي">دولار أمريكي</option>
                                                <option value="ريال سعودي">ريال سعودي</option>
                                            </select>
                                            <span id="span_loader" style="font-size: 12px; margin: auto 5px"></span>
                                            <span class="error-message">
                                                <strong></strong>
                                            </span>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                    <div class="form-group row ">
                                        <div class="col-md-1 mx-auto"></div>
                                        <label class="col-md-2 label-control">{{__('admin.amount')}} <span class="danger">*</span></label>
                                        <div class="col-md-7 mx-auto">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text font-default">RY</span></div>
                                                <input type="number" class="form-control" name="amount" min="0" step="any" required>
                                                <div class="input-group-append"><span class="input-group-text font-default">.00</span></div>
                                            </div>
                                            <span class="error-message">
                                                <strong></strong>
                                            </span>
                                        </div>
                                        <div class="col-md-2 mx-auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <fieldset id="button-container"
                                      class="form-group position-relative has-icon-left mb-0">
                                <button type="submit" class="btn btn-primary" id="btn-save"><i
                                        class="ft-save"></i> {{__('admin.save')}}</button>
                            </fieldset>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    {{-- start model show  Message--}}
    <div id="confirmModalShow" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="card-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="margin:7px;padding-top: 10px">&times;</span>
                    </button>
                    <div class="form-details card-body row">
                        <div class="user-display col-4" style="    padding-right: 30px;    padding-top: 10px;">
                            <img src="" class="rounded-circle img-thumbnail img-user-show-100" id="image_show"
                                 onerror="this.src='{{asset('storage/default-carrier.jpg')}}'"
                            >
                        </div>
                        <div class="user-display col-8 p-0">
                            <h2 class="name product-title"
                                style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif padding-top: 25px;"></h2>
                            <h6 class="email product-title font-default"
                                style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                            <h6 class="phone product-title font-default"
                                style="@if( app()->getLocale() == 'en') padding-left: 25px; @else padding-right: 25px; @endif"></h6>
                        </div>
                    </div>
                    <table class="table mb-0">
                        <tr>
                            <th>{{__('admin.id')}}</th>
                            <td id="id"></td>
                        </tr>
                        <tr>
                            <th>{{__('admin.gender')}}</th>
                            <td id="Gender"></td>
                        </tr>
                        <tr>
                            <th>{{__('admin.status')}}</th>
                            <td id="show_status"></td>
                        </tr>
                    </table>
                    <div class="row p-1">
                        <div class="col-sm-12">
                            <span class="details price" style="color: #0679f0"></span>
                        </div>
                        <div class="col-sm-9">
                            <small class="price category-color">{{__('admin.created_at')}}:</small>
                            <small class="price category-color" id="created"></small>
                            <br>
                            <small class="price category-color">{{__('admin.updated_at')}}:</small>
                            <small class="price category-color" id="updated_at"></small>
                        </div>
                        <div class="col-sm-3 text-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end model show  Message--}}


@endsection
@section('script')
    @include('managements.accounts.receipt.js')
    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
    <script>

        @can('create receipt')
        /*start code Delete ajax*/
        $(document).on('click', '.delete', async function () {
            let id_row = $(this).attr('id');
            let this_row = $(this).parent().parent();
            this_row.addClass('tr-color-active');
            let route = "{{url('receipt/destroy')}}" + "/" + id_row;
            try {
                await deletedItems(id_row, route);
                table_show.text(parseInt(table_show.text()) - 1);
                table_count.text(parseInt(table_count.text()) - 1);
                this_row.remove();
                count = 0;
                $("#openAddModal").attr('hidden', false);
            } catch (e) {
                this_row.removeClass('tr-color-active');
                return e;
            }
        });
        /*end code Delete ajax*/
        @endcan
    </script>
@endsection
