@php($page_title = __('admin.bonds'))
@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/selects/select2.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block"> {{__('admin.accounts')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home"></i></a></li>
                            <li class="breadcrumb-item active">{{__('admin.bonds')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <i class="la la-money"></i>
                        <label class="card-title" for="text">{{__('admin.bonds')}}</label>
                        <hr>
                    </div>
                    <div class="card-body">
                        <form id="form">
                            @csrf
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="col-md-3 label-control">{{__('admin.operation_type')}} <span class="danger">*</span></label>
                                    <div class="col-md-6 mx-auto">
                                        <select class="form-control bg-cyan" id="operation_type" name="branch_id" required="">
                                            <option value="">{{__('admin.select_option')}}</option>
                                            <option value="receipt">{{__('admin.receipt')}}</option>
                                            <option value="catch_receipt">{{__('admin.catch_receipt')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 mx-auto"></div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control">{{__('admin.type')}} <span class="danger">*</span></label>
                                    <div class="col-md-6 mx-auto">
                                        <select class="form-control" id="type" name="type" required="">
                                            <option value="account">{{__('admin.account')}}</option>
                                            <option value="branch">{{__('admin.a_branch')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 mx-auto"></div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control">{{__('admin.accounts')}} <span class="danger">*</span></label>
                                    <div class="col-md-6 mx-auto">
                                        <select class="select2 form-control font-default" id="account" name="account" required="">
                                            <option value="">{{__('admin.select_option')}}</option>
                                            @foreach($accounts as $account)
                                                <option value="{{$account->wallet_id}}">{{$account->name}}</option>
                                            @endforeach
                                        </select>
                                        <span id="span_loader" style="font-size: 12px; margin: auto 5px"></span>
                                    </div>
                                    <div class="col-md-3 mx-auto"></div>
                                </div>
                                <div class="form-group row ">
                                    <label class="col-md-3 label-control">{{__('admin.amount')}} <span class="danger">*</span></label>
                                    <div class="col-md-6 mx-auto">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text font-default">RY</span></div>
                                            <input type="number" class="form-control" value="" name="amount">
                                            <div class="input-group-append"><span class="input-group-text font-default">.00</span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 mx-auto"></div>
                                </div>

                                <div class="form-actions">
                                    <div class="col-md-3 mx-auto pull-right m-1"></div>
                                    <button type="submit" class="col-md-2 btn btn-primary pull-right m-1" id="btn-save"><i class="ft ft-save"></i> {{__('admin.save')}}</button>
                                    <button type="button" class="col-md-1 btn btn-dark pull-right m-1"><i class="ft-x"></i> {{__('admin.cancel')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        /*start code add or update*/
        $('#form').on('submit', async function (event) {
            event.preventDefault();
            let btn_save = $('#btn-save');
            let url = "{{route('receipt.store')}}";
            let data_form = new FormData(this);

            try {
                let data = await addOrUpdate(url, data_form, 'add', 'btn-save');
                if (data['status'] == 200) {
                    console.log(data);
                }
            } catch (error) {
                console.log(error);
            }
        });
        /*end code add or update*/


        /*ajax falter products start*/
        $(document).on('change', '#type', function () {
            let type = $(this).val();
            let url = "{{url('receipt/wallet-filter')}}" + "/" + type;
            if (type) {
                $.ajax({
                    type: "GET",
                    url: url,
                    beforeSend: function () {
                        $("#span_loader").html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                    },
                    success: function (data) {
                        $("#span_loader").empty();
                        $("#account").empty();
                        $.each(data, function (key, value) {
                            $("#account").append('<option value="' + value.wallet_id + '">' + value.name + '</option>');
                        });
                    },
                    error: function () {
                        $("#span_loader").html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    }
                });
            } else {

            }
        });
    </script>

    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
@endsection
