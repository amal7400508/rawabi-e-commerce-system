@php($page_title = __('admin.report'))
@php($route = route('accounts.export'))
@extends('include.report')
@section('content')
    <div class="row">
        <div class="col-12">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-center" style="padding-bottom: 0px;">
                                <h4 class="card-title">{{__('admin.account_movement')}}
                                    <span class="color-blue">({{$branch_name}})</span>
                                </h4>
                                <hr>
                                <h4 class="card-title">{{__('admin.from')}} {{__('admin.date_1')}} <span class="font-default color-blue">{{request()->from ?? "_________"}}</span> {{__('admin.to')}} {{__('admin.date_1')}} <span class="font-default color-blue">{{request()->to ?? \Carbon\Carbon::now()->toDateString()}}</span></h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table width="100%" class="table  zero-configuration">
                                            <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>{{__('admin.type')}}</th>
                                                <th>{{__('admin.account_name')}}</th>
                                                <th>{{__('admin.operation_type')}}</th>
                                                <th>{{__('admin.debit')}}</th>
                                                <th>{{__('admin.creditor')}}</th>
                                                <th>{{__('admin.Date')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($data ) == 0)
                                                <tr>
                                                    <td colspan="9" class="text-center">
                                                        {{__('admin.no_data')}}
                                                        <hr>
                                                    </td>
                                                </tr>
                                            @endif
                                            @php($sum_debit_YER = 0)
                                            @php($sum_creditor_YER = 0)
                                            @php($sum_debit_SAR = 0)
                                            @php($sum_creditor_SAR = 0)
                                            @php($sum_debit_DULAR = 0)
                                            @php($sum_creditor_DULAR = 0)
                                            @php($currencyArray = [])
                                            @php($debit_amount = '')
                                            @php($creditor_amount = '')
                                            @foreach($data as $balance_transaction)
                                                @php($currencyArray[] = $balance_transaction->currency)
                                                @php($currency = $balance_transaction->currency)
                                                @php($creditor_amount = '')
                                                @php($debit_amount = '')
                                                @if($balance_transaction->type == 'subtract')
                                                    @if ($currency == "دولار أمريكي")
                                                        @php($sum_debit_DULAR += $balance_transaction->amount)
                                                    @elseif ($currency == "ريال يمني")
                                                        @php($sum_debit_YER += $balance_transaction->amount)
                                                    @elseif ($currency == "ريال سعودي")
                                                        @php($sum_debit_SAR += $balance_transaction->amount)
                                                    @endif
                                                    @php($debit_amount = currency($balance_transaction->currency).' '.$balance_transaction->amount)
                                                @endif
                                                @if($balance_transaction->type == 'add')
                                                    @if ($currency == "دولار أمريكي")
                                                        @php($sum_creditor_DULAR += $balance_transaction->amount)
                                                    @elseif ($currency == "ريال يمني")
                                                        @php($sum_creditor_YER += $balance_transaction->amount)
                                                    @elseif ($currency == "ريال سعودي")
                                                        @php($sum_creditor_SAR += $balance_transaction->amount)
                                                    @endif
                                                    {{--                                                    @php($sum_creditor += $balance_transaction->amount)--}}
                                                    @php($creditor_amount = currency($balance_transaction->currency).' '.$balance_transaction->amount)
                                                @endif
                                                <tr style="{{$balance_transaction->background_color_row}}">
                                                    @if(request()->report_type != 'total')
                                                        <td hidden>{{$balance_transaction->updated_at}}</td>
                                                        <td>{{$balance_transaction->id}}</td>
                                                        <td>{{$balance_transaction->wallet_type}}</td>
                                                        <td>{{$balance_transaction->wallet->walletable->name ?? null}}</td>
                                                        <td>{{$balance_transaction->operationType->name ?? null}}</td>
                                                        <td>{{$debit_amount}}</td>
                                                        <td>{{$creditor_amount}}</td>
                                                        <td>{{$balance_transaction->created_at}}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot style="font-weight: 600">
                                            @if(in_array('ريال يمني', $currencyArray))
                                                <tr style="background-color: #dce8e0">
                                                    <td colspan="4">{{__('admin.total_price')}} ريال يمني</td>
                                                    <td>{{$sum_debit_YER.' YER'}}</td>
                                                    <td>{{$sum_creditor_YER.' YER'}}</td>
                                                    <t
                                                    </td>
                                                </tr>
                                            @endif
                                            @if(in_array('دولار أمريكي', $currencyArray))
                                                <tr style="background-color: #dce8e0">
                                                    <td colspan="4">{{__('admin.total_price')}} دولار</td>
                                                    <td>{{$sum_debit_DULAR.' $'}}</td>
                                                    <td>{{$sum_creditor_DULAR.' $'}}</td>
                                                    <td></td>
                                                </tr>
                                            @endif
                                            @if(in_array('ريال سعودي', $currencyArray))
                                                <tr style="background-color: #dce8e0">
                                                    <td colspan="4">{{__('admin.total_price')}} ريال سعودي</td>
                                                    <td>{{$sum_debit_SAR.' SAR'}}</td>
                                                    <td>{{$sum_creditor_SAR.' SAR'}}</td>
                                                    <td></td>
                                                </tr>
                                            @endif


                                            @php($sum_YER = $sum_debit_YER - $sum_creditor_YER)
                                            @php($debit_YER = 0)
                                            @php($creditor_YER = 0)
                                            @if($sum_YER > 0)
                                                @php($debit_YER = abs($sum_YER))
                                            @else
                                                @php($creditor_YER = abs($sum_YER))
                                            @endif

                                            @php($sum_SAR = $sum_debit_SAR - $sum_creditor_SAR)
                                            @php($debit_SAR = 0)
                                            @php($creditor_SAR = 0)
                                            @if($sum_SAR > 0)
                                                @php($debit_SAR = abs($sum_SAR))
                                            @else
                                                @php($creditor_SAR = abs($sum_SAR))
                                            @endif

                                            @php($sum_DULAR = $sum_debit_DULAR - $sum_creditor_DULAR)
                                            @php($debit_DULAR = 0)
                                            @php($creditor_DULAR = 0)
                                            @if($sum_DULAR > 0)
                                                @php($debit_DULAR = abs($sum_DULAR))
                                            @else
                                                @php($creditor_DULAR = abs($sum_DULAR))
                                            @endif

                                            @if(in_array('ريال يمني', $currencyArray))
                                                <tr style="background-color: #fff4de">
                                                    <td colspan="4">{{__('admin.current_balance')}} ريال يمني</td>
                                                    <td>{{$debit_YER.' YER'}}</td>
                                                    <td>{{$creditor_YER.' YER'}}</td>
                                                    <td></td>
                                                </tr>
                                            @endif
                                            @if(in_array('دولار أمريكي', $currencyArray))
                                                <tr style="background-color: #fff4de">
                                                    <td colspan="4">{{__('admin.current_balance')}} دولار</td>
                                                    <td>{{$debit_DULAR.' $'}}</td>
                                                    <td>{{$creditor_DULAR.' $'}}</td>
                                                    <td></td>
                                                </tr>
                                            @endif
                                            @if(in_array('ريال سعودي', $currencyArray))
                                                <tr style="background-color: #fff4de">
                                                    <td colspan="4">{{__('admin.current_balance')}} ريال سعودي</td>
                                                    <td>{{$debit_SAR.' SAR'}}</td>
                                                    <td>{{$creditor_SAR.' SAR'}}</td>
                                                    <td></td>
                                                </tr>
                                            @endif

                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
