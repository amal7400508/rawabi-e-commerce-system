@php($page_title = __('admin.accounts'))
@extends('layouts.main')
@section('content')
    <div class="content-wrapper">
        <div class="content-header row mb-1">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">{{__('admin.accounts')}}</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item span"><a href="{{url('/dashboard')}}"><i class="la la-home mh"></i></a>
                            </li>
                            <li class="breadcrumb-item active">{{__('admin.accounts')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.account_movement')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('accounts')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <form method="get">
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label>{{__('admin.id')}}</label>
                                                                <input type="number" class="form-control" value="{{request()->id}}" name="id">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label>{{__('admin.type')}}</label>
                                                                <select class="form-control" name="type" id="type">
                                                                    <option value="">{{__('admin.select_option')}}</option>
                                                                    <option value="account">{{__('admin.account')}}</option>
                                                                    <option value="branch">{{__('admin.a_branch')}}</option>
                                                                    <option value="user">{{__('admin.user')}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="" hidden>
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.provider')}}</label>
                                                                        <select class="form-control" name="provider" id="provider">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            @foreach($providers as $provider)
                                                                                <option value="{{$provider->id}}">{{$provider->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.account')}}</label>
                                                                        <select class="form-control" name="account" id="account">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            @foreach($accounts as $account)
                                                                                <option value="{{$account->id}}">{{$account->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <span id="span_loader" style="font-size: 12px;margin: -13px 21px;padding-bottom: 20px;"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.from')}}</label>
                                                                        <input type="date" class="form-control"
                                                                               value="{{request()->from}}" name="from">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.to')}}</label>
                                                                        <input type="date" class="form-control"
                                                                               value="{{request()->to}}" name="to">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.operation_type')}}</label>
                                                                        <select class="form-control" name="operation_type">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            @foreach($operation_type as $operation_types)
                                                                                <option
                                                                                    value="{{$operation_types->id}}">{{$operation_types->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.currency')}}</label>
                                                                        <select class="form-control" name="currency">
                                                                            <option value="">{{__('admin.select_option')}}</option>
                                                                            <option value="ريال يمني">ريال يمني</option>
                                                                            <option value="دولار أمريكي">دولار أمريكي</option>
                                                                            <option value="ريال سعودي">ريال سعودي</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>{{__('admin.report_type')}}</label>
                                                                        <select class="form-control" name="report_type">
                                                                            <option value="detailed">{{__('admin.detailed')}}</option>
                                                                            <option value="total">{{__('admin.a_total')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions pull-right">
                                                    {{--<button type="submit" formaction="{{route('accounts')}}" class=" btn btn-primary" id="button_preview"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-eye"></i> {{__('admin.preview')}}
                                                    </button>--}}
                                                    <button type="submit" formtarget="_blank" formaction="{{route('accounts.report')}}" class=" btn btn-primary" id="button_export"
                                                            style="margin-bottom: 10px; width: 120px">
                                                        <i class="ft-printer"></i> {{__('admin.report')}}
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic form layout section start -->
            <div class="row">
                <div class="col-md-12">
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">{{__('admin.post_index')}} {{__('admin.account_movement')}}</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a href="{{route('accounts')}}" style="color: #6B6F82;"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <div class="row">
                                                    @include('include.table_length')
                                                </div>
                                                <table width="100%" class="table  zero-configuration">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>{{__('admin.type')}}</th>
                                                        <th>{{__('admin.account_name')}}</th>
                                                        <th>{{__('admin.operation_type')}}</th>
                                                        <th>{{__('admin.debit')}}</th>
                                                        <th>{{__('admin.creditor')}}</th>
                                                        <th>{{__('admin.Date')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($data ) == 0)
                                                        <tr>
                                                            <td colspan="9" class="text-center">
                                                                {{__('admin.no_data')}}
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @foreach($data as $balance_transaction)
                                                        <tr style="{{$balance_transaction->background_color_row}}">
                                                            <td hidden>{{$balance_transaction->updated_at}}</td>
                                                            <td>{{$balance_transaction->id}}</td>
                                                            <td>{{$balance_transaction->wallet_type}}</td>
                                                            <td>{{$balance_transaction->wallet->walletable->name ?? null}}</td>
                                                            <td>{{$balance_transaction->operationType->name ?? null}}</td>
                                                            <td>
                                                                @if($balance_transaction->type == 'subtract')
                                                                    {{currency($balance_transaction->currency).' '.$balance_transaction->amount}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($balance_transaction->type == 'add')
                                                                    {{currency($balance_transaction->currency).' '.$balance_transaction->amount}}
                                                                @endif
                                                            </td>
                                                            <td>{{$balance_transaction->created_at}}</td>

                                                            {{--                                                            <td>{{$balance_transaction->amount}}</td>--}}
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            {{ $data->appends($pagination_links)->links() }}

                                            <span>
                                                {{__('admin.show')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-show">{{$data->count()}}</span>
                                                {{__('admin.out_of')}}
                                                <span class="rows-count-current font-default text-bold-600" id="table-count">{{$data_count}}</span>
                                                {{__('admin.record')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        /*$("select[name=operation_type]").val("{{request()->operation_type}}");
         $("select[name=type]").val("{{request()->type}}");
         $("select[name=branch_name]").val("{{request()->branch_name}}");*/

        $(document).on('change', '#provider', function () {
            let provider_id = $(this).val();
            let url = "{{url('accounts/branch-filter')}}" + "/" + provider_id;
            accountChange(provider_id, url);
        });

        $(document).on('change', '#type', function () {
            let type = $(this).val();
            let url = "{{url('accounts/wallet-filter')}}" + "/" + type;
            accountChange(type, url);
            if (type === 'branch') {
                $('select[name=provider]').parent().parent().attr('hidden', false).addClass('col-md-6');
                $('select[name=account]').parent().parent().attr('class', 'col-md-6');
            } else {
                $('select[name=provider]').parent().parent().attr('hidden', true).removeClass('col-md-6');
                $('select[name=account]').parent().parent().attr('class', 'col-md-12');
            }
        });

        function accountChange(type, url) {
            if (type) {
                $.ajax({
                    type: "GET",
                    url: url,
                    beforeSend: function () {
                        $("#span_loader").html('{{__('admin.loading')}} <i class="la la-spinner spinner"></i>');
                        $("#account").empty();
                    },
                    success: function (data) {
                        $("#span_loader").empty();
                        $("#account").html('<option value="">{{__('admin.select_option')}}</option>');
                        $.each(data, function (key, value) {
                            $("#account").append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    },
                    error: function () {
                        $("#span_loader").html('{{__('admin.loading_failed')}} <i class="ft-alert-triangle color-red"></i>');
                    }
                });
            }
        }

    </script>
@endsection
