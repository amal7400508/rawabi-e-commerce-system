<!-- BEGIN: Main Menu-->

{{--<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">--}}
{{--<div class="main-menu menu-fixed menu-accordion menu-shadow menu-light" data-scroll-to-active="true">--}}
<div class="main-menu menu-fixed menu-accordion menu-shadow menu-light menu-bordered" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            @can('show dashboard')
                <li class="@if($page_title == __('admin.Dashboard')) active @endif">
                    <a class="menu-item"
                       @if($page_title == __('admin.Dashboard')) onclick="event.preventDefault()"
                       @endif href="{{url('/')}}"><i class="la la-home"></i>
                        <span>{{__('admin.Dashboard')}}</span></a>
                </li>
            @endcan
            @canany(['show commissions','show main_categories','show working_times','show working_times_branch','show categories','show areas','show unit_types',
            'show units','show offer_types','show notes','show pharmacy_categories','show main_category_categories',
            'show branch_services','show social_medias','show ceiling','show advertisements','show material','show color'])

                <li class="nav-item">
                    <a href="#">
                        <i class="la la-television"></i>
                        <span class="menu-title" data-i18n="nav.templates.main">{{__('admin.encodings')}}</span>
                    </a>
                    <ul class="menu-content">
                        @can('show main_categories')
                            <li class="@if($page_title == __('admin.main_categories')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.main_categories')) onclick="event.preventDefault()"
                                   @endif href="{{route('encoding.main-categories')}}"><i class="icon-sid la la-sitemap"></i>
                                    <span>{{__('admin.main_categories')}}</span></a>
                            </li>
                        @endcan

                            @can('show categories')
                                <li class="@if($page_title == __('admin.sub_categories')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.sub_categories')) onclick="event.preventDefault()"
                                       @endif href="{{route('categories')}}"><i class="icon-sid ft-list"></i>
                                        <span>{{__('admin.sub_categories')}}</span>
                                    </a>
                                </li>
                            @endcan

                        @can('show working_times')
                            <li class="@if($page_title == __('admin.working_times')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.working_times')) onclick="event.preventDefault()"
                                   @endif href="{{route('working_times')}}"><i class="icon-sid ft-clock"></i>
                                    <span>{{__('admin.working_times')}}</span></a>
                            </li>
                        @endcan
                        @can('show working_times_branch')
                            <li class="@if($page_title == __('admin.working_times_branch')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.working_times_branch')) onclick="event.preventDefault()"
                                   @endif href="{{route('working_times_branch')}}"><i class="icon-sid ft-clock"></i>
                                    <span>{{__('admin.working_times_branch')}}</span></a>
                            </li>
                        @endcan

                        @can('show areas')
                            <li class="@if($page_title == __('admin.areas')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.areas')) onclick="event.preventDefault()"
                                   @endif href="{{route('areas')}}"><i class="icon-sid ft-map-pin"></i>
                                    <span>{{__('admin.areas')}}</span>
                                </a>
                            </li>
                        @endcan
                        {{--                    <li class="@if($page_title == __('admin.service_types')) active @endif">--}}
                        {{--                        <a class="menu-item"--}}
                        {{--                           @if($page_title == __('admin.service_types')) onclick="event.preventDefault()"--}}
                        {{--                           @endif href="{{route('service_types')}}"><i class="icon-sid la la-server"></i>--}}
                        {{--                            <span>{{__('admin.service_types')}}</span>--}}
                        {{--                        </a>--}}
                        {{--                    </li>--}}
                        @can('show unit_types')
                            <li class="@if($page_title == __('admin.unit_types')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.unit_types')) onclick="event.preventDefault()"
                                   @endif href="{{route('unit_types')}}"><i class="icon-sid icon-grid"></i>
                                    <span>{{__('admin.unit_types')}}</span>
                                </a>
                            </li>
                        @endcan

                        @can('show units')
                            <li class="@if($page_title == __('admin.units')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.units')) onclick="event.preventDefault()"
                                   @endif href="{{route('units')}}"><i class="icon-sid ft-pie-chart"></i>
                                    <span>{{__('admin.units')}}</span>
                                </a>
                            </li>
                        @endcan
                            @can('show color')
                                <li class="@if($page_title == __('admin.colors')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.colors')) onclick="event.preventDefault()"
                                       @endif href="{{route('color')}}"><i class="icon-sid ft-droplet "></i>
                                        <span>{{__('admin.colors')}}</span>
                                    </a>
                                </li>
                            @endcan
                        @can('show offer_types')
                            <li class="@if($page_title == __('admin.deal_of_the_day_types')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.deal_of_the_day_types')) onclick="event.preventDefault()"
                                   @endif href="{{route('offer_types')}}"><i class="icon-sid la la-gift"></i>
                                    <span>{{__('admin.deal_of_the_day_types')}}</span>
                                </a>
                            </li>
                        @endcan
                        @can('show notes')
                            <li class="@if($page_title == __('admin.notes')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.notes')) onclick="event.preventDefault()"
                                   @endif href="{{route('notes')}}"><i class="icon-sid la la-archive"></i>
                                    <span>{{__('admin.notes')}}</span>
                                </a>
                            </li>
                        @endcan
                        @can('show pharmacy_categories')
                            <li class="@if($page_title == __('admin.branches_categories')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.branches_categories')) onclick="event.preventDefault()"
                                   @endif href="{{route('pharmacy_categories')}}"><i class="icon-sid icon-grid"></i>
                                    <span>{{__('admin.branches_categories')}}</span>
                                </a>
                            </li>
                        @endcan

                        @can('show commissions')
                            <li class="@if($page_title == __('admin.commissions')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.commissions')) onclick="event.preventDefault()"
                                   @endif href="{{route('commissions')}}"><i class="icon-sid icon-grid"></i>
                                    <span>{{__('admin.commissions')}}</span>
                                </a>
                            </li>
                        @endcan
                        @can('show main_category_categories')
                            <li class="@if($page_title == __('admin.main_category_categories')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.main_category_categories')) onclick="event.preventDefault()"
                                   @endif href="{{route('main_category_categories')}}"><i class="icon-sid icon-grid"></i>
                                    <span>{{__('admin.main_category_categories')}}</span>
                                </a>
                            </li>
                        @endcan
                        {{--<li class="@if($page_title == __('admin.pharmacy_categories')) active @endif">
                            <a class="menu-item"
                               @if($page_title == __('admin.pharmacy_categories')) onclick="event.preventDefault()"
                               @endif href="{{route('pharmacy_categories')}}"><i class="icon-sid icon-grid"></i>
                                <span>{{__('admin.pharmacy_categories')}}</span>
                            </a>
                        </li>
    --}}
                        @can('show branch_services')
                            <li class="@if($page_title == __('admin.branch_services')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.branch_services')) onclick="event.preventDefault()"
                                   @endif href="{{route('branch_services')}}"><i class="icon-sid icon-grid"></i>
                                    <span>{{__('admin.branch_services')}}</span>
                                </a>
                            </li>
                        @endcan
                        @can('show social_medias')
                            <li class="@if($page_title == __('admin.social_medias')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.social_medias')) onclick="event.preventDefault()"
                                   @endif href="{{route('social_medias')}}"><i class="icon-sid la la-connectdevelop"></i>
                                    <span>{{__('admin.social_medias')}}</span>
                                </a>
                            </li>
                        @endcan
                        @can('show ceiling')
                            <li class="@if($page_title == __('admin.minimum_order')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.minimum_order')) onclick="event.preventDefault()"
                                   @endif href="{{route('ceiling')}}"><i class="icon-sid la la-dollar"></i>
                                    <span>{{__('admin.minimum_order')}}</span>
                                </a>
                            </li>
                        @endcan


                            @can('show advertisements')
                                <li class="@if($page_title == __('admin.advertisement')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.advertisement')) onclick="event.preventDefault()"
                                       @endif href="{{route('encoding.new_advertisement')}}"><i class="icon-sid la la-image"></i>
                                        <span>{{__('admin.advertisement')}}</span>
                                    </a>
                                </li>
                            @endcan

                            @can('show material')
                                <li class="@if($page_title == __('admin.material')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.material')) onclick="event.preventDefault()"
                                       @endif href="{{route('material')}}"><i class="icon-sid icon-grid"></i>
                                        <span>{{__('admin.material')}}</span>
                                    </a>
                                </li>
                            @endcan
                    </ul>
                </li>
            @endcanany
{{--            @canany([--}}
{{--            'show advertisement_types',--}}
{{--            'show advertisements',--}}
{{--            'show paid_advertisements',--}}
{{--            ])--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="#">--}}
{{--                        <i class="la la-tachometer"></i>--}}
{{--                        <span class="menu-title">{{__('admin.advertisements')}}</span>--}}
{{--                    </a>--}}

{{--                    <ul class="menu-content">--}}
{{--                        @can('show advertisement_types')--}}
{{--                            <li class="@if($page_title == __('admin.advertisement_types')) active @endif">--}}
{{--                                <a class="menu-item"--}}
{{--                                   @if($page_title == __('admin.advertisement_types')) onclick="event.preventDefault()"--}}
{{--                                   @endif href="{{route('advertisement_types')}}"><i class="icon-sid ft-list"></i>--}}
{{--                                    <span>{{__('admin.advertisement_types')}}</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @endcan--}}

{{--                        @can('show paid_advertisements')--}}
{{--                            <li class="@if($page_title == __('admin.paid_advertisements')) active @endif">--}}
{{--                                <a class="menu-item"--}}
{{--                                   @if($page_title == __('admin.paid_advertisements')) onclick="event.preventDefault()"--}}
{{--                                   @endif href="{{route('paid_advertisements')}}"><i class="icon-sid la la-image"></i>--}}
{{--                                    <span>{{__('admin.paid_advertisements')}}</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @endcan--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endcanany--}}

            @canany([
            'show users',
            'show deposits',
            'show user_addresses',
            'show products',
            'show product',
            'show offer',
            'show event_type',
            'show user_event'
            ])
                <li class="nav-item">
                    <a href="#">
                        <i class="icon-screen-smartphone"></i>
                        <span class="menu-title">{{__('admin.user_application')}}</span>
                    </a>

                    <ul class="menu-content">
                        @can('show users')
                            <li class="@if($page_title == __('admin.users')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.users')) onclick="event.preventDefault()"
                                   @endif href="{{route('users')}}"><i class="icon-sid ft-users"></i>
                                    <span>{{__('admin.users')}}</span>
                                </a>
                            </li>
                        @endcan

                            @can('show deposits')
                                <li class="nav-item">
                                    <a href="#">
                                        <i class="icon-sid la la-money"></i>
                                        <span class="menu-title">{{__('admin.deposits')}}</span>
                                    </a>

                                    <ul class="menu-content">
                                        <li class="@if($page_title == __('admin.deposits')) active @endif">
                                            <a class="menu-item"
                                               @if($page_title == __('admin.deposits')) onclick="event.preventDefault()"
                                               @endif href="{{route('deposits')}}"><i class="icon-sid"></i>
                                                <span class="m-lr-25">{{__('admin.deposits')}}</span>
                                            </a>
                                        </li>
                                        <li class="@if($page_title == __('admin.deposit_branches')) active @endif">
                                            <a class="menu-item"
                                               @if($page_title == __('admin.deposit_branches')) onclick="event.preventDefault()"
                                               @endif href="{{route('deposit_branches')}}"><i class="icon-sid"></i>
                                                <span class="m-lr-25">{{__('admin.deposit_branches')}}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            @endcan

                            @can('show event_type')
                                <li class="@if($page_title == __('admin.event_type')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.event_type')) onclick="event.preventDefault()"
                                       @endif href="{{route('event_type')}}"><i class="icon-sid la la-birthday-cake"></i>
                                        <span data-i18n="nav.templates.horz.main">{{__('admin.event_type')}}</span></a></li>
                            @endcan

                            @can('show user_event')
                                <li class="@if($page_title == __('admin.user_occasions')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.user_occasions')) onclick="event.preventDefault()"
                                       @endif href="{{route('user_event')}}"><i class="icon-sid la la-calendar-check-o"></i><span
                                            data-i18n="nav.editors.editor_ckeditor">{{__('admin.user_occasions')}}</span></a>
                                </li>
                            @endcan

                        @can('show user_addresses')
                            <li class="@if($page_title == __('admin.users_locations')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.users_locations')) onclick="event.preventDefault()"
                                   @endif href="{{route('user_addresses')}}"><i class="icon-sid ft-map-pin"></i>
                                    <span>{{__('admin.users_locations')}}</span>
                                </a>
                            </li>
                        @endcan
                        @can('show product')
                            <li class="@if($page_title == __('admin.product_branch')) active @endif">
                                <a class="menu-item" @if($page_title == __('admin.product_branch')) onclick="event.preventDefault()"
                                   @endif href="{{ route('product')}}"><i
                                        class="icon-sid ft ft-shopping-cart"></i><span
                                        data-i18n="nav.editors.editor_ckeditor">{{__('admin.products')}}</span></a></li>

                            @endcan
{{--                            @can('show products')--}}
{{--                                <li class="@if($page_title == __('admin.products')) active @endif">--}}
{{--                                    <a class="menu-item"--}}
{{--                                       @if($page_title == __('admin.products')) onclick="event.preventDefault()"--}}
{{--                                       @endif href="{{route('products')}}"><i class="icon-sid ft ft-shopping-cart"></i>--}}
{{--                                        <span>{{__('admin.products')}}</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            @endcan--}}

{{--                            @can('show products')--}}
{{--                                <li class="@if($page_title == __('admin.newproducts')) active @endif">--}}
{{--                                    <a class="menu-item"--}}
{{--                                       @if($page_title == __('admin.newproducts')) onclick="event.preventDefault()"--}}
{{--                                       @endif href="{{route('newproducts')}}"><i class="icon-sid ft ft-shopping-cart"></i>--}}
{{--                                        <span>{{__('admin.newproducts')}}</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            @endcan--}}

{{--                            @can('show stocks')--}}
{{--                                <li class="@if($page_title == __('admin.opening_balances')) active @endif">--}}
{{--                                    <a class="menu-item"--}}
{{--                                       @if($page_title == __('admin.opening_balances')) onclick="event.preventDefault()"--}}
{{--                                       @endif href="{{route('Stocks')}}"><i class="icon-sid ft ft-arrow-up-right"></i>--}}
{{--                                        <span>{{__('admin.opening_balances')}}</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            @endcan--}}

                            @can('show offer')
                                <li class="@if($page_title == __('admin.deal_of_the_day')) active @endif">
                                    <a class="menu-item"
                                       @if($page_title == __('admin.deal_of_the_day')) onclick="event.preventDefault()"
                                       @endif href="{{route('offers')}}"><i class="icon-sid la la-gift"></i>
                                        <span>{{__('admin.deal_of_the_day')}}</span>
                                    </a>
                                </li>

{{--                                <li class="@if($page_title == __('admin.deal_of_the_day')) active @endif">--}}
{{--                                    <a class="menu-item"--}}
{{--                                       @if($page_title == __('admin.deal_of_the_day')) onclick="event.preventDefault()"--}}
{{--                                       @endif href="{{route('offers')}}"> <i class="icon-sid la la-gift"></i>--}}
{{--                                        <span class="m-lr-25">{{__('admin.deal_of_the_day')}}</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                --}}{{--                                    <li class="@if($page_title == __('admin.offers_branches')) active @endif">--}}
{{--                                --}}{{--                                        <a class="menu-item"--}}
{{--                                --}}{{--                                           @if($page_title == __('admin.offers_branches')) onclick="event.preventDefault()"--}}
{{--                                --}}{{--                                           @endif href="{{route('offers_branches')}}"><i class="icon-sid"></i>--}}
{{--                                --}}{{--                                            <span class="m-lr-25">{{__('admin.offers_branches')}}</span>--}}
{{--                                --}}{{--                                        </a>--}}
{{--                                --}}{{--                                    </li>--}}


                            @endcan





{{--                        @can('show attentions')--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="#">--}}
{{--                                    <i class="icon-sid ft-bell"></i>--}}
{{--                                    <span class="menu-title">{{__('admin.attentions')}}</span>--}}
{{--                                </a>--}}

{{--                                <ul class="menu-content">--}}
{{--                                    <li class="@if($page_title == __('admin.attentions')) active @endif">--}}
{{--                                        <a class="menu-item"--}}
{{--                                           @if($page_title == __('admin.attentions')) onclick="event.preventDefault()"--}}
{{--                                           @endif href="{{route('attentions')}}"> <i class="icon-sid ft-bell"></i>--}}
{{--                                            <span class="m-lr-25">{{__('admin.attentions')}}</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <li class="@if($page_title == __('admin.attentions_branches')) active @endif">--}}
{{--                                        <a class="menu-item"--}}
{{--                                           @if($page_title == __('admin.attentions_branches')) onclick="event.preventDefault()"--}}
{{--                                           @endif href="{{route('attentions_branches')}}"><i class="icon-sid"></i>--}}
{{--                                            <span class="m-lr-25">{{__('admin.attentions_branches')}}</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                        @endcan--}}

                    </ul>
                </li>
            @endcanany
            @canany([
            'show request',
            'show tracking',
           // 'show special_product',
           // 'show deferred',
            'show prescription-replies',
            'show prescription',
            ])
                <li class=" nav-item"><a href="#"><i class="la la-cutlery"></i><span class="menu-title"
                                                                                     data-i18n="nav.internationalization.main">{{__('admin.requests')}}</span></a>
                    <ul class="menu-content">
                        @can('show request')
                            <li class="@if($page_title == __('admin.requests')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.requests')) onclick="event.preventDefault()"
                                   @endif href="{{route('request')}}"><i class="icon-sid ft ft-navigation"></i><span
                                        data-i18n="nav.pickers.pickers_color_picker">{{__('admin.requests')}}</span></a>
                            </li>
                        @endcan
                        @can('show tracking')
                            <li class="@if($page_title == __('admin.tracking_request')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.tracking_request')) onclick="event.preventDefault()"
                                   @endif href="{{ route('tracking') }}"><i class="icon-sid la la-search"></i><span
                                        data-i18n="nav.pickers.pickers_color_picker">{{__('admin.tracking_request')}}</span></a>
                            </li>
                        @endcan
{{--                            @can('show special_product')--}}
{{--                            <li class="@if($page_title == __('admin.Special_requests')) active @endif">--}}
{{--                            <a class="menu-item"--}}
{{--                               @if($page_title == __('admin.Special_requests')) onclick="event.preventDefault()"--}}
{{--                               @endif href="{{ route('special_product')}}"><i--}}
{{--                                    class="icon-sid la la-star"></i><span--}}
{{--                                    data-i18n="nav.editors.editor_ckeditor">{{__('admin.Special_requests')}}</span></a>--}}
{{--                        </li>--}}
{{--                            @endcan--}}
{{--                            @can('show deferred')--}}
{{--                            <li class="@if($page_title == __('admin.deferred_special_requests')) active @endif">--}}
{{--                            <a class="menu-item"--}}
{{--                               @if($page_title == __('admin.deferred_special_requests')) onclick="event.preventDefault()"--}}
{{--                               @endif href="{{ route('special_product.deferred')}}"><i--}}
{{--                                    class="icon-sid la la-clock-o"></i><span--}}
{{--                                    data-i18n="nav.editors.editor_ckeditor">{{__('admin.deferred_special_requests')}}</span></a>--}}
{{--                        </li>--}}
{{--                            @endcan--}}
{{--                            @can('show prescription-replies')--}}
{{--                            <li class="@if($page_title == __('admin.prescription_insurance')) active @endif">--}}
{{--                            <a class="menu-item"--}}
{{--                               @if($page_title == __('admin.prescription_insurance')) onclick="event.preventDefault()"--}}
{{--                               @endif href="{{ route('prescription-replies')}}"><i class="icon-sid la la-star"></i><span--}}
{{--                                    data-i18n="nav.editors.editor_ckeditor">{{__('admin.prescription_insurance')}}</span></a>--}}
{{--                        </li>--}}
{{--                            @endcan--}}
{{--                            @can('show prescription')--}}
{{--                            <li class="@if($page_title == __('admin.prescription')) active @endif">--}}
{{--                            <a class="menu-item"--}}
{{--                               @if($page_title == __('admin.prescription')) onclick="event.preventDefault()"--}}
{{--                               @endif href="{{ route('prescription')}}"><i class="icon-sid la la-clipboard"></i><span--}}
{{--                                    data-i18n="nav.editors.editor_ckeditor">{{__('admin.prescription')}}</span></a>--}}
{{--                        </li>--}}
{{--                                @endcan--}}
                    </ul>
                </li>
            @endcanany
            @canany([
            'show carriers',
            'show delivery_prices',
            ])
                <li class="nav-item">
                    <a href="#">
                        <i class="la la-truck"></i>
                        <span class="menu-title">{{__('admin.carrier_management')}}</span>
                    </a>

                    <ul class="menu-content">
                        @can('show carriers')
                            <li class="@if($page_title == __('admin.carriers')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.carriers')) onclick="event.preventDefault()"
                                   @endif href="{{route('carriers')}}"><i class="icon-sid la la-motorcycle"></i>
                                    <span>{{__('admin.carriers')}}</span>
                                </a>
                            </li>
                        @endcan
                        @can('show delivery_prices')
                            <li class="@if($page_title == __('admin.delivery_prices')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.delivery_prices')) onclick="event.preventDefault()"
                                   @endif href="{{route('delivery_prices')}}"><i class="icon-sid la la-money"></i>
                                    <span>{{__('admin.delivery_prices')}}</span>
                                </a>
                            </li>
                        @endcan
{{--                        @can('show delivery_prices')--}}
{{--                            <li class="@if($page_title == __('admin.multi_delivery')) active @endif">--}}
{{--                                <a class="menu-item"--}}
{{--                                   @if($page_title == __('admin.multi_delivery')) onclick="event.preventDefault()"--}}
{{--                                   @endif href="{{route('multi_delivery')}}"><i class="icon-sid la la-motorcycle"></i>--}}
{{--                                    <span>{{__('admin.multi_delivery')}}</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @endcan--}}
                    </ul>
                </li>
            @endcanany
{{--            @canany([--}}
{{--            'show provider',--}}
{{--            'show branches',--}}
{{--            ])--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="#">--}}
{{--                        <i class="la la-building"></i>--}}
{{--                        <span class="menu-title">{{__('admin.providers_management')}}</span>--}}
{{--                    </a>--}}

{{--                    <ul class="menu-content">--}}
{{--                        @can('show provider')--}}
{{--                            <li class="@if($page_title == __('admin.providers')) active @endif">--}}
{{--                                <a class="menu-item" @if($page_title == __('admin.providers')) onclick="event.preventDefault()"--}}
{{--                                   @endif href="{{route('providers')}}"><i class="icon-sid ft-users"></i>--}}
{{--                                    <span>{{__('admin.providers')}}</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @endcan--}}

{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endcanany--}}
            @canany([
            'show suggestions',
            'show rates',
            ])
                <li class=" nav-item"><a href="#"><i class="la la-thumbs-o-up"></i><span class="menu-title">{{__('admin.customer_reviews')}}</span></a>
                    <ul class="menu-content">
                        @can('show suggestions')
                            <li class="@if($page_title == __('admin.Suggestions_Complaints')) active @endif">
                                <a class="menu-item" @if($page_title == __('admin.Suggestions_Complaints')) onclick="event.preventDefault()"
                                   @endif href="{{route('suggestions')}}"><i class="icon-sid la la-bullhorn"></i>
                                    <span>{{__('admin.Suggestions_Complaints')}}</span>
                                </a>
                            </li>
                        @endcan
                        @can('show rates')
                            <li class="@if($page_title == __('admin.Evaluation')) active @endif">
                                <a class="menu-item" @if($page_title == __('admin.Evaluation')) onclick="event.preventDefault()"
                                   @endif href="{{route('rates')}}"><i class="icon-sid ft-star"></i>
                                    <span>{{__('admin.Evaluation')}}</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcanany
            @canany([
            'show notifications',
            'show notification_movement',
           ' show attentions'
            ])
                <li class=" nav-item"><a href="#"><i class="la la-fire"></i><span class="menu-title" data-i18n="nav.jquery_ui.main">{{__('admin.notifications')}}</span></a>
                    <ul class="menu-content">
                        <li class="@if($page_title == __('admin.notifications')) active @endif">
                            <a class="menu-item"
                               @if($page_title == __('admin.notifications')) onclick="event.preventDefault()"
                               @endif href="{{route('notifications')}}"><i class="icon-sid icon-fire"></i><span>{{__('admin.notifications')}}</span></a>
                        </li>


                        <li class="nav-item">
                            <a href="#">
                                <i class="icon-sid la la-fire"></i>
                                <span class="menu-title">{{__('admin.notifications_movement')}}</span>
                            </a>
                            <ul class="menu-content">
                                @can('show notifications')
                                    <li class="@if($page_title == __('admin.notifications_movement')) active @endif">
                                        <a class="menu-item"
                                           @if($page_title == __('admin.notifications_movement')) onclick="event.preventDefault()"
                                           @endif href="{{route('notification_movement')}}"><i class="icon-sid"></i>
                                            <span class="m-lr-25">{{__('admin.notifications_movement')}}</span>
                                        </a>
                                    </li>
                                @endcan
                                @can('show notification_movement')
                                    <li class="@if($page_title == __('admin.notifications_movement_branches')) active @endif">
                                        <a class="menu-item"
                                           @if($page_title == __('admin.notifications_movement_branches')) onclick="event.preventDefault()"
                                           @endif href="{{route('notification_movement_branches')}}"><i class="icon-sid"></i>
                                            <span class="m-lr-25">{{__('admin.notifications_movement_branches')}}</span>
                                        </a>
                                    </li>
                                @endcan


                            </ul>
                        </li>
                        @can('show attentions')
                            <li class="@if($page_title == __('admin.attentions')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.attentions')) onclick="event.preventDefault()"
                                   @endif href="{{route('attentions')}}"><i class="icon-sid ft-bell"></i>
                                    <span>{{__('admin.attentions')}}</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcanany
            @canany([
            'show coupon_type',
            'show coupons',
            'show volume_discounts',
            'show product_discounts'
            ])
                <li class=" nav-item"><a href="#"><i class="la la-code-fork"></i><span class="menu-title" data-i18n="nav.jquery_ui.main">{{__('admin.coupons').' & '. __('admin.discounts')}}</span></a>
                    <ul class="menu-content">
                        @can('show coupon_type')
                            <li class="@if($page_title == __('admin.coupons_types')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.coupons_types')) onclick="event.preventDefault()"
                                   @endif href="{{route('coupon_type')}}"><i class="icon-sid la la-credit-card"></i><span>{{__('admin.coupons_types')}}</span></a>
                            </li>
                        @endcan

                            @can('show coupons')
                                <li class="nav-item">
                                    <a href="#">
                                        <i class="icon-sid la la-code-fork"></i>
                                        <span class="menu-title">{{__('admin.coupons')}}</span>
                                    </a>
                                    <ul class="menu-content">
                                        <li class="@if($page_title == __('admin.coupons')) active @endif">
                                            <a class="menu-item"
                                               @if($page_title == __('admin.coupons')) onclick="event.preventDefault()"
                                               @endif href="{{route('coupons')}}"><i class="icon-sid"></i>
                                                <span class="m-lr-25">{{__('admin.coupons')}}</span>
                                            </a>
                                        </li>
                                        <li class="@if($page_title == __('admin.coupons_branches')) active @endif">
                                            <a class="menu-item"
                                               @if($page_title == __('admin.coupons_branches')) onclick="event.preventDefault()"
                                               @endif href="{{route('coupons_branches')}}"><i class="icon-sid"></i>
                                                <span class="m-lr-25">{{__('admin.coupons_branches')}}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            @endcan

                        @can('show volume_discounts')
                            <li class="@if($page_title == __('admin.volume_discount')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.volume_discount')) onclick="event.preventDefault()"
                                   @endif href="{{route('volume_discounts')}}"><i class="icon-sid la la-heartbeat"></i><span>{{__('admin.volume_discount')}}</span></a>
                            </li>
                        @endcan
                            @can('show product_discounts')
                            <li class="@if($page_title == __('admin.product_discounts')) active @endif">
                            <a class="menu-item"
                               @if($page_title == __('admin.product_discounts')) onclick="event.preventDefault()"
                               @endif href="{{route('product_discounts')}}"><i class="icon-sid la la-heartbeat"></i><span
                                    data-i18n="nav.templates.horz.main">{{__('admin.product_discounts')}}</span></a>
                        </li>
                                @endcan

                    </ul>
                </li>
            @endcanany
{{--            @canany(['show accounts', 'show receipt'])--}}
{{--                <li class="nav-item"><a href="#"><i class="la la-stack-overflow"></i><span class="menu-title" data-i18n="nav.jquery_ui.main">{{__('admin.accounts')}}</span></a>--}}
{{--                    <ul class="menu-content">--}}
{{--                        @can('show accounts')--}}
{{--                            <li class="@if($page_title == __('admin.accounts')) active @endif">--}}
{{--                                <a class="menu-item"--}}
{{--                                   @if($page_title == __('admin.accounts')) onclick="event.preventDefault()"--}}
{{--                                   @endif href="{{route('accounts')}}"><i class="icon-sid la la-paste"></i>--}}
{{--                                    <span data-i18n="nav.templates.horz.main">{{__('admin.accounts')}}</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @endcan--}}
{{--                        @can('show receipt')--}}
{{--                            <li class="@if($page_title == __('admin.bonds')) active @endif">--}}
{{--                                <a class="menu-item"--}}
{{--                                   @if($page_title == __('admin.bonds')) onclick="event.preventDefault()"--}}
{{--                                   @endif href="{{url('receipt')}}"><i class="icon-sid la la-money"></i>--}}
{{--                                    <span data-i18n="nav.templates.horz.main">{{__('admin.bonds')}}</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @endcan--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @endcanany--}}
{{--                @can('show statistics')--}}
{{--                <li class="@if($page_title ==__('admin.statistics')) active @endif">--}}
{{--                <a class="menu-item"--}}
{{--                   @if($page_title ==__('admin.statistics')) onclick="event.preventDefault()" @endif href="{{route('statistics')}}">--}}
{{--                    <i class="la la-newspaper-o"></i><span class="menu-title" data-i18n="nav.jquery_ui.main">{{__('admin.statistics')}}</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--                @endcan--}}
            @canany(['show users_report', 'show products_report', 'show request_report', 'show carriers_report'])

                <li class=" nav-item"><a href="#"><i class="ft ft-printer"></i><span class="menu-title" data-i18n="nav.jquery_ui.main">{{__('admin.reports')}}</span></a>
                    @can('show users_report')
                        <ul class="menu-content">
                            <li class="@if($page_title == __('admin.user_report')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.user_report')) onclick="event.preventDefault()"
                                   @endif href="{{route('users.report')}}">
                                    <span data-i18n="nav.templates.horz.main">{{__('admin.user_report')}}</span>
                                </a>
                            </li>
                        </ul>
                    @endcan
                    @can('show products_report')
                        <ul class="menu-content">
                            <li class="@if($page_title == __('admin.products_report')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.products_report'))
                                   @endif href="{{route('products.report')}}"><span>{{__('admin.products_report')}}</span>
                                </a>
                            </li>
                        </ul>
                    @endcan
                    @can('show request_report')
                        <ul class="menu-content">
                            <li class="@if($page_title == __('admin.requests_report')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.requests_report'))
                                   @endif href="{{route('request.report')}}"><span>{{__('admin.requests_report')}}</span>
                                </a>
                            </li>
                        </ul>
                    @endcan
                    @can('show carriers_report')
                        <ul class="menu-content">
                            <li class="@if($page_title == __('admin.carrier_report')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.carrier_report'))
                                   @endif href="{{route('carriers.report')}}"><span>{{__('admin.carrier_report')}}</span>
                                </a>
                            </li>
                        </ul>
                    @endcan
                </li>
            @endcanany
            @canany(['show admin','show audit','show permission'])
                <li class="nav-item"><a href="#"><i class="ft-users"></i><span class="menu-title" data-i18n="nav.jquery_ui.main">{{__('admin.employees_affairs')}}</span></a>
                    <ul class="menu-content">
                        @can('show admin')
                            <li class="@if($page_title == __('admin.employees')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.employees')) onclick="event.preventDefault()"
                                   @endif href="{{ route('admin')}}"><i class="icon-sid ft-users"></i><span
                                        data-i18n="nav.templates.horz.main">{{__('admin.employees')}}</span></a>
                            </li>
                        @endcan
                        @can('show audit')
                            <li class="@if($page_title == __('admin.audit')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.audit')) onclick="event.preventDefault()"
                                   @endif href="{{ route('audit')}}"><i
                                        class="icon-sid la la-binoculars"></i><span
                                        data-i18n="nav.templates.horz.main">{{__('admin.audit')}}</span></a></li>
                        @endcan
                        @can('show permission')
                            <li class="@if($page_title == __('admin.permission')) active @endif">
                                <a class="menu-item"
                                   @if($page_title == __('admin.permission').' | '.__('admin.permission')) onclick="event.preventDefault()"
                                   @endif href="{{ route('permission')}}"><i
                                        class="icon-sid la la-unlock"></i><span
                                        data-i18n="nav.templates.horz.main">{{__('admin.permission')}}</span></a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcanany
            <li class="nav-item"><a href="#"><i class="la la-user"></i>
                    <span class="menu-title" data-i18n="nav.jquery_ui.main">{{__('admin.profile')}}</span></a>
                <ul class="menu-content">
                    @can('show branches')
                        <li class="@if($page_title == __('admin.branches')) active @endif">
                            <a class="menu-item" @if($page_title == __('admin.branches')) onclick="event.preventDefault()"
                               @endif href="{{route('branches')}}"><i class="icon-sid la la-building"></i>
                                <span>{{__('admin.branches')}}</span>
                            </a>
                        </li>
                    @endcan
                        @can('show branches')
                            <li class="@if($page_title == __('admin.provider')) active @endif">
                                <a class="menu-item" @if($page_title == __('admin.provider')) onclick="event.preventDefault()"
                                   @endif href="{{route('provider')}}"><i class="icon-sid la la-building"></i>
                                    <span>{{__('admin.provider')}}</span>
                                </a>
                            </li>
                        @endcan
                    <li class="@if($page_title == __('admin.profile')) active @endif"><a
                            class="menu-item"
                            @if($page_title == __('admin.profile')) onclick="event.preventDefault()"
                            @endif href="{{ route('profile')}}"><i class="icon-sid la la-user"></i><span
                                data-i18n="nav.editors.editor_ckeditor">{{__('admin.profile')}}</span></a></li>
                    <li class="@if($page_title == __('admin.Edit_Profile')) active @endif">
                        <a class="menu-item"
                           @if($page_title == __('admin.Edit_Profile')) onclick="event.preventDefault()"
                           @endif href="{{ route('profile.edit_password', Auth::User()->id)}}"><i
                                class="icon-sid la la-key"></i><span
                                data-i18n="nav.editors.editor_ckeditor">{{__('admin.edit')}} {{__('admin.password')}}</span></a>
                    </li>
                    <li class="@if($page_title == __('admin.logout'))active @endif open">
                        <a class="menu-item"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                           href=""><i class="ft ft-power" style="margin-left: 10px;"></i> <span
                                data-i18n="nav.templates.vert.main">{{__('admin.Logout')}}</span></a>
                    </li>


                </ul>
            </li>
        </ul>
    </div>
</div>

<!-- END: Main Menu-->
