<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
        <a class="infinitecloud-a" href="https://www.infinitecloud.co/" target="_blank" style="color: #505050;font-weight: 600;">
            @if(auth()->user()->style == 'light')
                <img width="85" src="{{asset('infinitecloud-black.png')}}">
            @else
                <img width="85" src="{{asset('infinitecloud.png')}}">
            @endif
            Built with <i class="la la-heart red" {{--style="color: #ffc93b;"--}}></i> by
        </a>
    </p>
</footer>
<!-- END: Footer-->
