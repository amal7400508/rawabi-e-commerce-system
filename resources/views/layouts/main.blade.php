{{--@php(env('APP_NAME', __('admin.search')))--}}
@php(env('APP_NAME', 'admin.search'))
@php(!isset($is_morris) ? $is_morris = 0 : $is_morris = 1)
@php($page_title = !isset($page_title) ?app()->getLocale() == 'en' ? 'Smart Delivery' : 'التوصيل الذكي' : $page_title)
    <!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('layouts.head')

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern 2-columns fixed-navbar"
      data-open="click"
      data-menu="vertical-menu-modern"
      data-col="2-columns"
>

<!-- BEGIN: Main Nav-->
@include('layouts.nav')

<!-- BEGIN: Main Menu-->
@include('layouts.sidebar')

<!-- BEGIN: Content-->
<div class="app-content content">
    @yield('content')

    {{-- modal of loading beforeSend for show modal --}}
    @include('include.modal_loading_show')
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

@include('layouts.footer')

<!-- BEGIN: Vendor JS-->
<script src="{{asset('app-assets/vendors/js/vendors.min.js')}}"></script>
<!-- BEGIN Vendor JS-->

@yield('script')
<!-- BEGIN: Page Vendor JS-->
<script src="{{asset('app-assets/vendors/js/charts/chartist.min.js')}}"></script>
{{--<script src="{{asset('app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js')}}"></script>--}}
<script src="{{asset('app-assets/vendors/js/timeline/horizontal-timeline.js')}}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{asset('app-assets/js/core/app-menu.js')}}"></script>
<script src="{{asset('app-assets/js/core/app.js')}}"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/forms/switch.js')}}"></script>

<script src="{{asset('app-assets/vendors/js/extensions/sweetalert.min.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/extensions/sweet-alerts.js')}}"></script>
<!-- END: Page Vendor JS-->
<script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>

<!-- BEGIN: Page JS-->
{{--<script src="{{asset('app-assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script>--}}
<script src="{{asset('app-assets/js/scripts/customizer.min.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/footer.min.js')}}"></script>
<!-- END: Page JS-->

<!-- BEGIN: Page JS-->
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.min.js')}}"></script>
<!-- END: Page JS-->
<script src="{{asset('app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/ui/jquery-ui/navigations.min.js')}}"></script>

@include('include.ajax-CRUD')
@include('include.table_length_js')
@include('layouts.notify-js')

</body>
<!-- END: Body-->

</html>
