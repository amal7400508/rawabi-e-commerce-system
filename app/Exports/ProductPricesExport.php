<?php

namespace App\Exports;

use App\Http\Controllers\ProductController;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\Unit;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductPricesExport implements
    FromCollection
    , WithMapping
    , ShouldAutoSize
    , WithHeadings
{

    public $data;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            '#',
            Lang::get('admin.id'). ' ' . Lang::get('admin.product'),
            Lang::get('admin.name'),
            Lang::get('admin.price'),
            Lang::get('admin.currency'),
            Lang::get('admin.note') . ' ' . Lang::get('admin.ar'),
            Lang::get('admin.note') . ' ' . Lang::get('admin.en'),
            Lang::get('admin.available_for_hospitality'),
            Lang::get('admin.id'). ' ' . Lang::get('admin.unit'),
            Lang::get('admin.unit'),
            Lang::get('admin.created_at'),
            Lang::get('admin.updated_at'),
        ];
    }

    public function map($invoice): array
    {
        $order_availability = $invoice->order_availability == 1 ? Lang::get('admin.active') :
            $order_availability = Lang::get('admin.attitude');


        return [
            $invoice->id,
            $invoice->product->id ?? null,
            $invoice->product->name ?? null,
            $invoice->price,
            $invoice->currency,
            $invoice->note_ar,
            $invoice->note_en,
            $order_availability,
            $invoice->unit->id ?? null,
            $invoice->unit->name ?? null,
            $invoice->created_at,
            $invoice->updated_at,
        ];
    }
}
