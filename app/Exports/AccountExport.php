<?php

namespace App\Exports;

use App\Models\BalanceTransaction;
use App\Models\Request;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AccountExport implements
    FromCollection
    , WithMapping
    , ShouldAutoSize
    , WithHeadings
{

    public $data;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            '#',
            Lang::get('admin.type'),
            Lang::get('admin.account_name'),
            Lang::get('admin.operation_type'),
            Lang::get('admin.debit'),
            Lang::get('admin.creditor'),
            Lang::get('admin.Date'),
            Lang::get('admin.operation_number'),
            Lang::get('admin.invoice_number'),
        ];
    }

    public function map($invoice): array
    {
//        $sum_debit = 0;
//        $sum_creditor = 0;
        $debit_amount = '';
        $creditor_amount = '';

        $currency = currency($invoice->currency) . ' ';
        if ($invoice->type == 'subtract') {
//            $sum_debit += $invoice->amount;
            $debit_amount = $currency . $invoice->amount;
        }
        if ($invoice->type == 'add') {
//            $sum_creditor += $invoice->amount;
            $creditor_amount = $currency . $invoice->amount;
        }

        $balance_transaction = BalanceTransaction::where('balance_transactionable_id',$invoice->balance_transactionable_id)
            ->where('balance_transactionable_type','App\Models\Request')->pluck('balance_transactionable_id')->toArray();

        $request = Request::find($balance_transaction)->first()->request_number ?? null;

        return [
            $invoice->id,
            $invoice->wallet_type,
            $invoice->wallet->walletable->name ?? null,
            $invoice->operationType->name ?? null,
            $debit_amount,
            $creditor_amount,
            $invoice->created_at,
            $invoice->balance_transactionable_id,
            $request,
        ];
    }
}
