<?php
namespace App\Exports;

use App\Http\Controllers\CarrierController;
use App\Models\Admin;
use App\Models\Carrier;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CarrierExport implements
    FromCollection
    ,WithMapping
    ,ShouldAutoSize
    ,WithHeadings
{

    public $data;
    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            '#',
            Lang::get('admin.name'),
            Lang::get('admin.email'),
            Lang::get('admin.phone'),
            Lang::get('admin.gender'),
            Lang::get('admin.status'),
            Lang::get('admin.Evaluation'),
            Lang::get('admin.user_entry'),
            Lang::get('admin.created_at'),
        ];
    }

    public function map($invoice): array
    {
        $status = null;
        if ($invoice->status == 1){
            $status = Lang::get('admin.active');

        } else{
            $status = Lang::get('admin.attitude');
        }

        $gender = $invoice->gender == 'woman' ? Lang::get('admin.woman')
            : Lang::get('admin.man');

        $id = Carrier::find($invoice->id);
        if (isset($id->audits()->with('user')->first()->admins_id)) {
            $admins_id = $id->audits()->with('user')->first()->admins_id;
            $data_entry = Admin::find($admins_id)->first()->name;
        } else {
            $data_entry = "";
        }

        $average_rate = new CarrierController();
        return [
            $invoice->id,
            $invoice->name,
            $invoice->email,
            $invoice->phone,
            $gender,
            $status,
            $average_rate->averageRate($invoice->id),
            $data_entry,
            $invoice->created_at,
        ];
    }
}
