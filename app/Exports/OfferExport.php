<?php
namespace App\Exports;

use App\Admin;
use App\Offer;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OfferExport implements
    FromCollection
    ,WithMapping
    ,ShouldAutoSize
    ,WithHeadings
{

    public $data;
    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            '#',
            Lang::get('admin.name'),
            Lang::get('admin.offer_type'),
            Lang::get('admin.old_price'),
            Lang::get('admin.new_price'),
            Lang::get('admin.status'),
            Lang::get('admin.user_entry'),
            Lang::get('admin.created_at'),

        ];
    }

    public function map($invoice): array
    {
        $status = null;
        if ($invoice->status == 1){
            $status = Lang::get('admin.active');

        } else{
            $status = Lang::get('admin.attitude');
        }

        $id = Offer::find($invoice->id);
        if (isset($id->audits()->with('user')->first()->admins_id)) {
            $admins_id = $id->audits()->with('user')->first()->admins_id;
            $data_entry = Admin::find($admins_id)->first()->name;
        } else {
            $data_entry = "";
        }

        return [
            $invoice->id,
            $invoice->offer_name,
            $invoice->name_type,
            $invoice->old_price,
            $invoice->price,
            $status,
            $invoice->created_at,
            $data_entry,
        ];
    }
}
