<?php

namespace App\Exports;

use App\Admin;
use App\Disposit;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DepositExport implements
    FromCollection
    , WithMapping
    , ShouldAutoSize
    , WithHeadings
{

    public $data;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            Lang::get('admin.deposit_id'),
            Lang::get('admin.account_num'),
            Lang::get('admin.name'),
            Lang::get('admin.phone'),
            Lang::get('admin.amount'),
            Lang::get('admin.branch_name'),
            Lang::get('admin.status'),
            Lang::get('admin.gender'),
            Lang::get('admin.user_entry'),
            Lang::get('admin.deposits_date'),
        ];
    }

    public function map($invoice): array
    {
        $status = null;
        $gender = $invoice->gender == 'woman' ? Lang::get('admin.woman')
            : Lang::get('admin.man');
        if ($invoice->status == 1) {
            $status = Lang::get('admin.active');
        } else if ($invoice->status == 0) {
            $status = Lang::get('admin.attitude');
        }

        $id = Disposit::find($invoice->id);
        if (isset($id->audits()->with('user')->first()->admins_id)) {
            $admins_id = $id->audits()->with('user')->first()->admins_id;
            $data_entry = Admin::find($admins_id)->first()->name;
        } else {
            $data_entry = "";
        }
        return [
            $invoice->id,
            $invoice->user_wallet_id,
            $invoice->name,
            $invoice->phone,
            $invoice->amount,
            $invoice->branch->name,
            $status,
            $gender,
            $data_entry,
            $invoice->created_at,
        ];
    }
}
