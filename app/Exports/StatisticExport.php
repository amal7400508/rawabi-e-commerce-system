<?php

namespace App\Exports;

use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class StatisticExport implements
    FromCollection
    , WithMapping
    , ShouldAutoSize
    , WithHeadings
{

    public $data;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        $th = [];
        if (is_null(request()->type) or in_array('user', request()->option ?? [])) {
            $th[] = Lang::get('admin.user_name');
            $th[] = Lang::get('admin.phone');
        }
        $th[] = Lang::get('admin.click_type');

        if (in_array('click_item', request()->option ?? []) or is_null(request()->option)) {
            $th[] = Lang::get('admin.name');
        }
        $th[] = Lang::get('admin.number');
        if (is_null(request()->type)) {
            $th[] = Lang::get('admin.created_at');
        }

        return $th;
    }

    public function map($invoice): array
    {
        $array = [];
        if (is_null(request()->type) or in_array('user', request()->option ?? [])) {
            $array[] = $invoice->user->full_name;
            $array[] = substr($invoice->user->phone, 0, 4) . ' ' . substr($invoice->user->phone, 4);
        }

        $array[] = $invoice->click_type;

        if (in_array('click_item', request()->option ?? []) or is_null(request()->option)) {
            if ($invoice->statisticable_type == 'App\Models\Product'
                or $invoice->statisticable_type == 'App\Models\Offer'
                or $invoice->statisticable_type == 'App\Models\Category'
                or $invoice->statisticable_type == 'App\Models\Branch'
                or $invoice->statisticable_type == 'App\Models\Provider') {
                $array[] = $invoice->statisticable->name;
            }
        } elseif ($invoice->statisticable_type == 'App\Models\PaidAdvertisement') {
            $array[] = $invoice->statisticable->title;
        } elseif ($invoice->statisticable_type == 'App\Models\Advertisement') {
            $array[] = $invoice->statisticable->desc;
        } elseif ($invoice->click_type == 'search') {
            $array[] = $invoice->text;
        }

        $array[] = $invoice->number;

        if (is_null(request()->type)) {
            $array[] = $invoice->created_at;
        }

        return $array;
    }
}
