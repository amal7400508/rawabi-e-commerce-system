<?php

namespace App\Exports;

use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserMostRequest implements
    FromCollection
    , WithMapping
    , ShouldAutoSize
    , WithHeadings
{

    public $data;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            '#',
            Lang::get('admin.name'),
            Lang::get('admin.status'),
            Lang::get('admin.phone'),
            Lang::get('admin.requests_number'),
        ];
    }

    public function map($invoice): array
    {
        $status = null;
        if ($invoice->status == 1) {
            $status = Lang::get('admin.active');
        } else if ($invoice->status == 2) {
            $status = Lang::get('admin.deleted');
        } else if ($invoice->status == 3) {
            $status = Lang::get('admin.black_list');
        } else if ($invoice->status == 0) {
            $status = Lang::get('admin.attitude');
        }

        return [
            $invoice->id,
            $invoice->full_name,
            $status,
//            (string)$invoice->phone,
            substr($invoice->phone, 0, 4) . ' ' . substr($invoice->phone, 4),
            $invoice->requests_number,
        ];
    }
}
