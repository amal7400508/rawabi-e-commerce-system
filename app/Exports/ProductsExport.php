<?php

namespace App\Exports;

use App\Http\Controllers\ProductController;
use App\Models\Product;
use App\Models\ProductPrice;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductsExport implements
    FromCollection
    , WithMapping
    , ShouldAutoSize
    , WithHeadings
{

    public $data;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            '#',
            Lang::get('admin.name'),
            Lang::get('admin.unit') . ': ' . Lang::get('admin.price'),
            Lang::get('admin.category'),
            Lang::get('admin.type'),
            Lang::get('admin.status'),
            Lang::get('admin.requests_number'),
            Lang::get('admin.created_at'),
            Lang::get('admin.updated_at'),
        ];
    }

    public function map($invoice): array
    {
        $status = $invoice->status == 1 ? Lang::get('admin.active') :
            $status = Lang::get('admin.attitude');

        $type = $invoice->type == 'public' ? Lang::get('admin.public') :
            $type = Lang::get('admin.private');

        $is_new = $invoice->is_new == 1 ? Lang::get('admin.new') : '';
        $name = $invoice->name . '                   ' . $is_new;

        $product_prices = '';
        $prices = ProductPrice::where('product_id', $invoice->id)->get();
        foreach ($prices as $price) {
            $product_prices .= $price->unitName . ' : ' . $price->price . "  \n";
        }

        $most_product = new ProductController();

        return [
            $invoice->number,
            $name,
            $product_prices,
            $invoice->category_name,
            $type,
            $status,
            $most_product->mostProduct($invoice->id),
            $invoice->created_at,
            $invoice->updated_at,
        ];
    }
}
