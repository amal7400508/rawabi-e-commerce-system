<?php

namespace App\Exports;

use App\Models\CartDetail;
use App\Models\Coupon;
use App\Models\Offer;
use App\Models\Prescription;
use App\Models\ProductPrice;
use App\Models\SpecialProduct;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RequestExport implements
    FromCollection
    , WithMapping
    , ShouldAutoSize
    , WithHeadings
{

    public $data;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            Lang::get('admin.request_no'),
            Lang::get('admin.user_name'),
            Lang::get('admin.phone'),
            Lang::get('admin.gender'),
            Lang::get('admin.quantity'),
            Lang::get('admin.request_status'),
            Lang::get('admin.payment_type'),
            Lang::get('admin.branch_name'),
            Lang::get('admin.delivery_driver'),
            Lang::get('admin.total_price'),
            Lang::get('admin.date_1') . ' ' . Lang::get('admin.request'),
        ];

    }

    public function map($invoice): array
    {
        $status = $invoice->status;
        $gender = $invoice->gender == 'woman' ? Lang::get('admin.woman')
            : Lang::get('admin.man');

        if ($status == 'requested') {
            $status = Lang::get('admin.requested');
        } else if ($status == 'canceled') {
            $status = Lang::get('admin.canceled');
        } else if ($status == 'repair') {
            $status = Lang::get('admin.repair');
        } else if ($status == 'deliver') {
            $status = Lang::get('admin.deliver');
        } else if ($status == 'completedRequest') {
            $status = Lang::get('admin.completedRequest');
        } else if ($status == 'done') {
            $status = Lang::get('admin.deliverySuccessful');
        } else if ($status == 'delivered' || $status == 'on_branch') {
            $status = Lang::get('admin.delivered');
        } else if ($status == 'received') {
            $status = Lang::get('admin.received');
        } else if ($status == 'reviewed') {
            $status = Lang::get('admin.reviewed');
        }

        $price_sum = 0;
        $price__ = 0;

        $cart_detail = CartDetail::where('cart_id', $invoice->cart_id)->get();

        foreach ($cart_detail as $cart_details) {
            $product_price = ProductPrice::find($cart_details->product_price_id);
            if ($cart_details->item_type == 'offer') {
                $offer = Offer::find($cart_details->offer_id);
                $price__ = $offer->price;
            } elseif ($cart_details->item_type == 'product') {
                $price__ = $product_price->price;
            } elseif ($cart_details->item_type == 'prescription') {
                $prescription = Prescription::find($cart_details->prescription_reply_id);
                $price__ = $prescription->total_price;
            } elseif ($cart_details->item_type == 'special') {
                $SpecialProduct = SpecialProduct::find($cart_details->special_product_id);
                $price__ = $SpecialProduct->price;
            }
            $quantity_total = $cart_details->quantity;
            $price_sum = $price_sum + ($price__ * $quantity_total);
        }
        $coupon = null;
        $remaining_amount = $price_sum;
        if (!is_null($invoice->coupon_id)) {
            $coupon = Coupon::find($invoice->coupon_id)->price ?? 0;
            $remaining_amount = $coupon > $price_sum ? '0' : $price_sum - $coupon;
        }

        $payment_type = '';
        if ($invoice->payment_type == 'cash') {
            $payment_type = Lang::get('admin.cash');
        } elseif ($invoice->payment_type == 'fromWallet') {
            $payment_type = Lang::get('admin.from_wallet');
        }

        $phone = $invoice->cart->user->phone;
        return [
            $invoice->id,
            $invoice->cart->user->name,
            substr($phone, 0, 4) . ' ' . substr($phone, 4),
            $gender,
            $invoice->quantity,
            $status,
            $payment_type,
            $invoice->branch->name ?? '',
            $invoice->carrier->name ?? '',
            $remaining_amount,
            $invoice->created_at,
        ];
    }
}
