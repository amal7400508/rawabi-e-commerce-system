<?php

namespace App\Exports;

use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserExport implements
    FromCollection
    , WithMapping
    , ShouldAutoSize
    , WithHeadings
{

    public $data;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            '#',
            Lang::get('admin.name'),
            Lang::get('admin.email'),
            Lang::get('admin.phone'),
            Lang::get('admin.status'),
            Lang::get('admin.gender'),
            Lang::get('admin.created_at'),
        ];
    }

    public function map($invoice): array
    {
        $status = null;
        $gender = $invoice->gender == 'woman' ? Lang::get('admin.woman')
            : Lang::get('admin.man');
        if ($invoice->status == 1) {
            $status = Lang::get('admin.active');
        } else if ($invoice->status == 2) {
            $status = Lang::get('admin.deleted');
        } else if ($invoice->status == 3) {
            $status = Lang::get('admin.black_list');
        } else if ($invoice->status == 0) {
            $status = Lang::get('admin.attitude');
        }
        return [
            $invoice->id,
            $invoice->name,
            $invoice->email,
            substr($invoice->phone, 0, 4) . ' ' . substr($invoice->phone, 4),
            $status,
            $gender,
            $invoice->created_at,
        ];
    }
}
