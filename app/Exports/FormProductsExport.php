<?php
namespace App\Exports;

use App\Models\Admin;
use App\Http\Controllers\ProductController;
use App\Models\Product;
use App\Models\ProductPrice;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class FormProductsExport implements WithHeadings
{


    public function headings(): array
    {
        return [
           'number',
           'category_id',
           'name_ar',
           'details_ar',
           'name_en',
           'details_en',
        ];
    }

    public function map($invoice): array
    {
        return [
            '',
            '',
            '',
            '',
            '',
            '',
        ];
    }
}
