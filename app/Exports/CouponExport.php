<?php
namespace App\Exports;

use App\Admin;
use App\Coupon;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CouponExport implements
    FromCollection
    ,WithMapping
    ,ShouldAutoSize
    ,WithHeadings
{

    public $data;
    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            Lang::get('admin.id'),
            Lang::get('admin.amount'),
            Lang::get('admin.reasons_coupon'),
            Lang::get('admin.start_date'),
            Lang::get('admin.end_date'),
            Lang::get('admin.status'),
            Lang::get('admin.user_entry'),
            Lang::get('admin.created_at'),
        ];
    }

    public function map($invoice): array
    {
        $status = null;
        if ($invoice->status == 1){
            $status = Lang::get('admin.active');

        } else{
            $status = Lang::get('admin.attitude');
        }

        $id = Coupon::find($invoice->id);
        if (isset($id->audits()->with('user')->first()->admins_id)) {
            $admins_id = $id->audits()->with('user')->first()->admins_id;
            $data_entry = Admin::find($admins_id)->first()->name;
        } else {
            $data_entry = "";
        }
        return [
            $invoice->number,
            $invoice->price,
            $invoice->name,
            $invoice->start_date,
            $invoice->end_date,
            $status,
            $data_entry,
            $invoice->created_at,
        ];
    }
}
