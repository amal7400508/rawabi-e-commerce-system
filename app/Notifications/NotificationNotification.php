<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class NotificationNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * @param $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'id' => $notifiable->id,
            'ar_notification_title' => $notifiable->notification->translate('ar')->notification_title,
            'en_notification_title' => $notifiable->notification->translate('en')->notification_title,
            'ar_notification_message' => $notifiable->notification->translate('ar')->notification_message,
            'en_notification_message' => $notifiable->notification->translate('en')->notification_message,
            'image' => "/storage/notification_movement/" .$notifiable->image,
            'provider_id' => Auth::user()->id,
            'provider_name' => Auth::user()->name,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
