<?php
namespace App\Utilities\Helpers;

use App\Models\UserFireBaseToken;

class Fcm
{
    /**
     * Example
     * is_array([])         // true
     * is_array(null || '') // false
     *
     * @param string     $fcm_title
     * @param string     $fcm_body
     * @param null|array $user_ids
     * @param null       $image
     *
     * @return void
     */
    public static function send_MKA_notification(

        $fcm_title,
        $fcm_body,
        $user_ids = null,
        $image = null
    ) {
        $registration_ids = $user_ids == null
            ? null
            // ? If $user_ids has no firebase token the return value is: []
            : UserFireBaseToken::where('user_id', $user_ids)
            ->groupBy('token')->pluck('token')->toArray();

        $API_ACCESS_KEY = env('FCM_SERVER_KEY');

        // * prep the bundle
        $msg = is_null($image) ? array
        (
            'body' => $fcm_body ?? 'body',
            'title' => $fcm_title ?? 'title admin test!',
        )
        : array
        (
            'body' => $fcm_body ?? 'body',
            'title' => $fcm_title ?? 'title admin test!',
            'image' => $image ?? 'https://lh3.googleusercontent.com/ogw/ADGmqu-FXDyRYXDJObEy-LkLyZ2BW3C4ZokAvPRxog3Y',
        );

        // * prep the notification
        if (!is_array($registration_ids)) {
            $fields = array
            (
                'to' => '/topics/AlRwabiTelecome',
                'notification' => $msg
            );
        } else {
            $ids = $registration_ids;
            $fields = array
            (
                'registration_ids' => $ids,
                'notification' => $msg
            );
        }

        $headers = array
        (
            'Authorization: key='. $API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        // * Send Response To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
    }
}
