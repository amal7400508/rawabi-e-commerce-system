<?php

use App\Models\Product;
use App\Models\SystemNotification;
use App\Notifications\Admin\ProviderNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;

/**
 * @param $image
 * @param $path
 * @return string
 */
function uploadImage($image, $path)
{
    $imageName = $path . '_' . time() . '.' . $image->getClientOriginalExtension();
    $image->move(public_path('storage' . '/' . $path), $imageName);
    return $imageName;
}


/**
 * @param $file_name
 */
function deleteImage($file_name)
{
    $file_name = public_path() . $file_name;
    File::delete($file_name);
}

function currency($currency)
{
    if ($currency == "دولار أمريكي") {
        $currency = "$";
    } else if ($currency == "ريال يمني") {
        $currency = "YER";
    } else if ($currency == "ريال سعودي") {
        $currency = "SAR";
    }

    return $currency;
}

/**
 * @param int $rate_type_id
 * @param int $rate_id
 * @return int
 */
function getRateTypeAndRateId($rate_type_id, $rate_id)
{
    if (isset(
        \App\Models\RateDetail::where('rate_type_id', $rate_type_id)
            ->where('rate_id', $rate_id)
            ->get()
            ->first()
            ->rate
    )) {
        $rate_details = \App\Models\RateDetail::where('rate_type_id', $rate_type_id)
            ->where('rate_id', '=', $rate_id)->get()->first()->rate;
        return $rate_details;
    }
    return 0;
}

function appendRate($data)
{
    if ($data == 5) {
        $rate = "<div class='fonticon-wrap'>" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "</div>";
    } else if ($data == 4) {
        $rate = "<div class='fonticon-wrap'>" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "</div>";
    } else if ($data == 3) {
        $rate = "<div class='fonticon-wrap'>" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "</div>";
    } else if ($data == 2) {
        $rate = "<div class='fonticon-wrap'>" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "</div>";
    } else if ($data == 1) {
        $rate = "<div class='fonticon-wrap'>" .
            "<img src='" . asset('app-assets/images/raty/star-on-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "</div>";
    } else {
        $rate = "<div class='fonticon-wrap'>" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "<img src='" . asset('app-assets/images/raty/star-off-1.png') . "' >" .
            "</div>";
    }

    return $rate;
}

function responseNotify($id, $class_path, $admin_class_path = ProviderNotification::class)
{
    SystemNotification::where('notifiable_type', $class_path)
        ->where('type', '!=', $admin_class_path)
        ->where('notifiable_id', $id)
        ->update(['response_at' => Carbon::now()->toDateTime()]);
}

/**
 * @param $product_id int product id
 * @param $start_date
 * @param $end_date
 * @return int sum requests number
 */
function mostProduct(
    $product_id,
    $start_date = null,
    $end_date = null
)
{
    $obj = new \App\Http\Controllers\ProductController();

    return $obj->mostProduct(
        $product_id,
        $start_date,
        $end_date
    );

}

if (!function_exists('checkDays')) {

    function checkDays($day)
    {
        switch ($day) {
            case "SATURDAY":
                return __('admin.saturday');
                break;
            case "SUNDAY":
                return __('admin.sunday');
                break;
            case "MONDAY":
                return __('admin.monday');
                break;
            case "TUESDAY":
                return __('admin.tuesday');
                break;
            case "WEDNESDAY":
                return __('admin.wednesday');
                break;
            case "THURSDAY":
                return __('admin.thursday');
                break;
            case "FRIDAY":
                return __('admin.friday');
                break;
            default:
                return __('admin.error_date');
        }
    }
}
