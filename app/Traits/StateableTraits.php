<?php

namespace App\Traits;

use App\State;
use App\Stateable;

trait StateableTraits
{
    /**
     * Get the Stateable.
     */
    public function state()
    {
        return $this->morphToMany(State::class, 'stateable');
//        return $this->morphOne(Stateable::class, 'stateable');
    }
}
