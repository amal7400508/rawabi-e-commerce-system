<?php

namespace App\Traits;

use App\Models\Account;
use App\Models\BalanceTransaction;
use App\Models\Carrier;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Coupon;
use App\Models\Exports\RequestExport;
use App\Models\Hospitality;
use App\Models\Offer;
use App\Models\PointHistory;
use App\Models\PrescriptionReply;
use App\Models\ProductPrice;
use App\Models\Request;
use App\Models\SpecialProduct;
use App\Models\SystemNotification;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request as RequestPost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

trait RequestTrait
{
    protected function checkCurrency($cart, $payment_type)
    {
        $currencies = array();

        // get product currency
        $product_price_id = CartDetail::where('cart_id', $cart->id)->pluck('product_price_id')->toArray();
        $currencies = array_merge($currencies, ProductPrice::whereIn('id', $product_price_id)->pluck('currency')->toArray());

        // get offer currency
        $offer_id = CartDetail::where('cart_id', $cart->id)->pluck('offer_id')->toArray();
        $currencies = array_merge($currencies, Offer::whereIn('id', $offer_id)->pluck('currency')->toArray());

        // get special product currency
        $special_product_id = CartDetail::where('cart_id', $cart->id)->pluck('special_product_id')->toArray();
        $currencies = array_merge($currencies, SpecialProduct::whereIn('id', $special_product_id)->pluck('currency')->toArray());

        // get prescription currency
        $prescription_reply_ids = CartDetail::where('cart_id', $cart->id)->pluck('prescription_reply_id')->toArray();
        $currencies = array_merge($currencies, PrescriptionReply::whereIn('id', $prescription_reply_ids)->pluck('currency')->toArray());
        $currencies_unique = array_unique($currencies);
        if (count($currencies_unique) != 1) {
            return [
                'data' => 'use only one currency!',
                'status' => 401
            ];
        }

        if ($payment_type == 'fromWallet' and $currencies_unique[0] != 'ريال يمني') {
            return [
                'data' => 'Your balance is in the local currency. You cannot order in another currency!',
                'status' => 401
            ];
        }

        return [
            'data' => $currencies_unique[0],
            'status' => 200
        ];
    }

    /**
     *
     * @param $request
     * @return mixed
     * @throws \Exception
     */
    public function indexDataTable($request)
    {
        return datatables()->of($request)
            ->editColumn('branch', function (Request $request) {
                return $request->branch ? $request->branch->name : '';
            })->editColumn('carrier_id', function (Request $request) {
                return $request->carrier ? $request->carrier->name : '';
            })->editColumn('coupon_id', function (Request $request) {
                return $request->coupon ? $request->coupon->number : '';
            })->editColumn('cache_payment', function (Request $request) {
                return $this->cachePayment($request);
            })->editColumn('name', function (Request $request) {
                return $request->cart->user->name;
            })->editColumn('phone', function (Request $request) {
                return $request->cart->user->phone;
            })->editColumn('request', function (Request $request) {
                $request_count = Request::join('carts', 'requests.cart_id', '=', 'carts.id')
                    ->where('carts.user_id', $request->cart->user_id)
                    ->where(function ($q) {
                        $q->where('requests.status', 'on_branch')
                            ->orWhere('requests.status', 'delivered');
                    })
                    ->count();
                return $request_count;
            })->editColumn('created_at', function (Request $request) {
                return '<small><i class="ft-clock"></i> ' . $request->created_at->diffForHumans() . '</small>';
            })->setRowAttr([
                'style' => function (Request $request) {
                    return $this->backgroundColor($request);
                }
            ])
            ->addColumn('action', function ($data) {
                $span = '<a class="showB" id="' . $data->id . '"><span style="font-size:12px;color: #0092e2" title="' . Lang::get('admin.click_here_to_view_details') . '">' . Lang::get('admin.detail') . '...</span></a>';
                return $span;
            })
            ->rawColumns(['action', 'created_at'])
            ->make(true);
    }

    /**
     *  If the request type is hospitality,
     *  the background color of the record changes
     *
     * @param $request ,
     * @param $background , color of row the status
     * @return string , background color record
     */
    public function backgroundColor($request)
    {
        $cart_id = $request->cart_id;
        $cart = CartDetail::where('cart_id', $cart_id)->first();
        if ($cart->item_type == 'hospitality') {
            return 'background-color: #f3eae6;';
        }
        if ($request->status == 'requested')
            return 'background-color: #feeff0;';
        else if ($request->status == 'canceled')
            return 'background-color: #fafafa;';
        else if ($request->status == 'reviewed')
            return 'background-color: #f5fcfe;';
        else if ($request->status == 'repair')
            return 'background-color: #fdf6f3;';
        else if ($request->status == 'deliver')
            return 'background-color: #f1f3ff;';
        else if ($request->status == 'delivered')
            return 'background-color: #f4ffec;';
        else if ($request->status == 'done')
            return 'background-color: #f4ffec;';
        else if ($request->status == 'on_the_way')
            return 'background-color: #f4ffec;';
        else if ($request->status == 'on_branch')
            return 'background-color: #f4ffec;';
        else if ($request->status == 'received')
            return 'background-color: #f4ffec;';
        else if ($request->status == 'completedRequest')
            return 'background-color: #f1f3ff;';
        else
            return 'background-color: #fff;';
    }

    /**
     **
     * @return void
     */

    /**
     * Cancel the request
     *
     * @param $request_id
     */
    public function cancel($request_id)
    {
        $balance_transactions = BalanceTransaction::where('balance_transactionable_type', Request::class)
            ->where('balance_transactionable_id', $request_id)
            ->get();

        foreach ($balance_transactions as $balance_transaction) {
            $type = $balance_transaction->type == 'add' ? 'subtract' : 'add';
            BalanceTransaction::create([
                'amount' => $balance_transaction->amount,
                'type' => $type,
                'wallet_id' => $balance_transaction->wallet_id,
                'operation_type_id' => 8,
                'balance_transactionable_id' => $balance_transaction->balance_transactionable_id,
                'balance_transactionable_type' => $balance_transaction->balance_transactionable_type,
                'transactionable_id' => $balance_transaction->transactionable_id,
                'transactionable_type' => $balance_transaction->transactionable_id
            ]);

            $current_balance = $balance_transaction->type == 'add'
                ? $balance_transaction->wallet->current_balance - $balance_transaction->amount
                : $balance_transaction->wallet->current_balance + $balance_transaction->amount;

            Wallet::where('id', $balance_transaction->wallet_id)
                ->update(['current_balance' => $current_balance]);
        }

    }

    public function returnRequest($request_id, $payment_type)
    {
        if ($payment_type == 'fromWallet') {
            $balance_transaction = BalanceTransaction::where('balance_transactionable_type', Request::class)
                ->where('balance_transactionable_id', $request_id)
                ->whereHas('wallet', function ($q) {
                    $q->where('walletable_type', User::class);
                })
                ->first();

//            $main_wallet = Account::find(3)->wallet;
            $main_wallet = Account::find(3)->wallet->where('currency', 'ريال يمني')->first();
            $main_wallet_id = $main_wallet->id;
            $main_wallet_current_balance = $main_wallet->current_balance;

            // الصندوق الرئيسي
            BalanceTransaction::create([
                'amount' => $balance_transaction->amount,
                'type' => 'subtract',
                'wallet_id' => $main_wallet_id,
                'operation_type_id' => 9,
                'balance_transactionable_id' => $balance_transaction->balance_transactionable_id,
                'balance_transactionable_type' => $balance_transaction->balance_transactionable_type,
                'transactionable_id' => $balance_transaction->transactionable_id,
                'transactionable_type' => $balance_transaction->transactionable_id
            ]);

            Wallet::where('id', $main_wallet_id)
                ->update(['current_balance' => $main_wallet_current_balance - $balance_transaction->amount]);

//            $type = $balance_transaction->type == 'add' ? 'subtract' : 'add';

            BalanceTransaction::create([
                'amount' => $balance_transaction->amount,
                'type' => 'add',
                'wallet_id' => $balance_transaction->wallet_id,
                'operation_type_id' => 9,
                'balance_transactionable_id' => $balance_transaction->balance_transactionable_id,
                'balance_transactionable_type' => $balance_transaction->balance_transactionable_type,
                'transactionable_id' => $balance_transaction->transactionable_id,
                'transactionable_type' => $balance_transaction->transactionable_id
            ]);

//            $current_balance = $balance_transaction->type == 'add'
//                ? $balance_transaction->wallet->current_balance - $balance_transaction->amount
//                : $balance_transaction->wallet->current_balance + $balance_transaction->amount;

            $current_balance =  $balance_transaction->wallet->current_balance + $balance_transaction->amount;

            Wallet::where('id', $balance_transaction->wallet_id)
                ->update(['current_balance' => $current_balance]);
        } else {
            /*$balance_transaction = BalanceTransaction::where('balance_transactionable_type', Request::class)
                ->where('balance_transactionable_id', $request_id)
                ->whereHas('wallet', function ($q) {
                    $q->where('walletable_type', Account::class)
                        ->where('walletable_id', 3);
                })
                ->first();

            $main_wallet = Account::find(4)->wallet;
            $main_wallet_id = $main_wallet->id;
            $main_wallet_current_balance = $main_wallet->current_balance;

            BalanceTransaction::create([
                'amount' => $balance_transaction->amount,
                'type' => 'add',
                'wallet_id' => $main_wallet_id,
                'operation_type_id' => 9,
                'balance_transactionable_id' => $balance_transaction->balance_transactionable_id,
                'balance_transactionable_type' => $balance_transaction->balance_transactionable_type,
                'transactionable_id' => $balance_transaction->transactionable_id,
                'transactionable_type' => $balance_transaction->transactionable_id
            ]);

            Wallet::where('id', $main_wallet_id)
                ->update(['current_balance' => $main_wallet_current_balance + $balance_transaction->amount]);

            $type = $balance_transaction->type == 'subtract' ? 'subtract' : 'add';

            BalanceTransaction::create([
                'amount' => $balance_transaction->amount,
                'type' => $type,
                'wallet_id' => $balance_transaction->wallet_id,
                'operation_type_id' => 9,
                'balance_transactionable_id' => $balance_transaction->balance_transactionable_id,
                'balance_transactionable_type' => $balance_transaction->balance_transactionable_type,
                'transactionable_id' => $balance_transaction->transactionable_id,
                'transactionable_type' => $balance_transaction->transactionable_id
            ]);

            $current_balance = $balance_transaction->type == 'subtract'
                ? $balance_transaction->wallet->current_balance - $balance_transaction->amount
                : $balance_transaction->wallet->current_balance + $balance_transaction->amount;

            Wallet::where('id', $balance_transaction->wallet_id)
                ->update(['current_balance' => $current_balance]);*/
        }

    }

    /**
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function falter($id)
    {
        $carrier = Carrier::where('branch_id', $id)
            ->where('status', '<>', 0)
            ->get()
            ->pluck("name", "id");
        return response()->json($carrier);
    }

    /**
     * Filter the driver according to the status of the carrier
     * @param $id , branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function falterStatus($id)
    {
        $status_carrier = $_GET["status_carrier"];
        if ($status_carrier == null) {
            $carrier = Carrier::where('status', '<>', 0)->get()->pluck("name", "id");
        } else {
            $carrier = Carrier::where('status', $status_carrier)->pluck("name", "id");
        }
        return response()->json($carrier);
    }


    /**
     * @param $request
     * @return float|int
     */
    public function cachePayment($request)
    {
        if ($request->payment_type == 'fromWallet') {
            return 0.00;
        }

        $coupon = null;
        if (isset($request->coupon_id)) {
            $coupon = Coupon::find($request->coupon_id);
        }
        $cart = Cart::FindOrFail($request->cart_id);
        $remaining_amount = $cart['price_sum'];

        if (!is_null($coupon)) {
            // المبلغ المتوجب خصمة او دفعة في حالة وجود تخفيض كوبون
            $remaining_amount = ($coupon['value_type'] == 'percentage')
                ? $cart['price_sum'] - (($coupon['price'] / 100) * $cart['price_sum'])
                : max(($cart['price_sum'] - $coupon['price']), 0);
        }

        if (!is_null($request->volume_discount_id)) {
            $volume_discount = \App\VolumeDiscount::find($request->volume_discount_id) ?? null;
            $discount_value = $volume_discount['value'] ?? null;
            if (!is_null($discount_value) && !is_null($volume_discount['value_type'] ?? null)) {
                $remaining_amount = ($volume_discount['value_type'] == 'percentage')
                    ? $remaining_amount - (($discount_value / 100) * $remaining_amount)
                    : max(($remaining_amount - $discount_value), 0);
            }
        }

        return $remaining_amount;

        $cart_id = $request->cart_id;
        $cart_detail = CartDetail::where('cart_id', $cart_id)->get();
        $price_sum = 0;
        foreach ($cart_detail as $cart_details) {
            $product_price = ProductPrice::find($cart_details->product_price_id);

            if ($cart_details->item_type == 'offer') {
                $offer = Offer::find($cart_details->offer_id);
                $price__ = $offer->price;
            } elseif ($cart_details->item_type == 'product') {
                $price__ = $product_price->price;
            } elseif ($cart_details->item_type == 'hospitality') {
                $Hospitality = Hospitality::find($cart_details->hospitality_id);
                $price__ = $Hospitality->total_price;
            } elseif ($cart_details->item_type == 'special') {
                $SpecialProduct = SpecialProduct::find($cart_details->special_product_id);
                $price__ = $SpecialProduct->price;
            }
            $price_sum = $price_sum + ($price__ * $cart_details->quantity);
        }
        return $price_sum;
    }

    /**
     * @param $cart_details string the details of the cart
     *
     * @return Request
     */
    public function defaultRedirection($cart_details)
    {
        return Request::joinSub($cart_details, 'cart_details', function ($join) {
            $join->on('requests.cart_id', '=', 'cart_details.id_cart');
        })
            ->where('receiving_ty pe', '=', 'fromBranch')
            ->select('requests.*', 'quantity', 'requests.cache_payment')
            ->get();
    }

    public function allRequestTracking($cart_details, $table_length)
    {
        return Request::joinSub($cart_details, 'cart_details', function ($join) {
            $join->on('requests.cart_id', '=', 'cart_details.id_cart');
        })
            ->select('requests.*', 'quantity')
            ->orderBy('requests.updated_at', 'desc')
            ->paginate($table_length);
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public function cartDeatils()
    {
        return DB::table('carts')
            ->join('cart_details', 'carts.id', '=', 'cart_details.cart_id')
            ->select('cart_details.cart_id as id_cart', DB::raw('sum(cart_details.quantity) as quantity'))
            ->groupBy('id_cart');
    }

    /**
     * read data in notification system table
     * @param $id ,id for request
     */
    public function readAtNotification($id)
    {
        SystemNotification::where('notifiable_type', 'App\Request')
            ->where('notifiable_id', $id)
            ->update(['response_at' => Carbon::now()->toDateTime()]);

    }

    /**
     * Fetch reports according to conditions
     * @param $request
     * @return mixed
     * @throws \Exception
     */
    public function getReport($request)
    {
        if ($request->confirm == 1) {
            if (!is_null($request->product_id) || !is_null($request->category)) {
                $cart_details = $this->cartDetailsWithProducts();

            } else {
                $cart_details = $this->cartDeatils();
            }
            if (!is_null($request->category)) {
                $cart_details = $cart_details
                    ->where('products.category_id', $request->category);
            }
            if (!is_null($request->product_id)) {
                $cart_details = $cart_details
                    ->where('products.id', $request->product_id);
            }
            if ($request->request_type == "product"
                or $request->request_type == "special"
                or $request->request_type == "offer") {
                $cart_details = $cart_details
                    ->where('cart_details.item_type', $request->request_type);
            }
            $requests_data = Request::joinSub($cart_details, 'cart_details', function ($join) {
                $join->on('requests.cart_id', '=', 'cart_details.id_cart');
            });
            $requests_data = $requests_data->join('carts', 'requests.cart_id', 'carts.id')
                ->join('users', 'carts.user_id', 'users.id');

            if (!is_null($request->from)) {
                $requests_data = $requests_data->whereDate('requests.created_at', '>=', $request->from);
            }
            if (!is_null($request->to)) {
                $requests_data = $requests_data->whereDate('requests.created_at', '<=', $request->to);
            }
            if (!is_null($request->name)) {
                $requests_data = $requests_data->where('users.name', 'like', '%' . $request->name . '%');
            }
            if (!is_null($request->phone)) {
                $requests_data = $requests_data->where('users.phone', 'like', '%' . $request->phone . '%');
            }
            if (!is_null($request->gender)) {
                $requests_data = $requests_data->where('users.gender', $request->gender);
            }
            if (!is_null($request->payment_type)) {
                $requests_data = $requests_data->where('requests.payment_type', $request->payment_type);
            }
            if (!is_null($request->status)) {
                if ($request->status == 'delivered') {
                    $requests_data = $requests_data->where(function ($q) {
                        $q->where('requests.status', 'on_branch')
                            ->orWhere('requests.status', 'delivered');
                    });
                } else {
                    $requests_data = $requests_data->where('requests.status', $request->status);
                }
            }
            if (!is_null($request->request_no)) {
                $requests_data = $requests_data->where('requests.id', $request->request_no);
            }
            if (!is_null($request->quantity)) {
                $requests_data = $requests_data->where('quantity', $request->quantity);
            }
            if (!is_null($request->branch_name)) {
                $requests_data = $requests_data->where('branch_id', $request->branch_name);
            }
            if (!is_null($request->carrier)) {
                $requests_data = $requests_data->where('carrier_id', $request->carrier);
            }
            if ($request->request_type == "coupons") {
                $requests_data = $requests_data->where('requests.coupon_id', '!=', null);
            }
            if (!is_null($request->coupon)) {
                $requests_data = $requests_data->where('requests.coupon_id', $request->coupon);
            }
            $requests_data = $requests_data->select('requests.*', 'quantity', 'requests.cache_payment');

            $requests_data = $requests_data->get();
        } else {
            $requests_data = Request::where('id', 0)->get();
        }

        return $requests_data;
    }

    public function cartDetailsWithProducts()
    {
        $cart_details = Cart::join('cart_details', 'carts.id', '=', 'cart_details.cart_id')
            ->join('product_prices', 'cart_details.product_price_id', 'product_prices.id')
            ->join('products', 'product_prices.product_id', 'products.id')
            ->select('cart_details.cart_id as id_cart', DB::raw('sum(cart_details.quantity) as quantity'))
            ->groupBy('id_cart');

        return $cart_details;
    }

    public function export(RequestPost $request)
    {
        $request->request->add(['confirm' => 1]);
        $data = $this->getReport($request);
        return Excel::download(
            new RequestExport($data),
            'requests-' . now() . '.xlsx'
        );
    }

}
