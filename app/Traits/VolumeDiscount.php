<?php
/**
 * Http web routes
 * php version 7.3.1
 *
 * @category Web
 * @package  Moka_APIs
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
namespace App\Traits;

use App\Models\VolumeDiscount as AppVolumeDiscount;

trait VolumeDiscount
{
    /**
     * Check if value is between two numbers.
     *
     * @param double $value          compare
     * @param double $start          number
     * @param double $end            number
     * @param double $is_equal_start lower value
     * @param double $is_equal_end   upper value
     * @param double $check_order    false => start is lower, end is upper
     *                               true => start or end is lower or upper
     *
     * @return bool
     */
    public function between(
        $value,
        $start,
        $end,
        $is_equal_start = true,
        $is_equal_end = false,
        $check_order = false
    ) {
        if ($check_order) {
            if ($start < $end) {
                $lower = $start; $upper = $end;
            } else {
                $lower = $end; $upper = $start;
            }
        } else {
            $lower = $start;
            $upper = $end;
        }

        if ($is_equal_start && $is_equal_end) {
            return (($value >= $lower) && ($value <= $upper));
        }

        if ($is_equal_start) {
            return (($value >= $lower) && ($value < $upper));
        }

        if ($is_equal_end) {
            return (($value > $lower) && ($value <= $upper));
        }

        return (($value > $lower) && ($value < $upper));
    }

    /**
     * Check if passed price has a discount
     *
     * @param $price int
     *
     * @return \App\VolumeDiscount | null
     */
    public function checkVolumeDiscount($price)
    {
        $discount = AppVolumeDiscount::ofActive()->get();

        $data = [
            'id' => null, 'price_from' => null, 'price_to' => null,
            'value' => null, 'value_type' => null, 'note' => null,
        ];

        foreach ($discount as $d) {
            $stored_data =[
                'id' => $d->id,
                'price_from' => $d->price_from,
                'price_to' => $d->price_to,
                'value' => $d->value,
                'value_type' => $d->value_type,
                'note' => $d->note
            ];
            if ($d->price_from > $data['price_from']) {
                // ∞
                if ($d->price_to == null) {
                    if ($price >= $d->price_from) {
                        $data = $stored_data;
                    }
                } else {
                    if ($this->between($price, $d->price_from, $d->price_to)) {
                        $data = $stored_data;
                    }
                }
            }
        }

        return AppVolumeDiscount::find($data['id']);
    }
}
