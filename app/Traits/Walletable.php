<?php

namespace App\Traits;

use App\Models\Wallet;

trait Walletable
{
    /**
     * Get the wallet.
     */
    public function wallet()
    {
        return $this->morphOne(Wallet::class, 'walletable');
    }
}
