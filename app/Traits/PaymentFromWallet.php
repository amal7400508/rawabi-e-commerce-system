<?php
/**
 * Http Controllers
 * php version 7.3.1
 *
 * @category Controllers
 * @package  Mokasweets.com
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Traits;

use App\Models\Account;
use App\Models\BalanceTransaction;
use App\Models\Branch;
use App\Models\BranchesCategory;
use App\Models\CartDetail;
use App\Models\Commission;
use App\Models\DeliveryPrice;
use App\Models\MainCategoryCategory;
use App\Models\PharmacyCategory;
use App\Models\Prescription;
use App\Models\Request;
use App\Models\Wallet;
use function Complex\theta;

/**
 * Payment From Wallet Transaction
 *
 * @category Controllers
 *
 * @package Mokasweets.com
 *
 * @author BySwadi <muath.ye@gmail.com>
 *
 * @license IC infinitecloud.co
 *
 * @link Moka_Sweets https://www.mokasweets.com/
 *
 * @bySwadi
 */
trait PaymentFromWallet
{
    /**
     * Payment From Wallet Transaction
     *
     * @param $input mixed Illuminate\Http\Request  Request of data
     * @param $wallet
     * @param $wallet mixed App\userWallet          Current authenticated user wallet
     * //  * * @param $remaining_amount float               The amount to be deducted or paid in the absence of a coupon discount
     * @param $requestM mixed App\Request           The created request data
     *
     * @param string $currency
     * @return void
     */
    public function financialAccounts($input, $wallet, $remaining_amount, $requestM, $currency = 'ريال يمني')
    {
        if (!is_null($requestM->volume_discount_id)) {
            $volume_discount = \App\Models\VolumeDiscount::find($requestM->volume_discount_id) ?? null;
            if (!is_null($volume_discount)) {
                if ($volume_discount->branch_id == $input['branch_id']) {
                    $discount_value = $volume_discount['value'];
                    $remaining_amount = ($volume_discount['value_type'] == 'percentage')
                        ? $remaining_amount - (($discount_value / 100) * $remaining_amount)
                        : max(($remaining_amount - $discount_value), 0);
                }
            }
        }

        //        في حالة كان الطلب بدون شركات التأمين
        if (is_null($requestM->cart->prescription->insurance_company_id ?? null)) {
            return $this->financialOperations($input, $wallet, $remaining_amount, $requestM, $currency);
        }

        //        في حالة كان الطلب عبر شركات التأمين
        return $this->financialOperationsWithInsurance($input, $wallet, $remaining_amount, $requestM, $currency);
    }

    public function financialOperations($input, $wallet, $remaining_amount, $requestM, $currency = 'ريال يمني')
    {
        $operation_type_id = 3;
        $delivery_price = DeliveryPrice::find($input['delivery_price_id'])->price ?? 0;
        $amount = $remaining_amount;

        if ($currency == 'ريال يمني') {
            $remaining_amount = $remaining_amount + $delivery_price;
        }

        if ($input['payment_type'] == "fromWallet") {
            $operation_type_id = 4;
            $remain = $wallet->current_balance - $remaining_amount;

            BalanceTransaction::create(
                [
                    'amount' => $remain > 0 ? $remaining_amount : $wallet['current_balance'],
                    'type' => "subtract",
                    'wallet_id' => $wallet['id'],
                    'operation_type_id' => $operation_type_id,
                    'balance_transactionable_id' => $requestM['id'],
                    'balance_transactionable_type' => Request::class,
                    'transactionable_id' => $input['branch_id'],
                    'transactionable_type' => Branch::class,
                ]
            );

            $wallet->current_balance = $remain >= 0 ? $remain : 0;
            $wallet->save();
        } else {
            //الصندوق الرئيسي
            $account = Account::find(3);
            $account_box = $account = Wallet::where('currency', 'ريال يمني')->first();
            $main_box = $account_box->current_balance;

            if ($currency == 'ريال يمني') {
                BalanceTransaction::create([
                    'amount' => $remaining_amount,
                    'currency' => $currency,
                    'type' => 'subtract',
                    'wallet_id' => $account_box->id,
                    'operation_type_id' => $operation_type_id,
                    'balance_transactionable_id' => $requestM['id'],
                    'balance_transactionable_type' => Request::class,
                    'transactionable_id' => $input['branch_id'],
                    'transactionable_type' => Branch::class,
                ]);

                Wallet::where('id', $account_box->id)->update([
                    'current_balance' => $main_box - $remaining_amount
                ]);
            } else {
                BalanceTransaction::create([
                    'amount' => $delivery_price,
                    'currency' => 'ريال يمني',
                    'type' => 'subtract',
                    'wallet_id' => $account = Wallet::where('currency', 'ريال يمني')->first()->id,
                    'operation_type_id' => $operation_type_id,
                    'balance_transactionable_id' => $requestM['id'],
                    'balance_transactionable_type' => Request::class,
                    'transactionable_id' => $input['branch_id'],
                    'transactionable_type' => Branch::class,
                ]);

                Wallet::where('id', $account = Wallet::where('currency', 'ريال يمني')->first()->id)->update([
                    'current_balance' => $account = Wallet::where('currency', 'ريال يمني')->first()->current_balance - $delivery_price
                ]);

                BalanceTransaction::create([
                    'amount' => $remaining_amount,
                    'currency' => $currency,
                    'type' => 'subtract',
                    'wallet_id' => $account_box->id,
                    'operation_type_id' => $operation_type_id,
                    'balance_transactionable_id' => $requestM['id'],
                    'balance_transactionable_type' => Request::class,
                    'transactionable_id' => $input['branch_id'],
                    'transactionable_type' => Branch::class,
                ]);

                Wallet::where('id', $account_box->id)->update([
                    'current_balance' => $main_box - $remaining_amount
                ]);
            }
        }
    }

        // نسبة التوصيل
//        $this->deliveryRate($requestM, $input, $operation_type_id);
//
//        // عمولة التطبيق
//        $commission = $this->appRate($requestM, $input, $operation_type_id, $amount, $currency);
//
//        // اضافة المبلغ للفرع
//
//        $branch_wallet = Branch::find($input['branch_id'])->wallets->where('currency', $currency)->first();
//        $remaining_amount_for_branch = $amount - $commission;
//        $branch_wallet_box = $branch_wallet->current_balance;
//
//        BalanceTransaction::create([
//            'amount' => $remaining_amount_for_branch,
//            'currency' => $currency,
//            'type' => 'add',
//            'wallet_id' => $branch_wallet->id,
//            'operation_type_id' => $operation_type_id,
//            'balance_transactionable_id' => $requestM['id'],
//            'balance_transactionable_type' => Request::class,
//            'transactionable_id' => $input['branch_id'],
//            'transactionable_type' => Branch::class,
//        ]);
//
//        Wallet::where('id', $branch_wallet->id)->update([
//            'current_balance' => $branch_wallet_box + $remaining_amount_for_branch
//        ]);
//    }

    public function financialOperationsWithInsurance($input, $wallet, $remaining_amount, $requestM, $currency = 'ريال يمني')
    {
        $operation_type_id = 3;
        $prescription_rate = $requestM->cart->prescription->insurance_rate;

        $delivery_price = DeliveryPrice::find($input['delivery_price_id'])->price ?? 0;
        $amount = $remaining_amount;

        //
        if ($currency == 'ريال يمني') {
            $remaining_amount = $remaining_amount + $delivery_price;
        }

        // اذا كان الدفع عبر محفظة المستخدم
        if ($input['payment_type'] == "fromWallet") {
            $operation_type_id = 4;
            $remain = $wallet->current_balance - $remaining_amount;

            BalanceTransaction::create(
                [
                    'amount' => $remain > 0 ? $remaining_amount : $wallet['current_balance'],
                    'type' => "subtract",
                    'wallet_id' => $wallet['id'],
                    'operation_type_id' => $operation_type_id,
                    'balance_transactionable_id' => $requestM['id'],
                    'balance_transactionable_type' => Request::class,
                    'transactionable_id' => $input['branch_id'],
                    'transactionable_type' => Branch::class,
                ]
            );

            $wallet->current_balance = $remain >= 0 ? $remain : 0;
            $wallet->save();
        } else {
            // اذا كان الدفع نقداً عند الاستلام
            //الصندوق الرئيسي
            $account = Account::find(3);
            $account_box = $account = Wallet::where('currency','ريال يمني')->first();
            $main_box = $account_box->current_balance;

            $remaining = $remaining_amount - (($prescription_rate * $amount) / 100);
            if ($currency == 'ريال يمني') {
                BalanceTransaction::create([
                    'amount' => $remaining,
                    'currency' => $currency,
                    'type' => 'subtract',
                    'wallet_id' => $account_box->id,
                    'operation_type_id' => $operation_type_id,
                    'balance_transactionable_id' => $requestM['id'],
                    'balance_transactionable_type' => Request::class,
                    'transactionable_id' => $input['branch_id'],
                    'transactionable_type' => Branch::class,
                ]);

                Wallet::where('id', $account_box->id)->update([
                    'current_balance' => $main_box - $remaining
                ]);
            } else {
                BalanceTransaction::create([
                    'amount' => $delivery_price,
                    'currency' => 'ريال يمني',
                    'type' => 'subtract',
                    'wallet_id' => $account = Wallet::where('currency','ريال يمني')->first()->id,
                    'operation_type_id' => $operation_type_id,
                    'balance_transactionable_id' => $requestM['id'],
                    'balance_transactionable_type' => Request::class,
                    'transactionable_id' => $input['branch_id'],
                    'transactionable_type' => Branch::class,
                ]);

                Wallet::where('id', $account = Wallet::where('currency','ريال يمني')->first()->id)->update([
                    'current_balance' => $account = Wallet::where('currency','ريال يمني')->first()->current_balance - $delivery_price
                ]);

                BalanceTransaction::create([
                    'amount' => $remaining,
                    'currency' => $currency,
                    'type' => 'subtract',
                    'wallet_id' => $account_box->id,
                    'operation_type_id' => $operation_type_id,
                    'balance_transactionable_id' => $requestM['id'],
                    'balance_transactionable_type' => Request::class,
                    'transactionable_id' => $input['branch_id'],
                    'transactionable_type' => Branch::class,
                ]);

                Wallet::where('id', $account_box->id)->update([
                    'current_balance' => $main_box - $remaining
                ]);
            }
        }

        // نسبة التوصيل
//        $this->deliveryRate($requestM, $input, $operation_type_id);
//
//        // عمولة التطبيق
//        $commission = $this->appRate($requestM, $input, $operation_type_id, $amount, $currency);
////
////         اضافة المبلغ للفرع
//        $branch_wallet = Branch::find($input['branch_id'])->wallets->where('currency', $currency)->first();
//        $remaining_amount_for_branch = $amount - $commission;
//        $branch_wallet_box = $branch_wallet->current_balance;
//
//        BalanceTransaction::create([
//            'amount' => $remaining_amount_for_branch,
//            'currency' => $currency,
//            'type' => 'add',
//            'wallet_id' => $branch_wallet->id,
//            'operation_type_id' => $operation_type_id,
//            'balance_transactionable_id' => $requestM['id'],
//            'balance_transactionable_type' => Request::class,
//            'transactionable_id' => $input['branch_id'],
//            'transactionable_type' => Branch::class,
//        ]);
//
//        Wallet::where('id', $branch_wallet->id)->update([
//            'current_balance' => $branch_wallet_box + $remaining_amount_for_branch
//        ]);

        // تقييد المبلغ على شركة التأمين
        $insurance_company_account = $requestM->cart->prescription->insuranceCompany->provider->branch->first()
            ->wallet->where('currency', $currency)->first();

        $insurance_company_current_balance = $insurance_company_account->current_balance;

        $remaining = ($prescription_rate * $amount) / 100;
//        if ($currency == 'ريال يمني') {
        BalanceTransaction::create([
            'amount' => $remaining,
            'currency' => $currency,
            'type' => 'subtract',
            'wallet_id' => $insurance_company_account->id,
            'operation_type_id' => $operation_type_id,
            'balance_transactionable_id' => $requestM['id'],
            'balance_transactionable_type' => Request::class,
            'transactionable_id' => $input['branch_id'],
            'transactionable_type' => Branch::class,
        ]);

        Wallet::where('id', $account_box->id)->update([
            'current_balance' => $insurance_company_current_balance - $remaining
        ]);
    }
//    }

    /**
     * نسبة التوصيل
     *
     * @param $requestM
     * @param $input
     * @param $operation_type_id
     */
    public function deliveryRate($requestM, $input, $operation_type_id)
    {
        // نسبة التوصيل
        $delivery_price = DeliveryPrice::find($input['delivery_price_id'])->price ?? 0;
        $delivery_price_account = Account::find(2);
        $delivery_price_wallet = $delivery_price_account = Wallet::first();
        $delivery_price_wallet_box = $delivery_price_wallet->current_balance;

        BalanceTransaction::create([
            'amount' => $delivery_price,
            'type' => 'add',
            'wallet_id' => $delivery_price_wallet->id,
            'operation_type_id' => $operation_type_id,
            'balance_transactionable_id' => $requestM['id'],
            'balance_transactionable_type' => Request::class,
            'transactionable_id' => $input['branch_id'],
            'transactionable_type' => Branch::class,
        ]);

        Wallet::where('id', $delivery_price_wallet->id)->update([
            'current_balance' => $delivery_price_wallet_box + $delivery_price
        ]);
    }

    /**
     * @param $requestM
     * @param $input
     * @param $operation_typ_id
     * @param $amount
     * @param $currency
     * @return float|int
     */
    public function appRate($requestM, $input, $operation_type_id, $amount, $currency)
    {
        // عمولة التطبيق

        $cart_details = CartDetail::where('cart_id', $requestM->cart_id)->get();

        $app_ratio = 0;
        foreach ($cart_details as $cart_detail) {
            if ($cart_detail->item_type == 'product') {
                $category_id = $cart_detail->productPrice->product->category->id;
                $commission_category = Commission::where('branch_id', $input['branch_id'])->where('category_id', $category_id);
                if ($commission_category->count()) {
                    $ratio = $commission_category->first()->commission;
                } else {
                    $main_category_id = MainCategoryCategory::where('category_id', $category_id)->get()[0]->main_category_id;
                    $ratio = Commission::where('branch_id', $input['branch_id'])
                            ->where('main_category_id', $main_category_id)
                            ->first()->commission ?? null;

                    if (is_null($ratio)) {
                        $ratio = Commission::where('branch_id', $input['branch_id'])->where('type', 'others')->first()->commission ?? 0.00;
                    }
                }
                $total = $cart_detail->quantity * $cart_detail->productPrice->price;

                $total = ($total * $ratio) / 100;

                $app_ratio = $app_ratio + $total;
            } else {
                $ratio = Commission::where('branch_id', $input['branch_id'])->where('type', 'others')->first()->commission ?? 0.00;

                $total = 0;
                if ($cart_detail->item_type == 'special') {
                    $total = $cart_detail->quantity * $cart_detail->specialProduct->price;
                } elseif ($cart_detail->item_type == 'offer') {
                    $total = $cart_detail->quantity * $cart_detail->offer->price;
                } elseif ($cart_detail->item_type == 'prescription_reply') {
                    $total = $cart_detail->quantity * $cart_detail->prescriptionReply->medicine_price;
                }

                $total = ($total * $ratio) / 100;

                $app_ratio = $app_ratio + $total;
            }
        }

        $commission = $app_ratio;
        $app_ratio_account = Account::find(1);
        $app_ratio_wallet = $app_ratio_account=Wallet::where('currency','ريال يمني')->first();
        $app_ratio_wallet_box = $app_ratio_wallet->current_balance;

        BalanceTransaction::create([
            'amount' => $commission,
            'currency' => $currency,
            'type' => 'add',
            'wallet_id' => $app_ratio_wallet->id,
            'operation_type_id' => $operation_type_id,
            'balance_transactionable_id' => $requestM['id'],
            'balance_transactionable_type' => Request::class,
            'transactionable_id' => $input['branch_id'],
            'transactionable_type' => Branch::class,
        ]);

        Wallet::where('id', $app_ratio_wallet->id)->update([
            'current_balance' => $app_ratio_wallet_box + $commission
        ]);

        return $commission;
    }

    protected function commission($cart_id)
    {
        $cart_details = CartDetail::where('cart_id', $cart_id)->get();

        foreach ($cart_details as $cart_detail) {
            if ($cart_detail->item_type == 'product') {

            } else {

            }
        }
    }
}
