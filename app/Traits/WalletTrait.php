<?php
/**
 * Http web routes
 * php version 7.3.1
 *
 * @category Web
 * @package  Moka_APIs
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
namespace App\Traits;

use App\Models\User;
use App\Models\Wallet;

trait WalletTrait
{
    /**
     * Get the user's wallet.
     */
    public function wallet()
    {
//        return $this->morphMany('App\Models\Wallet', 'walletable');
        return $this->morphOne('App\Models\Wallet', 'walletable');
    }

    /**
     * Open balance
     * TODO: =>Test : App\Models\User::first()->openWallet(64)
     *
     * @param  int|null  $opening_balance  price amount
     *
     * @return int
     */
    public function openWallet($opening_balance = null)
    {
        $price_amount = $opening_balance ?? config('smartdelivery.opening-balance') ?? 0;

        // get new wallet data
        $wallet = new Wallet([
            'current_balance' =>  $price_amount,
            'openning_balance' =>  $price_amount
        ]);

        // get user|branch|so on data
        $model = $this->getModel();

        // insert wallet data for selected user
        $wallet = $model->wallet()->save($wallet);

        return $wallet;
    }
}
