<?php
/**
 * Http web routes
 * php version 7.3.1
 *
 * @category Web
 * @package  Moka_APIs
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Traits;

use App\Models\Account;
use App\Models\BalanceTransaction;
use App\Models\Branch;
use App\Models\Deposit;
use App\Models\Wallet;
use Illuminate\Support\Facades\DB;

trait TransactionTrait
{
    /**
     * Get the user's balanceTransactions.
     */
    public function balanceTransaction()
    {
        return $this->morphMany(
            'App\Models\balanceTransaction',
            'balance_transactionable',
            'balance_transactionable_type',
            'balance_transactionable_id',
            'id'
        );
    }

    /**
     * Get the service's, request's or so on transactions.
     */
    public function transaction()
    {
        return $this->morphMany(
            'App\Models\balanceTransaction',
            'transactionable',
            'transactionable_type',
            'transactionable_id',
            'id'
        );
    }

    /**
     * Add to balance
     *
     * @param float $amount money
     * @param int $wallet_id
     * @param int $branch_id
     * @param string $note
     *
     * @return int
     */
    public function addToBalance($amount, $wallet_id, $branch_id = null, $note = null)
    {
        // TODO: user DB::transaction
        // ^ get amount from method param
        // ^ get object from method param
        // * create new Transaction object with passed amount
        // ^ get current model name
        // ^ save transaction object to model by using transaction relation
        $data = array();

        DB::transaction(function () use ($amount, $wallet_id, $branch_id, $note, &$data) {
            $price_amount = $amount;

            // add to deposit
            Deposit::create([
                'amount' => $price_amount,
                'wallet_id' => $wallet_id,
//                'branch_id' => $branch_id,
                'is_approved' => 1,
                'note' => $note
            ]);

            // get new wallet data
            $balance = new BalanceTransaction([
                'amount' => $price_amount,
                'type' => 'add',
                'operation_type_id' => 2,
                'wallet_id' => $wallet_id
            ]);

            // get user|branch|so on data
            $model = $this->getModel();

            // Check if wallet is active
            if ($model->wallet->status == 0) {
                return DB::rollback();
            }

            // insert balance transaction data for selected user
            $data = $model->balanceTransaction()->save($balance);

            // update wallet for that use
            // get new wallet data
            $walletModel = $model;

            // insert wallet data for selected user
            $wallet = $walletModel->wallet()->update([
                'current_balance' => $model->wallet->current_balance + $price_amount
            ]);

            //الصندوق الرئيسي
            $account = Account::find(3);
            $account_box = Wallet::where('currency','ريال يمني')->first();
            $main_box = $account_box->current_balance;

            if($account && $account_box)
            {
                $main_box = $account_box->current_balance;

                BalanceTransaction::create([
                    'amount' => $price_amount,
                    'type' => 'add',
                    'wallet_id' => $account_box->id,
                    'operation_type_id' => 2,
                    'balance_transactionable_id' => $account->id,
                    'balance_transactionable_type' => Account::class,
                ]);

                Wallet::where('id', $account_box->id)->update([
                    'current_balance' => $main_box + $price_amount
                ]);
            }


        });
        return $data;
    }

    /**
     * Subtract from balance
     *
     * @param int $amount money
     * @param int $wallet_id
     * @param Illuminate\Database\Eloquent\Model $service like request, advertisement or so on
     *
     * @return int
     */
    public function SubFromBalance($amount, $wallet_id, $service)
    {
        // ^ get amount from method param
        // ^ get object from method param
        // * create new Transaction object with passed amount
        // ^ get current model name
        // ^ save transaction object to model by using transaction relation

        $data = array();

        DB::transaction(function () use ($amount, $wallet_id, $service, &$data) {

            $price_amount = $amount;

            $service = $service;
            $serviceId = $service->id;
            $serviceModel = $service->getModel();

            // get new wallet data
            $balance = new BalanceTransaction([
                'amount' => $price_amount,
                'type' => 'subtract',
                'wallet_id' => $wallet_id,
                'transactionable_id' => $serviceId,
                'transactionable_type' => get_class($serviceModel),
            ]);

            // get user|branch|so on data
            $model = $this->getModel();


            // Check if wallet is active
            if ($model->wallet->status == 0) {
                return DB::rollback();
            }

            // validate prices
            if (($model->wallet->current_balance - $price_amount) < 0) {
                return DB::rollback();
            }

            // insert balance transaction data for selected user
            $data = $model->balanceTransaction()->save($balance);

            // update wallet for that use
            // get new wallet data
            $walletModel = $model;

            // insert wallet data for selected user
            $data = $walletModel->wallet()->update([
                'current_balance' => $model->wallet->current_balance - $price_amount
            ]);

        });

        return $data;
    }


    /**
     * Add to balance
     *
     * @param int $deposit_id
     * @param string $note
     *
     * @return int
     */
    public function addToBalanceFromBranch($deposit_id, $branch_id = null, $note = null)
    {
        // TODO: user DB::transaction
        // ^ save transaction object to model by using transaction relation
        $data = array();

        DB::transaction(function () use ($deposit_id, $note, $branch_id, &$data) {
            $deposit = Deposit::find($deposit_id);
            $price_amount = $deposit->amount;
            $wallet_id = $deposit->wallet_id;

            $deposit->update([
                'is_approved' => 1,
//                'note' => $note
            ]);

            // get new wallet data
            $balance = new BalanceTransaction([
                'amount' => $price_amount,
                'type' => 'add',
                'operation_type_id' => 1,
                'wallet_id' => $wallet_id
            ]);

            // get user|branch|so on data
            $model = $this->getModel();

            // Check if wallet is active
            if ($model->wallet->status == 0) {
                return DB::rollback();
            }

            // insert balance transaction data for selected user
            $data = $model->balanceTransaction()->save($balance);

            // update wallet for that use
            // get new wallet data
            $walletModel = $model;

            // insert wallet data for selected user
            $wallet = $walletModel->wallet()->update([
                'current_balance' => $model->wallet->current_balance + $price_amount
            ]);

            //الصندوق الرئيسي
            $branch_wallet = Branch::find($branch_id)->wallet->where('currency', 'ريال يمني')->first();
            $branch_wallet_box = $branch_wallet->current_balance;


            BalanceTransaction::create([
                'amount' => $price_amount,
                'type' => 'subtract',
                'wallet_id' => $branch_wallet->id,
                'operation_type_id' => 1,
                'balance_transactionable_id' => $branch_id,
                'balance_transactionable_type' => Branch::class,
            ]);

            Wallet::where('id', $branch_wallet->id)->update([
                'current_balance' => $branch_wallet_box - $price_amount
            ]);
        });

        return $data;
    }
}
