<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Permission\Traits\HasRoles;

class Enterprise extends model implements TranslatableContract//, Auditable
{
//    use \OwenIt\Auditing\Auditable;
    use Translatable;
    use Notifiable;
    use SoftDeletes;
    use HasRoles;

    /**
     * @var string
     */
    public $guard_name = 'screen';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $translatedAttributes = ['name', 'address', 'admin_name'];

    protected $fillable = ['is_approved', 'status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'status',
        'translations',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $with = [
        /*'workingTimes',*/
        /*'provider'*/
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
//        'is_favorite',
        'actions',
        'app_edit_actions',
        'background_color_row',
//        'wallet_id',
    ];

    /**
     * Get the user's wallet.
     */
    public function wallet()
    {
        return $this->morphMany('App\Models\Wallet', 'walletable');
    }

    /**
     * Get active resource.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

//    public function getWalletIdAttribute()
//    {
//        return $this->wallet[0]->id;
//    }

    public function scopeActiveProvider($query)
    {
        return $query->whereHas('provider', function ($q) {
            $q->where('status', 1)->where('is_approved', 1);
        });
    }

    public function scopePharmacy($query)
    {
        return $query->whereHas('provider', function ($q) {
            $q->where('type', 'pharmacy');
          });
    }

    /**
     * Get areas for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    /**
     * Get provider for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function enterprise()
    {
        return $this->belongsTo(Enterprise::class, 'id');
    }

    public function appEdit()
    {
        return $this->hasMany(AppEdit::class);
    }

    public function appConfiguration()
    {
        return $this->hasMany(AppConfiguration::class);
    }

    public function deliveryPath()
    {
        return $this->hasMany(DeliveryPath::class);
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    /*public function branchFavorite()
    {
        return $this->belongsToMany('App\Models\User', 'user_branch_favorites')
            ->using(UserBranchFavorite::class)
            ->withPivot(
                [
                    'branch_id',
                    'user_id',
                ]
            );
    }*/

    /**
     * Check if current branch is favorite for authenticated user
     *
     * @return boolean
     */
/*    public function getIsFavoriteAttribute()
    {
        if (!auth()->check()) {
            return false;
        }

        return $this->branchFavorite->count() > 0 ? true : false;
    }*/


    public function getActionsAttribute()
    {
        if (Gate::check('update enterprises')) {
            if ($this->is_approved == 0) {
                return
                    '<div class="btn-group" role="group" aria-label="Basic example">' .
                    '<button type="button" class="show-detail-branch btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>' .
                    '<button type="button" class="reject btn btn-sm btn-outline-danger" id="' . $this->id . '">' . __('admin.reject') . '</button>' .
                    '<button type="button" class="accept btn btn-sm btn-outline-success" id="' . $this->id . '">' . __('admin.accept') . '</button>' .
                    '</div>';
            } else if ($this->is_approved == 3) {
                return
                    '<div class="btn-group" role="group" aria-label="Basic example">' .
                    '<button type="button" class="show-detail-branch btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>' .
                    '<button type="button" class="accept btn btn-sm btn-outline-danger" id="' . $this->id . '">' . __('admin.unacceptable') . '</button>' .
                    '</div>';
            }
        }
        if (Gate::check('update enterprises')) {
            return
                '<div class="btn-group" role="group" aria-label="Basic example">' .
                '<button type="button" class="show-detail-branch btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>' .
                '<button type="button" class="reject btn btn-sm btn-outline-success" id="' . $this->id . '">' . __('admin.acceptable') . '</button>' .
                '</div>';
        }

        return
            '<div class="btn-group" role="group" aria-label="Basic example">' .
            '<button type="button" class="show-detail-branch btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>' .
            '</div>';

    }

    public function getAppEditActionsAttribute()
    {
        $actions = '';
        if (Gate::check('create app_edit')) {
            if ($this->appEdit->count() != 0) {
                $actions .= '<a class="edit-table-row" id="' . $this->id . '" title="' . __('admin.edit') . '"><i class="ft-edit color-primary" style="margin: auto 8px"></i></a>';
            }
        }
        if (Gate::check('create app_edit')) {
            if ($this->appEdit->count() == 0) {
                $actions .= '<a class="openAddModal" id="' . $this->id . '" title="' . __('admin.create') . '"><i class="ft-plus-square color-info" ></i></a>';
            }
        }
        if (Gate::check('delete app_edit')) {
            if ($this->appEdit->count() != 0) {
                $actions .= '<a class="delete" id="' . $this->id . '" title="' . __('admin.delete') . '"><i class="ft-trash-2 color-red"></i></a>';
            }
        }

        return $actions;
    }

    public function getBackgroundColorRowAttribute()
    {
        $is_approved = $this->is_approved;
        if ($is_approved == 1) return ''; //active
        else if ($is_approved == 0) return 'background-color: #ff041508'; //waiting
        else if ($is_approved == 3) return 'background-color: #0f0f0f24'; //canceled
    }
}
