<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class VolumeDiscount extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;

    /**
     *
     * @var array
     */
    protected $fillable = ['is_approved', 'is_active'];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'price_with_type',
        'actions',
        'background_color_row',
        'branch_name',
//        'active',
    ];

    /**
     * Get active discount
     *
     * @param mixed $query .
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function scopeOfActive($query)
    {
        return $query->where('is_active', 1)->where(
            function ($qu) {
                return $qu->where('start_date', '<=', Carbon::now())
                    ->where(
                        function ($q) {
                            return $q->where('end_date', '>=', Carbon::now())
                                ->orWhere('end_date', Carbon::today());
                        }
                    );
            }
        );
    }

    /**
     * Get branch for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function getPriceWithTypeAttribute()
    {
        $price = $this->value;
        if ($this->value_type === 'percentage')
            return "% " . $price;

        return $price;
    }

    public function getBranchNameAttribute()
    {
    return $this->branch->name ?? '' ;
    }
    public function getActionsAttribute()
    {
        if (Gate::check('update volume_discounts')) {
            if ($this->is_approved == 0) {
                return
                    '<div class="btn-group" role="group" aria-label="Basic example">' .
                    '<button type="button" class="show-detail btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>' .
                    '<button type="button" class="reject btn btn-sm btn-outline-danger" id="' . $this->id . '">' . __('admin.reject') . '</button>' .
                    '<button type="button" class="accept btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.accept') . '</button>' .
                    '</div>';
            }
        }

        return '<button type="button" class="show-detail btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>';
    }

    public function getBackgroundColorRowAttribute()
    {
        $is_approved = $this->is_approved;
        if ($is_approved == 1) return ''; //active
        else if ($is_approved == 0) return 'background-color: #ff041508'; //waiting
        else if ($is_approved == 3) return 'background-color: #0f0f0f24'; //canceled
    }
}
