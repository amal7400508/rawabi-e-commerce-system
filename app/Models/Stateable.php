<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stateable extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'stateable_id',
        'stateable_type'
    ];

    protected $appends = ['state_name'];

    /**
     * Get the parent stateable model (user or post).
     */
    public function stateable()
    {
        return $this->morphTo();
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function getStateNameAttribute()
    {
        return $this->state->name ?? null;
    }

}
