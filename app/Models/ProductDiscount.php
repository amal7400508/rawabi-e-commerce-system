<?php
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
class ProductDiscount extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;



    public $appends = [
        'start_date_carbon',
        'end_date_carbon',
    ];

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productPrice()
    {
        return $this->belongsTo('App\Models\ProductPrice');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * Get active discount .
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @BySwadi important
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true)
            ->where('start_date', '<=', Carbon::now())
            ->where('end_date', '>', Carbon::now()->subDay());
    }

    public function getStartDateCarbonAttribute()
    {
        return Carbon::Parse($this->start_date)->format('Y-m-d');
    }

    public function getEndDateCarbonAttribute()
    {
        return Carbon::Parse($this->end_date)->format('Y-m-d');
    }

}
