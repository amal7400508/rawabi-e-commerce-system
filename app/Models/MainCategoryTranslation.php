<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class MainCategoryTranslation extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;
    public $hidden = ['id', 'translations'];

    public function mainCategory()
    {
        return $this->belongsTo('App\Models\MainCategory');
    }
}
