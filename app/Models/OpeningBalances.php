<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class OpeningBalances extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded=[];

    /**
     * The attributes that are translated.
     *
     * @var array
     */


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [

        'actions',
        'product_name',
//        'product_pricing',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

        'created_at',
        'updated_at',
        'deleted_at',

    ];


    /**
     * Get active resource.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */


    /**
     * Get active resource.
     *
     * @return bool
     */

    /**
     * Get country for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */


    /**
     * The buttons in datatable
     */

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getProductNameAttribute()
    {
        return $this->product->name;
    }

//
//    public function price()
//    {
//        return $this->belongsTo(ProductPrice::class);
//    }

//
//    public function getProductPricingAttribute()
//    {
//        return $this->pricing->price;
//    }


    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('delete stocks')) {
            $actions .= '<a class="delete" id="' . $this->id . '" title="' . __('admin.delete') . '"><i class="ft-trash-2 color-red" style="margin: auto 8px"></i></a>';
        }
        if (Gate::check('update stocks')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '" title="' . __('admin.edit') . '"><i class="ft-edit color-primary"></i></a>';
        }

        return $actions;
    }

//    public function getBackgroundColorRowAttribute()
//    {
//        return $this->status != 1 ? 'background-color: #ff041508;' : '';
//    }

}
