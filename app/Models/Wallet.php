<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Wallet extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'current_balance',
        'currency',
        'walletable_id',
        'walletable_type',
    ];

    /**
     * Get the parent walletable model (user or post).
     */
    public function walletable()
    {
        return $this->morphTo();
    }
}
