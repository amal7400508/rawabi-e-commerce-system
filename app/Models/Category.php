<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


use Illuminate\Notifications\Notifiable;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class Category extends model implements TranslatableContract , Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Translatable;


    use Notifiable;

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $translatedAttributes = ['name', 'details', 'image'];

    public $appends = [
        'image_path_64',
        'image_path_100',
        'image_path',
        'background_color_row',
        'actions',
        'category_name',
    ];

    protected $fillable = ['level','main_category_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'translations',
        "created_at",
        "updated_at",
        "deleted_at",
    ];

    /**
     * Get active resource.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function product()
    {
        return $this->hasMany('App\Product')->where('type', 'public');
    }

    public function Main()
    {
        return $this->belongsTo(MainCategory::class,'main_category_id');
    }

    public function getCategoryNameAttribute()
    {
        return $this->main->name;
    }

    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('delete categories')) {
            $actions .= '<a class="delete" id="' . $this->id . '"><i class="ft-trash-2 color-red"></i></a>';
        }
        $actions .= '<a class="show-detail-category" id="' . $this->id . '"><i class="ft-eye color-primary" style="margin: auto 8px"></i></a>';
        if (Gate::check('update categories')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '"><i class="ft-edit color-primary"></i></a>';
        }
        return $actions;
    }

    public function getImagePath64Attribute()
    {
        return asset('storage/categorise/64/' . $this->image);
    }

    public function getImagePath100Attribute()
    {
        return asset('storage/categorise/100/' . $this->image);
    }

    public function getImagePathAttribute()
    {
        return asset('storage/categorise/' . $this->image);
    }

    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }





}
