<?php
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
class Image extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * Save images in public path.
     *
     * @param Illuminate\Support\Facades\Request $request     .
     * @param string                             $var_name    .
     * @param string                             $folder_name .
     *
     * @return string
     */
    public static function savePublicImage($request, $var_name, $folder_name)
    {
        $custom_folder_name = $folder_name.'/'.date('y-m-d');
        $custom_file_name = time()
            .'-'
            .$request->file($var_name)->getClientOriginalName();
        $request->file($var_name)->storeAs(
            $custom_folder_name,
            $custom_file_name,
            'public'
        );

        return $custom_folder_name.'/'.$custom_file_name;
    }
}
