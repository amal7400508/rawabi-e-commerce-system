<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class UnitType extends Model implements TranslatableContract, Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Translatable;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass translated.
     *
     * @var array
     */
    public $translatedAttributes = ['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'translations',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'actions',
        'background_color_row'
    ];


    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('delete unit_types')) {
            $actions .= '<a class="delete" id="' . $this->id . '" title="' . __('admin.delete') . '"><i class="ft-trash-2 color-red" style="margin: auto 8px"></i></a>';
        }
        if (Gate::check('update unit_types')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '" title="' . __('admin.edit') . '"><i class="ft-edit color-primary"></i></a>';
        }
        return $actions;
    }

    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }
}
