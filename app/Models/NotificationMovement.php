<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class NotificationMovement extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;

    protected $fillable = [
        'is_approved',
        'approved_at'
    ];

    public $appends = [
        'buttons_index',
        'buttons_branches',
        'background_color_row_branches',
        'title',
        'message',
        'image_path'
    ];

    /**
     *
     * @return URL
     */
    public function getImagePathAttribute()
    {
        return asset('storage/notification_movements/' . $this->image);
    }

    /**
     * Get branch for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * Get user for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get notification for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notification()
    {
        return $this->belongsTo(Notification::class);
    }

    public function getTitleAttribute()
    {
        return $this->notification->notification_title ?? null;
    }

    public function getMessageAttribute()
    {
        return $this->notification->notification_message ?? null;
    }

    /**
     * The buttons in datatable
     */
    public function getButtonsIndexAttribute()
    {
        $actions = '<a class="show-detail" id="' . $this->id . '"><i class="ft-eye color-primary" style="margin: auto 8px"></i></a>';
        return $actions;
    }

    public function getButtonsBranchesAttribute()
    {
        if (Gate::check('update notification_movement')) {
            if ($this->is_approved == 0 and !is_null($this->branch_id)) {
                return
                    '<div class="btn-group" role="group" aria-label="Basic example">' .
                    '<button type="button" class="show-detail btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>' .
                    '<button type="button" class="reject btn btn-sm btn-outline-danger" id="' . $this->id . '">' . __('admin.reject') . '</button>' .
                    '<button type="button" class="accept btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.accept') . '</button>' .
                    '</div>';
            }
        }

        return '<button type="button" class="show-detail btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>';
    }

    public function getBackgroundColorRowBranchesAttribute()
    {
        $is_approved = $this->is_approved;
        if ($is_approved == 1) return ''; //active
        else if ($is_approved == 0) return 'background-color: #ff041508'; //waiting
        else if ($is_approved == 3) return 'background-color: #0f0f0f24'; //canceled
    }

}
