<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class MultiDelivery extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'actions',
    ];
    protected $table = 'multi_delivery';
    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
//        if (Gate::check('delete delivery_prices')) {
            $actions = '<a class="delete" id="' . $this->id . '" title="' . __('admin.delete') . '"><i class="ft-trash-2 color-red" style="margin: auto 8px"></i></a>';
//        }
//        if (Gate::check('update delivery_prices')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '" title="' . __('admin.edit') . '"><i class="ft-edit color-primary"></i></a>';
//        }
        return $actions;
    }

}
