<?php

namespace App\Models;

use App\Traits\WalletTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Contracts\Auditable;

class Account extends Authenticatable implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $appends = ['wallet_id'];

    /**
     * Get the user's wallet.
     */
    public function wallet()
    {
        return $this->morphMany(Wallet::class, 'walletable');
    }

    public function getWalletIdAttribute()
    {
        return $this->wallet[0]->id ?? null;
    }
}
