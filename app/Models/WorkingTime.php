<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;

class WorkingTime extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;
    use SoftDeletes;

    protected $fillable = ['id', 'start_time', 'end_time', 'message', 'status', 'week_day'];

    public $appends = ['day'];

    public function getDayAttribute()
    {
        $week_day = $this->week_day;

        switch ($week_day) {
            case "SATURDAY":
                return __('admin.saturday');
                break;
            case "SUNDAY":
                return __('admin.sunday');
                break;
            case "MONDAY":
                return __('admin.monday');
                break;
            case "TUESDAY":
                return __('admin.tuesday');
                break;
            case "WEDNESDAY":
                return __('admin.wednesday');
                break;
            case "THURSDAY":
                return __('admin.thursday');
                break;
            case "FRIDAY":
                return __('admin.friday');
                break;
            default:
                return "no week day";
        }

    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
}
