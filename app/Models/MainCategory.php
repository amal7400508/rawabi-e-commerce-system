<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class MainCategory extends Model implements TranslatableContract , Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Translatable;
//    use SoftDeletes;

    protected $appends = ['is_leaf', 'branches_count',/* 'category_count',*/'pharmacy_categories_count', 'main_category_categories_count', 'leafs_count'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $translatedAttributes = ['name'];

    /**
     * @return bool true: if this is last category
     */
    public function getIsLeafAttribute()
    {
        $categories_sub_count = MainCategory::where('main_category_id', $this->id)
//            ->where('status',1)
            ->get()
            ->count();

        return $categories_sub_count > 0 ? true : false;
    }

    public function branchesCategory()
    {
        return $this->hasMany(BranchesCategory::class);
    }

    public function getBranchesCountAttribute()
    {
        return $this->branchesCategory->count();
    }

    public function Category()
    {
        return $this->hasMany(Category::class);
    }

    public function getCategoryCountAttribute()
    {
        return $this->Category->count();
    }

    public function mainCategoryCategories()
    {
        return $this->hasMany(MainCategoryCategory::class);
    }

    public function getMainCategoryCategoriesCountAttribute()
    {
        return $this->mainCategoryCategories->count();
    }

    public function pharmacyCategories()
    {
        return $this->hasMany(PharmacyCategory::class,'main_category');
    }

    public function getPharmacyCategoriesCountAttribute()
    {
        return $this->pharmacyCategories->count();
    }

    /**
     * Get number of leafs attribute
     *
     * @return void
     */
    public function getLeafsCountAttribute()
    {
        return $this->sub($this->id)->count();
    }

    /**
     * Get child resource.
     *
     * @param int $main_category_id .
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSub($query, $main_category_id)
    {
        return $query->where('main_category_id', $main_category_id);
    }

    /**
     * MainCategory
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sub()
    {
        return $this->hasMany(MainCategory::class, 'main_category_id');
    }

    /**
     * Get active resource.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

}
