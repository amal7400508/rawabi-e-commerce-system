<?php

namespace App\Models;

use App\Traits\TransactionTrait;
use App\Traits\WalletTrait;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Laravel\Passport\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements  Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasApiTokens;
    use Notifiable;
    use SoftDeletes;
//    use Walletable;
    use TransactionTrait;
    use WalletTrait;


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email_verified_at',
        'deleted_at',
        'image',
        'created_at',
        'updated_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    public $appends = ['image_path', 'actions', 'background_color_row', 'wallet_id'];

    /**
     * Get image url path.
     *
     * @return string
     */
    public function getImagePathAttribute()
    {
        return asset('storage/' . $this->image);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'birth_date' => 'date',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    // protected $with=['productFavorite'];

    /**
     * Get active resource.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }



    public function getWalletIdAttribute()
    {
        return $this->wallet->id ?? 0;
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function productFavorite()
    {
        return $this->belongsToMany('App\Models\Product', 'user_favorites')
            ->using(UserProductFavorite::class);
        // TODO: show the product with status of orderable in cart
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function providerFavorite()
    {
        return $this->belongsToMany(Provider::class, 'user_provider_favorites')
            ->using(UserProviderFavorite::class);
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function branchFavorite()
    {
        return $this->belongsToMany(Branch::class, 'user_branch_favorites')
            ->using(UserBranchFavorite::class);
    }

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function address()
    {
        return $this->hasMany('App\Models\UserAddress');
    }



    /**
     * Define a has-many-through relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function request()
    {
        return $this->hasManyThrough('App\Models\Request', 'App\Models\Cart');
        // return $this->hasMany('App\Models\Request')->using('App\Models\UserProductFavorite');
    }

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pointHistory()
    {
        return $this->hasMany('App\Models\PointHistory');
        // return $this->hasMany('App\Models\Request')->using('App\Models\UserProductFavorite');
    }

    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('create deposits')) {
            $actions .= '<button style="width:120px;" id="' . $this->id . '" class="deposit btn btn-primary btn-sm" title="' . Lang::get("admin.deposit") . '"><i class="ft ft-plus"></i>' . ' ' . Lang::get("admin.deposit") . '</button>';
            $actions .= '&nbsp';
        }
        if (Gate::check('update users')) {
            $actions .= '<button id="' . $this->id . '"  class="edit btn btn-primary btn-sm" title="' . Lang::get("admin.edit") . '"><i class="ft ft-edit"></i></button>';
            $actions .= '&nbsp';
        }
        $actions .= '<button type="button"  id="' . $this->id . '"  class="show-details btn btn-primary btn-sm" title="' . Lang::get("admin.show") . '"><i class="ft ft-eye"></i></button>';


        return $actions;
    }

    public function getBackgroundColorRowAttribute()
    {
        $status = $this->status;
        if ($status == 1) return ''; //active
        else if ($status == 2) return 'background-color: #ff041508'; //deleted
        else if ($status == 3) return 'background-color: #0f0f0f0d'; //blacklist
        else return 'background-color: #fbc02d1a'; //attitude
    }

}
