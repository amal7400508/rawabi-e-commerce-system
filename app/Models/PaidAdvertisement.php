<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class PaidAdvertisement extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $appends = [
        'background_color_row',
        'class_color_row',
        'actions',
        'branch_name',
        'advertisement_type_name'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    public $with = [
        'branch',
        'advertisementType',
    ];

    /**
     * Get branch for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * Get advertisement type for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advertisementType()
    {
        return $this->belongsTo(AdvertisementType::class);
    }

    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('delete paid_advertisements')) {
            $actions .= '<a class="delete" id="' . $this->id . '"><i class="ft-trash-2 color-red"></i></a>';
        }
        $actions .= '<a class="show-detail-paid-advertisement" id="' . $this->id . '"><i class="ft-eye color-primary" style="margin: auto 8px"></i></a>';
        if (Gate::check('update paid_advertisements')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '"><i class="ft-edit color-primary"></i></a>';
        }
        return $actions;
    }

    public function getBackgroundColorRowAttribute()
    {
        return /*$this->end_date < Carbon::now() || */ $this->status != 'نشط' ? 'background-color: #ff041508;' : '';
    }

    public function getClassColorRowAttribute()
    {
        return /*$this->end_date < Carbon::now() || */ $this->status != 'نشط' ? 'tr-color-red' : '';
    }

    public function getAdvertisementTypeNameAttribute()
    {
        return $this->advertisementType->name ?? null;
    }

    public function getBranchNameAttribute()
    {
        return $this->branch->name ?? null;
    }
}
