<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class WorkingTimeTranslation extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $timestamps = false;
    public $hidden = ['id', 'translations'];

    public function workingTime()
    {
        return $this->belongsTo('App\WorkingTime', "working_time_id");
    }
}
