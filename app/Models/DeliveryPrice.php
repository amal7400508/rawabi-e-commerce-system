<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class DeliveryPrice extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'actions',
        'area_from_name',
        'area_to_name',
        'background_color_row'
    ];

    /**
     * Get areas from for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function areaFrom()
    {
        return $this->belongsTo(Area::class, 'area_id_from');
    }

    /**
     * Get areas to for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function areaTo()
    {
        return $this->belongsTo(Area::class, 'area_id_to');
    }

    public function getAreaFromNameAttribute()
    {
        return $this->areaFrom->name ?? null;
    }

    public function getAreaToNameAttribute()
    {
        return $this->areaTo->name ?? null;
    }

    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('delete delivery_prices')) {
            $actions = '<a class="delete" id="' . $this->id . '" title="' . __('admin.delete') . '"><i class="ft-trash-2 color-red" style="margin: auto 8px"></i></a>';
        }
        if (Gate::check('update delivery_prices')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '" title="' . __('admin.edit') . '"><i class="ft-edit color-primary"></i></a>';
        }
        return $actions;
    }

    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }
}
