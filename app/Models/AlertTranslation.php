<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AlertTranslation extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $hidden = ['id', 'translations'];
    public $timestamps = false;
}
