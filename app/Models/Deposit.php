<?php

namespace App\Models;

use App\Traits\Walletable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class Deposit extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;

    // TODO: make user wallet relation.

    protected $fillable = ['amount', 'wallet_id', 'is_approved'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    public $appends = ['actions', 'background_color_row', 'current_balance'];

    /**
     * Get branch for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class,'wallet_id');
    }

    /**
     * Get wallet for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }

    public function getBackgroundColorRowAttribute()
    {
        $is_approved = $this->is_approved;
        if ($is_approved == 1) return ''; //active
        else if ($is_approved == 0) return 'background-color: #ff041508'; //waiting
        else if ($is_approved == 3) return 'background-color: #0f0f0f24'; //canceled
//        else return 'background-color: #fbc02d1a'; //attitude
    }

    public function getCurrentBalanceAttribute()
    {
        return $this->wallet->current_balance;
    }

    public function getActionsAttribute()
    {
        if (Gate::check('update deposits')) {
            if ($this->is_approved == 0 and !is_null($this->branch_id)) {
                return
                    '<div class="btn-group" role="group" aria-label="Basic example">' .
                    '<button type="button" class="reject btn btn-sm btn-outline-danger" id="' . $this->id . '">' . __('admin.reject') . '</button>' .
                    '<button type="button" class="accept btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.accept') . '</button>' .
                    '</div>';
            }
        }

        return '<a><small class="show-detail color-info" id="' . $this->id . '">' . __('admin.detail') . '...</small></a>';

    }
}
