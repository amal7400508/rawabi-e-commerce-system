<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class PrescriptionReply extends Model
{
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prescription_id',
        'medicine_name',
        'medicine_price',
        'note',
        'is_alternative',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Get This Table name.
     *
     * @return string
     */
    public function getTableNameAttribute()
    {
        // return $this->table();
        return $this->table;
    }

    /**
     * Define a polymorphic one-to-many relationship.
     * To use other notification table.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifications()
    {
        return $this->morphMany(\App\Models\SystemNotification::class, 'notifiable')
            ->orderBy('created_at', 'desc');
    }

    /**
     * Get prescription for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function prescription()
    {
        return $this->belongsTo(Prescription::class);
    }
}
