<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use OwenIt\Auditing\Contracts\Auditable;

class OfferDetail extends Pivot implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'offer_details';


    public $appends = ['price_name'];

    /**
     * Get offer for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    /**
     * Get product price for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productPrice()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    public function getPriceNameAttribute()
    {
        return ($this->productPrice->unit->name  . ' | ' . $this->productPrice->product->name) ?? null;
    }
}
