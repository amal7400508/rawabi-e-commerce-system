<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class Coupon extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;

    protected $fillable = ['status', 'is_approved'];

    public $appends = [
        'coupon_type_name',
        'price_with_type',
        'actions',
        'actions_branches',
        'background_color_row',
        'background_color_row_branches',
    ];

    /**
     * Get branch for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * Get coupon type for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function couponType()
    {
        return $this->belongsTo(CouponType::class, 'coupon_types_id');
    }

    public function getCouponTypeNameAttribute()
    {
        return $this->couponType->name ?? null;
    }

    public function getPriceWithTypeAttribute()
    {
        $price = $this->price;
        if ($this->value_type === 'percentage')
            return "% " . $price;

        return $price;
    }

    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        $actions .= '<div class="btn-group" role="group" aria-label="Basic example">';
        $actions .= '<button type="button" class="show-detail btn btn-sm btn-outline-primary" id="' . $this->id . '"><i class="ft-eye"></i></button>';
        if (Gate::check('update coupons')) {
            $actions .= '<button type="button" class="delete btn btn-sm btn-outline-danger" id="' . $this->id . '"><i class="ft-trash-2"></i></button>';
        }
        $actions .= '<div class="btn-group" role="group" aria-label="Basic example"></div>';
        return $actions;

        $actions = '<a class="delete" id="' . $this->id . '"><i class="ft-trash-2 color-red"></i></a>';
        $actions .= '<a class="show-detail" id="' . $this->id . '"><i class="ft-eye color-primary" style="margin: auto 8px"></i></a>';

        return $actions;
    }

    public function getActionsBranchesAttribute()
    {
        if (Gate::check('update coupons')) {
            if ($this->is_approved == 0 and !is_null($this->branch_id)) {
                return
                    '<div class="btn-group" role="group" aria-label="Basic example">' .
                    '<button type="button" class="show-detail btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>' .
                    '<button type="button" class="reject btn btn-sm btn-outline-danger" id="' . $this->id . '">' . __('admin.reject') . '</button>' .
                    '<button type="button" class="accept btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.accept') . '</button>' .
                    '</div>';
            }
        }
        return '<button type="button" class="show-detail btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>';
    }


    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }

    public function getBackgroundColorRowBranchesAttribute()
    {
        $is_approved = $this->is_approved;
        if ($is_approved == 1) return ''; //active
        else if ($is_approved == 0) return 'background-color: #ff041508'; //waiting
        else if ($is_approved == 3) return 'background-color: #0f0f0f24'; //canceled
    }
}
