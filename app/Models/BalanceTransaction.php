<?php
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
class BalanceTransaction extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wallet_id',
        'amount',
        'currency',
        'operation_type_id',
        'balance_transactionable_id',
        'balance_transactionable_type',
        'transactionable_id',
        'transactionable_type',
        'type',
    ];

    public $appends = [
        'balanceTransactionable_name',
        'operation_type_name',
        'wallet_type',
        'actions',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at',
//        'id',
        'note',
        'wallet_id',
        'balance_transactionable_id',
//        'balance_transactionable_type',
        'transactionable_id',
        'transactionable_type',
    ];

    /**
     * Get the owning balanceTransactionable model.
     */
    public function balanceTransactionable()
    {
        return $this->morphTo();
    }

    /**
     * Get the service's, request's or so on transactionable model.
     */
    public function transactionable()
    {
        return $this->morphTo();
    }

    public function operationType()
    {
        return $this->belongsTo(OperationType::class, "operation_type_id");
    }

    public function getBalanceTransactionableNameAttribute()
    {
        $this->balanceTransactionable->name;
    }

    public function getOperationTypeNameAttribute()
    {
        $this->operationType->name;
    }

    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('create receipt')) {
            $actions .= '<a class="delete" id="' . $this->id . '" title="' . __('admin.delete') . '"><i class="ft-trash-2 color-red" style="margin: auto 8px"></i></a>';
        }

        return $actions;
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }

    public function getWalletTypeAttribute()
    {
        $type = __('admin.no_data');
        if ($this->wallet->walletable_type == 'App\Models\User') {
            $type = __('admin.user');
        } else if ($this->wallet->walletable_type == 'App\Models\Account') {
            $type = __('admin.account');
        } else if ($this->wallet->walletable_type == 'App\Models\Branch') {
            $type = __('admin.a_branch');
        }

        return $type;
    }

}
