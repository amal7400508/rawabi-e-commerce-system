<?php

namespace App\Models;

use Illuminate\Notifications\DatabaseNotification;
use OwenIt\Auditing\Contracts\Auditable;

class SystemNotification extends DatabaseNotification
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_notifications';

    protected $fillable = [
        'type',
        'notifiable_type',
        'notifiable_id',
        'data',
//        'response_at'
    ];

    /**
     * Get the parent walletable model (user or post).
     */
    public function walletable()
    {
        return $this->morphTo('system_notifications', 'notifiable_type', 'notifiable_id');
    }

}
