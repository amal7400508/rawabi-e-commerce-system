<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use OwenIt\Auditing\Contracts\Auditable;

class Alert extends Model implements TranslatableContract, Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = ['title', 'content'];

    protected $fillable = ['is_approved'];

    public $appends = [
        'buttons_index',
        'buttons_attentions_branches',
        'background_color_row',
        'background_color_row_branches',
        'start_date_carbon',
        'end_date_carbon',
        'image_path',
    ];

    /**
     *
     * @return URL
     */
    public function getImagePathAttribute()
    {
        return asset('storage/alert/' . $this->image);
    }

    /**
     * Get branch for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * The buttons in datatable
     */
    public function getButtonsIndexAttribute()
    {
        $actions = '';
        if (Gate::check('delete attentions')) {
            $actions .= '<a class="delete" id="' . $this->id . '"><i class="ft-trash-2 color-red"></i></a>';
        }
        $actions .= '<a class="show-detail" id="' . $this->id . '"><i class="ft-eye color-primary" style="margin: auto 8px"></i></a>';
        if (Gate::check('update attentions')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '"><i class="ft-edit color-primary"></i></a>';
        }
        return $actions;
    }

    public function getButtonsAttentionsBranchesAttribute()
    {
        if (Gate::check('update attentions')) {
            if ($this->is_approved == 0 and !is_null($this->branch_id)) {
                return
                    '<div class="btn-group" role="group" aria-label="Basic example">' .
                    '<button type="button" class="show-detail btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>' .
                    '<button type="button" class="reject btn btn-sm btn-outline-danger" id="' . $this->id . '">' . __('admin.reject') . '</button>' .
                    '<button type="button" class="accept btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.accept') . '</button>' .
                    '</div>';
            }
        }

        return '<button type="button" class="show-detail btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>';
    }

    public function getStartDateCarbonAttribute()
    {
        return is_null($this->start_date) ? "" : Carbon::Parse($this->start_date)->format('Y-m-d');
    }

    public function getEndDateCarbonAttribute()
    {
        return is_null($this->end_date) ? "" : Carbon::Parse($this->end_date)->format('Y-m-d');
    }

    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }

    public function getBackgroundColorRowBranchesAttribute()
    {
        $is_approved = $this->is_approved;
        if ($is_approved == 1) return ''; //active
        else if ($is_approved == 0) return 'background-color: #ff041508'; //waiting
        else if ($is_approved == 3) return 'background-color: #0f0f0f24'; //canceled
    }
}
