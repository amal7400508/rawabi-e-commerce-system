<?php
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
class Cart extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'status',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    public $with = [
        'cartProductItems',
        'cartOfferItems',
        'cartSpecialItems',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Define a has-one-through relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    // public function product()
    // {
    //     return $this->hasOneThrough('App\Models\ProductPrice', 'App\Models\Product');
    // }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['price_sum', 'items_count', 'prescription'];

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cartProductItems()
    {
        //by sara mohammed
        return $this->belongsToMany('App\Models\ProductPrice', 'cart_details')
            ->using('App\Models\CartDetail')->withPivot(
                [
                    'id', 'quantity', 'product_price_id',
                ]
            );
    }

    public function cartDetail()
    {
        return $this->hasMany(CartDetail::class);
    }
    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cartPrescriptionReplyItems()
    {
        return $this->belongsToMany(PrescriptionReply::class, 'cart_details')
            ->using('App\Models\CartDetail')->withPivot(
                [
                    'id', 'quantity',
                ]
            );
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cartOfferItems()
    {
        return $this->belongsToMany('App\Models\Offer', 'cart_details')
            ->using('App\Models\CartDetail')->withPivot(
                [
                    'id', 'quantity',
                ]
            );
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cartSpecialItems()
    {
        return $this->belongsToMany('App\Models\SpecialProduct', 'cart_details')
            ->using('App\Models\CartDetail')->withPivot(
                [
                    'id', 'quantity',
                ]
            );
    }

    /**
     * Problem solve http://192.168.191.1:8000/api/request/29
     * the request details did not appear and this is her solve.
     *
     * @return int;
     */
    public function getItemsCountAttribute()
    {
        return 0;
    }

    /**
     * Get the total of all cart items prices.
     *
     * @return int
     */
    public function getPriceSumAttribute()
    {
        return $this->cartProductItems->sum(
            function ($item) {
                return $item['price'] * $item['pivot']['quantity'];
            }
        ) + $this->cartOfferItems->sum(
            function ($item) {
                return $item['price'] * $item['pivot']['quantity'];
            }
        ) + $this->cartSpecialItems->sum(
            function ($item) {
                return $item['price'] * $item['pivot']['quantity'];
            }
        ) + $this->cartPrescriptionReplyItems->sum(
            function ($item) {
                return $item['medicine_price'] * $item['pivot']['quantity'];
            }
        );
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function prescriptionReply()
    {
        return $this->belongsToMany(PrescriptionReply::class, 'cart_details')
            ->using('App\Models\CartDetail');

    }

    public function getPrescriptionAttribute()
    {
       return $this->prescriptionReply->first()->prescription ?? null;

    }


}
