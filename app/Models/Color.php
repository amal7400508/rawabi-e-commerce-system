<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class Color extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Translatable;
    use SoftDeletes;

    /**
     * The attributes that are translated.
     *
     * @var array
     */
    public $translatedAttributes = ['name'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'active',
        'actions',

        'background_color_row'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'translations',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Get active resource.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
//    public function scopeActive($query)
//    {
//        return $query->where('status', 1)->whereHas('country');
//    }

    /**
     * Get active resource.
     *
     * @return bool
     */
    public function getActiveAttribute()
    {
        return $this->status ? true : false;
    }

    /**
     * Get country for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */


    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('delete color')) {
            $actions .= '<a class="delete" id="' . $this->id . '" title="' . __('admin.delete') . '"><i class="ft-trash-2 color-red" style="margin: auto 8px"></i></a>';
        }
        if (Gate::check('update color')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '" title="' . __('admin.edit') . '"><i class="ft-edit color-primary"></i></a>';
        }

        return $actions;
    }

    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }
}
