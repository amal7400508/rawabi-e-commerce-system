<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class Note extends Model implements TranslatableContract, Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Translatable;
    use SoftDeletes;

    /**
     * The attributes that are translated.
     *
     * @var array
     */
    public $translatedAttributes = ['message'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'start_date' => 'date:Y-m-d',
        'end_date' => 'date:Y-m-d',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'actions',
        'active',
        'background_color_row',
        'start_date_carbon',
        'end_date_carbon'
    ];

    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('delete notes')) {
            $actions .= '<a class="delete" id="' . $this->id . '" title="' . __('admin.delete') . '"><i class="ft-trash-2 color-red" style="margin: auto 8px"></i></a>';
        }
        if (Gate::check('update notes')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '" title="' . __('admin.edit') . '"><i class="ft-edit color-primary"></i></a>';
        }
        return $actions;
    }

    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }

    /**
     * Get last message.
     *
     * @return bool
     */
    public function getActiveAttribute()
    {
        $is_current_date = Carbon::now() >= $this->start_date
            && (
                Carbon::now() <= $this->end_date
                || Carbon::parse($this->end_date)->isToday()
            );

        $is_active = $this->status ? true : false;

        return $is_current_date and $is_active;
    }

    /**
     * Get last message.
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public static function last()
    {
        return self::latest()->first();
    }

    public function getStartDateCarbonAttribute()
    {
        return Carbon::Parse($this->start_date)->format('Y-m-d');
    }

    public function getEndDateCarbonAttribute()
    {
        return Carbon::Parse($this->end_date)->format('Y-m-d');
    }
}
