<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class NewProduct extends Model
{
    use Translatable;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are translated.
     *
     * @var array
     */
    public $translatedAttributes = ['name', 'details', 'image', 'taste'];

    /**
     * Get quantity.
     *
     * @return int
     */
    public function getQuantityAttribute()
    {
        return $this->pivot->quantity + 0;
    }

    /**
     * Get link of the product.
     *
     * @return URL|string
     */
    public function getLinkAttribute()
    {
        return route(providerType() . 'product.show', $this->id);
        // Missing required parameters for [Route: product.show] [URI: api/v1/user/product/{product}].
        // return route('product.show', ['id' => $this->id]);
    }

    /**
     * Get is favorite.
     *
     * @return bool
     */
    public function getIsFavAttribute()
    {
        return $this->id;
    }

    /**
     * The relations to eager load on every query.
     * commented to solve many request issue
     * TODO: try to come here if you face a problem with products.
     *
     * @var array
     */
    //protected $with = ['category', 'prices', 'favorite'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['link', 'is_orderable', 'actions', 'categoryName'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'type',
//        'category_id',
        'deleted_at',
        'pivot',
        'translations',
    ];

    /**
     * Get state name of the product.
     *
     * @return string|exception
     */
    // public function getStateAttribute()
    // {
    //     return $this->state()->name;
    // }

    /**
     * Get is orderable product.
     *
     * @return bool
     */
    public function getIsOrderableAttribute()
    {
        // TODO: remove this attribute
        return true;
    }

    /**
     * Define an inverse one-to-one or many relationship.
     * TODO: remove this relation with state_id column
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(ProductPrice::class)
            ->orderAvailability();
    }

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productPrice()
    {
        return $this->hasMany(ProductPrice::class);
    }

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function add()
    {
        return $this->hasMany(Add::class);
    }

    /**
     * product_image a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productImage()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function favorite()
    {
        return $this->belongsToMany('App\Models\User', 'user_favorites')
            ->using('App\Models\UserFavorite')
            ->withPivot(
                [
                    'product_id',
                    'user_id',
                ]
            );
    }

    /**
     * The states that belong to the product.
     */
    public function states()
    {
        // return $this->belongsToMany('App\Models\State', 'product_state');
        return $this->belongsToMany('App\Models\State')
            ->using('App\Models\ProductState');
    }

    /**
     * Get only new product.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function scopeNew($query)
    {
        return $query->where('is_new', 1);
    }

    /**
     * Get only searched product.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     * @param \Illuminate\Http\Request $request .
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function scopeOfSearch($query, $request)
    {
        if (!empty($request->search)) {
            $searchFields = ['name'];

            return $query->where(
                function ($query) use ($request, $searchFields) {
                    $searchWildcard = '%' . $request->search . '%';
                    foreach ($searchFields as $field) {
                        $query->whereTranslationLike($field, $searchWildcard);
                    }
                }
            );
        }
        // if(isset($request->search))

        // return $query->where('name','like', '$request->search');
        return $query;
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cartDetail()
    {
        return $this->belongsToMany('App\Models\Cart', 'cart_details')
            ->using('App\Models\CartDetail')->withPivot(
                'quantity'
            )->orderBy('quantity', 'desc');
    }

    /**
     * Define a has-one-through relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    public function cartDetail1()
    {
        return $this->hasOneThrough('App\Models\Product', 'App\Models\Cart');
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function mostSellProduct()
    {
        return $this->belongsToMany('App\Models\Cart', 'cart_details')
            ->using('App\Models\CartDetail')
            ->selectRaw('count(quantity) as aggregate')
            ->groupBy('product_id');
    }

    /**
     * Already defined
     * -------------------------------------------------------------------
     * Scope a query to only include products of a given [product.is_new].
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     *
     * @return \Illuminate\Database\Eloquent\Builder
     *
     * @function scopeOfSearch()
     */

    /**
     * Already defined[needed]
     * -------------------------------------------------------------------
     * Scope a query to only include products of a given [product.is_new].
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request $is_new
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @function scopeNew() used for api
     * @BySwadi  for filters in products page
     */
    public function scopeIsNew($query, $is_new)
    {
        return isset($is_new->is_new) && $is_new->is_new !== 0
            ? $query->where('is_new', 1)
            : $query;
    }

    /**
     * Scope a query to only include products of a given [product.type].
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @BySwadi for filters in products page
     */
    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    /**
     * Scope a query to only include products of a given [product.category_id].
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $category_id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @BySwadi for filters in products page
     */
    public function scopeOfCategory($query, $category_id)
    {
        return
            isset($category_id->category_id) && $category_id->category_id !== 0
                ? $query->where('category_id', $category_id->category_id)
                : $query;
    }

    /**
     * Scope a query to only include available products.
     * status 1 = available, status 0 = unavailable.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool $status
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @BySwadi important
     */
    public function scopeAvailable($query, $status = 1)
    {
        return isset($status)
            ? $query->where('status', $status)
            : $query;
    }

    /**
     * Scope a query to only include public products.
     * type 1 = public, type 0 = private.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool $type
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @BySwadi important
     */
    public function scopePublic($query, $type = 1)
    {
        return isset($type)
            ? $query->where('type', $type)
            : $query;
    }

    /**
     * Retrieve the products that are from active category.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfActiveCategory($query)
    {
        $categories_id = Category::where('status', 1)->pluck('id');

        return $query->whereIn('category_id', $categories_id);
    }

    /**
     * Get only approved product.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function scopeApproved($query)
    {
        return $query->where('is_approved', 1);
    }

    /**
     * Get active resource.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Get branch for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function getActionsAttribute()
    {
        $status = $this->status;
        if ($status == 1) {
            $span = '';
            if (Gate::check('create product_price') || Gate::check('update product_price')) {
                $span = '<button id="' . $this->id . '" name="create"  class="createPriceClick edit-table-row btn btn-primary btn-sm" data-toggle="edite" title="' . lang::get("provider.create") . ' ' . lang::get("provider.Product_pricing") . '"><i class="ft ft-plus"></i> ' . lang::get("provider.Product_pricing") . '</button>';
                $span .= '&nbsp';
                $span .= '<button id="' . $this->id . '" name="create"  class="createAddClick edit-table-row btn btn-primary btn-sm" data-toggle="edite" title="' . lang::get("provider.create") . ' ' . lang::get("provider.addition") . '"><i class="ft ft-plus"></i> ' . lang::get("provider.addition") . '</button>';
                $span .= '&nbsp';
            }
        } else {
            $span = '';
            if (Gate::check('create product_price') || Gate::check('update product_price')) {
                $span = '<button disabled id="' . $this->id . '" name="create"  class="createPriceClick edit-table-row btn btn-primary btn-sm" data-toggle="edite" title="' . lang::get("provider.create") . ' ' . lang::get("provider.Product_pricing") . '"><i class="ft ft-plus"></i> ' . lang::get("provider.Product_pricing") . '</button>';
                $span .= '&nbsp';
                $span .= '<button disabled id="' . $this->id . '" name="create"  class="createAddClick edit-table-row btn btn-primary btn-sm" data-toggle="edite" title="' . lang::get("provider.create") . ' ' . lang::get("provider.addition") . '"><i class="ft ft-plus"></i> ' . lang::get("provider.addition") . '</button>';
                $span .= '&nbsp';
            }
        }

        $span .= '<button id="' . $this->id . '" name="create"  class="edit-image btn btn-primary btn-sm" data-toggle="edit" title="' . lang::get("provider.edit") . ' ' . lang::get("provider.images") . '"><i class="ft ft-image"></i></button>';
        $span .= '&nbsp';

        $span .= '<span class="dropdown ">';
        $span .= '<button id="btnSearchDrop2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-primary btn-sm dropdown-toggle dropdown-menu-right"><i class="ft-settings"></i></button>';
        $span .= '<span aria-labelledby="btnSearchDrop2" class="dropdown-menu mt-1 dropdown-menu-right">';
        $span .= '<button type="button" name="show" id="' . $this->id . '" class="showB dropdown-item"><i class="la la-eye"></i>' . lang::get("provider.show") . '</button>';
        $span .= '';
        if (Gate::check('update product')) {
            $span .= '<button type="button" name="edit" id="' . $this->id . '" class="edit-row dropdown-item"><i class="la la-pencil"></i>' . lang::get("provider.edit") . '</button>';

        }
        $span .= '';
        if (Gate::check('delete product')) {
            $span .= '<button type="button" name="delete" class="deleteB dropdown-item" id="' . $this->id . '"><i class="la la-trash"></i>' . lang::get("provider.delete") . ' </button>';
        }
        return $span;
    }

    public function getCategoryNameAttribute()
    {
        return $this->category->name ?? null;
    }
}
