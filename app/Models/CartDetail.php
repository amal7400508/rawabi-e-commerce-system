<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use OwenIt\Auditing\Contracts\Auditable;

class CartDetail extends Pivot implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cart_details';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];


    /**
     * belongsTo product_price
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productPrice()
    {
        return $this->belongsTo(ProductPrice::class, 'product_price_id');
    }

    /**
     * belongsTo offer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }


    /**
     * belongsTo special_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function specialProduct()
    {
        return $this->belongsTo(SpecialProduct::class, 'special_product_id');
    }


    /**
     * belongsTo prescription_reply
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function prescriptionReply()
    {
        return $this->belongsTo(PrescriptionReply::class, 'prescription_reply_id');
    }

}
