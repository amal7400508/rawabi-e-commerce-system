<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;

class PrescriptionReplies extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;
//    use SoftDeletes;

    protected $fillable = ['is_alternative','prescription_id','medicine_name','medicine_price','note','currency'];

    public function prescription()
    {
        return $this->belongsTo(Prescription::class);
    }
}
