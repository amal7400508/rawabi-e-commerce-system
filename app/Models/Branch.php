<?php
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Models;


use App\Traits\WalletTrait;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
class Branch extends model implements TranslatableContract, Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Translatable;
    use Notifiable;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $translatedAttributes = ['name', 'address', 'sections', 'request_arrival_time'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'status',
        'translations',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = ['image','lat','long'];
    /**
     * Get active resource.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Get area for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
{
    return $this->belongsTo(Area::class);
}
    public function provider()
    {
        return $this->belongsTo(Provider::class,'provider_id');
    }

    /**
     * Get the user's wallet.
     */
    public function wallet()
    {
        return $this->morphMany(Wallet::class, 'walletable');
    }


    public function wallets()
    {
        return $this->hasMany(Wallet::class);
    }

    /**
     * Get provider for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */


    public function prescription()
    {
        return $this->hasMany(Prescription::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryProduct()
    {
        return $this->hasMany(CategoryProduct::class);
    }
    /**

     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offer()
    {
        return $this->hasMany(Offer::class);
    }
}
