<?php
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
class ProductPrice extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;
    use SoftDeletes;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    //protected $with = ['product'];

    protected $fillable = [
        'id','price','note','unit_id','color_id','product_id','image','currency', 'order_availability', 'note_ar', 'note_en',
    ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    public $appends = [
        'productName',
        'unitName',
        'color_name',
        'quantity',
        'old_price',
        'reorderable',
        'note',
//        'productLink',
//        'image_path',
//        'image_path_64',
//        'image_path_150',
//        'image_path_360',
//        'image_path_640',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
//        'product_id',
        'quantity',
        'unit',
//        'note',
        'created_at',
        'updated_at',
        'deleted_at',
        'product',
    ];

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo('App\Models\Unit');
    }

    public function color()
    {
        return $this->belongsTo('App\Models\Color');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    /**
     * Define a one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function productDiscount()
    {
        return $this->hasOne('App\Models\ProductDiscount')->active()
            ->where('product_price_id', $this->attributes['id']);
    }

    /**
     * Define a one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function productDiscountWithDeactivated()
    {
        return $this->hasOne('App\Models\ProductDiscount')
            ->where('product_price_id', $this->attributes['id']);
    }

    /**
     * Get Re-orderable attribute.
     *
     * @return bool
     */
    public function getReorderableAttribute()
    {
        if ((!is_null($this->productDiscountWithDeactivated))
            && (is_null($this->productDiscount))
        ) {
            return false;
        }

        // TODO: use states picot table
        return true;
        return $this->product->state_id === auth()->user()->state_id;
    }

    /**
     * Get price or discounted price attribute.
     *
     * @return int
     */
    public function getPriceAttribute()
    {
        $price = $this->attributes['price'];

        $product_discount = $this->productDiscount;

        if (!is_null($product_discount)) {
            $discount = $product_discount['value'];
            $price = ($product_discount['value_type'] == 'percentage')
                ? $price - (($discount / 100) * $price)
                : max(($price - $discount), 0);

            return (string) $price;
        }
        $this->attributes['price'] = $price;

        return $this->attributes['price'];
    }

    /**
     * Get null or original price if there is a discount attribute.
     *
     * @return int|string
     */
    public function getOldPriceAttribute()
    {
        $product_discount = $this->productDiscount;

        return
            !is_null($product_discount)
            ? $this->attributes['price']
            : '';
    }

    /**
     * Get product name of current product price.
     *
     * @return string|exception
     */
    public function getProductNameAttribute()
    {
        return $this->product->name;
    }

    /**
     * Get product unit name.
     *
     * @return string|exception
     */
    public function getUnitNameAttribute()
    {
        return $this->unit->name ?? null;
    }


    public function getColorNameAttribute()
    {
        return $this->color->name ?? null;
    }

    /**
     * Get product image link.
     *
     * @return string|exception
     */
    public function getProductLinkAttribute()
    {
        return domainAsset('storage/thumbnail/'.$this->product->image);
    }

    /**
     * Get product quantity or hide quantity attribute.
     *
     * @return int|exception
     */
    public function getQuantityAttribute()
    {
        if (isset($this->pivot)) {
            return $this->pivot->quantity + 0;
        }

        $this->addHidden(['quantity']);
    }

    /**
     * Get product image path.
     *
     * @return string
     */
    public function getImagePathAttribute()
    {
        return domainAsset('storage/product_price/'.$this->image);
    }

    /**
     * Get product 64 size image path.
     *
     * @return string
     */
    public function getImagePath64Attribute()
    {
        return domainAsset('storage/product_price/64/'.$this->image);
    }

    /**
     * Get product 150 size image path.
     *
     * @return string
     */
    public function getImagePath150Attribute()
    {
        return domainAsset('storage/product_price/150/'.$this->image);
    }

    /**
     * Get product 360 size image path.
     *
     * @return string
     */
    public function getImagePath360Attribute()
    {
        return domainAsset('storage/product_price/360/'.$this->image);
    }

    /**
     * Get product 640 size image path.
     *
     * @return string
     */
    public function getImagePath640Attribute()
    {
        return domainAsset('storage/product_price/640/'.$this->image);
    }

    /**
     * Scope a query to only include available products for order.
     * status 1 = available, status 0 = unavailable.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool                                  $status
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @BySwadi important
     */
    public function scopeOrderAvailability($query, $status = 1)
    {
        return isset($status)
            ? $query->where('order_availability', $status)
            : $query;
    }

    /**
     * get note lang
     *
     * @return string
     */
    protected function getNoteAttribute()
    {
        return app()->getLocale() == 'ar' ? $this->note_ar : $this->note_en;
    }
}
