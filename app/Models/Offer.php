<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class Offer extends Model implements TranslatableContract, Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Translatable;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are translated.
     *
     * @var array
     */
    public $translatedAttributes = ['name', 'image'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    public $with = ['type'];

    protected $fillable = ['is_approved'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    public $appends = [
        'currency_symbol',
        'branch_name',
        'offer_type_name',
        'quantity',
        'image_path',
        'actions',
        'actions_branches',
        'background_color_row',
        'background_color_row_branches',
        'start_date_carbon',
        'end_date_carbon'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'quantity',
    ];

    /**
     * Get branch for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(OfferType::class);
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cartDetail()
    {
        return $this->belongsToMany('App\Models\Cart', 'cart_details')->using('App\Models\CartDetail');
    }

//    public function offerDetail()
//    {
//        return $this->belongsToMany(OfferDetail::class);
//    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function offerProductPrice()
    {
        return $this->belongsToMany('App\Models\ProductPrice', 'offer_details')->using('App\Models\OfferDetail');
    }

    public function getCurrencySymbolAttribute()
    {
        return currency($this->currency);
    }

    /**
     * Get quantity of offer items.
     *
     * @return int
     */
    public function getQuantityAttribute()
    {
        return $this->pivot->quantity + 0;
    }

    /**
     * Get image path.
     *
     * @return string
     */
    public function getImagePathAttribute()
    {
        return asset('storage/offers/640/' . $this->image);
    }

    /**
     * Get only approved product.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function scopeApproved($query)
    {
        return $query->where('is_approved', 1);
    }

    /**
     * Get only approved product.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1)
            ->where('start_date', '<=', Carbon::now()->format('Y-m-d'))
            ->where('end_date', '>=', Carbon::now()->format('Y-m-d'));
    }

    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('delete offer')) {
            $actions .= '<a class="delete" id="' . $this->id . '"><i class="ft-trash-2 color-red"></i></a>';
        }
        $actions .= '<a class="show-detail-offer" id="' . $this->id . '"><i class="ft-eye color-primary" style="margin: auto 8px"></i></a>';
        if (Gate::check('update offer')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '"><i class="ft-edit color-primary"></i></a>';
        }
        return $actions;
    }

    public function getActionsBranchesAttribute()
    {
        if (Gate::check('update offer')) {
            if ($this->is_approved == 0 and !is_null($this->branch_id)) {
                return
                    '<div class="btn-group" role="group" aria-label="Basic example">' .
                    '<button type="button" class="show-detail-offer btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>' .
                    '<button type="button" class="reject btn btn-sm btn-outline-danger" id="' . $this->id . '">' . __('admin.reject') . '</button>' .
                    '<button type="button" class="accept btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.accept') . '</button>' .
                    '</div>';
            }
        }

        return '<button type="button" class="show-detail-offer btn btn-sm btn-outline-primary" id="' . $this->id . '">' . __('admin.detail') . '...</button>';
    }

    public function getStartDateCarbonAttribute()
    {
        return is_null($this->start_date) ? "" : Carbon::Parse($this->start_date)->format('Y-m-d');
    }

    public function getEndDateCarbonAttribute()
    {
        return is_null($this->end_date) ? "" : Carbon::Parse($this->end_date)->format('Y-m-d');
    }

    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }

    public function getBackgroundColorRowBranchesAttribute()
    {
        $is_approved = $this->is_approved;
        if ($is_approved == 1) return ''; //active
        else if ($is_approved == 0) return 'background-color: #ff041508'; //waiting
        else if ($is_approved == 3) return 'background-color: #0f0f0f24'; //canceled
    }

    public function getOfferTypeNameAttribute()
    {
        return $this->type->name;
    }

    public function getBranchNameAttribute()
    {
        return $this->branch->name;
    }
}
