<?php
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   ByAlsaloul <eng.alsaloul.mohammed@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
class Statistic extends Model
{
    use SoftDeletes;

    protected $with = ['statisticable'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'click_type',
        'number',
        'text',
        'statisticable_id',
        'statisticable_type',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = ['click'];

    /**
     * Get the parent statisticable.
     */
    public function statisticable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getClickAttribute()
    {
        $type = $this->click_type;
        if ($this->statisticable_type == 'App\Models\Product') {
            return '<a id="' . $this->statisticable_id . '" data-type="' . $this->click_type . '" class="show-detail"  style="color: #0d6fc6;">' . $type . '<a/>';
        } elseif ($this->statisticable_type == 'App\Models\Offer') {
            return '<a id="' . $this->statisticable_id . '" data-type="' . $this->click_type . '" class="show-detail-offer" style="color: #0d6fc6;">' . $type . '<a/>';
        } elseif ($this->statisticable_type == 'App\Models\Branch') {
            return '<a id="' . $this->statisticable_id . '" data-type="' . $this->click_type . '" class="show-detail-branch" style="color: #0d6fc6;">' . $type . '<a/>';
        } elseif ($this->statisticable_type == 'App\Models\Provider') {
            return '<a id="' . $this->statisticable_id . '" data-type="' . $this->click_type . '" class="show-detail-provider" style="color: #0d6fc6;">' . $type . '<a/>';
        } elseif ($this->statisticable_type == 'App\Models\Category') {
            return '<a id="' . $this->statisticable_id . '" data-type="' . $this->click_type . '" class="show-detail-category" style="color: #0d6fc6;">' . $type . '<a/>';
        } elseif ($this->statisticable_type == 'App\Models\MainCategory') {
            return '<a id="' . $this->statisticable_id . '" data-type="' . $this->click_type . '" class="" style="color: #0d6fc6;">' . $type . '<a/>';
        } elseif ($this->statisticable_type == 'App\Models\PaidAdvertisement') {
            return '<a id="' . $this->statisticable_id . '" data-type="' . $this->click_type . '" class="show-detail-paid-advertisement" style="color: #0d6fc6;">' . $type . '<a/>';
        } elseif ($this->statisticable_type == 'App\Models\Advertisement') {
            return '<a id="' . $this->statisticable_id . '" data-type="' . $this->click_type . '" class="show-detail-advertisement" style="color: #0d6fc6;">' . $type . '<a/>';
        }
        return $type;
    }
}
