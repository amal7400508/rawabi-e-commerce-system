<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Prescription extends Model
{
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_id',
        'user_id',
        'insurance_company_id',
        'insurance_company_number',
        'prescription_image',
        'note',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    public $appends = ['quantity', 'image_path', 'table_name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'created_at',
//        'updated_at',
        'deleted_at',
        'prescription_image',
        'quantity',
    ];

    /**
     * Get quantity.
     *
     * @return int|exception
     */
    public function getQuantityAttribute()
    {
        return $this->pivot->quantity + 0;
    }

    /**
     * Get image path.
     *
     * @return string
     */
    public function getImagePathAttribute()
    {
        return asset('storage/'.$this->prescription_image);
    }

    /**
     * Get This Table name.
     *
     * @return string
     */
    public function getTableNameAttribute()
    {
        // return $this->table();
        return $this->table;
    }

    /**
     * Define a polymorphic one-to-many relationship.
     * To use other notification table.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifications()
    {
        return $this->morphMany(\App\Models\SystemNotification::class, 'notifiable')
            ->orderBy('created_at', 'desc');
    }

    /**
     * Get branch for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * Get user for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get insurance company for current resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function insuranceCompany()
    {
        return $this->belongsTo(InsuranceCompany::class);
    }

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prescriptionReply()
    {
        return $this->hasMany(PrescriptionReply::class);
    }
}
