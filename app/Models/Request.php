<?php
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Models;

// ! important: This is the child class of RequestModel
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
class Request extends RequestModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    // ! put your code in parent class which represents the table

    protected $fillable = [
        'payment_type', 'receiving_type', 'address_id','branch_id','cart_id','booking_date'
    ];


    protected $hidden = [];

    protected $with=['requestItems','coupon','address','branch','requestItems.user'];

    protected $appends=[
        'payment_sum',
        'cache_payment',
        'user_request_number',
        'background_color_row'
    ];

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function requestItems()
    {
        return $this->belongsTo(Cart::class,"cart_id");
    }

    public function payment()
    {
        return $this->hasMany('App\BalanceTransaction');
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class,"coupon_id");
    }

    public function address()
    {
        return $this->belongsTo(UserAddress::class,"address_id");
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class,"branch_id");
    }
    public function cart()
    {
        return $this->belongsTo(Cart::class,"cart_id");
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class,"admin_id");
    }

    public function carrier()
    {
        return $this->belongsTo(Carrier::class,"carrier_id");
    }

    public function getPaymentSumAttribute()
    {
        return $this->payment()->sum('amount');
    }

    public function getCachePaymentAttribute()
    {
        if ($this->payment_type == 'fromWallet') {
            return 0.00;
        }
        $cart_id = $this->cart_id;
        $cart_detail = CartDetail::where('cart_id', $cart_id)->get();
        $price_sum = 0;
        $price__ =  0;
        $currency = [];
        foreach ($cart_detail as $cart_details) {
            $product_price = ProductPrice::find($cart_details->product_price_id);
            if ($cart_details->item_type == 'offer' || $cart_details->item_type == 'offer_app') {
                $offer = Offer::find($cart_details->offer_id);
                $price__ = $offer->price;
                $currency[] = $offer->currency;
            } elseif ($cart_details->item_type == 'product') {
                $price__ = $product_price->price ?? 0;
                $currency[] = $product_price->currency ?? 0;
            } elseif ($cart_details->item_type == 'prescription_reply') {
                $prescription_reply = PrescriptionReply::find($cart_details->prescription_reply_id);
                $price__ = $prescription_reply->medicine_price ?? 0;
                $currency[] = $prescription_reply->currency;
//                dd($price__);
            } elseif ($cart_details->item_type == 'special') {
                $SpecialProduct = SpecialProduct::find($cart_details->special_product_id);
                $price__ = $SpecialProduct->price ?? 0;
                $currency[] = $SpecialProduct->currency ?? null;
            }

            $price_sum = $price_sum + ($price__ * $cart_details->quantity);
        }

        return currency($currency[0]) . ' ' .$price_sum;
    }

    /**
     * The number of requests made by the user
     *
     * @return int
     */
    public function getUserRequestNumberAttribute()
    {
        $request_count = Request::join('carts', 'requests.cart_id', '=', 'carts.id')
            ->where('carts.user_id', $this->cart->user_id)
            ->where(function ($q) {
                $q->where('requests.status', 'on_branch')
                    ->orWhere('requests.status', 'delivered');
            })
            ->count();

        return $request_count;
    }

    /**
     * Returns the log color according to the status
     *
     * @return string color hexadecimal
     */
    public function getBackgroundColorRowAttribute()
    {
        $cart_id = $this->cart_id;
        $cart = CartDetail::where('cart_id', $cart_id)->first();
        if ($cart->item_type == 'hospitality') {
            return 'background-color: #f3eae6;';
        }
        $status = $this->status;
        if ($status == 'requested')
            return 'background-color: #ff001108;';
        else if ($status == 'canceled')
            return 'background-color: #00000008;';
        else if ($status == 'reviewed')
            return 'background-color: #00c6ff08;';
        else if ($status == 'repair')
            return 'background-color: #ff4d0008;';
        else if ($status == 'deliver')
            return 'background-color: #0024ff08;';
        else if ($status == 'delivered')
            return 'background-color: #6bff0008;';
        else if ($status == 'done')
            return 'background-color: #6bff0008;';
        else if ($status == 'on_the_way')
            return 'background-color: #6bff0008;';
        else if ($status == 'on_branch')
            return 'background-color: #6bff0008;';
        else if ($status == 'received')
            return 'background-color: #00c6ff08;';
        else if ($status == 'completedRequest')
            return 'background-color: #0024ff08;';
        else
            return 'background-color: #fffff00;';
    }
}
