<?php

namespace App\Models;

use App\Traits\StateableTraits;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use OwenIt\Auditing\Contracts\Auditable;

class Advertisement extends Model implements TranslatableContract, Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Translatable;
    use Notifiable;
    use SoftDeletes;
    use StateableTraits;

    public $translatedAttributes = ['image', 'desc'];

    protected $hidden = [
        'end_date',
        'translations',
        "created_at",
        "updated_at",
        "deleted_at",
    ];

    protected $appends = ['actions', 'background_color_row'];

    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
//        if (Gate::check('delete advertisement')) {
            $actions .= '<a class="delete" id="' . $this->id . '"><i class="ft-trash-2 color-red"></i></a>';
//        }
        $actions .= '<a class="show_advertisement" id="' . $this->id . '"><i class="ft-eye color-primary" style="margin: auto 8px"></i></a>';
//        if (Gate::check('update advertisement')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '"><i class="ft-edit color-primary"></i></a>';
//        }

        return $actions;
    }


    public function getBackgroundColorRowAttribute()
    {
        return $this->end_date >= Carbon::now()->format('Y-m-d') ? '' : 'background-color: #ff041508;';
    }
}
