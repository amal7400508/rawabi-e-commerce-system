<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class UnitTypeTranslation extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $hidden = ['id', 'translations'];
    public $timestamps = false;
}
