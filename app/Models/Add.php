<?php
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
class Add extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    /**
     * The attributes that are translated.
     *
     * @var array
     */
//    public $translatedAttributes = ['add','type'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'product_id',
        'type_ar',
        'type_en',
        'add_ar',
        'add_en',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
//    protected $appends = ['active'];

    /**
     * Get last message.
     *
     * @return bool
     */
    /*public function getActiveAttribute()
    {
        $is_current_date = Carbon::now() >= $this->start_date
            && (
                Carbon::now() <= $this->end_date
                || Carbon::parse($this->end_date)->isToday()
            );

        $is_active = $this->is_active ? true : false;

        return $is_current_date and $is_active;
    }*/

    /**
     * Get last message.
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    /*public static function last()
    {
        return self::latest()->first();
    }*/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
