<?php
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;

// ! important: This is the parent class of Request and RequestAuditable
/**
 * Models
 * php version 7.3.1.
 *
 * @category Apis
 *
 * @author   BySwadi <muath.ye@gmail.com>
 * @license  IC https://www.infinitecloud.co
 *
 * @link     Moka_Sweets https://www.mokasweets.com/
 */
class RequestModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'request_number',
        'payment_type',
        'address_id',
        'branch_id',
        'cart_id',
        'note',
        'delivery_price_id',
        'cache_payment',
        'coupon_id',
        'volume_discount_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'is_active', 'note', 'deleted_at',
        //"updated_at",
        'branch_id', 'admin_id', 'carrier_id', 'address_id', 'coupon_id',
        'laravel_through_key', 'status',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'requestItems',
        'coupon',
        'address',
        'addressWithTrashed',
        'branch',
        'requestItems.user',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['payment_sum', 'deliveryPrice'];

    /**
     * Get active requests.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function requestItems()
    {
        return $this->belongsTo('App\Models\Cart', 'cart_id');
    }

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payment()
    {
        return $this->hasMany('App\Models\BalanceTransaction');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon', 'coupon_id');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo('App\Models\UserAddress', 'address_id');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function addressWithTrashed()
    {
        return $this->belongsTo('App\Models\UserAddress', 'address_id')->withTrashed();
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'admin_id');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Admin', 'updated_by');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carrier()
    {
        return $this->belongsTo('App\Models\Carrier', 'carrier_id');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cart()
    {
        return $this->belongsTo('App\Models\Cart', 'cart_id');
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deliveryPriceings()
    {
        return $this->belongsTo('App\Models\DeliveryPrice', 'delivery_price_id');
    }

    /**
     * Get total payment.
     *
     * @return float|exception
     */
    public function getPaymentSumAttribute()
    {
        return $this->payment()->sum('amount');
    }

    /**
     * Get delivery price.
     *
     * @return float|exception
     */
    public function getDeliveryPriceAttribute()
    {
        return $this->deliveryPriceings()->sum('price');
    }

    //BySwadi: search for delivered and updated_at

    /**
     * Get 'delivered at' requests.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query   .
     * @param \Illuminate\Http\Request              $request .
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfDeliveredAt($query, $request)
    {
        if (!empty($request)) {
            $searchFields = ['updated_at'];
            $searchWildcard = '%'.$request->updated_at.'%';
            // return  $query->where($searchFields,'like', $searchWildcard);
            return $query->where($searchFields, $request);
            // return  $query->whereDate('updated_at', '=', Carbon::today()->toDateString());
        }

        // if(isset($request->search))
        // return $query->where('name','like', '$request->search');
        return $query;
    }

    /**
     * Define a polymorphic one-to-many relationship.
     * To use other notification table.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifications()
    {
        return $this->morphMany(\App\Models\SystemNotification::class, 'notifiable')
            ->orderBy('created_at', 'desc');
    }

    /**
     * To Get rating status.
     *
     * @return bool
     */
    public function rated()
    {
        $rates = Rate::where('request_id', $this->id)->first();

        return $rates == null ? 0 : 1;
    }
}
