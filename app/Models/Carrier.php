<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;

class Carrier extends Authenticatable implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasApiTokens;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $appends = [
        'image_path',
        'background_color_row',
        'class_color_row',
        'actions'
    ];

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function request()
    {
        return $this->hasMany(Request::class)
            ->whereIn(
                'status',
                [
                    'requested',
                    'repair',
                    'deliver',
                    'delivered',
                    'ready',
                    'canceled',
                    'received',
                    'on_branch',
                    'on_the_way',
                    'completedRequest',
                ]
            );
    }

    /**
     * Define a one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function wallet()
    {
        return $this->hasOne(UserWallet::class);
    }

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pointHistory()
    {
        return $this->hasMany('App\Models\PointHistory');
        // return $this->hasMany('App\Models\Request')->using('App\Models\UserProductFavorite');
    }


    /**
     * This function used in
     * Advertisement model to get image path by
     * public $appends = ['image_path'].
     *
     * @return URL
     */
    public function getImagePathAttribute()
    {
        if (is_null($this->image))
            return asset('storage' . '/default-carrier.jpg');

        return asset('storage/carriers' . '/' . $this->image);
    }

    /**
     * The buttons in datatable
     */
    public function getActionsAttribute()
    {
        $actions = '';
        if (Gate::check('delete carriers')) {
            $actions = '<a class="delete" id="' . $this->id . '"><i class="ft-trash-2 color-red"></i></a>';
        }
        $actions .= '<a class="show-detail" id="' . $this->id . '"><i class="ft-eye color-primary" style="margin: auto 8px"></i></a>';
        if (Gate::check('update carriers')) {
            $actions .= '<a class="edit-table-row" id="' . $this->id . '"><i class="ft-edit color-primary"></i></a>';
        }
        return $actions;
    }


    public function getBackgroundColorRowAttribute()
    {
        return $this->status != 1 ? 'background-color: #ff041508;' : '';
    }

    public function getClassColorRowAttribute()
    {
        return $this->status != 1 ? 'tr-color-red' : '';
    }
}
