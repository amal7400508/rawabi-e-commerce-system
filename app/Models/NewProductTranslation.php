<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewProductTranslation extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $hidden = ['id'];

    public $timestamps=false;
}
