<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\BalanceTransaction;
use App\Models\Branch;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Matrix\Exception;

class ReceiptController extends Controller
{
    public $view_path = 'managements.accounts.receipt.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = BalanceTransaction::whereIn('operation_type_id', [6, 7])
            ->where(function ($q) {
                $q->where('balance_transactionable_type', '!=', Account::class)
                    ->orWhere('balance_transactionable_id', '!=', 3);
            });

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', $query);
            } else if ($search_type == 'name') {
                $data = $data->where('name', 'like', '%' . $query . '%');
            } else if ($search_type == 'phone') {
                $data = $data->where('phone', 'like', '%' . $query . '%');
            }
        endif;

        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')
            ->with('accounts', Account::where('id', '!=', 3)->get())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('managements.accounts.receipt.create')
            ->with('accounts', Account::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'operation_type' => 'required|in:receipt,catch_receipt',
            'type' => 'required|in:account,branch',
            'account' => ['required', 'integer', function ($attribute, $value, $fail) use ($request) {
                if ($request->type == 'account' and $value == 2 and $request->currency != 'ريال يمني') {
                    $fail('لايوجد حساب ' . $request->currency . ' في حساب التوصيل.');
                }
            }],
            'amount' => 'required|numeric|min:0|max:9999999999999999999.99',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['error' => $validatedData->errors()], 401);
        }

        $data = DB::transaction(function () use ($request) {
            $arithmeticOperation = $this->arithmeticOperation($request, $request->currency);
            $data = $arithmeticOperation['data'];
            if (!$arithmeticOperation['status']) {
                return response()->json(['error' => $data], 401);
            }

            return $data;
        });

        return response()->json([
            'status' => 200,
            'data' => $data,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);

    }

    public function arithmeticOperation($request, $currency = 'ريال يمني')
    {
        try {
            //الصندوق الرئيسي
            $main_box_account = Account::find(3);
            $main_box_account_wallet = $main_box_account->wallet->where('currency', $currency)->first();
            $main_box_amount = $main_box_account_wallet->current_balance;


            if ($request['type'] == 'account') {
                $type = Account::class;
                $account = Account::find($request['account']);

            } else {
                $type = Branch::class;
                $account = Branch::find($request['account']);
            }

            $account_wallet = $account->wallet->where('currency', $currency)->first();;
            $wallet_amount = $account_wallet->current_balance;

            // سند صرف
            if ($request['operation_type'] == 'receipt') {
                // مدين
                //الصندوق الرئيسي
                BalanceTransaction::create([
                    'amount' => $request['amount'],
                    'currency' => $currency,
                    'type' => 'add',
                    'wallet_id' => $main_box_account_wallet->id,
                    'operation_type_id' => 7,
                    'balance_transactionable_id' => 3,
                    'balance_transactionable_type' => Account::class,
                    'transactionable_id' => $request['account'],
                    'transactionable_type' => $type,
                ]);

                Wallet::where('id', $main_box_account_wallet->id)->update([
                    'current_balance' => $main_box_amount + $request['amount']
                ]);

                // دائن
                $data = BalanceTransaction::create([
                    'amount' => $request['amount'],
                    'currency' => $currency,
                    'type' => 'subtract',
                    'wallet_id' => $account_wallet->id,
                    'operation_type_id' => 7,
                    'balance_transactionable_id' => $request['account'],
                    'balance_transactionable_type' => $type,
                    'transactionable_id' => 3,
                    'transactionable_type' => Account::class,
                ]);

                Wallet::where('id', $account_wallet->id)->update([
                    'current_balance' => $wallet_amount - $request['amount']
                ]);
            } else {
                // سند قبض
                // دائن
                //الصندوق الرئيسي
                BalanceTransaction::create([
                    'amount' => $request['amount'],
                    'currency' => $currency,
                    'type' => 'subtract',
                    'wallet_id' => $main_box_account_wallet->id,
                    'operation_type_id' => 6,
                    'balance_transactionable_id' => 3,
                    'balance_transactionable_type' => Account::class,
                    'transactionable_id' => $request['account'],
                    'transactionable_type' => $type,
                ]);

                Wallet::where('id', $main_box_account_wallet->id)->update([
                    'current_balance' => $main_box_amount - $request['amount']
                ]);

                // مدين
                $data = BalanceTransaction::create([
                    'amount' => $request['amount'],
                    'currency' => $currency,
                    'type' => 'add',
                    'wallet_id' => $account_wallet->id,
                    'operation_type_id' => 6,
                    'balance_transactionable_id' => $request['account'],
                    'balance_transactionable_type' => $type,
                    'transactionable_id' => 3,
                    'transactionable_type' => Account::class,
                ]);

                Wallet::where('id', $account_wallet->id)->update([
                    'current_balance' => $wallet_amount + $request['amount']
                ]);
            }

            return [
                'status' => true,
                'data' => $data
            ];
        } catch (Exception $e) {

            return [
                'status' => false,
                'data' => $e
            ];
        }
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*$id_account = $id - 1;

        $balance_transaction = BalanceTransaction::find($id);
        if ($balance_transaction->operation_type_id == 6 or
            $balance_transaction->operation_type_id == 7) {
            $amount = $balance_transaction->amount;
            $currency = $balance_transaction->currency;
            $wallet_id = $balance_transaction->wallet_id;

            if ()
            Wallet::where('id', $wallet_id->id)->update([
                'current_balance' => $main_box_amount + $request['amount']
            ]);
        }



        abort(403);
        dd($id);*/
    }

    public function walletFilter($type)
    {
        if ($type == 'branch') {
            return Branch::active()->get();
        }

        return Account::where('id', '!=', 3)->get();
    }
}
