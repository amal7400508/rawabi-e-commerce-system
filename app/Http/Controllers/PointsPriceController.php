<?php

namespace App\Http\Controllers;

use App\PointPrice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class PointsPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $dataselect = DB::table('points_price')
                ->whereNull('points_price.deleted_at')
                ->select('points_price.*')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $span = '<span class="dropdown">';
                    $span .= '<button id="btnSearchDrop2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings"></i></button>';
                    $span .= '<span aria-labelledby="btnSearchDrop2" class="dropdown-menu mt-1 dropdown-menu-right">';
                    $span .= '<button type="button" name="show" id="' . $data->id . '" class="showB dropdown-item"><i class="la la-eye"></i>' . lang::get("admin.show") . '</button>';
                    $span .= '';
                    if (Gate::check('update point_price')) {
                        $span .= '<a href="/point_price/edit/' . $data->id . '"   class="dropdown-item"><i class="la la-pencil"></i>' . lang::get("admin.edit") . '</a>';
                    }
                    return $span;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $dataselect_count = DB::table('points_price')
            ->whereNull('points_price.deleted_at')
            ->select('points_price.*')
            ->get()->count();
        return view('point_price.index')->with('dataselect_count', $dataselect_count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dataselect_count = DB::table('points_price')
            ->whereNull('points_price.deleted_at')
            ->select('points_price.*')
            ->get()->count();
        if ($dataselect_count < 1) {
            return view('point_price.create');
        } else {
            return redirect('/point_price');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $dataselect_count = DB::table('points_price')->get()->count();
        if ($dataselect_count > 1) {
            return redirect('/points_price');
        }
        $this->validate($request, [
//            'price' => 'required|string|max:191',
            'price' => 'required|numeric|max:99999999.99|min:0',

        ]);
        $point_price = new PointPrice();
        $point_price->price = $request->price;
        $point_price->save();

        return redirect('/point_price')->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $points_price = DB::table('points_price')
            ->whereNull('points_price.deleted_at')
            ->where('points_price.id', '=', $id)
            ->select('points_price.*')
            ->get()->first();

        return response()->json([
            'id' => $points_price->id,
            'price' => $points_price->price,
            'created_at' => Carbon::Parse($points_price->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($points_price->updated_at)->format('Y/m/d'),

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $points_price = PointPrice::find($id);
        return view('point_price.edit', array('points_price' => $points_price));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'price' => 'required|numeric|max:99999999.99|min:0',
        ]);
        $points_price = PointPrice::find($id);
        $points_price->price = $request->price;
        $points_price->save();
        return redirect('/point_price')->with('success', Lang::get('admin.edited_successfully'));
    }
}
