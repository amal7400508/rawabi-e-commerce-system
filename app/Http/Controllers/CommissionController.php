<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommissionResource;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Commission;
use App\Models\InsuranceCompany;
use App\Models\MainCategory;
use App\Models\MainCategoryCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class CommissionController extends Controller
{
    public $view_path = 'managements.encoding.commissions.';

    public function index()
    {
        return view($this->view_path . 'index')
            ->with('categories', MainCategory::whereDoesntHave('sub')->get())
            ->with('branches', Branch::get());
    }

    public function get($branch_id)
    {
        $data = Commission::where('branch_id', $branch_id)->get();
        return CommissionResource::collection($data);
    }

    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);
//        dd($request->all());

        $type = $request->type;

        $commission = new Commission();
        $commission->type = $type;
        $commission->branch_id = $request->branch_id;
        if ($type == 'product') {
            $commission->main_category_id = $request->category;
            $commission->category_id = $request->sub_category;
        }
        $commission->commission = $request->commission;
        $commission->save();

        return response()->json([
            'success' => Lang::get('admin.added_successfully'),
            'branch_id' => $request->branch_id
        ]);
    }

    public function changeLevel(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'commission' => 'required|int|max:100',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        Commission::where('id', $id)->update(['commission' => $request->commission]);

        return response()->json([
            'status' => 200,
            'data' => Commission::find($id),
            'title' => Lang::get('admin.edited_successfully'),
            'message' => ''
        ]);
    }


    public function validatedData($request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'branch_id' => [
                'required',
                'integer',
                'exists:branches,id',
                function ($attribute, $value, $fail) use ($request) {
                    $branch = Branch::active()->where('id', $value)->count();
                    if ($branch != 1) {
                        $fail(lang::get("error") . '.');
                    }
                }, function ($attribute, $value, $fail) use ($request) {
                    $commission = Commission::where('branch_id', $value)
                        ->where('type', $request->type)
                        ->where('main_category_id', $request->category)
                        ->where('category_id', $request->sub_category)
                        ->count();
                    if ($commission != 0) {
                        $fail(lang::get("validation.this_row_already_exists") . '.');
                    }
                }],
            'type' => 'required|in:others,product',
            'category' => [
                'nullable',
                'integer',
                'exists:main_categories,id',
                function ($attribute, $value, $fail) {
                    $category_is_leaf = MainCategory::find($value)->is_leaf;
                    if ($category_is_leaf) {
                        $fail(lang::get("validation.a_childless_a_category_should_be_chosen"));
                    }
                }, /*function ($attribute, $value, $fail) use ($request) {
                    $commission = Commission::where('main_category_id', $value)
                        ->where('type', $request->type)
                        ->where('branch_id', $request->branch_id)
                        ->where('category_id', $request->sub_category)
                        ->count();
                    if ($commission != 0) {
                        $fail(lang::get("validation.this_row_already_exists") . '.');
                    }
                }*/],
            'commission' => ['required', 'numeric', 'min:0', 'max:99'],
            'sub_category' => ['nullable', 'integer', 'exists:categories,id']
        ]);

        return $validatedData;
    }

    public function destroy($id)
    {
        $commission = Commission::find($id);
        $insurance_company_id = $commission->branch_id;
        $commission->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'insurance_company_id' => $insurance_company_id
        ]);
    }

    public function getSubCategories($main_Category_id)
    {
        $category_ids = MainCategoryCategory::where('main_category_id', $main_Category_id)
            ->pluck('category_id')->toArray();

        $categories = Category::whereIn('id', $category_ids)->get();
        return response()->json($categories);
    }
}
