<?php

namespace App\Http\Controllers;

use App\Exports\OfferExport;
use App\Exports\RequestExport;
use App\Http\Resources\FalterBranchCategoryResources;
use App\Http\Resources\FalterProductResource;
use App\Http\Resources\OfferDetailResource;
use App\Http\Resources\ProductPriceResource;

use App\Models\Branch;
use App\Models\CartDetail;
use App\Models\Category;
use App\Models\MainCategoryCategory;
use App\Models\Offer;
use App\Models\OfferDetail;
use App\Models\OfferType;
use App\Models\PharmacyCategory;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Notifications\Admin\ProviderNotification;
use App\Utilities\Helpers\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;


class OfferController extends Controller
{
    public $view_path = 'managements.app.offers.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = Offer::orderBy('id', 'desc');
        $view_name = 'index';

        return $this->displayOffers($data, $request, $view_name)
            ->with('branches', Branch::where('status', 1)->get())
            ->with('category', Category::where('status', 1)->get())
            ->with('offer_types', OfferType::where('status', 1)->get());
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function offersBranches(Request $request)
    {
        $data = Offer::whereNotNull('branch_id')
            ->orderBy('is_approved')
            ->orderBy('id', 'desc');
        $view_name = 'offers_branches';

        return $this->displayOffers($data, $request, $view_name);
    }

    public function displayOffers($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'name') {
                $data = $data->whereTranslationLike('name', '%' . $query . '%');
            } else if ($search_type == 'old_price') {
                $data = $data->where('old_price', 'like', '%' . $query . '%');
            } else if ($search_type == 'new_price') {
                $data = $data->where('price', 'like', '%' . $query . '%');
            } else if ($search_type == 'type') {
                $data = $data->whereHas('type', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
                $data = $data->whereTranslationLike('content', '%' . $query . '%');
            } else if ($search_type == 'branch') {
                $data = $data->whereHas('branch', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            }
        endif;

        $data_count = $data->count();

        return view($this->view_path . $view_name)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'branch_id' => 'required|string|max:191',
            'name_ar' => 'required|string|max:191',
            'name_en' => 'required|string|max:191',
            'old_price' => 'required|numeric|max:99999999.99',
            'new_price' => 'required|numeric|max:99999999.99',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date|after:start_date|max:191',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'currency' => 'required|in:ريال يمني,ريال سعودي,دولار أمريكي',
            'offer_type' => 'required|integer',
            'product_price_id.*' => 'required|integer',
            'product_price_id' => 'required|array',
            'quantity.*' => 'required|string|max:191',
            'quantity' => 'required|array',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $offer = null;
        DB::transaction(function () use ($request, &$offer) {
            $image_name = null;
            if (!is_null($request->file('image'))) {
                $image_name = Image::reSizeImage($request->image, 'storage/offers/', 'offers_');
//                $image_name = uploadImage($request->image, 'offers');
            }

            $offer = new Offer();
            $offer->is_approved = 1;
            $offer->branch_id = $request->branch_id;
            $offer->old_price = $request->old_price;
            $offer->price = $request->new_price;
            $offer->start_date = $request->start_date;
            $offer->end_date = $request->end_date;
            $offer->currency = $request->currency;
            $offer->type_id = $request->offer_type;
            $offer->status = $request->input('status', 0);
            $offer->save();

            $offer->translateOrNew('ar')->name = $request->name_ar;
            $offer->translateOrNew('ar')->image = $image_name;

            $offer->translateOrNew('en')->name = $request->name_en;
            $offer->translateOrNew('en')->image = $image_name;
            $offer->save();

            for ($i = 0; $i < count($request->product_price_id); $i++) {
                $offer_detail = new OfferDetail();
                $offer_detail->product_price_id = $request->product_price_id[$i];
                $offer_detail->quantity = $request->quantity[$i];
                $offer_detail->note = $request->note[$i];
                $offer_detail->offer_id = $offer->id;
                $offer_detail->save();
            }
        });

        return response()->json([
            'status' => 200,
            'offer' => Offer::find($offer->id),// $offer,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteDetailStore(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'offer_id' => 'required|integer',
            'product_price_id' => 'required|integer',
            'quantity' => 'required|numeric',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $offer = new OfferDetail();
        $offer->offer_id = $request->offer_id;
        $offer->product_price_id = $request->product_price_id;
        $offer->quantity = $request->quantity;
        $offer->note = $request->note;
        $offer->save();

        return response()->json([
            'status' => 200,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $offer = Offer::find($id);

        $offerOfferDetail = OfferDetail::where('offer_id', $offer->id)->get();
        $offer_details = OfferDetailResource::collection($offerOfferDetail);
//        dd($offer_details);

        return response()->json([
            'id' => $offer->id,
            'old_price' => $offer->old_price,
            'price' => $offer->price,
            'status' => $offer->status,
            'start_date' => $offer->start_date,
            'end_date' => $offer->end_date,
            'offer_name' => $offer->offer_name,
            'image' => $offer->image_path,
            'name_type' => $offer->type->name,
            'type_id' => $offer->type_id,
            'currency' => currency($offer->currency),
            'created_at' => is_null($offer->created_at) ? "" : Carbon::Parse($offer->created_at)->format('Y-m-d'),
            'updated_at' => is_null($offer->updated_at) ? "" : Carbon::Parse($offer->updated_at)->format('Y-m-d'),
            'offer_details' => $offer_details,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offer = Offer::find($id);
        $offer_ar = $offer->translate('ar');
        $offer_en = $offer->translate('en');

        $offerOfferDetail = OfferDetail::where('offer_id', $offer->id)->get();
        $offer_details = OfferDetailResource::collection($offerOfferDetail);

        return response()->json([
            'id' => $offer->id,
            'name_ar' => $offer_ar->name ?? null,
            'name_en' => $offer_en->name ?? null,
            'old_price' => $offer->old_price,
            'new_price' => $offer->price,
            'status' => $offer->status,
            'start_date' => $offer->start_date,
            'end_date' => $offer->end_date,
            'image' => $offer->image_path,
            'branch_id' => $offer->branch_id,
            'name_type' => $offer->type->name,
            'currency' => $offer->currency,
            'type_id' => $offer->type_id,
            'created_at' => is_null($offer->created_at) ? "" : Carbon::Parse($offer->created_at)->format('Y-m-d'),
            'updated_at' => is_null($offer->updated_at) ? "" : Carbon::Parse($offer->updated_at)->format('Y-m-d'),
            'offer_details' => $offer_details,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'branch_id' => 'required|string|max:191',
            'name_ar' => 'required|string|max:191',
            'name_en' => 'required|string|max:191',
            'old_price' => 'required|numeric|max:99999999.99',
            'new_price' => 'required|numeric|max:99999999.99',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date|after:start_date|max:191',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'currency' => 'required|in:ريال يمني,ريال سعودي,دولار أمريكي',
            'offer_type' => 'required|integer',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);


        $offer = Offer::find($id);

        $image_name = $offer->image;
        if ($request->hasFile('image')) {
            $file_path = 'storage/offers';
            $image_name = Image::reSizeImage($request->file('image'), $file_path, 'offers_');
            $folders_name = ['/640/', '/360/', '/150/', '/64/'];
            for ($i = 0; $i < count($folders_name); $i++) {
                $filename = public_path() . '/' . $file_path . $folders_name[$i] . $offer->image;
                File::delete($filename);
            }
        }

        $offer->is_approved = 1;
        $offer->branch_id = $request->branch_id;
        $offer->old_price = $request->old_price;
        $offer->price = $request->new_price;
        $offer->start_date = $request->start_date;
        $offer->end_date = $request->end_date;
        $offer->currency = $request->currency;
        $offer->type_id = $request->offer_type;
        $offer->status = $request->input('status', 0);
        $offer->save();

        $offer->translateOrNew('ar')->name = $request->name_ar;
        $offer->translateOrNew('ar')->image = $image_name;

        $offer->translateOrNew('en')->name = $request->name_en;
        $offer->translateOrNew('en')->image = $image_name;
        $offer->save();

        return response()->json([
            'status' => 200,
            'offer' => $offer,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart_details = CartDetail::where('offer_id', $id)->count();
        if ($cart_details > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }

        $offer = Offer::find($id);
        $file_path = 'storage/offers';
        $folders_name = ['/640/', '/360/', '/150/', '/64/'];
        for ($i = 0; $i < count($folders_name); $i++) {
            $filename = public_path() . '/' . $file_path . $folders_name[$i] . $offer->image;
            File::delete($filename);
        }
        $offer->delete();

        return response()->json([
            'message' => Lang::get('admin.deleted_successfully'),
            'data_count' => Offer::count()
        ],
            200
        );
    }

    public function deleteDetailOffer($id)
    {
        $cart_detail = OfferDetail::find($id);
        $cart_details = OfferDetail::where('offer_id', $cart_detail->offer_id)->count();
        if ($cart_details <= 1) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }

        $cart_detail->delete();

        return response()->json([
            'message' => Lang::get('admin.deleted_successfully'),
            'data_count' => Offer::count()
        ],
            200
        );
    }

    public function falterProduct($id)
    {
        $product = Product::where('category_id', $id)
           /* ->approved()->available()*/->get();
        $data = FalterProductResource::collection($product);

        return response()->json($data);
    }

    public function falterProductDiscount($id,$branch_id)
    {
        $product = Product::where('branch_id', $branch_id)
            ->where('category_id', $id)
            ->approved()->available()->get();
        $data = FalterProductResource::collection($product);

        return response()->json($data);
    }

    public function falterProductRequest($id)
    {
        $product = Product::where('branch_id', $id)
            ->approved()->available()->get();
        $data = FalterProductResource::collection($product);

        return response()->json($data);
    }

    public function getProductPrice($product_id)
    {
        $product_price = ProductPrice::where('product_id', $product_id)
            ->where('order_availability', 1)
            ->get();

        $data = ProductPriceResource::collection($product_price);
        return response()->json($data);
    }

    public function falterPrice($id)
    {
        $data = ProductPrice::where('product_id', $id)
            ->where('order_availability', 1)
            ->get();

        $data = ProductPriceResource::collection($data);

        return response()->json([
            'data' => $data
        ]);
    }


    public function accept(Request $request)
    {
        $id = $request->items_id;
        $offer = Offer::find($id);
        if ($offer->is_approved != 0 or is_null($offer->branch_id)) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($offer, $id) {
            $offer->update(['is_approved' => 1]);
            responseNotify($id, Offer::class, ProviderNotification::class);
            $offer->notify(new ProviderNotification());
        });

        return response()->json([
            'status' => 200,
            'data' => Offer::find($id),
            'title' => Lang::get('admin.accepted_successfully'),
            'message' => ''
        ]);
    }

    public function reject(Request $request)
    {
        $id = $request->items_id;
        $offer = Offer::find($id);
        if ($offer->is_approved != 0 or is_null($offer->branch_id)) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($offer, $id) {
            $offer->update(['is_approved' => 3]);
            responseNotify($id, Offer::class, ProviderNotification::class);
            $offer->notify(new ProviderNotification());
        });


        return response()->json([
            'status' => 200,
            'data' => Offer::find($id),
            'title' => Lang::get('admin.rejected_successfully'),
            'message' => ''
        ]);
    }

    public function falterMainCategory($id)
    {
        $branch_category = PharmacyCategory::where('branch_id', $id)->pluck('main_category')->toArray();
        $main_category_categories = Category::whereIn('main_category_id', $branch_category)->get();
        $data = FalterBranchCategoryResources::collection($main_category_categories);

        return response()->json($data);
    }
}
