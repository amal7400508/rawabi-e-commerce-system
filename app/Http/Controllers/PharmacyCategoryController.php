<?php

namespace App\Http\Controllers;

use App\Http\Resources\PharmacyCategoriesResource;
use App\Models\Branch;
use App\Models\Category;
use App\Models\InsuranceCompany;
use App\Models\MainCategory;
use App\Models\MainCategoryCategory;
use App\Models\PharmacyCategory;
use App\Models\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class PharmacyCategoryController extends Controller
{
    public $view_path = 'managements.encoding.pharmacy_categories.';

    public function index()
    {
        return view($this->view_path . 'index')
            ->with('categories', MainCategory::/*whereDoesntHave('sub')->*/get())
            ->with('insurance_companies', InsuranceCompany::get())
            ->with('pharmacies', Branch::get());
    }

    public function get($pharmacy_id)
    {
        $data = PharmacyCategory::where('branch_id', $pharmacy_id)->get();
        return PharmacyCategoriesResource::collection($data);
    }

    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $pharmacy_category = new PharmacyCategory();
//        $pharmacy_category->insurance_company_id = $request->insurance_company;
        $pharmacy_category->branch_id = $request->pharmacy;
        $pharmacy_category->main_category = $request->category;
//        $pharmacy_category->commission = $request->commission;
        $pharmacy_category->save();

        return response()->json([
            'success' => Lang::get('admin.added_successfully'),
            'pharmacies_id' => $request->pharmacy
        ]);
    }

    public function validatedData($request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'insurance_company' => [
                'nullable',
                'integer',
                'exists:insurance_companies,id'
            ],
            'pharmacy' => [
                'required',
                'integer',
                'exists:branches,id',
                function ($attribute, $value, $fail) use ($request) {
                    $branch = Branch::active()->where('id', $value)->count();
                    if ($branch != 1) {
                        $fail(lang::get("error") . '.');
                    }
                }],
            'category' => [
                'required',
                'integer',
                'exists:main_categories,id',
               /* function ($attribute, $value, $fail) {
                    $category_is_leaf = MainCategory::find($value)->is_leaf;
                    if ($category_is_leaf) {
                        $fail(lang::get("validation.a_childless_a_category_should_be_chosen"));
                    }
                },*/
               function ($attribute, $value, $fail) use ($request) {
                    $pharmacy_category = PharmacyCategory::where('main_category', $value)
                        ->where('insurance_company_id', $request->insurance_company)
                        ->where('branch_id', $request->pharmacy)
//                        ->where('category_id', $request->sub_category)
//                        ->whereNull('category_id')
                        ->count();
                    if ($pharmacy_category != 0) {
                        $fail(lang::get("validation.this_row_already_exists") . '.');
                    }
                }],
//            'commission' => ['required', 'numeric', 'min:0', 'max:99'],
//            'sub_category' => ['nullable', 'integer', 'exists:categories,id']
        ]);

        return $validatedData;
    }

    public function destroy($id)
    {
        $pharmacy_category = PharmacyCategory::find($id);
        $insurance_company_id = $pharmacy_category->branch_id;
        $pharmacy_category->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'insurance_company_id' => $insurance_company_id
        ]);
    }

    public function getSubCategories($main_Category_id)
    {
        $category_ids = MainCategoryCategory::where('main_category_id', $main_Category_id)
            ->pluck('category_id')->toArray();

        $categories = Category::whereIn('id', $category_ids)->get();
        return response()->json($categories);
    }
}
