<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use App\Models\CouponType;
use App\Notifications\Admin\ProviderNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CouponController extends Controller
{
    public $view_path = 'managements.discounts.coupons.';

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = new Coupon();
        $data = $data->whereNull('branch_id')
            ->orderBy('id', 'desc');

        $coupon_types = CouponType::where('status', 1)->whereNull('branch_id')->get();

        return $this->display($data, $request, 'index')
            ->with('coupon_types', $coupon_types);

    }

    public function couponsBranches(Request $request)
    {
        $data = new Coupon();
        $data = $data->whereNotNull('branch_id')
            ->orderBy('is_approved')
            ->orderBy('id', 'desc');

        $coupon_types = CouponType::where('status', 1)->whereNull('branch_id')->get();

        return $this->display($data, $request, 'coupons_branches')
            ->with('coupon_types', $coupon_types);
    }

    public function display($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'coupon') {
                $data = $data->where('number', 'like', '%' . $query . '%');
            } else if ($search_type == 'price') {
                $data = $data->where('price', $query);
            } else if ($search_type == 'coupon_type') {
                $data = $data->whereHas('couponType', function ($q) use ($query) {
                    $q->where('name', 'like', '%' . $query . '%');
                });
            }
        endif;

        $data_count = $data->count();
        $coupon_types = CouponType::where('status', 1)->whereNull('branch_id')->get();

        return view($this->view_path . $view_name)
            ->with('coupon_types', $coupon_types)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $coupon = new Coupon();
        $coupon->coupon_types_id = $request->coupon_type;
        $coupon->number = $request->number;
        $coupon->value_type = $request->value_type;
        $coupon->price = $request->price;
        $coupon->start_date = $request->start_date;
        $coupon->end_date = $request->end_date;
        $coupon->status = $request->input('status', 0);
        $coupon->is_approved = 1;
        $coupon->save();

        return response()->json([
            'status' => 200,
            'coupon' => $coupon,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request)
    {
        return Validator::make(
            $request->all(), [
            'coupon_type' => 'required|integer',
            'number' => [
                'required',
                'string',
                'max:191',
                Rule::unique('coupons')->where(function ($query) use ($request) {
                    return $query->where('number', $request->number)
                        ->where('branch_id', $request->branch_id);
                }),
            ],
            'value_type' => 'required|string|in:price,percentage',
            'price' => ['required', 'numeric', function ($attribute, $value, $fail) use ($request) {
                if ($request->value_type == 'percentage') {
                    if ($value > 100)
                        $fail(lang::get("validation.max.numeric") . ' 100.');
                    if ($value < 0)
                        $fail(lang::get("validation.min.numeric") . ' 0.');
                } else {
                    if ($value > 1000000)
                        $fail(lang::get("validation.max.numeric") . ' 1000000');
                }
            }],
            'start_date' => 'required|date|after:yesterday',
            'end_date' => 'nullable|date|after_or_equal:start_date',
        ], [
            'after' => __('validation.date_most_be_at_least_today'),
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $coupon = Coupon::find($id);

        return response()->json([
            'id' => $coupon->id,
            'branch_name' => $coupon->branch->name ?? null,
            'coupon_type' => $coupon->coupon_type_name,
            'number' => $coupon->number,
            'price' => $coupon->price,
            'status' => $coupon->status,
            'start_date' => $coupon->start_date,
            'end_date' => $coupon->end_date,
            'price_with_type' => $coupon->price_with_type,
            'created_at' => is_null($coupon->created_at) ? "" : Carbon::Parse($coupon->created_at)->format('Y-m-d'),
            'updated_at' => is_null($coupon->updated_at) ? "" : Carbon::Parse($coupon->updated_at)->format('Y-m-d'),

        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'status' => 'required|in:active,attitude',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $coupon = Coupon::find($id);
        $coupon->update([
            'status' => ($request->status == 'active') ? 1 : 0
        ]);


        return response()->json([
            'id' => $id,
            'status' => $coupon->status,
            'background_color_row' => $coupon->background_color_row,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = Request::where('coupon_id', $id)->count();
        if ($request > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }
        Coupon::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Coupon::count()
        ],
            200
        );
    }

    public function accept(Request $request)
    {
        $id = $request->items_id;
        $coupon = Coupon::find($id);
        if ($coupon->is_approved != 0 or is_null($coupon->branch_id)) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($coupon, $id) {
            $coupon->update(['is_approved' => 1]);
            responseNotify($id, Coupon::class, ProviderNotification::class);
            $coupon->notify(new ProviderNotification());
        });

        return response()->json([
            'status' => 200,
            'data' => Coupon::find($id),
            'title' => Lang::get('admin.accepted_successfully'),
            'message' => ''
        ]);
    }

    public function reject(Request $request)
    {
        $id = $request->items_id;
        $coupon = Coupon::find($id);
        if ($coupon->is_approved != 0 or is_null($coupon->branch_id)) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($coupon, $id) {
            $coupon->update(['is_approved' => 3]);
            responseNotify($id, Coupon::class, ProviderNotification::class);
            $coupon->notify(new ProviderNotification());
        });

        return response()->json([
            'status' => 200,
            'data' => Coupon::find($id),
            'title' => Lang::get('admin.rejected_successfully'),
            'message' => ''
        ]);
    }
}
