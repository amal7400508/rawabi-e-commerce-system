<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Encryption\DecryptException;
use \Illuminate\Support\Facades\Crypt as Key;

class RouteKeyController extends Controller
{
    /**
     * @param $e
     * @return false|string
     */
    static function compress($e)
    {

//        $pass = "d3l7H4fdsTsd782Y";
//        $iv = "7049498215454651";
//        $method = "AES-256-CBC";

        try {
            $pass = str_replace(
                "AAAA8PYBZGM:",
                "",
                Key::decryptString(
                    "eyJpdiI6I" .
                    env('RK_PASS')
                )
            );

            $iv = str_replace(
                "IV:",
                "",
                Key::decryptString(
                    "eyJpdiI6I" .
                    env('RK_IV')
                )
            );

            $method = str_replace(
                "method:",
                "",
                Key::decryptString(
                    "eyJpdiI6I" .
                    env('RK_METHOD')
                )
            );


            $ed = base64_decode($e);

            $n = openssl_decrypt(
                "$ed",
                $method, $pass,
                0,
                $iv
            );

            return $n;

        } catch (DecryptException $exception) {
            return "error " . $exception;
        }
    }
}
