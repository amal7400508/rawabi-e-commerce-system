<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\MultiDelivery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class MultiDeliveryController extends Controller
{
    public $view_path = 'managements.carrier_management.multi_delivery.';

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
//        $de = count(Area::all());
//        for ($i = 1; $i <= $de; $i++) {
//            for ($j = $i; $j <= $de ; $j++){
//                $multi_delivery = new DeliveryPrice();
//                $multi_delivery->area_id_from = $i;
//                $multi_delivery->area_id_to = $j;
//                $multi_delivery->price = 600;
//                $multi_delivery->status = 1;
//                $multi_delivery->save();
//            }
//        }
//        dd('done.........');

        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new MultiDelivery();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'price') {
                $data = $data->where('price', 'like', '%' . $query . '%');
            } else if ($search_type == 'branches_number') {
                $data = $data->where('branches_number', 'like', '%' . $query . '%');
            }
        endif;

        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();

//        dd($data->get()[0]->area_from_to);
        return view($this->view_path . 'index')
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request, true);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $multi_delivery = new MultiDelivery();
        $multi_delivery->branches_number = $request->branches_number;
        $multi_delivery->price = $request->price;
        $multi_delivery->save();

        return response()->json([
            'status' => 200,
            'area' => $multi_delivery,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request)
    {
        return Validator::make(
            $request->all(), [
            'branches_number' => 'required|integer|unique:multi_delivery,branches_number',
            'price' => 'required|numeric|max:10000000000000000000.00',
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $multi_delivery = MultiDelivery::find($id);
        return response()->json([
            'id' => $multi_delivery->id,
            'branches_number' => $multi_delivery->branches_number,
            'price' => $multi_delivery->price,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $multi_delivery = MultiDelivery::find($id);
        $multi_delivery->branches_number = $request->branches_number;
        $multi_delivery->price = $request->price;
        $multi_delivery->save();

        return response()->json([
            'status' => 200,
            'area' => $multi_delivery,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MultiDelivery::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => MultiDelivery::count()
        ],
            200
        );
    }
}
