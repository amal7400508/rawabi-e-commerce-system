<?php

namespace App\Http\Controllers;

use App\PointPrice;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class PointHistorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (PointPrice::count() < 1) {
            return redirect('point_price')->with('error', Lang::get('admin.enter_point_price'));
        }
        $points_price = PointPrice::query()->first();
        if (request()->ajax()) {
            $balance_transactions = DB::table('balance_transactions')
                ->select('user_wallet_id', DB::raw('sum(amount) as points_number'))
                ->where('type', '=', 'add_from_point')
                ->groupBy('user_wallet_id');

            $balance = DB::table('user_wallets')
                ->joinSub($balance_transactions, 'balance_transactions', function ($join) {
                    $join->on('user_wallets.id', '=', 'balance_transactions.user_wallet_id');
                })
                ->join('users', 'user_wallets.user_id', '=', 'users.id')
                ->select('user_wallets.id', 'points_number', 'users.name as user_name', 'users.phone as user_phone', 'user_wallets.updated_at')
                ->get();
            return datatables()->of($balance)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="showenter" id="' . $data->id . '"  class="showB showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i>  ' . Lang::get("admin.show") . '</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('point_histories.index')->with('points_price', $points_price->price);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $points_price = PointPrice::query()->first();

        $balance_transactions = DB::table('balance_transactions')
            ->select('user_wallet_id', DB::raw('sum(amount) as points_number'))
            ->where('type', '=', 'add_from_point')
            ->where('user_wallet_id', '=', $id)
            ->groupBy('user_wallet_id');

        $balance = DB::table('user_wallets')
            ->joinSub($balance_transactions, 'balance_transactions', function ($join) {
                $join->on('user_wallets.id', '=', 'balance_transactions.user_wallet_id');
            })
            ->join('users', 'user_wallets.user_id', '=', 'users.id')
            ->select('user_wallets.id', 'points_number', 'users.name as user_name', 'users.phone as user_phone')
            ->get()->first();
        return response()->json([
            'id' => $balance->id,
            'user_name' => $balance->user_name,
            'phone' => $balance->user_phone,
            'points_number' => $balance->points_number / $points_price->price,
        ]);
    }
}
