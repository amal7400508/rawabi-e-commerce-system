<?php

namespace App\Http\Controllers;


use App\Exports\UserExport;
use App\Exports\UserMostRequest;
use App\Models\Branch;
use App\Models\Cart;
use App\Models\User;
use App\Models\Disposit;
use App\Utilities\Helpers\Fcm;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    public $view_path = 'managements.app.users.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
//        dd(Auth::user()->roles[0]->permissions->count());
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $users = new User();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $users = $users->where('id', '=', $query);
            } else if ($search_type == 'name') {
                $users = $users->where('full_name', 'like', '%' . $query . '%');
            } else if ($search_type == 'phone') {
                $users = $users->where('phone', 'like', '%' . $query . '%');
            }
        endif;

        $users = $users->orderBy('updated_at', 'desc');
        $users_count = $users->count();

        return view($this->view_path . 'index')
            ->with('data', $users->paginate($table_length))
            ->with('data_count', $users_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    public function deposit(Request $request, $id)
    {
        $user = User::find($id);
        $wallet = $user->wallet;


        $validatedData = $this->validatedDeposit($request, $wallet);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        // add Deposit
        $user->addToBalance($request->amount, $wallet->id);

        $name = '<span class="color-info">' . $user->full_name . '</span>';
        $user_current_balance = User::find($id)->wallet->current_balance;
        $amount = '<span class="font-default color-info" >' . $request->amount . ' YER' . '</span>';
        $current_balance = '<span class="font-default color-info">' . $user_current_balance . ' YER' . '</span>';
        $m = '<br><small><a style="color: #1E9FF2" href="' . route('deposits') . '">اضغط هنا للانتقال الى سجل الايداعات</a></small>';

        $message_ar = 'لقد تم اضافة ' . $amount . ' الى حساب ' . $name . ' رصيد المستخدم الحالي ' . $current_balance . '.' . $m;
        $message_en = 'It has been added ' . $amount . 'to account ' . $name . 'Current user balance ' . $current_balance . '.';

        // fcm firbase notify
        $fcm_title = __('admin.deposit');
        $fcm_body_ar = 'عزيزي: ' . $user->full_name . ' تم إيداع مبلغ ' . $request->amount . ' ريال في حسابك، رصيدك الحالي ' . $user_current_balance . ' ريال ';
        $fcm_body_en = 'My dear: ' . $user->full_name . 'Your account has been deposited ' . $request->amount . ' RY, your current balance is ' . $user_current_balance . ' RY';

        $fcm_body = app()->getLocale() == 'ar'
            ? $fcm_body_ar
            : $fcm_body_en;

        Fcm::send_MKA_notification($fcm_title, $fcm_body, $user->id);
        // end

        return response()->json([
            'status' => 200,
            'title' => Lang::get('admin.the_deposit_was_successful'),
            'message' => app()->getLocale() == 'ar' ? $message_ar : $message_en,
        ]);
    }

    public function validatedDeposit($request, $wallet)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'amount' => [
                'required', 'numeric', 'max:1000000000000000000',
                function ($attribute, $value, $fail) use ($wallet) {
                    if (is_null($wallet)) {
                        $fail(lang::get("validation.sorry_there_is_no_account_for_this_user"));
                    }
                    if ($value % 5 != 0) {
                        $fail(lang::get("validation.amount_incorrect"));
                    }
                    $max_amount = 1000000000000000000;
                    if (($wallet->current_balance + $value) > $max_amount) {
                        $fail(lang::get(
                                "validation.Sorry_the_current_user_balance_cannot_be_more_than") . ' : ' . $max_amount
                        );
                    }
                }]
        ]);

        return $validatedData;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return response()->json([
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'gender' => $user->gender,
            'image' => $user->image,
            'status' => $user->status,
            'birth_date' => Carbon::Parse($user->birth_date)->format('Y/m/d'),
            'address' => $user->address,
            'updated_at' => Carbon::Parse($user->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($user->created_at)->format('Y/m/d'),
            'opening_balance' => $user->wallet->openning_balance ?? 0.00,
            'current_balance' => $user->wallet->current_balance ?? 0.00
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'status' => 'required|in:1,2,3,0',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $user = User::find($id);
        $user->status = $request->status;
        $user->save();

        return response()->json([
            'status' => 200,
            'data' => $user,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    public function report(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = $this->getDataReport($request);
        $data = $data->orderBy('users.id', 'desc');

        $data_count = $data->count();

        $most_data = $this->getMostRequest($request);
        $most_data_count = $data->count();

        return view($this->view_path . '.report')
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('most_data', $most_data->get())
            ->with('most_data_count', $most_data_count)

            ->with('pagination_links', [
                    'table_length' => isset($_GET['table_length']) ? $_GET['table_length'] : '',
                ]
            );
    }

    protected function getDataReport($request)
    {
        $user = new User();
        if (!is_null($request->from)) {
            $user = $user->whereDate('created_at', '>=', $request->from);
        }
        if (!is_null($request->to)) {
            $user = $user->whereDate('created_at', '<=', $request->to);
        }
        if (!is_null($request->name)) {
            $user = $user->where('full_name', 'like', '%' . $request->name . '%');
        }
        if (!is_null($request->email)) {
            $user = $user->where('email', 'like', '%' . $request->email . '%');
        }
        if (!is_null($request->phone)) {
            $user = $user->where('phone', 'like', '%' . $request->phone . '%');
        }
        if (!is_null($request->status)) {
            $user = $user->where('status', $request->status);
        }
        if (!is_null($request->gender)) {
            $user = $user->where('gender', $request->gender);
        }

        return $user;
    }

    public function reportExport(Request $request)
    {
        $data = $this->getDataReport($request);
        $data = $data->orderBy('users.id', 'desc');
        if ($request->report == 1) {
            $data = $this->getMostRequest($request);
        }

        return view($this->view_path . '.report_export')
            ->with('data', $data->get());
    }

    public function export(Request $request)
    {
        if ($request->report == 1) {
            $data = $this->getMostRequest($request)->get();
            return Excel::download(
                new UserMostRequest($data),
                'most-request-users-' . now() . '.xlsx'
            );
        }

        $data = $this->getDataReport($request)->get();
        return Excel::download(new UserExport($data), 'users-' . now() . '.xlsx');
    }

    public function mostRequest(Request $request)
    {
        $data = $this->getMostRequest($request);

        if (request()->ajax()) {
            return datatables()->of($data)
                ->addColumn('action', function (User $data) {
                    $button = '<button type="button" name="showenter" id="' . $data->id . '"  class="showdetail showenter btn btn-primary btn-sm" data-toggle="show" title="' . Lang::get("admin.show") . '"><i class="ft ft-eye"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return response()->json([]);
    }

    public function getMostRequest($request)
    {

        $number_users = isset($request->number_user) ? $request->number_user : 3;
        if ($number_users == '') $number_users = 3;

        $cart = Cart::join('requests', 'carts.id', 'requests.cart_id')
            ->orWhere('requests.status', '!=', 'canceled');

        if (!is_null($request->from_2)) {
            $cart = $cart->whereDate('requests.created_at', '>=', $request->from_2);
        }
        if (!is_null($request->to_2)) {
            $cart = $cart->whereDate('requests.created_at', '<=', $request->to_2);
        }
        $cart = $cart->select('carts.user_id', DB::raw('count(carts.user_id) as requests_number'))
            ->groupBy('carts.user_id');

        $data = User::joinSub($cart, 'cart', function ($join) {
            $join->on('users.id', '=', 'cart.user_id');
        });
        if (!is_null($request->number)) {
            $data = $data->where('requests_number', $request->comparison_operations, $request->number);
        }
        $data = $data->orderBy('requests_number', 'Desc')
            ->limit($number_users);

        return $data;
    }

    public function exportMostRequest(Request $request)
    {
        $data = $this->getMostRequest($request)->get();
        return Excel::download(
            new UserMostRequest($data),
            'most-request-users-' . now() . '.xlsx'
        );
    }


}
