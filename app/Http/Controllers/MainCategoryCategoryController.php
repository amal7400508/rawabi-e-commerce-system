<?php

namespace App\Http\Controllers;

use App\Http\Resources\MainCategoryCategoryResource;
use App\Models\Category;
use App\Models\MainCategory;
use App\Models\MainCategoryCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class MainCategoryCategoryController extends Controller
{
    public $view_path = 'managements.encoding.main_category_categories.';

    public function index()
    {
        return view($this->view_path . 'index')
            ->with('main_categories', MainCategory::whereDoesntHave('sub')->get())
            ->with('sub_categories', Category::get())
            ;
    }


    public function getCategories($main_category_id)
    {
        $data = MainCategoryCategory::where('main_category_id', $main_category_id)->get();
        $data_resource = MainCategoryCategoryResource::collection($data);

        return response()->json([
            'data' => $data_resource,
        ]);
    }

    public function store(Request $request)
    {

        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $category_product = new MainCategoryCategory();
        $category_product->main_category_id = $request->main_category;
        $category_product->category_id = $request->sub_category;
        $category_product->save();

        return response()->json([
            'success' => Lang::get('admin.added_successfully'),
            'category_id' => $request->main_category
        ]);
    }

    public function validatedData($request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'main_category' => [
                'required',
                'integer',
                function ($attribute, $value, $fail) {
                    $category_is_leaf = MainCategory::find($value)->is_leaf;
                    if ($category_is_leaf){
                        $fail(lang::get("validation.a_childless_a_category_should_be_chosen"));
                    }
                }],
            'sub_category' => [
                'required',
                'integer',
                function ($attribute, $value, $fail) use ($request) {
                    $sub_categories = MainCategoryCategory::where('category_id', $value)
                        ->where('main_category_id', $request->main_category)
                        ->count();
                    if ($sub_categories != 0) {
                        $fail(lang::get("validation.this_sub_category_is_already_in_this_main_category") . '.');
                    }
                }]
        ]);

        return $validatedData;
    }

    public function destroy($id)
    {
        $data = MainCategoryCategory::find($id);
        $category_id = $data->main_category_id;
        $data->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'category_id' => $category_id
        ]);
    }
}
