<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UserEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('user_events')
                ->join('users', 'user_events.user_id', '=', 'users.id')
                ->join('event_type_translations', 'user_events.event_type_id', '=', 'event_type_translations.event_type_id')
                ->where('event_type_translations.locale', '=', $locale)
                ->select('user_events.*', 'users.name as user_name', 'event_type_translations.name as event_type_translations_name')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="showenter" id="' . $data->id . '"  class="showB showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('managements.app.user_event.index');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $locale = app()->getLocale();
        $user_event = DB::table('user_events')
            ->join('users', 'user_events.user_id', '=', 'users.id')
            ->join('event_type_translations', 'user_events.event_type_id', '=', 'event_type_translations.event_type_id')
            ->where('event_type_translations.locale', '=', $locale)
            ->where('user_events.id', '=', $id)
            ->select('user_events.*', 'event_type_translations.name as event_type_translations_name', 'users.name as user_name')
            ->get()->first();

        return response()->json([
            'id' => $user_event->id,
            'name' => $user_event->name,
            'date' => $user_event->date,
            'user_name' => $user_event->user_name,
            'event_type_translations_name' => $user_event->event_type_translations_name,
            'updated_at' => Carbon::Parse($user_event->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($user_event->created_at)->format('Y/m/d'),
        ]);

    }
}
