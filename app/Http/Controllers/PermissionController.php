<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$role = Role::where('name', 'admin_administrator')->first();
        $role->givePermissionTo(Permission::where('guard_name', 'admin')->get());*/

        if (request()->ajax()) {
            $data_select = Role::where('name', '!=', 'admin_administrator')->where('guard_name', 'admin')->get();
            return datatables()->of($data_select)
                ->addColumn('action', function ($data) {
                    $span = '<span class="dropdown">';
                    $span .= '<button id="btnSearchDrop2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings"></i></button>';
                    $span .= '<span aria-labelledby="btnSearchDrop2" class="dropdown-menu mt-1 dropdown-menu-right">';
                    $span .= '<a href="' . url('admins_management/permission') . '/show/' . $data->id . '" class=" dropdown-item"><i class="la la-eye"></i>' . lang::get("admin.show") . '</a>';
                    $span .= '';
                    if (Gate::check('update permission')) {
                        $span .= '<a href="' . url('admins_management/permission') . '/edit/' . $data->id . '"   class="dropdown-item"><i class="la la-pencil"></i>' . lang::get("admin.edit") . '</a>';
                    }
                    $span .= '';
                    if (Gate::check('delete permission')) {
                        $span .= '<button type="button" name="delete" class="deleteB dropdown-item" id="' . $data->id . '"><i class="la la-trash"></i>' . lang::get("admin.delete") . '</button></span></span>';
                    }
                    return $span;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('managements.admins_management.permission.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Role::all();
        return view('managements.admins_management.permission.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'role' => 'required|string|max:191|min:2|unique:roles,name',
        ]);
        $role = Role::create([
            'name' => $request->role,
            'guard_name' => 'admin'
        ]);
        $role->syncPermissions($request->permission);
        return redirect()->route('permission')->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $permission = $role->getPermissionNames();
        return view('managements.admins_management.permission.show')
            ->with('permission', $permission)
            ->with('role', $role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::where('name', '!=', 'admin_administrator')->find($id);
        $permission = $role->getPermissionNames();
        return view('managements.admins_management.permission.edit')
            ->with('permission', $permission)
            ->with('role', $role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $role = Role::find($id);
        $role = Role::where('name', '!=', 'admin_administrator')->find($id);

        if ($role->name != $request->role) {
            $this->validate($request, [
                'role' => 'required|string|max:191|min:2|unique:roles,name',
            ]);
        }
        $this->validate($request, [
            'role' => 'required|string|max:191|min:2',
        ]);
        $role->syncPermissions($request->permission);
        $role->name = $request->role;
        $role->save();
        return redirect()->route('permission')->with('success', Lang::get('admin.edited_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role_count = DB::table('model_has_roles')->where('role_id', $id)->get()->count();
        if ($role_count > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }

        $role = Role::where('name', '!=', 'admin_administrator')->find($id);
        $role->delete();
        return response()->json([
            'success' => 'success'
        ]);
    }
}
