<?php

namespace App\Http\Controllers;

use App\Http\Resources\AppNotifyResource;
use App\Http\Resources\SysNotifyResource;
use App\Models\Alert;
use App\Models\Branch;
use App\Models\Coupon;
use App\Models\Deposit;
use App\Models\NotificationMovement;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Provider;
use App\Models\Request;
use Illuminate\Http\Request as RequestHttp;
use App\Models\SystemNotification;
use App\Models\VolumeDiscount;
use App\Notifications\Admin\ProviderNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;

class SystemNotificationController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $sys_notify = $this->getNotify()->get();
        return response()->json(SysNotifyResource::collection($sys_notify));
    }

    public function count()
    {
        $notify_count = $this->getNotify()->count();
        $notify_app_count = $this->getNotifyApp()->count();

        return response()->json([
            'sys_notify' => $notify_count,
            'app_notify' => $notify_app_count
        ]);
    }

    public function notifyApp()
    {
        $sys_notify = $this->getNotifyApp()->get();
//        return response()->json($sys_notify);
        return response()->json(AppNotifyResource::collection($sys_notify));
    }

    public function getNotify()
    {
        $sys_notify = SystemNotification::where(function ($q) {
            if (Gate::check('show products')) {
                $q->where('notifiable_type', Product::class);
            }
            if (Gate::check('show offer')) {
                $q->orWhere('notifiable_type', Offer::class);
            }
            if (Gate::check('show attentions')) {
                $q->orWhere('notifiable_type', Alert::class);
            }
            if (Gate::check('show branches')) {
                $q->orWhere('notifiable_type', Branch::class);
            }
            if (Gate::check('show coupons')) {
                $q->orWhere('notifiable_type', Coupon::class);
            }
            if (Gate::check('show volume_discounts')) {
                $q->orWhere('notifiable_type', VolumeDiscount::class);
            }
            if (Gate::check('show notification_movement')) {
                $q->orWhere('notifiable_type', NotificationMovement::class);
            }
            if (Gate::check('show deposits')) {
                $q->orWhere('notifiable_type', Deposit::class);
            }
            if (Gate::check('show provider')) {
                $q->orWhere('notifiable_type', Provider::class);
            }
        })
            ->where('type', '!=', ProviderNotification::class)
            ->whereNull('response_at')
            ->with('walletable')
            ->whereHasMorph('walletable', [
                Product::class,
                Offer::class,
                Alert::class,
                Branch::class,
                Coupon::class,
                VolumeDiscount::class,
                NotificationMovement::class,
                Deposit::class,
                Provider::class
            ])
            ->orderby('created_at', 'desc');
        return $sys_notify;
    }

    public function getNotifyApp()
    {
        $sys_notify = SystemNotification::where(function ($q) {
            $q->where('notifiable_type', 'App\V1\Models\Request')//                ->orWhere('notifiable_type', Deposit::class)
            ->orWhere('notifiable_type', 'App\V1\Models\SpecialProduct')
                ->orWhere('notifiable_type', 'App\V1\Models\Prescription');
        })
            ->where('type', '!=', ProviderNotification::class)
            ->whereNull('response_at')
            ->orderby('created_at', 'desc');
        return $sys_notify;
    }

    public function responseAt(RequestHttp $request)
    {

        SystemNotification::where('notifiable_type', $request->notifiable_type)
            ->where('type', '!=', ProviderNotification::class)
            ->where('notifiable_id', $request->notifiable_id)
            ->update([
                'response_at' => Carbon::now()->toDateTime(),
            ]);
    }
}
