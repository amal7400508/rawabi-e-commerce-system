<?php

namespace App\Http\Controllers;

use App\Models\Rate;
use App\Models\RateDetail;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * @var string
     */
    public $view_path = 'managements.customer_reviews.rates.';

    /**
     * Display providers data.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = Rate::orderBy('id', 'desc');
        return $this->display($data, $request, 'index');
    }

    public function display($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'request_number') {
                $data = $data->whereHas('request', function ($q) use ($query) {
                    $q->where('request_number', $query);
                });
            } else if ($search_type == 'user_name') {
                $data = $data->whereHas('user', function ($q) use ($query) {
                    $q->where('full_name', 'like', '%' . $query . '%');
                });
            } else if ($search_type == 'phone') {
                $data = $data->whereHas('user', function ($q) use ($query) {
                    $q->where('phone', 'like', '%' . $query . '%');
                });
            } else if ($search_type == 'carrier') {
                $data = $data->whereHas('carrier', function ($q) use ($query) {
                    $q->where('name', 'like', '%' . $query . '%');
                });
            } else if ($search_type == 'branch') {
                $data = $data->whereHas('branch', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            } else if ($search_type == 'note') {
                $data = $data->where('note', 'like', '%' . $query . '%');
            }
        endif;

        $data_count = $data->count();

        return view($this->view_path . $view_name)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * @param int $rate_type_id
     * @param int $rate_id
     * @return int
     */
    public function getRateTypeAndRateId($rate_type_id, $rate_id)
    {
        if (isset(RateDetail::where('rate_type_id', $rate_type_id)->where('rate_id', $rate_id)->get()->first()->rate)) {
            $rate_details = RateDetail::where('rate_type_id', $rate_type_id)
                ->where('rate_id', '=', $rate_id)->get()->first()->rate;
            return $rate_details;
        }

        return 0;
    }
}
