<?php

namespace App\Http\Controllers;

use App\Models\EventType;
use App\Models\EventTypeTranslation;
use App\Models\UserEvent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class Event_typeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('event_type_translations')
                ->join('event_types', 'event_type_translations.event_type_id', '=', 'event_types.id')
                ->where('event_type_translations.locale', '=', $locale)
                ->whereNull('event_types.deleted_at')
                ->select('event_type_translations.*', 'event_types.updated_at')
                ->orderBy('event_types.created_at')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    if (Gate::check('create event_type')) {
                        $button = '<button type="button" name="edit" id="' . $data->event_type_id . '" class="edit-table-row edit btn btn-primary btn-sm" data-toggle="edite" title="edite"><i class="ft ft-edit"></i></button>';
                        $button .= '&nbsp';
                    } else {
                        $button = '';
                    }
                    $button .= '<button type="button" name="showenter" id="' . $data->id . '"  class="showdetail showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    if (Gate::check('delete event_type')) {
                        $button .= '&nbsp';
                        $button .= '<button type="button" name="delete" id="' . $data->event_type_id . '" class="delete btn btn-danger btn-sm" data-toggle="delete" title="delete"><i class="ft ft-trash-2"></button>';
                    }
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('managements.app.event_type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event_type = EventTypeTranslation::where('id', $id)->first();
        $event_types = EventType::find($event_type->event_type_id);
        $btnShow = '<button  class="btn btn-primary col-md-6 btn-sm" data-toggle="Edit" title="Edit"><i class="ft ft-edit"></i></button>';
        $btnShow .= '<button  class="delete btn btn-danger col-md-6 btn-sm" id="' . $event_type->event_type_id . '" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></i></button>';
        return response()->json([
            'id' => $event_type->event_type_id,
            'name' => $event_type->name,
            'notification_title' => $event_type->notification_title,
            'notification_message' => $event_type->notification_message,
            'created_at_e' => $event_types->created_at,
            'updated_at_e' => $event_types->updated_at,
            'btn' => $btnShow,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // show 404 page if id not found
        EventType::findOrFail($id);
        $event_type_ar = EventTypeTranslation::where('event_type_id', '=', $id)->where('locale', 'ar')->first();
        $event_type_en = EventTypeTranslation::where('event_type_id', '=', $id)->where('locale', 'en')->first();
        return response()->json([
            'edit_name_ar' => $event_type_ar->name,
            'edit_id' => $event_type_ar->event_type_id,
            'edit_name_en' => $event_type_en->name,
            'edit_notification_title_ar' => $event_type_ar->notification_title,
            'edit_notification_title_en' => $event_type_en->notification_title,
            'edit_notification_message_ar' => $event_type_ar->notification_message,
            'edit_notification_message_en' => $event_type_en->notification_message,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'ar_name' => 'required|string|max:191',
            'en_name' => 'required|string|max:191',
            'ar_notification_title' => 'required|string|max:191',
            'en_notification_title' => 'required|string|max:191',
            'ar_notification_message' => 'required|string|max:191',
            'en_notification_message' => 'required|string|max:191',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)
                ->withInput(
                    [
                        'method' => 'update',
                        'id' => $id,
                        'ar_name' => $request->ar_name,
                        'en_name' => $request->en_name,
                        'ar_notification_title' => $request->ar_notification_title,
                        'en_notification_title' => $request->en_notification_title,
                        'ar_notification_message' => $request->ar_notification_message,
                        'en_notification_message' => $request->en_notification_message
                    ]
                );
        }
        $event_type = EventType::find($id);
        $event_type->save();

        $event_type->translateOrNew('ar')->name = $request->ar_name;
        $event_type->translateOrNew('ar')->notification_title = $request->ar_notification_title;
        $event_type->translateOrNew('ar')->notification_message = $request->ar_notification_message;

        $event_type->translateOrNew('en')->name = $request->en_name;
        $event_type->translateOrNew('en')->notification_title = $request->en_notification_title;
        $event_type->translateOrNew('en')->notification_message = $request->en_notification_message;

        $event_type->save();
        //  return redirect()->back()->with('success', Lang::get('admin.edited_successfully') .'تمت الحذف بنجاح');
        return redirect()->back()->with('success', Lang::get('admin.edited_successfully'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ar_name' => 'required|string|max:191|unique:event_type_translations,name',
            'en_name' => 'required|string|max:191|unique:event_type_translations,name',
            'ar_notification_title' => 'required|string|max:191',
            'en_notification_title' => 'required|string|max:191',
            'ar_notification_message' => 'required|string|max:191',
            'en_notification_message' => 'required|string|max:191',
        ]);

        $event_type = new EventType();
        $event_type->save();

        $event_type->translateOrNew('ar')->name = $request->ar_name;
        $event_type->translateOrNew('ar')->notification_title = $request->ar_notification_title;
        $event_type->translateOrNew('ar')->notification_message = $request->ar_notification_message;

        $event_type->translateOrNew('en')->name = $request->en_name;
        $event_type->translateOrNew('en')->notification_title = $request->en_notification_title;
        $event_type->translateOrNew('en')->notification_message = $request->en_notification_message;

        $event_type->save();
        return redirect()->back()->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_event = UserEvent::withTrashed()->where('event_type_id', $id)->count();
        if ($user_event > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        $event_type = EventType::find($id);
        $event_type->delete();
    }

    /**
     *  displays the deleted data as an initial deletion
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function recycle_bin()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('event_type_translations')
                ->join('event_types', 'event_type_translations.event_type_id', '=', 'event_types.id')
                ->where('event_type_translations.locale', '=', $locale)
                ->whereNotNull('event_types.deleted_at')
                ->select('event_type_translations.*')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="restore" id="' . $data->event_type_id . '" class="restore btn btn-primary btn-sm" data-toggle="Restore" title="Restore"><i class="ft ft-rotate-cw"></i></button>';
                    $button .= '&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->event_type_id . '" class="delete btn btn-danger btn-sm" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('managements.app.event_type.recycle_bin');

    }

    /**
     * retrieves deleted data from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore($id)
    {
        $event_type = EventType::withTrashed()->where('id', $id)->first();
        $event_type->restore();
        return response()->json();
    }

    /**
     * performs the final deletion from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function hdelete($id)
    {
        $user_event = UserEvent::withTrashed()->where('event_type_id', $id)->count();
        if ($user_event > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        DB::table('event_type_translations')
            ->where('event_type_id', '=', $id)
            ->delete();
        $event_type = EventType::withTrashed()->where('id', $id)->first();
        $event_type->forceDelete();
    }

}
