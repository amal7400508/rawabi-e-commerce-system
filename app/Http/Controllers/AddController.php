<?php

namespace App\Http\Controllers;

use App\Helpers\Image;
use App\Models\Add;
use App\Models\CartDetail;
use App\Models\HospitalityDetail;
use App\Models\Product;
use App\Models\ProductPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class AddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataSelect = DB::table('product_prices')
                ->join('unit_translations', 'product_prices.unit_id', '=', 'unit_translations.unit_id')
//                ->join('taste_translations', 'product_prices.taste_id', '=', 'taste_translations.taste_id')
                ->join('product_translations', 'product_prices.product_id', '=', 'product_translations.product_id')
                ->where('unit_translations.locale', '=', $locale)
//                ->where('taste_translations.locale', '=', $locale)
                ->where('product_translations.locale', '=', $locale)
                ->select('product_prices.*', 'unit_translations.name as unit_name'
                    , 'product_translations.name as product_name')
                ->get();
            /*$dataSelect = DB::table('product_prices')
                ->join('unit_translations', 'product_prices.unit_id', '=', 'unit_translations.unit_id')
                ->join('taste_translations', 'product_prices.taste_id', '=', 'taste_translations.taste_id')
                ->join('product_translations', 'product_prices.product_id', '=', 'product_translations.product_id')
                ->where('unit_translations.locale', '=', $locale)
                ->where('taste_translations.locale', '=', $locale)
                ->where('product_translations.locale', '=', $locale)
                ->select('product_prices.*', 'unit_translations.name as unit_name'
                    , 'taste_translations.taste as taste_name', 'product_translations.name as product_name')
                ->get();*/
            return datatables()->of($dataSelect)
                ->addColumn('action', function ($data) {
                    $span = '<span class="dropdown">';
                    $span .= '<button id="btnSearchDrop2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings"></i></button>';
                    $span .= '<span aria-labelledby="btnSearchDrop2" class="dropdown-menu mt-1 dropdown-menu-right">';
                    $span .= '<button type="button" name="show" id="' . $data->id . '" class="showB dropdown-item"><i class="la la-eye"></i>' . lang::get("provider.show") . '</button>';
                    $span .= '<a href="product_price/edit/' . $data->id . '"   class="dropdown-item"><i class="la la-pencil"></i>' . lang::get("provider.edit") . '</a>';
                    return $span;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('product_price.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
//        if (Gate::check('create product_price') || Gate::check('update product_price')) {
        $this->validate($request, [
            'product_id_add' => 'required',
            'type.*' => 'required|string|max:191',
            'color.*' => 'required|string|max:191',
            'ar_add.*' => 'required|string|max:191',
            'en_add.*' => 'required|string|max:191',
        ]);
        $input_product[] = [];
        try {
            foreach ($request->all() as $key => $st) {
                if ($st < 2) continue;
                for ($i = 0; $i < count($request->add_ar); $i++) {
                    $input_product[$i][$key] = isset($st[$i]) ? $st[$i] : null;
                }
                for ($i = 0; $i < count($request->type_id); $i++) {
                    $input_product[$i][$key] = isset($st[$i]) ? $st[$i] : null;
                }
                for ($i = 0; $i < count($request->color); $i++) {
                    $input_product[$i][$key] = isset($st[$i]) ? $st[$i] : null;
                }
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 401);
        }
        $count_add = 0;
        foreach ($input_product as $input_products) {
            $count_add++;
            $input_products['type_id'] = isset($input_products['type_id']) ? $input_products['type_id'] : null;
            $input_products['color'] = isset($input_products['color']) ? $input_products['color'] : null;
            $input_products['add_ar'] = isset($input_products['add_ar']) ? $input_products['add_ar'] : null;
            $input_products['add_en'] = isset($input_products['add_en']) ? $input_products['add_en'] : null;

            $product_price_id_add = isset($input_products['product_price_id_add']) ? $input_products['product_price_id_add'] : null;


            Add::updateOrCreate(['id' => $product_price_id_add], [
                    'product_id' => $request->product_id_add,
                    'type_id' => $input_products['type_id'],
                    'color' => $input_products['color'],
                    'add_ar' => $input_products['add_ar'],
                    'add_en' => $input_products['add_en'],
                ]
            );
        }
        return response()->json([
            'message' => Lang::get('provider.added_successfully')
        ]);
//        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_price = Product::find($id)->add;
        return response()->json([
            'product_price_data' => $product_price,
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
//        $carts_detail = CartDetail::where('product_price_id', $id)->count();
//        $hospitality_detail = HospitalityDetail::where('product_price_id', $id)->count();
//        if ($carts_detail > 0) {
//            return response()->json([
//                'error_deleting' => Lang::get('provider.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
//            ]);
//        }

        $product_price_delete = Add::withTrashed()->where('id', $id);
        $product_price_delete->forceDelete();

        return response()->json();

    }
}
