<?php

namespace App\Http\Controllers;

use App\Models\Note;
use App\Models\NoteTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class NoteController extends Controller
{
    public $view_path = 'managements.encoding.notes.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;

        $data = Note::orderBy('id', 'desc');
        $count = $data->count();
        return view($this->view_path . 'index')
            ->with('data', $data->paginate($table_length))
            ->with('count', $count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $note = new Note();
        $note->status = $request->input('status', 0);
        $note->start_date = $request->start_date;
        $note->end_date = $request->end_date;
        $note->save();

        $note->translateOrNew('ar')->message = $request->message_ar;
        $note->translateOrNew('en')->message = $request->message_en;
        $note->save();

        return response()->json([
            'status' => 200,
            'note' => $note,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request)
    {
        return Validator::make(
            $request->all(), [
            'message_ar' => 'required|string|max:191',
            'message_en' => 'required|string|max:191',
            'start_date' => 'required|date|after_or_equal:today',
            'end_date' => 'required|date|after_or_equal:start_date',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = Note::find($id);
        $note_ar = NoteTranslation::where('note_id', '=', $id)->where('locale', 'ar')->first();
        $note_en = NoteTranslation::where('note_id', '=', $id)->where('locale', 'en')->first();
        return response()->json([
            'id' => $note->id,
            'status' => $note->status,
            'start_date' => $note->start_date_carbon,
            'end_date' => $note->end_date_carbon,
            'message_ar' => $note_ar->message,
            'message_en' => $note_en->message,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $note = Note::find($id);
        $note->start_date = $request->start_date;
        $note->end_date = $request->end_date;
        $note->status = $request->input('status', 0);
        $note->save();

        $note->translateOrNew('ar')->message = $request->message_ar;
        $note->translateOrNew('en')->message = $request->message_en;
        $note->save();

        return response()->json([
            'status' => 200,
            'note' => $note,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Note::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Note::count()
        ],
            200
        );
    }
}
