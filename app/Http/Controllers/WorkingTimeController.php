<?php

namespace App\Http\Controllers;

use App\Http\Resources\WorkingTimesDetailsResource;
use App\Rules\CheckDays;
use App\Models\WorkingTime;
use App\Models\WorkingTimeTranslation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class WorkingTimeController extends Controller
{
    public $view_path = 'managements.encoding.working_times.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if (request()->ajax()) {
            $working_time = WorkingTime::where('branch_id', null)->groupBy('week_day')->get('week_day');
            return datatables()->of($working_time)
                ->addColumn('action', function ($data) {
                    $actions = '<a class="action-item detail" id="' . $data->week_day . '"><span style="font-size:12px;color: #ed1b24">' . __('admin.detail') . '...</span></a>';
                    return $actions;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view($this->view_path . 'index');
    }

    public function details($week_day)
    {
        $working_time = WorkingTime::where('week_day', $week_day)->get();
        $data = WorkingTimesDetailsResource::collection($working_time);

        return response()->json([
            'day' => $working_time->first()->day,
            'max_updated_at' => $working_time->max('updated_at')->diffForHumans(),
            'data' => $data
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'days.*' => [function ($attribute, $value, $fail) use ($request) {
                $days = ['FRIDAY', 'SATURDAY', 'SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY'];
                if (!in_array($value, $days)) {
                    $fail(lang::get("admin.123") . '.');
                }
            }],
            'start_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i|after_or_equal:start_time',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $count_days = count($request->days ?? []);
        if ($count_days == 0) {
            return response()->json([
                'days' => Lang::get('validation.you_must_specify_at_least_one_day')
            ], 401);
        }


        foreach ($request->days as $day) {
            $working_time = new WorkingTime();
            $working_time->week_day = $day;
            $working_time->status = 1;
            $working_time->start_time = $request->start_time;
            $working_time->end_time = $request->end_time;
            $working_time->save();

//            $working_time->translateOrNew('ar')->message = $request->message_ar_add;
//            $working_time->translateOrNew('en')->message = $request->message_en_add;
//            $working_time->save();
        }

        return response()->json([
            'message' => Lang::get('admin.added_successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $working_time = WorkingTime::where('id', $id)->first();

        return response()->json([
            'id' => $working_time->id,
            'is_active' => $working_time->status,
            'start_time' => Carbon::Parse($working_time->start_time)->format('H:i'),
            'end_time' => Carbon::Parse($working_time->end_time)->format('H:i'),
            'day' => $working_time->day,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        session()->put('note_id', $id);

        $this->validateWorkingTime($request);

        $working_time = WorkingTime::find($id);
        $working_time->start_time = $request->start_time;
        $working_time->end_time = $request->end_time;
        $working_time->status = $request->input('status', 0);
        $working_time->save();

//        $working_time->translateOrNew('ar')->message = $request->message_ar ?? '';
//        $working_time->translateOrNew('en')->message = $request->message_en ?? '';
//        $working_time->save();

        return redirect()->back()->with('success', Lang::get('admin.edited_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attention = WorkingTime::findOrFail($id);
        $attention->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => WorkingTime::count()
        ],
            200
        );
    }

    public function validateWorkingTime($request)
    {
        return $this->validate($request, [
            'start_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i|after_or_equal:start_time',
        ]);
    }
}
