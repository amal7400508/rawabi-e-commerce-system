<?php

namespace App\Http\Controllers;

use App\Exports\StatisticExport;
use App\Models\Advertisement;
use App\Models\Branch;
use App\Models\Category;
use App\Models\MainCategory;
use App\Models\Offer;
use App\Models\PaidAdvertisement;
use App\Models\Product;
use App\Models\Provider;
use App\Models\Statistic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class StatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = $this->getData($request);

        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();

        return view('managements.statistics.index')
            ->with('providers', Provider::all())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count);
    }

    /**
     * Fetch reports according to conditions
     *
     * @param $request
     * @return mixed
     */
    public function getData($request)
    {
//        search,product,offer,branch,provider,advertisement_image,advertisement_video,advertisement_popup,advertisement_slider,cart
        $data = new Statistic();
        if (!is_null($request->from)) {
            $data = $data->whereDate('created_at', '>=', $request->from);
        }
        if (!is_null($request->to)) {
            $data = $data->whereDate('created_at', '<=', $request->to);
        }

        if (!is_null($request->provider)) {
            $branches_ids = Provider::find($request->provider)->branch->pluck('id')->toArray();
            $data = $data->where(function ($query) use ($branches_ids) {
                $query->whereHasMorph('statisticable', [
                    Product::class,
                    Offer::class,
                    PaidAdvertisement::class,
                ], function ($q) use ($branches_ids) {
                    $q->whereIn('branch_id', $branches_ids);
                })->orWhereHasMorph('statisticable', [
                    Branch::class,
                ], function ($q) use ($branches_ids) {
                    $q->whereIn('id', $branches_ids);
                });
            });
        }
        if (!is_null($request->branch_name)) {
            $branch_id = $request->branch_name;
            $data = $data->where(function ($query) use ($branch_id) {
                $query->whereHasMorph('statisticable', [
                    Product::class,
                    Offer::class,
                    PaidAdvertisement::class,
                ], function ($q) use ($branch_id) {
                    $q->where('branch_id', $branch_id);
                })->orWhereHasMorph('statisticable', [
                    Branch::class,
                ], function ($q) use ($branch_id) {
                    $q->where('id', $branch_id);
                });
            });        }

        if (!is_null($request->click_type)) {
            $data = $data->where('click_type', $request->click_type);
        }
        if (!is_null($request->search)) {
            $data = $data->where(function ($query) use ($request) {
                $query->whereHasMorph('statisticable', [
                    Category::class,
                    MainCategory::class,
                    Product::class,
                    Offer::class,
                    Branch::class,
                ], function ($q) use ($request) {
                    $q->whereTranslationLike('name', '%' . $request->search . '%');
                })->orWhere(function ($query) use ($request) {
                    $query->whereHasMorph('statisticable', [
                        Advertisement::class,
                    ], function ($q) use ($request) {
                        $q->whereTranslationLike('desc', '%' . $request->search . '%');
                    });
                })->orWhere(function ($query) use ($request) {
                    $query->whereHasMorph('statisticable', [
                        PaidAdvertisement::class,
                    ], function ($q) use ($request) {
                        $q->where('title', 'LIKE', '%' . $request->search . '%');
                    });
                })->orWhere(function ($query) use ($request) {
                    $query->whereHasMorph('statisticable', [
                        Provider::class,
                    ], function ($q) use ($request) {
                        $q->where('name', 'LIKE', '%' . $request->search . '%');
                    });
                })->orWhere('text', 'LIKE', '%' . $request->search . '%');
            });
        }
        if (!is_null($request->user_name)) {
            $data = $data->whereHas('user', function ($q) use ($request) {
                $q->where('full_name', 'LIKE', '%' . $request->user_name . '%');
            });
        }
        if (!is_null($request->phone)) {
            $data = $data->whereHas('user', function ($q) use ($request) {
                $q->where('phone', 'LIKE', '%' . $request->phone . '%');
            });

        }
        if (!is_null($request->gender)) {
            $data = $data->whereHas('user', function ($q) use ($request) {
                $q->where('gender', $request->gender);
            });
        }

        if ($request->type == 'total') {
            $array = [];
            foreach ($request->option ?? [] as $option) {
                if ($option == 'user') {
                    $array[] = 'user_id';
                }
                if ($option == 'click_item') {
                    $array[] = 'statisticable_type';
                    $array[] = 'statisticable_id';
                }
            }

            $array[] = 'click_type';
            $array[] = DB::raw('sum(number) as number');
            $array_select = $array;
            array_pop($array);

            $data = $data->select($array_select)
                ->groupBy($array)
                ->orderBy('number', 'Desc');
        }

        return $data;
    }

    public function export(Request $request)
    {
        $data = $this->getData($request);

        return Excel::download(
            new StatisticExport($data->get()),
            'statistics-' . now() . '.xlsx'
        );
    }
}
