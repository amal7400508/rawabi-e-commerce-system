<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\AreaTranslation;
use App\Models\Branch;
use App\Models\BranchService;
use App\Models\BranchTranslation;
use App\Models\Carrier;
use App\Models\Provider;
use App\Notifications\BranchNotification;
use App\Helpers\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class BranchController extends Controller
{
    /**
     * @var string
     */
    public $view_path = 'managements.profile_management.branches.';

    /**
     * Display branches data.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('branch_translations')
                ->join('branches', 'branch_translations.branch_id', '=', 'branches.id')
                ->where('branch_translations.locale', '=', $locale)
                ->whereNull('branches.deleted_at')
//                ->whereIn('branch_id', authProviderBranchesIds())
                ->select('branch_translations.*', 'branches.status', 'branches.updated_at')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $span = '<span class="dropdown">';
                    $span .= '<button id="btnSearchDrop2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings"></i></button>';
                    $span .= '<span aria-labelledby="btnSearchDrop2" class="dropdown-menu mt-1 dropdown-menu-right">';
                    $span .= '<button type="button" name="show" id="' . $data->branch_id . '" class="showB dropdown-item"><i class="la la-eye"></i>' . lang::get("admin.show") . '</button>';
                    $span .= '';
                    if (Gate::check('update branches')) {
                        $span .= '<a href="branches/edit/' . $data->branch_id . '"   class="dropdown-item"><i class="la la-pencil"></i>' . lang::get("admin.edit") . '</a>';
                    }
                    $span .= '';
                    if (Gate::check('delete branches')) {
//                        $span .= '<button type="button" name="delete" class="deleteB dropdown-item" id="' . $data->branch_id . '"><i class="la la-trash"></i>' . lang::get("provider.delete") . ' </button></span></span>';
                    }
                    return $span;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        // Todo this service_type_id for add branch
//        $branch_services = BranchService::
//            where('service_type_id', 5)
//            ->count();

        return view('managements.profile_management.branches.index')
            ->with('branch_services');
    }

//    public function map()
//    {
//        $branch = Branch::whereIn('provider_id',authProviderBranches())->get();
//        return view('branches.map',compact('branch'));
//    }

    public function create()
    {
        return view('managements.profile_management.branches.create')
            ->with('areas', Area::where('status', 1)->get())
             ->with('providers', Provider::where('status', 1)
                ->get());

    }


    public function store(Request $request)
    {



      $this->validate($request, [
          'area_id' => 'required|string|max:191',
          'provider_id' => 'required|string|max:191',
          'ar_name_B' => 'required|string|max:191',
          'en_name_B' => 'required|string|max:191',
          'ar_address' => 'required|string|max:191',
          'en_address' => 'required|string|max:191',
          'ar_sections' => 'required|string|max:191',
          'en_sections' => 'required|string|max:191',
//                'ar_request_arrival_time' => 'required|string|max:191',
//                'en_request_arrival_time' => 'required|string|max:191',
          'email' => 'required|email|max:191|unique:admins,email',
          'phone' => 'required|numeric|min:999999',
          'long' => 'required|numeric',
          'lat' => 'required|numeric',

        ]);

        $imageName = null;
        if ($request->hasFile('image')) {
            $imageName = Image::reSizeImage($request->file('image'), 'storage/branch/', 'smart_delivery_branch_');
        }
        $branch = new Branch();
//        $branch->provider_id = Auth::user()->id;
        $branch->area_id = $request->area_id;
        $branch->provider_id = $request->provider_id;
        $branch->status = $request->status;
        $branch->email = $request->email;
        $branch->phone = $request->phone;
        $branch->long = $request->long;
        $branch->lat = $request->lat;
        $branch->image = $imageName;

        $branch->save();

        $branch->translateOrNew('ar')->name = $request->ar_name_B;
        $branch->translateOrNew('ar')->address = $request->ar_address;
        $branch->translateOrNew('ar')->sections = $request->ar_sections;
        $branch->translateOrNew('ar')->request_arrival_time = $request->ar_request_arrival_time;

        $branch->translateOrNew('en')->name = $request->en_name_B;
        $branch->translateOrNew('en')->address = $request->en_address;
        $branch->translateOrNew('en')->sections = $request->en_sections;
        $branch->translateOrNew('en')->request_arrival_time = $request->en_request_arrival_time;

        $branch->save();
//        dd(123);

        return redirect('profile_management/branches')->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $carrier = Branch::find($id);

        return response()->json([
            'id' => $carrier->id,
            'name' => $carrier->name,
            'phone' => $carrier->phone,
            'email' => $carrier->email,
            'status' => $carrier->status,
            'area' => $carrier->area->name,
            'provider' => $carrier->provider_id,
            'address' => $carrier->address,
            'sections' => $carrier->sections,
            'lat' => $carrier->lat,
            'long' => $carrier->long,
            'request_arrival_time' => $carrier->request_arrival_time ?? null,

            'provider_name' => $carrier->provider->name ?? null,
            'provider_phone' => $carrier->provider->phone ?? null,
            'provider_email' => $carrier->provider->email ?? null,
            'image' => $carrier->provider->image_path ?? null,

            'updated_at' => Carbon::Parse($carrier->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($carrier->created_at)->format('Y/m/d'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provider = Branch::find($id)->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()){
//            abort(403);
//        }

        $branches = Branch::find($id);
        $branches_ar = BranchTranslation::where('branch_id', '=', $id)->where('locale', 'ar')->first();
        $branches_en = BranchTranslation::where('branch_id', '=', $id)->where('locale', 'en')->first();
        $all_areas = Area::all();
        $all_provider = Provider::all();
        return view('managements.profile_management.branches.edit', array(
            'branches_ar' => $branches_ar,
            'branches_en' => $branches_en,
            'branches' => $branches,
            'all_areas' => $all_areas,
            'all_provider' => $all_provider
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch = Branch::find($id);
//        $provider = $branch->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()){
//            abort(403);
//        }

        if ($branch->is_approved == 0 or $branch->is_approved == 3) {
            $this->validate($request, [
                'area_id' => 'required|string|max:191',
                'provider_id' => 'required|string|max:191',
                'ar_name_B' => 'required|string|max:191',
                'en_name_B' => 'required|string|max:191',
                'ar_address' => 'required|string|max:191',
                'en_address' => 'required|string|max:191',
                'ar_sections' => 'required|string|max:191',
                'en_sections' => 'required|string|max:191',
//                'ar_request_arrival_time' => 'required|string|max:191',
//                'en_request_arrival_time' => 'required|string|max:191',
                'email' => 'required|email|max:191|unique:admins,email',
                'phone' => 'required|numeric|min:999999',
                'long' => 'required|numeric',
                'lat' => 'required|numeric',
            ]);

            if ($request->hasFile('image')) {
                $imageName = Image::reSizeImage($request->file('image'), 'storage/branch/', 'smart_delivery_branch_');
                $folders_name = ['/640/', '/360/', '/150/', '/64/'];
                $image_name_store = $branch->image;
                for ($i = 0; $i < count($folders_name); $i++) {
                    $filename = public_path() . '/' . 'storage/branch/' . $folders_name[$i] . $image_name_store;
                    File::delete($filename);
                }
            }

            $branch = Branch::find($id);
//            $branch->provider_id = Auth::user()->id;
            if ($request->hasFile('image')) {
                $branch->image = $imageName;
            }
            $branch->area_id = $request->area_id;
            $branch->provider_id = $request->provider_id;
            $branch->status = $request->status;
            $branch->email = $request->email;
            $branch->phone = $request->phone;
            $branch->long = $request->long;
            $branch->lat = $request->lat;
            $branch->save();

            $branch->translateOrNew('ar')->name = $request->ar_name_B;
            $branch->translateOrNew('ar')->address = $request->ar_address;
            $branch->translateOrNew('ar')->sections = $request->ar_sections;
            $branch->translateOrNew('ar')->request_arrival_time = $request->ar_request_arrival_time;

            $branch->translateOrNew('en')->name = $request->en_name_B;
            $branch->translateOrNew('en')->address = $request->en_address;
            $branch->translateOrNew('en')->sections = $request->en_sections;
            $branch->translateOrNew('en')->request_arrival_time = $request->en_request_arrival_time;

            $branch->save();
        } else {
            if ($request->hasFile('image')) {
                $imageName = Image::reSizeImage($request->file('image'), 'storage/branch/', 'smart_delivery_branch_');
                $folders_name = ['/640/', '/360/', '/150/', '/64/'];
                $image_name_store = $branch->image;
                for ($i = 0; $i < count($folders_name); $i++) {
                    $filename = public_path() . '/' . 'storage/branch/' . $folders_name[$i] . $image_name_store;
                    File::delete($filename);
                }
            }

            if ($request->hasFile('image')) {
                $branch->image = $imageName;
            }
            $branch->long = $request->long;
            $branch->lat = $request->lat;
            $branch->status = $request->status;
            $branch->update();
        }
        return redirect('profile_management/branches')->with('success', Lang::get('admin.edited_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = \App\Models\Request::where('branch_id', $id)->count();
        if ($request > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        $carrier = Carrier::withTrashed()->where('branch_id', $id)->count();
        if ($carrier > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        $branch = Branch::find($id);
        $provider = $branch->provider ?? null;
        if (setProviderID($provider) != getParentProviderId()){
            abort(403);
        }
        $branch->delete();
        return redirect('/');
    }

    /**
     *  displays the deleted data as an initial deletion
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function recycle_bin()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('branch_translations')
                ->join('branches', 'branch_translations.branch_id', '=', 'branches.id')
                ->where('branch_translations.locale', '=', $locale)
                ->whereNotNull('branches.deleted_at')
                ->select('branch_translations.*', 'branches.status')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="restore" id="' . $data->branch_id . '" class="restore btn btn-primary btn-sm" data-toggle="Restore" title="Restore"><i class="ft ft-rotate-cw"></i></button>';
                    $button .= '&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->branch_id . '" class="delete btn btn-danger btn-sm" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('branches.recycle_bin');
    }

    /**
     * retrieves deleted data from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore($id)
    {
        $branch = Branch::withTrashed()->where('id', $id)->first();
        $provider = $branch->provider ?? null;
        if (setProviderID($provider) != getParentProviderId()){
            abort(403);
        }
        $branch->restore();
        return response()->json();
        /*        return redirect()->back()->with('success','تمت الإستعادة بنجاح');*/
    }

    /**
     * performs the final deletion
     * from the Recycle Bin
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function hdelete($id)
    {
        $carrier = Carrier::withTrashed()->where('branch_id', $id)->count();
        if ($carrier > 0) {
            return response()->json([
                'error_delete' => Lang::get('provider.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        DB::table('branch_translations')
            ->where('branch_id', '=', $id)
            ->delete();
        $branch = Branch::withTrashed()->where('id', $id)->first();
        $branch->forceDelete();
    }

//    public function validatedData($request)
//    {
//
//        return Validator::make(
//            $request->all(), [
//            'area_id' => ['required', 'string', 'max:191'],
//            'ar_name_B' => 'required|string|max:191',
//            'en_name_B' => 'required|string|max:191',
//            'ar_address' => 'required|string|max:191',
//            'en_address' => 'required|string|max:191',
//            'ar_sections' => 'required|string|max:191',
//            'en_sections' => 'required|string|max:191',
//            'ar_request_arrival_time' => 'required|string|max:191',
//            'en_request_arrival_time' => 'required|string|max:191',
//            'email' => 'required|email|max:191|unique:admins,email',
//            'phone' => 'required|numeric|min:999999',
//        ]);
//    }
}
