<?php

namespace App\Http\Controllers;

use App\Utilities\Helpers\Image;
use App\Models\CartDetail;
use App\Models\HospitalityDetail;
use App\Models\Product;
use App\Models\ProductPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class ProductPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataSelect = DB::table('product_prices')
                ->join('unit_translations', 'product_prices.unit_id', '=', 'unit_translations.unit_id')
//                ->join('taste_translations', 'product_prices.taste_id', '=', 'taste_translations.taste_id')
                ->join('product_translations', 'product_prices.product_id', '=', 'product_translations.product_id')
                ->where('unit_translations.locale', '=', $locale)
//                ->where('taste_translations.locale', '=', $locale)
                ->where('product_translations.locale', '=', $locale)
                ->select('product_prices.*', 'unit_translations.name as unit_name'
                    , 'product_translations.name as product_name')
                ->get();
            /*$dataSelect = DB::table('product_prices')
                ->join('unit_translations', 'product_prices.unit_id', '=', 'unit_translations.unit_id')
                ->join('taste_translations', 'product_prices.taste_id', '=', 'taste_translations.taste_id')
                ->join('product_translations', 'product_prices.product_id', '=', 'product_translations.product_id')
                ->where('unit_translations.locale', '=', $locale)
                ->where('taste_translations.locale', '=', $locale)
                ->where('product_translations.locale', '=', $locale)
                ->select('product_prices.*', 'unit_translations.name as unit_name'
                    , 'taste_translations.taste as taste_name', 'product_translations.name as product_name')
                ->get();*/
            return datatables()->of($dataSelect)
                ->addColumn('action', function ($data) {
                    $span = '<span class="dropdown">';
                    $span .= '<button id="btnSearchDrop2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings"></i></button>';
                    $span .= '<span aria-labelledby="btnSearchDrop2" class="dropdown-menu mt-1 dropdown-menu-right">';
                    $span .= '<button type="button" name="show" id="' . $data->id . '" class="showB dropdown-item"><i class="la la-eye"></i>' . lang::get("admin.show") . '</button>';
                    $span .= '<a href="product_price/edit/' . $data->id . '"   class="dropdown-item"><i class="la la-pencil"></i>' . lang::get("admin.edit") . '</a>';
                    return $span;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('product_price.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
//        if (Gate::check('create product_price') || Gate::check('update product_price')) {
        $this->validate($request, [
            'product_id' => 'required',
            'price.*' => 'required|numeric|max:99999999.99',
        ]);
        $input_product[] = [];
        try {
            foreach ($request->all() as $key => $st) {
                if ($st < 2) continue;
                for ($i = 0; $i < count($request->price); $i++) {
                    $input_product[$i][$key] = isset($st[$i]) ? $st[$i] : null;
                }
                for ($i = 0; $i < count($request->note_ar); $i++) {
                    $input_product[$i][$key] = isset($st[$i]) ? $st[$i] : null;
                }
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 401);
        }
        $count = 0;
        foreach ($input_product as $input_products) {
            $count++;
            $input_products['image'] = isset($input_products['image']) ? $input_products['image'] : null;
            $input_products['unit_id'] = isset($input_products['unit_id']) ? $input_products['unit_id'] : null;
            $input_products['color_id'] = isset($input_products['color_id']) ? $input_products['color_id'] : null;
            $input_products['note_ar'] = isset($input_products['note_ar']) ? $input_products['note_ar'] : null;
            $input_products['note_en'] = isset($input_products['note_en']) ? $input_products['note_en'] : null;
            $input_products['currency'] = isset($input_products['currency']) ? $input_products['currency'] : null;
            $input_products['status'] = isset($input_products['status']) ? $input_products['status'] : null;
            $input_products['status_price'] = isset($input_products['status_price']) ? $input_products['status_price'] : null;

            if ($input_products['image'] != null) {
                $imageName = Image::reSizeImage(
                    $input_products['image'],
                    'storage/product_price',
                    'smart_delivery_product_price_'
                );
                $imageNameStore = $imageName;
            } else {
//                    $imageNameStore = 'defualt_product.png';
                $imageNameStore = null;
            }

            $state = $input_products['status'] != null ? $input_products['status'] : 0;
            $status_price = $input_products['status_price'] != null ? $input_products['status_price'] : 0;
            $id_unit = $input_products['unit_id'] != null ? $input_products['unit_id'] : null;
            $id_color = $input_products['color_id'] != null ? $input_products['color_id'] : null;
//                $currency = $input_products['currency'] != null ? $input_products['currency'] : null;
            $product_price_id = isset($input_products['product_price_id']) ? $input_products['product_price_id'] : null;

            if ($input_products['image'] == null) {
                ProductPrice::updateOrCreate(['id' => $product_price_id], [
                        'available' => $state,
                        'price' => $input_products['price'],
                        'unit_id' => $id_unit,
                        'color_id' =>  $id_color,
                        'product_id' => $request->product_id,
                        'note_ar' => $input_products['note_ar'],
                        'note_en' => $input_products['note_en'],
                        'currency' => $input_products['currency'],
                    ]
                );

            } else {
                ProductPrice::updateOrCreate(['id' => $product_price_id], [
                        'available' => $state,
                        'price' => $input_products['price'],
                        'unit_id' => $id_unit,
                        'color_id' =>  $id_color,
                        'product_id' => $request->product_id,
                        'note_ar' => $input_products['note_ar'],
                        'note_en' => $input_products['note_en'],
                        'currency' => $input_products['currency'],
                        'image' => $imageNameStore,
                    ]
                );
            }
        }
        return response()->json([
            'message' => Lang::get('admin.added_successfully')
        ]);
//        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_price = Product::find($id)->productPrice;
        return response()->json([
            'product_price_data' => $product_price,
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $carts_detail = CartDetail::where('product_price_id', $id)->count();
//        $hospitality_detail = HospitalityDetail::where('product_price_id', $id)->count();
        if ($carts_detail > 0) {
            return response()->json([
                'error_deleting' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        try {
            Image::deleteImageFromFolder(
                'storage/product_price',
                ProductPrice::find($id)->image
            );
            $product_price_delete = ProductPrice::withTrashed()->where('id', $id);
            $product_price_delete->forceDelete();
        } catch (\Exception $e) {
            return response()->json([
                'error_deleting' => 'Message: ' . $e->getMessage()
            ]);
        }
    }
}
