<?php

namespace App\Http\Controllers;

use App\Models\Advertisement;
use App\Models\AdvertisementTranslation;

use App\Models\Stateable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class AdvertisementController extends Controller
{
    public $view_path = 'managements.advertisements.advertisements.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new Advertisement();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', $query);
            } else if ($search_type == 'type') {
                if ($query == 'صور منبثقة') $query = 'popup';
                else if($query == 'اعلانات متحركة') $query = 'slider';
                else if($query == 'Popup image') $query = 'popup';
                $data = $data->where('type', $query);
            } else if ($search_type == 'description') {
                $data = $data->whereTranslationLike('desc', '%' . $query . '%');
            } else if ($search_type == 'end_date') {
                $data = $data->where('end_date', 'like','%' . $query . '%');
            }
        endif;

        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'type' => 'required|in:slider,popup',
            'end_date' => 'nullable|date',
            'description_ar' => 'nullable|string',
            'description_en' => 'nullable|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $image_name = uploadImage($request->image, 'advertisements');

        $advertisement = new Advertisement();
        $advertisement->type = $request->type;
        $advertisement->end_date = $request->end_date;
        $advertisement->slide_number = $request->slide_number;
        $advertisement->save();

        $advertisement->translateOrNew('ar')->desc = $request->description_ar;
        $advertisement->translateOrNew('ar')->image = $image_name;

        $advertisement->translateOrNew('en')->desc = $request->description_en;
        $advertisement->translateOrNew('en')->image = $image_name;
        $advertisement->save();

        return response()->json([
            'status' => 200,
            'advertisement' => $advertisement,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $advertisement = Advertisement::find($id);
        $btnShow = '';
        if (Gate::check('delete advertisement')) {
            $btnShow = '<button class="delete btn btn-float btn-round btn-danger" id="' . $advertisement->advertisement_id . '"><i class="ft-trash-2"></i></button>';
        }

        $states = Stateable::where('stateable_type', Advertisement::class)
            ->where('stateable_id', $id)
            ->get();

        return response()->json([
            'id' => $advertisement->category_id,
            'type' => $advertisement->type,
            'desc' => $advertisement->desc,
            'image' => $advertisement->image,
            'end_date' => $advertisement->end_date,
            'states' => $states,
            'created_at' => Carbon::Parse($advertisement->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($advertisement->updated_at)->format('Y/m/d'),
            'btn' => $btnShow,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertisement = Advertisement::find($id);
        $advertisement_ar = AdvertisementTranslation::where('advertisement_id', '=', $id)->where('locale', 'ar')->first();
        $advertisement_en = AdvertisementTranslation::where('advertisement_id', '=', $id)->where('locale', 'en')->first();

        return response()->json([
            'id' => $advertisement->id,
            'desc_ar' => $advertisement_ar->desc,
            'desc_en' => $advertisement_en->desc,
            'type' => $advertisement->type,
            'end_date' => $advertisement->end_date,
            'slide_number' => $advertisement->slide_number,
            'image' => $advertisement->image_path,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'type' => 'required|in:slider,popup',
            'end_date' => 'nullable|date',
            'description_ar' => 'nullable|string',
            'description_en' => 'nullable|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $advertisement = Advertisement::find($id);

        $image_name = $advertisement->image;
        if ($request->hasFile('image')) {
            deleteImage('/storage/advertisements/' . $advertisement->image);
            $image_name = uploadImage($request->image, 'advertisements');
        }

        $advertisement->type = $request->type;
        $advertisement->end_date = $request->end_date;
        $advertisement->slide_number = $request->slide_number;
        $advertisement->save();

        $advertisement->translateOrNew('ar')->desc = $request->description_ar;
        $advertisement->translateOrNew('ar')->image = $image_name;

        $advertisement->translateOrNew('en')->desc = $request->description_en;
        $advertisement->translateOrNew('en')->image = $image_name;
        $advertisement->save();


        return response()->json([
            'status' => 200,
            'advertisement' => $advertisement,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertisement = Advertisement::find($id);
        deleteImage('/storage/advertisements/' . $advertisement->image);
        $advertisement->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Advertisement::count()
        ],
            200
        );
    }

}
