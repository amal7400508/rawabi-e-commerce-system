<?php

namespace App\Http\Controllers;

use App\Http\Resources\BranchesCategoryResource;
use App\Models\Branch;
use App\Models\BranchesCategory;
use App\Models\MainCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class BranchesCategoryController extends Controller
{
    public $view_path = 'managements.encoding.branches_categories.';

    public function index()
    {
        return view($this->view_path . 'index')
            ->with('categories', MainCategory::whereDoesntHave('sub')->active()->get())
            ->with('branches', Branch::active()->get())
            ;
    }


    public function getBranches($main_category_id)
    {
        $data = BranchesCategory::where('main_category_id', $main_category_id)->get();
        $branches_category = BranchesCategoryResource::collection($data);

        return response()->json([
            'data' => $branches_category,
        ]);
    }

    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $category_product = new BranchesCategory();
        $category_product->main_category_id = $request->category_id;
        $category_product->branch_id = $request->branch;
        $category_product->commission = $request->commission;
        $category_product->save();

        return response()->json([
            'success' => Lang::get('admin.added_successfully'),
            'category_id' => $request->category_id
        ]);
    }

    public function validatedData($request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'category_id' => [
                'required',
                'integer',
                function ($attribute, $value, $fail) {
                    $category_is_leaf = MainCategory::find($value)->is_leaf;
                    if ($category_is_leaf){
                        $fail(lang::get("validation.a_childless_a_category_should_be_chosen"));
                    }
                }],
            'branch' => [
                'required',
                'integer',
                function ($attribute, $value, $fail) use ($request) {
                    $category_product = BranchesCategory::where('branch_id', $value)
                        ->where('main_category_id', $request->category_id)
                        ->count();
                    if ($category_product != 0) {
                        $fail(lang::get("validation.this_branch_is_already_in_this_category") . '.');
                    }
                }],
            'commission' => ['required', 'numeric', 'min:0', 'max:99']
        ]);

        return $validatedData;
    }

    public function destroy($id)
    {
        $branches_category = BranchesCategory::find($id);
        $category_id = $branches_category->main_category_id;
        $branches_category->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'category_id' => $category_id
        ]);
    }
}
