<?php

namespace App\Http\Controllers;

use App\Models\Deposit;
use App\Models\User;
use App\Notifications\Admin\ProviderNotification;
use App\Utilities\Helpers\Fcm;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class DepositController extends Controller
{
    /**
     * @var string
     */
    public $view_path = 'managements.app.deposits.';

    /**
     * Display deposit data via admin.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = Deposit::whereNull('deposits.branch_id');
        $data = $data->orderBy('deposits.id', 'desc');

        $view_name = 'index';

        return $this->displayDeposit($data, $request, $view_name);
    }

    /**
     * Display deposit data via branch.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function depositsViaBranch(Request $request)
    {
        $data = Deposit::whereNotNull('deposits.branch_id');
        $data = $data->orderBy('deposits.is_approved', 'asc')
            ->orderBy('deposits.id', 'desc');

        $view_name = 'deposit_branches';

        return $this->displayDeposit($data, $request, $view_name);
    }

    public function displayDeposit($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = $this->search($data, $request);

        $data_count = $data->count();

        return view($this->view_path . $view_name)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    public function search($data, $request)
    {
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'user_name') {
                $data = $data->join('wallets', 'deposits.wallet_id', 'wallets.id')
                    ->join('users', 'wallets.walletable_id', 'users.id')
                    ->where('wallets.walletable_type', User::class)
                    ->where('users.full_name', 'like', '%' . $query . '%');
            } else if ($search_type == 'phone') {
                $data = $data->join('wallets', 'deposits.wallet_id', 'wallets.id')
                    ->join('users', 'wallets.walletable_id', 'users.id')
                    ->where('wallets.walletable_type', User::class)
                    ->where('users.phone', 'like', '%' . $query . '%');
            } else if ($search_type == 'amount') {
                $data = $data->whereHas('wallet', function ($q) use ($query) {
                    $q->where('amount', $query);
                });
            } else if ($search_type == 'current_balance') {
                $data = $data->whereHas('wallet', function ($q) use ($query) {
                    $q->where('current_balance', $query);
                });
            }
        endif;

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $deposit = Deposit::find($id);

        $is_approved = $deposit->is_approved;

        if ($is_approved == 0) $status = __('admin.waiting');
        else if ($is_approved == 1) $status = __('admin.certified');
        else $status = __('admin.canceled');

        return response()->json([
            'id' => $deposit->id,
            'name' => $deposit->wallet->walletable->full_name,
            'email' => $deposit->wallet->walletable->email,
            'phone' => $deposit->wallet->walletable->phone,
            'image' => $deposit->wallet->walletable->image,
            'amount' => $deposit->amount,
            'is_approved' => $status,
            'note' => $deposit->note,
            'branch_name' => $deposit->branch->name ?? null,
            'updated_at' => Carbon::Parse($deposit->updated_at)->format('Y/m/d H:i:s'),
            'created_at' => Carbon::Parse($deposit->created_at)->format('Y/m/d H:i:s'),
        ]);
    }

    public function accept(Request $request)
    {
        $id = $request->items_id;
        $deposit = Deposit::find($id);
        if($deposit->is_approved != 0 or is_null($deposit->branch_id)){
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($deposit, $id) {
            $user = $deposit->wallet->walletable;
            $user->addToBalanceFromBranch($id, $deposit->branch_id);
            $deposit = Deposit::find($id);
            responseNotify($id, Deposit::class, ProviderNotification::class);
            $deposit->notify(new ProviderNotification());
        });

        // fcm firbase notify
        $deposit = Deposit::find($id);
        $user_id = $deposit->wallet->walletable_id;
        $user = User::find($user_id);
        $fcm_title = __('admin.deposit');
        $fcm_body_ar = 'عزيزي: ' . $user->full_name . ' تم إيداع مبلغ ' . $deposit->amount . ' ريال في حسابك، رصيدك الحالي ' . $deposit->current_balance . ' ريال ';
        $fcm_body_en = 'My dear: ' . $user->full_name . 'Your account has been deposited ' . $deposit->amount . ' RY, your current balance is ' . $deposit->current_balance . ' RY';

        $fcm_body = app()->getLocale() == 'ar'
            ? $fcm_body_ar
            : $fcm_body_en;

        Fcm::send_MKA_notification($fcm_title, $fcm_body, $user->id);

        return response()->json([
            'status' => 200,
            'data' => Deposit::find($id),
            'title' => Lang::get('admin.accepted_successfully'),
            'message' => ''
        ]);
    }

    public function reject(Request $request)
    {
        $id = $request->items_id;
        $deposit = Deposit::find($id);
        if($deposit->is_approved != 0 or is_null($deposit->branch_id)){
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($deposit, $id) {
            $deposit->update(['is_approved' => 3]);
            responseNotify($id, Deposit::class, ProviderNotification::class);
            $deposit->notify(new ProviderNotification());
        });

        return response()->json([
            'status' => 200,
            'data' => Deposit::find($id),
            'title' => Lang::get('admin.rejected_successfully'),
            'message' => ''
        ]);
    }
}
