<?php

namespace App\Http\Controllers;

use App\Models\ProductPrice;
use App\Models\Unit;
use App\Models\UnitTranslation;
use App\Models\UnitType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class UnitController extends Controller
{

    public $view_path = 'managements.encoding.units.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new Unit();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'name') {
                $data = $data->whereTranslationLike('name', '%' . $query . '%');
            } else if ($search_type == 'unit_type') {
                $data = $data->whereHas('unitType', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            }
        endif;

        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')
            ->with('unit_types', UnitType::where('status', 1)->get())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $unit = new Unit();
        $unit->unit_type_id = $request->unit_type;
        $unit->status = $request->input('status', 0);
        $unit->save();

        $unit->translateOrNew('ar')->name = $request->name_ar;
        $unit->translateOrNew('en')->name = $request->name_en;
        $unit->save();

        return response()->json([
            'status' => 200,
            'unit' => $unit,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request)
    {
        return Validator::make(
            $request->all(), [
            'unit_type' => 'required|integer',
            'name_ar' => 'required|string|max:191',
            'name_en' => 'required|string|max:191',
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit = Unit::find($id);
        $unit_ar = UnitTranslation::where('unit_id', '=', $id)->where('locale', 'ar')->first();
        $unit_en = UnitTranslation::where('unit_id', '=', $id)->where('locale', 'en')->first();

        return response()->json([
            'id' => $unit->id,
            'unit_type' => $unit->unit_type_id,
            'status' => $unit->status,
            'name_ar' => $unit_ar->name,
            'name_en' => $unit_en->name,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $unit = Unit::find($id);
        $unit->unit_type_id = $request->unit_type;
        $unit->status = $request->input('status', 0);
        $unit->save();

        $unit->translateOrNew('ar')->name = $request->name_ar;
        $unit->translateOrNew('en')->name = $request->name_en;
        $unit->save();

        return response()->json([
            'status' => 200,
            'unit' => $unit,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_price = ProductPrice::where('unit_id', $id)->count();
        if ($product_price > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }

        Unit::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Unit::count()
        ],
            200
        );
    }
}
