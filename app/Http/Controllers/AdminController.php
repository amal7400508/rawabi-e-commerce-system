<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Utilities\Helpers\Image;
use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $admins = Admin::all();
            return datatables()->of($admins)
                ->editColumn('updated_at', function (Admin $admin) {
                    return $admin->updated_at;
                })
                ->addColumn('action', function ($data) {
                    $span = '<span class="dropdown ">';
                    $span .= '<button id="btnSearchDrop2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-primary  dropdown-toggle dropdown-menu-right"><i class="ft-settings"></i></button>';
                    $span .= '<span aria-labelledby="btnSearchDrop2" class="dropdown-menu mt-1 dropdown-menu-right">';
                    $span .= '<button type="button" name="show" id="' . $data->id . '" class="show-admin dropdown-item"><i class="la la-eye"></i>' . lang::get("admin.show") . '</button>';
                    if (Gate::check('update admin')) {
                        $span .= '<a href="' . url("/admins_management/admin/edit") . '/' . $data->id . '"   class="dropdown-item"><i class="la la-pencil"></i>' . lang::get("admin.edit") . '</a>';
                    }
                    return $span;
                })->setRowAttr([
                    'style' => function (Admin $admin) {
                        if ($admin->status == 0)
                            return 'background-color: #fefafa;';

                    }
                ])
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('managements.admins_management.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('managements.admins_management.admin.create')
            ->with('role', Role::where('guard_name', 'admin')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|email|max:191|unique:admins,email',
            'phone' => 'required|numeric|max:999999999|min:999999',
            'status' => 'required|string|max:191',
            'password' => ['required', 'string', 'min:5', 'confirmed'],
        ]);
        if ($request->hasFile('image')) {
            $imageName = Image::reSizeImage($request->file('image'), 'storage/admins/', 'smart_delivery_admin_');
        }
        $admin = new Admin();
        if ($request->hasFile('image')) {
            $admin->image = $imageName;
        } else {
            $admin->image = 'avatar.jpg';
        }
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->gender = $request->input('gender', 'woman');
        $admin->phone = $request->phone;
        $admin->status = $request->status;
        $admin->password = bcrypt($request->password);
        $admin->save();
        $admin->syncRoles($request->role);
        return redirect('/admins_management/admin')->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role_admin = isset(Admin::find($id)->getRoleNames()[0]) ? Admin::find($id)->getRoleNames()[0] : null;
        $admins = Admin::find($id);
        return response()->json([
            'id' => $admins->id,
            'name' => $admins->name,
            'email' => $admins->email,
            'phone' => $admins->phone,
            'gender' => $admins->gender,
            'image' => $admins->image,
            'status' => $admins->status,
            'role_admin' => $role_admin,
            'updated_at' => Carbon::Parse($admins->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($admins->created_at)->format('Y/m/d'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role_admin = isset(Admin::find($id)->getRoleNames()[0]) ? Admin::find($id)->getRoleNames()[0] : null;
        return view('managements.admins_management.admin.edit')
            ->with('admin', Admin::find($id))
            ->with('role', Role::where('guard_name', 'admin')->get())
            ->with('role_admin', $role_admin);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Admin::find($id);
        $admin->syncRoles($request->role);
        if ($request->email == $admin->email) {
            $this->validate($request, [
                'name' => 'required|string|max:191',
                'phone' => 'required|numeric|max:999999999|min:999999',
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required|string|max:191',
                'email' => 'required|email|max:191|unique:admins,email',
                'phone' => 'required|numeric|max:999999999|min:999999',
            ]);
        }
        if ($admin->image != 'avatar.jpg') {
            Image::deleteImageFromFolder('storage/admins', $admin->image);
        }
        if ($request->hasFile('image')) {
            $imageName = Image::reSizeImage($request->file('image'), 'storage/admins/', 'moka_admin_');
            $admin->image = $imageName;
        } else
            $admin->image = 'avatar.jpg';
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->gender = $request->input('gender', 'woman');
        $admin->phone = $request->phone;
        $admin->status = $request->status;
        $admin->save();
        return redirect('/admins_management/admin')->with('success', Lang::get('admin.edited_successfully'));
    }
}
