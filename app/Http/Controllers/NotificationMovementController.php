<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\NotificationMovement;
use App\Models\User;
use App\Notifications\Admin\ProviderNotification;
use App\Utilities\Helpers\Fcm;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class NotificationMovementController extends Controller
{
    public $view_path = 'managements.notify_management.notification_movements.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = NotificationMovement::whereNull('branch_id')
            ->orderBy('id', 'desc');
        $view_name = 'index';

        return $this->display($data, $request, $view_name)
            ->with('notifications', Notification::all());
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notificationMovementBranches(Request $request)
    {
        $data = NotificationMovement::whereNotNull('branch_id')
            ->orderBy('is_approved')
            ->orderBy('id', 'desc');
        $view_name = 'notification_movement_branches';

        return $this->display($data, $request, $view_name)
            ->with('notifications', Notification::all());

    }

    public function display($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'branch') {
                $data = $data->whereHas('branch', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            }  else if ($search_type == 'title') {
                $data = $data->whereHas('notification', function ($q) use ($query) {
                    $q->whereTranslationLike('notification_title', '%' . $query . '%');
                });
            } else if ($search_type == 'content') {
                $data = $data->whereHas('notification', function ($q) use ($query) {
                    $q->whereTranslationLike('notification_message', '%' . $query . '%');
                });
            } else if ($search_type == 'type') {
                $data = $data->where('type', 'like', '%' . $query . '%');
            }
        endif;

        $data_count = $data->count();

        return view($this->view_path . $view_name)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notification_movements.create');
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function checkPhone(Request $request)
    {
        if (is_null($request->phone)) {
            return response()->json('<span class="error-message">' . __('validation.required') . '</span>');
        }

        $user = User::where('phone', $request->phone)->first();
        if ($user->id ?? null) {
            return response()->json(__('admin.this_phone_exists_by_name') . ' : ' . $user->full_name);
        }

        return response()->json('<span class="error-message">' . __('admin.this_phone_not_found') . '</span>');
    }

    public function validateAttention($request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'notification' => 'required|integer|exists:notifications,id',
            'type' => 'required|in:public,private',
            'phone' => [function ($attribute, $value, $fail) use ($request) {
                if ($request->type == 'private') {
                    if (is_null($value)) {
                        return $fail(lang::get("validation.required"));
                    }
                    $user_id = User::where('phone', $value)->first()->id ?? null;
                    if (is_null($user_id)) {
                        $fail(lang::get("admin.this_phone_not_found"));
                    }
                }
            }],
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        return $validatedData;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateAttention($request);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $image_name = null;
        if ($request->hasFile('image')) {
            $image_name = uploadImage($request->image, 'notification_movements');
        }

        $user_id = null;
        if ($request->type == 'private') {
            $user_id = User::where('phone', $request->phone)->first()->id;
        }

        $notification_movement = new NotificationMovement();
        $notification_movement->notification_id = $request->notification;
        $notification_movement->type = $request->type;
        $notification_movement->user_id = $user_id;
        $notification_movement->image = $image_name;
        $notification_movement->note = $request->note;
//        $notification_movement->is_approved = 1;
        $notification_movement->save();

        $image_path = $notification_movement->image_path;
        if (is_null($notification_movement->image)) {
            $image_path = null;
        }
        $fcm_title = $notification_movement->title;
        $fcm_body = $notification_movement->message;
        Fcm::send_MKA_notification($fcm_title, $fcm_body, $user_id, $image_path);

        return response()->json([
            'status' => 200,
            'notification_movement' => $notification_movement,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification_movement = NotificationMovement::find($id);

        return response()->json([
            'id' => $notification_movement->id,
            'branch' => $notification_movement->branch->name ?? null,
            'title' => $notification_movement->notification->notification_title ?? null,
            'content' => $notification_movement->notification->notification_message ?? null,
            'user_name' => $notification_movement->user->full_name ?? null,
            'type' => $notification_movement->type,
            'sending_date' => $notification_movement->approved_at,
            'note' => $notification_movement->note,
            'created_at' => Carbon::Parse($notification_movement->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($notification_movement->updated_at)->format('Y/m/d'),
            'image' => $notification_movement->image,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function accept(Request $request)
    {
        $id = $request->items_id;
        $notification_movement = NotificationMovement::find($id);

        if ($notification_movement->is_approved != 0 or is_null($notification_movement->branch_id)) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($notification_movement, $id) {
            $notification_movement->update(['is_approved' => 1, 'approved_at' => Carbon::now()->toDateTime()]);
            responseNotify($id, NotificationMovement::class);
            $notification_movement->notify(new ProviderNotification());

            $fcm_title = $notification_movement->title;
            $fcm_body = $notification_movement->message;
            $user_id = $notification_movement->user_id;
            $image_path = $notification_movement->image_path;
            if (is_null($notification_movement->image)) {
                $image_path = null;
            }

            Fcm::send_MKA_notification($fcm_title, $fcm_body, $user_id, $image_path);
        });

        return response()->json([
            'status' => 200,
            'data' => NotificationMovement::find($id),
            'title' => Lang::get('admin.accepted_successfully'),
            'message' => ''
        ]);
    }

    public function reject(Request $request)
    {
        $id = $request->items_id;
        $notification_movement = NotificationMovement::find($id);
        if ($notification_movement->is_approved != 0 or is_null($notification_movement->branch_id)) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($notification_movement, $id) {
            $notification_movement->update(['is_approved' => 3, 'approved_at' => Carbon::now()->toDateTime()]);
            responseNotify($id, NotificationMovement::class);
            $notification_movement->notify(new ProviderNotification());
        });

        return response()->json([
            'status' => 200,
            'data' => NotificationMovement::find($id),
            'title' => Lang::get('admin.rejected_successfully'),
            'message' => ''
        ]);
    }
}
