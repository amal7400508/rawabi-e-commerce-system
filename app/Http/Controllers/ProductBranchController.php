<?php

namespace App\Http\Controllers;

use App\Exports\FormProductsExport;
use App\Exports\ProductPricesExport;
use App\Models\Color;
use App\Models\Material;
use App\Utilities\Helpers\Image;
use App\Http\Middleware\Pharmacy;
use App\Http\Resources\FalterBranchCategoryResources;
use App\Http\Resources\falterUnitResources;
use App\Models\Branch;
use App\Models\BranchesCategory;
use App\Models\CartDetail;
use App\Models\Category;
use App\Exports\ProductsExport;
//use App\Models\HospitalityDetail;
use App\Http\Resources\ProductShowResource;
use App\Models\CategoryTranslation;
use App\Models\MainCategoryCategory;
use App\Models\PharmacyCategory;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductPrice;
use App\Models\ProductState;
use App\Models\ProductTranslation;
use App\Models\StatesProduct;
use App\Models\Unit;
use App\Models\UnitType;
use App\Models\User;
use App\Notifications\ProductNotification;
use App\Rules\CheckBranch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;

class ProductBranchController extends Controller
{
    public $view_path = 'managements.app.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new Product();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('number', '=', $query);
            } else if ($search_type == 'name') {
                $data = $data->whereTranslationLike('name', '%' . $query . '%');
            } else if ($search_type == 'category') {
                $data = $data->whereHas('category', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            } else if ($search_type == 'branch_name') {
                $data = $data->whereHas('branch', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            } else if ($search_type == 'image') {
                $data = $data->whereTranslationLike('image', '%' . $query . '%');
            } else if ($search_type == 'is_approved') {
                $data = $data->where('is_approved', '=', $query);
            }
        endif;

        $data = $data->orderBy('is_approved', 'asc')
            ->orderBy('id', 'desc');
        $data_count = $data->count();

        $branches = Branch::get();
        $categories = Category::get();
        $colors = Color::get();
        $materials = Material::get();
        return view($this->view_path . 'product.index')
            ->with('unit_types', UnitType::where('status', 1)->get())
            ->with('branches', $branches)
            ->with('colors', $colors)
            ->with('materials', $materials)
            ->with('categories', $categories)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );

    }

    function authProviderBranchCategories()
    {
        $branch_category = \App\Models\PharmacyCategory::all()->pluck('main_category')->toArray();

        $main_category_categories = MainCategoryCategory::whereIn('main_category_id', $branch_category)->get();

        return $main_category_categories;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function falterMainCategory($id)
    {
        $branch_category = PharmacyCategory::where('branch_id', $id)->pluck('main_category')->toArray();
        $main_category_categories = MainCategoryCategory::whereIn('main_category_id', $branch_category)->get();
        $data = FalterBranchCategoryResources::collection($main_category_categories);

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches = Branch::get();
        $categories = Category::get();
        $materials = Material::get();
        return view($this->view_path . 'product.create')
            ->with('categories', $categories)
            ->with('branches', $branches)
            ->with('materials', $materials);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'branch_id' => ['required'],
            'number' => [
                'required',
                'string',
                'max:191',
                Rule::unique('products')->where(function ($query) use ($request) {
                    return $query->where('number', $request->number)
                        ->where('branch_id', $request->branch_id);
                }),
            ],
            'ar_name' => 'required|string|max:191',
            'en_name' => 'required|string|max:191',
            'category_id' => 'required|string|max:191',
            'materials_id' => 'required|string|max:191',
            'image' => 'nullable|max:2048',
            'status' => 'required',
        ]);

        $imageName = 'default.jpg';
        if ($request->hasFile('image')) {
            $imageName = Image::reSizeImage($request->file('image'), 'storage/thumbnail', 'rawabi_telecom_product_');
        }

        DB::transaction(function () use ($request, $imageName) {
            $product = new Product();
            $product->branch_id = $request->branch_id;
            $product->number = $request->number;
            $product->status = $request->status;
            $product->is_new = $request->input('is_new', 0);

            $product->category_id = $request->category_id;
            $product->materials_id = $request->materials_id;
            $product->save();

            $product->translateOrNew('ar')->name = $request->ar_name;
            $product->translateOrNew('ar')->details = $request->ar_details;
            $product->translateOrNew('ar')->image = $imageName;

            $product->translateOrNew('en')->name = $request->en_name;
            $product->translateOrNew('en')->details = $request->en_details;
            $product->translateOrNew('en')->image = $imageName;

            $product->save();

//            $product->notify(new ProductNotification($request->all()));
            /*foreach ($request->city as $city) {
                $states_product = new ProductState();
                $states_product->product_id = $product->id;
                $request->city = 'sana_a';
                if ($city == 'sana_a') {
                    $states_product->state_id = 1;
                }
                elseif ($city == 'ibb') {
                    $states_product->state_id = 2;
                }
                $states_product->save();
            }*/

        });

        return redirect('app/product')->with('success', Lang::get('admin.added_successfully'));
    }

    public function addFromExcel(Request $request)
    {
        if ($request->type == 'product') {
            $validatedData = Validator::make(
                $request->all(), [
                'branch_id' => ['required'],
                'file_products' => 'required|max:50000|mimes:xlsx',
            ]);

            if ($validatedData->fails())
                return response()->json(['error' => $validatedData->errors()], 401);

            $products = (new FastExcel)->import(request()->file('file_products'), function ($line) use ($request) {
                $product = new Product();
                $product->branch_id = $request->branch_id;
                $product->number = $line['number'];
                $product->status = 1;
                $product->is_new = 0;
                $product->category_id = $line['category_id'];
                $product->save();

                $product->translateOrNew('ar')->name = $line['name_ar'];
                $product->translateOrNew('ar')->details = $line['details_ar'];
                $product->translateOrNew('en')->name = $line['name_en'];
                $product->translateOrNew('en')->details = $line['details_en'];
                $product->save();

//                $product->notify(new ProductNotification());

                return $product;
            });
        } elseif ($request->type == 'Product_pricing') {
            $validatedData = Validator::make(
                $request->all(), [
                'branch_id' => ['required'],
                'file_products' => 'required|max:50000|mimes:xlsx',
            ]);

            if ($validatedData->fails())
                return response()->json(['error' => $validatedData->errors()], 401);

            $products = (new FastExcel)->import(request()->file('file_products'), function ($line) use ($request) {
                foreach ($line as $id) {
                    $product_price = ProductPrice::find($id);
//                    $product_price->id = $line['id'];
                    $product_price->product_id = $line['الرقم المنتج'];
                    $product_price->price = $line['السعر'];
                    $product_price->note_ar = $line['الملاحظات عربي'];
                    $product_price->note_en = $line['الملاحظات انجليزي'];
                    if ($line['متاح للضيافة'] == 'نشط')
                        $order_availability = 1;
                    elseif ($line['متاح للضيافة'] == 'موقف')
                        $order_availability = 0;
                    $product_price->order_availability = $order_availability;
//                    $product_price->unit_id = $line['الرقم الوحدة'] ?? null;
                    $product_price->update();

                    return $product_price;
                }

            });
        }
        return response()->json([
            'message' => __('admin.created_successfully'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $product = Product::find($id);
//        $provider = $product->branch->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()) {
//            abort(403);
//        }

        $cart_detail = $this->totalDoneRequestsOrders(
            $request->start_date,
            $request->end_date
        );

        $data = ProductPrice::joinSub($cart_detail, 'cart_detail', function ($join) {
            $join->on('product_prices.id', '=', 'cart_detail.product_price_id');
        })
            ->where('product_prices.product_id', $id)
            ->select('product_prices.*', 'product_count')
            ->get();


        $data = ProductPrice::where('product_id', $id)->get();
        $product_price = ProductShowResource::collection($data);


        return response()->json([
            'id' => $product->id,
            'name' => $product->name ?? null,
            'details' => $product->details,
            'taste' => $product->taste,
            'created_at' => Carbon::Parse($product->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($product->updated_at)->format('Y/m/d'),
            'is_new' => $product->is_new,
            'type' => $product->type,
            'category_translations_name' => $product->category->name ?? null,
            'status' => $product->status,
            'image' => $product->image,
            'branch_name' => $product->branch->name,
            'product_price' => $product_price,
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        $product = Product::find($id);
        $product_ar = ProductTranslation::where('product_id', '=', $id)->where('locale', 'ar')->first();
        $product_en = ProductTranslation::where('product_id', '=', $id)->where('locale', 'en')->first();

        return response()->json([
            'id' => $product->id,
            'status' => $product->status,
            'name_ar' => $product_ar->name,
            'name_en' => $product_en->name,
            'details_ar' => $product_ar->details,
            'details_en' => $product_en->details,
            'is_new' => $product->is_new,
            'number' => $product->number,
            'branch_id' => $product->branch_id,
            'category_id' => $product->category_id,
            'materials_id' => $product->materials_id,
            'is_approved' => $product->is_approved,
            'image' => is_null($product->image) ? null : asset('storage/thumbnail/150') . '/' . $product->image,
        ]);


        $product = Product::find($id);
//        $provider = $product->branch->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()) {
//            abort(403);
//        }

        $branch_ids = authProviderBranchesIds();
//        $product = Product::where('id', $id)->first();

        $product_ar = DB::table('product_translations')
            ->join('products', 'product_translations.product_id', '=', 'products.id')
            ->where('product_translations.locale', '=', 'ar')
            ->whereNull('products.deleted_at')
            ->where('products.id', '=', $id)
            ->select('product_translations.*', 'products.updated_at', 'products.number', 'products.status', 'products.category_id',
                'products.is_new')
            ->get()->first();
        /*$product_ar = DB::table('product_translations')
            ->join('products', 'product_translations.product_id', '=', 'products.id')
            ->where('product_translations.locale', '=', 'ar')
            ->whereNull('products.deleted_at')
            ->where('products.id', '=', $id)
            ->select('product_translations.*', 'products.updated_at', 'products.number', 'products.status', 'products.category_id',
                'products.is_new', 'products.type')
            ->get()->first();*/
        $product_en = ProductTranslation::where('product_id', '=', $id)->where('locale', 'en')->first();
        $states = Product::find($id)->states->pluck('id')->toArray();
        $branches = Branch::get();
        return view('product.edit', array('product_ar' => $product_ar, 'product_en' => $product_en))
            ->with('category_translation', CategoryTranslation::all())
            ->with('states', $states)
            ->with('products', $product)
            ->with('branches', $branches);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public
    function update(Request $request, Product $product)
    {
//        $provider = $product->branch->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()) {
//            abort(403);
//        }

        $number = $request->number;
        $branch_id = $request->branch_id;
        if ($product->number != $request->number) {
            $messages = [
                'number.unique' => 'هذا الرمز مستخدم بالفعل.',
            ];

            $this->validate($request, [
                'number' => [
                    'required',
                    Rule::unique('products')->where(function ($query) use ($number, $branch_id) {
                        return $query->where('number', $number)
                            ->where('branch_id', $branch_id);
                    }),
                ],
            ],
                $messages
            );
        }

        $this->validate($request, [
            'ar_name' => 'required|string|max:191',
            'en_name' => 'required|string|max:191',
            'category_id' => 'required|string|max:191',
            'image' => 'nullable|max:2048',
//            'city' => 'required',
//            'city.*' => 'required|in:sana_a,ibb',
            'status' => 'required',
        ]);

        $imageName = $product->image;
        if ($request->hasFile('image')) {
            $file_path = 'storage/thumbnail';
            $imageName = Image::reSizeImage($request->file('image'), $file_path, 'rawabi_telecom_product_');
            $folders_name = ['/640/', '/360/', '/150/', '/64/'];
            $image_name_store = ProductTranslation::where('product_id', '=', $product->id)->get()->first()->image;
            for ($i = 0; $i < count($folders_name); $i++) {
                $filename = public_path() . '/' . $file_path . $folders_name[$i] . $image_name_store;
                File::delete($filename);
            }
        }
        DB::transaction(function () use ($request, $imageName, $product, $number) {
            $product->branch_id = $request->branch_id;
            $product->number = $number;
            $product->status = $request->status;
            $product->is_new = $request->input('is_new', 0);
            $product->category_id = $request->category_id;
            $product->materials_id = $request->materials_id;
            $product->update();

            $product->translateOrNew('ar')->name = $request->ar_name;
            $product->translateOrNew('ar')->details = $request->ar_details;
//                if ($request->hasFile('image')) {
            $product->translateOrNew('ar')->image = $imageName;
//                }

            $product->translateOrNew('en')->name = $request->en_name;
            $product->translateOrNew('en')->details = $request->en_details;
//                if ($request->hasFile('image')) {
            $product->translateOrNew('en')->image = $imageName;
//                }
            $product->update();


            /*foreach ($request->city as $city) {
                $states_product = new ProductState();
                $states_product->product_id = $product->id;
                if ($city == 'sana_a') {
                    $states_product->state_id = 1;
                } elseif ($city == 'ibb') {
                    $states_product->state_id = 2;
                }
                $states_product->$this->update();
            }*/
        });

        return response()->json([
            'status' => 200,
            'product' => $product,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
//        return redirect('app/product')->with('success', Lang::get('admin.edited_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
//        $product = Product::findOrFail($id);
//        $provider = $product->branch->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()) {
//            abort(403);
//        }

        $carts_detail = 0;
        $hospitality_detail = 0;
        $product_price = ProductPrice::withTrashed()->where('product_id', $id)->get();

        foreach ($product_price as $product_prices) {
            $carts_detail += CartDetail::where('product_price_id', $product_prices->id)->count();
//            $hospitality_detail += HospitalityDetail::where('product_price_id', $product_prices->id)->count();
        }

        if ($hospitality_detail > 0 || $carts_detail > 0) {
            return response()->json([
                'error_deleting' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        $product = Product::findOrFail($id);
        $product->delete();
    }

    /**
     * delete all checked the products
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAll(Request $request)
    {
        $items_id = $request->items_id;

        foreach ($items_id as $id) {
            $carts_detail = 0;
            $hospitality_detail = 0;
            $product_price = ProductPrice::withTrashed()->where('product_id', $id)->get();

            foreach ($product_price as $product_prices) {
                $carts_detail += CartDetail::where('product_price_id', $product_prices->id)->count();
            }

            if ($hospitality_detail > 0 || $carts_detail > 0) {
                return response()->json([
                    'message' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links'),
                ], 401);
            }
        }

        $products = Product::whereIn('id', (array)$items_id);
        $result = $products->delete();
        if ($result == 0) {
            $result = 'عذراً. لم يتم تحديد اي عنصر ';
            return response()->json(['message' => $result], 401);
        }
        $result = ' تم حذف ' . $result . ' عنصر بنجاح.';
        return response()->json(['message' => $result], 200);
    }

    /**
     *  displays the deleted data as an initial deletion
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public
    function recycle_bin()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('product_translations')
                ->join('products', 'product_translations.product_id', '=', 'products.id')
                ->join('category_translations', 'category_translations.category_id', '=', 'products.category_id')
                ->where('product_translations.locale', '=', $locale)
                ->where('category_translations.locale', '=', $locale)
                ->whereNotNull('products.deleted_at')
                ->select('product_translations.*', 'products.status', 'products.number', 'products.category_id', 'products.is_new', 'category_translations.name as category_translations_name')
                ->get();
            /*$dataselect = DB::table('product_translations')
                ->join('products', 'product_translations.product_id', '=', 'products.id')
                ->join('category_translations', 'category_translations.category_id', '=', 'products.category_id')
                ->where('product_translations.locale', '=', $locale)
                ->where('category_translations.locale', '=', $locale)
                ->whereNotNull('products.deleted_at')
                ->select('product_translations.*', 'products.status', 'products.number', 'products.category_id', 'products.is_new', 'category_translations.name as category_translations_name', 'products.type')
                ->get();*/
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="restore" id="' . $data->product_id . '" class="restore btn btn-primary btn-sm" data-toggle="Restore" title="Restore"><i class="ft ft-rotate-cw"></i></button>';
                    $button .= '&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->product_id . '" class="delete btn btn-danger btn-sm" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('product.recycle_bin');
    }

    /**
     * retrieves deleted data from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function restore($id)
    {
        $product = Product::withTrashed()
            ->where('id', $id)->first();

//        $provider = $product->branch->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()) {
//            abort(403);
//        }

        $product->restore();
        return response()->json();
    }

    /**
     * performs the final deletion from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function hdelete($id)
    {
//        $product = Product::withTrashed()->where('id', $id)->first();
//        $provider = $product->branch->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()) {
//            abort(403);
//        }

        $file_path = 'storage/thumbnail';
        $folders_name = ['/640/', '/360/', '/150/', '/64/'];
        $image_name_store = ProductTranslation::where('product_id', '=', $id)->get()->first()->image;
        for ($i = 0; $i < count($folders_name); $i++) {
            $filename = public_path() . '/' . $file_path . $folders_name[$i] . $image_name_store;
            File::delete($filename);
        }

        $product_price = ProductPrice::withTrashed()->where('product_id', $id);
        $product_price->forceDelete();
        DB::table('product_translations')
            ->where('product_id', '=', $id)
            ->delete();
        $product = Product::withTrashed()->where('id', $id)->first();
        $product->forceDelete();

        return response()->json();

    }

    public
    function falter($id)
    {
        $unit = Unit::where('unit_type_id', $id)->get();
        $data = FalterUnitResources::collection($unit);

        return response()->json($data);
    }

    public
    function report(Request $request)
    {
        if (request()->ajax()) {
            $data = $this->getReport($request);
            return datatables()->of($data)
                ->editColumn('category_name', function ($data) {
                    return $data->category->name;
                })
                ->editColumn('most_product', function ($data) use ($request) {
                    return $this->mostProduct(
                        $data->id,
                        $request->from_date_requests_number,
                        $request->to_date_requests_number);
                })
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" id="' . $data->id . '"  class="showB btn btn-primary btn-sm" data-toggle="show" title="' . Lang::get("admin.show") . '"><i class="ft ft-eye"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('product.report')
            ->with('categories', Category::where('status', 1)->get());
    }

    /**
     * Fetch reports according to conditions
     *
     * @param $request
     * @return mixed
     */
    public
    function getReport($request)
    {
        $locale = app()->getLocale();

        if ($request->confirm == 1) {
            $data = new Product();
            if (!is_null($request->from)) {
                $data = $data->whereDate('products.created_at', '>=', $request->from);
            }
            if (!is_null($request->to)) {
                $data = $data->whereDate('products.created_at', '<=', $request->to);
            }
            if (!is_null($request->name)) {
                $data = $data->join('product_translations', 'products.id', '=', 'product_translations.product_id')
                    ->where('product_translations.locale', '=', $locale)
                    ->where('product_translations.name', 'like', '%' . $request->name . '%');
            }
            if (!is_null($request->id)) {
                $data = $data->where('products.number', $request->id);
            }
            if (!is_null($request->category)) {
                $data = $data->where('products.category_id', $request->category);
            }
            /*if (!is_null($request->type)) {
                $data = $data->where('products.type', $request->type);
            }*/
            if (!is_null($request->status)) {
                $data = $data->where('products.status', $request->status);
            }
            if ($request->is_new == 1) {
                $data = $data->where('products.is_new', 1);
            }
            return $data->select('products.*')->get();
        }

        return Product::where('id', 0)->get();
    }

    public
    function export(Request $request)
    {
        $request->request->add(['confirm' => 1]);
        $data = $this->getReport($request);
        return Excel::download(
            new ProductsExport($data),
            'products-' . now() . '.xlsx'
        );
    }

    /**
     * @param $product_id int product id
     * @param $start_date
     * @param $end_date
     * @return int sum requests number
     */
    public
    function mostProduct(
        $product_id,
        $start_date = null,
        $end_date = null
    )
    {
        $cart_detail = $this->totalDoneRequestsOrders($start_date, $end_date);
        $product_prices = ProductPrice::joinSub($cart_detail, 'cart_detail', function ($join) {
            $join->on('product_prices.id', '=', 'cart_detail.product_price_id');
        })
            ->where('product_prices.product_id', $product_id)
            ->select('product_count')
            ->get();

        $product_count = 0;
        foreach ($product_prices as $product_price) {
            $product_count = $product_count + $product_price->product_count;
        }
        return $product_count;
    }

    public
    function totalDoneRequestsOrders($start_date, $end_date)
    {
        $cart_detail = CartDetail::groupBy('product_price_id')
            ->join('requests', 'cart_details.cart_id', 'requests.cart_id')
            ->where(function ($q) {
                $q->where('status', 'on_branch')
                    ->orWhere('status', 'done');
            })
            ->select('cart_details.product_price_id',
                DB::raw('sum(cart_details.quantity) as product_count'
                )
            );

        if (!is_null($start_date)) {
            $cart_detail = $cart_detail->whereDate('requests.created_at', '>=', $start_date);
        }
        if (!is_null($end_date)) {
            $cart_detail = $cart_detail->whereDate('requests.created_at', '<=', $end_date);
        }

        return $cart_detail;
    }

    function upload(Request $request)
    {
        $product = Product::find($request->_id);
//        $provider = $product->branch->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()) {
//            abort(403);
//        }

        $image = $request->file('file');
        $imageName = 'product_image_' . rand(10, 100) . '_' . time() . '.' . $image->extension();
        $image->move(public_path('storage/product_image'), $imageName);
        $product_image = new ProductImage();
        $product_image->image = $imageName;
        $product_image->product_id = $product->id;
        $product_image->save();

        return response()->json(['success' => $imageName]);
    }

    function fetch($id)
    {
        $product = Product::find($id);
//        $provider = $product->branch->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()) {
//            abort(403);
//        }

        $images = $product->productImage;
        $output = '<div class="row">';
        foreach ($images as $image) {
            $output .= '
            <div class="col-md-3" style="margin-bottom:16px;" align="center">
                <img src="' . $image->image_path . '" class="img-thumbnail" width="175" height="175" style="height:175px;" />
                <a class="btn btn-sm btn-danger btn-round remove_image" data-id="' . $image->id . '" data-path="' . $image->path . '"><i  class="ft-trash-2"></i></a>
            </div>';
        }
        $output .= '</div>';
        echo $output;
    }

    function delete(Request $request)
    {
        $id = $request->id; //Crypt::decryptString($request->id);
        $product_image = ProductImage::find($id);

//        $provider = $product_image->product->branch->provider ?? null;
//        if (setProviderID($provider) != getParentProviderId()) {
//            abort(403);
//        }

        deleteImage('/storage/product_image/' . $product_image->image);
        $product_image->delete();

        return response()->json($product_image->product->id);
    }

    /**
     * export_excel_form_to_enter_products
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Exception
     */
    public
    function exportExcelFormProducts(Request $request)
    {
        return Excel::download(
            new FormProductsExport(),
            'export_excel_form_to_enter_products' . '.xlsx'
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public
    function exportExcelFormProductPrices(Request $request)
    {
        $data_product = Product::where('branch_id', $request->branch_id_price)->pluck('id');
        $data = ProductPrice::whereIn('product_id', $data_product)->get();
        return Excel::download(
            new ProductPricesExport(
                $data
            ),
            'product_prices-' . now() . '.xlsx'
        );
    }
}
