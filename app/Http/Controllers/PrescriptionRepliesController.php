<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Branch;
use App\Models\Carrier;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Coupon;
use App\Models\InsuranceCompany;
use App\Models\Offer;
use App\Models\Prescription;
use App\Models\PrescriptionReplies;
use App\Models\PrescriptionReply;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\SpecialProduct;
use App\Models\SystemNotification;
use App\Models\User;
use App\WeddingCard;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Request as RequestPost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class PrescriptionRepliesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = Prescription::whereNotNull('insurance_company_id')
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('user_name', function ($data) {
                    return $data->user->name;
                })
                ->editColumn('insurance_company_name', function ($data) {
                    return $data->insuranceCompany->name;
                })
                ->editColumn('phone', function ($data) {
                    return $data->user->phone;
                })
                ->editColumn('insurance_company', function ($data) {
                    return $data->insuranceCompany->name ?? null;
                })
                ->addColumn('action', function ($row) {
                    $btn = '<a class="showB" title="عرض" id="' . $row->id . '"><i class="la la-eye mr-1"></i></a>';
                    /*$btn .= '<a class="createPriceClick primary mr-1" title="تعديل" id="' . $row->id . '"><i class="la la-pencil"></i></a>';*/
                    return $btn;
                })
                ->make(true);
        }
        return view('managements.requests_management.prescription-replies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $wedding_card = new WeddingCard();
        $wedding_card->grooms_name = 'المهندس : محب مطهر الحواني';
        $wedding_card->nickname = '#أبو_إحسان';
        $wedding_card->wedding_date = '30/7/2021';
        $wedding_card->wedding_day = 'يوم الجمعة';
        $wedding_card->Wedding_hall = 'قاعة المفرج - البونية - أمام الخدمة المدنية';
        $wedding_card->the_wedding = 'أمام منزلنا - حي القاع - خلف بيت السلال';
        $wedding_card->note = 'أهلا وسهلا بالجميع أرحبو فوق العين والرأس';
        $wedding_card->save();

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\PrescriptionReplies $prescriptionReplies
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prescription = Prescription::whereNotNull('insurance_company_id')
            ->where('id', $id)
            ->first();

        return response()->json([
            'id' => $prescription->id,
            'user_name' => $prescription->user->name,
            'phone' => $prescription->user->phone,
            'insurance_company_number' => $prescription->insurance_company_number ?? '',
            'insurance_company_number_path' => $prescription->insurance_company_number_path ?? '',
            'prescription_image' => $prescription->prescription_image,
            'note' => $prescription->note,
            'created_at' => Carbon::Parse($prescription->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($prescription->updated_at)->format('Y/m/d'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\PrescriptionReplies $prescriptionReplies
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prescription_replies = Prescription::whereNotNull('insurance_company_id')
            ->where('id', $id)
            ->first();


        if (!is_null($prescription_replies->is_insurance_company_approved))
            return response()->json([
                'error' => Lang::get('admin.error')
            ], 401);

        return response()->json([
            'id' => $prescription_replies->id,
            'price' => $prescription_replies->price,
            'is_insurance_company_approved' => $prescription_replies->is_insurance_company_approved,
            'insurance_rate' => $prescription_replies->insurance_rate,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\PrescriptionReplies $prescriptionReplies
     * @param $id
     * @return void
     */
    public function update(Request $request, PrescriptionReplies $prescriptionReplies, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'insurance_rate' => ['nullable', 'numeric', 'min:1', 'max:100']
        ]);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $prescription_replies = Prescription::whereNotNull('insurance_company_id')
            ->where('id', $id)
            ->first();


        $prescription_replies->is_insurance_company_approved = $request->is_insurance_company_approved;
        $prescription_replies->insurance_rate = $request->insurance_rate;
        $prescription_replies->update();

        // response_at
        $app_notify = SystemNotification::where('notifiable_type', 'App\V1\Models\prescription')
            ->where('type', '!=', 'App\Notifications\Admin\ProviderNotification')
            ->where('notifiable_id', $id);

        if (!is_null($app_notify->first()->data ?? null)) {
            $data_notify = $app_notify->first()->data;
            $app_notify->update([
                'response_at' => Carbon::now()->toDateTime(),
                'data' => json_encode([
                    'id' => $data_notify['id'],
                    'user_id' => $data_notify['user_id'],
                    'branch_id' => $data_notify['branch_id'],
                    'provider_id' => $data_notify['provider_id'],
                    'insurance_provider_id' => $data_notify['insurance_provider_id'],
                    'status' => true
                ])
            ]);
        }
        return response()->json([
            'success' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\PrescriptionReplies $prescriptionReplies
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrescriptionReplies $prescriptionReplies)
    {
        //
    }

    /**
     * Show the form for index a new resource.
     *
     * @param RequestPost $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orderTracking(RequestPost $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;

        // insurance id where provider_id(auth)
        $insurance_id = InsuranceCompany::pluck('id')->toArray();
        // prescription id
        $prescription_ids = Prescription::whereIn('insurance_company_id', $insurance_id)->pluck('id')->toArray();
        // reply id
        $prescription_replies_ids = PrescriptionReplies::whereIn('prescription_id', $prescription_ids)->pluck('id')->toArray();
        // cart_details get cart_id
        $cart_details_ids = CartDetail::whereIn('prescription_reply_id', $prescription_replies_ids)->pluck('cart_id')->toArray();
        // request

        $request_data = \App\Models\Request::join('carts', 'requests.cart_id', 'carts.id')
            ->join('users', 'carts.user_id', 'users.id')
            ->whereIn('carts.id', $cart_details_ids);
//dd($request_data->get());
        if (!is_null($request['filter_name'])):
            if ($request['filter_name'] == 'requested') {
                $request_data = $request_data->where('requests.status', '=', 'requested');
            } else if ($request['filter_name'] == 'received') {
                $request_data = $request_data->where('requests.status', '=', 'received');
            } else if ($request['filter_name'] == 'repair') {
                $request_data = $request_data->where('requests.status', '=', 'repair');
            } else if ($request['filter_name'] == 'deliver') {
                $request_data = $request_data->where('requests.status', '=', 'deliver');
            } else if ($request['filter_name'] == 'delivered') {
                $request_data = $request_data->where('requests.status', '=', 'delivered');
            } else if ($request['filter_name'] == 'canceled') {
                $request_data = $request_data->where('requests.status', '=', 'canceled');
            }
        endif;
        if (!is_null($request['search_type']) && !is_null($request['query'])):
            if ($request['search_type'] == 'number') {
                $request_data = $request_data->where('requests.request_number', '=', $request['query']);
            } else if ($request['search_type'] == 'name') {
                $request_data = $request_data->where('users.name', 'like', '%' . $request['query'] . '%');
            } else if ($request['search_type'] == 'phone') {
                $request_data = $request_data->where('users.phone', '=', $request['query']);
            }
        endif;

        $request_data = $request_data->select('requests.*')
            ->orderBy('requests.updated_at', 'desc');

        $tracking_request_count = $request_data->count();
        return view('managements.requests_management.prescription-replies.order_tracking')
            ->with('tracking_requests', $request_data->paginate($table_length))
            ->with('tracking_request_count', $tracking_request_count)
            ->with('pagination_links', [
                    'filter_name' =>
                        isset($_GET['filter_name']) ? $_GET['filter_name'] : '',
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * show details request from branch
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showDetailsRequestFromBranch($id)
    {
//        if (Gate::check('show request')
//            or Gate::check('show request_branch')
//            or Gate::check('show tracking')) {

        $request = \App\Models\Request::find($id);
        $branch = isset(Branch::find($request->branch_id)->name) ?
            Branch::find($request->branch_id)->name : '';
        $cart_id = $request->cart_id;
        $cart = Cart::find($cart_id)->user_id;
        $user = User::find($cart);
        $cart_detail = CartDetail::where('cart_id', $cart_id)->get();
        $sum = 0;
        $price_sum = 0;
        $product[] = '';
        $price[] = '';
        $unit[] = '';
        $cart_detail_id[] = '';
        $total_quantity[] = '';
        $product_number[] = '';
        $request_type[] = '';
        $product_discount_id[] = '';
        foreach ($cart_detail as $cart_details) {
            $discount = 0;
            if (!is_null($cart_details->product_discount_id)
                && $cart_details->product_price_id == $cart_details->productDiscount->product_price_id
            ) {
                $discount = $cart_details->productDiscount->value;
                $discount = ($cart_details->productDiscount->value_type == 'percentage') ?
                    $discount . ' %' : $discount . ' YER';
            }

            $product_discount_id[] .= $discount;

            $sum = $sum + $cart_details->quantity;
            $product_price = ProductPrice::find($cart_details->product_price_id);
            $request_type[] .= $cart_details->item_type;
            if ($cart_details->item_type == 'offer') {
                $offer = Offer::find($cart_details->offer_id);
                $price[] .= $offer->price;
                $price__ = $offer->price;
                $unit[] .= ' ';
                $product_number[] .= $offer->id;
                $product[] .= $offer->name;
            } elseif ($cart_details->item_type == 'product') {
                $product_row = Product::find($product_price->product_id);
                $price[] .= $product_price->price;
                $unit[] .= $product_price->unit->name ?? ' ';
                $product_number[] .= $product_row->number;
                $price__ = $product_price->price;
                $product[] .= $product_row->name;
            } elseif ($cart_details->item_type == 'special') {
                $SpecialProduct = SpecialProduct::find($cart_details->special_product_id);
                $price[] .= $SpecialProduct->price;
                $price__ = $SpecialProduct->price;
                $unit[] .= ' ';
                $product_number[] .= $SpecialProduct->id;
                $product[] .= $SpecialProduct->name;
            } elseif ($cart_details->item_type == 'prescription_reply') {
                $prescription_reply = PrescriptionReply::find($cart_details->prescription_reply_id);
                $price[] .= $prescription_reply->medicine_price ?? 0;
                $price__ = $prescription_reply->medicine_price ?? 0;
                $unit[] .= ' ';
                $product_number[] .= $prescription_reply->id ?? '';
                $product[] .= $prescription_reply->medicine_name ?? '';
            }
            $quantity_total = $cart_details->quantity;
            $cart_detail_id[] .= $cart_details->id;
            if ($cart_details->item_type == 'hospitality') {
                $quantity_total = 1;
            }
//
//                $total_quantity[] .= $price__ * $quantity_total;
//                $price_sum = $price_sum + ($price__ * $quantity_total);

            if (!is_null($cart_details->product_discount_id)
                && $cart_details->product_price_id == $cart_details->productDiscount->product_price_id
            ) {
                $discount = $cart_details->productDiscount->value;
                $price__ = ($cart_details->productDiscount->value_type == 'percentage')
                    ? $price__ - (($discount / 100) * $price__)
                    : max(($price__ - $discount), 0);
            }
            $total_quantity[] .= $price__ * $quantity_total;
            $price_sum = $price_sum + ($price__ * $quantity_total);
        }
        $coupon = null;
        $coupon_value_type = null;
        $remaining_amount = $price_sum;

        if ($request->coupon_id != null) {
            $coupons = Coupon::find($request->coupon_id) ?? exit();
            $coupon_value_type = $coupons->value_type;
            $coupon = $coupons->price;
            $remaining_amount = ($coupons->value_type == 'percentage')
                ? $price_sum - (($coupon / 100) * $price_sum)
                : max(($price_sum - $coupon), 0);

//                $coupon = Coupon::find($request->coupon_id)->price ?? 0;
//                $remaining_amount = $coupon > $price_sum ? 0 : $price_sum - $coupon;
        }

        // volume_discount
        $discount_value = null;
        $discount_value_type = null;
        if ($request->volume_discount_id != null) {
            $volume_discount = \App\VolumeDiscount::find($request->volume_discount_id) ?? null;
            $discount_value_type = $volume_discount->value_type ?? null;
            $discount_value = $volume_discount->value ?? null;
            if (!is_null($discount_value) && !is_null($discount_value_type)) {
                $remaining_amount = ($discount_value_type == 'percentage')
                    ? $remaining_amount - (($discount_value / 100) * $remaining_amount)
                    : max(($remaining_amount - $discount_value), 0);
            }
        }

        $city_of_request = "<span class='category-color'>" . __('admin.no_data') . "</span>";
        if (!is_null($request->state_id)) {
            $city_of_request = $request->state->name;
        }
        return response()->json([
            'city_of_request' => $city_of_request,
            'discount_value' => $discount_value,
            'discount_value_type' => $discount_value_type,
            'coupon_value_type' => $coupon_value_type,
            'product_discount_id' => $product_discount_id,
            'cart_detail_id' => $cart_detail_id,
            'total_quantity' => $total_quantity,
            'product_number' => $product_number,
            'unit' => $unit,
            'note' => $request->note,
            'cancel_note' => $request->cancel_note,
            'updated_by' => is_null($request->updated_by)
                ? isset($request->carrier->name)
                    ? " ( " . __('admin.carrier') . " ) " . $request->carrier->name
                    : null
                : " ( " . __('admin.admin') . " ) " . Admin::find($request->updated_by)->name,
            'coupon' => $coupon,
            'remaining_amount' => $remaining_amount,
            'receiving_type' => $request->receiving_type,
            'user_name' => $user->name,
            'carrier' => isset($request->carrier->name) ? $request->carrier->name : '',
            'request_type' => $request_type,
            'branch' => $branch,
            'address' => isset($request->address->name) ? $request->address->name : '',
            'desc' => isset($request->address->desc) ? $request->address->desc : '',
            'price_sum' => $price_sum,
            'sum' => $sum,
            'payment_type' => $request->payment_type,
            'cache_payment' => $request->cache_payment,
            'request_no' => $request->request_number,
            'request_id' => $request->id,
            'cart_detail' => $cart_detail,
            'product' => $product,
            'product_price' => $price,
            'created_at' => Carbon::Parse($request->created_at)->format('Y/m/d  H:i:s'),
            'created_at_diffForHumans' => $request->created_at->diffForHumans()
        ]);
    }

}
