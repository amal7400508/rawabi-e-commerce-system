<?php

namespace App\Http\Controllers;

use App\Models\MainCategory;
use App\Models\MainCategoryTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class MainCategoryController extends Controller
{

    public $view_path = 'managements.encoding.main-categories.';
    public $public_storage_path = 'main-categories';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view_path . 'index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function treeBuilding()
    {
        $rank = MainCategory::select(DB::raw('count(*) as ranks_count, ranks'))
//            ->where('status', 1)
            ->groupBy('ranks')
            ->get();

        $i = 0;
        $category = [];

        foreach ($rank as $ranks) {
            $category[$i] = MainCategory::/*where('status', 1)->*/where('ranks', $ranks->ranks)->get()->toArray();
            $i++;
        }

        return response()->json([
            'category' => $category
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (MainCategory::find($request->main_category_id)->branches_count ?? 0 != 0) {
            return response()->json([
                'message' => 'error: 301'
            ], 401);
        }

        $validatedData = $this->validationCategory($request);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        if ($request->category_id == 0 && is_null($request->image)) {
            return response()->json([
                'error' => [
                    'image' => 'يجب اضافة صورة لهذا التصنيف.'
                ]
            ], 401);
        }

        $category = new MainCategory();
        if ($request->hasFile('image')) {
            $category->image = uploadImage(
                $request->file('image'),
                $this->public_storage_path
            );
        }

        $category->ranks = $request->ranks + 1;
        $category->status = $request->input('status', 0);
        $category->level = $request->level;
        if ($request->main_category_id != 0) {
            $category->main_category_id = $request->main_category_id;
        }
        $category->save();

        $category->translateOrNew('ar')->name = $request->category_name_ar;
        $category->translateOrNew('en')->name = $request->category_name_en;
        $category->save();

        return response()->json([
            'request' => $request->all(),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $category = MainCategoryTranslation::where('main_category_id', $id)
            ->where('locale', 'ar')
            ->first();

        $name_en = MainCategoryTranslation::where('main_category_id', $id)
            ->where('locale', 'en')
            ->first()->name ?? null;

        $image = asset('storage/main-categories') . '/'
            . $category->mainCategory->image;

        return response()->json([
            'name_ar' => $category->name,
            'name_en' => $name_en,
            'image' => $image,
            'level' => $category->mainCategory->level,
            'status' => $category->mainCategory->status,
        ], 200);
    }

    /**
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validationCategory($request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'category_name_ar' => 'required|string|max:191',
            'category_name_en' => 'required|string|max:191',
            'level' => 'required|int|max:10000',
            'ranks' => 'required|string|max:191',
            'main_category_id' => 'numeric',
            'image' => ['image', 'mimes:jpeg,png,jpg,gif', 'max:2048']
        ]);

        return $validatedData;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedData = $this->validationCategory($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $status = is_null($request->input('status', 0))
            ? 1
            : $request->input('status', 0);

        $category = MainCategory::find($request->id);
        $category->status = $status;
        $category->level = $request->level;

        if ($request->hasFile('image')) {
            deleteImage('/storage/' . $this->public_storage_path . '/' . $category->image);
            $category->image = uploadImage($request->file('image'), $this->public_storage_path);
        }
        $category->update();

        $category->translateOrNew('ar')->name = $request->category_name_ar;
        $category->translateOrNew('en')->name = $request->category_name_en;
        $category->update();

        return response()->json([
            'message' => Lang::get('admin.edited_successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Category $category
     * @param \App\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
//        deleteTranslations
//        $service = MainCategory::where('id', request('items_id'))->has('service')->get()->count();
//        if ($service > 0) {
//            return response()->json([
//                'message' => 'عذراً: لايمكنك حذف هذا السجل لان لدية ارتباطات أخرى '
//            ], 401);
//        }


        $category = MainCategory::find($request->items_id);
        if (
            $category->branches_count != 0 ||
            $category->main_category_categories_count != 0 ||
            $category->pharmacy_categories_count != 0 ||
            $category->category_count != 0
        ) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }



        deleteImage('/storage/main-categories/' . $category->image);
        $category->deleteTranslations();
        $result = $category->delete();

        if ($result == 0) {
            $result = 'عذراً. لم يتم تحديد اي عنصر ';
            return response()->json(['message' => $result], 401);
        }

//        $result = ' تم حذف ' . $result . ' عنصر بنجاح.';
        $result = Lang::get('admin.deleted_successfully');
        return response()->json(['message' => $result], 200);
    }
}
