<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use App\Models\OfferType;
use App\Models\OfferTypeTranslation;

class OfferTypeController extends Controller
{
    public $view_path = 'managements.encoding.offer_types.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;

        $data = new OfferType();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'name') {
                $data = $data->whereTranslationLike('name', '%' . $query . '%');
            }
        endif;
        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();
        return view($this->view_path . 'index')
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $offer_type = new OfferType();
        $offer_type->status = $request->input('status', 0);
        $offer_type->save();

        $offer_type->translateOrNew('ar')->name = $request->name_ar;
        $offer_type->translateOrNew('en')->name = $request->name_en;
        $offer_type->save();

        return response()->json([
            'status' => 200,
            'offer_type' => $offer_type,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request)
    {
        return Validator::make(
            $request->all(), [
            'name_ar' => 'required|string|max:191',
            'name_en' => 'required|string|max:191',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offer_type = OfferType::find($id);
        $offer_type_ar = OfferTypeTranslation::where('offer_type_id', '=', $id)->where('locale', 'ar')->first();
        $offer_type_en = OfferTypeTranslation::where('offer_type_id', '=', $id)->where('locale', 'en')->first();
        return response()->json([
            'id' => $offer_type->id,
            'status' => $offer_type->status,
            'name_ar' => $offer_type_ar->name,
            'name_en' => $offer_type_en->name,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $offer_type = OfferType::find($id);
        $offer_type->status = $request->input('status', 0);
        $offer_type->save();

        $offer_type->translateOrNew('ar')->name = $request->name_ar;
        $offer_type->translateOrNew('en')->name = $request->name_en;
        $offer_type->save();

        return response()->json([
            'status' => 200,
            'offer_type' => $offer_type,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = Offer::where('type_id', $id)->count();
        if ($offer > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }

        OfferType::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => OfferType::count()
        ],
            200
        );
    }
}
