<?php

namespace App\Http\Controllers;

use App\Http\Resources\PrescriptionRepliesResources;
use App\Models\CartDetail;
use App\Models\Prescription;
use App\Models\PrescriptionReplies;
use App\Models\SystemNotification;
use App\Models\User;
use App\Utilities\Helpers\Fcm;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use RolesAndPermissionsSeeder;
use Yajra\DataTables\DataTables;

class PrescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = Prescription::where(function ($query) {
                    return $query
                        ->where(function ($query) {
                            return $query
                                ->whereNull('insurance_company_id')
                                ->whereNull('is_insurance_company_approved');
                        })
                        ->Orwhere(function ($query) {
                            return $query
                                ->whereNotNull('insurance_company_id')
                                ->where('is_insurance_company_approved', 1);
                        });
                })
                ->get();


            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('user_name', function ($data) {
                    return $data->user->name;
                })
                ->editColumn('phone', function ($data) {
                    return $data->user->phone;
                })
                ->editColumn('branch_name', function ($data) {
                    return $data->branch->name;
                })
                ->editColumn('insurance_company', function ($data) {
                    return $data->insuranceCompany->name ?? null;
                })
                ->addColumn('action', function ($row) {
                    // get prescription_reply_id of prescription_id from prescription_reply table
                    $prescription_reply_id = PrescriptionReplies::where('prescription_id', $row->id)->pluck('id')->toArray();
                    // search for prescription_reply_id in cart_details
                    $cart = CartDetail::whereIn('prescription_reply_id', $prescription_reply_id)->exists();
//                    dump($cart);
                    $btn = '<a class="showB" title="عرض" id="' . $row->id . '"><i class="la la-eye mr-1"></i></a>';
//                    if ($cart == false) {
                    $btn .= '<a class="createPriceClick primary mr-1" title="تعديل" id="' . $row->id . '"><i class="la la-pencil"></i></a>';
//                    }
                    return $btn;
                })
                ->make(true);
        }
        return view('managements.requests_management.prescription.index')->with('prescription');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            /*'medicine_price.*' => ['required', 'numeric','max:99999999.99', function ($attribute, $value, $fail) use ($request) {
               foreach ($request->currency as $item){
                   if ($item == 'ريال يمني') {
                       if ($value % 5 != 0) {
                           $fail(lang::get("validation.max.numeric"));

                       }
                   }
               }
            }],*/
            'medicine_price.*' => 'required|numeric|max:99999999.99',
        ]);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);
        $input_product[] = [];
        try {
            foreach ($request->all() as $key => $st) {
                if ($st < 2) continue;
                for ($i = 0; $i < count($request->medicine_price); $i++) {
                    $input_product[$i][$key] = isset($st[$i]) ? $st[$i] : null;
                }
            }
        } catch (\Exception $e) {
            return response()->json('Message: ' . $e->getMessage(), 401);
        }
        $count = 0;
        foreach ($input_product as $input_products) {
            $count++;
            $input_products['medicine_name'] = isset($input_products['medicine_name']) ? $input_products['medicine_name'] : null;
            $input_products['note'] = isset($input_products['note']) ? $input_products['note'] : null;
            $input_products['currency'] = isset($input_products['currency']) ? $input_products['currency'] : null;
            $input_products['is_alternative'] = isset($input_products['is_alternative']) ? $input_products['is_alternative'] : null;

            $is_alternative = $input_products['is_alternative'] != null ? $input_products['is_alternative'] : 0;
            $note = $input_products['note'] != null ? $input_products['note'] : null;
            $currency = $input_products['currency'] != null ? $input_products['currency'] : null;
            $medicine_name = $input_products['medicine_name'] != null ? $input_products['medicine_name'] : null;
            $prescription_id = isset($input_products['prescription_id']) ? $input_products['prescription_id'] : null;

            /*$carts_detail = CartDetail::where('prescription_reply_id', $request->prescription_id[0])->count();
            if ($carts_detail > 0) {
                return response()->json([
                    'error_deleting' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
                ]);
            }*/


            $p_id = is_null($prescription_id) ? 0 : $prescription_id;
            $carts_detail = CartDetail::where('prescription_reply_id', $p_id)->count();

            if ($carts_detail == 0) {
                PrescriptionReplies::updateOrCreate(
                    [
                        'id' => $prescription_id
                    ],
                    [
                        'prescription_id' => $request->prescription_id_value,
                        'medicine_name' => $medicine_name,
                        'medicine_price' => $input_products['medicine_price'],
                        'is_alternative' => $is_alternative,
                        'currency' => $currency,
                        'note' => $note,
                    ]
                );
            }
        }

        $prescription = Prescription::find($request->prescription_id_value);
        $fcm_title = __('admin.prescription_replies');
        $fcm_body_ar = 'عزيزي المستخدم: تم قبول طلبك يرجى الأنتقال إلى طلبات الصيدلية';
        $fcm_body_en = 'My dear user: Your request has been accepted. Please go to the pharmacy requests ';

        $fcm_body = app()->getLocale() == 'ar'
            ? $fcm_body_ar
            : $fcm_body_en;

        Fcm::send_MKA_notification($fcm_title, $fcm_body, $prescription->user_id);

        //read_at
        SystemNotification::where('notifiable_type', 'App\V1\Models\prescription')
            ->where('notifiable_id', $request->prescription_id_value)
            ->update(['read_at' => Carbon::now()->toDateTime()]);

        return response()->json([
            'message' => Lang::get('admin.added_successfully')
        ]);
    }

    /**
     * Display the specified resource.
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prescription = Prescription::find($id);
//        dd($prescription);
        return response()->json([
            'id' => $prescription->id,
            'user_name' => $prescription->user->name,
            'name' => $prescription->insuranceCompany->name ?? '',
            'insurance_company_number' => $prescription->insurance_company_number ?? '',
            'insurance_company_number_path' => $prescription->insurance_company_number_path,
            'prescription_image' => $prescription->prescription_image,
            'note' => $prescription->note,
            'created_at' => Carbon::Parse($prescription->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($prescription->updated_at)->format('Y/m/d'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return void
     */
    public function edit($id)
    {
        $prescription = PrescriptionReplies::where('prescription_id', $id)->get();

        $prescription = PrescriptionRepliesResources::collection($prescription);

        return response()->json([
            'prescription_data' => $prescription,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'medicine_price.*' => 'required|numeric|max:99999999.99',
        ]);
//        dd(1);
        $input_product[] = [];
        try {
            foreach ($request->all() as $key => $st) {
                if ($st < 2) continue;
                for ($i = 0; $i < count($request->medicine_price); $i++) {
                    $input_product[$i][$key] = isset($st[$i]) ? $st[$i] : null;
                }
            }
        } catch (\Exception $e) {
            dd('Message: ' . $e->getMessage());
        }
        $count = 0;

        DB::beginTransaction();
        try {
            $replaies = PrescriptionReplies::where('prescription_id', $request->parent_prescription_id);
            if ($replaies->count() > 0) {
                if ($replaies->delete() <= 0) {
                    throw new \Exception('you cant delete replies');
                }
            }
            foreach ($input_product as $input_products) {
                $count++;
                $input_products['medicine_name'] = isset($input_products['medicine_name']) ? $input_products['medicine_name'] : null;
                $input_products['note'] = isset($input_products['note']) ? $input_products['note'] : null;
                $input_products['is_alternative'] = isset($input_products['is_alternative']) ? $input_products['is_alternative'] : null;
//            $input_products['status_price'] = isset($input_products['status_price']) ? $input_products['status_price'] : null;

                $is_alternative = $input_products['is_alternative'] != null ? $input_products['is_alternative'] : 0;
                $note = $input_products['note'] != null ? $input_products['note'] : null;
                $medicine_name = $input_products['medicine_name'] != null ? $input_products['medicine_name'] : null;
                $prescription_id = isset($input_products['prescription_id']) ? $input_products['prescription_id'] : null;

                if (!PrescriptionReplies::create(
                    [
                        'prescription_id' => $id,
                        'medicine_name' => $medicine_name,
                        'medicine_price' => $input_products['medicine_price'],
                        'is_alternative' => $is_alternative,
                        'note' => $note,
                    ]
                )) {
                    throw new \Exception('you cant insert new reply');
                };
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 401);
        }
        return response()->json([
            'message' => Lang::get('admin.added_successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $carts_detail = CartDetail::where('prescription_reply_id', $id)->count();
//        dd($id,$carts_detail );
//        $hospitality_detail = HospitalityDetail::where('product_price_id', $id)->count();
        if ($carts_detail > 0) {
            return response()->json([
                'error_deleting' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        try {
            $product_price_delete = PrescriptionReplies::where('id', $id);
//            $product_price_delete = PrescriptionReplies::withTrashed()->where('id', $id);
            $product_price_delete->forceDelete();
        } catch (\Exception $e) {
            return response()->json([
                'error_deleting' => 'Message: ' . $e->getMessage()
            ]);
        }
    }
}
