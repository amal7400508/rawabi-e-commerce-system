<?php

namespace App\Http\Controllers;


use App\Models\Branch;
use App\Models\Category;
use App\Models\Enterprise;
use App\Models\Provider;
use App\Models\Wallet;
use App\Notifications\Admin\ProviderNotification;
use App\Utilities\Helpers\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class EnterpriseController extends Controller
{
    /**
     * @var string
     */
    public $view_path = 'managements.profile_management.provider.';

    /**
     * Display enterprises data.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = Provider::orderBy ('is_approved')
            ->orderBy ('id', 'desc');

        $view_name = 'index';

        return $this->display ($data, $request, $view_name);
    }

    public function display($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null ($search_type) && !is_null ($query)):
            if ($search_type == 'number') {
                $data = $data->where ('id', '=', $query);
            } else if ($search_type == 'enterprise') {
                $data = $data->whereTranslationLike ('name', '%' . $query . '%');
            } else if ($search_type == 'admin_name') {
                $data = $data->whereTranslationLike ('admin_name', '%' . $query . '%');
            } else if ($search_type == 'address') {
                $data = $data->whereTranslationLike ('address', '%' . $query . '%');
            } else if ($search_type == 'phone') {
                $data = $data->where ('phone', 'like', '%' . $query . '%');
            }
        endif;

        $data_count = $data->count ();

        return view ($this->view_path . $view_name)
            ->with ('data', $data->paginate ($table_length))
            ->with ('data_count', $data_count)
            ->with ('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $validatedData = $this->validatedData ($request);
        if ($validatedData->fails ())
            return response ()->json (['error' => $validatedData->errors ()], 401);

        $imageName = null;
        if ($request->hasFile ('image')) {
            $imageName = Image::reSizeImage ($request->file ('image'), 'storage/provider/', 'RawabiTelecom_');
        }

        $enterprises = null;
        DB::transaction (function () use ($request, $imageName, $enterprises) {
            $enterprises = new Provider();
            $enterprises->status = $request->input ('status', 0);
            $enterprises->is_approved = $request->input ('is_approved', 0);
            $enterprises->gender = $request->input ('gender', 'أنثى');
//            $enterprises->type = $request->input ('type', 'enterprise');
            $enterprises->image = $imageName;
            $enterprises->email = $request->email;
            $enterprises->admin_name = $request->admin_name;
            $enterprises->name = $request->name;
            $enterprises->phone = $request->phone;
            $enterprises->telephone = $request->telephone;
            $enterprises->whats_app = $request->whats_app;
            $enterprises->password = bcrypt ($request->password);

            $enterprises->save ();


//            $category = new Category();
//            $category->provider_id = $enterprises->id;
//            $category->status = 1;
//            $category->save ();
//
//            $category->translateOrNew ('ar')->name = 'التصنيف الرئيسي';
//            $category->translateOrNew ('ar')->details = 'هذا التصنيف تم أنشاءه تلقائياً';
//            $category->translateOrNew ('ar')->image = $imageName;
//
//            $category->translateOrNew ('en')->name = 'main category';
//            $category->translateOrNew ('en')->details = 'This category has been automatically created';
//            $category->translateOrNew ('en')->image = $imageName;
//            $category->save ();

        });
        return response ()->json ([
            'status' => 200,
            'provider' => Provider::get(),
            'title' => Lang::get ('admin.added'),
            'message' => Lang::get ('admin.added_successfully'),
        ]);
    }

    public function validatedData($request)
    {
        return Validator::make (
            $request->all (), [
            'name' => 'required|unique:providers,name|string|max:191',

            'admin_name' => 'required|string|max:191',

            'image' => 'required',
            'email' => 'required|email|max:191|unique:providers,email',
            'phone' => 'required|numeric|max:999999999|min:999999',
            'telephone' => 'required|numeric|max:999999999|min:99999',
            'whats_app' => 'required|numeric|max:999999999|min:999999',
            'status' => 'required|string|max:191',
            'password' => ['required', 'string', 'min:5', 'confirmed'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $enterprise = Provider::find ($id);

        return response ()->json ([
            'id' => $enterprise->id,
            'name' => $enterprise->name,
            'phone' => $enterprise->phone,
            'email' => $enterprise->email,
            'status' => $enterprise->status,
            'type' => $enterprise->type,
//            'area' => $enterprise->area->name,
//            'address' => $enterprise->address,
//            'sections' => $enterprise->sections,

            'provider_name' => $enterprise->name ?? null,
            'provider_phone' => $enterprise->phone ?? null,
            'provider_email' => $enterprise->email ?? null,
//            'image' => $enterprise->image_path ?? null,

            'updated_at' => Carbon::Parse ($enterprise->updated_at)->format ('Y/m/d'),
            'created_at' => Carbon::Parse ($enterprise->created_at)->format ('Y/m/d'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make (
            $request->all (), [
            'status' => 'required|in:active,attitude',
        ]);

        if ($validatedData->fails ())
            return response ()->json (['error' => $validatedData->errors ()], 401);

        $enterprise = Provider::find ($id);
        $enterprise->update ([
            'status' => ($request->status == 'active') ? 1 : 0
        ]);


        return response ()->json ([
            'id' => $id,
            'status' => $enterprise->status,
            'background_color_row' => $enterprise->background_color_row,
            'title' => Lang::get ('admin.updated'),
            'message' => Lang::get ('admin.edited_successfully'),
        ]);
    }

    public function accept(Request $request)
    {
        $id = $request->items_id;
        $enterprise = Provider::find ($id);

        DB::transaction (function () use ($enterprise, $id) {
            $enterprise->update (['is_approved' => 1]);
            /*responseNotify($id, Enterprise::class, ProviderNotification::class);
            $enterprise->notify(new ProviderNotification());
            if (is_null($enterprise->wallet)) {
                Wallet::create([
                    'walletable_id' => $id,
                    'walletable_type' => Enterprise::class
                ]);
                Wallet::create([
                    'walletable_id' => $id,
                    'currency' => 'دولار أمريكي',
                    'walletable_type' => Enterprise::class
                ]);
                Wallet::create([
                    'walletable_id' => $id,
                    'currency' => 'ريال سعودي',
                    'walletable_type' => Enterprise::class
                ]);
            }*/
        });

        return response ()->json ([
            'status' => 200,
            'data' => Provider::find ($id),
            'title' => Lang::get ('admin.accepted_successfully'),
            'message' => ''
        ]);
    }

    public function reject(Request $request)
    {
        $id = $request->items_id;
        $enterprise = Provider::find ($id);

        DB::transaction (function () use ($enterprise, $id) {
            $enterprise->update (['is_approved' => 3]);
            /*responseNotify($id, Provider::class, ProviderNotification::class);
            $enterprise->notify(new ProviderNotification());*/
        });

        return response ()->json ([
            'status' => 200,
            'data' => Provider::find ($id),
            'title' => Lang::get ('admin.rejected_successfully'),
            'message' => ''
        ]);
    }

    public function getBranch($enterprise_id)
    {
        return Branch::where ('enterprise_id', $enterprise_id)
            ->get ();
    }

}
