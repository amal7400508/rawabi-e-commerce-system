<?php

namespace App\Http\Controllers;

use App\Exports\ProductsExport;
use App\Http\Resources\ProductShowResource;
use App\Http\Resources\SysNotifyResource;
use App\Models\Branch;
use App\Models\CartDetail;
use App\Models\Category;
use App\Models\CategoryProduct;
use App\Models\MainCategoryCategory;
use App\Models\PharmacyCategory;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\Provider;
use App\Models\SystemNotification;
use App\Notifications\Admin\ProviderNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    public $view_path = 'managements.app.products.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new Product();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('number', '=', $query);
            } else if ($search_type == 'name') {
                $data = $data->whereTranslationLike('name', '%' . $query . '%');
            } else if ($search_type == 'category') {
                $data = $data->whereHas('category', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            } else if ($search_type == 'branch_name') {
                $data = $data->whereHas('branch', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            } else if ($search_type == 'image') {
                $data = $data->whereTranslationLike('image', '%' . $query . '%');
            } else if ($search_type == 'is_approved') {
                $data = $data->where('is_approved', '=', $query);
            }
        endif;

        $data = $data->orderBy('is_approved', 'asc')
            ->orderBy('id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')
            ->with('branches', Branch::active()->get())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
//        $cart_detail = $this->totalDoneRequestsOrders(
//            $request->start_date,
//            $request->end_date
//        );
//
//        $data = ProductPrice::joinSub($cart_detail, 'cart_detail', function ($join) {
//            $join->on('product_prices.id', '=', 'cart_detail.product_price_id');
//        })
//            ->where('product_prices.product_id', $id)
//            ->select('product_prices.*', 'product_count')
//            ->get();
//
//        $product_price = ProductShowResource::collection($data);

        $product = Product::find($id);
        $product_price = ProductShowResource::collection($product->prices);
        $states = $product->states->pluck('name')->toArray();

        return response()->json([
            'id' => $product->id,
            'name' => $product->name ?? null,
            'details' => $product->details,
            'taste' => $product->taste,
            'created_at' => Carbon::Parse($product->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($product->updated_at)->format('Y/m/d'),
            'is_new' => $product->is_new,
            'states' => $states,
            'type' => $product->type,
            'category_translations_name' => $product->category->name ?? null,
            'status' => $product->status,
            'image' => $product->image,
            'branch_name' => $product->branch->name,
            'product_price' => $product_price,
        ]);
    }

    public function totalDoneRequestsOrders($start_date, $end_date)
    {
        $cart_detail = CartDetail::groupBy('product_price_id')
            ->join('requests', 'cart_details.cart_id', 'requests.cart_id')
//            ->where(function ($q) {
//                $q->where('status', 'on_branch')
//                    ->orWhere('status', 'done');
//            })
            ->select('cart_details.product_price_id',
                DB::raw('sum(cart_details.quantity) as product_count'
                )
            );

        if (!is_null($start_date)) {
            $cart_detail = $cart_detail->whereDate('requests.created_at', '>=', $start_date);
        }
        if (!is_null($end_date)) {
            $cart_detail = $cart_detail->whereDate('requests.created_at', '<=', $end_date);
        }

        return $cart_detail;
    }


    public function accept(Request $request)
    {
        $id = $request->items_id;
        $product = Product::find($id);
        if (is_null($product->branch_id)) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($product, $id) {
            $product->update(['is_approved' => 1]);
            responseNotify($id, Product::class, ProviderNotification::class);
            $product->notify(new ProviderNotification());
        });

        return response()->json([
            'status' => 200,
            'data' => Product::find($id),
            'title' => Lang::get('admin.accepted_successfully'),
            'message' => ''
        ]);
    }

    public function acceptAll(Request $request)
    {
        $id = $request->items_id;
        $product = Product::whereIn('id', (array)$id)->get();
        $products = Product::whereIn('id', (array)$id)->count();
        DB::transaction(function () use ($product, $id) {
            $product_notify = Product::whereIn('id', $id)->update(['is_approved' => 1]);
            foreach ($id as $ids) {
                responseNotify($ids, Product::class, ProviderNotification::class);
                foreach ($product as $ids) {
                    $ids->notify(new ProviderNotification());
                }
            }
        });
        if ($products == 0) {
            $products = 'عذراً. لم يتم تحديد اي عنصر ';
            return response()->json(['message' => $products], 401);
        }
        $products = ' تم قبول ' . $products . ' عنصر بنجاح.';
        return response()->json(['message' => $products], 200);
    }

    public function rejectAll(Request $request)
    {
        $id = $request->items_id;
        $product = Product::whereIn('id', (array)$id)->get();
        $products = Product::whereIn('id', (array)$id)->count();
        DB::transaction(function () use ($product, $id) {
            Product::whereIn('id', $id)->update(['is_approved' => 3]);
            /*foreach ($id as $ids) {
                responseNotify($ids, Product::class, ProviderNotification::class);
                $product->notify(new ProviderNotification());
            }*/
        });
        if ($products == 0) {
            $products = 'عذراً. لم يتم تحديد اي عنصر ';
            return response()->json(['message' => $products], 401);
        }
        $products = ' تم رفض ' . $products . ' عنصر بنجاح.';
        return response()->json(['message' => $products], 200);
    }


    public function reject(Request $request)
    {
        $id = $request->items_id;
        $product = Product::find($id);
        if (is_null($product->branch_id)) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($product, $id) {
            $product->update(['is_approved' => 3]);
            responseNotify($id, Product::class, ProviderNotification::class);
            $product->notify(new ProviderNotification());
        });


        return response()->json([
            'status' => 200,
            'data' => Product::find($id),
            'title' => Lang::get('admin.rejected_successfully'),
            'message' => ''
        ]);
    }

    public function changeLevel(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'level' => 'required|int|max:10000',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        Product::where('id', $id)->update(['level' => $request->level]);

        return response()->json([
            'status' => 200,
            'data' => Product::find($id),
            'title' => Lang::get('admin.edited_successfully'),
            'message' => ''
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'is_new' => 'required|in:new,old',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $provider = Product::find($id);
        $provider->update([
            'is_new' => ($request->is_new == 'new') ? 1 : 0
        ]);

        return response()->json([
            'id' => $id,
            'is_new' => $provider->is_new,
            'name' => $provider->name,
            'background_color_row' => $provider->background_color_row,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }


    public function report(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = $this->getReport($request);

        $data_count = $data->count();

        return view($this->view_path . '.report')
//            ->with('providers', Provider::all())
            ->with('categories', Category::where('status', 1)->get())
            ->with('branches', Branch::where('status', 1)->get())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' => isset($_GET['table_length']) ? $_GET['table_length'] : '',
                ]
            );
    }

    public function reportExport(Request $request)
    {

        $data = $this->getReport($request);
        $data = $data->orderBy('products.id', 'desc');
        return view($this->view_path . '.report_export')
            ->with('data', $data->get());
    }


    /**
     * Fetch reports according to conditions
     *
     * @param $request
     * @return mixed
     */
    public function getReport($request)
    {
        $data = new Product();
        if (!is_null($request->from)) {
            $data = $data->whereDate('products.created_at', '>=', $request->from);
        }
        if (!is_null($request->to)) {
            $data = $data->whereDate('products.created_at', '<=', $request->to);
        }
        if (!is_null($request->name)) {
            $data = $data->whereTranslationLike('name', '%' . $request->name . '%');
        }
        if (!is_null($request->id)) {
            $data = $data->where('products.number', $request->id);
        }
        if (!is_null($request->provider)) {
            $data = $data->whereHas('branch', function ($q) use ($request) {
                $q->where('provider_id', $request->provider);
            });
        }
        if (!is_null($request->branch_name)) {
            $data = $data->where('products.branch_id', $request->branch_name);
        }
        if (!is_null($request->category)) {
            $data = $data->where('products.category_id', $request->category);
        }
        if (!is_null($request->type)) {
            $data = $data->where('products.type', $request->type);
        }
        if (!is_null($request->status)) {
            $data = $data->where('products.status', $request->status);
        }
        if ($request->is_new == 1) {
            $data = $data->where('products.is_new', 1);
        }

        $data = $data->select('products.*');

//        dd($data->get());
        $cart_detail = CartDetail::groupBy('product_price_id')
            ->join('requests', 'cart_details.cart_id', 'requests.cart_id')
//            ->where(function ($q) {
//                $q->where('status', 'on_branch')
//                    ->orWhere('status', 'done');
//            })
            ->select('cart_details.product_price_id',
                DB::raw('sum(cart_details.quantity) as product_price_count'
                )
            );

        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if (!is_null($start_date)) {
            $cart_detail = $cart_detail->whereDate('requests.created_at', '>=', $start_date);
        }
        if (!is_null($end_date)) {
            $cart_detail = $cart_detail->whereDate('requests.created_at', '<=', $end_date);
        }

        $product = ProductPrice::joinSub($cart_detail, 'cart_detail', function ($join) {
            $join->on('product_prices.id', '=', 'cart_detail.product_price_id');
        })
            ->join('products', 'product_prices.product_id', 'products.id')
            ->groupBy('product_prices.product_id')
            ->select('product_prices.product_id as id_product',
                DB::raw('sum(product_price_count) as product_count'
                )
            );

        //______________________________

//        $data = $data->joinSub($product, 'product_count', function ($join) {
//            $join->on('id', '=', 'product_count.id_product');
//        });
//
//        $data->select('products.*', 'product_count');
//        $data->orderBy('product_count', 'desc');
////        dd($data->get()[0] ?? null);


        return $data->select('products.*');
    }

    public function export(Request $request)
    {
        $data = $this->getReport($request)->get();
        return Excel::download(
            new ProductsExport(
                $data,
                $request->from_date_requests_number,
                $request->to_date_requests_number
            ),
            'products-' . now() . '.xlsx'
        );
    }

    /**
     * @param $product_id int product id
     * @param $start_date
     * @param $end_date
     * @return int sum requests number
     */
    public function mostProduct(
        $product_id,
        $start_date = null,
        $end_date = null
    )
    {
        $cart_detail = $this->totalDoneRequestsOrders($start_date, $end_date);
        $product_prices = ProductPrice::joinSub($cart_detail, 'cart_detail', function ($join) {
            $join->on('product_prices.id', '=', 'cart_detail.product_price_id');
        })
            ->where('product_prices.product_id', $product_id)
            ->select('product_count')
            ->get();

        $product_count = 0;
        foreach ($product_prices as $product_price) {
            $product_count = $product_count + $product_price->product_count;
        }
        return $product_count;
    }

    public function getBranch($provider_id)
    {
        return Branch::active()
            ->where('provider_id', $provider_id)
            ->get();
    }

    public function getCategories($branch_id)
    {
        $main_category_ids = PharmacyCategory::where('branch_id', $branch_id)->pluck('main_category')->toArray();
        $category_ids = MainCategoryCategory::whereIn('main_category_id', $main_category_ids)->pluck('category_id')->toArray();
        $categories = Category::whereIn('id', $category_ids)->get();

        return response()->json(['categories' => $categories]);
    }

    public function getProducts($category_id)
    {
        $product_ids = CategoryProduct::where('category_id', $category_id)->pluck('product_id')->toArray();
        $products = Product::whereIn('id', $product_ids)->orWhere('category_id', $category_id)->get();

        return response()->json(['products' => $products]);
    }
}
