<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\DeliveryPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class DeliveryPriceController extends Controller
{
    public $view_path = 'managements.carrier_management.delivery_prices.';

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
//        $de = count(Area::all());
//        for ($i = 1; $i <= $de; $i++) {
//            for ($j = $i; $j <= $de ; $j++){
//                $delivery_prices = new DeliveryPrice();
//                $delivery_prices->area_id_from = $i;
//                $delivery_prices->area_id_to = $j;
//                $delivery_prices->price = 600;
//                $delivery_prices->status = 1;
//                $delivery_prices->save();
//            }
//        }
//        dd('done.........');

        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new DeliveryPrice();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'price') {
                $data = $data->where('price', 'like', '%' . $query . '%');
            } else if ($search_type == 'type') {
                $data = $data->where('type', 'like', '%' . $query . '%');
            } else if ($search_type == 'from') {
                $data = $data->whereHas('areaFrom', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            } else if ($search_type == 'to') {
                $data = $data->whereHas('areaTo', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            }
        endif;

        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();

//        dd($data->get()[0]->area_from_to);
        return view($this->view_path . 'index')
            ->with('areas', Area::where('status', 1)->get())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request, true);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $delivery_prices = new DeliveryPrice();
        $delivery_prices->area_id_from = $request->from;
        $delivery_prices->area_id_to = $request->to;
        $delivery_prices->price = $request->price;
        $delivery_prices->branch_count = $request->branch_count;
        $delivery_prices->type = $request->type;
        $delivery_prices->status = $request->input('status', 0);
        $delivery_prices->save();

        $delivery_prices->save();

        return response()->json([
            'status' => 200,
            'area' => $delivery_prices,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request, $is_unique = false)
    {
        return Validator::make(
            $request->all(), [
            'from' => 'required|integer',
            'type' => 'required',
            'to' => ['required', 'integer',
                function ($attribute, $value, $fail) use ($request, $is_unique) {
                    if ($is_unique) {
                        $delivery_price = DeliveryPrice::where('area_id_from', $request->from)
                            ->where('area_id_to', $request->to)
                            ->count();
                        if ($delivery_price != 0) {
                            $fail(lang::get("validation.unique") . '.');
                        }
                    }
                }
            ],
            'price' => 'required|numeric|max:10000000000000000000.00',
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $delivery_prices = DeliveryPrice::find($id);
        return response()->json([
            'id' => $delivery_prices->id,
            'from' => $delivery_prices->area_id_from,
            'to' => $delivery_prices->area_id_to,
            'price' => $delivery_prices->price,
            'branch_count' => $delivery_prices->branch_count,
            'type' => $delivery_prices->type,
            'status' => $delivery_prices->status,

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $delivery_prices = DeliveryPrice::find($id);
        $delivery_prices->area_id_from = $request->from;
        $delivery_prices->area_id_to = $request->to;
        $delivery_prices->price = $request->price;
        $delivery_prices->branch_count = $request->branch_count;
        $delivery_prices->type = $request->type;
        $delivery_prices->status = $request->input('status', 0);
        $delivery_prices->save();

        return response()->json([
            'status' => 200,
            'area' => $delivery_prices,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = Request::where('delivery_price_id', $id)->count();
        if ($request > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }
        DeliveryPrice::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => DeliveryPrice::count()
        ],
            200
        );
    }
}
