<?php

namespace App\Http\Controllers;


use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    public $view_path = 'managements.notify_management.notifications.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = Notification::whereNull('branch_id')
            ->orderBy('id', 'desc');

        return $this->displayAttention($data, $request, 'index');
    }

    public function displayAttention($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'title') {
                $data = $data->whereTranslationLike('notification_title', '%' . $query . '%');
            } else if ($search_type == 'content') {
                $data = $data->whereTranslationLike('notification_message', '%' . $query . '%');
            }
        endif;

        $data_count = $data->count();

        return view($this->view_path . $view_name)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    public function create()
    {
        //
    }

    public function validateAttention($request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'title_ar' => 'required|string|max:191',
            'title_en' => 'required|string|max:191',
            'content_ar' => 'required|string|max:191',
            'content_en' => 'required|string|max:191',
        ]);

        return $validatedData;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateAttention($request);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $notification = new Notification();
        $notification->save();
        $notification->translateOrNew('ar')->notification_title = $request->title_ar;
        $notification->translateOrNew('ar')->notification_message = $request->content_ar;
        $notification->translateOrNew('en')->notification_title = $request->title_en;
        $notification->translateOrNew('en')->notification_message = $request->content_en;
        $notification->save();

        return response()->json([
            'status' => 200,
            'notification' => $notification,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification = Notification::find($id);

        return response()->json([
            'id' => $notification->id,
            'title' => $notification->notification_title,
            'content' => $notification->notification_message,
            'created_at' => Carbon::Parse($notification->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($notification->updated_at)->format('Y/m/d'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notification = Notification::find($id);
        $notification_ar = $notification->translate('ar');
        $notification_en = $notification->translate('en');

        return response()->json([
            'id' => $notification->id,
            'title_ar' => $notification_ar->notification_title ?? null,
            'title_en' => $notification_en->notification_title ?? null,
            'content_ar' => $notification_ar->notification_message ?? null,
            'content_en' => $notification_en->notification_message ?? null,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateAttention($request);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $notification = Notification::find($id);

        $notification->translateOrNew('ar')->notification_title = $request->title_ar;
        $notification->translateOrNew('ar')->notification_message = $request->content_ar;

        $notification->translateOrNew('en')->notification_title = $request->title_en;
        $notification->translateOrNew('en')->notification_message = $request->content_en;
        $notification->save();

        return response()->json([
            'status' => 200,
            'notification' => $notification,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification = Notification::find($id);
        deleteImage('/storage/notifications/' . $notification->image);
        $notification->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Notification::count()
        ],
            200
        );
    }

}
