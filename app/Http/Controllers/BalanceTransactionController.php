<?php

namespace App\Http\Controllers;

use App\Exports\AccountExport;
use App\Exports\BalanceTransactionsExport;
use App\Models\Account;
use App\Models\BalanceTransaction;
use App\Models\Branch;
use App\Models\OperationType;
use App\Models\Provider;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Http\Request as RequestPost;
use Maatwebsite\Excel\Facades\Excel;

class BalanceTransactionController extends Controller
{
    public $view_path = 'managements.accounts.balance_transactions.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;
        $data = $this->getReport($request);
        $data_count = $data->count();

        return view($this->view_path . '.index')
            ->with('accounts', Account::get())
            ->with('providers', Provider::where('is_approved', 1)->get())
            ->with('operation_type', OperationType::all())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' => isset($_GET['table_length']) ? $_GET['table_length'] : '',
                ]
            );
    }

    public function report(Request $request)
    {
        $account_name = $request->type == 'account' ? Account::find($request->account)->name ?? null
            : Branch::find($request->account)->name ?? null;

        return view($this->view_path . '.report')
            ->with('data', $this->getReport($request)->get())
            ->with('branch_name', $account_name);
    }

    public function getReport($request)
    {
        $data = new BalanceTransaction();
        if (!is_null($request->id)) {
            $data = $data->where('id', $request->id);
        }
        if (!is_null($request->type)) {
            switch ($request->type) {
                case "user":
                    $type = "App\Models\User";
                    break;
                case "account":
                    $type = "App\Models\Account";
                    break;
                case "branch":
                    $type = "App\Models\Branch";
                    break;
                default:
                    $type = "";
            }

            $data = $data->whereHas('wallet', function ($q) use ($request, $type) {
                $q->where('walletable_type', $type);
            });
        }
        if (!is_null($request->provider)) {
            $wallet_id = [];
            $branches = Provider::find($request->provider)->branch;
            foreach ($branches as $branch) {
                foreach ($branch->wallet as $wallet) {
                    $wallet_id[] = $wallet->id;
                }
            }
            $data = $data->whereIn('wallet_id', $wallet_id);
        }
        if (!is_null($request->account)) {

            $wallet_id = [];
            if (is_null($request->currency)) {
                if ($request->type == 'account') {
                    $wallet_id = Account::find($request->account)->wallet->pluck('id')->toArray();
                } elseif ($request->type == 'branch') {
                    $wallet_id = Branch::find($request->account)->wallet->pluck('id')->toArray();
                }
            } else {
                if ($request->type == 'account') {
                    $wallet_id = Account::find($request->account)->wallet->where('currency', $request->currency)->pluck('id')->toArray();
                } elseif ($request->type == 'branch') {
                    $wallet_id = Branch::find($request->account)->wallet->where('currency', $request->currency)->pluck('id')->toArray();
                }
            }

            $data = $data->whereIn('wallet_id', $wallet_id);
        }
        if (!is_null($request->currency)) {
            $data = $data->where('currency', $request->currency);
        }
        if (!is_null($request->from)) {
            $data = $data->whereDate('balance_transactions.created_at', '>=', $request->from);
        }
        if (!is_null($request->to)) {
            $data = $data->whereDate('balance_transactions.created_at', '<=', $request->to);
        }
        if (!is_null($request->operation_type)) {
            $data = $data->where('operation_type_id', $request->operation_type);
        }
        if (!is_null($request->request_status)) {
            $data = $data->where('operation_type_id', $request->operation_type);
        }
        $data = $data->orderBy('id', 'desc');
        return $data;
    }

    public function export(RequestPost $request)
    {
        $data = $this->getReport($request)->get();
        return Excel::download(
            new AccountExport($data),
            'balance_transactions-' . now() . '.xlsx'
        );
    }

    public function walletFilter($type)
    {
        if ($type == 'branch') {
            return Branch::active()->get();
        } else if ($type == 'user') {
            return User::active()->get();
        } else if ($type == 'account') {
            return Account::get();
        }

        return response()->json(['error'], 401);
    }

    public function branchFilter($provider_id)
    {
        return Branch::active()
            ->where('provider_id', $provider_id)
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
