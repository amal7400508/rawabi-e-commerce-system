<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Models\Audit;

class AuditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (request()->ajax()) {
            $data_select =Audit::join('admins', 'audits.admins_id', '=', 'admins.id')
//                ->join('providers', 'audits.admins_id', '=', 'providers.id')
                ->select('audits.*', 'admins.name as admin_name')
                ->get();
            return datatables()->of($data_select)
                ->editColumn('new_values', function (Audit $select_data) {
                    return $this->showUpdatedValues($select_data,'new_values');

                })
                ->editColumn('old_values', function (Audit $select_data) {
                    return $this->showUpdatedValues($select_data,'old_values');
                })
                ->make(true);
        }
        return view('managements.admins_management.audit.index');
    }

    /**
     * They read and display data from an array
     *
     * @param $select_data , select from table
     * @param $column , name of column in table
     * @return string , show all updated values
     */
    public  function showUpdatedValues($select_data, $column)
    {
        $values='';
        $select_data=$select_data->toArray()[$column];
        foreach ($select_data as $key=>$value) {
            $values .=  $key . ' = ' . $value.' & ';
        }
        return $values;
    }
}
