<?php

namespace App\Http\Controllers;

use App\ProductPrice;
use App\Taste;
use App\TasteTranslation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class TasteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('taste_translations')
                ->join('tastes', 'taste_translations.taste_id', '=', 'tastes.id')
                ->where('taste_translations.locale', '=', $locale)
                ->where('tastes.id', '!=', 1)
                ->whereNull('tastes.deleted_at')
                ->select('taste_translations.*', 'tastes.updated_at')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '';
                    if (Gate::check('update taste')) {
                        $button .= '<button type="button" name="edit" id="' . $data->taste_id . '" class="edit-table-row btn btn-primary btn-sm" data-toggle="edite" title="edite"><i class="ft ft-edit"></i></button>';
                        $button .= '&nbsp';
                    }
                    $button .= '<button type="button" name="showenter" id="' . $data->id . '"  class="showdetail showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    if (Gate::check('delete taste')) {
                        $button .= '&nbsp';
                        $button .= '<button type="button" name="delete" id="' . $data->taste_id . '" class="delete btn btn-danger btn-sm" data-toggle="delete" title="delete"><i class="ft ft-trash-2"></button>';
                    }
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('taste.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ar_taste' => 'required|string|max:191|unique:taste_translations,taste',
            'en_taste' => 'required|string|max:191',
        ]);

        $taste = new Taste();
        $taste->save();

        $taste->translateOrNew('ar')->taste = $request->ar_taste;
        $taste->translateOrNew('en')->taste = $request->en_taste;
        $taste->save();

        return redirect()->back()->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $taste = TasteTranslation::where('id', $id)->first();
        $tastes = Taste::find($taste->taste_id);
        $btnShow = '';
        if (Gate::check('delete taste')) {
            $btnShow = '<button  class="delete btn btn-danger btn-float btn-round" id="' . $taste->taste_id . '" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></i></button>';
        }
        return response()->json([
            'id' => $taste->taste_id,
            'taste' => $taste->taste,
            'created_at' => Carbon::Parse($tastes->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($tastes->updated_at)->format('Y/m/d'),
            'btn' => $btnShow,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Taste::findOrFail($id);
        $taste_ar = TasteTranslation::where('taste_id', '=', $id)->where('locale', 'ar')->first();
        $taste_en = TasteTranslation::where('taste_id', '=', $id)->where('locale', 'en')->first();
        return response()->json([
            'edit_taste_ar' => $taste_ar->taste,
            'edit_id' => $taste_ar->taste_id,
            'edit_taste_en' => $taste_en->taste,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'ar_taste' => 'required|string|max:191',
            'en_taste' => 'required|string|max:191',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)
                ->withInput(
                    [
                        'method' => 'update',
                        'id' => $id,
                        'ar_taste' => $request->ar_taste,
                        'en_taste' => $request->en_taste,
                    ]
                );
        }

        $taste = Taste::find($id);
        $taste->save();
        $taste->translateOrNew('ar')->taste = $request->ar_taste;
        $taste->translateOrNew('en')->taste = $request->en_taste;
        $taste->save();
        return redirect()->back()->with('success', Lang::get('admin.edited_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_price = ProductPrice::withTrashed()->where('taste_id', $id)->count();
        if ($product_price > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        $taste = Taste::find($id);
        $taste->delete();
    }

    /**
     *  displays the deleted data as an initial deletion
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function recycle_bin()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('taste_translations')
                ->join('tastes', 'taste_translations.taste_id', '=', 'tastes.id')
                ->where('taste_translations.locale', '=', $locale)
                ->whereNotNull('tastes.deleted_at')
                ->select('taste_translations.*')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="restore" id="' . $data->taste_id . '" class="restore btn btn-primary btn-sm" data-toggle="Restore" title="Restore"><i class="ft ft-rotate-cw"></i></button>';
                    $button .= '&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->taste_id . '" class="delete btn btn-danger btn-sm" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('taste.recycle_bin');
    }

    /**
     * retrieves deleted data from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore($id)
    {
        $taste = Taste::withTrashed()->where('id', $id)->first();
        $taste->restore();
        return response()->json();
    }

    /**
     * performs the final deletion from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function hdelete($id)
    {
        $product_price = ProductPrice::withTrashed()->where('taste_id', $id)->count();
        if ($product_price > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        DB::table('taste_translations')
            ->where('taste_id', '=', $id)
            ->delete();
        $taste = Taste::withTrashed()->where('id', $id)->first();
        $taste->forceDelete();
    }
}
