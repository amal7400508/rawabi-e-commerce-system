<?php

namespace App\Http\Controllers;

use App\Models\CategoryProduct;
use App\Models\CategoryTranslation;
use App\Models\Category;
use App\Models\Country;
use App\Models\MainCategory;
use App\Models\MainCategoryCategory;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Image as ImageFaker;

class CategoryController extends Controller
{
    public $view_path = 'managements.encoding.categories.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new Category();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'name') {
                $data = $data->whereTranslationLike('name', '%' . $query . '%');
            } else if ($search_type == 'details') {
                $data = $data->whereTranslationLike('details', '%' . $query . '%');
            }
        endif;

        $data = $data->orderBy('categories.id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')
            ->with('mainCategory', MainCategory::all())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'name_ar' => 'required|string|max:191',
            'name_en' => 'required|string|max:191',
            'details_ar' => 'nullable|string|max:191',
            'details_en' => 'nullable|string|max:191',
            'level' => 'nullable|int|max:10000',
            'main_categories' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $image_name = is_null($request->image) ? null : $this->uploadImage($request->file('image'));

        $category = new Category();
        $category->status = $request->input('status', 0);
        $category->level = $request->level;
        $category->main_category_id = $request->main_categories;
        $category->save();

        $category->translateOrNew('ar')->name = $request->name_ar;
        $category->translateOrNew('ar')->details = $request->details_ar;
        $category->translateOrNew('ar')->image = $image_name;

        $category->translateOrNew('en')->name = $request->name_en;
        $category->translateOrNew('en')->details = $request->details_en;
        $category->translateOrNew('en')->image = $image_name;
        $category->save();

        return response()->json([
            'status' => 200,
            'category' => $category,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function uploadImage($image)
    {
        $imageName = 'category_' . time() . '.' . $image->getClientOriginalExtension();
        $file_path = 'storage/categorise/';

        $path100 = public_path($file_path . '/100/'); // For phone
        $path64 = public_path($file_path . '/64/'); // For search

        File::exists($path100)
            ?: File::makeDirectory($path100, $mode = 0777, true, true);
        File::exists($path64)
            ?: File::makeDirectory($path64, $mode = 0777, true, true);

        ImageFaker::make($image)
            ->resize(100, 100)
            ->fit(100, 100)->save($file_path . '/100/' . $imageName);
        ImageFaker::make($image)
            ->resize(64, 64)
            ->fit(64, 64)->save($file_path . '/64/' . $imageName);

        $image->move(public_path($file_path), $imageName);

        return $imageName;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        return response()->json([
            'id' => $category->id,
            'name' => $category->name,
            'details' => $category->details,
            'image' => $category->image_path,
            'status' => $category->status,
            'updated_at' => Carbon::Parse($category->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($category->created_at)->format('Y/m/d'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $category_ar = CategoryTranslation::where('category_id', '=', $id)->where('locale', 'ar')->first();
        $category_en = CategoryTranslation::where('category_id', '=', $id)->where('locale', 'en')->first();

        return response()->json([
            'id' => $category->id,
            'name_ar' => $category_ar->name ?? null,
            'name_en' => $category_en->name ?? null,
            'details_ar' => $category_ar->details ?? null,
            'details_en' => $category_en->details ?? null,
            'status' => $category->status,
            'level' => $category->level,
            'image' => $category->image_path_100,
            'main_categories'=> $category->main_category_id ?? null,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'name_ar' => 'required|string|max:191',
            'name_en' => 'required|string|max:191',
            'details_ar' => 'nullable|string|max:191',
            'details_en' => 'nullable|string|max:191',
            'level' => 'nullable|int|max:10000',
            'main_categories' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $category = Category::find($id);

        if ($request->hasFile('image')) {
            $image = $category->image;
            File::delete(public_path() . '/storage/categorise/' . $image);
            File::delete(public_path() . '/' . 'storage/categorise/100/' . $image);
            File::delete(public_path() . '/' . 'storage/categorise/64/' . $image);
            $image_name = is_null($request->image) ? null
                : $this->uploadImage($request->file('image'));
        }

        $status = is_null($request->input('status', 0))
            ? 1
            : $request->input('status', 0);

        $category->status = $status;
        $category->level = $request->level;
        $category->main_category_id = $request->main_categories;
        $category->save();

        $category->translateOrNew('ar')->name = $request->name_ar;
        $category->translateOrNew('ar')->details = $request->details_ar;

        $category->translateOrNew('en')->name = $request->name_en;
        $category->translateOrNew('en')->details = $request->details_en;

        if ($request->hasFile('image')) {
            $category->translateOrNew('ar')->image = $image_name;
            $category->translateOrNew('en')->image = $image_name;
        }

        $category->save();

        return response()->json([
            'status' => 200,
            'category' => $category,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::withTrashed()->where('category_id', $id)->count();
        $category_product = CategoryProduct::where('category_id', $id)->count();
        $main_category_category = MainCategoryCategory::withTrashed()->where('category_id', $id)->count();
        if (($product + $category_product + $main_category_category) > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }

        $category = Category::find($id);
        $image = $category->image;
        File::delete(public_path() . '/storage/categorise/' . $image);
        File::delete(public_path() . '/' . 'storage/categorise/100/' . $image);
        File::delete(public_path() . '/' . 'storage/categorise/64/' . $image);
        $category->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Category::count()
        ],
            200
        );
    }

    public function changeLevel(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'level' => 'required|int|max:10000',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        Category::where('id', $id)->update(['level' => $request->level]);

        return response()->json([
            'status' => 200,
            'data' => Category::find($id),
            'title' => Lang::get('admin.edited_successfully'),
            'message' => ''
        ]);
    }

}
