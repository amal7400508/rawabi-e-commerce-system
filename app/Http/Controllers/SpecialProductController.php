<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\CartDetail;
use App\Models\SpecialProduct;
use App\Models\SystemNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SpecialProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            /*$special_product = DB::table('special_products')
                ->join('users', 'special_products.user_id', '=', 'users.id')
                ->whereNull('special_products.deleted_at')
                ->select('special_products.*', 'users.name as user_name', 'users.phone')
                ->get()->toarray();*/
            $special_product = SpecialProduct::all();
            return datatables()->of($special_product)
                ->addColumn('action', function ($data) {
                    $span = '<button type="button" name="show" id="' . $data->id . '" class="showB btn-sm btn btn-info"><i class="la la-eye"></i><span></span></button>';
                    return $span;
                })->editColumn('branch_name', function ($data) {
                    return $data->branch->name ?? '';
                })->editColumn('user_name', function ($data) {
                    return $data->user->name ?? '';
                })->editColumn('phone', function ($data) {
                    return $data->user->phone ?? '';
                })->rawColumns(['action'])
                ->make(true);
        }
        return view('managements.requests_management.special_product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $special_product = SpecialProduct::join('users', 'special_products.user_id', '=', 'users.id')
            ->whereNull('special_products.deleted_at')
            ->where('special_products.id', '=', $id)
            ->select('special_products.*', 'users.name as user_name')
            ->get()->first();

        return response()->json([
            'id' => $special_product->id,
            'name' => $special_product->name,
            'user_name' => $special_product->user_name,
            'status' => $special_product->status,
            'image' => $special_product->image_path,
            'price' => $special_product->price,
            'date' => $special_product->date,
            'desc' => $special_product->desc,
            'note' => $special_product->note,
            'created_at' => Carbon::Parse($special_product->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($special_product->updated_at)->format('Y/m/d'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $special_product = DB::table('special_products')
            ->join('users', 'special_products.user_id', '=', 'users.id')
            ->whereNull('special_products.deleted_at')
            ->where('special_products.id', '=', $id)
            ->select('special_products.*', 'users.name as user_name')
            ->get()->first();
        return response()->json([
            'id' => $special_product->id,
            'price' => $special_product->price,
            'status' => $special_product->status,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $special_product = SpecialProduct::find($id);
        if ($request->status == 1) {
            if ($request->price > 99999999.99) {
                return response()->json([
                    'error' => Lang::get('validation.max.numeric') . ': 99999999.99 ',
                ]);
            } else if ($request->price < 0.00) {
                return response()->json([
                    'error' => Lang::get('validation.min.numeric')
                ]);
            }
            $this->validate($request, [
                'price' => 'required|numeric|min:0|max:99999999:99',
            ]);
            $special_product->price = $request->price;
            $special_product->currency = $request->currency;
            $special_product->admin_message = $request->admin_message;
            $special_product->status = 3;
        } else {
            $special_product->price = 0;
            $special_product->status = 2;
            $special_product->admin_message = $request->admin_message;

            $fcm_body = is_null($request->admin_message) ?
                app()->getLocale() == 'ar' ? ' عذراً لقد تم إلغاء طلبك الخاص رقم: ' . $request->id :
                    'Sorry your special products has been canceled no: ' . $request->id
                : $request->admin_message . 'رقم: ' . $request->id;

            $fcm_title = app()->getLocale() == 'ar'
                ? 'إلغاء الطلب الخاص' : 'Cancel special products';

            // Send notification via Fire pace
            \App\BySwadi\Helpers\Fcm::send_MKA_notification($fcm_title, $fcm_body, $special_product->user_id);
        }
        $special_product->save();

        SystemNotification::where('notifiable_type', 'App\SpecialProduct')
            ->where('notifiable_id', $id)
            ->update(['response_at' => Carbon::now()->toDateTime()]);

        return response()->json([
            'success' => Lang::get('admin.edited_successfully'),
        ]);
    }

    public function completedRequest(Request $request, $id)
    {
        $special_product = SpecialProduct::find($id);
        if ($request->status == 1) {
            $special_product->status = 1;
        } else if (CartDetail::where('special_product_id', $id)->count() == 0) {
            $special_product->status = 2;
            $special_product->admin_message = $request->admin_message;

            $fcm_body = is_null($request->admin_message) ?
                app()->getLocale() == 'ar' ? ' عذراً لقد تم إلغاء طلبك الخاص رقم: ' . $request->id :
                    'Sorry your special products has been canceled no: ' . $request->id
                : $request->admin_message . 'رقم: ' . $request->id;

            $fcm_title = app()->getLocale() == 'ar'
                ? 'إلغاء الطلب الخاص' : 'Cancel special products';

            // Send notification via Fire pace
            \App\BySwadi\Helpers\Fcm::send_MKA_notification($fcm_title, $fcm_body, $special_product->user_id);
        } else {
            return response()->json(['error' => 'error'], 401);
        }
        $special_product->save();
        return response()->json([
            'success' => Lang::get('admin.edited_successfully'),
        ]);
    }

    public function deferred()
    {
        if (request()->ajax()) {
            /*$special_alert_message = DB::table('special_products')
                ->join('users', 'special_products.user_id', '=', 'users.id')
                ->where('special_products.status', '=', 3)
                ->select('special_products.*', 'users.name as user_name', 'users.phone')
                ->get()->toarray();*/
            $special_alert_message = SpecialProduct::where('status',3)->get();
            return datatables()->of($special_alert_message)
                ->addColumn('action', function ($data) {
                    $span = '<button type="button" name="show" id="' . $data->id . '" class="showB btn-sm btn btn-info"><i class="la la-eye"></i><span></span></button>';
                    return $span;
                })->editColumn('status', function ($data) {
                    if (CartDetail::where('special_product_id', $data->id)->count() != 0) {
                        return 5; // هذا الرقم يعني ان هذا الطلب الخاص يعتبر مؤكد
                    }
                    return $data->status;
                })->editColumn('branch_name', function ($data) {
                    return $data->branch->name ?? '';
                })->editColumn('user_name', function ($data) {
                    return $data->user->name ?? '';
                })->editColumn('phone', function ($data) {
                    return $data->user->phone ?? '';
                })->rawColumns(['action'])
                ->make(true);
        }
        return view('managements.requests_management.special_product.deferred');
    }
}
