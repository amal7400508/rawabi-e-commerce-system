<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Utilities\Helpers\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;



class ProfileController extends Controller
{
    public function index()
    {
        /*$role = Role::where('name', 'admin_administrator')->first();
        $role->givePermissionTo(Permission::where('guard_name', 'admin')->get());*/

        return view('managements.profile_management.profile.index');
    }

    public function edit($id)
    {
        return view('managements.profile_management.profile.index')->with('profile', Auth::User()->$id);
    }

    public function update(Request $request)
    {
        if ($request->name == null && $request->email == null && $request->phone == null) {
            return redirect('/profile_management/profile')->with('error', Lang::get('admin.no_data_for_edit'));
        }
        $this->validate($request, [
            'name' => 'nullable|string|max:191',
            'email' => 'nullable|email|max:191|unique:admins,email',
            'phone' => 'nullable|numeric|max:999999999',
        ]);
        $profile = Admin::find(Auth::user()->id);
        if ($request->name != null) {
            $profile->name = $request->name;
        }
        if ($request->email != null) {
            $profile->email = $request->email;
        }
        if ($request->phone != null) {
            $profile->phone = $request->phone;
        }
        $profile->save();
        return redirect('/profile_management/profile')->with('success', Lang::get('admin.edited_successfully'));
    }

    public function edit_password($id)
    {
        $profile = Admin::find($id);
        return view('managements.profile_management.profile.edit_password')->with('profile', $profile);
    }

    public function edit_image(Request $request)
    {
        $this->validate($request, [
            'image' => 'required',
        ]);
        if ($request->image) {
        }
        if ($request->hasFile('image')) {
            $imageName = Image::reSizeImage($request->file('image'), 'storage/admins/', 'moka_admin_');
        }
        $id = Auth::user()->id;

        if (Auth::user()->image != 'avatar.jpg') {
            Image::deleteImageFromFolder(
                'storage/admins',
                Auth::user()->image);
        }
        $profile = Admin::find($id);
        $profile->image = $imageName;
        $profile->save();
        return redirect('/profile_management/profile')->with('success', Lang::get('admin.edited_successfully'));
    }

    public function delete_image()
    {
        $image = 'avatar.jpg';
        if (Auth::user()->image == $image) {
            return response()->json([
                'message' => Lang::get('admin.no_image_for_delete')
            ]);
        }
        $id = Auth::user()->id;
        Image::deleteImageFromFolder(
            'storage/admins',
            Auth::user()->image);
        $profile = Admin::find($id);
        $profile->image = $image;
        $profile->save();
        return response()->json([
            'image' => $image
        ]);
    }

    public function update_password(Request $request, $id)
    {
        $messages = [
            'password_Present.required' => Lang::get('validation.required'),
            'password_Present.match_old_password' => Lang::get('validation.match_old_password'),
            'password.required' => Lang::get('validation.required'),
            'password.confirmed' => Lang::get('validation.confirmed'),
        ];
        $validator = Validator::make($request->all(), [
            'password_Present' => ['required', 'match_old_password'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $user = Admin::find(Auth::User()->id);
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect('/profile_management/profile')->with('success', Lang::get('admin.edited_successfully'));
    }


    public function updateStyle($value)
    {
        if ($value == 'dark' or $value == 'light'){
            Admin::where('id', Auth::id())->update(['style' => $value]);
        }
        return redirect()->back();
    }
}
