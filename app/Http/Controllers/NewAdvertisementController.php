<?php

namespace App\Http\Controllers;


use App\Http\Resources\FalterProductResource;
use App\Models\AdvertisementTranslation;
use App\Models\Category;
use App\Models\Advertisement;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Stateable;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class NewAdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new Advertisement();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', $query);
            } else if ($search_type == 'media') {
                $data = $data->where('media', 'like', '%' . $query . '%');
            } else if ($search_type == 'media_type') {
                $data = $data->where('media_type', 'like', '%' . $query . '%');
            }
        endif;

        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();

        return view('managements.encoding.new_advertisement.index')
            ->with('offers', Offer::active()->get())
            ->with('categories', Category::where('status', 1)->get())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $image = $request->file('image');
        $imageName = 'Rawabi' . time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('storage/advertisement/'), $imageName);

        $advertisementable_type = null;
        $advertisementable_id = null;

        if ($request->link_type == 'product') {
            $advertisementable_type = Product::class;
            $advertisementable_id = $request->product;
        } elseif ($request->link_type == 'offer') {
            $advertisementable_type = Offer::class;
            $advertisementable_id = $request->offer;
        }

        $advertisement = new Advertisement();
        $advertisement->type = $request->type;
        $advertisement->end_date = $request->end_date;
        $advertisement->slide_number = $request->slide_number;
        $advertisement->advertisementable_type = $advertisementable_type;
        $advertisement->advertisementable_id = $advertisementable_id;
        $advertisement->save();

        $advertisement->translateOrNew('ar')->desc = $request->description_ar;
        $advertisement->translateOrNew('ar')->image = 'advertisement/' . $imageName;

        $advertisement->translateOrNew('en')->desc = $request->description_en;
        $advertisement->translateOrNew('en')->image = 'advertisement/' . $imageName;
        $advertisement->save();

//        foreach ($request->city as $city) {
//            $state_id = $city == 'ibb' ? 2 : 1;
//            Stateable::create([
//                'state_id' => $state_id,
//                'stateable_id' => $advertisement->id,
//                'stateable_type' => Advertisement::class,
//            ]);
//        }

        return response()->json([
            'status' => 200,
            'advertisement' => $advertisement,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request, $required = 'required')
    {
        return Validator::make(
            $request->all(), [
            'type' => 'required|in:slider,popup',
            'slide_number' => 'required',
            'description_ar' => 'required|string|max:191',
            'description_en' => 'required|string|max:191',
            'end_date' => 'required|date|after:yesterday',
            'image' => [$required, 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
//            'city' => 'required',
//            'city.*' => 'required|in:sana_a,ibb',
            'link_type' => function () use ($request) {
                if ($request->check_link == 1) {
                    return 'required|in:offer,product';
                }
                return 'nullable';
            },
            'product' => function () use ($request) {
                if ($request->check_link == 1 and $request->link_type == 'product') {
                    return 'required';
                }
                return 'nullable';
            },
            'offer' => function () use ($request) {
                if ($request->check_link == 1 and $request->link_type == 'offer') {
                    return 'required|integer';
                }
                return 'nullable';
            }
        ], [
            'after' => __('validation.date_most_be_at_least_today'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertisement = Advertisement::find($id);
        $states = Stateable::where('stateable_type', Advertisement::class)
            ->where('stateable_id', $id)
            ->pluck('state_id')
            ->toArray();

        $sana_a = in_array(1, $states);
        $ibb = in_array(2, $states);

        $check_link = is_null($advertisement->advertisementable_id) ? false : true;

        $link_type = null;
        $product_name = null;

        if ($advertisement->advertisementable_type == Product::class) {
            $link_type = 'product';
            $product_name = Product::find($advertisement->advertisementable_id)->name;

        } elseif ($advertisement->advertisementable_type == Offer::class) {
            $link_type = 'offer';
        }

        return response()->json([
            'id' => $advertisement->id,
            'end_date' => $advertisement->end_date,
            'type' => $advertisement->type,
            'description_ar' => $advertisement->translate('ar')->desc,
            'description_en' => $advertisement->translate('en')->desc,
            'image' => asset('storage') . '/' . $advertisement->image,
            'sana_a' => $sana_a,
            'ibb' => $ibb,
            'advertisementable_id' => $advertisement->advertisementable_id,
            'advertisementable_type' => $advertisement->advertisementable_type,
            'slide_number' => $advertisement->slide_number,

            'check_link' => $check_link,
            'link_type' => $link_type,
            'product_name' => $product_name
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request, 'nullable');
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $advertisementable_type = null;
        $advertisementable_id = null;

        if ($request->link_type == 'product') {
            $advertisementable_type = Product::class;
            $advertisementable_id = $request->product;
        } elseif ($request->link_type == 'offer') {
            $advertisementable_type = Offer::class;
            $advertisementable_id = $request->offer;
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = 'moka_' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('storage/advertisement/'), $imageName);
            $filename = public_path() . '/' . AdvertisementTranslation::where('advertisement_id', '=', $id)->get()->first()->image;
            File::delete($filename);
        }

        $advertisement = Advertisement::find($id);
        $advertisement->type = $request->type;
        $advertisement->end_date = $request->end_date;
        $advertisement->slide_number = $request->slide_number;
        $advertisement->advertisementable_type = $advertisementable_type;
        $advertisement->advertisementable_id = $advertisementable_id;
        $advertisement->save();

        $advertisement->translateOrNew('ar')->desc = $request->description_ar;
        if ($request->hasFile('image')) {
            $advertisement->translateOrNew('ar')->image = 'advertisement/' . $imageName;
        }
        $advertisement->translateOrNew('en')->desc = $request->description_en;
        if ($request->hasFile('image')) {
            $advertisement->translateOrNew('en')->image = 'advertisement/' . $imageName;
        }
        $advertisement->save();

        Stateable::where('stateable_type', Advertisement::class)
            ->where('stateable_id', $id)
            ->delete();

//        foreach ($request->city as $city) {
//            $state_id = $city == 'ibb' ? 2 : 1;
//            Stateable::create([
//                'state_id' => $state_id,
//                'stateable_id' => $advertisement->id,
//                'stateable_type' => Advertisement::class,
//            ]);
//        }

        return response()->json([
            'status' => 200,
            'advertisement' => $advertisement,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Advertisement::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Advertisement::count()
        ],
            200
        );
    }
    /**
     *  displays the deleted data as an initial deletion
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function recycle_bin()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('advertisement_translations')
                ->join('advertisements', 'advertisement_translations.advertisement_id', '=', 'advertisements.id')
                ->where('advertisement_translations.locale', '=', $locale)
                ->whereNotNull('advertisements.deleted_at')
                ->select('advertisement_translations.*', 'advertisements.end_date')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="restore" id="' . $data->advertisement_id . '" class="restore btn btn-primary btn-sm" data-toggle="Restore" title="Restore"><i class="ft ft-rotate-cw"></i></button>';
                    $button .= '&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->advertisement_id . '" class="delete btn btn-danger btn-sm" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('advertisement.recycle_bin');
    }

    /**
     * retrieves deleted data from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore($id)
    {
        $advertisement = Advertisement::withTrashed()->where('id', $id)->first();
        $advertisement->restore();
        return response()->json();
    }

    /**
     * performs the final deletion
     * from the Recycle Bin
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function hdelete($id)
    {
        $filename = public_path() . '/' . AdvertisementTranslation::where('advertisement_id', '=', $id)->get()->first()->image;
        File::delete($filename);
        DB::table('advertisement_translations')
            ->where('advertisement_id', '=', $id)
            ->delete();
        $advertisement = Advertisement::withTrashed()->where('id', $id)->first();
        $advertisement->forceDelete();
    }

    public function falterProduct($id)
    {
        $product = Product::where('category_id', $id)
             ->approved()->available()->get();
        $data = FalterProductResource::collection($product);

        return response()->json($data);
    }
}
