<?php

namespace App\Http\Controllers;

use App\Models\Alert;
use App\Models\AttentionTranslation;
use App\Notifications\Admin\ProviderNotification;
use App\Utilities\Helpers\Fcm;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class AttentionController extends Controller
{
    public $view_path = 'managements.app.attentions.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = Alert::whereNull('branch_id')
            ->orderBy('id', 'desc');
        $view_name = 'index';

        return $this->displayAttention($data, $request, $view_name);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function attentionsBranches(Request $request)
    {
        $data = Alert::whereNotNull('branch_id')
            ->orderBy('is_approved')
            ->orderBy('id', 'desc');
        $view_name = 'attentions_branches';

        return $this->displayAttention($data, $request, $view_name);
    }

    public function displayAttention($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'title') {
                $data = $data->whereTranslationLike('title', '%' . $query . '%');
            } else if ($search_type == 'content') {
                $data = $data->whereTranslationLike('content', '%' . $query . '%');
            }
        endif;

        $data_count = $data->count();

        return view($this->view_path . $view_name)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('attentions.create');
    }


    public function validateAttention($request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'title_ar' => 'required|string|max:191',
            'title_en' => 'required|string|max:191',
            'content_ar' => 'required|string',
            'content_en' => 'required|string',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        return $validatedData;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateAttention($request);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $image_name = null;
        if ($request->hasFile('image')) {
            $image_name = uploadImage($request->image, 'alert');
        }

        $attention = new Alert();
        $attention->status = $request->input('status', 0);
        $attention->image = $image_name;
        $attention->is_approved = 1;
        $attention->start_date = $request->start_date;
        $attention->end_date = $request->end_date;
        $attention->save();

        $attention->translateOrNew('ar')->title = $request->title_ar;
        $attention->translateOrNew('ar')->content = $request->content_ar;

        $attention->translateOrNew('en')->title = $request->title_en;
        $attention->translateOrNew('en')->content = $request->content_en;
        $attention->save();

        $fcm_title = $attention->title;
        $fcm_body = $attention->content;
        $image_path = $attention->image_path;
        if (is_null($attention->image)) {
            $image_path = null;
        }
        Fcm::send_MKA_notification($fcm_title, $fcm_body, null, $image_path);

        return response()->json([
            'status' => 200,
            'attention' => $attention,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attention = Alert::find($id);

        return response()->json([
            'id' => $attention->id,
            'branch' => $attention->branch->name ?? null,
            'title' => $attention->title,
            'content' => $attention->content,
            'start_date_carbon' => $attention->start_date_carbon,
            'end_date_carbon' => $attention->end_date_carbon,
            'created_at' => Carbon::Parse($attention->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($attention->updated_at)->format('Y/m/d'),
            'status' => $attention->status,
            'image' => $attention->image,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attention = Alert::find($id);
        $attention_ar = $attention->translate('ar');
        $attention_en = $attention->translate('en');

        return response()->json([
            'id' => $attention->id,
            'title_ar' => $attention_ar->title ?? null,
            'title_en' => $attention_en->title ?? null,
            'content_ar' => $attention_ar->content ?? null,
            'content_en' => $attention_en->content ?? null,
            'start_date' => Carbon::Parse($attention->start_date)->format('Y-m-d'),
            'end_date' => is_null($attention->end_date) ? null : Carbon::Parse($attention->end_date)->format('Y-m-d'),
            'status' => $attention->status,
            'image' => $attention->image_path,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateAttention($request);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $attention = Alert::find($id);

        $image_name = $attention->image;
        if ($request->hasFile('image')) {
            deleteImage('/storage/alert/' . $attention->image);
            $image_name = uploadImage($request->image, 'alert');
        }

        $attention->status = $request->input('status', 0);
        $attention->image = $image_name;
        $attention->is_approved = 1;
        $attention->start_date = $request->start_date;
        $attention->end_date = $request->end_date;
        $attention->save();

        $attention->translateOrNew('ar')->title = $request->title_ar;
        $attention->translateOrNew('ar')->content = $request->content_ar;

        $attention->translateOrNew('en')->title = $request->title_en;
        $attention->translateOrNew('en')->content = $request->content_en;
        $attention->save();

        return response()->json([
            'status' => 200,
            'attention' => $attention,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attention = Alert::find($id);
        deleteImage('/storage/attentions/' . $attention->image);
        $attention->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Alert::count()
        ],
            200
        );
    }


    public function accept(Request $request)
    {
        $id = $request->items_id;
        $alert = Alert::find($id);
        if($alert->is_approved != 0 or is_null($alert->branch_id)){
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($alert, $id) {
            $alert->update(['is_approved' => 1]);
            responseNotify($id, Alert::class, ProviderNotification::class);
            $alert->notify(new ProviderNotification());
        });

        Fcm::send_MKA_notification($alert->title, $alert->content, null, $alert->image_path);

        return response()->json([
            'status' => 200,
            'data' => Alert::find($id),
            'title' => Lang::get('admin.accepted_successfully'),
            'message' => ''
        ]);
    }

    public function reject(Request $request)
    {
        $id = $request->items_id;
        $alert = Alert::find($id);
        if($alert->is_approved != 0 or is_null($alert->branch_id)){
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($alert, $id) {
            $alert->update(['is_approved' => 3]);
            responseNotify($id, Alert::class, ProviderNotification::class);
            $alert->notify(new ProviderNotification());
        });

        return response()->json([
            'status' => 200,
            'data' => Alert::find($id),
            'title' => Lang::get('admin.rejected_successfully'),
            'message' => ''
        ]);
    }
}
