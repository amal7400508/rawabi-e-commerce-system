<?php

namespace App\Http\Controllers;

use App\Http\Resources\FalterCarrierResource;
use App\Http\Resources\FalterProductResource;
use App\Models\Admin;
use App\Models\Alert;
use App\Models\BalanceTransaction;
use App\Models\Branch;
use App\Models\BranchesCategory;
use App\Models\BranchTranslation;
use App\Models\Carrier;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Category;
use App\Exports\RequestExport;
use App\Models\CategoryProduct;
use App\Models\Ceiling;
use App\Models\DeliveryPrice;
use App\Models\Hospitality;
use App\Models\MainCategoryCategory;
use App\Models\Offer;
use App\Models\PharmacyCategory;
use App\Models\PointHistory;
use App\Models\PrescriptionReply;
use App\Models\Product;
use App\Models\ProductDiscount;
use App\Models\ProductPrice;
use App\Models\Provider;
use App\Models\SpecialProduct;
use App\Models\SystemNotification;
use App\Notifications\Admin\ProviderNotification;
use App\Traits\PaymentFromWallet;
use App\Traits\RequestTrait;
use App\Models\Request;
use App\Traits\VolumeDiscount;
use App\Models\User;
use App\Models\userWallet;
use App\Utilities\Helpers\Fcm;
use Carbon\Carbon;
use App\Models\Coupon;
use Illuminate\Http\Request as RequestPost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Audit;
use function MongoDB\BSON\toJSON;

class RequestController extends Controller
{
    use RequestTrait, PaymentFromWallet, VolumeDiscount;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws \Exception
     */
    public function index(RequestPost $requestPost)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;

        $request_data = Request::join('carts', 'requests.cart_id', 'carts.id')
            ->join('users', 'carts.user_id', 'users.id')
            ->where('requests.status', '=', 'requested');

        if (!is_null($requestPost['search_type']) && !is_null($requestPost['query'])):
            if ($requestPost['search_type'] == 'number') {
                $request_data = $request_data->where('requests.request_number', '=', $requestPost['query']);
            } else if ($requestPost['search_type'] == 'name') {
                $request_data = $request_data->where('users.name', 'like', '%' . $requestPost['query'] . '%');
            } else if ($requestPost['search_type'] == 'phone') {
                $request_data = $request_data->where('users.phone', '=', $requestPost['query']);
            }
        endif;

        $request_data = $request_data->select('requests.*')
            ->orderBy('requests.updated_at', 'desc');

        $tracking_request_count = $request_data->count();
        return view('managements.requests_management.request.index')
            ->with('carriers', Carrier::where('status', 1)->get())
            ->with('branch', Branch::all())
            ->with('category', Category::where('status', 1)->get())
            ->with('tracking_requests', $request_data->paginate($table_length))
            ->with('tracking_request_count', $tracking_request_count)
            ->with('pagination_links', [
                    'filter_name' =>
                        isset($_GET['filter_name']) ? $_GET['filter_name'] : '',
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    public function getStatus()
    {
        $sys_notify = Request::where(function ($q) {
            $q->whereNotNull('status');
        })
            ->where('status', '!=', 'requested')
//            ->whereNull('response_at')
            ->orderby('updated_at', 'desc')->count();
        return $sys_notify;
    }

    /**
     * Show the form for index a new resource.
     *
     * @param RequestPost $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trackingRequest(RequestPost $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;

        $request_data = Request::join('carts', 'requests.cart_id', 'carts.id')
            ->join('users', 'carts.user_id', 'users.id');

        if (!is_null($request['filter_name'])):
            if ($request['filter_name'] == 'requested') {
                $request_data = $request_data->where('requests.status', '=', 'requested');
            } else if ($request['filter_name'] == 'received') {
                $request_data = $request_data->where('requests.status', '=', 'received');
            } else if ($request['filter_name'] == 'repair') {
                $request_data = $request_data->where('requests.status', '=', 'repair');
            } else if ($request['filter_name'] == 'deliver') {
                $request_data = $request_data->where('requests.status', '=', 'deliver');
            } else if ($request['filter_name'] == 'delivered') {
                $request_data = $request_data->where('requests.status', '=', 'delivered');
            } else if ($request['filter_name'] == 'canceled') {
                $request_data = $request_data->where('requests.status', '=', 'canceled');
            }
        endif;
        if (!is_null($request['search_type']) && !is_null($request['query'])):
            if ($request['search_type'] == 'number') {
                $request_data = $request_data->where('requests.request_number', '=', $request['query']);
            } else if ($request['search_type'] == 'name') {
                $request_data = $request_data->where('users.name', 'like', '%' . $request['query'] . '%');
            } else if ($request['search_type'] == 'phone') {
                $request_data = $request_data->where('users.phone', '=', $request['query']);
            }
        endif;

        $request_data = $request_data->select('requests.*')
            ->where('requests.status', '!=', 'requested')
            ->orderBy('requests.updated_at', 'desc');

        $tracking_request_count = $request_data->count();
        return view('managements.requests_management.request.tracking_request')
            ->with('carriers', Carrier::where('status', 1)->get())
            ->with('branch', Branch::all())
            ->with('tracking_requests', $request_data->paginate($table_length))
            ->with('tracking_request_count', $tracking_request_count)
            ->with('pagination_links', [
                    'filter_name' =>
                        isset($_GET['filter_name']) ? $_GET['filter_name'] : '',
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        if (Gate::check('show request') or Gate::check('show request_branch')) {
        $request = Request::find($id);
        $status = $request->status;
        if ($status == 'requested' or $status == 'on_branch' or $status == 'on_the_way' or $status == 'received' or $status == 'repair' or $status == 'deliver' or $status == 'delivered') {
            return response()->json([
                'status' => $request->status,
                'id' => $request->id,
                'is_active' => $request->is_active,
                'branch_id' => $request->branch_id,
                'carrier_id' => $request->carrier_id,
                'receiving_type' => $request->receiving_type,
            ]);
        }
//        }
        return response()->json(['error' => 'error'], 401);
    }

    public function falterStatus($id)
    {
        $today = Carbon::today()->toDateTimeString();
        $tomorrow = Carbon::tomorrow()->toDateTimeString();
        $request = Request::where('status', $id)
            ->whereBetween('updated_at', [$today, $tomorrow])
            ->groupBy('carrier_id')
            ->orderby('updated_at', 'desc')
            ->select('requests.*', DB::raw('sum(cache_payment) as cache_payment_today'))
            ->get();

        $data = FalterCarrierResource::collection($request);

        return response()->json($data);
    }

    /**
     * Assigning the request to a carrier
     * Update the specified resource in storage.
     *
     * This function is accessed if the request is via a carrier
     *
     * @param \Illuminate\Http\Request $request_post
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestPost $request_post, $id)
    {
//        if (Gate::check('show request') or Gate::check('show request_branch')) {
        $is_active = $request_post->available;
        $request = Request::find($id);
        //  منع تغيرر حالة الطلب في حالة كان الطلب ملغي
        if ($request->is_active == 2)
            return response()->json(['_error' => lang::get("admin.error_cancel_request")]);

        $acceptable = true;
        if ($is_active == 1) {
            $wallet = $request->cart->user->wallet;
            $coupon = null;
            if (isset($request->coupon_id)) {
                $coupon = Coupon::find($request->coupon_id);
            }
            $cart = Cart::FindOrFail($request->cart_id);
            $input = $request;

            $currency = $this->checkCurrency($cart, $request->payment_type);
            if ($currency['status'] == 401) {
                return response()->json(['error' => $currency['data']], 401);
            }

            if (is_null($coupon)) {
                // المبلغ المتوجب خصمة او دفعة في حالة عدم وجود تخفيض كوبون
                $remaining_amount = $cart['price_sum'];
            } else {
                // المبلغ المتوجب خصمة او دفعة في حالة وجود تخفيض كوبون
                $remaining_amount = ($coupon['value_type'] == 'percentage')
                    ? $cart['price_sum'] - (($coupon['price'] / 100) * $cart['price_sum'])
                    : max(($cart['price_sum'] - $coupon['price']), 0);
            }

            $validator = Validator::make($request_post->all(), [
//                    'branch_id' => 'required',
                'carrier_id' => ['required',
                    function ($attribute, $value, $fail) use ($input, $cart, $wallet, $coupon, $request, $remaining_amount, $currency) {
                        if ($request->payment_type === 'fromWallet') {
                            if (!isset($wallet['current_balance']))
                                $fail(lang::get("admin.the_user_don_not_have_a_balance_yet") . '.');

                            /**
                             * todo: Minimum for request 2000 YER
                             */
                            if ($currency['data'] == "ريال يمني") {
                                $ceiling = Ceiling::first()->price ?? 0;
                                if ($cart['price_sum'] < $ceiling)
                                    $fail(lang::get("admin.the_minimum_request_must_be") . ' ' . $ceiling . '.');
                            }
                            //if there is a balance for the user check if the balance is enough and the user not confirm the purchase
                            if ($remaining_amount > $wallet['current_balance'])
                                $fail(lang::get("admin.the_wallet_price_is_not_enough") . '.');

                        }
                    }
                ],
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->messages()], 401);
            }

            $currency = $currency['data'];
            DB::transaction(function () use (
                $request,
                $request_post,
                $is_active,
                $wallet,
                $remaining_amount,
                $input,
                $currency
            ) {

                // todo: العمليات المالية
                $this->financialAccounts($input, $wallet, $remaining_amount, $request, $currency);

                $request->is_active = $is_active;
//                $request->branch_id = $request_post->branch_id;
                $request->carrier_id = $request_post->carrier_id;
                $request->status = 'received';

                /*$user_id = $request_post->carrier_id ;
                $fcm_body_ar = $fcm_body = is_null($request_post->cancel_text) ?
                    ' عذراً لقد تم إلغاء طلبك رقم: ' . $request->request_number
                    : $request_post->cancel_text . ' رقم: ' . $request->request_number;

                $fcm_body_en = $fcm_body = is_null($request_post->cancel_text) ?
                    ' Sorry your request has been canceled no: ' . $request->request_number
                    : $request_post->cancel_text . ' رقم: ' . $request->request_number;


                $fcm_body = app()->getLocale() == 'ar' ? $fcm_body_ar : $fcm_body_en;
                $fcm_title = app()->getLocale() == 'ar' ? 'إلغاء الطلب' : 'Cancel request';

                $acceptable = false;

                // Send notification via Fire pace
                Fcm::send_MKA_notification($fcm_title, $fcm_body, $user_id);*/
            });

            $message = Lang::get('admin.edited_successfully');

        } else {
            // منع إلغاء الطلب اذا تمت الموافقة علية
            if ($request->status != 'requested')
                return response()->json(['_error' => lang::get("admin.error_cannot_cancel")]);

            DB::transaction(function () use ($request, $request_post) {
                $user_id = Cart::find($request->cart_id)->user_id;
//                    $this->cancel($request->id, $user_id);
                $request->carrier_id = null;
                $request->cancel_note = $request_post->cancel_text;
                $request->status = "canceled";
                $request->is_active = 2;

                $fcm_body_ar = $fcm_body = is_null($request_post->cancel_text) ?
                    ' عذراً لقد تم إلغاء طلبك رقم: ' . $request->request_number
                    : $request_post->cancel_text . ' رقم: ' . $request->request_number;

                $fcm_body_en = $fcm_body = is_null($request_post->cancel_text) ?
                    ' Sorry your request has been canceled no: ' . $request->request_number
                    : $request_post->cancel_text . ' رقم: ' . $request->request_number;


                $fcm_body = app()->getLocale() == 'ar' ? $fcm_body_ar : $fcm_body_en;
                $fcm_title = app()->getLocale() == 'ar' ? 'إلغاء الطلب' : 'Cancel request';

                $acceptable = false;

                // Send notification via Fire pace
                Fcm::send_MKA_notification($fcm_title, $fcm_body, $user_id);
            });

            $message = Lang::get('admin.canceled_successfully');
        }
        $request->save();

//        responseNotify($request->id, 'App\V1\Models\Request');

        // response_at
        $app_notify = SystemNotification::where('notifiable_type', 'App\V1\Models\Request')
            ->where('type', '!=', ProviderNotification::class)
            ->where('notifiable_id', $request->id);

        if (!is_null($app_notify->first()->data ?? null)) {
            $data_notify = $app_notify->first()->data;
            $app_notify->update([
                'response_at' => Carbon::now()->toDateTime(),
                'data' => json_encode([
                    'id' => $data_notify['id'],
                    'branch_id' => $data_notify['branch_id'],
                    'provider_id' => $data_notify['provider_id'],
                    'status' => $acceptable
                ])
            ]);
        }

//        $this->readAtNotification($id);

        return response()->json([
            'success' => $message
        ]);
//        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function requestFromBranch(RequestPost $request)
    {
        /*$cart_details = $this->cartDeatils();
        if (isset($request['filter_name'])):
            if ($request['filter_name'] == 'requested'):
                $request = $this->requestedQuery($cart_details);
            elseif ($request['filter_name'] == 'repair'):
                $request = $this->repairQuery($cart_details);
            elseif ($request['filter_name'] == 'canceled'):
                $request = $this->canceledQuery($cart_details);
            elseif ($request['filter_name'] == 'completedRequest'):
                $request = $this->completedRequestQuery($cart_details);
            elseif ($request['filter_name'] == 'done'):
                $request = $this->deliveredQuery($cart_details);
            else:
                $request = $this->defaultRedirection($cart_details);
            endif;
        else:
            $request = $this->defaultRedirection($cart_details);
        endif;
        if (request()->ajax()) {
            return $this->indexDataTable($request);
        }
        return view('request.request_from_branch')->with('branch', Branch::all());*/
    }

    /**
     * show details request from branch
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showDetailsRequestFromBranch($id)
    {
        $request = Request::find($id);
        if ($request->delivery_pricing == 0){
            $delivery_price = DeliveryPrice::find($request->delivery_price_id)->price ?? 0;
        } else{
            $delivery_price = $request->delivery_pricing;
        }
        $carrier_price = 'YER ' . $delivery_price;
        if ($request->branch_count == 1) {
            $branch = isset(Branch::find($request->branch_id)->name) ?
                Branch::find($request->branch_id)->name : '';
        } else {
            $cart_details_branch = CartDetail::where('cart_id', $request->cart_id)->pluck('branch_id')->toArray();
            $branch = BranchTranslation::whereIn('branch_id', $cart_details_branch)->where('locale', 'ar')->pluck('name')->toArray();
        }
        $cart_id = $request->cart_id;
        $cart = Cart::find($cart_id)->user_id;
        $user = User::find($cart);
        $cart_detail = CartDetail::where('cart_id', $cart_id)->get();
        $sum = 0;
        $price_sum = 0;
        $product[] = '';
        $price[] = '';
        $unit[] = '';
        $product_note[] = '';
        $cart_detail_id[] = '';
        $total_quantity[] = '';
        $product_number[] = '';
        $request_type[] = '';
        $product_discount_id[] = '';
        $insurance_company_amount = 0;
        foreach ($cart_detail as $cart_details) {
            $discount = 0;
            if (!is_null($cart_details->product_discount_id)
                && $cart_details->product_price_id == $cart_details->productDiscount->product_price_id
            ) {
                $discount = $cart_details->productDiscount->value;
                $discount = ($cart_details->productDiscount->value_type == 'percentage') ?
                    $discount . ' %' : $discount . ' YER';
            }

            $product_discount_id[] .= $discount;

            $sum = $sum + $cart_details->quantity;
            $product_price = ProductPrice::find($cart_details->product_price_id);
            $currency = null;

            if ($cart_details->item_type == 'offer' || $cart_details->item_type == 'offer_app') {
                $request_type[] .= __('admin.offer');
                $offer = Offer::find($cart_details->offer_id);
                $currency = currency($offer->currency) . ' ';
                $price[] .= $currency . $offer->price;
                $price__ = $offer->price;
                $unit[] .= '';
                $product_note[] .= '';
                $product_number[] .= $offer->id;
                $product[] .= $offer->name;
            } elseif ($cart_details->item_type == 'product') {
                $request_type[] .= __('admin.a_product');
                $product_row = Product::find($product_price->product_id);
                $currency = currency($product_price->currency) . ' ';
                $price[] .= $currency . $product_price->price;
                $unit[] .= $product_price->unit->name ?? '';
                if (app()->getLocale() == 'ar') {
                    $product_note[] .= $product_price->note_ar ?? '';
                } else {
                    $product_note[] .= $product_price->note_en ?? '';
                }
                $product_number[] .= $product_row->number;
                $price__ = $product_price->price;
                $product[] .= $product_row->name;
            } elseif ($cart_details->item_type == 'special') {
                $request_type[] .= __('admin.a_special_product');
                $SpecialProduct = SpecialProduct::find($cart_details->special_product_id);
                $currency = currency($SpecialProduct->currency) . ' ';
                $price[] .= $currency . $SpecialProduct->price ?? 0;
                $price__ = $SpecialProduct->price ?? 0;
                $unit[] .= '';
                $product_note[] .= '';
                $product_number[] .= $SpecialProduct->id ?? 0;
                $product[] .= $SpecialProduct->name ?? null;
            } elseif ($cart_details->item_type == 'prescription_reply') {
                $request_type[] .= __('admin.prescription');
                $prescription_reply = PrescriptionReply::find($cart_details->prescription_reply_id);
                $currency = currency($prescription_reply->currency) . ' ';
                $price[] .= $currency . $prescription_reply->medicine_price ?? 0;
                $price__ = $prescription_reply->medicine_price ?? 0;
                $unit[] .= '';
                $product_note[] .= '';
                $product_number[] .= $prescription_reply->id ?? '';
                $product[] .= $prescription_reply->medicine_name ?? '';
            }
            $quantity_total = $cart_details->quantity;
            $cart_detail_id[] .= $cart_details->id;
            if ($cart_details->item_type == 'hospitality') {
                $quantity_total = 1;
            }

            if (!is_null($cart_details->product_discount_id)
                && $cart_details->product_price_id == $cart_details->productDiscount->product_price_id
            ) {
                $discount = $cart_details->productDiscount->value;
                $price__ = ($cart_details->productDiscount->value_type == 'percentage')
                    ? $price__ - (($discount / 100) * $price__)
                    : max(($price__ - $discount), 0);
            }
            $total_quantity[] .= $currency . ($price__ * $quantity_total);
            $price_sum = $price_sum + ($price__ * $quantity_total);
        }
        $coupon = null;
        $coupon_value_type = null;
        $remaining_amount = $price_sum;

        if ($request->coupon_id != null) {
            $coupons = Coupon::find($request->coupon_id) ?? exit();
            $coupon_value_type = $coupons->value_type;
            $coupon = $coupons->price;
            $remaining_amount = ($coupons->value_type == 'percentage')
                ? $price_sum - (($coupon / 100) * $price_sum)
                : max(($price_sum - $coupon), 0);
        }

        // volume_discount
        $discount_value = null;
        $discount_value_type = null;
        if ($request->volume_discount_id != null) {
            $volume_discount = \App\Models\VolumeDiscount::find($request->volume_discount_id) ?? null;
            if (!is_null($volume_discount)) {
                if ($volume_discount->branch_id == $request->branch_id && $volume_discount->type == 'minimum') {
                    $discount_value_type = $volume_discount->value_type;
                    $discount_value = $volume_discount->value;
                    $remaining_amount = ($discount_value_type == 'percentage')
                        ? $remaining_amount - (($discount_value / 100) * $remaining_amount)
                        : max(($remaining_amount - $discount_value), 0);
                } elseif ($volume_discount->branch_id == $request->branch_id &&
                    $volume_discount->type == 'balance' &&
                    $request->payment_type == 'fromWallet') {
                    $discount_value_type = $volume_discount->value_type;
                    $discount_value = $volume_discount->value;
                    $remaining_amount = ($discount_value_type == 'percentage')
                        ? $remaining_amount - (($discount_value / 100) * $remaining_amount)
                        : max(($remaining_amount - $discount_value), 0);
                } elseif ($volume_discount->branch_id == $request->branch_id && $volume_discount->type == 'delivery') {
                    $discount_value_type = $volume_discount->value_type;
                    $discount_value = $volume_discount->value;
                    $delivery_price = ($discount_value_type == 'percentage')
                        ? $delivery_price - (($discount_value / 100) * $delivery_price)
                        : max(($delivery_price - $discount_value), 0);
                }
            }
        }

        if (!is_null($request->cart->prescription->insurance_company_id ?? null)) {
            $insurance_company_amount = (($request->cart->prescription->insurance_rate * $remaining_amount) / 100);
        }

        $remaining_amount = $remaining_amount - $insurance_company_amount;
        $city_of_request = "<span class='category-color'>" . __('admin.no_data') . "</span>";
        if (!is_null($request->state_id)) {
            $city_of_request = $request->state->name;
        }

        return response()->json([
            'city_of_request' => $city_of_request,
            'discount_value' => $discount_value,
            'discount_value_type' => $discount_value_type,
            'coupon_value_type' => $coupon_value_type,
            'product_discount_id' => $product_discount_id,
            'cart_detail_id' => $cart_detail_id,
            'total_quantity' => $total_quantity,
            'product_number' => $product_number,
            'unit' => $unit,
            'product_note' => $product_note,
            'note' => $request->note,
            'cancel_note' => $request->cancel_note,
            'updated_by' => is_null($request->updated_by)
                ? isset($request->carrier->name)
                    ? " ( " . __('admin.carrier') . " ) " . $request->carrier->name
                    : null
                : " ( " . __('admin.admin') . " ) " . Admin::find($request->updated_by)->name,
            'coupon' => $coupon,
            'remaining_amount' => $currency . $remaining_amount,
            'receiving_type' => $request->receiving_type,
            'user_name' => $user->full_name,
            'carrier' => isset($request->carrier->name) ? $request->carrier->name : '',
            'request_type' => $request_type,
            'branch' => $branch,
            'address' => isset($request->address->name) ? $request->address->name : '',
            'desc' => isset($request->address->desc) ? $request->address->desc : '',
            'price_sum' => $currency . $price_sum,
            'sum' => $sum,
            'payment_type' => $request->payment_type,
            'cache_payment' => $request->cache_payment,
            'request_no' => $request->request_number,
            'request_id' => $request->id,
            'cart_detail' => $cart_detail,
            'product' => $product,
            'product_price' => $price,
//            'carrier_price' => $carrier_price,
            'carrier_price' => 'YER ' . round($delivery_price),
            'insurance_company_amount' => $insurance_company_amount,
            'currency' => $currency,
            'created_at' => Carbon::Parse($request->created_at)->format('Y/m/d  H:i:s'),
            'created_at_diffForHumans' => $request->created_at->diffForHumans()
        ]);
    }

    public function editStatusAndCarrier(RequestPost $request_post, $id)
    {
        $is_active = $request_post->available;
        $request = Request::find($id);
        $status = $request->status;
        //  منع تغيرر حالة الطلب في حالة كان الطلب ملغي
        if ($request->is_active == 2)
            return response()->json(['_error' => lang::get("admin.error_cancel_request")]);

        if ($is_active == 2) {
            if ($request->status == 'received'
                || $request->status == 'received'
                || $request->status == 'received'
            ) {
                DB::transaction(function () use ($request, $request_post) {
                    $user_id = Cart::find($request->cart_id)->user_id;
                    $this->cancel($request->id);
                    $request->carrier_id = null;
                    $request->cancel_note = $request_post->cancel_text;
                    $request->status = "canceled";
                    $request->is_active = 2;
                    $request->save();

                    $fcm_body = is_null($request_post->cancel_text) ?
                        app()->getLocale() == 'ar' ? ' عذراً لقد تم إلغاء طلبك رقم: ' . $request->request_number :
                            ' Sorry your request has been canceled no: ' . $request->request_number
                        : $request_post->cancel_text . ' رقم: ' . $request->request_number;

                    $fcm_title = app()->getLocale() == 'ar'
                        ? 'إلغاء الطلب' : 'Cancel request';

                    // Send notification via Fire pace
                    Fcm::send_MKA_notification($fcm_title, $fcm_body, $user_id);
                });

                $message = Lang::get('admin.canceled_successfully');
                return response()->json([
                    'status' => $request->status,
                    'id' => $request->id,
                    'background_color_row' => $request->background_color_row,
                    'success' => $message
                ]);
            }
            return response()->json(['_error' => lang::get("admin.error_cancel_request")]);
        }

        $validator = Validator::make($request_post->all(), [
            'carrier_id' => ['nullable', 'integer',
                function ($attr, $value, $fail) use ($status) {
                    if ($status != 'received')
                        $fail($attr . ' error.');
                }
            ],
            'status' => [
                'required', 'string', 'max:191',
                /*'in:received,repair,deliver,delivered',
                function ($attr, $value, $fail) use ($status) {
                    if (
                        ($status == 'repair' && $value == 'received') ||
                        ($status == 'deliver' && ($value == 'repair' || $value == 'received')) ||
                        ($status == 'delivered' && ($value == 'deliver' || $value == 'repair' || $value == 'received'))
                    ) {
                        $fail($attr . ' is invalid.');
                    }
                }*/
            ]
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 401);
        }

        if ((!is_null($request_post->carrier_id))
            && ($request->status == 'received')
        ) {
            $request->carrier_id = $request_post->carrier_id;
        }

        $request->updated_by = auth()->user()->id;
        $request->status = $request_post->status;
        $request->save();

        $message = Lang::get('admin.edited_successfully');
        return response()->json([
            'status' => $request->status,
            'id' => $request->id,
            'background_color_row' => $request->background_color_row,
            'success' => $message
        ]);
    }

    public function reviewed(RequestPost $request_post, $id)
    {
        $request = Request::find($id);
        $status = $request->status;
        //  منع تغيرر حالة الطلب في حالة كان الطلب ملغي
        if ($request->is_active == 2)
            return response()->json(['_error' => lang::get("admin.error_cancel_request")], 401);

        $validator = Validator::make($request_post->all(), [
            'status' => ['required', 'string', 'in:reviewed',]
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 401);
        }

        if ($status == 'on_branch' or $status == 'delivered') {
            DB::transaction(function () use ($request, $request_post) {
                $user_id = Cart::find($request->cart_id)->user_id;

                $this->returnRequest($request->id, $request->payment_type);
                $request->carrier_id = null;
                $request->cancel_note = $request_post->cancel_text;
                $request->status = "reviewed";
                $request->save();

                $fcm_body = is_null($request_post->cancel_text) ?
                    app()->getLocale() == 'ar' ? ' عذراً لقد تم إلغاء طلبك رقم: ' . $request->request_number :
                        ' Sorry your request has been canceled no: ' . $request->request_number
                    : $request_post->cancel_text . ' رقم: ' . $request->request_number;

                $fcm_title = app()->getLocale() == 'ar'
                    ? 'إلغاء الطلب' : 'Cancel request';

                // Send notification via Fire pace
                Fcm::send_MKA_notification($fcm_title, $fcm_body, $user_id);
            });

            $message = "تم إرجاع الطلب";
            return response()->json([
                'status' => $request->status,
                'id' => $request->id,
                'background_color_row' => $request->background_color_row,
                'success' => $message
            ]);
        }

        return response()->json(['_error' => 'error']);
    }

    public function report(RequestPost $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = $this->getReport($request);
        $data_count = $data->count();

        return view('managements.requests_management.request.report')
//            ->with('providers', Provider::all())
            ->with('branches', Branch::all())
            ->with('coupons', Coupon::all())
            ->with('carriers', Carrier::get())
            ->with('categories', Category::all())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' => isset($_GET['table_length']) ? $_GET['table_length'] : '',
                ]
            );

        if (request()->ajax()) {
            return $this->indexDataTable(
                $this->getReport($request)
            );
        }

        return view('managements.requests_management.request.report')
            ->with('branch', Branch::where('status', 1)->get())
            ->with('coupons', Coupon::all())
            ->with('carrier', Carrier::where('status', 1)->get())
            ->with('categories', Category::where('status', 1)->get());
    }


//    public function report(RequestPost $request)
//    {
//        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
//        if ($table_length == '') $table_length = 10;
//
//        $data = $this->getReport($request);
//        $data_count = $data->count();;
//
//        return view('request.report')
//            ->with('branch', Branch::where('status', 1)->get())
//            ->with('coupons', Coupon::all())
//            ->with('carrier', Carrier::where('status', 1)->get())
//            ->with('categories', Category::where('status', 1)->get())
//            ->with('data', $data->paginate($table_length))
//            ->with('data_count', $data_count)
//            ->with('pagination_links', [
//                    'table_length' => isset($_GET['table_length']) ? $_GET['table_length'] : '',
//                ]
//            );
//    }


    /**
     * Fetch reports according to conditions
     *
     * @param $request
     * @return mixed
     */
    public function getReport($request)
    {
        if (!is_null($request->product_id)
            || !is_null($request->category)
            || !is_null($request->product_price)) {
            $cart_details = $this->cartDetailsWithProducts();
        } else {
            $cart_details = $this->cartDeatils();
        }
        if (!is_null($request->category)) {
            $cart_details = $cart_details
                ->where('products.category_id', $request->category);
        }
        if (!is_null($request->product_id)) {
            $cart_details = $cart_details
                ->where('products.id', $request->product_id);
        }
        if (!is_null($request->product_price)) {
            $cart_details = $cart_details
                ->where('cart_details.product_price_id', $request->product_price);
        }
        if ($request->request_type == "product"
            or $request->request_type == "special"
            or $request->request_type == "offer") {
            $cart_details = $cart_details
                ->where('cart_details.item_type', $request->request_type);
        }
//        $count_data = Request::join('carts', 'requests.cart_id', 'carts.id')
//            ->join('users', 'carts.user_id', 'users.id')
//            ->select('carts.user_id as id_user', DB::raw('count(carts.user_id) as count'))
//            ->where('requests.status', '!=', 'canceled')
////            ->where('users.id', 1)
//            ->groupBy('id_user');


        $requests_data = Request::joinSub($cart_details, 'cart_details', function ($join) {
            $join->on('requests.cart_id', '=', 'cart_details.id_cart');
        });
        $requests_data = $requests_data->join('carts', 'requests.cart_id', 'carts.id')
//            ->select('requests.*', 'carts.user_id as id_user')
            ->join('users', 'carts.user_id', 'users.id');

        if (!is_null($request->from)) {
            $requests_data = $requests_data->whereDate('requests.created_at', '>=', $request->from);
        }
        if (!is_null($request->carrier)) {
            $requests_data = $requests_data->where('requests.carrier_id', $request->carrier);
        }
        if (!is_null($request->to)) {
            $requests_data = $requests_data->whereDate('requests.created_at', '<=', $request->to);
        }
        if (!is_null($request->name)) {
            $requests_data = $requests_data->where('users.full_name', 'like', '%' . $request->name . '%');
        }
        if (!is_null($request->phone)) {
            $requests_data = $requests_data->where('users.phone', 'like', '%' . $request->phone . '%');
        }
        if (!is_null($request->gender)) {
            $requests_data = $requests_data->where('users.gender', $request->gender);
        }
        if (!is_null($request->payment_type)) {
            $requests_data = $requests_data->where('requests.payment_type', $request->payment_type);
        }
        if (!is_null($request->status)) {
            if ($request->status == 'delivered') {
                $requests_data = $requests_data->where(function ($q) {
                    $q->where('requests.status', 'on_branch')
                        ->orWhere('requests.status', 'delivered');
                });
            } else {
                $requests_data = $requests_data->where('requests.status', $request->status);
            }
        }
        if (!is_null($request->request_no)) {
            $requests_data = $requests_data->where('requests.id', $request->request_no);
        }
        if (!is_null($request->quantity)) {
            $requests_data = $requests_data->where('quantity', $request->quantity);
        }
        if (!is_null($request->branch_name)) {
            $requests_data = $requests_data->where('branch_id', $request->branch_name);
        }
        if (!is_null($request->carrier)) {
            $requests_data = $requests_data->where('carrier_id', $request->carrier);
        }
        if ($request->request_type == "coupons") {
            $requests_data = $requests_data->where('requests.coupon_id', '!=', null);
        }
        if (!is_null($request->coupon)) {
            $requests_data = $requests_data->where('requests.coupon_id', $request->coupon);
        }
        $requests_data = $requests_data->select('requests.*', 'quantity', 'carts.user_id as id_user')
            ->orderBy('created_at', 'desc');

//        $r = User::joinSub($requests_data, 'requests_data', function ($join) {
//            $join->on('users.id', '=', 'requests_data.id_user');
//        })
////            ->with('request')
////            ->withCount('cartRequest')
//            ->orderBy('users.cart_request_count', 'desc')
//            ->select('requests_data.*', 'users.name')
//        ;
//        dd($requests_data->get()[0]);

        return $requests_data;
    }

    public function cartDetailsWithProducts()
    {
        $cart_details = Cart::join('cart_details', 'carts.id', '=', 'cart_details.cart_id')
            ->join('product_prices', 'cart_details.product_price_id', 'product_prices.id')
            ->join('products', 'product_prices.product_id', 'products.id')
            ->select('cart_details.cart_id as id_cart', DB::raw('sum(cart_details.quantity) as quantity'))
            ->groupBy('id_cart');

        return $cart_details;
    }

    public function export(RequestPost $request)
    {
        $data = $this->getReport($request)->get();
        return Excel::download(
            new RequestExport($data),
            'requests-' . now() . '.xlsx'
        );
    }

    public function addToCart(RequestPost $request)
    {
        $cart_request = Request::find($request->request_id);
//        if ($cart_request->status != 'requested')
//            return response()->json(['_error' => 'You cannot modify the cart.']);

        $product_discount_id = ProductDiscount::active()
            ->where(
                'product_price_id', $request->products_price_id
            )
            ->first();

        $cart_id = $cart_request->cart_id;
        $cart_detail = new CartDetail();
        $cart_detail->quantity = $request->quantity;
        $cart_detail->item_type = 'product';
        $cart_detail->cart_id = $cart_id;
        $cart_detail->product_price_id = $request->products_price_id;

        if (!is_null($product_discount_id)) {
            $cart_detail->product_discount_id = $product_discount_id->id;
        }

        $cart_detail->save();

        // volume discount
        $this->volumeDiscount($request->request_id);

        return response()->json();
    }

    public function changeQuantity(RequestPost $request, $id)
    {

        $validatedData = Validator::make(
            $request->all(), [
            'quantity' => 'required|int|max:10000',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        CartDetail::where('id', $id)->update(['quantity' => $request->quantity]);

        return response()->json([
            'status' => 200,
            'data' => CartDetail::find($id),
            'title' => Lang::get('admin.edited_successfully'),
            'message' => ''
        ]);
    }

    /**
     * delete row from cart details
     *
     * @param $cart_detail_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFromCart($cart_detail_id)
    {
        $cart_detail = CartDetail::findOrFail($cart_detail_id);
        $request_status = Request::where('cart_id', $cart_detail->cart_id)->first()->status;
//        if ($request_status != 'requested')
//            return response()->json(['_error' => 'You cannot modify the cart.']);

        $cart_count = CartDetail::where('cart_id', $cart_detail->cart_id)->count();

        if ($cart_count <= 1) {
            return response()->json(
                ['error' => Lang::get('admin.you_cannot_completely_empty_the_basket') . '.'],
                401
            );
        }
        $cart_detail->delete();

        // volume discount
        $request = Request::where('cart_id', $cart_detail->cart_id)->first();
        $this->volumeDiscount($request->id);

        return response()->json();
    }

    public function getProductPrice($product_id)
    {
        $product_price = ProductPrice::where('product_id', $product_id)
            ->where('order_availability', 1)
            ->get();

        return response()->json([
            'product_name' => $product_price->first()->product->name ?? null,
            'data' => $product_price
        ]);
    }

    public function Audit($id)
    {
        $audits = Audit::where('auditable_id', $id)
            ->where(function ($query) {
                $query->where('auditable_type', 'App\Models\Request')
                    ->orWhere('auditable_type', 'App\Models\RequestAuditable');
            })
            ->get();

        $data = [];
        $admins_type = null;
        $admin_name = null;
        foreach ($audits as $audit) {
            if ($audit->admins_type == "App\Models\Admin") {
                $admins_type = __('admin.admin');
                $admin_name = $audit->admin->name ?? null;
            } elseif ($audit->admins_type == "App\Models\Carrier") {
                $admins_type = __('admin.carrier');
                $admin_name = Carrier::find($audit->admins_id)->name ?? null;
            }

            $carrier = Carrier::find($audit->new_values['carrier_id'] ?? 0);
            $old_status = $audit->old_values['status'] ?? null;
            $new_status = $audit->new_values['status'] ?? null;

            if (!is_null($new_status)) {
                $data[] = [
                    'status' => $new_status,
                    'old_status' => $old_status,
                    'carrier' => is_null($carrier) ? null : $carrier->name ?? null,
                    'admin_name' => $admin_name,
                    'admins_type' => $admins_type,
                    'diffForHumans' => $audit->created_at->diffForHumans(),
                    'created_at' => $audit->created_at->format('Y/m/d  H:i:s'),
                ];
            }
        }
        return response()->json([
            'data' => $data
        ]);
    }

    public function volumeDiscount($request_id)
    {
        $coupon = null;
        $cart_request = Request::find($request_id);
        $price_sum = $cart_request->cart->price_sum;
        if (isset($cart_request->coupon_id)) {
            $coupon = Coupon::find($cart_request->coupon_id);
        }

        if (is_null($coupon)) {
            // المبلغ المتوجب خصمة او دفعة في حالة عدم وجود تخفيض كوبون
            $remaining_amount = $price_sum;
        } else {
            // المبلغ المتوجب خصمة او دفعة في حالة وجود تخفيض كوبون
            $remaining_amount = ($coupon['value_type'] == 'percentage')
                ? $price_sum - (($coupon['price'] / 100) * $price_sum)
                : max(($price_sum - $coupon['price']), 0);
        }

        $volume_discount = $this->checkVolumeDiscount($remaining_amount);

        $volume_discount_id = null;
        if (!is_null($volume_discount)) {
            $volume_discount_id = $volume_discount->id;
        }
        $cart_request->volume_discount_id = $volume_discount_id;
        $cart_request->save();
    }


    public function getBranchCategories($branch_id)
    {
        return BranchesCategory::active()
            ->where('branch_id', $branch_id)
            ->get();

        //branch_name
    }

    public function reportExport(RequestPost $request)
    {
        $data = $this->getReport($request);
        $data_count = $data->count();

        return view('managements.requests_management.request.report_export')
            ->with('data', $data->paginate(100))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' => isset($_GET['table_length']) ? $_GET['table_length'] : '',
                ]
            );
    }
}
