<?php

namespace App\Http\Controllers;

use App\Models\AdvertisementType;
use App\Models\Branch;
use App\Models\PaidAdvertisement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class PaidAdvertisementController extends Controller
{
    public $view_path = 'managements.advertisements.paid_advertisements.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new PaidAdvertisement();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', $query);
            } else if ($search_type == 'type') {
                if ($query == 'صور منبثقة') $query = 'popup';
                else if ($query == 'اعلانات متحركة') $query = 'slider';
                else if ($query == 'Popup image') $query = 'popup';
                $data = $data->where('type', $query);
            } else if ($search_type == 'description') {
                $data = $data->whereTranslationLike('desc', '%' . $query . '%');
            } else if ($search_type == 'end_date') {
                $data = $data->where('end_date', 'like', '%' . $query . '%');
            }
        endif;

        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')
            ->with('advertisement_types', AdvertisementType::where('status', 1)->get())
            ->with('branches', Branch::active()->get())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paid_advertisement = PaidAdvertisement::find($id);

        $content = null;
        $text = $paid_advertisement->content;
        if ($paid_advertisement->type != 'text') {
            $text = null;
            $content = asset('storage/paid_advertisements' . '/' . $paid_advertisement->content);
        }

        return response()->json([
            'id' => $paid_advertisement->id,
            'branch' => $paid_advertisement->branch->name,
            'advertisement_type' => $paid_advertisement->advertisementType->name,
            'format' => $paid_advertisement->type,
            'title' => $paid_advertisement->title,
            'start_date' => $paid_advertisement->start_date,
            'end_date' => $paid_advertisement->end_date,
            'rank' => $paid_advertisement->rank,
            'status' => $paid_advertisement->status ? 1 : 0,
            'price' => $paid_advertisement->price,
            'content' => $content,
            'text' => $text,
            'updated_at' => Carbon::Parse($paid_advertisement->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($paid_advertisement->created_at)->format('Y/m/d'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->♥($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $content = $request['content'];

        if ($request['format'] == 'image' || $request['format'] == 'video') {
            $content = uploadImage($request['content'], 'paid_advertisements');
        }

        $status = $request->input('status', 'موقف');
        $paid_advertisement = new PaidAdvertisement();
        $paid_advertisement->branch_id = $request['branch_name'];
        $paid_advertisement->advertisement_type_id = $request['type'];
        $paid_advertisement->type = $request['format'];
        $paid_advertisement->title = $request['title'];
        $paid_advertisement->start_date = $request['start_date'];
        $paid_advertisement->end_date = $request['end_date'];
        $paid_advertisement->rank = $request['level'];
        $paid_advertisement->price = $request['price'];
        $paid_advertisement->content = $content;
        $paid_advertisement->status = $status == 1 ? 'نشط' : $status;
        $paid_advertisement->save();

        return response()->json([
            'status' => 200,
            'paid_advertisement' => $paid_advertisement,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paid_advertisement = PaidAdvertisement::find($id);

        return response()->json([
            'id' => $paid_advertisement->id,
            'branch_id' => $paid_advertisement->branch_id,
            'advertisement_type_id' => $paid_advertisement->advertisement_type_id,
            'type' => $paid_advertisement->type,
            'title' => $paid_advertisement->title,
            'content' => $paid_advertisement->content,
            'start_date' => $paid_advertisement->start_date,
            'end_date' => $paid_advertisement->end_date,
            'rank' => $paid_advertisement->rank,
            'status' => $paid_advertisement->status == 'نشط' ? 1 : 0,
            'price' => $paid_advertisement->price,
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->♥($request, false);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $paid_advertisement = PaidAdvertisement::find($id);

        $content = $paid_advertisement->content;

        if ($paid_advertisement['type'] == 'image' || $paid_advertisement['type'] == 'video') {
            if ($request->hasFile('content')) {
                deleteImage('/storage/paid_advertisements/' . $paid_advertisement->content);
                $content = uploadImage($request['content'], 'paid_advertisements');
            }
        }

        $status = $request->input('status', 'موقف');

        $paid_advertisement->branch_id = $request['branch_name'];
        $paid_advertisement->advertisement_type_id = $request['type'];
        $paid_advertisement->title = $request['title'];
        $paid_advertisement->start_date = $request['start_date'];
        $paid_advertisement->end_date = $request['end_date'];
        $paid_advertisement->rank = $request['level'];
        $paid_advertisement->price = $request['price'];
        $paid_advertisement->content = $content;
        $paid_advertisement->status = $status == 1 ? 'نشط' : $status;
        $paid_advertisement->save();
        return response()->json([
            'status' => 200,
            'paid_advertisement' => $paid_advertisement,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    public function ♥($request, $required = true)
    {
        $rule = $required ? 'required' : 'nullable';
        $validatedData = Validator::make(
            $request->all(), [
            'branch_name' => 'required|integer',
            'price' => 'required|numeric|max:99999999.99',
            'level' => 'required|numeric|in:1,2,3,4,5,6',
            'format' => [$rule, 'string', 'in:text,image,video',
                function ($attr, $value, $filed) use ($required) {
                    if (!$required && $value != null) $filed('error');
                }
            ],
            'title' => 'required|string',
            'status' => 'in:1',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date|after:start_date|max:191',
            'type' => 'required|integer',
        ]);

        $validatedData->sometimes(['content'], [$rule, 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'], function ($request) {
            return $request['format'] == 'image';
        });
        $validatedData->sometimes(['content'], [$rule, 'mimes:mp4', 'max:200000'], function ($request) {
            return $request['format'] == 'video';
        });
        $validatedData->sometimes(['content'], 'required|string', function ($request) {
            return $request['format'] == 'text';
        });

        return $validatedData;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paid_advertisement = PaidAdvertisement::find($id);
        if ($paid_advertisement->type != 'text') {
            deleteImage('/storage/paid_advertisements/' . $paid_advertisement->content);
        }
        $paid_advertisement->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => PaidAdvertisement::count()
        ],
            200
        );
    }
}
