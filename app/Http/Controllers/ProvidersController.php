<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\InsuranceCompany;
use App\Models\Provider;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class ProvidersController extends Controller
{
    /**
     * @var string
     */
    public $view_path = 'managements.providers_management.providers.';

    /**
     * Display providers data.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = Provider::orderBy('is_approved')->orderBy('id', 'desc');
        $view_name = 'index';

        return $this->display($data, $request, $view_name);
    }

    public function display($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'name') {
                $data = $data->where('name', 'like', '%' . $query . '%');
            } else if ($search_type == 'phone') {
                $data = $data->where('phone', 'like', '%' . $query . '%');
            }
        endif;

        $data_count = $data->count();

        return view($this->view_path . $view_name)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $carrier = Provider::find($id);

        return response()->json([
            'id' => $carrier->id,
            'name' => $carrier->name,
            'phone' => $carrier->phone,
            'email' => $carrier->email,
            'gender' => $carrier->gender,
            'status' => $carrier->status,
            'type' => $carrier->type_name,
            'telephone' => $carrier->telephone,
            'whats_app' => $carrier->whats_app,
            'image' => $carrier->image_path,
            'updated_at' => Carbon::Parse($carrier->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($carrier->created_at)->format('Y/m/d'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'status' => 'required|in:active,attitude',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $provider = Provider::find($id);
        $provider->update([
            'status' => ($request->status == 'active') ? 1 : 0
        ]);

        return response()->json([
            'id' => $id,
            'status' => $provider->status,
            'background_color_row' => $provider->background_color_row,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }
    public function updateAvailable(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'available' => 'required|in:active,attitude',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $provider = Provider::find($id);
        $provider->update([
            'available' => ($request->available == 'active') ? 1 : 0
        ]);

        return response()->json([
            'id' => $id,
            'available' => $provider->available,
            'background_color_row' => $provider->background_color_row,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    public function accept(Request $request)
    {
        $id = $request->items_id;
        $provider = Provider::find($id);
        if ($provider->is_approved != 0) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($provider, $id) {
            $provider->update(['is_approved' => 1]);
            Provider::find($id)->syncRoles('administrator');

//            Branch::where('provider_id', $provider->id)->first()->update(['is_approved' => 1]);
            $branch = $provider->branch->first();

            if ($provider->type == 'insurance') {
                $insurance_company = new InsuranceCompany();
                $insurance_company->provider_id = $provider->id;
                $insurance_company->type = 'healthy';
                $insurance_company->email = $provider->email;
                $insurance_company->cell_phone = $provider->phone;
                $insurance_company->tele_phone = $provider->telephone;
                $insurance_company->save();
                $insurance_company->translateOrNew('ar')->name = $branch->translate('ar')->name;
                $insurance_company->translateOrNew('en')->name = $branch->translate('en')->name;
                $insurance_company->save();
            }

            if (!count($branch->wallet)) {
                $currencies = ['ريال يمني', 'دولار أمريكي', 'ريال سعودي'];
                foreach ($currencies as $currency) {
                    Wallet::create([
                        'current_balance' => 0.00,
                        'currency' => $currency,
                        'walletable_id' => $branch->id,
                        'walletable_type' => Branch::class
                    ]);
                }
            }

            responseNotify($provider->id, Provider::class);
        });

        return response()->json([
            'status' => 200,
            'data' => Provider::find($id),
            'title' => Lang::get('admin.accepted_successfully'),
            'message' => ''
        ]);
    }

    public function reject(Request $request)
    {
        $id = $request->items_id;
        $provider = Provider::find($id);
        if ($provider->is_approved != 0) {
            return response()->json(['error' => 'error'], 401);
        }

        $provider->update(['is_approved' => 3]);
        responseNotify($provider->id, Provider::class);

        return response()->json([
            'status' => 200,
            'data' => Provider::find($id),
            'title' => Lang::get('admin.rejected_successfully'),
            'message' => ''
        ]);
    }

    public function changeLevel(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'level' => 'required|int|max:10000',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        Provider::where('id', $id)->update(['level' => $request->level]);

        return response()->json([
            'status' => 200,
            'data' => Provider::find($id),
            'title' => Lang::get('admin.edited_successfully'),
            'message' => ''
        ]);
    }
}
