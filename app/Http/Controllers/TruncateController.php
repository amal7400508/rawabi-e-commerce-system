<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class TruncateController extends Controller
{

    /**
     * @param Request $request
     * @param $value
     * @return array|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $value)
    {
        $this->validate($request, [
            'type' => 'in:empty,delete',
        ]);

//        $path = 'http://127.0.0.1:9090/smart-delivery/auth.xml';
//        $data = simplexml_load_file($path);
//        return $data->USERNAME;

//        if (Hash::check($request->user_name, $data->USERNAME)
//            and Hash::check($request->pass, $data->PASS)) {

        Schema::disableForeignKeyConstraints();
        if ($request->type == 'empty') {
            DB::table($value)->truncate();
        } else {
            DB::table($value)->delete();
        }
        Schema::enableForeignKeyConstraints();

        return array(
            'status' => true,
            'name' => $value,
            'type' => $request->type
        );
//        }

        return abort(404);
    }
}
