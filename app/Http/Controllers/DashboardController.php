<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Carrier;
use App\Models\Product;
use App\Models\SpecialProduct;
use App\Models\User;
use App\Models\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class DashboardController extends Controller
{
    public function Appointment()
    {
        $dates = collect();
        $posts = Request::groupBy('date')
            ->orderBy('date')
            ->get([
                DB::raw('DATE( created_at ) as date'),
                DB::raw('COUNT( * ) as "count"')
            ])
            ->pluck('count', 'date');
        $dates = $dates->merge($posts);
        $rep = [
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
        ];
        foreach ($dates as $key => $value) {
            $rep[Carbon::create($key)->dayOfWeek] += $value;
        }
        return $rep;
    }

    public function AppointmentRequestFromDelivery()
    {
        $dates = collect();
        $posts = Request::groupBy('date')
            /*->where('receiving_type','delivery')*/
            ->orderBy('date')
            ->get([
                DB::raw('DATE( created_at ) as date'),
                DB::raw('COUNT( * ) as "count"')
            ])
            ->pluck('count', 'date');
        $dates = $dates->merge($posts);
        $rep = [
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
        ];
        foreach ($dates as $key => $value) {
            $rep[Carbon::create($key)->dayOfWeek] += $value;
        }
        return $rep;
    }

    public function AppointmentHasBeenApproved()
    {
        $dates = collect();
        $posts = Request::where('is_active', '1')
            ->groupBy('date')
            ->orderBy('date')
            ->get([
                DB::raw('DATE( created_at ) as date'),
                DB::raw('COUNT( * ) as "count"')
            ])
            ->pluck('count', 'date');
        $dates = $dates->merge($posts);
        $rep = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0,
        ];
        foreach ($dates as $key => $value) {
            $rep[Carbon::create($key)->dayOfWeek] += $value;
        }
        return $rep;
    }

    public function AppointmentNotApproved()
    {
        $dates = collect();
        $posts = Request::where('is_active', '0')
            ->groupBy('date')
            ->orderBy('date')
            ->get([
                DB::raw('DATE( created_at ) as date'),
                DB::raw('COUNT( * ) as "count"')
            ])
            ->pluck('count', 'date');
        $dates = $dates->merge($posts);
        $rep = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0,
        ];
        foreach ($dates as $key => $value) {
            $rep[Carbon::create($key)->dayOfWeek] += $value;
        }
        return $rep;
    }

    public function AppointmentCanceled()
    {
        $dates = collect();
        $posts = Request::where('status', 'canceled')
            ->groupBy('date')
            ->orderBy('date')
            ->get([
                DB::raw('DATE( created_at ) as date'),
                DB::raw('COUNT( * ) as "count"')
            ])
            ->pluck('count', 'date');
        $dates = $dates->merge($posts);
        $rep = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0,
        ];
        foreach ($dates as $key => $value) {
            $rep[Carbon::create($key)->dayOfWeek] += $value;
        }
        return $rep;
    }

    public function AppointmentSpecialRequests()
    {
        $dates = collect();
        $posts = SpecialProduct::
        groupBy('created_at')
            ->
            get([
                DB::raw('DATE( created_at ) as date'),
                DB::raw('COUNT( * ) as "count"')
            ])
            ->pluck('count', 'date');
        $dates = $dates->merge($posts);
        $rep = [
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
        ];
        foreach ($dates as $key => $value) {
            $rep[Carbon::create($key)->dayOfWeek] += $value;
        }
        return $rep;
    }

    public function index()
    {
        if (!Gate::check('show dashboard')) {
            return redirect('/profile_management/profile');
        }
        $daysName = [
            0 => Lang::get('admin.SUN'),
            1 => Lang::get('admin.MON'),
            2 => Lang::get('admin.TUE'),
            3 => Lang::get('admin.WED'),
            4 => Lang::get('admin.THU'),
            5 => Lang::get('admin.FRI'),
            6 => Lang::get('admin.SAT'),
        ];
        $weekday = $daysName[Carbon::now()->dayOfWeek];
        $users = User::count();
        $carrier = Carrier::count();
        $branch = Branch::count();
        $request = Request::count();
        $special_product = SpecialProduct::count();
        return view('dashboard', array(
            'users' => $users,
            'users_stop' => User::where('status', '0')->count(),
            'users_active' => User::where('status', '1')->count(),
            'users_delete' => User::where('status', '2')->count(),
            'users_black_list' => User::where('status', '3')->count(),
            'carrier' => $carrier,
            'carrier_active' => Carrier::where('status', '1')->count(),
            'carrier_stop' => Carrier::where('status', '0')->count(),
            'branch' => $branch,
            'branch_enable' => Branch::where('status', '1')->count(),
            'branch_disable' => Branch::where('status', '0')->count(),
            'request' => $request,
            'special_product' => $special_product,
            'request_has_been_approved' => Request::where('is_active', '1')->count(),
            'request_not_approved' => Request::where('is_active', '2')->count(),
            'request_canceled' => Request::where('status', 'canceled')->count(),
            'request_weeks' => $weekday,

            'reports' => $this->Appointment(),
            'reports_request_from_delivery' => $this->AppointmentRequestFromDelivery(),
            'reports_status_1' => $this->AppointmentHasBeenApproved(),
            'reports_status_0' => $this->AppointmentNotApproved(),
            'reports_status_canceled' => $this->AppointmentCanceled(),
            'reports_special_request' => $this->AppointmentSpecialRequests(),
        ));
    }
}
