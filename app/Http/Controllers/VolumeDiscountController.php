<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\VolumeDiscount;
use App\Notifications\Admin\ProviderNotification;
use App\Notifications\VolumeDiscountNotification;
use App\Rules\CheckBranch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class VolumeDiscountController extends Controller
{
    /**
     * @var string
     */
    public $view_path = 'managements.discounts.volume_discounts.';

    /**
     * Display volume_discounts data.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = VolumeDiscount::orderBy('id', 'desc')->orderBy('is_approved');
        return $this->display($data, $request, 'index');
    }

    public function display($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'branch') {
                $data = $data->whereHas('branch', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            } else if ($search_type == 'price_from') {
                $data = $data->where('price_from', $query);
            } else if ($search_type == 'price_to') {
                $data = $data->where('price_to', $query);
            } else if ($search_type == 'discount') {
                $data = $data->where('value', $query);
            }
        endif;

        $data_count = $data->count();

        return view($this->view_path . $view_name)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('branches', Branch::all())
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $carrier = VolumeDiscount::find($id);

        return response()->json([
            'id' => $carrier->id,
            'branch_name' => $carrier->branch->name ?? null,
            'price_from' => $carrier->price_from,
            'price_to' => $carrier->price_to,
            'value' => $carrier->price_with_type,
            'value_type' => $carrier->value_type,
            'type' => $carrier->type,
            'status' => $carrier->is_active,
            'note' => $carrier->note,
            'start_date' => $carrier->start_date,
            'end_date' => $carrier->end_date,

            'updated_at' => Carbon::Parse($carrier->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($carrier->created_at)->format('Y/m/d'),
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'branch_id' => ['nullable'],
            'value_type' => 'required|string|in:price,percentage',
            'type' => 'required|string|in:minimum,balance,delivery',
            'price_from' => 'nullable|numeric|max:1000000000',
            'price_to' => ['nullable', 'numeric', 'max:1000000000',
                function ($attribute, $value, $fail) use ($request) {
                    if ($value <= $request->price_from) {
                        $fail(lang::get("validation.the_price_is_greater_than_the_minimum"));
                    }
                }],
            'price' => [
                'required',
                'numeric',
                function ($attribute, $value, $fail) use ($request) {
                    if ($request->value_type == 'percentage') {
                        if ($value > 100)
                            $fail(lang::get("validation.max.numeric") . ' 100.');
                        if ($value < 0)
                            $fail(lang::get("validation.min.numeric") . ' 0.');
                    } else {
                        if ($value > 1000000)
                            $fail(lang::get("validation.max.numeric") . ' 1000000');
                    }
                }],

            'start_date' => ['required', 'date', 'after:yesterday'],
            'end_date' => 'required|date|after_or_equal:start_date',
        ], [
            'after' => __('validation.date_most_be_at_least_today'),
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $volume_discount = new VolumeDiscount();
        $volume_discount->branch_id = $request->branch_id;
        $volume_discount->price_from = $request->price_from;
        $volume_discount->price_to = $request->price_to;
        $volume_discount->start_date = $request->start_date;
        $volume_discount->end_date = $request->end_date;
        $volume_discount->value_type = $request->value_type;
        $volume_discount->type = $request->type;
        $volume_discount->value = $request->price;
        $volume_discount->is_active = 1;
        $volume_discount->is_approved = 1;
        $volume_discount->note = $request->note;
        $volume_discount->save();

        return response()->json([
            'volume_discount' => $volume_discount,
            'success' => Lang::get('admin.added_successfully')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'status' => 'required|in:active,attitude',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $volume_discount = VolumeDiscount::find($id);
        $volume_discount->update([
            'is_active' => ($request->status == 'active') ? 1 : 0
        ]);

        return response()->json([
            'id' => $id,
            'status' => $volume_discount->is_active,
            'background_color_row' => $volume_discount->background_color_row,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    public function accept(Request $request)
    {
        $id = $request->items_id;
        $volume_discount = VolumeDiscount::find($id);
        if ($volume_discount->is_approved != 0) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($volume_discount, $id) {
            $volume_discount->update(['is_approved' => 1]);
            responseNotify($id, VolumeDiscount::class, ProviderNotification::class);
            $volume_discount->notify(new ProviderNotification());
        });

        return response()->json([
            'status' => 200,
            'data' => VolumeDiscount::find($id),
            'title' => Lang::get('admin.accepted_successfully'),
            'message' => ''
        ]);
    }

    public function reject(Request $request)
    {
        $id = $request->items_id;
        $volume_discount = VolumeDiscount::find($id);
        if ($volume_discount->is_approved != 0) {
            return response()->json(['error' => 'error'], 401);
        }

        DB::transaction(function () use ($volume_discount, $id) {
            $volume_discount->update(['is_approved' => 3]);
            responseNotify($id, VolumeDiscount::class, ProviderNotification::class);
            $volume_discount->notify(new ProviderNotification());
        });

        return response()->json([
            'status' => 200,
            'data' => VolumeDiscount::find($id),
            'title' => Lang::get('admin.rejected_successfully'),
            'message' => ''
        ]);
    }
}
