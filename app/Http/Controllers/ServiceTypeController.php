<?php

namespace App\Http\Controllers;

use App\Models\ServiceType;
use App\Models\ServiceTypeTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class ServiceTypeController extends Controller
{
    public $view_path = 'managements.encoding.service_types.';
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;
        $data = new ServiceType();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'name') {
                $data = $data->whereTranslationLike('name', '%' . $query . '%');
            }
        endif;
        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();
        return view($this->view_path . 'index')
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);
        $service_type = new ServiceType();
        $service_type->status = $request->input('status', 0);
        $service_type->save();
        $service_type->translateOrNew('ar')->name = $request->name_ar;
        $service_type->translateOrNew('en')->name = $request->name_en;
        $service_type->save();
        return response()->json([
            'status' => 200,
            'service_type' => $service_type,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request)
    {
        return Validator::make(
            $request->all(), [
            'name_ar' => 'required|string|max:191',
            'name_en' => 'required|string|max:191',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service_type = ServiceType::find($id);
        $service_type_ar = ServiceTypeTranslation::where('service_type_id', '=', $id)->where('locale', 'ar')->first();
        $service_type_en = ServiceTypeTranslation::where('service_type_id', '=', $id)->where('locale', 'en')->first();
        return response()->json([
            'id' => $service_type->id,
            'status' => $service_type->status,
            'name_ar' => $service_type_ar->name,
            'name_en' => $service_type_en->name,
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $service_type = ServiceType::find($id);
        $service_type->status = $request->input('status', 0);
        $service_type->save();

        $service_type->translateOrNew('ar')->name = $request->name_ar;
        $service_type->translateOrNew('en')->name = $request->name_en;
        $service_type->save();

        return response()->json([
            'status' => 200,
            'service_type' => $service_type,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServiceType::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => ServiceType::count()
        ],
            200
        );
    }
}
