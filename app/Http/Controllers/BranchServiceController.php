<?php

namespace App\Http\Controllers;

use App\Http\Resources\BranchServicesResource;
use App\Models\Branch;
use App\Models\BranchService;
use App\Models\ServiceType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class BranchServiceController extends Controller
{
    public $view_path = 'managements.encoding.branch_services.';

    public function index()
    {
        return view($this->view_path . 'index')
            ->with('service_types', ServiceType::get())
            ->with('branches', Branch::get());
    }

    public function get($branch_id)
    {
        $data = BranchService::where('branch_id', $branch_id)->get();
        return BranchServicesResource::collection($data);
    }

    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $branch_service = new BranchService();
        $branch_service->branch_id = $request->branch_id;
        $branch_service->service_type_id = $request->service_type_id;
        $branch_service->save();

        return response()->json([
            'success' => Lang::get('admin.added_successfully'),
            'branch_id' => $request->branch_id
        ]);
    }

    public function validatedData($request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'branch_id' => [
                'required',
                'integer',
                'exists:branches,id'
            ],
            'service_type_id' => [
                'required',
                'integer',
                'exists:main_categories,id',
                function ($attribute, $value, $fail) use ($request) {
                    $branch_service = BranchService::where('service_type_id', $value)
                        ->where('branch_id', $request->branch_id)
                        ->count();
                    if ($branch_service != 0) {
                        $fail(lang::get("validation.this_row_already_exists") . '.');
                    }
                }],
        ]);

        return $validatedData;
    }

    public function destroy($id)
    {
        $branch_service = BranchService::find($id);
        $branch_id = $branch_service->branch_id;
        $branch_service->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'branch_id' => $branch_id
        ]);
    }
}
