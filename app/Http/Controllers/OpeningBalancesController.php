<?php

namespace App\Http\Controllers;

use App\Http\Resources\FalterBranchCategoryResources;
use App\Http\Resources\FalterProductResource;
use App\Models\Area;
use App\Models\AreaTranslation;
use App\Models\MainCategoryCategory;
use App\Models\OpeningBalances;

use App\Models\Branch;
use App\Models\Country;
use App\Models\DeliveryPrice;
use App\Models\PharmacyCategory;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class OpeningBalancesController extends Controller
{
    public $view_path = 'managements.app.stocks.';

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new OpeningBalances();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'product_no') {
                $data = $data->where('product_number', '=', $query);
            } else if ($search_type == 'pricing') {
                $data = $data->where('pricing', '%' . $query . '%');
            } else if ($search_type == 'quantity') {
                $data = $data->where('quantity', '%' . $query . '%');
            }
        endif;

//        $data = $data->orderBy('Stocks.id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')
            ->with('products', Product::where('status', 1)->get())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request, true);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $stocks = new OpeningBalances();
        $stocks->product_id = $request->product;
        $stocks->pricing = $request->pricing;
        $stocks->quantity = $request->quantity;

        $stocks->save();



        return response()->json([
            'status' => 200,
            'stocks' => $stocks,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request)
    {
//        $is_unique = $is_unique ? 'unique:area_translations,name' : null;
        return Validator::make(
            $request->all(), [
            'product' => 'required|integer',
            'pricing' => 'required',
            'quantity' => 'required|integer',
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stocks = OpeningBalances::find($id);

        return response()->json([
            'id' => $stocks->id,
            'product' => $stocks->product_id,
            'pricing' => $stocks->pricing,
            'quantity' => $stocks->quantity,

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $stocks = OpeningBalances::find($id);

        $stocks->product_id = $request->product;
        $stocks->pricing = $request->pricing;
        $stocks->quantity = $request->quantity;
        $stocks->save();



        return response()->json([
            'status' => 200,
            'stocks' => $stocks,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        OpeningBalances::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => OpeningBalances::count()
        ],
            200
        );
    }



    public function falterProduct($id)
    {
        $products = DB::table("product_prices")->where("product_id", $id)->pluck("price", "id");
        return json_encode($products);
    }
}
