<?php

namespace App\Http\Controllers;

use App\Models\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\View\View;

class UserAddressController extends Controller
{
    public $view_path = 'managements.app.user_addresses.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new UserAddress();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'user_name') {
                $data = $data->whereHas('user', function ($q) use ($query) {
                    $q->where('full_name', 'like', '%' . $query . '%');
                });
            } else if ($search_type == 'name') {
                $data = $data->where('name', 'like', '%' . $query . '%');
            } else if ($search_type == 'description') {
                $data = $data->where('desc', 'like', '%' . $query . '%');
            } else if ($search_type == 'area') {
                $data = $data->whereHas('area', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            }
        endif;

        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_address = UserAddress::find($id);

        return response()->json([
            'id' => $user_address->id,
            'name' => $user_address->name,
            'desc' => $user_address->desc,
            'user_name' => $user_address->user->full_name,
            'status' => $user_address->status,
            'favorated' => $user_address->favorated,
            'updated_at' => Carbon::Parse($user_address->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($user_address->created_at)->format('Y/m/d'),


        ]);
    }
}
