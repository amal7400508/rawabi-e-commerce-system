<?php

namespace App\Http\Controllers;

use App\PostModel;
use Illuminate\Http\Request;


class PosatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('post.post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new PostModel();
        $post->save();
        $post->translateOrNew('ar')->name = $request->ar_name;
        $post->translateOrNew('ar')->body = $request->ar_body;

        $post->translateOrNew('en')->name = $request->en_name;
        $post->translateOrNew('en')->body = $request->en_body;

        $post->save();
        redirect()->back();
    }
}
