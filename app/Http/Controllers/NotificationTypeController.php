<?php

namespace App\Http\Controllers;

use App\Notification;
use App\NotificationType;
use App\NotificationTypeTranslation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class NotificationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('notification_type_translations')
                ->join('notification_types', 'notification_type_translations.notification_type_id', '=', 'notification_types.id')
                ->where('notification_type_translations.locale', '=', $locale)
                ->whereNull('notification_types.deleted_at')
                ->select('notification_type_translations.*', 'notification_types.updated_at')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '';
                    if (Gate::check('update notification_type')) {
                        $button .= '<button type="button" name="edit" id="' . $data->notification_type_id . '" class="edit-table-row btn btn-primary btn-sm" data-toggle="edite" title="edite"><i class="ft ft-edit"></i></button>';
                        $button .= '&nbsp';
                    }
                    $button .= '<button type="button" name="showenter" id="' . $data->id . '"  class="showdetail showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    if (Gate::check('delete notification_type')) {
                        $button .= '&nbsp';
                        $button .= '<button type="button" name="delete" id="' . $data->notification_type_id . '" class="delete btn btn-danger btn-sm" data-toggle="delete" title="delete"><i class="ft ft-trash-2"></button>';
                    }
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('notification_type.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ar_name_N' => 'required|string|max:191',
            'en_name_N' => 'required|string|max:191',
        ]);

        $notification = new NotificationType();
        $notification->save();

        $notification->translateOrNew('ar')->name = $request->ar_name_N;
        $notification->translateOrNew('en')->name = $request->en_name_N;

        $notification->save();
        return redirect()->back()->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification = NotificationTypeTranslation::where('id', $id)->first();
        $notifications = NotificationType::find($notification->notification_type_id);
        $btnShow = '';
        return response()->json([
            'id' => $notification->notification_id,
            'name' => $notification->name,
            'created_at_n' => Carbon::Parse($notifications->created_at)->format('Y/m/d'),
            'updated_at_n' => Carbon::Parse($notifications->updated_at)->format('Y/m/d'),
            'btn' => $btnShow,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notification_ar = NotificationTypeTranslation::where('notification_type_id', '=', $id)->where('locale', 'ar')->first();
        $notification_en = NotificationTypeTranslation::where('notification_type_id', '=', $id)->where('locale', 'en')->first();
        return response()->json([
            'edit_name_ar' => $notification_ar->name,
            'edit_id' => $notification_ar->notification_type_id,
            'edit_name_en' => $notification_en->name,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'ar_name_N' => 'required|string|max:191',
            'en_name_N' => 'required|string|max:191',
        ]);

        $notification = NotificationType::find($id);
        $notification->save();

        $notification->translateOrNew('ar')->name = $request->ar_name_N;
        $notification->translateOrNew('en')->name = $request->en_name_N;

        $notification->save();

        return redirect()->back()->with('success', Lang::get('admin.edited_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification = Notification::withTrashed()->where('type_id', $id)->count();
        if ($notification > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        $notification_type = NotificationType::findOrFail($id);
        $notification_type->delete();
        return redirect('/');
    }

    /**
     *  displays the deleted data as an initial deletion
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function recycle_bin()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('notification_type_translations')
                ->join('notification_types', 'notification_type_translations.notification_type_id', '=', 'notification_types.id')
                ->where('notification_type_translations.locale', '=', $locale)
                ->whereNotNull('notification_types.deleted_at')
                ->select('notification_type_translations.name', 'notification_types.*')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="restore" id="' . $data->id . '" class="restore btn btn-primary btn-sm" data-toggle="Restore" title="Restore"><i class="ft ft-rotate-cw"></i></button>';
                    $button .= '&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('notification_type.recycle_bin');
    }

    /**
     * retrieves deleted data from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore($id)
    {
        $notification = NotificationType::withTrashed()->where('id', $id)->first();
        $notification->restore();
        return response()->json();
    }

    /**
     * performs the final deletion from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function hdelete($id)
    {
        $notification = Notification::withTrashed()->where('type_id', $id)->count();
        if ($notification > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        DB::table('notification_type_translations')
            ->where('notification_type_id', '=', $id)
            ->delete();
        $notification_type = NotificationType::withTrashed()->where('id', $id)->first();
        $notification_type->forceDelete();
    }
}
