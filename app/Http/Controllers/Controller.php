<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Utilities: [DumpJson] to dump data into json format
     *
     * @param boolean $status true or false
     * @param boolean $code   200, 404 or any http code
     *
     * @return \illuminate\Http\Response
     */
    public function dj($data, $status = true, $code = 200)
    {
        return response()->json(
            [
                'success'=>$status,
                'data'=>$data
            ],
            $code
        );
    }
}
