<?php

namespace App\Http\Controllers;

use App\Exports\CarrierExport;
use App\Models\Branch;
use App\Models\Carrier;
use App\Models\Rate;
use App\Models\RateDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Request as CarrierRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class CarrierController extends Controller
{
    public $view_path = 'managements.carrier_management.carriers.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new Carrier();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', $query);
            } else if ($search_type == 'name') {
                $data = $data->where('name', 'like', '%' . $query . '%');
            } else if ($search_type == 'phone') {
                $data = $data->where('phone', 'like', '%' . $query . '%');
            }
        endif;

        $data = $data->orderBy('id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')
            ->with('branches', Branch::active()->get())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'name' => 'required|string|max:191',
            'phone' => 'required|string|max:191',
            'email' => 'required|email|max:191',
            'gender' => 'required|in:man,woman',
            'status' => 'in:1',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'password' => 'required|string|max:20|min:6',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $image_name = null;
        if ($request->hasFile('image')) {
            $image_name = uploadImage($request->image, 'carriers');
        }

        $carrier = new Carrier();
        $carrier->name = $request->name;
        $carrier->email = $request->email;
        $carrier->phone = $request->phone;
        $carrier->gender = $request->gender;
        $carrier->password = Hash::make($request->password);;
        $carrier->image = $image_name;
        $carrier->status = $request->input('status', 0);
        $carrier->save();

        return response()->json([
            'status' => 200,
            'carrier' => $carrier,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param CarrierRequest $request
     * @return \Illuminate\Http\Response
     */
    public function show($id, CarrierRequest $request)
    {
        $carrier = Carrier::find($id);
        $average_rate = round(
            $this->averageRate($id, $request->from_date_evaluation, $request->to_date_evaluation)
            , 2
        );

        return response()->json([
            'id' => $carrier->id,
            'name' => $carrier->name,
            'email' => $carrier->email,
            'gender' => $carrier->gender,
            'phone' => $carrier->phone,
            'status' => $carrier->status,
            'image' => $carrier->image_path,
            'created_at' => Carbon::Parse($carrier->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($carrier->updated_at)->format('Y/m/d'),

            'number_requests' => $this->numberRequests($id, $request->from_date_evaluation, $request->to_date_evaluation),
            'average_rate' => $average_rate,
            'rate_total' => $this->rate($id, null, $request->from_date_evaluation, $request->to_date_evaluation),
            'rate_5' => $this->rate($id, 5, $request->from_date_evaluation, $request->to_date_evaluation),
            'rate_4' => $this->rate($id, 4, $request->from_date_evaluation, $request->to_date_evaluation),
            'rate_3' => $this->rate($id, 3, $request->from_date_evaluation, $request->to_date_evaluation),
            'rate_2' => $this->rate($id, 2, $request->from_date_evaluation, $request->to_date_evaluation),
            'rate_1' => $this->rate($id, 1, $request->from_date_evaluation, $request->to_date_evaluation),
        ]);
    }

    public function rate($carrier_id, $number = null, $form_date = null, $to_date = null)
    {
//        $rate_count = Rate::join('rate_details', 'rates.id', 'rate_details.rate_id')
//            ->where('rates.carrier_id', $carrier_id)
//            ->where('rate_details.rate_type_id', 3);
//        if (!is_null($number)){
//            $rate_count = $rate_count->where('rate', $number);
//        }
//
//
//        return $rate_count->count();


        $carrier_rates_id = Rate::where('carrier_id', $carrier_id);

        if (!is_null($form_date)) {
            $carrier_rates_id = $carrier_rates_id->whereDate('created_at', '>=', $form_date);
        }
        if (!is_null($to_date)) {
            $carrier_rates_id = $carrier_rates_id->whereDate('created_at', '<=', $to_date);
        }
        $carrier_rates_id = $carrier_rates_id->get('id')->toArray();
        $rate_count = RateDetail::whereIn('rate_id', $carrier_rates_id)
            ->where('rate_type_id', 3);

        if (!is_null($number)){
            $rate_count = $rate_count->where('rate', $number);
        }

        return $rate_count->count();

    }


    /*public function show($id)
    {
        $carrier = Carrier::find($id);

        return response()->json([
            'id' => $carrier->id,
            'name' => $carrier->name,
            'phone' => $carrier->phone,
            'email' => $carrier->email,
            'gender' => $carrier->gender,
            'status' => $carrier->status,
            'image' => $carrier->image_path,
            'updated_at' => Carbon::Parse($carrier->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($carrier->created_at)->format('Y/m/d'),
        ]);
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carrier = Carrier::find($id);

        return response()->json([
            'id' => $carrier->id,
            'name' => $carrier->name,
            'phone' => $carrier->phone,
            'email' => $carrier->email,
            'gender' => $carrier->gender,
            'status' => $carrier->status,
            'image' => is_null($carrier->image) ? null : $carrier->image_path,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'name' => 'required|string|max:191',
            'phone' => 'required|string|max:191',
            'email' => 'required|email|max:191',
            'gender' => 'required|in:man,woman',
            'status' => 'in:1',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $carrier = Carrier::find($id);
        $image_name = $carrier->image;
        if ($request->hasFile('image')) {
            deleteImage('/storage/carriers/' . $carrier->image);
            $image_name = uploadImage($request->image, 'carriers');
        }
        $carrier->name = $request->name;
        $carrier->email = $request->email;
        $carrier->phone = $request->phone;
        $carrier->gender = $request->gender;
        $carrier->image = $image_name;
        $carrier->status = $request->input('status', 0);
        $carrier->save();

        return response()->json([
            'status' => 200,
            'carrier' => $carrier,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = CarrierRequest::where('carrier_id', $id)->count();
        if ($request > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }

        $carrier = Carrier::find($id);
        deleteImage('/storage/carriers/' . $carrier->image);
        $carrier->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Carrier::count()
        ],
            200
        );
    }

    public function report(Request $request)
    {
        if (request()->ajax()) {
            $data = $this->getDataReport($request);
            return datatables()->of($data)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="show" id="' . $data->id . '"  class="showB btn btn-primary btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    return $button;
                })
                ->editColumn('number_requests', function ($data) use ($request) {
                    return $this->numberRequests(
                        $data->id,
                        $request->from_date_evaluation,
                        $request->to_date_evaluation
                    );
                })->editColumn('rate', function ($data) use ($request) {
                    return $this->averageRate(
                        $data->id,
                        $request->from_date_evaluation,
                        $request->to_date_evaluation
                    );
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view($this->view_path . '.report')
            ->with('branches', Branch::where('status', 1)->get());
    }

    public function getDataReport($request)
    {
        if ($request->confirm == 1) {
            $data = new Carrier;
            if (!is_null($request->from)) {
                $data = $data->whereDate('carriers.created_at', '>=', $request->from);
            }
            if (!is_null($request->to)) {
                $data = $data->whereDate('carriers.created_at', '<=', $request->to);
            }
            if (!is_null($request->name)) {
                $data = $data->where('carriers.name', 'like', '%' . $request->name . '%');
            }
            if (!is_null($request->phone)) {
                $data = $data->where('carriers.phone', 'like', '%' . $request->phone . '%');
            }
            if (!is_null($request->email)) {
                $data = $data->where('carriers.email', 'like', '%' . $request->email . '%');
            }
            if (!is_null($request->status)) {
                $data = $data->where('carriers.status', $request->status);
            }
            if (!is_null($request->gender)) {
                $data = $data->where('carriers.gender', $request->gender);
            }
            if (!is_null($request->branch_id)) {
                $data = $data->where('carriers.branch_id', $request->branch_id);
            }
            return $data->get();
        } else {
            $deposits = Carrier::all();
        }

        return $deposits;

    }

    public function export(Request $request)
    {
        $request->request->add(['confirm' => 1]);
        $data = $this->getDataReport($request);
        return Excel::download(
            new CarrierExport(
                $data,
                $request->from_date_evaluation,
                $request->to_date_evaluation
            ),
            'accounts-' . now() . '.xlsx'
        );
    }

    /**
     * average rate for carriers from the users
     *
     * @param $carrier int carrier id
     * @param $form_date string rate start date
     * @param $to_date string rate end date
     * @return float|int
     */
    public function averageRate($carrier, $form_date = null, $to_date = null)
    {
        $carrier_rates_id = Rate::where('carrier_id', $carrier);

        if (!is_null($form_date)) {
            $carrier_rates_id = $carrier_rates_id->whereDate('created_at', '>=', $form_date);
        }
        if (!is_null($to_date)) {
            $carrier_rates_id = $carrier_rates_id->whereDate('created_at', '<=', $to_date);
        }
        $carrier_rates_id = $carrier_rates_id->get('id')->toArray();

        $rates_count = RateDetail::whereIn('rate_id', $carrier_rates_id)
            ->where('rate_type_id', 1)
            ->get()->count();

        $rates_sum = RateDetail::whereIn('rate_id', $carrier_rates_id)
            ->where('rate_type_id', 1)
            ->get('rate')->sum('rate');

        if ($rates_count <= 0) {
            return 0;
        }

        $data = $rates_sum / $rates_count;
        return $data;
    }

    public function numberRequests($carrier_id, $form_date = null, $to_date = null)
    {
        $request = CarrierRequest::where('carrier_id', $carrier_id)
            ->where(function ($q) {
                $q->where('requests.status', '!=', 'requested')
                    ->where('requests.status', '!=', 'canceled');
//                $q->where('status', 'on_branch')
//                    ->orWhere('status', 'delivered');
            });

        if (!is_null($form_date)) {
            $request = $request->whereDate('created_at', '>=', $form_date);
        }
        if (!is_null($to_date)) {
            $request = $request->whereDate('created_at', '<=', $to_date);
        }

        return $request->count();
    }
}
