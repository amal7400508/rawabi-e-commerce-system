<?php

namespace App\Http\Controllers;

use App\Models\Suggestion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;

class SuggestionController extends Controller
{
    /**
     * @var string
     */
    public $view_path = 'managements.customer_reviews.suggestions.';

    /**
     * Display providers data.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = Suggestion::orderBy('id', 'desc');
        return $this->display($data, $request, 'index');
    }

    public function display($data, $request, $view_name)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;

        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'name') {
                $data = $data->whereHas('user', function ($q) use ($query) {
                    $q->where('full_name', 'like', '%' . $query . '%');
                });
            } else if ($search_type == 'phone') {
                $data = $data->whereHas('user', function ($q) use ($query) {
                    $q->where('phone', 'like', '%' . $query . '%');
                });
            } else if ($search_type == 'branch') {
                $data = $data->whereHas('branch', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            } else if ($search_type == 'text') {
                $data = $data->where('text', 'like', '%' . $query . '%');
            }
        endif;

        $data_count = $data->count();

        return view($this->view_path . $view_name)
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $suggestion = Suggestion::find($id);

        return response()->json([
            'id' => $suggestion->id,
            'name' => $suggestion->user->full_name,
            'phone' => $suggestion->user->phone,
            'text' => $suggestion->text,
            'branch_name' => $suggestion->branch->name ?? null,
            'image' => $suggestion->image,
            'updated_at' => Carbon::Parse($suggestion->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($suggestion->created_at)->format('Y/m/d'),
        ]);
    }
}
