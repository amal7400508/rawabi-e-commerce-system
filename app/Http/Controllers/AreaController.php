<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\AreaTranslation;
use App\Models\Branch;
use App\Models\Country;
use App\Models\DeliveryPrice;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class AreaController extends Controller
{

    public $view_path = 'managements.encoding.areas.';

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new Area();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $data = $data->where('id', '=', $query);
            } else if ($search_type == 'name') {
                $data = $data->whereTranslationLike('name', '%' . $query . '%');
            } else if ($search_type == 'country') {
                $data = $data->whereHas('country', function ($q) use ($query) {
                    $q->whereTranslationLike('name', '%' . $query . '%');
                });
            }
        endif;

        $data = $data->orderBy('areas.id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')
            ->with('countries', Country::where('status', 1)->get())
            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request, true);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $area = new Area();
        $area->country_id = $request->country;
        $area->status = $request->input('status', 0);
        $area->save();

        $area->translateOrNew('ar')->name = $request->name_ar;
        $area->translateOrNew('en')->name = $request->name_en;
        $area->save();

        return response()->json([
            'status' => 200,
            'area' => $area,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request, $is_unique = false)
    {
        $is_unique = $is_unique ? 'unique:area_translations,name' : null;
        return Validator::make(
            $request->all(), [
            'country' => 'required|integer',
            'name_ar' => ['required', 'string', 'max:191', $is_unique],
            'name_en' => ['required', 'string', 'max:191', $is_unique],
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = Area::find($id);
        $area_ar = AreaTranslation::where('area_id', '=', $id)->where('locale', 'ar')->first();
        $area_en = AreaTranslation::where('area_id', '=', $id)->where('locale', 'en')->first();

        return response()->json([
            'id' => $area->id,
            'country' => $area->country_id,
            'status' => $area->status,
            'name_ar' => $area_ar->name,
            'name_en' => $area_en->name,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $area = Area::find($id);
        $area->country_id = $request->country;
        $area->status = $request->input('status', 0);
        $area->save();

        $area->translateOrNew('ar')->name = $request->name_ar;
        $area->translateOrNew('en')->name = $request->name_en;
        $area->save();

        return response()->json([
            'status' => 200,
            'area' => $area,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branch::where('area_id', $id)->count();
        if ($branch > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }

        $user_addresses = UserAddress::where('area_id', $id)->count();
        if ($user_addresses > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }

        $delivery_price = DeliveryPrice::where('area_id_from', $id)->OrWhere('area_id_to', $id)->count();
        if ($delivery_price > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ], 401);
        }

        Area::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Area::count()
        ],
            200
        );
    }
}
