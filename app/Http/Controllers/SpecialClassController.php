<?php

namespace App\Http\Controllers;

use App\ShowCases;
use App\SpecialClass;
use App\SpecialClassTranslation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Image as ImageFaker;

class SpecialClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $special_class = SpecialClass::all();

            return datatables()->of($special_class)
                ->addColumn('action', function ($data) {
                    $button = '';
//                    if (Gate::check('update categories')) {
                    $button .= '<button type="button" name="edit" id="' . $data->id . '" class="edit-table-row btn btn-primary btn-sm" data-toggle="edite" title="edite"><i class="ft ft-edit"></i></button>';
                    $button .= '&nbsp';
//                    }
                    $button .= '<button type="button" name="showenter" id="' . $data->id . '"  class="showdetail showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
//                    if (Gate::check('delete categories')) {
                    $button .= '&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm" data-toggle="delete" title="delete"><i class="ft ft-trash-2"></button>';
//                    }
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('special_class.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $special_class = SpecialClass::find($id);
        return response()->json([
            'special_class_ar' => $this->getSpecialClass($id, 'ar'),
            'special_class_en' => $this->getSpecialClass($id, 'en'),
            'special_class' => $special_class
        ]);
    }

    /**
     * get SpecialClass translation
     *
     * @param $id int
     * @param $locale string
     * @return mixed
     */
    public function getSpecialClass($id, $locale)
    {
        $special_class = SpecialClassTranslation::where('special_class_id', $id)
            ->where('locale', $locale)->get();

        return $special_class;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $special_class = SpecialClass::find($id);
        $btnShow = '<button class="delete btn btn-float btn-round btn-danger" id="' . $special_class->id . '"><i class="ft-trash-2"></i></button>';

        return response()->json([
            'special_class' => $special_class,
            'created_at' => Carbon::Parse($special_class->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($special_class->updated_at)->format('Y/m/d'),
            'btn' => $btnShow
        ]);
    }

    public function addOrUpdate(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'ar_name' => 'required|string',
            'en_name' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        if($request->image){
            $image = $request->file('image');
            $imageName = 'moka_' . time() . '.' . $image->getClientOriginalExtension();
            $file_path='storage/special_class/';

            $path100 = public_path($file_path.'/100/'); // For phone
            $path64  = public_path($file_path.'/64/'); // For search

            File::exists($path100)
                ?: File::makeDirectory($path100, $mode = 0777, true, true);
            File::exists($path64)
                ?: File::makeDirectory($path64, $mode = 0777, true, true);

            ImageFaker::make($image)
                ->resize(100, 100)
                ->fit(100, 100)->save($file_path.'/100/' . $imageName);
            ImageFaker::make($image)
                ->resize(64, 64)
                ->fit(64, 64)->save($file_path.'/64/' . $imageName);

            $image->move(public_path($file_path), $imageName);

        }

        $special_class = SpecialClass::updateOrCreate(
            ['id' => $request->special_class_id],
            ['status' => $request->input('status', 0) ]
        );

        $special_class->translateOrNew('ar')->name = $request->ar_name;
        $special_class->translateOrNew('ar')->details = $request->ar_details;

        $special_class->translateOrNew('en')->name = $request->en_name;
        $special_class->translateOrNew('en')->details = $request->en_details;
        if ($request->image){
            $special_class->translateOrNew('ar')->image = $file_path . $imageName;
            $special_class->translateOrNew('en')->image = $file_path . $imageName;
        }
        $special_class->save();

        if ($request->sand_type == 'add')
            return response()->json(['success' => Lang::get('admin.added_successfully')]);

        return response()->json(['success' => Lang::get('admin.edited_successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $show_cases = ShowCases::withTrashed()->where('special_class_id', $id)->count();
        if ($show_cases > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }

        $special_class = SpecialClass::find($id);
        $special_class->delete();
        return response()->json([
            'success' => 'success'
        ]);
    }
}
