<?php

namespace App\Http\Controllers;


use App\Models\Color;
use App\Models\ColorTranslation;
use App\Models\Country;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class ColorController extends Controller
{
    public $view_path = 'managements.encoding.color.';

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $data = new Color();
        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
             if ($search_type == 'name') {
                $data = $data->whereTranslationLike('name', '%' . $query . '%');
            }
        endif;

//        $data = $data->orderBy('areas.id', 'desc');
        $data_count = $data->count();

        return view($this->view_path . 'index')

            ->with('data', $data->paginate($table_length))
            ->with('data_count', $data_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request, true);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $color = new Color();

        $color->status = $request->input('status', 0);
        $color->save();

        $color->translateOrNew('ar')->name = $request->name_ar;
        $color->translateOrNew('en')->name = $request->name_en;
        $color->save();

        return response()->json([
            'status' => 200,
            'color' => $color,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request, $is_unique = false)
    {
        $is_unique = $is_unique ? 'unique:color_translations,name' : null;
        return Validator::make(
            $request->all(), [

            'name_ar' => ['required', 'string', 'max:191', $is_unique],
            'name_en' => ['required', 'string', 'max:191', $is_unique],
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $color = Color::find($id);
        $color_ar = ColorTranslation::where('color_id', '=', $id)->where('locale', 'ar')->first();
        $color_en = ColorTranslation::where('color_id', '=', $id)->where('locale', 'en')->first();

        return response()->json([
            'id' => $color->id,

            'status' => $color->status,
            'name_ar' => $color_ar->name,
            'name_en' => $color_en->name,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $color = Color::find($id);

        $color->status = $request->input('status', 0);
        $color->save();

        $color->translateOrNew('ar')->name = $request->name_ar;
        $color->translateOrNew('en')->name = $request->name_en;
        $color->save();

        return response()->json([
            'status' => 200,
            'color' => $color,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        Color::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Color::count()
        ],
            200
        );
    }
}
