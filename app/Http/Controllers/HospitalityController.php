<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HospitalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $dataselect = DB::table('hospitalities')
                ->join('users', 'hospitalities.user_id', '=', 'users.id')
                ->join('boxing__types', 'boxing__types.id', '=', 'hospitalities.boxing_type_id')
                ->select('hospitalities.*', 'users.name as user_name', 'boxing__types.type')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="showenter" id="' . $data->id . '"  class="showB showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('Hospitality.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $locale = app()->getLocale();
        $hospitality_details = DB::table('hospitality_details')
            ->join('hospitalities', 'hospitality_details.hospitality_id', '=', 'hospitalities.id')
            ->join('product_prices', 'hospitality_details.product_price_id', '=', 'product_prices.id')
            ->join('products', 'product_prices.product_id', '=', 'products.id')
            ->join('product_translations', 'products.id', '=', 'product_translations.product_id')
            ->join('units', 'product_prices.unit_id', '=', 'units.id')
            ->join('unit_translations', 'units.id', '=', 'unit_translations.unit_id')
            ->join('tastes', 'product_prices.taste_id', '=', 'tastes.id')
            ->join('taste_translations', 'tastes.id', '=', 'taste_translations.taste_id')
            ->whereNull('products.deleted_at')
            ->whereNull('units.deleted_at')
            ->whereNull('tastes.deleted_at')
            ->where('product_translations.locale', '=', $locale)
            ->where('unit_translations.locale', '=', $locale)
            ->where('taste_translations.locale', '=', $locale)
            ->where('hospitalities.id', '=', $id)
            ->select('hospitality_details.*', 'hospitalities.name', 'product_translations.name as product_translations_name',
                'unit_translations.type', 'unit_translations.name as unit_translations_name', 'taste_translations.taste'
                , 'hospitalities.created_at as created', 'hospitalities.updated_at as updated','hospitalities.date')
            ->get();
        return response()->json([
            'hospitality_details' => $hospitality_details
        ]);
    }
}
