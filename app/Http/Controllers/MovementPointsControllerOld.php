<?php

namespace App\Http\Controllers;

use App\PointPrice;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class MovementPointsController extends Controller
{
    public function index()
    {
//        if (PointPrice::count() < 1) {
//            return redirect('point_price')->with('error', Lang::get('admin.enter_point_price'));
//        }
        if (request()->ajax()) {
            $locale = app()->getLocale();
            $latestPosts = DB::table('gain_methods')
                ->select('gain_type_id', 'user_id',
                    DB::raw('sum(gain_methods.price) as price_sum'),
                    DB::raw('max(point_histories.updated_at) as updated'))
                ->join('point_histories', 'gain_methods.id', '=', 'point_histories.gain_method_id')
                ->groupBy('gain_methods.gain_type_id', 'point_histories.user_id');

            $gain_type = DB::table('gain_types')
                ->joinSub($latestPosts, 'latest_posts', function ($join) {
                    $join->on('gain_types.id', '=', 'latest_posts.gain_type_id');
                })
                ->join('gain_type_translations', 'gain_types.id', '=', 'gain_type_translations.gain_type_id')
                ->join('users', 'user_id', '=', 'users.id')
                ->whereNull('gain_types.deleted_at')
                ->where('gain_type_translations.locale', '=', $locale)
                ->select('gain_type_translations.name', 'price_sum', 'updated', 'users.name as user_name', 'user_id', 'users.phone')
                ->get();
            return datatables()->of($gain_type)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="showenter" id="' . $data->user_id . '"  class="showB showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
//        $points_price = PointPrice::query()->first();
        return view('movement_points.index');/*->with('points_price', $points_price->price);*/
    }

    public function show($id)
    {
        $locale = app()->getLocale();
        $latestPosts = DB::table('gain_methods')
            ->select('gain_type_id', 'user_id', DB::raw('sum(gain_methods.price) as price_sum'), DB::raw('max(point_histories.updated_at) as updated'))
            ->join('point_histories', 'gain_methods.id', '=', 'point_histories.gain_method_id')
            ->where('point_histories.user_id', '=', $id)
            ->groupBy('gain_methods.gain_type_id', 'point_histories.user_id');

        $movement_points = DB::table('gain_types')
            ->joinSub($latestPosts, 'latest_posts', function ($join) {
                $join->on('gain_types.id', '=', 'latest_posts.gain_type_id');
            })
            ->join('gain_type_translations', 'gain_types.id', '=', 'gain_type_translations.gain_type_id')
            ->join('users', 'user_id', '=', 'users.id')
            ->whereNull('gain_types.deleted_at')
            ->where('gain_type_translations.locale', '=', $locale)
            ->select('gain_type_translations.name', 'price_sum', 'updated', 'users.name as user_name', 'user_id', 'users.phone')
            ->get()->first();

        return response()->json([
            'user_id' => $movement_points->user_id,
            'price_sum' => $movement_points->price_sum,
            'user_name' => $movement_points->user_name,
            'phone' => $movement_points->phone,
            'name' => $movement_points->name,
            'updated_at' => Carbon::Parse($movement_points->updated)->format('Y/m/d'),
        ]);

    }
}
