<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductDiscount;
use App\Models\ProductPrice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class ProductDiscountController extends Controller
{
    public $view_path = 'managements.discounts.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = ProductDiscount::all();
            return datatables()->of($data)
                ->addColumn('action', function ($data) {
                    $actions = '';
                    if (Gate::check('update product_discounts')) {
                        $actions = '<a class="action-item edit-table-row" id="' . $data->id . '"><i class="ft ft-edit color-blue"></i></a>';
                    }
                    return $actions;
                })->setRowAttr([
                    'style' => function ($data) {
                        if ($data->is_active != 1) return 'background-color: #fefafa;';
                        if ($data->end_date < Carbon::now()) return 'background-color: #fefafa;';
                    }
                ])
                ->editColumn('product_name', function ($data) {
                    return $data->productPrice->product->name ?? null .  ' ' . $data->productPrice->unit->name ?? null;
                })->editColumn('branch_name', function ($data) {
                    return $data->branch->name;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view($this->view_path . 'product_discounts.index')
            ->with('categories', Category::where('status', 1)->get())
            ->with('branches', $branches = Branch::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::where('branch_id', $request->branch_id)->pluck('id')->toArray();
        $product_price_val = ProductPrice::whereIn('product_id', $product)->pluck('id')->toArray();

        if ($request->type == 'product') {
        $count_product_price_val = count($request->product_price_val ?? []);
        if ($count_product_price_val == 0) {
            return response()->json([
                'product_price_val' => Lang::get('validation.you_must_specify_at_least_one_product')
            ], 401);
        }

            $validatedData = Validator::make(
                $request->all(), [
                'branch_id' => 'required|string|max:191',
                'value_type' => 'required|string|in:price,percentage',
                'price' => [
                    'required',
                    'numeric',
                    function ($attribute, $value, $fail) use ($request) {
                        if ($request->value_type == 'percentage') {
                            if ($value > 100)
                                $fail(lang::get("validation.max.numeric") . ' 100.');
                            if ($value < 0)
                                $fail(lang::get("validation.min.numeric") . ' 0.');
                        } else {
                            if ($value > 1000000)
                                $fail(lang::get("validation.max.numeric") . ' 1000000');
                        }
                    }],
                'start_date' => ['required', 'date', 'after:yesterday',
                    function ($attribute, $value, $fail) use ($request) {
                        $product_discounts = ProductDiscount::
                        where('product_price_id', $request->product_price_val)
                            ->where('is_active', 1)
                            ->get();
                        foreach ($product_discounts as $product_discount) {
                            if ($value <= $product_discount->end_date) {
                                $fail(Lang::get('validation.there_is_a_prior_discount_for_this_date') . '.');
                            }
                        }
                    }],
                'end_date' => 'required|date|after_or_equal:start_date',
            ], [
                'after' => __('validation.date_most_be_at_least_today'),
            ]);

            if ($validatedData->fails())
                return response()->json(['error' => $validatedData->errors()], 401);


            foreach ($request->product_price_val as $product_price) {

                $product_discount = new ProductDiscount();
                $product_discount->product_price_id = $product_price;
                $product_discount->start_date = $request->start_date;
                $product_discount->branch_id = $request->branch_id;
                $product_discount->end_date = $request->end_date;
                $product_discount->value_type = $request->value_type;
                $product_discount->value = $request->price;
                $product_discount->save();
            }
        } else {

            $count_product_price_val = count($product_price_val ?? []);
            if ($count_product_price_val == 0) {
                return response()->json([
                    'product_price_val' => Lang::get('validation.you_must_specify_at_least_one_product')
                ], 401);
            }

            $validatedData = Validator::make(
                $request->all(), [
                'branch_id' => 'required|string|max:191',
                'value_type' => 'required|string|in:price,percentage',
                'price' => [
                    'required',
                    'numeric',
                    function ($attribute, $value, $fail) use ($request) {
                        if ($request->value_type == 'percentage') {
                            if ($value > 100)
                                $fail(lang::get("validation.max.numeric") . ' 100.');
                            if ($value < 0)
                                $fail(lang::get("validation.min.numeric") . ' 0.');
                        } else {
                            if ($value > 1000000)
                                $fail(lang::get("validation.max.numeric") . ' 1000000');
                        }
                    }],
                'start_date' => ['required', 'date', 'after:yesterday',
                    function ($attribute, $value, $fail) use ($request,$product_price_val) {
                        $product_discounts = ProductDiscount::
                        where('product_price_id', $product_price_val)
                            ->where('is_active', 1)
                            ->get();
                        foreach ($product_discounts as $product_discount) {
                            if ($value <= $product_discount->end_date) {
                                $fail(Lang::get('validation.there_is_a_prior_discount_for_this_date') . '.');
                            }
                        }
                    }],
                'end_date' => 'required|date|after_or_equal:start_date',
            ], [
                'after' => __('validation.date_most_be_at_least_today'),
            ]);

            if ($validatedData->fails())
                return response()->json(['error' => $validatedData->errors()], 401);

            foreach ($product_price_val as $product_price) {

                $product_discount = new ProductDiscount();
                $product_discount->product_price_id = $product_price;
                $product_discount->start_date = $request->start_date;
                $product_discount->branch_id = $request->branch_id;
                $product_discount->end_date = $request->end_date;
                $product_discount->value_type = $request->value_type;
                $product_discount->value = $request->price;
                $product_discount->save();
            }
        }

        return response()->json([
            'success' => Lang::get('admin.added_successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_discount = ProductDiscount::find($id);

        $product_name = $product_discount->productPrice->product->name . " (<span style='color:rgba(10,85,164,0.76)'> " . $product_discount->productPrice->unit->name . "</span>)";
//        if ($product_discount->unitName != null) {
//            $product_name = $product_name . " (<span style='color:#f1c40f'>" . $product_discount->unitName . "</span>)";
//        }

//        if ($product_discount->tasteName != null){
//            $product_name = $product_name . " |<span style='color:rgba(10,85,164,0.76)'>" . $product_discount->tasteName . "</span>";
//        }

        return response()->json([
            'id' => $product_discount->id,
            'product_name' => $product_name,
            'price' => $product_discount->value,
            'value_type' => $product_discount->value_type,
            'start_date' => $product_discount->start_date_carbon,
            'branch_id' => $product_discount->branch_id,
            'is_active' => $product_discount->is_active,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'status' => 'required|integer',
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $product_discount = ProductDiscount::find($id);


        if ($request->status == 1) {
            $discounts = ProductDiscount::where('id', '!=', $id)
                ->where('product_price_id', $product_discount->product_price_id)
                ->where('is_active', 1)
                ->get();

            foreach ($discounts as $discount) {
                if ($product_discount->start_date <= $discount->end_date) {
                    return response()->json([
                        'status_error' => Lang::get('validation.there_is_a_prior_discount_for_this_date') . ' ' .
                            Lang::get('validation.you_cannot_activate')
                    ], 401);
                }
            }
        }
        $product_discount = ProductDiscount::find($id);
        $product_discount->is_active = $request->status;
        $product_discount->save();

        return response()->json([
            'success' => Lang::get('admin.edited_successfully')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
