<?php

namespace App\Http\Controllers;

use App\Video;
use App\VideoTranslation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('video_translations')
                ->join('videos', 'video_translations.video_id', '=', 'videos.id')
                ->where('video_translations.locale', '=', $locale)
                ->whereNull('videos.deleted_at')
                ->select('videos.*', 'video_translations.name')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $span = '<span class="dropdown">';
                    $span .= '<button id="btnSearchDrop2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings"></i></button>';
                    $span .= '<span aria-labelledby="btnSearchDrop2" class="dropdown-menu mt-1 dropdown-menu-right">';
                    $span .= '<button type="button" name="show" id="' . $data->id . '" class="showB dropdown-item"><i class="la la-eye"></i>' . lang::get("admin.show") . '</button>';
                    $span .= '';
                    if (Gate::check('update video')) {
                        $span .= '<a href="' . url("video/edit") . '/' . $data->id . '"   class="dropdown-item"><i class="la la-pencil"></i>' . lang::get("admin.edit") . '</a>';
                    }
                    if (Gate::check('delete video')) {
                        $span .= '<button type="button" name="delete" class="deleteB dropdown-item" id="' . $data->id . '"><i class="la la-trash"></i>' . lang::get("admin.delete") . ' </button></span></span>';
                    }
                    return $span;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('video.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('video.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'en_name' => 'required|string|max:191',
            'start_date' => 'required|date|after:yesterday',
            'end_date' => 'nullable|date|after_or_equal:start_date',
            'link' => 'required|url|max:191',
        ],[
            'after' => __('validation.date_most_be_at_least_today'),
        ]);

        $video = new Video();
        $video->links = $request->link;
        $video->start_date = $request->start_date;
        $video->end_date = $request->end_date;
        $video->save();

        $video->translateOrNew('ar')->name = $request->name;
        $video->translateOrNew('ar')->desc = $request->desc_ar;
        $video->translateOrNew('en')->name = $request->en_name;
        $video->translateOrNew('en')->desc = $request->desc_en;

        $video->save();
        return redirect('video')->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $locale = app()->getLocale();
        $video = DB::table('video_translations')
            ->join('videos', 'video_translations.video_id', '=', 'videos.id')
            ->where('video_translations.locale', '=', $locale)
            ->whereNull('videos.deleted_at')
            ->where('videos.id', '=', $id)
            ->select('videos.*', 'video_translations.name', 'video_translations.desc')
            ->get()
            ->first();

        return response()->json([
            'id' => $video->id,
            'name' => $video->name,
            'desc' => $video->desc,
            'start_date' => $video->start_date,
            'end_date' => $video->end_date,
            'created_at' => Carbon::Parse($video->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($video->updated_at)->format('Y/m/d'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::find($id);
        $video_ar = VideoTranslation::where('video_id', '=', $id)->where('locale', 'ar')->first();
        $video_en = VideoTranslation::where('video_id', '=', $id)->where('locale', 'en')->first();
        return view('video.edit', array('video' => $video, 'video_ar' => $video_ar, 'video_en' => $video_en));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'en_name' => 'required|string|max:191',
            'start_date' => 'required|date|after:yesterday',
            'end_date' => 'nullable|date|after_or_equal:start_date',
            'link' => 'required|url|max:191',
        ],[
            'after' => __('validation.date_most_be_at_least_today'),
        ]);

        $video = Video::find($id);
        $video->links = $request->link;
        $video->start_date = $request->start_date;
        $video->end_date = $request->end_date;
        $video->save();

        $video->translateOrNew('ar')->name = $request->name;
        $video->translateOrNew('ar')->desc = $request->desc_ar;
        $video->translateOrNew('en')->name = $request->en_name;
        $video->translateOrNew('en')->desc = $request->desc_en;

        $video->save();
        return redirect('video')->with('success', Lang::get('admin.edited_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::findOrFail($id);
        $video->delete();
        return redirect('/');
    }

    /**
     *  displays the deleted data as an initial deletion
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function recycle_bin()
    {
        $locale = app()->getLocale();
        if (request()->ajax()) {
            $dataselect = DB::table('video_translations')
                ->join('videos', 'video_translations.video_id', '=', 'videos.id')
                ->where('video_translations.locale', '=', $locale)
                ->whereNotNull('videos.deleted_at')
                ->select('videos.*', 'video_translations.name')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="restore" id="' . $data->id . '" class="restore btn btn-primary btn-sm" data-toggle="Restore" title="Restore"><i class="ft ft-rotate-cw"></i></button>';
                    $button .= '&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('video.recycle_bin');
    }

    /**
     * retrieves deleted data from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore($id)
    {
        $video = Video::withTrashed()->where('id', $id)->first();
        $video->restore();
        return response()->json();
    }

    /**
     * performs the final deletion from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function hdelete($id)
    {
        DB::table('video_translations')
            ->where('video_id', '=', $id)
            ->delete();
        $video = Video::withTrashed()->where('id', $id)->first();
        $video->forceDelete();
    }
}
