<?php

namespace App\Http\Controllers;

use App\BalanceTransaction;
use App\PointPrice;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class MovementPointsController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $movement_points = BalanceTransaction::where('type', 'add_from_point')
                ->orderBy('created_at', 'desc')
                ->get();
            return datatables()->of($movement_points)
                ->editColumn('created_at', function ($data) {
                    return $data->created_at;
                })
                ->editColumn('user_name', function ($data) {
                    return $data->userWallet->user->name;
                })
                ->editColumn('phone', function ($data) {
                    return $data->userWallet->user->phone;
                })
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="showenter" id="' . $data->id . '"  class="showB showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('movement_points.index');
    }

    public function show($id)
    {

        $movement_points = BalanceTransaction::find($id);

        return response()->json([
            'id' => $movement_points->id,
            'amount' => $movement_points->amount,
            'user_name' => $movement_points->userWallet->user->name,
            'phone' => $movement_points->userWallet->user->phone,
            'created_at' => Carbon::Parse($movement_points->created_at)->format('Y/m/d'),
        ]);

    }
}
