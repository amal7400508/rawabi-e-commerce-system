<?php

namespace App\Http\Controllers;

use App\ShowCases;
use App\SpecialClass;
use App\SpecialProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class ShowCasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $dataselect = ShowCases::all();
            return datatables()->of($dataselect)
                ->editColumn('special_class', function ($data){
                    return $data->specialClass->name;
                })
                ->addColumn('action', function ($data) {
                    $button = '';
                    if (Gate::check('create show_cases')) {
                        $button .= '<button type="button" name="edit" id="' . $data->id . '" class="edit-table-row btn btn-primary btn-sm" data-toggle="edite" title="edite"><i class="ft ft-edit"></i></button>';
                        $button .= '&nbsp';
                    }
                    $button .= '<button type="button" name="showenter" id="' . $data->id . '"  class="showdetail showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    if (Gate::check('delete show_cases')) {
                        $button .= '&nbsp';
                        $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm" data-toggle="delete" title="delete"><i class="ft ft-trash-2"></button>';
                    }
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('show_cases.index')
            ->with(
                'special_classes',
                SpecialClass::where('status', 1)->get()
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'special_class' => 'required|integer',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image = $request->file('image');
        $imageName = 'moka_' . time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('storage/showCase/'), $imageName);
        $show_cases = new ShowCases();
        $show_cases->special_class_id = $request->special_class;
        $show_cases->image = 'showCase/' . $imageName;
        $show_cases->save();

        return redirect()->back()->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show_cases = ShowCases::find($id);
        $btnShow = '';
        if (Gate::check('delete taste')) {
            $btnShow .= '<button  class="delete btn btn-danger btn-float btn-round" id="' . $show_cases->id . '" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></i></button>';
        }
        return response()->json([
            'id' => $show_cases->id,
            'special_class' => $show_cases->specialClass->name,
            'image' => $show_cases->image,
            'created_at' => Carbon::Parse($show_cases->created_at)->format('Y/m/d'),
            'updated_at' => Carbon::Parse($show_cases->updated_at)->format('Y/m/d'),
            'btn' => $btnShow,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $show_cases = ShowCases::find($id);
        return response()->json([
            'id' => $show_cases->id,
            'special_class' => $show_cases->special_class_id,
            'image' => $show_cases->image,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'special_class' => 'required|integer',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = 'moka_' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('storage/showCase/'), $imageName);

            $filename = public_path() . '/storage/' . ShowCases::where('id', '=', $id)->get()->first()->image;

            File::delete($filename);
        }
        $show_cases = ShowCases::find($id);
        $show_cases->special_class_id = $request->special_class;

        if ($request->hasFile('image')) {
            $show_cases->image = 'showCase/' . $imageName;
        }
        $show_cases->save();

        return redirect()->back()->with('success', Lang::get('admin.edited_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function recycle_bin()
    {
        if (request()->ajax()) {
            $dataselect = ShowCases::withTrashed()
                ->whereNotNull('deleted_at')
                ->get();
            return datatables()->of($dataselect)
                ->editColumn('special_class', function ($data){
                    return $data->specialClass->name;
                })
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="restore" id="' . $data->id . '" class="restore btn btn-primary btn-sm" data-toggle="Restore" title="Restore"><i class="ft ft-rotate-cw"></i></button>';
                    $button .= '&nbsp';
                    $button .= '<button type="button" name="delete" id="' . $data->id . '" class="delete btn btn-danger btn-sm" data-toggle="Delete" title="Delete"><i class="ft ft-trash-2"></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('show_cases.recycle_bin');
    }

    public function destroy($id)
    {
        $special_product = SpecialProduct::withTrashed()->where('show_case_id', $id)->count();
        if ($special_product > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        $show_cases = ShowCases::find($id);
        $show_cases->delete();
        return response()->json();
    }

    /**
     * retrieves deleted data from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore($id)
    {
        $show_cases = ShowCases::withTrashed()->where('id', $id)->first();
        $show_cases->restore();
        return response()->json();
    }

    /**
     * performs the final deletion from the Recycle Bin
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function hdelete($id)
    {
        $special_product = SpecialProduct::withTrashed()->where('show_case_id', $id)->count();
        if ($special_product > 0) {
            return response()->json([
                'error_delete' => Lang::get('admin.Sorry_you_cannot_delete_this_record_because_it_has_other_links')
            ]);
        }
        $filename = public_path() . '/storage/' . DB::table('show_cases')->where('id', '=', $id)->get()->first()->image;
        File::delete($filename);
        $show_cases = ShowCases::withTrashed()->where('id', $id)->first();
        $show_cases->forceDelete();
    }
}
