<?php

namespace App\Http\Controllers;

use App\Exports\AccountExport;
use App\NotificationsMovement;
use App\User;
use App\User_wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Facades\Excel;

class UserWalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;
        if ($table_length == '') $table_length = 10;

        $wallets = User_wallet::join('users', 'user_wallets.user_id', '=', 'users.id')
            ->select('user_wallets.*', 'users.name', 'users.phone');

        $query = $request['query'] ?? null;
        $search_type = $request['search_type'] ?? null;
        if (!is_null($search_type) && !is_null($query)):
            if ($search_type == 'number') {
                $wallets = $wallets->where('user_wallets.id', $query);
            } else if ($search_type == 'name') {
                $wallets = $wallets->where('full_name', 'like', '%' . $query . '%');
            } else if ($search_type == 'phone') {
                $wallets = $wallets->where('phone', '=', $query);
            }
        endif;

        $wallets = $wallets->orderBy('updated_at', 'desc');
        $wallets_count = $wallets->count();

        return view('user_wallets.index')
            ->with('wallets', $wallets->paginate($table_length))
            ->with('wallets_count', $wallets_count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    public function index1()
    {
        if (request()->ajax()) {
            $dataselect = DB::table('user_wallets')
                ->join('users', 'user_wallets.user_id', '=', 'users.id')
                ->select('user_wallets.*', 'users.name', 'users.phone')
                ->get();
            return datatables()->of($dataselect)
                ->addColumn('action', function ($data) {
                    $button = '';
                    if (Gate::check('update user_wallet')) {
                        $button .= '<a href="/user_wallet/edit/' . $data->id . '" name="edit"  class="edit-table-row edit btn btn-primary btn-sm" data-toggle="edite" title="edite"><i class="ft ft-edit"></i></a>';
                        $button .= '&nbsp';
                    }
                    $button .= '<button type="button" name="showenter" id="' . $data->id . '"  class="showdetail showenter btn btn-info btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('user_wallets.index');
    }

//    public function account_open()
//    {
//        if (request()->ajax()) {
//
//            $userdata = DB::table('users')->latest();
//            return datatables()->of($userdata)
//                ->addColumn('action', function ($data) {
//                    if ($data->status == 1) {
//                        $button = '<a href="/user_wallet/create/' . $data->id . '" name="showenter" id="' . $data->id . '"  class=" btn btn-primary btn-sm" data-toggle="account_open" title="' . Lang::get("admin.account_open") . '"><i class="ft ft-plus"></i>' . ' ' . Lang::get("admin.account_open") . '</a>';
//                        $button .= '&nbsp';
//                        $button .= '<a href="/deposit/create/' . $data->id . '" name="showenter" id="' . $data->id . '"  class=" btn btn-primary btn-sm" data-toggle="account_open" title="' . Lang::get("admin.deposit") . '"><i class="ft ft-plus"></i>' . ' ' . Lang::get("admin.deposit") . '</a>';
//                    } else {
//                        $button = '<button disabled class=" btn btn-primary btn-sm" data-toggle="account_open" title="' . Lang::get("admin.account_open") . '"><i class="ft ft-plus"></i>' . ' ' . Lang::get("admin.account_open") . '</button>';
//                        $button .= '&nbsp';
//                        $button .= '<button disabled class=" btn btn-primary btn-sm" data-toggle="account_open" title="' . Lang::get("admin.deposit") . '"><i class="ft ft-plus"></i>' . ' ' . Lang::get("admin.deposit") . '</button>';
//                    }
//                    $button .= '&nbsp';
//                    $button .= '<button type="button" name="showenter" id="' . $data->id . '"  class="showdetail showenter btn btn-info btn-sm" data-toggle="show" title="' . Lang::get("admin.show") . '"><i class="ft ft-eye"></i></button>';
//                    return $button;
//                })
//                ->rawColumns(['action'])
//                ->make(true);
//        }
//        return view('user_wallets.account_open')->with('user_wallet', User_wallet::all());
//    }

    public function usershow($id)
    {
        $users = User::find($id);
        $btnShow = '<a href="/users/edit/' . $users->id . '" class="btn btn-float btn-round btn-primary" data-toggle="Edit" title="Edit"><i class="ft ft-edit"></i></a>';
        return response()->json([
            'id' => $users->id,
            'name' => $users->name,
            'email' => $users->email,
            'phone' => $users->phone,
            'gender' => $users->gender,
            'image' => $users->image,
            'status' => $users->status,
            'birth_date' => Carbon::Parse($users->birth_date)->format('Y/m/d'),
            'email_verified_at' => $users->email_verified_at,
            'address' => $users->address,
            'points_account_num' => $users->points_account_num,
            'parent_points_account_num' => $users->parent_points_account_num,
            'monthly_share' => $users->monthly_share,
            'updated_at' => Carbon::Parse($users->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($users->created_at)->format('Y/m/d'),
            'btn' => $btnShow,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $user = User::find($id);
        if ($user->status != 1) {
            return redirect()->back();
        }
        $user_wallet = User_wallet::all();
        foreach ($user_wallet as $user_wallets) {
            if ($user_wallets->user_id == $user->id) {
                return redirect()->back()->with('error', Lang::get('admin.this_user_already_has_an_account'));
            }
        }
        $birth = \Carbon\Carbon::Parse($user->birth_date)->format('Y/m/d');
        return view('user_wallets.create')->with('user', $user)->with('birth', $birth);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'opening_balance' => 'required|numeric|min:2000|max:900000',
        ]);

        if ( $request->opening_balance % 5 != 0) {
            return back()->withErrors(['opening_balance'=> Lang::get('validation.amount_incorrect')])
                ->withInput();
        }
        $user_wallet = new User_wallet();
        $user_wallet->openning_balance = $request->opening_balance;
        $user_wallet->current_balance = $request->current_balance + $request->opening_balance;
        $user_wallet->status = $request->status;
        $user_wallet->user_id = $request->id;
        $user_wallet->save();

        $notifications_movement = new NotificationsMovement();
        $notifications_movement->notification_id = 2;
        $notifications_movement->type = 'private';
        $notifications_movement->sanding_time = Carbon::now()->toTimeString();
        $notifications_movement->sanding_date = Carbon::now()->toDateString();
        $notifications_movement->user_id = $user_wallet->user_id;
        $notifications_movement->amount = $request->opening_balance. ' RY';
        $notifications_movement->save();

        $fcm_title = app()->getLocale() == 'ar'
            ? 'فتح حساب' : 'Open an account';
        $fcm_body = app()->getLocale() == 'ar'
            ? 'لقد تم فتح حسابك بمبلغ ' . $request->opening_balance. ' ريال'
            : 'Your account has been opened with ' . $request->opening_balance. ' RY';
        $user_ids = $request->id;
        \App\BySwadi\Helpers\Fcm::send_MKA_notification($fcm_title, $fcm_body, $user_ids);

        return redirect('/user_wallet')->with('success', Lang::get('admin.added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_wallet = User_wallet::find($id);
        $btnShow = '';
        if (Gate::check('update user_wallet')) {
            $btnShow .= '<a href="/user_wallet/edit/' . $user_wallet->id . '" class="btn btn-float btn-round btn-primary" data-toggle="Edit" title="Edit"><i class="ft ft-edit"></i></a>';
        }
        return response()->json([
            'id' => $user_wallet->id,
            'name' => $user_wallet->user->name,
            'email' => $user_wallet->user->email,
            'phone' => $user_wallet->user->phone,
            'image' => $user_wallet->user->image,
            'status' => $user_wallet->status,
            'openning_balance' => $user_wallet->openning_balance,
            'current_balance' => $user_wallet->current_balance,
            'updated_at' => Carbon::Parse($user_wallet->updated_at)->format('Y/m/d'),
            'created_at' => Carbon::Parse($user_wallet->created_at)->format('Y/m/d'),
            'btn' => $btnShow,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_wallet_show = DB::table('user_wallets')
            ->join('users', 'user_wallets.user_id', '=', 'users.id')
            ->select('user_wallets.*', 'users.name', 'users.phone', 'users.email', 'users.image')
            ->where('user_wallets.id', '=', $id)
            ->get()
            ->first();
        return view('user_wallets.edit')->with('user_wallet_show', $user_wallet_show);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_wallet = User_wallet::find($id);
        $user_wallet->status = $request->status;
        $user_wallet->save();
        return redirect('/user_wallet')->with('success', Lang::get('admin.edited_successfully'));
    }


    public function report(Request $request)
    {
        if (request()->ajax()) {
            $data = $this->getDataReport($request);
            return datatables()->of($data)
                ->addColumn('action', function ($data) {
                    $button = '<button type="button" name="showenter" id="' . $data->id . '"  class="showdetail showenter btn btn-primary btn-sm" data-toggle="show" title="show"><i class="ft ft-eye"></i></button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('user_wallets.report');
    }

    public function getDataReport($request)
    {
        if ($request->confirm == 1) {
            $data = User_wallet::join('users', 'user_wallets.user_id', '=', 'users.id')
                ->select('user_wallets.*', 'users.name', 'users.phone');
            if (!is_null($request->from)) {
                $data = $data->whereDate('user_wallets.created_at', '>=', $request->from);
            }
            if (!is_null($request->to)) {
                $data = $data->whereDate('user_wallets.created_at', '<=', $request->to);
            }
            if (!is_null($request->account_no)) {
                $data = $data->where('user_wallets.id', $request->account_no);
            }
            if (!is_null($request->phone)) {
                $data = $data->where('users.phone', 'like', '%' . $request->phone . '%');
            }
            if (!is_null($request->user_name)) {
                $data = $data->where('users.name', 'like', '%' . $request->user_name . '%');
            }
            if (!is_null($request->email)) {
                $data = $data->where('users.email', 'like', '%' . $request->email . '%');
            }
            if (!is_null($request->opening_balance)) {
                $data = $data->where('user_wallets.opening_balance', $request->opening_balance);
            }
            if (!is_null($request->current_balance)) {
                $data = $data->where('user_wallets.current_balance', $request->current_balance);
            }
            if (!is_null($request->status)) {
                $data = $data->where('user_wallets.status', $request->status);
            }
            if (!is_null($request->gender)) {
                $data = $data->where('users.gender', $request->gender);
            }

            return $data->get();
        } else {
            $deposits = User_wallet::where('id', 0);
        }

        return $deposits;

    }

    public function export(Request $request)
    {
        $request->request->add(['confirm'=>1]);
        $data = $this->getDataReport($request);
        return Excel::download(
            new AccountExport($data),
            'accounts-'.now().'.xlsx'
        );
    }
}
