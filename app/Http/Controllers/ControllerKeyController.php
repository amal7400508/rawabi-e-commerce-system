<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Encryption\DecryptException;
use \Illuminate\Support\Facades\Crypt as Key;

class ControllerKeyController extends Controller
{
    /**
     * @param $e
     * @return false|string
     */
    static function compress($e)
    {
//        $pass = "alsaloulpp80ifmo";
//        $iv = "0051474548110147";
//        $method = "AES-256-CBC";

        try {
            $pass = str_replace(
                "AAAA8PYBZGM:",
                "",
                Key::decryptString(
                    "eyJpdiI6I" .
                    env('CK_PASS')
                )
            );

            $iv = str_replace(
                "IV:",
                "",
                Key::decryptString(
                    "eyJpdiI6I" .
                    env('CK_IV')
                )
            );

            $method = str_replace(
                "method:",
                "",
                Key::decryptString(
                    "eyJpdiI6I" .
                    env('CK_METHOD')
                )
            );

            $ed = base64_decode($e);

            $n = openssl_decrypt(
                "$ed",
                $method,
                $pass,
                0,
                $iv
            );
            return $n;
        } catch (DecryptException $exception) {
            return $exception;
        }
    }
}
