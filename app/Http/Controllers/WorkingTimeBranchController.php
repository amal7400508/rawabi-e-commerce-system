<?php

namespace App\Http\Controllers;

use App\Http\Resources\WorkingTimesDetailsResource;
use App\Models\Branch;
use App\Rules\CheckDays;
use App\Models\WorkingTime;
use App\Models\WorkingTimeTranslation;
use App\Rules\CheckWorkingTimes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class WorkingTimeBranchController extends Controller
{
    public $view_path = 'managements.encoding.working_times_branch.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if (request()->ajax()) {
            $working_time = WorkingTime::whereNotNull('branch_id')->groupBy('week_day')->get('week_day');
            return datatables()->of($working_time)
                ->addColumn('action', function ($data) {
                    if (Gate::check('update working_times_branch') || Gate::check('delete working_times_branch')) {
                        $actions = '<a class="action-item detail" id="' . $data->week_day . '"><span style="font-size:12px;color: #ed1b24">' . __('admin.detail') . '...</span></a>';
                    }
                    $actions .='';
                    return $actions;
                })->rawColumns(['action'])
                ->make(true);
        }
        $branches = Branch::get();
        return view($this->view_path . 'index')->with('branches', $branches);
    }

    public function details($week_day)
    {
        $working_time = WorkingTime::whereNotNull('branch_id')->where('week_day', $week_day)
            ->get();
        $data = WorkingTimesDetailsResource::collection($working_time);
        return response()->json([
            'day' => $working_time->first()->day,
            'max_updated_at' => $working_time->max('updated_at')->diffForHumans(),
            'data' => $data
        ]);

    }

    public function detailsBranch($week_day, $branch_id)
    {
        if ($branch_id != 0) {
            $working_time = WorkingTime::whereNotNull('branch_id')->where('week_day', $week_day)
                ->where('branch_id', $branch_id)
                ->get();
        } else {
            $working_time = WorkingTime::whereNotNull('branch_id')->where('week_day', $week_day)
                ->get();
        }
        $data = WorkingTimesDetailsResource::collection($working_time);
        return response()->json([
            'day' => $working_time->first()->day,
            'max_updated_at' => $working_time->max('updated_at')->diffForHumans(),
            'data' => $data
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make(
            $request->all(), [
            'days.*' => [/*new CheckWorkingTimes(),*/ function ($attribute, $value, $fail) use ($request) {
                $days = ['FRIDAY', 'SATURDAY', 'SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY'];
                if (!in_array($value, $days)) {
                    $fail(lang::get("admin.error_date") . '.');
                }
            }],
            'start_time' => ['required', 'date_format:H:i',
//                function ($attribute, $value, $fail) use ($request) {
//                    foreach ($request->days as $day) {
//                        $time = Carbon::Parse($value)->format('H:i');
//                        $working_times = WorkingTime::whereNull('branch_id')
//                            ->where('week_day', '=', $day)
//                            ->where('start_time', '<=', $time)
//                            ->where('end_time', '>=', $time)
//                            ->get()->count();
//
//                        if ($working_times == 0) {
//                            $fail(checkDays($day . __('admin.error_date')));
//                        }
//                    }
//                }
            ],
            'end_time' => ['required', 'date_format:H:i', 'after_or_equal:start_time',
//                function ($attribute, $value, $fail) use ($request) {
//                    foreach ($request->days as $day) {
//                        $time = Carbon::Parse($value)->format('H:i');
//                        $working_times = WorkingTime::whereNull('branch_id')
//                            ->where('week_day', '=', $day)
//                            ->where('start_time', '<=', $time)
//                            ->where('end_time', '>=', $time)
//                            ->get()->count();
//
//                        if ($working_times == 0) {
//                            $fail(checkDays($day . __('admin.error_date')));
//                        }
//                    }
//                }
            ],
//            'branch_id' => ['required', new CheckBranch()],
            'branch_id' => ['required'],
        ]);

        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $count_days = count($request->days ?? []);
        if ($count_days == 0) {
            return response()->json([
                'days' => Lang::get('validation.you_must_specify_at_least_one_day')
            ], 401);
        }

        $branches = Branch::pluck('id')->toArray();

        if ($request->branch_id != 'all') {
            foreach ($request->days as $day) {
                $working_time = new WorkingTime();
                $working_time->branch_id = $request->branch_id;
                $working_time->week_day = $day;
                $working_time->status = 1;
                $working_time->start_time = $request->start_time;
                $working_time->end_time = $request->end_time;
                $working_time->save();
            }
        } else {
            foreach ($branches as $branch) {
                foreach ($request->days as $day) {
                    $working_time = new WorkingTime();
                    $working_time->branch_id = $branch;
                    $working_time->week_day = $day;
                    $working_time->status = 1;
                    $working_time->start_time = $request->start_time;
                    $working_time->end_time = $request->end_time;
                    $working_time->save();
                }
            }
        }

        return response()->json([
            'message' => Lang::get('admin.added_successfully')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $working_time = WorkingTime::where('id', $id)->first();

        return response()->json([
            'id' => $working_time->id,
            'branch_id' => $working_time->branch_id,
            'is_active' => $working_time->status,
            'start_time' => Carbon::Parse($working_time->start_time)->format('H:i'),
            'end_time' => Carbon::Parse($working_time->end_time)->format('H:i'),
            'day' => $working_time->day,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        session()->put('note_id', $id);

        $working_time = WorkingTime::find($id);

        $this->validateWorkingTime($request);

        $working_time->branch_id = $working_time->branch_id;
        $working_time->start_time = $request->start_time;
        $working_time->end_time = $request->end_time;
        $working_time->status = $request->input('status', 0);
        $working_time->save();

//        $working_time->translateOrNew('ar')->message = $request->message_ar ?? '';
//        $working_time->translateOrNew('en')->message = $request->message_en ?? '';
//        $working_time->save();

        return redirect()->back()->with('success', Lang::get('admin.edited_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $working_time = WorkingTime::findOrFail($id);
        $working_time->delete();

        $message = Lang::get('admin.deleted_successfully');
        return response()->json(['message' => $message], 200);
    }

    public function validateWorkingTime($request)
    {
        return $this->validate($request, [
            'start_time' => ['required', 'date_format:H:i',
//                function ($attribute, $value, $fail) use ($request) {
//                    $time = Carbon::Parse($value)->format('H:i');
//                    $working_times = WorkingTime::whereNull('branch_id')
//                        ->where('start_time', '<=', $time)
//                        ->where('end_time', '>=', $time)
//                        ->get()->count();
//
//                    if ($working_times == 0) {
//                        $fail(__('admin.error_time'));
//                    }
//                }
            ],
            'end_time' => ['required', 'date_format:H:i', 'after_or_equal:start_time',
//                function ($attribute, $value, $fail) use ($request) {
//                    $time = Carbon::Parse($value)->format('H:i');
//                    $working_times = WorkingTime::whereNull('branch_id')
//                        ->where('start_time', '<=', $time)
//                        ->where('end_time', '>=', $time)
//                        ->get()->count();
//
//                    if ($working_times == 0) {
//                        $fail(__('admin.error_time'));
//                    }
//                }
            ],
        ]);
    }

    /**
     * delete all checked the products
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAll(Request $request)
    {
        $items_id = $request->items_id;

        foreach ($items_id as $id) {
            $working_time = WorkingTime::whereIn('id', (array)$items_id);
            $result = $working_time->delete();
            if ($result == 0) {
                $result = 'عذراً. لم يتم تحديد اي عنصر ';
                return response()->json(['message' => $result], 401);
            }
            $result = ' تم حذف ' . $result . ' عنصر بنجاح.';
            return response()->json(['message' => $result], 200);
        }

    }
}
