<?php

namespace App\Http\Controllers;

use App\Models\Ceiling;
use App\Models\Note;
use App\Models\NoteTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class CeilingController extends Controller
{
    public $view_path = 'managements.encoding.ceiling.';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $table_length = isset($_GET['table_length']) ? $_GET['table_length'] : 10;

        if ($table_length == '') $table_length = 10;

        $data = Ceiling::orderBy('id', 'desc');
        $count = $data->count();
        return view($this->view_path . 'index')
            ->with('data', $data->paginate($table_length))
            ->with('count', $count)
            ->with('pagination_links', [
                    'table_length' =>
                        isset($_GET['table_length']) ? $_GET['table_length'] : '',
                    'search_type' =>
                        isset($_GET['search_type']) ? $_GET['search_type'] : '',
                    'query' =>
                        isset($_GET['query']) ? $_GET['query'] : '',
                ]
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        if (Ceiling::count() != 0){
            return response()->json(['error' => 'error'], 401);
        }

        $ceiling = new Ceiling();
        $ceiling->price = $request->price;
        $ceiling->save();

        return response()->json([
            'status' => 200,
            'ceiling' => $ceiling,
            'title' => Lang::get('admin.added'),
            'message' => Lang::get('admin.added_successfully'),
        ]);
    }

    public function validatedData($request)
    {
        return Validator::make(
            $request->all(), [
            'price' => 'required|numeric|max:999999999.99|min:0',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ceiling = Ceiling::find($id);
        return response()->json([
            'id' => $ceiling->id,
            'price' => $ceiling->price,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validatedData($request);
        if ($validatedData->fails())
            return response()->json(['error' => $validatedData->errors()], 401);

        $ceiling = Ceiling::find($id);
        $ceiling->price = $request->price;
        $ceiling->save();

        return response()->json([
            'status' => 200,
            'ceiling' => $ceiling,
            'title' => Lang::get('admin.updated'),
            'message' => Lang::get('admin.edited_successfully'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ceiling::find($id)->delete();
        $message = Lang::get('admin.deleted_successfully');
        return response()->json([
            'message' => $message,
            'data_count' => Ceiling::count()
        ],
            200
        );
    }
}
