<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PharmacyCategoriesResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
//            'insurance_company' => $this->insuranceCompany->name ?? '',
            'pharmacy' =>  $this->branch->name,
            'category' => $this->mainCategory->name ?? null,
            'sub_category' => $this->subCategory->name ?? null,
            'commission' => $this->commission,
        ];
    }
}
