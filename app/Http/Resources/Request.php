<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Request extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
//        return dd($this);
        return [
            'is_active' => $this->is_active,
            'payment_type' => $this->payment_type,
            'receiving_type' => $this->receiving_type,
            'status' => $this->status,
            'branch_id' => $this->branch->name,
            'address_id' => !$this->address ?'':$this->address->name,
            'carrier_id' => !$this->carrier?'':$this->carrier->name,
            'coupon_id' => !$this->coupon?'':$this->coupon->number,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
