<?php

namespace App\Http\Resources;

use App\Models\Prescription;
use App\Models\SpecialProduct;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class AppNotifyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $image_on_error = asset('/storage/admins/64/avatar.jpg');

        if ($this->notifiable_type == 'App\V1\Models\Request') {
            $order = \App\Models\Request::find($this->data['id']);
            $user = $order->cart->user;
            $image = asset('/storage/' . $user->image);
//            $route = route( 'tracking');
            $route = route('request');
            $text_first = 'لقد قام المستخدم ';
            $user_name = $user->full_name;
            $text = ' بالطلب من الفرع ';
            $branch_name = '<strong class="font-size-base">' . $order->branch->name . '</strong>';
            $order_no = ' رقم الطلب: ' . '<span class="font-default">' . $order->request_number . '</span>';
            $text_last = $text . $branch_name . $order_no;
            $icon_and_bg_color = 'la la-shopping-cart bg-orange';
            $color = '#94264c08';
        } elseif ($this->notifiable_type == 'App\V1\Models\SpecialProduct') {
            $order = SpecialProduct::find($this->data['id']);
            $user = $order->user;
            $image = asset('/storage/' . $user->image);
            $route = route('special_product');
            $text_first = 'لقد قام المستخدم ';
            $user_name = $user->full_name;
            $text = ' بطلب طلب خاص من الفرع ';
            $branch_name = '<strong class="font-size-base">' . $order->branch->name ?? null . '</strong>';
            $order_no = ' رقم الطلب: ' . '<span class="font-default">' . $order->id . '</span>';
            $text_last = $text . $branch_name . $order_no;
            $icon_and_bg_color = 'la la-star bg-info bg-darken-4';
            $color = '#34517408';
        } elseif ($this->notifiable_type == 'App\V1\Models\Prescription') {
            $order = Prescription::find($this->data['id']);
            $user = $order->user;
            $image = asset('/storage/' . $user->image);
//            $route = '#';
//            if(\Illuminate\Support\Facades\Route::has(Auth::user()->type != 'insurance' ? route(providerType() . 'prescription') : route(providerType() . 'prescription-replies')) ) {
//                $route = Auth::user()->type != 'insurance' ? route(providerType() . 'prescription') : route(providerType() . 'prescription-replies');
//            }
            $route = route( 'prescription-replies');

            $text_first = 'لقد قام المستخدم ';
            $user_name = $user->full_name;
            $text = ' بطلب طلب روشتة من الفرع ';
            $branch_name = '<strong class="font-size-base">' . $order->branch->name ?? null . '</strong>';
            $order_no = ' رقم الطلب: ' . '<span class="font-default">' . $order->id . '</span>';
            $text_last = $text . $branch_name . $order_no;
            $icon_and_bg_color = 'la la-user-md bg-info';
            $color = '#34517408';
        }

        $notify =
            '<a href="' . $route . '">'
            . '     <div class="media" id="notifications-details" style="background-color: ' . $color . '">'
            . '         <div class="media-left align-self-center" id="notifications-icon">'
            . '             <div class="position-relative">'
            . '                 <img style="width: 64px;" onerror="this.src=\'' . $image_on_error . '\'" id="image-sidebar" class="rounded-circle img-center-64" src="' . $image . '">'
            . '                 <i class="icon-bg-circle icon-notify-img ' . $icon_and_bg_color . '"></i>'
            . '             </div>'
            . '         </div>'
            . '         <div class="media-body"><h6 class="media-heading" id="notifications-title" style="font-family: normal"></h6>'
            . '             <h6 class="notification_type-text text-muted" id="notifications-text" style="font-size: 13px">'
            . $text_first
            . '                 <strong class="font-size-base">' . $user_name . '</strong>'
            . $text_last
            . '             </h6>'
            . '             <small class="price category-color" id="created_at" style="font-family: normal">'
            . '                 <i class="ft-clock"></i> ' . $this->created_at->diffForHumans()
            . '             </small>'
            . '         </div>'
            . '     </div>'
            . ' </a>';

        return [
            'notify' => $notify
        ];
    }
}
