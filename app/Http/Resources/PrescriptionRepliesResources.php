<?php

namespace App\Http\Resources;

use App\Models\CartDetail;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed id
 * @property mixed medicine_name
 * @property mixed medicine_price
 * @property mixed is_alternative
 * @property mixed currency
 * @property mixed prescription_id
 * @property mixed note
 */
class PrescriptionRepliesResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'medicine_name' => $this->medicine_name,
            'medicine_price' => $this->medicine_price,
            'note' => $this->note ?? '',
            'is_alternative' => $this->is_alternative,
            'currency' => $this->currency,
            'prescription_id' => $this->prescription_id,
            'edit_disabled' => CartDetail::where('prescription_reply_id', $this->id)->count()? 'disabled' : '',
        ];
    }
}
