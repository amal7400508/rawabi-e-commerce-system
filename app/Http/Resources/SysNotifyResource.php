<?php

namespace App\Http\Resources;

use App\Models\Alert;
use App\Models\Branch;
use App\Models\Coupon;
use App\Models\Deposit;
use App\Models\NotificationMovement;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Provider;
use App\Models\User;
use App\Models\VolumeDiscount;
use Illuminate\Http\Resources\Json\JsonResource;

class SysNotifyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $provider_id = is_null($this->data['provider_id'] ?? null)
            ? $this->data['id'] : $this->data['provider_id'];

        $provider = Provider::find($provider_id);
        $image = asset('/storage/providers/64/' . $provider->image);
//        $image = 'http://127.0.0.1:201/storage/providers/64/' . $provider->image;
        $image_on_error = asset('/storage/admins/64/avatar.jpg');

        if ($this->notifiable_type == Product::class) {
            $number = Product::find($this->data['id'])->number;
            $route = route('products');
            $text_first = 'لقد قام ';
            $text_last = ' بإضافة منتج جديد برقم: <span class="font-default">' . $number . '</span> .';
            $icon_and_bg_color = 'la la-shopping-cart bg-info bg-darken-4';
        } elseif ($this->notifiable_type == Offer::class) {
            $route = route('offers_branches');
            $text_first = 'لقد قام ';
            $text_last = ' بإضافة عرض جديد برقم: <span class="font-default">' . $this->data['id'] . '</span> .';
            $icon_and_bg_color = 'la la-gift bg-red';
        } elseif ($this->notifiable_type == Alert::class) {
            $route = route('attentions_branches');
            $text_first = 'لقد قام ';
            $text_last = '  بإنشاء تنبية جديد ' . ' برقم: <span class="font-default">' . $this->data['id'] . '</span> .';
            $icon_and_bg_color = 'la la-bell bg-orange';
        } elseif ($this->notifiable_type == Deposit::class) {
            $route = route('deposit_branches');
            $text_first = 'لقد قام ';
            $user_name = User::find($this->data['user_id'])->full_name;
            $text_last = '  بإضافة ' . '<span class="font-default">' . $this->data['amount'] . '</span>' . ' ريال للمستخدم ' . '<strong class="font-size-base">' . $user_name . '</strong>' . '  برقم: <span class="font-default">' . $this->data['id'] . '</span> .';
            $icon_and_bg_color = 'la la-money bg-teal';
        } elseif ($this->notifiable_type == Branch::class) {
            $route = route('branches');
            $text_first = 'لقد قام ';
            $text_last = ' بإضافة فرع جديد برقم: <span class="font-default">' . $this->data['id'] . '</span> .';
            $icon_and_bg_color = 'la la-building bg-warning';
        } elseif ($this->notifiable_type == Coupon::class) {
            $coupon = Coupon::find($this->data['id']);
            $route = route('coupons_branches');
            $text_first = 'لقد قام ';
            $text_last = ' بإنشاء كوبون جديد بكود رقم: <span class="font-default">' . $coupon->number . '</span>' . ' بتخفيض ' . '<span class="font-default">' . $coupon->price_with_type . '</span>' . ' برقم: <span class="font-default">' . $this->data['id'] . '</span> .';
            $icon_and_bg_color = 'la la-code-fork bg-info';
        } elseif ($this->notifiable_type == VolumeDiscount::class) {
            $volume_discount = Coupon::find($this->data['id']);
            $route = route('volume_discounts');
            $text_first = 'لقد قام ';
            $text_last = ' بإنشاء تخفيض الحد الادنى للطلب بتخفيض ' . ' برقم: <span class="font-default">' . $this->data['id'] . '</span> .';
            $icon_and_bg_color = 'la la-heartbeat bg-info bg-darken-4';
        } elseif ($this->notifiable_type == NotificationMovement::class) {
            $route = "";//route('volume_discounts');
            $text_first = 'لقد قام ';
            $text_last = ' بإضافة حركة اشعار جديدة برقم: <span class="font-default">' . $this->data['id'] . '</span> .';
            $icon_and_bg_color = 'la la-bell bg-blue-grey';
        } elseif ($this->notifiable_type == Provider::class) {
            $route = route('providers');
            $text_first = 'لقد قام ';
            $text_last = '<span class="font-default"> بطلب تسجيل حساب مزود</span> .';
            $icon_and_bg_color = 'la la-user bg-blue-grey';
        }

        $notify =
            '<a href="' . $route . '">'
            . '     <div class="media" id="notifications-details">'
            . '         <div class="media-left align-self-center" id="notifications-icon">'
            . '             <div class="position-relative">'
            . '                 <img onerror="this.src=\'' . $image_on_error . '\'" id="image-sidebar" class="rounded-circle img-center-64" src="' . $image . '">'
            . '                 <i class="icon-bg-circle icon-notify-img ' . $icon_and_bg_color . '"></i>'
            . '             </div>'
            . '         </div>'
            . '         <div class="media-body"><h6 class="media-heading" id="notifications-title" style="font-family: normal"></h6>'
            . '             <h6 class="notification_type-text text-muted" id="notifications-text" style="font-size: 13px">'
            . $text_first
            . '                 <strong class="font-size-base">' . $provider->name . '</strong>'
            . $text_last
            . '             </h6>'
            . '             <small class="price category-color" id="created_at" style="font-family: normal">'
            . '                 <i class="ft-clock"></i> ' . $this->created_at->diffForHumans()
            . '             </small>'
            . '         </div>'
            . '     </div>'
            . ' </a>';

        return [
            'notify' => $notify
        ];
    }
}
