<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FalterCarrierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->carrier->id,
            'name' => $this->carrier->name,
            'cache_payment' => $this->cache_payment_today,
        ];
    }
}
