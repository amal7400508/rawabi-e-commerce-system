<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Gate;

class WorkingTimesDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $actions = '';
        if (Gate::check('update working_times')) {
            $actions .= '<a class="action-item edit-table-row" id="' . $this->id . '"><i class="la la-edit color-primary"></i></a>';
        }
        if (Gate::check('delete working_times')) {
            $actions .= '<a class="action-item delete" id="' . $this->id . '"><i class="la la-trash color-red"></i></a>';
        }

        $status = ($this->status == 1)
            ? "<div class='fonticon-wrap'><i class='ft-unlock' style='color:#002581'></i></div>"
            : "<div class='fonticon-wrap'><i class='ft-lock' style='color:#FC0021'></i></div>";

        return [
            'id' => $this->id,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'message' => $this->message,
            'status' => $status,
            'name_branch' => $this->branch->name ?? '',
            'status_color' => ($this->status == 1) ? '' : 'bg-attitude',
            'week_day' => $this->week_day,
            'created_at' => $this->created_at->diffForHumans(),
            'action' => $actions
        ];
    }
}
