<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommissionResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'branch' =>/* $this->branch->provider->name . ' | ' .*/ $this->branch->name,
            'category' => !is_null($this->mainCategory) ? $this->mainCategory->name ?? '' : '',
            'sub_category' => !is_null($this->subCategory) ? $this->subCategory->name ?? '' : '',
            'commission' => $this->commission,
        ];
    }
}
