<?php

namespace App\Http\Resources;

use App\CartDetail;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class ProductShowResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'unit_name' => $this->unitName,
            'note_ar' => $this->note_ar,
            'currency' => currency($this->currency),
            'price' => $this->price,
            'image' => $this->image,
            'order_availability' => $this->order_availability,
            'id_unit' => $this->unit->id ?? null,
            'request_number' => round($this->product_count)
        ];
    }
}
