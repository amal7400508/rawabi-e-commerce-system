<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OfferDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->productPrice->price,
            'price_name' => $this->productPrice->product->name ?? null . '<span style="margin: auto 5px">|</span>' . '<span class="blue font-default">' . $this->productPrice->unit->name ?? null . '</span>',// $this->price_name,
            'quantity' => $this->quantity,
            'note' => is_null($this->note)? '' : $this->note,
        ];
    }
}
