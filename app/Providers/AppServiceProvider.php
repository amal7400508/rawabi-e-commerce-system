<?php

namespace App\Providers;

use App\Models\Admin;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Intervention\Image\Gd\Driver;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Validator::extend('match_old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Admin::find(Auth::User()->id)->password);
//            dump(Hash::check($value, Admin::find(Auth::User()->id)->password));
//            dump(bcrypt($value));
//            dd( Admin::find(Auth::User()->id)->password);
//            dd(bcrypt($value) == Admin::find(Auth::User()->id)->password);
//            return bcrypt($value) == Admin::find(Auth::User()->id)->password;
        });




    }
}
