<?php

namespace App\Rules;

use App\Models\WorkingTime;
use Illuminate\Contracts\Validation\Rule;

class CheckStartDate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $start_date = WorkingTime::where('branch_id',null)
            ->where('start_date',$value)->get();
        return $start_date >= $value ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
