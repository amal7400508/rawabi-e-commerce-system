<?php

namespace App\Rules;

use App\Models\Branch;
use Illuminate\Contracts\Validation\Rule;

class CheckBranch implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute // name of checked data: 'branch_id'
     * @param  mixed  $value // sent by user: 2
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // check value if in branch
        if (!is_null(Branch::find($value))) {
            /*check if with provider*/

            // get provider id
            $provider_id = auth()->user()->id;
            // get branches ids array
            $branches_id = Branch::where('provider_id', $provider_id)->pluck('id')->toArray();
            // compare branches with value
            if (in_array($value, $branches_id)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The branch not exists.';
    }
}
