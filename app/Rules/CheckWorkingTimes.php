<?php

namespace App\Rules;

use App\Models\WorkingTime;
use Illuminate\Contracts\Validation\Rule;

class CheckWorkingTimes implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // check week days
        $times = WorkingTime::where('branch_id',null)
            ->where('week_day', $value)->get()->count();
        return $times > 0 ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The admin does not have exit error message.';
    }
}
