<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class CheckDays implements Rule
{
    protected $param;
    protected $message= 'error';

    /**
     * Create a new rule instance.
     *
     * @param $param array is the request->days
     * @return void
     */
    public function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $count_days = count($param ?? []);
        if ($count_days == 0){
            $this->message = __('admin.12');
            return false;
        }

        $days = ['FRIDAY','SATURDAY','SUNDAY','MONDAY','TUESDAY','WEDNESDAY','THURSDAY'];
        if(!in_array($value, $days)){
            $this->message = __('admin.8884848');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message();
    }
}
