<?php

namespace App\Rules;

use App\CategoryProduct;
use App\Product;
use Illuminate\Contracts\Validation\Rule;

class UniqueCategoryProduct implements Rule
{
    protected $param;
    protected $error_message;

    /**
     * Create a new rule instance.
     *
     * @param $param int is the category id
     */
    public function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value product id
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // check if product id is in products table with the same category id
        $product_exist = Product::where('id',$value)
                ->where('category_id', $this->param)
                ->get()->count() > 0;

        // store error message
        if ($product_exist) {
            $this->error_message = __('admin.this_product_is_classified_as_a_major_classification');
            return false;
        }

        // check if product if in category_product table with the same category id
        $category_product_exist = CategoryProduct::where('product_id', $value)
                ->where('category_id', $this->param)
                ->get()->count() > 0;

        // store error message
        if ($category_product_exist) {
            $this->error_message = __('admin.this_product_is_already_rated');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->error_message . '.';
    }
}
