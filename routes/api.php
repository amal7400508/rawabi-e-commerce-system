<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [UserController::class,'login']);
Route::group( //Utilities
    [
        // 'middleware' => ['auth:api'],
        'prefix' => 'admin'
    ], function () {

//        Route::post('logout', 'ReportsAPI\UserController@logout');

    Route::get('reports', [UserController::class,'reports']);

    // Used: number of users, carriers and requests
    Route::get('main-reports', [MainController::class,'mainReports']);

    // Used: all sells of all branches
    Route::get('branches-sells', [MainController::class,'sells']);

    // Used: all sells of all branches in period of date
    Route::get('branches-sells/{begin_date?}+{end_date?}', 'ReportsAPI\MainController@sells');

    // all sells of specified branch id
    Route::get('branch-sells/{branches_id}', 'ReportsAPI\MainController@BranchSells');

    // all sells of specified branch id in period of date
    Route::get('branch-sells/{branches_id}/{begin_date?}+{end_date?}', 'ReportsAPI\MainController@BranchSells');

    //Utilities: This APIs for general use if needed
    Route::get('get-branches/{index?}', 'ReportsAPI\MainController@getBranches');

    //all branches with request count
    Route::get('setBranchesWithRequest/', 'ReportsAPI\MainController@setBranchesWithRequest');

    // Used: get users statistics
    Route::get('users_statistics/', 'ReportsAPI\UsersStatisticsController@usersStatistics');

    // Used: get users statistics in period of date
    Route::get('users_statistics/{begin_date?}+{end_date?}', 'ReportsAPI\UsersStatisticsController@usersStatistics');

    // Used: get carriers statistics
    Route::get('carriers_statistics/', 'ReportsAPI\UsersStatisticsController@carriersStatistics');

    // Used: get carriers statistics in period of date
    Route::get('carriers_statistics/{begin_date?}+{end_date?}',
        'ReportsAPI\UsersStatisticsController@carriersStatistics');

    // Used: get statistics of carriers in the branches
    Route::get('branch-carriers/', 'ReportsAPI\UsersStatisticsController@branchCarriers');

    // Used: get statistics of carriers in the branches in period of date
    Route::get('branch-carriers/{begin_date?}+{end_date?}',
        'ReportsAPI\UsersStatisticsController@branchCarriers');

    // Used: get Most requested products
    Route::get('most-requested-products/',
        'ReportsAPI\UsersStatisticsController@mostRequestedProducts');

    // Used: get Most requested products in period of date
    Route::get('most-requested-products/{begin_date?}+{end_date?}',
        'ReportsAPI\UsersStatisticsController@mostRequestedProducts');

    // Used: get Most Buy products Branch
    Route::get('most-product-branch/', 'ReportsAPI\UsersStatisticsController@mostProductBranch');

    // Used: get Most Buy products Branch in period of date
    Route::get('most-product-branch/{begin_date?}+{end_date?}',
        'ReportsAPI\UsersStatisticsController@mostProductBranch');

    // Used: get Points of login and points of order
    Route::get('points-statistics/',
        'ReportsAPI\UsersStatisticsController@PointsStatistics');

    // Used: get Points of login and points of order in period of date
    Route::get('points-statistics/{begin_date?}+{end_date?}',
        'ReportsAPI\UsersStatisticsController@PointsStatistics');

    // Used:get how many buy product for category
    Route::get('buy-product-category/', 'ReportsAPI\UsersStatisticsController@buyProductCategory');

    // Used:get how many buy product for category in period of date
    Route::get('buy-product-category/{begin_date?}+{end_date?}',
        'ReportsAPI\UsersStatisticsController@buyProductCategory');

//        // Used: get Points of login and points of order
//        Route::get('points-statistics/',
//        'ReportsAPI\UsersStatisticsController@PointsStatistics');

    // Used: get requests count Statistics
    Route::get('requests-statistics/',
        'ReportsAPI\UsersStatisticsController@requestsStatistics');

    // Used: get requests count Statistics in period of date
    Route::get('requests-statistics/{begin_date?}+{end_date?}',
        'ReportsAPI\UsersStatisticsController@requestsStatistics');
}
);
