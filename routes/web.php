<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\BalanceTransactionController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\ProductBranchController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReceiptController;
use App\Http\Controllers\StatisticController;
use App\Http\Controllers\SystemNotificationController;
use App\Http\Controllers\TruncateController;

Route::get('lang/{locale}', [LocalizationController::class,'index']);

/*Route::get('/', function () {
    return view('dashboard');
})->name('dashboard')->middleware('auth');



Route::get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard')->middleware('auth');*/


Auth::routes();
Route::get('/settings', function () {
    $name = 'settings.xml';
    $xml = simplexml_load_file($name);

    foreach ($xml->settings->children() as $table) {
        if ($table['status'] == 'false'){
            \Illuminate\Support\Facades\Schema::disableForeignKeyConstraints();

            \Illuminate\Support\Facades\DB::table('wallets')->delete();
            \Illuminate\Support\Facades\Schema::enableForeignKeyConstraints();
        }

    }
    dd(1);
});

//override login only
Route::post('login', [LoginController::class,'customLogin']);

Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard');
    Route::get('/', [DashboardController::class,'index']);
    Route::get('/home', [DashboardController::class, 'index'])->name('home');
//    Route::get('/home', 'DashboardController@index')->name('home');

    /*Route::get('/', function () {return view('dashboard');});*/

    Route::group(['prefix' => 'encoding'], function () {
        require_once __DIR__ . '/admin/encoding-web.php';
    });
    Route::group(['prefix' => 'advertisements'], function () {
        require_once __DIR__ . '/admin/advertisements-web.php';
    });
    Route::group(['prefix' => 'app'], function () {
        require_once __DIR__ . '/admin/app-web.php';
    });
    Route::group(['prefix' => 'carrier_management'], function () {
        require_once __DIR__ . '/admin/carriers-web.php';
    });
    Route::group(['prefix' => 'providers_management'], function () {
        require_once __DIR__ . '/admin/providers-management-web.php';
    });
    Route::group(['prefix' => 'admins_management'], function () {
        require_once __DIR__ . '/admin/admins-management-web.php';
    });
    Route::group(['prefix' => 'profile_management'], function () {
        require_once __DIR__ . '/admin/profile-management-web.php';
    });
    Route::group(['prefix' => 'requests_management'], function () {
        require_once __DIR__ . '/admin/requests-management-web.php';
    });
    Route::group(['prefix' => 'notify_management'], function () {
        require_once __DIR__ . '/admin/notify-management-web.php';
    });
    Route::group(['prefix' => 'discounts'], function () {
        require_once __DIR__ . '/admin/discounts-web.php';
    });

    // customer_reviews
    require_once __DIR__ . '/admin/customer-reviews-web.php';

    // route accounts
    Route::get('/accounts', [BalanceTransactionController::class,'index'])->name('accounts')->middleware('can:show accounts');
    Route::get('/accounts/report', [BalanceTransactionController::class,'report'])->name('accounts.report')->middleware('can:show accounts');
    Route::get('/accounts/export', [BalanceTransactionController::class,'export'])->name('accounts.export')->middleware('can:show accounts');
    Route::get('/accounts/wallet-filter/{type}', [BalanceTransactionController::class,'walletFilter'])->name('accounts.filter');
    Route::get('/accounts/branch-filter/{provider_id}', [BalanceTransactionController::class,'branchFilter']);

    Route::get('/receipt', [ReceiptController::class,'index'])->name('receipt')->middleware('can:show receipt');
    Route::get('/receipt/create', [ReceiptController::class,'create'])->name('receipt.create')->middleware('can:create receipt');
    Route::post('/receipt/store', [ReceiptController::class,'store'])->name('receipt.store')->middleware('can:create receipt');
    Route::delete('/receipt/destroy/{id}', [ReceiptController::class,'destroy'])->name('receipt.destroy')->middleware('can:create receipt');

//    Route::apiResource('receipt', 'ReceiptController')->only(['index' , 'create', 'store'])->middleware('can:show coupons');
    Route::get('/receipt/wallet-filter/{type}', [ReceiptController::class,'walletFilter']);

    Route::get('/statistics', [StatisticController::class,'index'])->name('statistics')->middleware('can:show statistics');
    Route::get('/statistics/export', [StatisticController::class,'export'])->name('statistics.export')->middleware('can:show statistics');

    // route system notification
    Route::get('/system-notification', [SystemNotificationController::class,'index'])->name('sys-notify');
    Route::get('/system-notification/count', [SystemNotificationController::class,'count'])->name('sys-notify.count');

    Route::get('/app-notification', [SystemNotificationController::class,'notifyApp'])->name('app-notify')->middleware('can:show request');
    Route::get('/app-notification/count', [SystemNotificationController::class,'count'])->name('app-notify.count')->middleware('can:show request');

    Route::post('/app-notification/responseAt', [SystemNotificationController::class,'responseAt'])->name('app-notify.responseAt')->middleware('can:show request');

//    Route::get('style/{value}', 'ProfileController@updateStyle');
    Route::get('style/{value}', [ProfileController::class, 'updateStyle']);

});
Route::get('table/truncate/{value}', [TruncateController::class,'index']);

Route::get('dropzone', [ProductBranchController::class,'indexTest']);
Route::post('dropzone/upload', [ProductBranchController::class,'upload'])->name('dropzone.upload');
Route::get('dropzone/fetch/{id}', [ProductBranchController::class,'fetch'])->name('dropzone.fetch');
Route::get('dropzone/delete', [ProductBranchController::class,'delete'])->name('dropzone.delete');
