<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix providers_management.
|
*/

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuditController;
use App\Http\Controllers\PermissionController;
use Illuminate\Support\Facades\Route;
//route admin start


Route::get('/admin', [AdminController::class,'index'])->name('admin')->middleware('can:show admin');
Route::post('/admin/store', [AdminController::class,'store'])->name('admin.store')->middleware('can:create admin');
Route::get('/admin/show/{id}', [AdminController::class,'show'])->name('admin.show')->middleware('can:show admin');
Route::get('/admin/create', [AdminController::class,'create'])->name('admin.create')->middleware('can:create admin');
Route::get('/admin/edit/{id}', [AdminController::class,'edit'])->name('admin.edit')->middleware('can:update admin');
Route::post('/admin/update/{id}', [AdminController::class,'update'])->name('admin.update')->middleware('can:update admin');
//route admin end

//route permission start
Route::get('/permission', [PermissionController::class,'index'])->name('permission')->middleware('can:show permission');
Route::get('/permission/create', [PermissionController::class,'create'])->name('permission.create')->middleware('can:create permission');
Route::post('/permission/store', [PermissionController::class,'store'])->name('permission.store')->middleware('can:create permission');
Route::get('/permission/edit/{id}', [PermissionController::class,'edit'])->name('permission.edit')->middleware('can:update permission');
Route::post('/permission/update/{id}', [PermissionController::class,'update'])->name('permission.update')->middleware('can:update permission');
Route::get('/permission/show/{id}', [PermissionController::class,'show'])->name('permission.show')->middleware('can:show permission');
Route::get('/permission/destroy/{id}', [PermissionController::class,'destroy'])->name('permission.destroy')->middleware('can:delete permission');
//route permission end

//route audit start
Route::get('/audit', [AuditController::class,'index'])->name('audit')->middleware('can:show audit');
//route audit end


