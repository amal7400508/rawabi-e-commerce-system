<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix providers_management.
|
*/

use App\Http\Controllers\PrescriptionController;
use App\Http\Controllers\PrescriptionRepliesController;
use App\Http\Controllers\RequestController;
use App\Http\Controllers\SpecialProductController;
use Illuminate\Support\Facades\Route;

Route::get('/request', [RequestController::class,'index'])->name('request')->middleware('can:show request');
Route::get('/request/count', [RequestController::class,'count'])->name('status.count')->middleware('can:show request');

Route::get('/request/report', [RequestController::class,'report'])->name('request.report')->middleware('can:show request_report');
Route::get('/request/report_export', [RequestController::class,'reportExport'])->name('request.report_export')->middleware('can:show tracking');
Route::get('/request/export', [RequestController::class,'export'])->name('request.export')->middleware('can:show tracking');

Route::get('/request/show/{id}', [RequestController::class,'show'])->name('request.show')->middleware('can:show request');
Route::get('/request/edit/{id}', [RequestController::class,'edit'])->name('request.edit')->middleware('can:update request');
Route::post('/request/update/{id}', [RequestController::class,'update'])->name('request.update')->middleware('can:update request');
Route::get('/request/falter/{id}', [RequestController::class,'falter'])->name('request.falter');
Route::get('/request/falterStatus/{id}', [RequestController::class,'falterStatus']);
Route::get('/tracking', [RequestController::class,'trackingRequest'])->name('tracking')->middleware('can:show tracking');
Route::post('/request/edit-status/{id}', [RequestController::class,'editStatusAndCarrier'])->name('edit_status')->middleware('can:show tracking');
Route::post('/request/reviewed/{id}', [RequestController::class,'reviewed'])->name('request.reviewed')->middleware('can:show tracking');
Route::get('/requestFromBranch', [RequestController::class,'requestFromBranch'])->name('requestFromBranch')->middleware('can:show request');
Route::get('/request/fromBranch/showDetails/{id}', [RequestController::class,'showDetailsRequestFromBranch'])->name('showDetailsRequestFromBranch');
Route::post('/request/store/{id}', [RequestController::class,'store'])->name('request.store')->middleware('can:update request');

Route::delete('/request/cart-delete/{cart_detail_id}', [RequestController::class,'deleteFromCart'])->name('request.cart-delete')/*->middleware('can:update request')*/;
Route::get('request/product-price/{product_id}', [RequestController::class,'getProductPrice'])/*->middleware('can:update request')*/;
Route::post('request/add-to-cart', [RequestController::class,'addToCart'])->name('request.add-to-cart')/*->middleware('can:update request')*/;
Route::post('/request/change-quantity/{id}', [RequestController::class,'changeQuantity'])->name('request.change-quantity')->middleware('can:update request');

Route::get('request/audit/{id}', [RequestController::class,'Audit'])->name('request.audit')/*->middleware('can:show tracking')*/;
//    Route::get('/request/falterStatus/{id}', 'RequestController@falterStatus')->name('request.falterStatus');
//route requests start

//Route::get('/request/get-branch/{provider_id}', 'RequestController@getBranch')->name('request.get-branch');

//Route::get('/request/get-categories/{branch_id}', 'RequestController@getCategories')->name('request.get-categories');
//Route::get('/request/get-products/{category_id}', 'RequestController@getProducts')->name('request.get-products');

//route special_product
Route::get('/special_product', [SpecialProductController::class,'index'])->name('special_product')->middleware('can:show special_product');
Route::get('/special_product/deferred', [SpecialProductController::class,'deferred'])->name('special_product.deferred')->middleware('can:show deferred');
Route::post('/special_product/completedRequest/{id}', [SpecialProductController::class,'completedRequest'])->name('special_product.completedRequest')->middleware('can:update special_product');
Route::get('/special_product/show/{id}', [SpecialProductController::class,'show'])->name('special_product.show')->middleware('can:show special_product');
Route::get('/special_product/edit/{id}', [SpecialProductController::class,'edit'])->name('special_product.edit')->middleware('can:update special_product');
Route::post('/special_product/update/{id}', [SpecialProductController::class,'update'])->name('special_product.update')->middleware('can:update special_product');
//route special_product

//route prescription
Route::get('/prescription', [PrescriptionController::class,'index'])->name('prescription')->middleware('can:show prescription');
Route::get('/prescription/deferred', [PrescriptionController::class,'deferred'])->name('prescription.deferred')->middleware('can:show prescription');
Route::post('/prescription/completedRequest/{id}', [PrescriptionController::class,'completedRequest'])->name('prescription.completedRequest');
Route::get('/prescription/show/{id}', [PrescriptionController::class,'show'])->name('prescription.show')->middleware('can:show prescription');
Route::get('/prescription/edit/{id}', [PrescriptionController::class,'edit'])->name('prescription.edit')->middleware('can:update prescription');
Route::post('/prescription/update/{id}', [PrescriptionController::class,'update'])->name('prescription.update')->middleware('can:update prescription');
Route::get('/prescription/delete/{id}', [PrescriptionController::class,'delete'])->name('prescription.delete')->middleware('can:delete prescription');
Route::post('/prescription/store', [PrescriptionController::class,'store'])->name('prescription.store')->middleware('can:update prescription');

//route prescription

//route prescription_replies
Route::get('/prescription-replies', [PrescriptionRepliesController::class,'index'])->name('prescription-replies')->middleware('can:show prescription-replies');
Route::get('/prescription_replies/deferred', [PrescriptionRepliesController::class,'deferred'])->name('prescription_replies.deferred')->middleware('can:show prescription-replies');
Route::post('/prescription_replies/completedRequest/{id}', [PrescriptionRepliesController::class,'completedRequest'])->name('prescription_replies.completedRequest')->middleware('can:show prescription-replies');
Route::get('/prescription_replies/show/{id}', [PrescriptionRepliesController::class,'show'])->name('prescription_replies.show')->middleware('can:show prescription-replies');
Route::get('/prescription_replies/edit/{id}', [PrescriptionRepliesController::class,'edit'])->name('prescription_replies.edit')->middleware('can:update prescription-replies');
Route::post('/prescription_replies/update/{id}', [PrescriptionRepliesController::class,'update'])->name('prescription_replies.update')->middleware('can:update prescription-replies');
Route::post('/prescription_replies/store', [PrescriptionRepliesController::class,'store'])->name('prescription_replies.store')->middleware('can:update prescription-replies');
//route prescription_replies
