<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix app.
|
*/


//route users start
use App\Http\Controllers\AddController;
use App\Http\Controllers\AttentionController;
use App\Http\Controllers\DepositController;
use App\Http\Controllers\Event_typeController;
use App\Http\Controllers\NewProductController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\OpeningBalancesController;
use App\Http\Controllers\ProductBranchController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductPriceController;
use App\Http\Controllers\UserAddressController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserEventController;

Route::get('/users', [UserController::class,'index'])->name('users')->middleware('can:show users');
Route::post('/users/deposit/{user_id}', [UserController::class,'deposit'])->middleware('can:create deposits');
Route::post('/users/update/{user_id}', [UserController::class,'update'])->middleware('can:update users');
Route::get('/users/edit/{id}', [UserController::class,'edit'])->middleware('can:update users');
Route::get('/users/show/{id}', [UserController::class,'show'])->middleware('can:show users');

Route::get('/users/report', [UserController::class,'report'])->name('users.report')->middleware('can:show users_report');
Route::get('/users/report_export', [UserController::class,'reportExport'])->name('users.report_export')->middleware('can:show users');
Route::get('/users/export-most-request', [UserController::class,'exportMostRequest'])->name('users.export-most-request')->middleware('can:show users');

Route::get('/users/export', [UserController::class,'export'])->name('users.export')->middleware('can:show users');
//route users end

//route deposits start
Route::get('/deposits', [DepositController::class,'index'])->name('deposits')->middleware('can:show deposits');
Route::get('/deposits/branches', [DepositController::class,'depositsViaBranch'])->name('deposit_branches')->middleware('can:show deposits');
Route::get('/deposits/show/{id}', [DepositController::class,'show'])->middleware('can:show deposits');
Route::post('/deposits/accept', [DepositController::class,'accept'])->name('deposits.accept')->middleware('can:update deposits');
Route::post('/deposits/reject', [DepositController::class,'reject'])->name('deposits.reject')->middleware('can:update deposits');
//route deposits end

//route user_addresses start
Route::get('/user_addresses', [UserAddressController::class,'index'])->name('user_addresses')->middleware('can:show user_addresses');
Route::get('/user_addresses/show/{id}', [UserAddressController::class,'show'])->middleware('can:show user_addresses');
//route user_addresses end

//route products start
Route::get('/products', [ProductController::class,'index'])->name('products')->middleware('can:show products');
Route::get('/products/show/{id}', [ProductController::class,'show'])->middleware('can:show products');
Route::post('/products/accept', [ProductController::class,'accept'])->name('products.accept')->middleware('can:update products');
Route::post('/products/accept-all', [ProductController::class,'acceptAll'])->name('products.accept-all')->middleware('can:update products');
Route::post('/products/reject-all', [ProductController::class,'rejectAll'])->name('products.reject-all')->middleware('can:update products');
Route::post('/products/reject', [ProductController::class,'reject'])->name('products.reject')->middleware('can:update products');
Route::post('/products/update/{id}', [ProductController::class,'update'])->name('products.update')->middleware('can:update products');

Route::get('/products/report', [ProductController::class,'report'])->name('products.report')->middleware('can:show products_report');
Route::get('/products/report_export', [ProductController::class,'reportExport'])->name('products.report_export')->middleware('can:show products');
Route::get('/products/export', [ProductController::class,'export'])->name('products.export')->middleware('can:show products');

Route::get('/products/get-branch/{provider_id}', [ProductController::class,'getBranch'])->name('products.get-branch');
Route::get('/products/get-categories/{branch_id}', [ProductController::class,'getCategories'])->name('products.get-categories');
Route::get('/products/get-products/{category_id}', [ProductController::class,'getProducts'])->name('products.get-products');

Route::post('/products/change-level/{id}', [ProductController::class,'changeLevel'])->name('products.change-level')->middleware('can:update products');
//route products end


//route newproducts start
Route::get('/newproducts', [NewProductController::class,'index'])->name('newproducts');
Route::get('/newproducts/show/{id}', [NewProductController::class,'show']);
Route::post('/newproducts/accept', [NewProductController::class,'accept'])->name('newproducts.accept');
Route::post('/newproducts/accept-all', [NewProductController::class,'acceptAll'])->name('newproducts.accept-all');
Route::post('/newproducts/reject-all', [NewProductController::class,'rejectAll'])->name('newproducts.reject-all');
Route::post('/newproducts/reject', [NewProductController::class,'reject'])->name('newproducts.reject');
Route::post('/newproducts/update/{id}', [NewProductController::class,'update'])->name('newproducts.update');

Route::get('/newproducts/report', [NewProductController::class,'report'])->name('newproducts.report');
Route::get('/newproducts/report_export', [NewProductController::class,'reportExport'])->name('newproducts.report_export');
Route::get('/newproducts/export', [NewProductController::class,'export'])->name('newproducts.export');

Route::get('/newproducts/get-branch/{provider_id}', [NewProductController::class,'getBranch'])->name('newproducts.get-branch');
Route::get('/newproducts/get-categories/{branch_id}', [NewProductController::class,'getCategories'])->name('newproducts.get-categories');
Route::get('/newproducts/get-newproducts/{category_id}', [NewProductController::class,'getProducts'])->name('newproducts.get-newproducts');

Route::post('/newproducts/change-level/{id}', [NewProductController::class,'changeLevel'])->name('newproducts.change-level');
//route newproducts end

//route product start
Route::get('/product', [ProductBranchController::class,'index'])->name('product')->middleware('can:show product');
Route::get('/product/report', [ProductBranchController::class,'report'])->name('product.report')->middleware('can:show product');
Route::get('/product/export', [ProductBranchController::class,'export'])->name('product.export')->middleware('can:show product');
Route::post('/product/store', [ProductBranchController::class,'store'])->name('product.store')->middleware('can:create product');

Route::post('/product/add-from-excel', [ProductBranchController::class,'addFromExcel'])->name('product.add-from-excel')->middleware('can:create product');
Route::get('/product/exportExcelFormProducts', [ProductBranchController::class,'exportExcelFormProducts'])->name('product.exportExcelFormProducts')->middleware('can:create product');
Route::get('/product/exportExcelFormProductPrices', [ProductBranchController::class,'exportExcelFormProductPrices'])->name('product.exportExcelFormProductPrices')->middleware('can:create product');

Route::get('/product/destroy/{id}', [ProductBranchController::class,'destroy'])->name('product.destroy')->middleware('can:delete product');
Route::get('/product/show/{id}', [ProductBranchController::class,'show'])->name('product.show')->middleware('can:show product');
Route::get('/product/recycle_bin', [ProductBranchController::class,'recycle_bin'])->name('product.recycle_bin')->middleware('can:delete product');
Route::get('/product/create', [ProductBranchController::class,'create'])->name('product.create')->middleware('can:create product');
Route::get('/product/restore/{id}', [ProductBranchController::class,'restore'])->name('product.restore')->middleware('can:delete product');
Route::get('/product/hdelete/{id}', [ProductBranchController::class,'hdelete'])->name('product.hdelete')->middleware('can:delete product');
Route::get('/product/edit/{id}', [ProductBranchController::class,'edit'])->name('product.edit')->middleware('can:update product');
Route::post('/product/update/{product}', [ProductBranchController::class,'update'])->name('product.update')->middleware('can:update product');
Route::get('/product/falter/{id}', [ProductBranchController::class,'falter'])->name('product.falter');
Route::get('/product/falter_main_category/{id}', [ProductBranchController::class,'falterMainCategory'])->name('product.falter_main_category');

Route::delete('/product/destroy-all', [ProductBranchController::class,'destroyAll'])->name('product.destroy-all')->middleware('can:delete product');

//route product end

//route delivery_pricing start
Route::post('/product_price/store', [ProductPriceController::class,'store'])->name('product_price.store')/*->middleware('can:create product_price')*/;
Route::get('/product_price/delete/{id}', [ProductPriceController::class,'delete'])->name('product_price.delete')/*->middleware('can:delete product_price')*/;
Route::get('/product_price/edit/{id}', [ProductPriceController::class,'edit'])->name('product_price.edit')/*->middleware('can:update product_price')*/;
//route delivery_pricing end

//route addition start
Route::post('/add/store', [AddController::class,'store'])->name('add.store')/*->middleware('can:create product_price')*/;
Route::get('/add/delete/{id}', [AddController::class,'delete'])->name('add.delete')/*->middleware('can:delete product_price')*/;
Route::get('/add/edit/{id}', [AddController::class,'edit'])->name('add.edit')/*->middleware('can:update product_price')*/;
//route addition end


//route attentions start
Route::get('/attentions', [AttentionController::class,'index'])->name('attentions')->middleware('can:show attentions');
Route::get('/attentions/attentions_branches', [AttentionController::class,'attentionsBranches'])->name('attentions_branches')->middleware('can:show attentions');
Route::get('/attentions/show/{id}', [AttentionController::class,'show'])->middleware('can:show attentions');
Route::get('/attentions/edit/{id}', [AttentionController::class,'edit'])->middleware('can:update attentions');
Route::post('/attentions/accept', [AttentionController::class,'accept'])->name('attentions.accept')->middleware('can:update attentions');
Route::post('/attentions/reject', [AttentionController::class,'reject'])->name('attentions.reject')->middleware('can:update attentions');

Route::post('/attentions/store', [AttentionController::class,'store'])->name('attentions.store')->middleware('can:create attentions');
Route::post('/attentions/update/{id}', [AttentionController::class,'update'])->name('attentions.update')->middleware('can:update attentions');
Route::delete('/attentions/destroy/{id}', [AttentionController::class,'destroy'])->name('attentions.destroy')->middleware('can:delete attentions');
//route attentions end


//route offers start
Route::get('/offers', [OfferController::class,'index'])->name('offers')->middleware('can:show offer');
Route::post('/offers/store', [OfferController::class,'store'])->name('offers.store')->middleware('can:create offer');
Route::delete('/offers/destroy/{id}', [OfferController::class,'destroy'])->name('offers.destroy')->middleware('can:delete offer');
Route::get('/offers/offers_branches', [OfferController::class,'offersBranches'])->name('offers_branches')->middleware('can:show offer');
Route::get('/offers/show/{id}', [OfferController::class,'show'])->middleware('can:show offer');
Route::get('/offers/edit/{id}', [OfferController::class,'edit'])->middleware('can:update offer');
Route::post('/offers/accept', [OfferController::class,'accept'])->name('offers.accept')->middleware('can:update offer');
Route::post('/offers/reject', [OfferController::class,'reject'])->name('offers.reject')->middleware('can:update offer');

Route::post('/offers/delete-detail/store', [OfferController::class,'deleteDetailStore'])->name('offers.delete-detail.store')->middleware('can:create offer');

Route::post('/offers/update/{id}', [OfferController::class,'update'])->name('offers.update')->middleware('can:update offer');
Route::delete('/offers/delete-detail-offer/{id}', [OfferController::class,'deleteDetailOffer'])->name('offers.delete-detail-offer')->middleware('can:delete offer');

Route::get('/offers/falter-product/{id}', [OfferController::class,'falterProduct'])->name('offers.falter-product');
Route::get('/offers/falter-product-discount/{id}/{branch_id}', [OfferController::class,'falterProductDiscount'])->name('offers.falter-product-discount');
Route::get('/offers/falter-product-request/{id}', [OfferController::class,'falterProductRequest'])->name('offers.falter-product-request');
Route::get('/offers/falter-price/{id}', [OfferController::class,'falterPrice'])->name('offer.faltser-price');
Route::get('offers/product-price/{product_id}', [OfferController::class,'getProductPrice']);
Route::get('/offers/falter_main_category/{id}', [OfferController::class,'falterMainCategory'])->name('offers.falter_main_category');
//route offers end



//route Stocks start
Route::get('/Stocks', [OpeningBalancesController::class,'index'])->name('Stocks')->middleware('can:show stocks');
Route::post('/Stocks/store', [OpeningBalancesController::class,'store'])->name('Stocks.store')->middleware('can:create stocks');
Route::get('/Stocks/show/{id}', [OpeningBalancesController::class,'show'])->name('Stocks.show')->middleware('can:show stocks');
Route::get('/Stocks/edit/{id}', [OpeningBalancesController::class,'edit'])->name('Stocks.edit')->middleware('can:update stocks');
Route::post('/Stocks/update/{id}', [OpeningBalancesController::class,'update'])->name('Stocks.update')->middleware('can:update stocks');
Route::delete('/Stocks/destroy/{id}', [OpeningBalancesController::class,'destroy'])->name('Stocks.destroy')->middleware('can:delete stocks');
Route::get('/Stocks/falter_product/{id}', [OpeningBalancesController::class,'falterProduct'])->name('Stocks.falter_product');
//route Stocks end

//route user_event start
Route::get('/user_event', [UserEventController::class,'index'])->name('user_event')->middleware('can:show user_event');
Route::get('/user_event/show/{id}', [UserEventController::class,'show'])->name('user_event.show')->middleware('can:show user_event');
//route user_event end

//route event type start
Route::get('/event_type', [Event_typeController::class,'index'])->name('event_type')->middleware('can:show event_type');
Route::post('/event_type/store', [Event_typeController::class,'store'])->name('event_type.store')->middleware('can:create event_type');
Route::get('/event_type/destroy/{id}', [Event_typeController::class,'destroy'])->name('event_type.destroy')->middleware('can:delete event_type');
Route::get('/event_type/show/{id}', [Event_typeController::class,'show'])->name('event_type.show')->middleware('can:show event_type');
Route::get('/event_type/recycle_bin', [Event_typeController::class,'recycle_bin'])->name('event_type.recycle_bin')->middleware('can:delete event_type');
Route::get('/event_type/restore/{id}', [Event_typeController::class,'restore'])->name('event_type.restore')->middleware('can:delete event_type');;
Route::get('/event_type/hdelete/{id}', [Event_typeController::class,'hdelete'])->name('event_type.hdelete')->middleware('can:delete event_type');
Route::get('/event_type/edit/{id}', [Event_typeController::class,'edit'])->name('event_type.edit')->middleware('can:update event_type');
Route::get('/event_type/dele/{id}', [Event_typeController::class,'dele'])->name('event_type.dele')->middleware('can:delete event_type');
Route::post('/event_type/update/{id}', [Event_typeController::class,'update'])->name('event_type.update')->middleware('can:update event_type');
//route event type end
