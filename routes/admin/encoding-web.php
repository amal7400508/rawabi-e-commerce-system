<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix encoding.
|
*/


use App\Http\Controllers\AdvertisementTypeController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\BranchesCategoryController;
use App\Http\Controllers\BranchServiceController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CeilingController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\MainCategoryCategoryController;
use App\Http\Controllers\MainCategoryController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\NewAdvertisementController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\OfferTypeController;
use App\Http\Controllers\OpeningBalancesController;
use App\Http\Controllers\PharmacyCategoryController;
use App\Http\Controllers\SocialMediaController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\UnitTypeController;
use App\Http\Controllers\WorkingTimeBranchController;
use App\Http\Controllers\WorkingTimeController;

Route::get('main-categories', [MainCategoryController::class,'index'])->name('encoding.main-categories')->middleware('can:show main_categories');
Route::get('main-categories/tree-building', [MainCategoryController::class,'treeBuilding'])->name('encoding.main-categories.treeBuilding')->middleware('can:show main_categories');
Route::post('main-categories/store', [MainCategoryController::class,'store'])->name('encoding.main-categories.store')->middleware('can:create main_categories');
Route::post('main-categories/update', [MainCategoryController::class,'update'])->name('encoding.main-categories.update')->middleware('can:update main_categories');
Route::get('main-categories/edit/{id}', [MainCategoryController::class,'edit'])->name('encoding.main-categories.edit')->middleware('can:update main_categories');
Route::delete('main-categories/destroy', [MainCategoryController::class,'destroy'])->name('encoding.main-categories.destroy')->middleware('can:delete main_categories');

//route working_times start
Route::get('working_times', [WorkingTimeController::class,'index'])->name('working_times')->middleware('can:show working_times');
Route::get('working_times/details/{day}', [WorkingTimeController::class,'details'])->name('working_times.details')->middleware('can:show working_times');
Route::get('working_times/details_branch/{day}/{branch_id}', [WorkingTimeBranchController::class,'detailsBranch'])->name('working_times.details_branch')->middleware('can:show working_times');
Route::post('working_times/store', [WorkingTimeController::class,'store'])->name('working_times.store')->middleware('can:create working_times');
Route::get('working_times/edit/{id}', [WorkingTimeController::class,'edit'])->name('working_times.edit')->middleware('can:update working_times');
Route::post('working_times/update/{id}', [WorkingTimeController::class,'update'])->name('working_times.update')->middleware('can:update working_times');
Route::delete('working_times/destroy/{id}', [WorkingTimeController::class,'destroy'])->name('working_times.destroy')->middleware('can:delete working_times');
//route working_times end

//route working_times_branch start
Route::get('working_times_branch', [WorkingTimeBranchController::class,'index'])->name('working_times_branch')->middleware('can:show working_times_branch');
Route::get('working_times_branch/details/{day}', [WorkingTimeBranchController::class,'details'])->name('working_times_branch.details')->middleware('can:show working_times_branch');
Route::post('working_times_branch/store', [WorkingTimeBranchController::class,'store'])->name('working_times_branch.store')->middleware('can:create working_times_branch');
Route::get('working_times_branch/edit/{id}', [WorkingTimeBranchController::class,'edit'])->name('working_times_branch.edit')->middleware('can:update working_times_branch');
Route::post('working_times_branch/update/{id}', [WorkingTimeBranchController::class,'update'])->name('working_times_branch.update')->middleware('can:update working_times_branch');
Route::delete('working_times_branch/destroy/{id}', [WorkingTimeBranchController::class,'destroy'])->name('working_times_branch.destroy')->middleware('can:delete working_times_branch');
Route::delete('/working_times_branch/destroy-all', [WorkingTimeBranchController::class,'destroyAll'])->name('working_times_branch.destroy-all')->middleware('can:delete working_times_branch');
//route working_times_branch end

//route categories start
Route::get('/categories', [CategoryController::class,'index'])->name('categories')->middleware('can:show categories');
Route::post('/categories/store', [CategoryController::class,'store'])->name('categories.store')->middleware('can:create categories');
Route::get('/categories/show/{id}', [CategoryController::class,'show'])->name('categories.show')->middleware('can:show categories');
Route::get('/categories/edit/{id}', [CategoryController::class,'edit'])->name('categories.edit')->middleware('can:update categories');
Route::post('/categories/update/{id}', [CategoryController::class,'update'])->name('categories.update')->middleware('can:update categories');
Route::delete('/categories/destroy/{id}', [CategoryController::class,'destroy'])->name('categories.destroy')->middleware('can:delete categories');

Route::post('/categories/change-level/{id}', [CategoryController::class,'changeLevel'])->name('categories.change-level')->middleware('can:update categories');
//route categories end

//route areas start
Route::get('/areas', [AreaController::class,'index'])->name('areas')->middleware('can:show areas');
Route::post('/areas/store', [AreaController::class,'store'])->name('areas.store')->middleware('can:create areas');
Route::get('/areas/show/{id}', [AreaController::class,'show'])->name('areas.show')->middleware('can:show areas');
Route::get('/areas/edit/{id}', [AreaController::class,'edit'])->name('areas.edit')->middleware('can:update areas');
Route::post('/areas/update/{id}', [AreaController::class,'update'])->name('areas.update')->middleware('can:update areas');
Route::delete('/areas/destroy/{id}', [AreaController::class,'destroy'])->name('areas.destroy')->middleware('can:delete areas');
//route areas end

//route service_types start
//Route::get('/service_types', 'ServiceTypeController@index')->name('service_types');
//Route::post('/service_types/store', 'ServiceTypeController@store')->name('service_types.store');
//Route::get('/service_types/show/{id}', 'ServiceTypeController@show')->name('service_types.show');
//Route::get('/service_types/edit/{id}', 'ServiceTypeController@edit')->name('service_types.edit');
//Route::post('/service_types/update/{id}', 'ServiceTypeController@update')->name('service_types.update');
//Route::delete('/service_types/destroy/{id}', 'ServiceTypeController@destroy')->name('service_types.destroy');
//route service_types end

//route unit_types start
Route::get('/unit_types', [UnitTypeController::class,'index'])->name('unit_types')->middleware('can:show unit_types');
Route::post('/unit_types/store', [UnitTypeController::class,'store'])->name('unit_types.store')->middleware('can:create unit_types');
Route::get('/unit_types/show/{id}', [UnitTypeController::class,'show'])->name('unit_types.show')->middleware('can:show unit_types');
Route::get('/unit_types/edit/{id}', [UnitTypeController::class,'edit'])->name('unit_types.edit')->middleware('can:update unit_types');
Route::post('/unit_types/update/{id}', [UnitTypeController::class,'update'])->name('unit_types.update')->middleware('can:update unit_types');
Route::delete('/unit_types/destroy/{id}', [UnitTypeController::class,'destroy'])->name('unit_types.destroy')->middleware('can:delete unit_types');
//route unit_types end

//route units start
Route::get('/units', [UnitController::class,'index'])->name('units')->middleware('can:show units');
Route::post('/units/store', [UnitController::class,'store'])->name('units.store')->middleware('can:create units');
Route::get('/units/show/{id}', [UnitController::class,'show'])->name('units.show')->middleware('can:show units');
Route::get('/units/edit/{id}', [UnitController::class,'edit'])->name('units.edit')->middleware('can:update units');
Route::post('/units/update/{id}', [UnitController::class,'update'])->name('units.update')->middleware('can:update units');
Route::delete('/units/destroy/{id}', [UnitController::class,'destroy'])->name('units.destroy')->middleware('can:delete units');
//route units end

//route advertisement_types start
Route::get('/advertisement_types', [AdvertisementTypeController::class,'index'])->name('advertisement_types')->middleware('can:show advertisement_types');
Route::post('/advertisement_types/store', [AdvertisementTypeController::class,'store'])->name('advertisement_types.store')->middleware('can:create advertisement_types');
Route::get('/advertisement_types/show/{id}', [AdvertisementTypeController::class,'show'])->name('advertisement_types.show')->middleware('can:show advertisement_types');
Route::get('/advertisement_types/edit/{id}', [AdvertisementTypeController::class,'edit'])->name('advertisement_types.edit')->middleware('can:update advertisement_types');
Route::post('/advertisement_types/update/{id}', [AdvertisementTypeController::class,'update'])->name('advertisement_types.update')->middleware('can:update advertisement_types');
Route::delete('/advertisement_types/destroy/{id}', [AdvertisementTypeController::class,'destroy'])->name('advertisement_types.destroy')->middleware('can:delete advertisement_types');
//route advertisement_types end

//route offer_types start
Route::get('/offer_types', [OfferTypeController::class,'index'])->name('offer_types')->middleware('can:show offer_types');
Route::post('/offer_types/store', [OfferTypeController::class,'store'])->name('offer_types.store')->middleware('can:create offer_types');
Route::get('/offer_types/show/{id}', [OfferTypeController::class,'show'])->name('offer_types.show')->middleware('can:show offer_types');
Route::get('/offer_types/edit/{id}', [OfferTypeController::class,'edit'])->name('offer_types.edit')->middleware('can:update offer_types');
Route::post('/offer_types/update/{id}', [OfferTypeController::class,'update'])->name('offer_types.update')->middleware('can:update offer_types');
Route::delete('/offer_types/destroy/{id}', [OfferTypeController::class,'destroy'])->name('offer_types.destroy')->middleware('can:delete offer_types');
//route offer_types end

//route notes start
Route::get('/notes', [NoteController::class,'index'])->name('notes')->middleware('can:show notes');
Route::post('/notes/store', [NoteController::class,'store'])->name('notes.store')->middleware('can:create notes');
Route::get('/notes/show/{id}', [NoteController::class,'show'])->name('notes.show')->middleware('can:show notes');
Route::get('/notes/edit/{id}', [NoteController::class,'edit'])->name('notes.edit')->middleware('can:update notes');
Route::post('/notes/update/{id}', [NoteController::class,'update'])->name('notes.update')->middleware('can:update notes');
Route::delete('/notes/destroy/{id}', [NoteController::class,'destroy'])->name('notes.destroy')->middleware('can:delete notes');
//route notes end

//route branches_categories start
Route::get('/branches_categories', [BranchesCategoryController::class,'index'])->name('branches_categories')->middleware('can:show pharmacy_categories');
Route::get('/branches_categories/get-branches/{category_id}', [BranchesCategoryController::class,'getBranches'])->middleware('can:show pharmacy_categories');
Route::post('/branches_categories/store', [BranchesCategoryController::class,'store'])->name('branches_categories.store')->middleware('can:create pharmacy_categories');
Route::delete('/branches_categories/destroy/{id}', [BranchesCategoryController::class,'destroy'])->name('branches_categories.destroy')->middleware('can:delete pharmacy_categories');
//route branches_categories end

//route main_category_categories start
Route::get('/main_category_categories', [MainCategoryCategoryController::class,'index'])->name('main_category_categories')->middleware('can:show main_category_categories');
Route::get('/main_category_categories/get-category/{category_id}', [MainCategoryCategoryController::class,'getCategories'])->middleware('can:show main_category_categories');
Route::post('/main_category_categories/store', [MainCategoryCategoryController::class,'store'])->name('main_category_categories.store')->middleware('can:create main_category_categories');
Route::delete('/main_category_categories/destroy/{id}', [MainCategoryCategoryController::class,'destroy'])->name('main_category_categories.destroy')->middleware('can:delete main_category_categories');
//route main_category_categories end

//route pharmacy_categories start
Route::get('/pharmacy_categories', [PharmacyCategoryController::class,'index'])->name('pharmacy_categories')->middleware('can:show pharmacy_categories');
Route::get('/pharmacy_categories/get/{category_id}', [PharmacyCategoryController::class,'get'])->middleware('can:show pharmacy_categories');
Route::post('/pharmacy_categories/store', [PharmacyCategoryController::class,'store'])->name('pharmacy_categories.store')->middleware('can:create pharmacy_categories');
Route::delete('/pharmacy_categories/destroy/{id}', [PharmacyCategoryController::class,'destroy'])->name('pharmacy_categories.destroy')->middleware('can:delete pharmacy_categories');
Route::get('/pharmacy_categories/get-sub-categories/{category_id}', [PharmacyCategoryController::class,'getSubCategories'])->middleware('can:show pharmacy_categories');
//route pharmacy_categories end

//route commissions start
Route::get('/commissions', [CommissionController::class,'index'])->name('commissions')->middleware('can:show commissions');
Route::get('/commissions/get/{category_id}', [CommissionController::class,'get'])->middleware('can:show commissions');
Route::post('/commissions/store', [CommissionController::class,'store'])->name('commissions.store')->middleware('can:create commissions');
Route::delete('/commissions/destroy/{id}', [CommissionController::class,'destroy'])->name('commissions.destroy')->middleware('can:delete commissions');
Route::get('/commissions/get-sub-categories/{category_id}', [CommissionController::class,'getSubCategories'])->middleware('can:show pharmacy_categories');
Route::post('/commissions/change-level/{id}', [CommissionController::class,'changeLevel'])->name('commissions.change-level')->middleware('can:create commissions');

//route pharmacy_categories end

//route branch_services start
Route::get('/branch_services', [BranchServiceController::class,'index'])->name('branch_services')->middleware('can:show branch_services');
Route::get('/branch_services/get/{category_id}', [BranchServiceController::class,'get'])->middleware('can:show branch_services');
Route::post('/branch_services/store', [BranchServiceController::class,'store'])->name('branch_services.store')->middleware('can:create branch_services');
Route::delete('/branch_services/destroy/{id}', [BranchServiceController::class,'destroy'])->name('branch_services.destroy')->middleware('can:delete branch_services');
//route branch_services end

//route social_medias start
Route::get('/social_medias', [SocialMediaController::class,'index'])->name('social_medias')->middleware('can:show social_medias');
Route::post('/social_medias/store', [SocialMediaController::class,'store'])->name('social_medias.store')->middleware('can:create social_medias');
Route::get('/social_medias/show/{id}', [SocialMediaController::class,'show'])->name('social_medias.show')->middleware('can:show social_medias');
Route::get('/social_medias/edit/{id}', [SocialMediaController::class,'edit'])->name('social_medias.edit')->middleware('can:update social_medias');
Route::post('/social_medias/update/{id}', [SocialMediaController::class,'update'])->name('social_medias.update')->middleware('can:update social_medias');
Route::delete('/social_medias/destroy/{id}', [SocialMediaController::class,'destroy'])->name('social_medias.destroy')->middleware('can:delete social_medias');
//route social_medias end

//route ceiling start
Route::get('/ceiling', [CeilingController::class,'index'])->name('ceiling')->middleware('can:show ceiling');
Route::post('/ceiling/store', [CeilingController::class,'store'])->name('ceiling.store')->middleware('can:create ceiling');
Route::get('/ceiling/show/{id}', [CeilingController::class,'show'])->name('ceiling.show')->middleware('can:show ceiling');
Route::get('/ceiling/create', [CeilingController::class,'create'])->name('ceiling.create')->middleware('can:create ceiling');
Route::get('/ceiling/edit/{id}', [CeilingController::class,'edit'])->name('ceiling.edit')->middleware('can:update ceiling');
Route::post('/ceiling/update/{id}', [CeilingController::class,'update'])->name('ceiling.update')->middleware('can:update ceiling');
Route::delete('/ceiling/destroy/{id}', [CeilingController::class,'destroy'])->name('ceiling.destroy')->middleware('can:delete ceiling');
//route ceiling start


//route color start
Route::get('/color', [ColorController::class,'index'])->name('color')->middleware('can:show color');
Route::post('/color/store', [ColorController::class,'store'])->name('color.store')->middleware('can:create color');
Route::get('/color/show/{id}', [ColorController::class,'show'])->name('color.show')->middleware('can:show color');
Route::get('/color/create', [ColorController::class,'create'])->name('color.create')->middleware('can:create color');
Route::get('/color/edit/{id}', [ColorController::class,'edit'])->name('color.edit')->middleware('can:update color');
Route::post('/color/update/{id}', [ColorController::class,'update'])->name('color.update')->middleware('can:update color');
Route::delete('/color/destroy/{id}', [ColorController::class,'destroy'])->name('color.destroy')->middleware('can:delete color');
//route color end

//route new_advertisement start
Route::get('/new_advertisement', [NewAdvertisementController::class,'index'])->name('encoding.new_advertisement');
Route::post('/new_advertisement/store', [NewAdvertisementController::class,'store'])->name('encoding.new_advertisement.store');
Route::delete('/new_advertisement/destroy/{id}', [NewAdvertisementController::class,'destroy'])->name('encoding.new_advertisement.destroy');
Route::get('/new_advertisement/show/{id}', [NewAdvertisementController::class,'show'])->name('encoding.new_advertisement.show');
Route::get('/new_advertisement/recycle_bin', [NewAdvertisementController::class,'recycle_bin'])->name('encoding.new_advertisement.recycle_bin');
Route::get('/new_advertisement/restore/{id}', [NewAdvertisementController::class,'restore'])->name('encoding.new_advertisement.restore');
Route::get('/new_advertisement/hdelete/{id}', [NewAdvertisementController::class,'hdelete'])->name('encoding.new_advertisement.hdelete');
Route::get('/new_advertisement/edit/{id}', [NewAdvertisementController::class,'edit'])->name('encoding.new_advertisement.edit');
Route::post('/new_advertisement/update/{id}', [NewAdvertisementController::class,'update'])->name('encoding.new_advertisement.update');
Route::get('/new_advertisement/falterProduct/{id}', [NewAdvertisementController::class,'falterProduct'])->name('encoding.new_advertisement.falterProduct');
//route new_advertisement end


//route material start
Route::get('/material', [MaterialController::class,'index'])->name('material')->middleware('can:show material');
Route::post('/material/store', [MaterialController::class,'store'])->name('material.store')->middleware('can:create material');
Route::get('/material/show/{id}', [MaterialController::class,'show'])->name('material.show')->middleware('can:show material');
Route::get('/material/edit/{id}', [MaterialController::class,'edit'])->name('material.edit')->middleware('can:update material');
Route::post('/material/update/{id}', [MaterialController::class,'update'])->name('material.update')->middleware('can:update material');
Route::delete('/material/destroy/{id}', [MaterialController::class,'destroy'])->name('material.destroy')->middleware('can:delete material');
//route material end
