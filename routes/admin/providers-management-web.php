<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix providers_management.
|
*/

//route providers start
use App\Http\Controllers\BranchController;
use App\Http\Controllers\ProvidersController;

Route::get('/providers', [ProvidersController::class,'index'])->name('providers')->middleware('can:show provider');
Route::post('/providers/accept', [ProvidersController::class,'accept'])->name('providers.accept')->middleware('can:update provider');
Route::post('/providers/reject', [ProvidersController::class,'reject'])->name('providers.reject')->middleware('can:update provider');
Route::post('/providers/update/{id}', [ProvidersController::class,'update'])->name('providers.update')->middleware('can:update provider');
Route::post('/providers/update_available/{id}', [ProvidersController::class,'updateAvailable'])->name('providers.update_available')->middleware('can:update provider');
Route::get('/providers/show/{id}', [ProvidersController::class,'show'])->name('providers.show')->middleware('can:show provider');
Route::post('/providers/change-level/{id}', [ProvidersController::class,'changeLevel'])->name('providers.change-level')->middleware('can:update provider');
//route providers end





