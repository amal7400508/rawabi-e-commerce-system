<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix providers_management.
|
*/

use App\Http\Controllers\BranchController;
use App\Http\Controllers\EnterpriseController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
//route profile start

//Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/profile', [ProfileController::class, 'index'])->name('profile');

Route::get('/profile/delete_image', [ProfileController::class,'delete_image'])->name('profile.delete_image');
Route::post('/profile/edit_image', [ProfileController::class,'edit_image'])->name('profile.edit_image');
Route::get('/profile/edit/{id}', [ProfileController::class,'edit'])->name('profile.edit');
Route::get('/profile/edit_password/{id}', [ProfileController::class,'edit_password'])->name('profile.edit_password');
Route::post('/profile/update', [ProfileController::class,'update'])->name('profile.update');
Route::post('/profile/update_password/{id}', [ProfileController::class,'update_password'])->name('profile.update_password');
Route::post('/profile/hdelete/{id}', [ProfileController::class,'update_password'])->name('profile.hdelete');
//route profile end

//route branches start
Route::get('/branches', [BranchController::class,'index'])->name('branches');
Route::get('/branches/map', [BranchController::class,'map'])->name('branches.map');
Route::post('/branches/store', [BranchController::class,'store'])->name('branches.store');
Route::get('/branches/destroy/{id}', [BranchController::class,'destroy'])->name('branches.destroy');
Route::get('/branches/show/{id}', [BranchController::class,'show'])->name('branches.show');
Route::get('/branches/recycle_bin', [BranchController::class,'recycle_bin'])->name('branches.recycle_bin');
Route::get('/branches/create', [BranchController::class,'create'])->name('branches.create');
Route::get('/branches/restore/{id}', [BranchController::class,'restore'])->name('branches.restore');
Route::get('/branches/hdelete/{id}', [BranchController::class,'hdelete'])->name('branches.hdelete');
Route::get('/branches/edit/{id}', [BranchController::class,'edit'])->name('branches.edit');
Route::post('/branches/update/{id}', [BranchController::class,'update'])->name('branches.update');
//route branches start

//route enterprises start
Route::get('/provider', [EnterpriseController::class,'index'])->name('provider');
Route::post('/provider/accept', [EnterpriseController::class,'accept'])->name('provider.accept');
Route::post('/provider/sreject', [EnterpriseController::class,'reject'])->name('provider.reject');
Route::post('/provider/update/{id}', [EnterpriseController::class,'update'])->name('provider.update');
Route::get('/provider/show/{id}', [EnterpriseController::class,'show'])->name('provider.show');
Route::post('/provider/store', [EnterpriseController::class,'store'])->name('profile_managment_provider.store');
Route::get('/provider/get-branch/{enterprise_id}', [EnterpriseController::class,'getBranch'])->name('provider.get-branch');

//route enterprises end
