<?php

//route suggestions start.
use App\Http\Controllers\RateController;
use App\Http\Controllers\SuggestionController;

Route::get('/suggestions', [SuggestionController::class,'index'])->name('suggestions')->middleware('can:show suggestions');
Route::get('/suggestions/show/{id}', [SuggestionController::class,'show'])->name('suggestions.show')->middleware('can:show suggestions');
//route suggestions end.

//route rate start
Route::get('/rates', [RateController::class,'index'])->name('rates')->middleware('can:show rates');
//route rate end
