<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix notify_management.
|
*/

use App\Http\Controllers\NotificationController;
use App\Http\Controllers\NotificationMovementController;
use Illuminate\Support\Facades\Route;

//route notifications start
Route::get('/notifications', [NotificationController::class,'index'])->name('notifications')->middleware('can:show notifications');
Route::get('/notifications/show/{id}', [NotificationController::class,'show'])->middleware('can:show notifications');
Route::get('/notifications/edit/{id}', [NotificationController::class,'edit'])->middleware('can:update notifications');
Route::post('/notifications/store', [NotificationController::class,'store'])->name('notifications.store')->middleware('can:create notifications');
Route::post('/notifications/update/{id}', [NotificationController::class,'update'])->name('notifications.update')->middleware('can:update notifications');
Route::delete('/notifications/destroy/{id}', [NotificationController::class,'destroy'])->name('notifications.destroy')->middleware('can:delete notifications');
//route notifications end

//route notification_movement start
Route::get('/notification_movement', [NotificationMovementController::class,'index'])->name('notification_movement')->middleware('can:show notification_movement');
Route::get('/notification_movement_branches', [NotificationMovementController::class,'notificationMovementBranches'])->name('notification_movement_branches')->middleware('can:show notification_movement');
Route::get('/notification_movement/show/{id}', [NotificationMovementController::class,'show'])->middleware('can:show notification_movement');
Route::post('/notification_movement/store', [NotificationMovementController::class,'store'])->name('notification_movement.store')->middleware('can:create notification_movement');

Route::post('/notification_movement/check-phone', [NotificationMovementController::class,'checkPhone'])->name('notification_movement.check_phone')->middleware('can:create notification_movement');

Route::post('/notification_movement/accept', [NotificationMovementController::class,'accept'])->name('notification_movement.accept')->middleware('can:update notification_movement');
Route::post('/notification_movement/reject', [NotificationMovementController::class,'reject'])->name('notification_movement.reject')->middleware('can:update notification_movement');

//route notification_movement end



