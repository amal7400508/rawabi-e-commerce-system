<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix advertisements.
|
*/

//route advertisement_types start
use App\Http\Controllers\AdvertisementController;
use App\Http\Controllers\AdvertisementTypeController;
use App\Http\Controllers\PaidAdvertisementController;

Route::get('/advertisement_types', [AdvertisementTypeController::class,'index'])->name('advertisement_types')->middleware('can:show advertisement_types');
Route::post('/advertisement_types/store', [AdvertisementTypeController::class,'store'])->name('advertisement_types.store')->middleware('can:create advertisement_types');
Route::get('/advertisement_types/show/{id}', [AdvertisementTypeController::class,'show'])->name('advertisement_types.show')->middleware('can:show advertisement_types');
Route::get('/advertisement_types/edit/{id}', [AdvertisementTypeController::class,'edit'])->name('advertisement_types.edit')->middleware('can:update advertisement_types');
Route::post('/advertisement_types/update/{id}', [AdvertisementTypeController::class,'update'])->name('advertisement_types.update')->middleware('can:update advertisement_types');
Route::delete('/advertisement_types/destroy/{id}', [AdvertisementTypeController::class,'destroy'])->name('advertisement_types.destroy')->middleware('can:delete advertisement_types');
//route advertisement_types end

//route advertisements start
Route::get('/advertisements', [AdvertisementController::class,'index'])->name('advertisements')->middleware('can:show advertisements');
Route::post('/advertisements/store', [AdvertisementController::class,'store'])->name('advertisements.store')->middleware('can:create advertisements');
Route::get('/advertisements/show/{id}', [AdvertisementController::class,'show'])->name('advertisements.show')->middleware('can:show advertisements');
Route::get('/advertisements/edit/{id}', [AdvertisementController::class,'edit'])->name('advertisements.edit')->middleware('can:update advertisements');
Route::post('/advertisements/update/{id}', [AdvertisementController::class,'update'])->name('advertisements.update')->middleware('can:update advertisements');
Route::delete('/advertisements/destroy/{id}', [AdvertisementController::class,'destroy'])->name('advertisements.destroy')->middleware('can:delete advertisements');
//route advertisements end

//route paid_advertisements start
Route::get('/paid_advertisements', [PaidAdvertisementController::class,'index'])->name('paid_advertisements')->middleware('can:show paid_advertisements');
Route::post('/paid_advertisements/store', [PaidAdvertisementController::class,'store'])->name('paid_advertisements.store')->middleware('can:show paid_advertisements');
Route::get('/paid_advertisements/show/{id}', [PaidAdvertisementController::class,'show'])->name('paid_advertisements.show')->middleware('can:show paid_advertisements');
Route::get('/paid_advertisements/edit/{id}', [PaidAdvertisementController::class,'edit'])->name('paid_advertisements.edit')->middleware('can:show paid_advertisements');
Route::post('/paid_advertisements/update/{id}', [PaidAdvertisementController::class,'update'])->name('paid_advertisements.update')->middleware('can:show paid_advertisements');
Route::delete('/paid_advertisements/destroy/{id}', [PaidAdvertisementController::class,'destroy'])->name('paid_advertisements.destroy')->middleware('can:show paid_advertisements');
//route paid_advertisements end
