<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix discounts.
|
*/


//route coupon_type start.
use App\Http\Controllers\CouponController;
use App\Http\Controllers\CouponTypeController;
use App\Http\Controllers\ProductDiscountController;
use App\Http\Controllers\VolumeDiscountController;

Route::get('/coupon_type', [CouponTypeController::class,'index'])->name('coupon_type')->middleware('can:show coupon_type');
Route::post('/coupon_type/store', [CouponTypeController::class,'store'])->name('coupon_type.store')->middleware('can:create coupon_type');
Route::delete('/coupon_type/destroy/{id}', [CouponTypeController::class,'destroy'])->name('coupon_type.destroy')->middleware('can:delete coupon_type');
Route::get('/coupon_type/show/{id}', [CouponTypeController::class,'show'])->name('coupon_type.show')->middleware('can:show coupon_type');
Route::get('/coupon_type/edit/{id}', [CouponTypeController::class,'edit'])->name('coupon_type.edit')->middleware('can:update coupon_type');
Route::post('/coupon_type/update/{id}', [CouponTypeController::class,'update'])->name('coupon_type.update')->middleware('can:update coupon_type');
//route coupon_type end.

//route coupons start.
Route::get('/coupons', [CouponController::class,'index'])->name('coupons')->middleware('can:show coupons');
Route::get('/coupons_branches', [CouponController::class,'couponsBranches'])->name('coupons_branches')->middleware('can:show coupons');
Route::post('/coupons/store', [CouponController::class,'store'])->name('coupons.store')->middleware('can:create coupons');
Route::delete('/coupons/destroy/{id}', [CouponController::class,'destroy]'])->name('coupons.destroy')->middleware('can:delete coupons');
Route::get('/coupons/show/{id}', [CouponController::class,'show'])->name('coupons.show')->middleware('can:show coupons');
Route::get('/coupons/create', [CouponController::class,'create'])->name('coupons.create')->middleware('can:create coupons');
Route::get('/coupons/edit/{id}', [CouponController::class,'edit'])->name('coupons.edit')->middleware('can:update coupons');
Route::post('/coupons/update/{id}', [CouponController::class,'update'])->name('coupons.update')->middleware('can:update coupons');

Route::post('/coupons/accept', [CouponController::class,'accept'])->name('coupons.accept')->middleware('can:update coupons');
Route::post('/coupons/reject', [CouponController::class,'reject'])->name('coupons.reject')->middleware('can:update coupons');
//route coupons end.


//route volume_discounts start
Route::get('/volume_discounts', [VolumeDiscountController::class,'index'])->name('volume_discounts')->middleware('can:show volume_discounts');
Route::post('/volume_discounts/store', [VolumeDiscountController::class,'store'])->name('volume_discounts.store')->middleware('can:update volume_discounts');
Route::post('/volume_discounts/accept', [VolumeDiscountController::class,'accept'])->name('volume_discounts.accept')->middleware('can:update volume_discounts');
Route::post('/volume_discounts/reject', [VolumeDiscountController::class,'reject'])->name('volume_discounts.reject')->middleware('can:update volume_discounts');
Route::post('/volume_discounts/update/{id}', [VolumeDiscountController::class,'update'])->name('volume_discounts.update')->middleware('can:update volume_discounts');
Route::get('/volume_discounts/show/{id}', [VolumeDiscountController::class,'show'])->name('volume_discounts.show')->middleware('can:show volume_discounts');
//route volume_discounts end


//route product_discounts start
Route::get('/product_discounts', [ProductDiscountController::class,'index'])->name('product_discounts')->middleware('can:show product_discounts');
Route::get('/product_discounts/create', [ProductDiscountController::class,'create'])->name('product_discounts.create')->middleware('can:create product_discounts');
Route::post('/product_discounts/store', [ProductDiscountController::class,'store'])->name('product_discounts.store')->middleware('can:create product_discounts');
Route::get('/product_discounts/edit/{id}', [ProductDiscountController::class,'edit'])->name('product_discounts.edit')->middleware('can:update product_discounts');
Route::post('/product_discounts/update/{id}', [ProductDiscountController::class,'update'])->name('product_discounts.update')->middleware('can:update product_discounts');
Route::delete('/product_discounts/destroy/{id}', [ProductDiscountController::class,'destroy'])->name('product_discounts.destroy')->middleware('can:update product_discounts');
//route product_discounts end

