<?php


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
| Prefix carrier_management.
|
*/

//route carriers start
use App\Http\Controllers\CarrierController;
use App\Http\Controllers\DeliveryPriceController;
use App\Http\Controllers\MultiDeliveryController;

Route::get('/carriers', [CarrierController::class,'index'])->name('carriers')->middleware('can:show carriers');
Route::post('/carriers/store', [CarrierController::class,'store'])->name('carriers.store')->middleware('can:create carriers');
Route::get('/carriers/show/{id}', [CarrierController::class,'show'])->name('carriers.show')->middleware('can:show carriers');
Route::get('/carriers/edit/{id}', [CarrierController::class,'edit'])->name('carriers.edit')->middleware('can:update carriers');
Route::post('/carriers/update/{id}', [CarrierController::class,'update'])->name('carriers.update')->middleware('can:update carriers');
Route::delete('/carriers/destroy/{id}', [CarrierController::class,'destroy'])->name('carriers.destroy')->middleware('can:delete carriers');

Route::get('/carriers/report', [CarrierController::class,'report'])->name('carriers.report')->middleware('can:show carriers_report');
Route::get('/carriers/export', [CarrierController::class,'export'])->name('carriers.export')->middleware('can:show carriers');
//route carriers end

//route carrier_price start
Route::get('/delivery_prices', [DeliveryPriceController::class,'index'])->name('delivery_prices')->middleware('can:show delivery_prices');
Route::post('/delivery_prices/store', [DeliveryPriceController::class,'store'])->name('delivery_prices.store')->middleware('can:create delivery_prices');
Route::get('/delivery_prices/show/{id}', [DeliveryPriceController::class,'show'])->name('delivery_prices.show')->middleware('can:show delivery_prices');
Route::get('/delivery_prices/edit/{id}', [DeliveryPriceController::class,'edit'])->name('delivery_prices.edit')->middleware('can:update delivery_prices');
Route::post('/delivery_prices/update/{id}', [DeliveryPriceController::class,'update'])->name('delivery_prices.update')->middleware('can:update delivery_prices');
Route::delete('/delivery_prices/destroy/{id}', [DeliveryPriceController::class,'destroy'])->name('delivery_prices.destroy')->middleware('can:delete delivery_prices');
//route carrier_price end

//route multi_delivery start
Route::get('/multi_delivery', [MultiDeliveryController::class,'index'])->name('multi_delivery')/*->middleware('can:show multi_delivery')*/;
Route::post('/multi_delivery/store', [MultiDeliveryController::class,'store'])->name('multi_delivery.store')/*->middleware('can:create multi_delivery')*/;
Route::get('/multi_delivery/show/{id}', [MultiDeliveryController::class,'show'])->name('multi_delivery.show')/*->middleware('can:show multi_delivery')*/;
Route::get('/multi_delivery/edit/{id}', [MultiDeliveryController::class,'edit'])->name('multi_delivery.edit')/*->middleware('can:update multi_delivery')*/;
Route::post('/multi_delivery/update/{id}', [MultiDeliveryController::class,'update'])->name('multi_delivery.update')/*->middleware('can:update multi_delivery')*/;
Route::delete('/multi_delivery/destroy/{id}', [MultiDeliveryController::class,'destroy'])->name('multi_delivery.destroy')/*->middleware('can:delete multi_delivery')*/;
//route multi_delivery end

