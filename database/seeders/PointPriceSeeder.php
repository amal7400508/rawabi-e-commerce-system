<?php

use App\PointPrice;
use Illuminate\Database\Seeder;

class PointPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $point_price = new PointPrice();
        $point_price->price=100;
        $point_price->save();
    }
}
