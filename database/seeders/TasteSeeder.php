<?php

use Illuminate\Database\Seeder;
use App\Taste;

class TasteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taste = new Taste();
        $taste->save();
        $taste->translateOrNew('ar')->taste = 'لايوجد بيانات';
        $taste->translateOrNew('en')->taste = 'no data';
        $taste->save();
    }
}
