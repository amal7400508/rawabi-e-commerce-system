<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        // create Permissions for interface

        //permission dashboard
        Permission::create(['name' => 'show dashboard', 'guard_name' => 'admin', 'guard_name' => 'admin']);

        //permission main_categories
        Permission::create(['name' => 'show main_categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update main_categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create main_categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete main_categories', 'guard_name' => 'admin']);

        //permission working_timesweb
        Permission::create(['name' => 'show working_times', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update working_times', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create working_times', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete working_times', 'guard_name' => 'admin']);

        //permission categories
        Permission::create(['name' => 'show categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete categories', 'guard_name' => 'admin']);

        //permission areas
        Permission::create(['name' => 'show areas', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update areas', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create areas', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete areas', 'guard_name' => 'admin']);

        //permission unit_types
        Permission::create(['name' => 'show unit_types', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update unit_types', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create unit_types', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete unit_types', 'guard_name' => 'admin']);

        //permission units
        Permission::create(['name' => 'show units', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update units', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create units', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete units', 'guard_name' => 'admin']);

        //permission offer_types
        Permission::create(['name' => 'show offer_types', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update offer_types', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create offer_types', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete offer_types', 'guard_name' => 'admin']);

        //permission notes
        Permission::create(['name' => 'show notes', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update notes', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create notes', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete notes', 'guard_name' => 'admin']);

        //permission pharmacy_categories
        Permission::create(['name' => 'show pharmacy_categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update pharmacy_categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create pharmacy_categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete pharmacy_categories', 'guard_name' => 'admin']);

        //permission main_category_categories
        Permission::create(['name' => 'show main_category_categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create main_category_categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete main_category_categories', 'guard_name' => 'admin']);

        //permission branch_services
        Permission::create(['name' => 'show branch_services', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create branch_services', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete branch_services', 'guard_name' => 'admin']);

        //permission social_medias
        Permission::create(['name' => 'show social_medias', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update social_medias', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create social_medias', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete social_medias', 'guard_name' => 'admin']);

        //permission ceiling
        Permission::create(['name' => 'show ceiling', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update ceiling', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create ceiling', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete ceiling', 'guard_name' => 'admin']);

        //permission advertisement_types
        Permission::create(['name' => 'show advertisement_types', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update advertisement_types', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create advertisement_types', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete advertisement_types', 'guard_name' => 'admin']);

        //permission advertisements
        Permission::create(['name' => 'show advertisements', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update advertisements', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create advertisements', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete advertisements', 'guard_name' => 'admin']);

        //permission paid_advertisements
        Permission::create(['name' => 'show paid_advertisements', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update paid_advertisements', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create paid_advertisements', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete paid_advertisements', 'guard_name' => 'admin']);

        //permission users
        Permission::create(['name' => 'show users', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update users', 'guard_name' => 'admin']);

        //permission deposits
        Permission::create(['name' => 'show deposits', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create deposits', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update deposits', 'guard_name' => 'admin']);

        //permission user_addresses
        Permission::create(['name' => 'show user_addresses', 'guard_name' => 'admin']);

        //permission products
        Permission::create(['name' => 'show products', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update products', 'guard_name' => 'admin']);

        //permission attentions
        Permission::create(['name' => 'show attentions', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update attentions', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create attentions', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete attentions', 'guard_name' => 'admin']);

        //permission offer
        Permission::create(['name' => 'show offer', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update offer', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create offer', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete offer', 'guard_name' => 'admin']);

        //permission request
        Permission::create(['name' => 'show request', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update request', 'guard_name' => 'admin']);

        //permission tracking
        Permission::create(['name' => 'show tracking', 'guard_name' => 'admin']);

        //permission carriers
        Permission::create(['name' => 'show carriers', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update carriers', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create carriers', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete carriers', 'guard_name' => 'admin']);

        //permission delivery_prices
        Permission::create(['name' => 'show delivery_prices', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update delivery_prices', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create delivery_prices', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete delivery_prices', 'guard_name' => 'admin']);

        //permission provider
        Permission::create(['name' => 'show provider', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update provider', 'guard_name' => 'admin']);

        //permission branches
        Permission::create(['name' => 'show branches', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update branches', 'guard_name' => 'admin']);

        //permission suggestions
        Permission::create(['name' => 'show suggestions', 'guard_name' => 'admin']);

        //permission rates
        Permission::create(['name' => 'show rates', 'guard_name' => 'admin']);

        //permission notifications
        Permission::create(['name' => 'show notifications', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update notifications', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create notifications', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete notifications', 'guard_name' => 'admin']);

        //permission notification_movement
        Permission::create(['name' => 'show notification_movement', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update notification_movement', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create notification_movement', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete notification_movement', 'guard_name' => 'admin']);

        //permission coupon_type
        Permission::create(['name' => 'show coupon_type', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update coupon_type', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create coupon_type', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete coupon_type', 'guard_name' => 'admin']);

        //permission color
        Permission::create(['name' => 'show color', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update color', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create color', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete color', 'guard_name' => 'admin']);


        //permission stocks
        Permission::create(['name' => 'show stocks', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update stocks', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create stocks', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete stocks', 'guard_name' => 'admin']);

        //permission coupons
        Permission::create(['name' => 'show coupons', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update coupons', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create coupons', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete coupons', 'guard_name' => 'admin']);

        //permission volume_discounts
        Permission::create(['name' => 'show volume_discounts', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update volume_discounts', 'guard_name' => 'admin']);

        //permission accounts
        Permission::create(['name' => 'show accounts', 'guard_name' => 'admin']);

        //permission receipt
        Permission::create(['name' => 'show receipt', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create receipt', 'guard_name' => 'admin']);

        //permission admin
        Permission::create(['name' => 'show admin', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update admin', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create admin', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create admin_permission', 'guard_name' => 'admin']);

        //permission audit
        Permission::create(['name' => 'show audit', 'guard_name' => 'admin']);

        //permission permission
        Permission::create(['name' => 'show permission', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update permission', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create permission', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete permission', 'guard_name' => 'admin']);

        //permission commission
        Permission::create(['name' => 'show commissions', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update commissions', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create commissions', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete commissions', 'guard_name' => 'admin']);

        //permission working_times_branch
        Permission::create(['name' => 'show working_times_branch', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update working_times_branch', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create working_times_branch', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete working_times_branch', 'guard_name' => 'admin']);

        //permission product
        Permission::create(['name' => 'show product', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update product', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create product', 'guard_name' => 'admin']);
        Permission::create(['name' => 'delete product', 'guard_name' => 'admin']);

        //permission special_product
        Permission::create(['name' => 'show special_product', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update special_product', 'guard_name' => 'admin']);

        //permission deferred
        Permission::create(['name' => 'show deferred', 'guard_name' => 'admin']);

        //permission prescription-replies
        Permission::create(['name' => 'show prescription-replies', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update prescription-replies', 'guard_name' => 'admin']);

        //permission prescription
        Permission::create(['name' => 'show prescription', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update prescription', 'guard_name' => 'admin']);

        //permission product_discounts
        Permission::create(['name' => 'show product_discounts', 'guard_name' => 'admin']);
        Permission::create(['name' => 'update product_discounts', 'guard_name' => 'admin']);
        Permission::create(['name' => 'create product_discounts', 'guard_name' => 'admin']);

        //permission statistics
        Permission::create(['name' => 'show statistics', 'guard_name' => 'admin']);

        //permission users_report
        Permission::create(['name' => 'show users_report', 'guard_name' => 'admin']);

        //permission products_report
        Permission::create(['name' => 'show products_report', 'guard_name' => 'admin']);

        //permission request_report
        Permission::create(['name' => 'show request_report', 'guard_name' => 'admin']);

        //permission carriers_report
        Permission::create(['name' => 'show carriers_report', 'guard_name' => 'admin']);

        // this can be done as separate statements


        $role = Role::create(['name' => 'admin_administrator', 'guard_name' => 'admin']);
        $role->givePermissionTo(Permission::where('guard_name', 'admin')->get());

        Admin::where('email', 'alsaloul@gmail.com')->first()->assignRole('admin_administrator');

//        Admin::create([
//            'name' => 'Administrator',
//            'email' => 'admin@moka.com',
//            'gender' => 'man',
//            'image' => 'avatar.jpg',
//            'phone' => '774411357',
//            'status' => '1',
//            'password' => Hash::make('123456789'),
//        ]);

        $admin_role = Role::create(['name' => 'admin', 'guard_name' => 'admin']); //for mk
        $admin_role->givePermissionTo('show dashboard');
    }
}
