<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use NotificationSeeder;
use NotificationTypeSeeder;
use OfferTypeSeeder;
use PointPriceSeeder;
use RateTypeSeeder;

use TasteSeeder;
use UnitSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(UnitSeeder::class);
        $this->call(OfferTypeSeeder::class);
        $this->call(TasteSeeder::class);
        $this->call(RateTypeSeeder::class);
        $this->call(PointPriceSeeder::class);
        $this->call(NotificationTypeSeeder::class);
        $this->call(NotificationSeeder::class);
    }
}
