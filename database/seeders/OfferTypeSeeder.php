<?php

use Illuminate\Database\Seeder;
use App\OfferType;

class OfferTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $offer_type = new OfferType();
        $offer_type->save();
        $offer_type->translateOrNew('ar')->name = 'لايوجد بيانات';
        $offer_type->translateOrNew('en')->name = 'no data';
        $offer_type->save();

        $offer_type = new OfferType();
        $offer_type->save();
        $offer_type->translateOrNew('ar')->name = 'جدبدة';
        $offer_type->translateOrNew('en')->name = 'new';
        $offer_type->save();

        $offer_type = new OfferType();
        $offer_type->save();
        $offer_type->translateOrNew('ar')->name = 'موسمية';
        $offer_type->translateOrNew('en')->name = 'Seasonality';
        $offer_type->save();

        $offer_type = new OfferType();
        $offer_type->save();
        $offer_type->translateOrNew('ar')->name = 'خاصة';
        $offer_type->translateOrNew('en')->name = 'specially';
        $offer_type->save();

    }
}
