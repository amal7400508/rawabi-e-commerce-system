<?php

use Illuminate\Database\Seeder;
USE App\RateType;

class RateTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taste = new RateType();
        $taste->save();
        $taste->translateOrNew('ar')->name = 'الخدمة';
        $taste->translateOrNew('en')->name = 'service';
        $taste->save();

        $taste = new RateType();
        $taste->save();
        $taste->translateOrNew('ar')->name = 'المذاق';
        $taste->translateOrNew('en')->name = 'taste';
        $taste->save();

        $taste = new RateType();
        $taste->save();
        $taste->translateOrNew('ar')->name = 'الموصل';
        $taste->translateOrNew('en')->name = 'carrier';
        $taste->save();
    }
}
