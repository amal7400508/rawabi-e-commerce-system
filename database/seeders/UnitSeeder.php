<?php

use Illuminate\Database\Seeder;
use App\Unit;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unit = new Unit();
        $unit->save();
        $unit->translateOrNew('ar')->name = 'لايوجد بيانات';
        $unit->translateOrNew('ar')->type = 'لايوجد بيانات';

        $unit->translateOrNew('en')->name = 'no data';
        $unit->translateOrNew('en')->type = 'no data';
        $unit->save();
    }
}
