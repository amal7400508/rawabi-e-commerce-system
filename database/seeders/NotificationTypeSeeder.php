<?php

use Illuminate\Database\Seeder;
USE App\NotificationType;

class NotificationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notification_type = new NotificationType();
        $notification_type->save();
        $notification_type->translateOrNew('ar')->name = 'إشعارات النظام';
        $notification_type->translateOrNew('en')->name = 'Notification System';
        $notification_type->save();

    }
}
