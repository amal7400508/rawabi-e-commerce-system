<?php

use Illuminate\Database\Seeder;
USE App\Notification;

class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notification = new Notification();
        $notification->type_id = 1;
        $notification->save();
        $notification->translateOrNew('ar')->notification_title = 'إيداع';
        $notification->translateOrNew('ar')->notification_message = 'تم الإيداع إلى رصيدك مبلغ';
        $notification->translateOrNew('en')->notification_title = 'Deposit';
        $notification->translateOrNew('en')->notification_message = 'Deposit to your balance amount';
        $notification->save();

        $notification2 = new Notification();
        $notification2->type_id = 1;
        $notification2->save();
        $notification2->translateOrNew('ar')->notification_title = 'فتح حساب';
        $notification2->translateOrNew('ar')->notification_message = 'لقد تم فتح حسابك بمبلغ';
        $notification2->translateOrNew('en')->notification_title = 'Open an account';
        $notification2->translateOrNew('en')->notification_message = 'Your account has been opened with';
        $notification2->save();

    }
}
