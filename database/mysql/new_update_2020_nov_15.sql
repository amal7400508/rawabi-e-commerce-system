-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2020 at 09:31 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `moka_at_10_12`
--

-- --------------------------------------------------------

--
-- Table structure for table `special_classes`
--

CREATE TABLE `special_classes` (
                                   `id` bigint(20) UNSIGNED NOT NULL,
                                   `status` tinyint(1) NOT NULL DEFAULT 1,
                                   `created_at` timestamp NULL DEFAULT NULL,
                                   `updated_at` timestamp NULL DEFAULT NULL,
                                   `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `special_class_translations`
--

CREATE TABLE `special_class_translations` (
                                              `id` bigint(20) UNSIGNED NOT NULL,
                                              `special_class_id` bigint(20) UNSIGNED NOT NULL,
                                              `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                                              `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                                              `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                              `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
                          `id` bigint(20) UNSIGNED NOT NULL,
                          `status` tinyint(1) NOT NULL DEFAULT 1,
                          `created_at` timestamp NULL DEFAULT NULL,
                          `updated_at` timestamp NULL DEFAULT NULL,
                          `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `state_translations`
--

CREATE TABLE `state_translations` (
                                      `id` bigint(20) UNSIGNED NOT NULL,
                                      `state_id` bigint(20) UNSIGNED NOT NULL,
                                      `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `special_classes`
--
ALTER TABLE `special_classes`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `special_class_translations`
--
ALTER TABLE `special_class_translations`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `special_class_translations_special_class_id_locale_unique` (`special_class_id`,`locale`),
    ADD KEY `special_class_translations_locale_index` (`locale`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state_translations`
--
ALTER TABLE `state_translations`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `state_translations_state_id_locale_unique` (`state_id`,`locale`),
    ADD KEY `state_translations_locale_index` (`locale`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `special_classes`
--
ALTER TABLE `special_classes`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `special_class_translations`
--
ALTER TABLE `special_class_translations`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `state_translations`
--
ALTER TABLE `state_translations`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `special_class_translations`
--
ALTER TABLE `special_class_translations`
    ADD CONSTRAINT `special_class_translations_special_class_id_foreign` FOREIGN KEY (`special_class_id`) REFERENCES `special_classes` (`id`);

--
-- Constraints for table `state_translations`
--
ALTER TABLE `state_translations`
    ADD CONSTRAINT `state_translations_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`);


-- ----------------------------------------------------------------------------------------------------
--
-- INSERT NEW SPECIAL CLASS ROW
--
INSERT INTO `special_classes` (`id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES ('1', '1', '2020-11-15 00:00:00', '2020-11-15 00:00:00', '2020-11-15 00:00:00');
--
-- INSERT NEW SPECIAL CLASS TRANSLATIONS ROW
--
INSERT INTO `special_class_translations` (`id`, `special_class_id`, `locale`, `name`, `details`, `image`) VALUES (NULL, '1', 'ar', 'عام', NULL, NULL), (NULL, '1', 'en', 'General', NULL, NULL);

--
-- ADD_COLUMN for table `show_cases`
--
ALTER TABLE `show_cases` ADD `special_class_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '1' AFTER `id`;

--
-- Constraints for table `show_cases`
--
ALTER TABLE `show_cases` ADD CONSTRAINT `show_cases_show_case_id_forign` FOREIGN KEY (`special_class_id`) REFERENCES `special_classes`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

COMMIT;
