<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/**
 * EvnNoti = EventNotification
 *
 */
class CreateEvnNotiTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evn_noti_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("evn_noti_id");
            $table->string('locale')->index();
            $table->string('title');
            $table->string('notification');

            $table->unique(['evn_noti_id','locale']);
            $table->foreign('evn_noti_id')->references('id')->on('evn_notis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evn_noti_translations');
    }
}
