<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_products', function (Blueprint $table) {

            $table->bigIncrements('id');

            //TODO: Test this in api if there is a problem in special products of the user
            $table->boolean("status")->default(false);

            $table->string('name');
            $table->text('desc');
            $table->decimal('price', 8, 2)->nullable();
            $table->text('note')->nullable();
            $table->text('admin_message')->nullable();
            $table->string('image')->default("special_product_image.jpg");

            $table->unsignedBigInteger("user_id");

           $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
