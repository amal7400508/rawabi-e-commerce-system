<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitalities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean("status")->default(true);
            $table->string('name');
            $table->text('desc');
            $table->decimal('total_price', 10, 0);
            $table->decimal('quantity', 8, 2)->default(1.0);
            $table->date('date');
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("boxing_type_id");


            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('boxing_type_id')->references('id')->on('boxing__types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitalities');
    }
}
