<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_wallets', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->decimal('openning_balance', 8, 2)->default(0.0);
            $table->decimal('current_balance', 12, 2);
            $table->decimal('frozen_amount', 8, 2)->default(0.0);
            $table->boolean("status")->default(true);

            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('carrier_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('carrier_id')->references('id')->on('carriers');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_wallets');
    }
}
