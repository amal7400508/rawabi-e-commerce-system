<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrierPathTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrier_path', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('start_date')->useCurrent();
            $table->date('end_date')->nullable();
            $table->boolean("status")->default(true);

            $table->unsignedBigInteger("carrier_id");
            $table->unsignedBigInteger("delivery_path_id");


           $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('carrier_id')->references('id')->on('carriers');
            $table->foreign('delivery_path_id')->references('id')->on('delivery_paths');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrier_path');
    }
}
