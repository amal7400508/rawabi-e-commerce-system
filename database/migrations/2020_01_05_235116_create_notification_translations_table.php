<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("notification_id");
            $table->string('locale')->index();

//            $table->string('name')->nullable();
            $table->string('notification_title')->nullable();
            $table->string('notification_message');
//            $table->string('notification_image')->nullable();


            $table->unique(['notification_id','locale']);
            $table->foreign('notification_id')->references('id')->on('notifications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_translations');
    }
}
