<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_histories', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->unsignedBigInteger("gain_method_id");
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('carrier_id')->nullable();

            $table->integer('price')->default(0);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('carrier_id')->references('id')->on('carriers');

            $table->foreign('gain_method_id')->references('id')->on('gain_methods');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_histories');
    }
}
