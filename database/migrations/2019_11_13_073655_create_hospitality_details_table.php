<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalityDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitality_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("hospitality_id");
            $table->unsignedBigInteger("product_price_id");
            $table->bigInteger("product_quantity")->default(1);
            //$table->decimal('total_price', 8, 2);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('hospitality_id')->references('id')->on('hospitalities');
            $table->foreign('product_price_id')->references('id')->on('product_prices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitality_details');
    }
}
