<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_product', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger("product_id")->nullable();
            $table->unsignedBigInteger("event_type_id")->nullable();

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('event_type_id')->references('id')->on('event_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_product');
    }
}
