<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestStatusHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_status_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('status', ['requested', 'accepted','cancelled','ondeliver','delivered','reviewed','received']);

            $table->unsignedBigInteger("requst_id");

           $table->timestamps();
            $table->softDeletes();
            $table->foreign('requst_id')->references('id')->on('requests');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_status_histories');
    }
}
