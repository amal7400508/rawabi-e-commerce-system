<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
           
            $table->decimal('old_price', 8, 2);
            $table->decimal('price', 8, 2);
            $table->boolean("status")->default(true);
            $table->date('start_date')->useCurrent();
            $table->date('end_date')->nullable();

            $table->unsignedBigInteger("type_id")->nullable();
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('type_id')->references('id')->on('offer_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
