<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disposits', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->decimal('amount', 8, 2);
            $table->text('note')->nullable();
            
            $table->unsignedBigInteger("branch_id");
            $table->unsignedBigInteger("user_wallet_id");

           $table->timestamps();
            $table->softDeletes();

            
            $table->foreign('user_wallet_id')->references('id')->on('user_wallets');
            $table->foreign('branch_id')->references('id')->on('branches');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disposits');
    }
}
