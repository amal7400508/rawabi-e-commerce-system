<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->decimal('amount', 8, 2)->default(0.0);


            $table->enum('type',['add','subtract','cash','refund','add_from_point']);

            $table->text('note')->nullable();

            $table->unsignedBigInteger('user_wallet_id');
            $table->unsignedBigInteger('request_id')->nullable();
            $table->unsignedBigInteger('branch_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_wallet_id')->references('id')->on('user_wallets');
            $table->foreign('request_id')->references('id')->on('requests');
            $table->foreign('branch_id')->references('id')->on('branches');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_transactions');
    }
}
