<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->enum('gender',['man','woman']);
            $table->string('phone');
            $table->string('image',250)->nullable("users/avatar.jpg");
            $table->boolean("status")->default(true)->comment('1: active, 2: deleted, 3: blacklist, 0: attitude');
            $table->date("birth_date");
            $table->timestamp("email_verified_at")->nullable();
            $table->string("address");
            $table->bigInteger("points_account_num")->unique()->nullable();
            $table->bigInteger("parent_points_account_num")->nullable();
            $table->integer("monthly_share")->default(0);
            $table->string('password');
            $table->rememberToken();

            $table->timestamps();
            $table->softDeletes();

            $table->index(['points_account_num', 'id']);


            $table->foreign('parent_points_account_num')->references('points_account_num')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
