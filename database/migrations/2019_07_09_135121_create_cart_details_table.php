<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('quantity', 8, 2)->default(1.0);
            $table->enum("item_type",['product','special','offer','hospitality']);
            $table->unsignedBigInteger("cart_id");
            $table->unsignedBigInteger("product_price_id")->nullable();
            $table->unsignedBigInteger("offer_id")->nullable();
            $table->unsignedBigInteger("special_product_id")->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('cart_id')->references('id')->on('carts');
            $table->foreign('product_price_id')->references('id')->on('product_prices');
            $table->foreign('offer_id')->references('id')->on('offers');
            $table->foreign('special_product_id')->references('id')->on('special_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_details');
    }
}
