<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('rate');

            $table->unsignedBigInteger("rate_id");
            $table->unsignedBigInteger("rate_type_id");

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('rate_id')->references('id')->on('rates');
            $table->foreign('rate_type_id')->references('id')->on('rate_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_details');
    }
}
