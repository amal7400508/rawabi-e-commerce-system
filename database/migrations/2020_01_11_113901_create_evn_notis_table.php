<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * EvnNoti = EventNotification
 *
 */

class CreateEvnNotisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evn_notis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("event_type_id");
            $table->string('image')->nullable();
            $table->timestamps();

            $table->foreign('event_type_id')->references('id')->on('event_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evn_notis');
    }
}
