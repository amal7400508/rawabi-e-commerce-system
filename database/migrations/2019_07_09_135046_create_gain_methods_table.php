<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGainMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gain_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('required_min_price', 8, 2)->nullable();
            $table->decimal('required_max_price', 8, 2)->nullable();
            $table->decimal('required_count', 8, 2)->nullable();
            $table->decimal('required_percentage', 8, 2)->nullable();
            $table->decimal('price', 8, 2);
            $table->text('note')->nullable();

            $table->unsignedBigInteger("gain_type_id");

           $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('gain_type_id')->references('id')->on('gain_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gain_methods');
    }
}
