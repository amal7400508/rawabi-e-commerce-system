<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedByToRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function (Blueprint $table) {
            // 1. Create new column
            // You probably want to make the new column nullable
            $table->bigInteger('updated_by', 20)
                ->unsigned()->nullable()->after('coupon_id');

            $table->text('cancel_note')->nullable()->after('coupon_id');


            // 2. Create foreign key constraints
            $table->foreign('updated_by')->references('id')->on('admins')
                ->onDelete('SET NULL')->onUpdate('CASCADE');

            // ! MySQL Commands
            /**
             * Add columns
             * ALTER TABLE `requests` ADD `cancel_note` TEXT NULL DEFAULT NULL AFTER `coupon_id`, ADD `updated_by` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `cancel_note`;
             */
            /**
             * Add constraints
             * ALTER TABLE `requests` ADD CONSTRAINT `requests_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `admins`(`id`) ON DELETE SET NULL ON UPDATE CASCADE
             */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            // 1. Drop foreign key constraints
            $table->dropForeign(['updated_by']);

            // 2. Drop the column
            $table->dropColumn('updated_by');
            $table->dropColumn('cancel_note');
        });
    }
}
