<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('number')->unique();
            $table->decimal('price', 8, 2);
//            $table->integer('hour');
            $table->text('note')->nullable();
            $table->boolean("status")->default(true);
            $table->boolean("uses_status")->default(false);

            $table->date('start_date')->useCurrent();
            $table->date('end_date')->nullable();

            $table->unsignedBigInteger("coupon_types_id");

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('coupon_types_id')->references('id')->on('coupon_types');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
