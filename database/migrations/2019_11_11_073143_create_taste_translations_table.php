<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasteTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taste_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("taste_id");
            $table->string('locale')->index();
            $table->String('taste');

            $table->unique(['taste_id','locale']);
            $table->foreign('taste_id')->references('id')->on('tastes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taste_translations');
    }
}
