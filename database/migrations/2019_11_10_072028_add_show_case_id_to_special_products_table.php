<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowCaseIdToSpecialProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('special_products', function (Blueprint $table) {
            $table->unsignedBigInteger('show_case_id')->nullable();

            $table->foreign('show_case_id')->references('id')->on('show_cases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('special_products', function (Blueprint $table) {
            $table->dropColumn(['show_case_id']);
        });
    }
}
