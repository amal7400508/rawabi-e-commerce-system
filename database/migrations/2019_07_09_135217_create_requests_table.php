<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean("is_active")->default(true);
            $table->text('note')->nullable();
            $table->enum('payment_type', ['cash', 'fromWallet']);
            $table->enum('receiving_type', ['fromBranch', 'delivery']);
            $table->decimal('cache_payment',8,2)->default(0.00);
            $table->enum('status', ['requested', 'repair','delivered','deliver','reviewed',"canceled",
                    "received",'done',"completedRequest","on_the_way","on_branch"])->default('requested');

            $table->unsignedBigInteger("cart_id");
            $table->unsignedBigInteger("delivery_price_id")->nullable()->default(1);
            $table->unsignedBigInteger("branch_id")->nullable();
            $table->unsignedBigInteger("admin_id")->nullable();
            $table->unsignedBigInteger("carrier_id")->nullable();
            $table->unsignedBigInteger("address_id")->nullable();
            $table->unsignedBigInteger("coupon_id")->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cart_id')->references('id')->on('carts');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('admin_id')->references('id')->on('admins');
            $table->foreign('carrier_id')->references('id')->on('carriers');
            $table->foreign('address_id')->references('id')->on('user_addresses');
            $table->foreign('coupon_id')->references('id')->on('coupons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
